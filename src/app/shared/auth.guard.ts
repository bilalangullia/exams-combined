import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KdAlertEvent } from 'openemis-styleguide-lib';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  // private userDetails:any;

  constructor(public router: Router, private kdAlertEvent: KdAlertEvent) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (route.data.page === 'auth') {
      if (!this.isLoggedIn()) {
        return true;
      } else {
        let toasterConfig: any = {
          title: 'Unauthorised Access',
          body: '',
          showCloseButton: true,
          tapToDismiss: false
        };
        this.kdAlertEvent.error(toasterConfig);
        this.router.navigate(['/main']);
        return false;
      }
    } else if (route.data.page === 'main') {
      if (this.isLoggedIn()) {
        return true;
      } else {
        let toasterConfig: any = {
          title: 'Unauthorised Access',
          body: '',
          showCloseButton: true,
          tapToDismiss: false
        };
        this.kdAlertEvent.error(toasterConfig);
        this.router.navigate(['/auth']);
        return false;
      }
    } else {
      if (this.isLoggedIn()) {
        return true;
      } else {
        let toasterConfig: any = {
          title: 'Unauthorised Access',
          body: '',
          showCloseButton: true,
          tapToDismiss: false
        };
        this.kdAlertEvent.error(toasterConfig);
        this.router.navigate(['/auth']);
        return false;
      }
    }
  }

  isLoggedIn() {
    let accessToken: any;
    if (localStorage.getItem('token')) {
      accessToken = JSON.parse(window.atob(localStorage.getItem('token')));
      // this.userDetails = JSON.parse(localStorage.getItem('userInfo'));
    }
    return accessToken ? true : false;
  }
}
