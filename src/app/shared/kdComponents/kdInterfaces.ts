export interface IKdTabs {
  tabName?: string;
  tabContent?: string;
  tabId?: string;
  isActive?: boolean;
  disabled?: boolean;
  routerPath?: string;
}
