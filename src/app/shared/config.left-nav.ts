export const LEFT_NAV = [
  {
    header: 'Registrations',
    icon: 'fa fa-users',
    list: [
      { header: 'Candidates', path: 'main/registration/candidates' },
      { header: 'Fees', path: 'main/registration/fees' }
    ]
  },
  {
    header: 'Marks',
    icon: 'kd-header-row',
    list: [
      { header: 'Components', path: 'main/marks/components' },
      { header: 'Multiple Choices', path: 'main/marks/multiple-choice' }
    ]
  },
  {
    header: 'Results',
    icon: 'fa fa-graduation-cap',
    list: [
      { header: 'Forecast Grades', path: 'main/results/forecast-grades' },
      { header: 'Grade Reviews', path: 'main/results/grade-reviews' },
      { header: 'Final Grades', path: 'main/results/final-grades' }
    ]
  },
  {
    header: 'Certificates',
    icon: 'fa fa-certificate',
    list: [
      { header: 'Collections', path: 'main/certificates/collections' },
      { header: 'Duplicates', path: 'main/certificates/duplicates' },
      { header: 'Verifications', path: 'main/certificates/verifications' }
    ]
  },
  {
    header: 'Reports',
    icon: ' kd-reports',
    list: [
      { header: 'Registrations', path: 'main/reports/list' },
      {
        header: 'Form',
        list: [
          { header: 'MS Examiner', path: 'main/reports/form' },
          { header: 'Grades', path: 'main/reports/form/grade' },
          { header: 'Multiple Choice', path: 'main/reports/form/multichoice' }
        ]
      },
      { header: 'Marks', path: 'main/reports/marks' },
      { header: 'Results', path: 'main/reports/results' },
      { header: 'Certificates', path: 'main/reports/certificates' },
      {
        header: 'Statistics',
        list: [
          { header: 'Exam-Statistics', path: 'main/reports/statistics/statisticexam' },
          { header: 'Centre-Statistics', path: 'main/reports/statistics/statisticentre' }
        ]
      }
    ]
  },
  {
    header: 'Administration',
    icon: 'fa fa-cogs',
    list: [
      {
        header: 'System Setup',
        path: 'main/system-setup',
        list: [
          { header: 'Administrative Boundaries', path: 'main/system-setup/administrative-boundaries' },
          { header: 'Academic Period', path: 'main/system-setup/academic-period' },
          { header: 'Education Structure', path: 'main/system-setup/education-structure' },
          { header: 'Field Options', path: 'main/system-setup/field-options' },
          { header: 'System Configurations', path: 'main/system-setup/system-configurations' }
        ]
      },
      {
        header: 'Security',
        path: 'main/security',
        list: [
          { header: 'Users', path: 'main/security/users' },
          { header: 'Groups', path: 'main/security/groups' },
          { header: 'Roles', path: 'main/security/roles' }
        ]
      },
      {
        header: 'Examinations',
        path: 'main/examinations',
        list: [
          { header: 'Exams', path: 'main/examinations/exams' },
          { header: 'Centres', path: 'main/examinations/centres' },
          { header: 'Grading Types', path: 'main/examinations/gradingtypes' },
          {
            header: 'Multiple Choice',
            path: 'main/examinations/multiple-choice'
          },
          { header: 'Markers', path: 'main/examinations/markers' },
          { header: 'Fees', path: 'main/examinations/fees' }
        ]
      }
    ]
  }
];
