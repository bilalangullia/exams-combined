import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import urls from '../config.urls';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private accessList: any = null;
  public accessListChanged = new BehaviorSubject<any>({ ...this.accessList });

  constructor(private http: HttpClient, private router: Router) {}

  private handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };

  private setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers };
    }
  }

  getPermissionsList(token: string) {
    this.http
      .get(`${environment.baseUrl}/${urls.permission}${urls.list}`, this.setHeader())
      // .pipe(catchError(this.handleError))
      .subscribe(
        (res) => {
          // console.log('res', res);
        },
        (err) => {
          console.log('err', err);
        }
      );
  }
}

/* http://openemis.n2.iworklab.com/api/permissionlist */
