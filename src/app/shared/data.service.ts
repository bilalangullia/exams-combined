import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import urls from './config.urls';

@Injectable({ providedIn: 'root' })
export class DataService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient, public router: Router) {}

  private handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
      // case 0:
      //   console.log(0);
      //   break;
    }
    console.log(error);
    return throwError(error);
  };

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  getCandidateList() {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/listing`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  registrationCandidate(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/listing?start=${data.start}&end=${data.end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  serachCandidateList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/listing?start=${data.start}&end=${data.end}&search=${data.query}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
   advanceSearch(data){
     return this.httpClient
     .post(
       `${this.baseUrl}/${urls.registration}/${urls.candidate}/advance-search?start=${data.start}&end=${data.end}`,data,
       this.setHeader()
     )
     .pipe(catchError(this.handleError));
   }
  getCandidateView(data) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/view/${data}/details`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getCandidateDeleted(data) {
    return this.httpClient
      .delete(`${this.baseUrl}/${urls.registration}/${urls.candidate}/destroy-candidate/${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getCandidateImportTemplate(){
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/candidate-template`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  importFile(data) {
    return this.httpClient
      .post(`${this.baseUrl}/${urls.registration}/${urls.candidate}/import-candidates`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  importGradingType(data) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/grading-type/import`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  centreImportFile(data) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/centre/import`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getDefaultDropdownValues() {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}?entities=gender,nationality,academicPeriods,attendanceTypes,identityTypes,specialNeedTypes,specialNeedDifficulties`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getExamination(id) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${id}/examination`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getExamCenter(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/region/${data.area_id}/academic-id/${data.academic_period_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  searchCandidateId(id) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/autocomplete-candidate-id/${id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getPastExamCandidateDetail(data) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/${data}/details-by-id`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  searchOpenEmisID(id) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/autocomplete-emis-no/${id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getOpenEmisCandidateDetail(data) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/${data}/details-by-emis`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getCandidateEditDetails(id) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/get-candidate-detail/${id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getSubjects(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/subjects?examId=${data.examination_id}&attendanceType=${data.examination_attendance_types_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getAmountDue(data) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/exam/${data}/fee`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getAreaName() {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/country/region`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getAddressArea() {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/address/region`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getCandidateFeeList() {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/fee-list`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getCandidateIDList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/generate-candidate-ids?examination_centre_id=${data.examination_centre_id}&examination_id=${data.examination_id}&academic_period_id=${data.academic_period_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  getFormCandidateList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/candidate-dropdown?examination_centre_id=${data.examination_centre_id}&examination_id=${data.examination_id}&academic_period_id=${data.academic_period_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getMultipleChoiceFormList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/list-reports?start=${data.start}&end=${data.end}&module=multiple-choice`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getGradeFormList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/list-reports?start=${data.start}&end=${data.end}&module=grade`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getExaminerFormList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/list-reports?start=${data.start}&end=${data.end}&module=Msexaminer`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getStatisticExamList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/statistics/list?module=StatisticsExam&start=${data.start}&end=${data.end}&keyword=`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getStatistiCentreList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/statistics/list?module=CandidatePerSubjectStatistics&start=${data.start}&end=${data.end}&keyword=`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  getCertificateReportList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/list-reports?module=Certificate&start=${data.start}&end=${data.end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getResultReportList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/list-reports?module=Results&start=${data.start}&end=${data.end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  getFormSubject(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/${data.candidate_id}/subject-dropdown?examination_id=${data.examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getSubjectByExaminationID(data) {
    return this.httpClient
      .get(`${this.baseUrl}/registration/candidate/dropdown/${data}/examination-subject`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getOptionBySubjectId(data) {
    return this.httpClient
      .get(`${this.baseUrl}/registration/candidate/dropdown/${data}/examination-options`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /*** ADD | Autocomplete Canidate ID ***/
  getCandidateId(keyword: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.autocomplete}-${urls.candidate}-id/${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** ADD PAGE GET DETAILS BY ID  ***/
  getDetailsByCandidateId(
    academic_period_id,
    examination_id,
    examination_centre_id,
    examination_options_id,
    examination_components_id,
    candidate_id
  ) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/marks/${urls.component}/detailsby-cid?academic_period_id=${academic_period_id}&examination_id=${examination_id}&examination_centre_id=${examination_centre_id}&examination_options_id=${examination_options_id}&examination_components_id=${examination_components_id}&candidate_id=${candidate_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getOptionList() {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/option-list`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  register(payload) {
    return this.httpClient
      .post(`${this.baseUrl}/${urls.registration}/${urls.candidate}/register`, payload, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  updateCandidate(id, payload) {
    return this.httpClient
      .post(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/update-candidate-detail/${id.value}`,
        payload,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  registrationReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/registration/candidate/generate-report`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  formMultipleChoiceReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/forms/multiple-choice/generate-report`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  formScalingleMSReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/forms/generate/ms-examiner`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  certificateReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/certificates/generate-report-certificate`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  addCentreStaticReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/statistics/candidate-per-subject`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  addExamStatisticReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/statistics/exam-reports`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  formGradeReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/forms/grades/generate-forecastGrade-report`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }
  resultReport(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/results/generate-report-result`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getRegistrationReportList() {
    return this.httpClient
      .get(`${this.baseUrl}/registration/candidate/list-reports`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  downloadRegistrationReportList(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/downloadReport`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }
  subjectOption(data: any) {
    return this.httpClient
      .get(`${this.baseUrl}/marks/component/option-list/${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  marksComponents() {
    return this.httpClient.get(`${this.baseUrl}/component`, this.setHeader()).pipe(catchError(this.handleError));
  }

  addMarksComponent(data) {
    return this.httpClient
      .post(`${this.baseUrl}/marks/${urls.component}/${urls.addMarks}`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getStatusMarksComponent() {
    return this.httpClient
      .get(`${this.baseUrl}/marks/${urls.component}/status-dropdown`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getTypeMarksComponent() {
    return this.httpClient
      .get(`${this.baseUrl}/marks/${urls.component}/marktype-dropdown`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  addComponent(id, mark, status_id, examination_mark_types_id, examination_markers_id?) {
    let url = examination_markers_id ? `&examination_markers_id=${examination_markers_id}` : '';
    return this.httpClient
      .post(
        `${this.baseUrl}/${urls.component}/${urls.updateMarks}/${id}?mark=${mark}&status_id=${status_id}&examination_mark_types_id=${examination_mark_types_id}${url}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  login(payload) {
    return this.httpClient
      .post(`${this.baseUrl}/${urls.login}`, payload, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getCurrentUsername() {
    return this.httpClient.get(`${this.baseUrl}/${urls.username}`, this.setHeader()).pipe(catchError(this.handleError));
  }

  /** MARKS | MULTIPLE CHOICE | APIs START **/
  getComponentList(data: any) {
    return this.httpClient
      .get(
        `${this.baseUrl}/marks/component/component-list?academic_period_id=${data.academic_period_id}&examination_id=${data.examination_id}&examination_centre_id=${data.examination_centre_id}&examination_options_id=${data.examination_options_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** Adminstarter Grading Type  **/

  gradingTypeList() {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/grading-type/list`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  searchGradingTypeList(data: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/grading-type/list?keyword=${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  viewGradingType(data: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/grading-type/${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  addGradingType(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/grading-type/store`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  updateGradingType(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/grading-type/update`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  deleteGradingType(data: any) {
    return this.httpClient
      .delete(`${this.baseUrl}/administration/examinations/grading-type/${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** Adminstration examination centre */
  examCentreList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/list?examination_id=${data.examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  examCentreSearch(data, id) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/list?academic_period_id=${data.academic_period_id}&examination_id=${data.examination_id}&keyword=${id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  examCentreOverView(data) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/${data.id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  examCentreUpdate(data) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/centre/update`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  examCentreAdd(data) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/centre/store`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  ownerShipList() {
    return this.httpClient
      .get(`${this.baseUrl}/dropdown/get-ownershiplist`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  statusList() {
    return this.httpClient
      .get(`${this.baseUrl}/component/status-dropdown`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  centreDelete(data: any) {
    return this.httpClient
      .delete(`${this.baseUrl}/administration/examinations/centre/delete/${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** Adminstration examination centre rooms */
  centreRoomsList(data: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/room/list/${data.id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  centreRoomsView(data: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/room/view/${data.id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  centreRoomsUpdate(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/centre/room/update`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  centreRoomsAdd(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/centre/room/store`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  centreRoomsSerach(data: any, value: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/room/list/${data.id}?keyword=${value}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  centreRoomsDelete(data: any) {
    return this.httpClient
      .delete(`${this.baseUrl}/administration/examinations/centre/room/${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }
  /** Adminstration examination centre invigilators */

  invigilatorsList(data: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/invigilator/list/${data.id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  invigilatorsView(data: any) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/invigilator/view/${data.id}/${data.invigilator_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  invigilatorsAdd(data: any) {
    return this.httpClient
      .post(`${this.baseUrl}/administration/examinations/centre/invigilator/store`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  invigilatorsSearch(data: any, key?: any) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/invigilator/list/${data.id}?keyword=${key}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  invigilatorsDropDownSearch(key: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/invigilator/dropdown?keyword=${key}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  invigilatorsDelete(data: any) {
    return this.httpClient
      .delete(
        `${this.baseUrl}/administration/examinations/centre/${data.id}/invigilator/${data.invigilator_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** Adminstration examination centre subject **/
  subjectList(data: any, key?: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/subject/list/${data.examination_id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  subjectSerarch(data: any, key?: any) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/subject/list/${data.examination_id}?keyword=${key}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  subjectView(data: any) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/subject/view/${data.centerId}/${data.subjectId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** Adminstration examination centre student */
  studentList(data: any) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/student/list/${data.id}/${data.examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  studentView(data: any) {
    return this.httpClient
      .get(`${this.baseUrl}/administration/examinations/centre/student/view/${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  studenttSerarch(data: any, key?: any) {
    return this.httpClient
      .get(
        `${this.baseUrl}/administration/examinations/centre/student/list/${data.id}/${data.examination_id}?keyword=${key}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  Common APIs START **/
  /** ACADEMIC PERIOD DROPDOWN **/
  getOptionsAcademicPeriod(data) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.registration}/${urls.candidate}/dropdown?entities=${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** EXAM DROPDOWN  **/
  getOptionsExam(academic_id) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/dropdown/${academic_id}/examination`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** EXAM CENTER DROPDOWN  **/
  getOptionsExamCenter(examination_id) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.dropdown}/${examination_id}/${urls.examCentres}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getCertificateExamCenter(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/certificates/dropdown-examCentre?academic_period_id=${data.academic_period_id}&examination_id=${data.examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getOptionsExamCenterArea(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/area/${data.area_id}/exam/${data.examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  /** OPTION DROPDOWN  **/
  getOptionsOptions(examination_id) {
    return this.httpClient
      .get(`${this.baseUrl}/${urls.mark}s/${urls.component}/${urls.optionList}/${examination_id}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /**Reports Mark */
  getReportsOptionList() {
    return this.httpClient
      .get(`${this.baseUrl}/dropdown/option-list`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getMarkscomponentList(data) {
    return this.httpClient
      .get(`${this.baseUrl}/reports/marks/coursework-marks/${data}/component-dropdown`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getReportsMarksList(data) {
    return this.httpClient
      .get(
        `${this.baseUrl}/reports/forms/multiple-choice/list-reports?start=${data.start}&end=${data.end}&module=course`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  addMarksReport(data) {
    return this.httpClient
      .post(`${this.baseUrl}/reports/marks/coursework-marks/generate-report`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** Mark Import API  */
  getMarkImportCentreList(data: any) {
    return this.httpClient.get(`${this.baseUrl}/dropdown/get-examcentrelist/${data}`, this.setHeader());
  }

  getMarkImportComponentList(data: any) {
    return this.httpClient.get(
      `${this.baseUrl}/registration/candidate/dropdown/${data}/examination-components`,
      this.setHeader()
    );
  }

  importMarkList(data: any) {
    return this.httpClient.post(`${this.baseUrl}/multiple-choice/import-questions-response`, data, this.setHeader());
  }

  componentMarkList(data: any) {
    return this.httpClient.post(`${this.baseUrl}/marks/component/import`, data, this.setHeader());
  }
}
