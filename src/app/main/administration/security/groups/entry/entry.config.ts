import { IKdTabs } from '../../../../../shared/kdComponents/kdInterfaces';

const ROUTER_BASE_PATH: string = 'main/security/groups';

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'User Groups', routerPath: ROUTER_BASE_PATH + '/user-groups', isActive: true },
  { tabName: 'System Groups', routerPath: ROUTER_BASE_PATH + '/system-groups' }
];
