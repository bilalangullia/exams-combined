import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  IPageheaderConfig,
  IPageheaderApi,
  IBreadcrumbConfig
} from 'openemis-styleguide-lib';

import { IKdTabs } from '../../../../../shared/kdComponents/kdInterfaces';
import { TABS_LIST } from './entry.config';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security';

  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public tabsList: Array<IKdTabs> = TABS_LIST;
  public isTabActive: boolean = true;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public pageEvent: KdPageBaseEvent) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj) => {
      this.breadcrumbList = breadcrumbObj;
    });
  }

  ngOnInit() {
    this.tabsList[0]['isActive'] = true;
    super.updateBreadcrumb();
    super.updatePageHeader();
  }

  selectedTab(event) {
    // console.log('EntryComponent -> selectedTab -> event', event);
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
