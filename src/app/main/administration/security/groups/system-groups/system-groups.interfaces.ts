export interface IFiltersSystemGroups {
  academic_period?: { key: number; value: string };
  examination?: { key: number; value: string };
}
