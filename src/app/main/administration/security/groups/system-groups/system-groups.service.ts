import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { GroupsDataService } from '../groups-data.service';
import { IFiltersSystemGroups } from './system-groups.interfaces';

@Injectable()
export class SystemGroupsService {
  private optionsAcademicPeriod: Array<any> = [];
  private optionsExamination: Array<any> = [];
  private filters: IFiltersSystemGroups = null;
  private list: Array<any> = [];
  private systemGroupId: string = null;
  private details: any = null;

  public optionsAcademicPeriodChanged = new BehaviorSubject<Array<any>>(this.optionsAcademicPeriod);
  public optionsExaminationChanged = new BehaviorSubject<Array<any>>(this.optionsExamination);
  public filtersChanged = new BehaviorSubject<IFiltersSystemGroups>(this.filters);
  public listChanged = new BehaviorSubject<Array<any>>(this.list);
  public systemGroupIdChanged = new BehaviorSubject<string>(this.systemGroupId);
  public detailsChanged = new BehaviorSubject<any>(this.details);

  constructor(private groupsDataService: GroupsDataService, private sharedService: SharedService) {}

  getOptionsAcademicPeriod() {
    this.groupsDataService.getOptionsAcademicPeriod().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']['academic_period_id']) {
          this.optionsAcademicPeriod = res['data']['academic_period_id'];
          this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
        } else {
          this.optionsAcademicPeriod = [];
          this.optionsAcademicPeriodChanged.next([]);
        }
      },
      (err) => {
        this.optionsAcademicPeriod = [];
        this.optionsAcademicPeriodChanged.next([]);
      }
    );
  }

  getOptionsExamination(academicPeriodId: number) {
    this.groupsDataService.getOptionsExamination(academicPeriodId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']) {
          this.optionsExamination = res['data'];
          this.optionsExaminationChanged.next([...this.optionsExamination]);
        } else {
          this.optionsExamination = [];
          this.optionsExaminationChanged.next([]);
        }
      },
      (err) => {
        this.optionsExamination = [];
        this.optionsExaminationChanged.next([]);
      }
    );
  }

  setFilters(filters: IFiltersSystemGroups) {
    this.filters = { ...this.filters, ...filters };
    this.filtersChanged.next({ ...this.filters });
  }

  resetFilters() {
    this.filters = null;
    this.filtersChanged.next(null);
  }

  getTotalRecords(academicPeriodId: number, examinationId: number, start?: number, end?: number) {
    return this.groupsDataService.getSystemGroupsList(academicPeriodId, examinationId, start, end);
  }

  getList(academicPeriodId: number, examinationId: number, start?: number, end?: number) {
    return this.groupsDataService.getSystemGroupsList(academicPeriodId, examinationId, start, end);
  }

  searchList(keyword: string) {
    this.groupsDataService.searchSystemGroupsList(keyword).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']['candidateRecords']) {
          this.list = res['data']['candidateRecords'];
          this.listChanged.next([...this.list]);
        } else {
          this.list = [];
          this.listChanged.next([]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([]);
      }
    );
  }

  resetList() {
    this.list = [];
    this.listChanged.next([]);
  }

  setSystemGroupId(systemGroupId: string) {
    this.systemGroupId = systemGroupId;
    this.systemGroupIdChanged.next(this.systemGroupId);
  }

  resetSystemGroupId() {
    this.systemGroupId = null;
    this.systemGroupIdChanged.next(null);
  }

  getDetails(systemGroupId: string) {
    this.groupsDataService.getSystemGroupDetails(systemGroupId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.details = res['data'][0];
          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = [];
          this.detailsChanged.next(null);
        }
      },
      (err) => {
        this.details = [];
        this.detailsChanged.next(null);
      }
    );
  }

  resetDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }
}
