const TABLE_COLUMNS: Array<any> = [
  { headerName: 'OpenEMIS ID', field: 'open_emis_id', visible: true, filterable: true, filterValue: [] },
  { headerName: 'Name', field: 'user_name', visible: true, filterable: true, filterValue: [] },
  { headerName: 'Role', field: 'user_role', visible: true, filterable: true, filterValue: [] }
];

export const QUESTION_BASE: Array<any> = [
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string' },
  {
    key: 'users',
    label: 'Users',
    controlType: 'table',
    column: [...TABLE_COLUMNS],
    row: [],
    config: {
      id: 'usersTable',
      rowIdKey: 'id',
      loadType: 'normal',
      gridHeight: 250
    }
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    type: 'string'
  }
];
