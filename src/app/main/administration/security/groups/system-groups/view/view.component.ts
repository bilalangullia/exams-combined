import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { SystemGroupsService } from '../system-groups.service';
import { QUESTION_BASE } from './view.config';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - System Groups';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public questionBase: Array<any> = QUESTION_BASE;

  /* Subscriptions */
  private systemGroupIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private systemGroupsService: SystemGroupsService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([{ type: 'back', path: 'main/security/groups/system-groups' }]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;

    this.showForm = true;

    this.systemGroupIdSub = this.systemGroupsService.systemGroupIdChanged.subscribe((systemGroupId: string) => {
      if (!systemGroupId) {
        this.router.navigate(['main/security/groups/system-groups']);
      } else {
        this.systemGroupsService.getDetails(systemGroupId);

        this.detailsSub = this.systemGroupsService.detailsChanged.subscribe((details: any) => {
          if (details) {
            this.setDetails(details);
          }
        });
      }
    });
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      this.api.setProperty(
        question['key'],
        'value',
        details[question['key']]
          ? details[question['key']]['value']
            ? details[question['key']['value']]
            : details[question['key']]
          : ''
      );
    });
  }

  ngOnDestroy() {
    if (this.systemGroupIdSub) {
      this.systemGroupIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe;
    }
    this.systemGroupsService.resetDetails();
    this.systemGroupsService.resetSystemGroupId();
    super.destroyPageBaseSub();
  }
}
