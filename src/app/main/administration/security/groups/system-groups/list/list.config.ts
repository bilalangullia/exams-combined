import { ITableColumn } from 'openemis-styleguide-lib';

interface IColumnList {
  Name?: ITableColumn;
  NoOfUsers?: ITableColumn;
}

export const GRID_ID: string = 'systemGroupsList';
export const PAGE_SIZE: number = 20;
export const TOTAL_ROWS: number = 800;

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  }
];

export const TABLE_COLUMN_LIST: IColumnList = {
  Name: { headerName: 'Name', field: 'name', sortable: true, filterable: true, filterValue: [] },
  NoOfUsers: { headerName: 'No. of Users', field: 'noOfUser', sortable: true, filterable: false }
};
