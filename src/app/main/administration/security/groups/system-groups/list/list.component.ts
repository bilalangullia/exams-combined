import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdFilter,
  ITableApi,
  ITableConfig,
  ITableColumn,
  KdTableEvent,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { SystemGroupsService } from '../system-groups.service';
import { IFiltersSystemGroups } from '../system-groups.interfaces';
import { GRID_ID, PAGE_SIZE, TOTAL_ROWS, TABLE_COLUMN_LIST, FILTER_INPUTS } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - System Groups';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public showTable: boolean = false;
  public filterInputs = FILTER_INPUTS;
  public tableApi: ITableApi = {};

  public tableConfig: ITableConfig = {
    id: GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTAL_ROWS },
    action: {
      enabled: true,
      list: [
        {
          name: 'View',
          icon: 'fa fa-eye',
          custom: true,
          callback: (params: any): void => {
            this.systemGroupsService.setSystemGroupId(params['data']['system_group_id']);
            setTimeout(() => {
              this.router.navigate(['main/security/groups/system-groups/view']);
            }, 0);
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [TABLE_COLUMN_LIST.Name, TABLE_COLUMN_LIST.NoOfUsers];
  public tableRows: Array<any> = null;

  private currentPage: number = null;
  private currentFilters: IFiltersSystemGroups = {};

  @ViewChild('filters') filters: KdFilter;

  /* Subscriptions */
  private academicPeriodOptionsSub: Subscription;
  private examinationOtionsSub: Subscription;
  private filtersSub: Subscription;
  private listSub: Subscription;
  private tableEventSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public tableEvent: KdTableEvent,
    private systemGroupsService: SystemGroupsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.systemGroupsService.getOptionsAcademicPeriod();

    this.academicPeriodOptionsSub = this.systemGroupsService.optionsAcademicPeriodChanged.subscribe(
      (options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('academic_period', options);
        } else {
          this.setDropdownOptions('academic_period', []);
        }
      }
    );

    this.examinationOtionsSub = this.systemGroupsService.optionsExaminationChanged.subscribe((options: Array<any>) => {
      if (options && options.length) {
        this.setDropdownOptions('examination', options);
      } else {
        this.setDropdownOptions('examination', []);
      }
    });

    this.filtersSub = this.systemGroupsService.filtersChanged.subscribe((filters: IFiltersSystemGroups) => {
      if (
        filters &&
        filters['academic_period'] &&
        filters['academic_period']['key'] &&
        filters['examination'] &&
        filters['examination']['key']
      ) {
        this.currentFilters = { ...filters };

        this.systemGroupsService
          .getTotalRecords(filters['academic_period']['key'], filters['examination']['key'], 0, 1)
          .subscribe(
            (res: any) => {
              if (res && res['data'] && res['data']['total']) {
                this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
                this.showTable = true;
              } else {
                this.tableConfig.paginationConfig.total = 0;
                this.showTable = false;
              }
            },
            (err) => {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          );
      }
    });

    setTimeout(() => {
      this.tableEventSub = this.tableEvent.onKdTableEventList(GRID_ID).subscribe((event: any): void => {
        console.log(event);
        if (event instanceof KdTableDatasourceEvent) {
          if (
            /* event['rowParams']['startRow'] */ /* && */ event['rowParams']['endRow'] &&
            !event['rowParams']['sortModel'].length
          ) {
            this.currentPage = event['rowParams']['endRow'] / PAGE_SIZE;
            console.log(this.currentPage);
            this.fetchList(event);
          } else if (event['rowParams']['sortModel'].length) {
            console.log(event['rowParams']['sortModel']);
            this.fetchList(event);
          } else if (event['rowParams']['filterModel'].length) {
            console.log(event['rowParams']['filterModel']);
            this.fetchList(event);
          }
        }
      });
    }, 500);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      options = data.map((item) => ({ key: item.id, value: item.name }));
      this.filters.setInputProperty(key, 'options', options);
    } else {
      options.unshift({ key: null, value: '-- Select --' });
      this.filters.setInputProperty(key, 'options', options);
    }
    setTimeout(() => {
      this.filters.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  detectValue(event) {
    if (event['key'] === 'academic_period') {
      this.systemGroupsService.getOptionsExamination(event['value']);
    }

    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj) {
      let filter: IFiltersSystemGroups = {};
      filter[event['key']] = { ...filterObj };
      this.systemGroupsService.setFilters(filter);
    }
  }

  fetchList(event) {
    /* rowParams: { startRow, endRow, filterModel[], groupKeys[], sortModel[] } */
    console.log(event);
    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    this.systemGroupsService
      .getList(
        this.currentFilters['academic_period']['key'],
        this.currentFilters['examination']['key'],
        event.rowParams.startRow,
        event.rowParams.endRow
      )
      .toPromise()
      .then(
        (res: any) => {
          if (res && res['data'] && res['data']['candidateRecords']) {
            list = res['data']['candidateRecords'];
            total = res['data']['total'];
            this.tableColumns.forEach((column, colIndex) => {
              let filterValues: Array<any> = [];
              list.forEach((item) => {
                if (item[column['field']] && !filterValues.includes(item[column['field']])) {
                  filterValues.push(item[column['field']]);
                }
              });
              column['filterValue'] = filterValues;
            });
          } else {
            list = [];
            total = 0;
          }
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
        },
        (err) => {
          list = [];
          total = 0;
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
        }
      );
  }

  populateTable(list: Array<any>) {
    this.tableRows = list;
    this.tableColumns.forEach((column, colIndex) => {
      if (column['filterable']) {
        let filterValues: Array<any> = [];
        list.forEach((item) => {
          if (item[column['field']] && !filterValues.includes(item[column['field']])) {
            filterValues.push(item[column['field']]);
          }
        });
        this.tableColumns[colIndex]['filterValue'] = filterValues;
      }
    });
    return list;
  }

  searchList(keyword: string) {
    if (keyword.length && keyword.length > 2) {
      this.systemGroupsService.searchList(keyword);
    } else {
      this.systemGroupsService.searchList(keyword);
    }
  }

  ngOnDestroy() {
    if (this.academicPeriodOptionsSub) {
      this.academicPeriodOptionsSub.unsubscribe();
    }
    if (this.examinationOtionsSub) {
      this.examinationOtionsSub.unsubscribe();
    }
    if (this.filtersSub) {
      this.filtersSub.unsubscribe();
    }
    if (this.listSub) {
      this.listSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    this.systemGroupsService.resetList();
    this.systemGroupsService.resetFilters();

    super.destroyPageBaseSub();
  }
}
