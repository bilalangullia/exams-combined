import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { SystemGroupsRoutingModule } from './system-groups-routing.module';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { SystemGroupsService } from './system-groups.service';

@NgModule({
  imports: [CommonModule, SystemGroupsRoutingModule, SharedModule],
  declarations: [ListComponent, ViewComponent],
  providers: [SystemGroupsService]
})
export class SystemGroupsModule {}
