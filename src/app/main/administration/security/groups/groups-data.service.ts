import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class GroupsDataService {
  constructor(private http: HttpClient, private router: Router) {}

  private setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      // 'Content-Type': 'application/json'
      return { headers: headers };
    }
  }

  private handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
      // case 0:
      //   console.log(0);
      //   break;
    }
    console.log(error);
    return throwError(error);
  };

  /********** User Group APIs START **********/

  /********** User Groups | List - Academic Period Dropdown Options **********/
  getOptionsAcademicPeriod() {
    // http://openemis.n2.iworklab.com/api/registration/candidate/dropdown?entities=academicPeriods
    return this.http
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}?entities=academicPeriods`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | List - Examination Dropdown Options **********/
  getOptionsExamination(academicPeriodId: number) {
    // http://openemis.n2.iworklab.com/api/registration/candidate/dropdown/4/examination
    return this.http
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${academicPeriodId}/${urls.examination}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | List - Listing **********/
  getUserGroupsList(start: number = 0, end: number = 20) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-groups/user-group-list?start=0&end=10&keyword=dis
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.user}-group-${urls.list}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | List - Search List **********/
  searchUserGroupsList(keyword: string, start: number = 0, end: number = 20) {
    // http://openemis.n2.iworklab.com/api/administration/security/groups/user-group-list?start=0&end=10&keyword=super
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.user}-group-${urls.list}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | Add - Areas Education **********/
  getAreasEducation(keyword: string) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-groups/autocomplete-area/nam
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.autocomplete}-${urls.area}/${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | Add - Institutions **********/
  getInstitutions(keyword: string) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-groups/autocomplete-institution/A1
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.autocomplete}-${urls.institution}/${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | Add - OpenEMIS ID **********/
  getUsersByOpenEMISId(keyword: string) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-groups/autocomplete-security-users/152227196
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.autocomplete}-${urls.security}-${urls.users}/${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | Add - Save **********/
  addUserGroup(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-groups/add-user-group
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.add}-${urls.user}-${urls.group}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | View - Details **********/
  getUserGroupDetails(userGroupId: string) {
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.user}-group-${urls.details}/${userGroupId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | Edit - Roles Dropdown Options **********/
  getRolesDropdownOptions() {
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.role}-${urls.dropdown}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | Edit - Update Details **********/
  updateUserGroupDetails(userGroupId: string, payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-groups/update-user-group/1004
    return this.http /* .post */
      .put(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.update}-${urls.user}-${urls.group}/${userGroupId}`,
        payload,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Groups | Delete **********/
  deleteUserGroup(userGroupId: string) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-groups/update-user-group/1004
    return this.http
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.groups}/${urls.delete}-${urls.user}-${urls.group}/${userGroupId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** User Group APIs END **********/
  /*********************************************/
  /********** SYSTEM GROUP APIS START **********/

  /********** System Groups | List | Listing	 **********/
  getSystemGroupsList(academicPeriodId: number, examinationId: number, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/security/system-groups/system-group-list?start=0&end=5&examination_id=3&academic_period_id=4
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.system}-${urls.groups}/${urls.system}-group-${urls.list}?academic_period_id=${academicPeriodId}&examination_id=${examinationId}&start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** System Groups | List | Search  **********/
  searchSystemGroupsList(keyword: string, start: number = 0, end: number = 20) {
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.system}-${urls.groups}/${urls.system}-group-${urls.list}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********* System Groups | View | Details **********/
  getSystemGroupDetails(systemGroupId: string) {
    return this.http.get(
      `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.system}-${urls.groups}/${urls.system}-group-${urls.details}/${systemGroupId}`
    );
  }

  /********** SYSTEM GROUP APIS END **********/
}
