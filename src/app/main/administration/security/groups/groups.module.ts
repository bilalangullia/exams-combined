import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { GroupsRoutingModule } from './groups-routing.module';
import { GroupsDataService } from './groups-data.service';
import { EntryComponent } from './entry/entry.component';

@NgModule({
  imports: [CommonModule, GroupsRoutingModule, SharedModule],
  declarations: [EntryComponent],
  providers: [GroupsDataService]
})
export class GroupsModule {}
