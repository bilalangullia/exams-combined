const TABLE_COLUMNS_LIST: any = {
  feature: {
    headerName: 'Feature',
    field: 'feature',
    visible: true,
    controlType: 'text',
    type: 'string',
    filterable: true,
    filterValue: []
  },
  noOfRecords: {
    headerName: 'No. of Records',
    field: 'no_of_record',
    visible: true,
    controlType: 'text',
    type: 'string',
    filterable: true,
    filterValue: []
  }
};

export const QUESTION_BASE: Array<any> = [
  { key: 'to_be_deleted', label: 'To Be Deleted', visible: true, controlType: 'text', type: 'string', readonly: true },
  {
    key: 'associated_records',
    label: 'Associated Records',
    visible: true,
    controlType: 'table',
    row: [],
    column: [TABLE_COLUMNS_LIST.feature, TABLE_COLUMNS_LIST.noOfRecords],
    config: { id: 'recordsTable', rowIdKey: 'id', loadType: 'normal', gridHeight: 50 }
  }
];
