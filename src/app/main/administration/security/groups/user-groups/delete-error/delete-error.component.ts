import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { UserGroupsService } from '../user-groups.service';
import { QUESTION_BASE } from './delete-error.config';
import { timer } from 'rxjs';
import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';

@Component({
  selector: 'app-delete-error',
  templateUrl: './delete-error.component.html',
  styleUrls: ['./delete-error.component.css']
})
export class DeleteErrorComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Groups';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public questionBase: Array<any> = QUESTION_BASE;
  public api: IDynamicFormApi = {};
  public showForm: boolean = false;

  private details: any = null;
  private errorMessage: IToasterConfig = {
    title: 'Delete Failed',
    body: 'Delete operation is not allowed as there are other information linked to this record.',
    type: 'error',
    timeout: 5000,
    tapToDismiss: true
  };

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private sharedService: SharedService,
    private userGroupsService: UserGroupsService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([{ type: 'back', path: 'main/security/groups/user-groups' }]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.userGroupsService.deleteErrorChanged.subscribe((errorObj: any) => {
      if (errorObj && errorObj['associated_records'] && errorObj['to_be_deleted']) {
        this.sharedService.setToaster(this.errorMessage);
        this.details = errorObj;

        this.setDetails(errorObj);
      }
    });
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      if (question['key'] === 'associated_records') {
        question['row'] = details[question['key']];
      } else {
        question['value'] = details[question['key']];
      }
    });

    timer(100).subscribe(() => {
      this.showForm = true;
    });
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
