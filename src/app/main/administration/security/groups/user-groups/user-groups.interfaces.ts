export interface IAddPayload {
  name: string;
  area?: any;
  institution?: any;
  user?: any;
}

export interface IUpdatePayload {
  name: string;
  area?: any;
  institution?: any;
  user?: any;
}
