import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { UserGroupsRoutingModule } from './user-groups-routing.module';
import { UserGroupsService } from './user-groups.service';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { DeleteErrorComponent } from './delete-error/delete-error.component';

@NgModule({
  imports: [CommonModule, UserGroupsRoutingModule, SharedModule],
  declarations: [ListComponent, AddComponent, ViewComponent, EditComponent, DeleteErrorComponent],
  providers: [UserGroupsService]
})
export class UserGroupsModule {}
