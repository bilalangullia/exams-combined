import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscriber, timer, Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi,
  ITableApi,
  ITableFormUpdateParams
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { IAddPayload } from '../user-groups.interfaces';
import { UserGroupsService } from '../user-groups.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Groups';

  public loading: boolean = true;
  public showForm: boolean = false;
  public questionBase: Array<any> = QUESTION_BASE;
  public formButtons: Array<any> = FORM_BUTTONS;
  public api: IDynamicFormApi = {};
  public tableApi: ITableApi = {};

  private roleOptions: Array<any> = [];
  private areasList: Array<any> = [];
  private institutionsList: Array<any> = [];
  private usersList: Array<any> = [];

  /* Subscription */
  private rolesOptionsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private userGroupsService: UserGroupsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;
    this.userGroupsService.getRolesDropdownOptions();

    this.rolesOptionsSub = this.userGroupsService.rolesOptionsChanged.subscribe((options: Array<any>) => {
      if (options && options.length) {
        this.roleOptions = options;
        this.setOptions(options);
      }
    });
  }

  setOptions(options?: any) {
    this.questionBase.forEach((question: any) => {
      if (question['key'] === 'users') {
        let roleOptions: Array<any> = [];
        roleOptions = options.map((item) => ({ key: item['id'], value: item['name'] }));
        roleOptions.unshift({ key: null, value: '-- Please Select --' });
        question['column'].find((item) => item.field === 'role')['config']['input']['options'] = [...roleOptions];
        question['config']['action']['list'][0]['callback'] = this.deleteRow.bind(this);
      } else if (question['key'] === 'areasTable') {
        question['config']['action']['list'][0]['callback'] = this.deleteRow.bind(this);
      } else if (question['key'] === 'institutionsTable') {
        question['config']['action']['list'][0]['callback'] = this.deleteRow.bind(this);
      } else if (question['key'] === 'area_education') {
        question['dropdownCallback'] = this.searchArea.bind(this);
      } else if (question['key'] === 'institution') {
        question['dropdownCallback'] = this.searchInstitution.bind(this);
      } else if (question['key'] === 'user') {
        question['dropdownCallback'] = this.searchOpenEmisId.bind(this);
      }
    });

    setTimeout(() => {
      this.showForm = true;
    }, 1000);
  }

  searchArea(params: any): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data: Array<any> = [];
      // timer(500).subscribe((): void => {
      if (params['query'].length > 2) {
        this.userGroupsService.getAreasEducation(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'].map((item) => ({ ...item, key: item['area_id'], value: item['area'] }));
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            _observer.next([]);
            _observer.complete();
            console.log(err);
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
    // });
  }

  searchInstitution(params: string) {
    return new Observable((_observer: Subscriber<any>): any => {
      let data: Array<any> = [];
      // timer(500).subscribe((): void => {
      if (params['query'].length) {
        this.userGroupsService.getInstitutions(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'].map((item) => ({ ...item, key: item['institution_id'], value: item['institution'] }));
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            _observer.next([]);
            _observer.complete();
            console.log(err);
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
    // });
  }

  searchOpenEmisId(params: string): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data = [];
      // timer(2000).subscribe((): void => {
      if (params['query'].length > 2) {
        this.userGroupsService.getUsersByOpenEmisId(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'].map((item) => ({
                ...item,
                key: item['openemis_id'],
                value: item['name'],
                id: item['user_id'],
                role: null
              }));
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
    // });
  }

  deleteRow(params: any) {
    if (params['data']['area_id']) {
      let deleteIndex: number = this.areasList.findIndex((item) => item['area_id'] === params['data']['area_id']);
      this.areasList.splice(deleteIndex, 1);
      this.api.setProperty('areasTable', 'row', [...this.areasList]);
    } else if (params['data']['institution_id']) {
      let deleteIndex: number = this.institutionsList.findIndex(
        (item) => item['institution_id'] === params['data']['institution_id']
      );
      this.institutionsList.splice(deleteIndex, 1);
      this.api.setProperty('institutionsTable', 'row', [...this.institutionsList]);
    } else if (params['data']['user_id']) {
      let deleteIndex: number = this.usersList.findIndex((item) => item['user_id'] === params['data']['user_id']);
      this.usersList.splice(deleteIndex, 1);
      this.api.setProperty('users', 'row', [...this.usersList]);
    }
  }

  detectValue(event) {
    if (event['key'] === 'area_education' && event['value'] && event['value']['value']) {
      if (this.areasList.findIndex((item) => item['area_id'] === event['value']['area_id']) === -1) {
        this.areasList.push({
          area_id: event['value']['area_id'],
          area_code: event['value']['code'],
          area_level: event['value']['level'],
          area_name: event['value']['area']
        });
      }
      setTimeout(() => {
        this.api.setProperty('areasTable', 'row', [...this.areasList]);
      }, 100);
    } else if (event['key'] === 'institution' && event['value'] && event['value']['value']) {
      if (
        this.institutionsList.findIndex((item) => item['institution_id'] === event['value']['institution_id']) === -1
      ) {
        this.institutionsList.push({
          institution_code: event['value']['code'],
          institution_name: event['value']['institution'],
          institution_id: event['value']['institution_id']
        });
      }
      this.api.setProperty('institutionsTable', 'row', [...this.institutionsList]);
    } else if (event['key'] === 'user' && event['value'] && event['value']['value']) {
      if (this.usersList.findIndex((item) => item['user_id'] === event['value']['user_id']) === -1) {
        let userObj = {
          id: event['value']['user_id'],
          user_id: event['value']['user_id'],
          openemis_id: event['value']['openemis_id'],
          name: event['value']['name']
        };
        this.usersList.push(userObj);
        this.api.table('users').addRow([userObj]);
        let roleCell: ITableFormUpdateParams = {
          questionKey: 'role',
          colId: 'role',
          rowId: event['value']['user_id'],
          property: 'value',
          data: event['value']['role'] ? event['value']['role'] : null
        };
        this.api.table('users').setTableCellInputProperty(roleCell);
      }
    }
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question: any) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 0);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      let errorMsg: IToasterConfig = {
        title: 'Missing required fields!',
        type: 'error',
        timeout: 1000
      };
      this.sharedService.setToaster(errorMsg);
    } else {
      let payload: IAddPayload = {
        name: form['name'],
        area: this.areasList && this.areasList.length ? this.areasList : null,
        institution: this.institutionsList && this.institutionsList.length ? this.institutionsList : null,
        user:
          this.usersList && this.usersList.length
            ? [...this.usersList.map((item) => ({ ...item, role: +item['role']['role'] }))]
            : null
      };

      // return;
      this.userGroupsService.addUserGroup(payload).subscribe(
        (res: any) => {
          if (res && res['data']) {
            let successMsg: IToasterConfig = {
              title: 'User Group Added',
              type: 'success',
              timeout: 1000
            };
            this.sharedService.setToaster(successMsg);
            timer(500).subscribe(() => {
              this.router.navigate(['main/security/groups/user-groups']);
            });
          } else {
            let errorMsg: IToasterConfig = {
              title: 'Error while saving.',
              type: 'error',
              timeout: 1000
            };
            this.sharedService.setToaster(errorMsg);
          }
        },
        (err) => {
          let error: IToasterConfig = {
            title: 'An Error Occurred',
            type: 'error',
            timeout: 1000
          };
          this.sharedService.setToaster(error);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/security/groups/user-groups']);
  }

  ngOnDestroy() {
    if (this.rolesOptionsSub) {
      this.rolesOptionsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
