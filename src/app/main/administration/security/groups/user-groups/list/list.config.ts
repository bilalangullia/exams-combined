import { ITableColumn } from 'openemis-styleguide-lib';

interface IColumnList {
  Name?: ITableColumn;
  NoOfUsers?: ITableColumn;
}

export const GRID_ID: string = 'userGroupsList';
export const PAGE_SIZE: number = 20;
export const TOTAL_ROWS: number = 1000;

export const TABLE_COLUMN_LIST: IColumnList = {
  Name: { headerName: 'Name', field: 'name', sortable: true, filterable: true, filterValue: [] },
  NoOfUsers: { headerName: 'No. of Users', field: 'noOfUser', sortable: true, filterable: true, filterValue: [] }
};
