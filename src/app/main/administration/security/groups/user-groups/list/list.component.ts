import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBaseEvent, KdPageBase, ITableConfig, ITableApi } from 'openemis-styleguide-lib';

import { UserGroupsService } from '../user-groups.service';
import { GRID_ID, PAGE_SIZE, TOTAL_ROWS, TABLE_COLUMN_LIST } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Groups';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public showTable: boolean = false;
  public tableApi: ITableApi = {};
  public tableConfig: ITableConfig = {
    id: GRID_ID,
    loadType: 'oneshot',
    gridHeight: 500,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTAL_ROWS },
    action: {
      enabled: true,
      list: [
        {
          name: 'View',
          icon: 'fa fa-eye',
          custom: true,
          callback: (params: any) => {
            this.userGroupsService.setUserGroupId(params['data']['user_group_id']);
            this.router.navigate(['main/security/groups/user-groups/view']);
          }
        }
      ]
    }
  };
  public tableColumns = [TABLE_COLUMN_LIST.Name, TABLE_COLUMN_LIST.NoOfUsers];
  public tableRows: Array<any> = [];

  /* Subscriptions */
  private listSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private userGroupsService: UserGroupsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/security/groups/user-groups/add' }]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.userGroupsService.getList();

    this.listSub = this.userGroupsService.listChanged.subscribe((list: Array<any>) => {
      if (list.length) {
        this.populateTable(list);
      } else {
        this.showTable = false;
      }
    });
  }

  populateTable(list: Array<any>) {
    this.tableRows = list;

    this.tableColumns.forEach((column, colIndex) => {
      if (column['filterable']) {
        let filterValues: Array<any> = [];
        list.forEach((item) => {
          if (item[column['field']] && !filterValues.includes(item[column['field']])) {
            filterValues.push(item[column['field']]);
          }
        });
        this.tableColumns[colIndex]['filterValue'] = filterValues;
      }
    });
    setTimeout(() => {
      this.showTable = true;
    }, 500);
  }

  searchList(keyword: string) {
    if (keyword.length) {
      if (keyword.length > 2) {
        this.userGroupsService.searchList(keyword);
      }
    } else {
      this.userGroupsService.searchList(keyword);
    }
  }

  ngOnDestroy() {
    if (this.listSub) {
      this.listSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
