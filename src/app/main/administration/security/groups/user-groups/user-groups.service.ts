import { Injectable } from '@angular/core';

import { GroupsDataService } from '../groups-data.service';
import { BehaviorSubject } from 'rxjs';
import { SharedService } from '../../../../../shared/shared.service';

@Injectable()
export class UserGroupsService {
  private list: Array<any> = [];
  private userGroupId: string = null;
  private details: any = null;
  private rolesOptions: Array<any> = [];
  private deleteError: any = null;

  public listChanged = new BehaviorSubject<Array<any>>([]);
  public userGroupdIdChanged = new BehaviorSubject<string>(null);
  public detailsChanged = new BehaviorSubject<any>(null);
  public rolesOptionsChanged = new BehaviorSubject<Array<any>>([]);
  public deleteErrorChanged = new BehaviorSubject<any>(null);

  constructor(private groupsDataService: GroupsDataService, private sharedService: SharedService) {}

  getList() {
    this.groupsDataService.getUserGroupsList().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']['candidateRecords'] && res['data']['candidateRecords'].length) {
          this.list = res['data']['candidateRecords'];
          this.listChanged.next([...this.list]);
        } else {
          this.list = res['data'];
          this.listChanged.next([]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([]);
      }
    );
  }

  searchList(keyword: string) {
    this.groupsDataService.searchUserGroupsList(keyword).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']['candidateRecords'] && res['data']['candidateRecords'].length) {
          this.list = res['data']['candidateRecords'];
          this.listChanged.next([...this.list]);
        } else {
          this.list = res['data'];
          this.listChanged.next([]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([]);
      }
    );
  }

  resetList() {
    this.list = [];
    this.listChanged.next([]);
  }

  getAreasEducation(keyword: string) {
    return this.groupsDataService.getAreasEducation(keyword);
  }

  getInstitutions(keyword: string) {
    return this.groupsDataService.getInstitutions(keyword);
  }

  getUsersByOpenEmisId(keyword: string) {
    return this.groupsDataService.getUsersByOpenEMISId(keyword);
  }

  addUserGroup(payload: any) {
    return this.groupsDataService.addUserGroup(payload);
  }

  setUserGroupId(userGroupId: string) {
    this.userGroupId = userGroupId;
    this.userGroupdIdChanged.next(this.userGroupId);
  }

  resetUserGroupdId() {
    this.userGroupId = null;
    this.userGroupdIdChanged.next(null);
  }

  getDetails(userGroupId: string) {
    this.groupsDataService.getUserGroupDetails(userGroupId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.details = res['data'][0];
          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = null;
          this.detailsChanged.next(null);
        }
      },
      (err) => {
        this.details = null;
        this.detailsChanged.next(null);
      }
    );
  }

  resetDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  getRolesDropdownOptions() {
    this.groupsDataService.getRolesDropdownOptions().subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.rolesOptions = res['data'];
          this.rolesOptionsChanged.next(this.rolesOptions);
        } else {
          this.rolesOptions = [];
          this.rolesOptionsChanged.next([]);
        }
      },
      (err) => {
        this.rolesOptions = [];
        this.rolesOptionsChanged.next([]);
      }
    );
  }

  updateGroupDetails(userGroupId: string, payload: any) {
    return this.groupsDataService.updateUserGroupDetails(userGroupId, payload);
  }

  deleteUserGroup(userGroupId: string) {
    return this.groupsDataService.deleteUserGroup(userGroupId);
  }

  setDeleteError(errorObj: any) {
    this.deleteError = { ...errorObj };
    this.deleteErrorChanged.next(this.deleteError);
  }

  resetDeleteError() {
    this.deleteError = null;
    this.deleteErrorChanged.next(null);
  }
}
