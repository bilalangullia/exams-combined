import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent } from 'openemis-styleguide-lib';

import { IToasterConfig, IModalConfig } from '../../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { UserGroupsService } from '../user-groups.service';
import { QUESTION_BASE } from './view.config';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Groups';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public questionBase: Array<any> = QUESTION_BASE;
  public modalConfig: IModalConfig = {
    title: 'Delete User Group',
    body: 'Are you sure you want to delete?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.deleteUserGroup();
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  private userGroupId: string = null;
  private roleOptions: Array<any> = [];

  /* Subscriptions */
  private userGroupIdSub: Subscription;
  private roleOptionsSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private sharedService: SharedService,
    private modalEvent: KdModalEvent,
    private userGroupsService: UserGroupsService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/security/groups/user-groups' },
      { type: 'edit', path: 'main/security/groups/user-groups/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.modalEvent.toggleOpen();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;
    this.showForm = true;

    this.userGroupIdSub = this.userGroupsService.userGroupdIdChanged.subscribe((userGroupId: string) => {
      if (!userGroupId) {
        this.router.navigate(['main/security/groups/user-groups']);
      } else {
        this.userGroupId = userGroupId;

        this.roleOptionsSub = this.userGroupsService.rolesOptionsChanged.subscribe((options: any) => {
          if (options && options.length) {
            this.roleOptions = options;
          }
        });

        this.userGroupsService.getDetails(userGroupId);

        this.detailsSub = this.userGroupsService.detailsChanged.subscribe((details: any) => {
          if (details) {
            this.setDetails(details);
          }
        });
      }
    });
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      if (question['key'] === 'areaTable') {
        // question['row'] = details['areas'];
        this.api.setProperty(question['key'], 'row', details['areas']);
      } else if (question['key'] === 'institutionsTable') {
        // question['row'] = details['institutions'];
        this.api.setProperty(question['key'], 'row', details['institutions']);
      } else if (question['key'] === 'usersTable') {
        // question['row'] = details['users'];
        details['users'].forEach((user) => {
          let roleObj = this.roleOptions.find((role) => role['id'] === user['role']);
          user['role'] = roleObj ? roleObj['value'] : '';
        });
        this.api.setProperty(question['key'], 'row', details['users']);
      } else {
        this.api.setProperty(question['key'], 'value', details[question['key']]);
      }
    });
  }

  deleteUserGroup() {
    this.modalEvent.toggleClose();
    this.userGroupsService.deleteUserGroup(this.userGroupId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length && res['data'][0]) {
          let errorObj = res['data'][0]['error'];
          this.userGroupsService.setDeleteError({ ...errorObj });
          timer(250).subscribe(() => {
            this.router.navigate(['main/security/groups/user-groups/delete-error']);
          });
        } else if (res && res['data']) {
          let successMsg: IToasterConfig = {
            title: 'Deleted Successfully',
            type: 'success',
            timeout: 1000
          };
          this.sharedService.setToaster(successMsg);
          timer(500).subscribe(() => {
            this.router.navigate(['main/security/groups/user-groups']);
          });
        } else {
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  ngOnDestroy() {
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    if (this.userGroupIdSub) {
      this.userGroupIdSub.unsubscribe();
    }
    if (this.roleOptionsSub) {
      this.roleOptionsSub.unsubscribe();
    }
    this.userGroupsService.resetDetails();

    super.destroyPageBaseSub();
  }
}
