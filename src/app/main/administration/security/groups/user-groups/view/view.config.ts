import { ITableFormInput } from 'openemis-styleguide-lib';

const TABLE_COLUMNS_AREAS: Array<any> = [
  { headerName: 'Level', field: 'area_level', filterable: true, visible: true },
  { headerName: 'Code', field: 'area_code', filterable: true, visible: true },
  { headerName: 'Area', field: 'area_name', filterable: true, visible: true }
];
const TABLE_COLUMNS_INSTITUTIONS: Array<any> = [
  { headerName: 'Code', field: 'institution_code', filterable: true, visible: true },
  { headerName: 'Institution', field: 'institution_name', filterable: true, visible: true }
];
const TABLE_COLUMNS_USERS: Array<any> = [
  { headerName: 'OpenEMIS ID', field: 'openemis_id', filterable: true, visible: true },
  { headerName: 'Name', field: 'user_name', filterable: true, visible: true },
  { headerName: 'Role', field: 'role', filterable: true, visible: true }
];

export const QUESTION_BASE: Array</* ITableFormInput */ any> = [
  { key: 'user_group_name', label: 'Name', visible: true, controlType: 'text', type: 'string', value: 'Test Name' },
  {
    key: 'areaTable',
    label: 'Areas (Education)',
    controlType: 'table',
    gridHeight: 200,
    row: [],
    column: [...TABLE_COLUMNS_AREAS],
    config: { id: 'areasTable2', rowIdKey: 'id', loadType: 'normal', gridHeight: 50 }
  },
  {
    key: 'institutionsTable',
    label: 'Institutions',
    controlType: 'table',
    gridHeight: 200,
    row: [],
    column: [...TABLE_COLUMNS_INSTITUTIONS],
    config: { id: 'institutionsTable', rowIdKey: 'id', loadType: 'normal', gridHeight: 50 }
  },
  {
    key: 'usersTable',
    label: 'Users',
    visible: true,
    controlType: 'table',
    gridHeight: 200,
    row: [],
    column: [...TABLE_COLUMNS_USERS],
    config: { id: 'usersTale', rowIdKey: 'id', loadType: 'normal', gridHeight: 50 }
  },
  { key: 'modified_by', label: 'Modified By', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_on', label: 'Modified On', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_by', label: 'Created By', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_on', label: 'Created On', visible: true, controlType: 'text', type: 'string' }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
