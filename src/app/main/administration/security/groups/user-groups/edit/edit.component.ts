import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable, Subscriber, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, ITableFormUpdateParams } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { UserGroupsService } from '../user-groups.service';
import { IUpdatePayload } from '../user-groups.interfaces';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';
import { saveSuccess, saveFail, serverError } from '../../../../../../shared/shared.toasters';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Groups';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public questionBase = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  private userGroupId: string = null;
  private roleOptions: Array<any> = [];
  private areasList: Array<any> = [];
  private institutionsList: Array<any> = [];
  private usersList: Array<any> = [];

  /* Subscriptions */
  private userGroupsIdSub: Subscription;
  private rolesOptionsSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private userGroupsService: UserGroupsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setToolbarMainBtns([]);
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.userGroupsIdSub = this.userGroupsService.userGroupdIdChanged.subscribe((userGroupId: string) => {
      if (!userGroupId) {
        this.router.navigate(['main/security/groups/user-groups']);
      } else {
        this.userGroupId = userGroupId;
        this.userGroupsService.getRolesDropdownOptions();
        this.userGroupsService.getDetails(userGroupId);

        this.rolesOptionsSub = this.userGroupsService.rolesOptionsChanged.subscribe((options: Array<any>) => {
          if (options && options.length) {
            this.roleOptions = options;
            this.setOptions(options);

            timer(100).subscribe(() => {
              this.detailsSub = this.userGroupsService.detailsChanged.subscribe((details: any) => {
                if (details) {
                  this.setDetails(details);
                }
              });
            });
          }
        });
      }
    });
  }

  setOptions(options: Array<any>) {
    this.questionBase.forEach((question: any) => {
      if (question['key'] === 'users') {
        let roleOptions: Array<any> = [];
        roleOptions = options.map((item) => ({ key: item['id'], value: item['name'] }));
        roleOptions.unshift([{ key: null, value: '-- Please Select --' }]);
        question['column'].find((item) => item.field === 'role')['config']['input']['options'] = [...roleOptions];
        question['config']['action']['list'][0]['callback'] = this.deleteRow.bind(this);
      } else if (question['key'] === 'areasTable') {
        question['config']['action']['list'][0]['callback'] = this.deleteRow.bind(this);
      } else if (question['key'] === 'institutionsTable') {
        question['config']['action']['list'][0]['callback'] = this.deleteRow.bind(this);
      } else if (question['key'] === 'area_education') {
        question['dropdownCallback'] = this.searchArea.bind(this);
      } else if (question['key'] === 'institution') {
        question['dropdownCallback'] = this.searchInstitution.bind(this);
      } else if (question['key'] === 'user') {
        question['dropdownCallback'] = this.searchOpenEmisId.bind(this);
      }
    });

    setTimeout(() => {
      this.showForm = true;
    }, 0);
  }

  setDetails(details: any) {
    this.areasList = details['areas'] && details['areas'].length ? [...details['areas']] : [];
    this.institutionsList =
      details['institutions'] && details['institutions'].length ? [...details['institutions']] : [];

    if (details['users'] && details['users'].length) {
      this.usersList = details['users'].map((item) => ({
        openemis_id: item['openemis_id'],
        user_name: item['user_name'],
        user_id: item['user_id'],
        id: item['user_id'],
        role: item['role']
      }));
    }

    this.questionBase.forEach((question) => {
      if (question['key'] === 'areasTable') {
        this.api.setProperty(question['key'], 'row', [...this.areasList]);
      } else if (question['key'] === 'institutionsTable') {
        this.api.setProperty(question['key'], 'row', [...this.institutionsList]);
      } else if (question['key'] === 'users') {
        this.api.setProperty(question['key'], 'row', [...this.usersList]);
        this.usersList.forEach((user, userIndex) => {
          let roleCell: ITableFormUpdateParams = {
            questionKey: 'role',
            colId: 'role',
            rowId: user['user_id'],
            property: 'value',
            data: details[userIndex]['role'] ? event['value']['role'] : null
          };
          this.api.table('users').setTableCellInputProperty(roleCell);
        });
      } else {
        this.api.setProperty(question['key'], 'value', details[question['key']]);
      }
    });
  }

  searchArea(params: any): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data: Array<any> = [];
      // timer(500).subscribe((): void => {
      if (params['query'].length > 2) {
        this.userGroupsService.getAreasEducation(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'].map((item) => ({ ...item, key: item['area_id'], value: item['area'] }));
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            _observer.next([]);
            _observer.complete();
            console.log(err);
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
    // });
  }

  searchInstitution(params: string) {
    return new Observable((_observer: Subscriber<any>): any => {
      let data: Array<any> = [];
      // timer(500).subscribe((): void => {
      if (params['query'].length > 1) {
        this.userGroupsService.getInstitutions(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'].map((item) => ({ ...item, key: item['institution_id'], value: item['institution'] }));
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            _observer.next([]);
            _observer.complete();
            console.log(err);
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
    // });
  }

  searchOpenEmisId(params: string): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data = [];
      // timer(2000).subscribe((): void => {
      if (params['query'].length > 2) {
        this.userGroupsService.getUsersByOpenEmisId(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'].map((item) => ({ ...item, key: item['user_id'], value: item['name'] }));
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
    // });
  }

  deleteRow(params: any) {
    if (params['data']['area_id']) {
      let deleteIndex: number = this.areasList.findIndex((item) => item['area_id'] === params['data']['area_id']);
      this.areasList.splice(deleteIndex, 1);
      this.api.setProperty('areasTable', 'row', [...this.areasList]);
    } else if (params['data']['institution_id']) {
      let deleteIndex: number = this.institutionsList.findIndex(
        (item) => item['institution_id'] === params['data']['institution_id']
      );
      this.institutionsList.splice(deleteIndex, 1);
      this.api.setProperty('institutionsTable', 'row', [...this.institutionsList]);
    } else if (params['data']['user_id']) {
      let deleteIndex: number = this.usersList.findIndex((item) => item['user_id'] === params['data']['user_id']);
      this.usersList.splice(deleteIndex, 1);
      this.api.setProperty('usersTable', 'row', [...this.usersList]);
    }
  }

  detectValue(event) {
    if (event['key'] === 'area_education' && event['value'] && event['value']['value']) {
      if (this.areasList.findIndex((item) => item['area_id'] === event['value']['area_id']) === -1) {
        this.areasList.push({
          area_id: event['value']['area_id'],
          area_code: event['value']['code'],
          area_level: event['value']['level'],
          area_name: event['value']['area']
        });
      }
      this.api.setProperty('areasTable', 'row', [...this.areasList]);
    } else if (event['key'] === 'institution' && event['value'] && event['value']['value']) {
      if (
        this.institutionsList.findIndex((item) => item['institution_id'] === event['value']['institution_id']) === -1
      ) {
        this.institutionsList.push({
          institution_code: event['value']['code'],
          institution_name: event['value']['institution'],
          institution_id: event['value']['institution_id']
        });
      }
      this.api.setProperty('institutionsTable', 'row', [...this.institutionsList]);
    } else if (event['key'] === 'user' && event['value'] && event['value']['value']) {
      if (this.usersList.findIndex((item) => item['user_id'] === event['value']['user_id']) === -1) {
        let userObj = {
          id: event['value']['user_id'],
          user_id: event['value']['user_id'],
          openemis_id: event['value']['openemis_id'],
          user_name: event['value']['name']
        };
        this.usersList.push(userObj);
        this.api.table('users').addRow([userObj]);
        let rowReponse: ITableFormUpdateParams = {
          questionKey: 'role',
          colId: 'role',
          rowId: event['value']['user_id'],
          property: 'value',
          data: event['value']['role'] ? event['value']['role'] : null
        };
        this.api.table('users').setTableCellInputProperty(rowReponse);
      }
    }
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      const toasterConfig: IToasterConfig = {
        title: 'Missing required fields',
        type: 'error',
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      console.log(form);
      return;

      let payload: IUpdatePayload = {
        name: form['user_group_name'],
        area: this.areasList && this.areasList.length ? this.areasList : null,
        institution: this.institutionsList && this.institutionsList.length ? this.institutionsList : null,
        user: this.usersList && this.usersList.length ? this.usersList : null
      };

      this.userGroupsService.updateGroupDetails(this.userGroupId, payload).subscribe(
        (res: any) => {
          if (res && res['data'] && !res['data']['error']) {
            this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
            setTimeout(() => {
              this.cancel();
            }, 500);
          } else {
            this.sharedService.setToaster({ ...saveFail, body: res['message'] });
          }
        },
        (err) => {
          this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/security/groups/user-groups']);
  }

  ngOnDestroy() {
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    if (this.rolesOptionsSub) {
      this.rolesOptionsSub.unsubscribe();
    }
    if (this.userGroupsIdSub) {
      this.userGroupsIdSub.unsubscribe();
    }
    this.userGroupsService.resetDetails();
    this.userGroupsService.resetUserGroupdId();
    super.destroyPageBaseSub();
  }
}
