import { ITableActionApi } from 'openemis-styleguide-lib';

const TABLE_COLUMNS_AREAS: any = [
  { headerName: 'Level', field: 'area_level', visible: true },
  { headerName: 'Code', field: 'area_code', visible: true },
  { headerName: 'Area', field: 'area_name', visible: true }
];
const TABLE_COLUMNS_INSTITUTIONS: Array<any> = [
  { headerName: 'Code', field: 'institution_code', visible: true },
  { headerName: 'Institution', field: 'institution_name', visible: true }
];
const TABLE_COLUMNS_USERS: Array<any> = [
  { headerName: 'OpenEMIS ID', field: 'openemis_id', visible: true },
  { headerName: 'Name', field: 'user_name', visible: true },
  {
    headerName: 'Role',
    field: 'role',
    visible: true,
    type: 'input',
    config: {
      input: {
        key: 'role',
        controlType: 'dropdown',
        visible: true,
        options: [{ key: null, value: '-- Select --' }]
      }
    }
  }
];

export const QUESTION_BASE: Array</* ITableFormInput */ any> = [
  { key: 'user_group_name', label: 'Name', visible: true, controlType: 'text', type: 'string', required: true },
  { key: 'AreasEducationHeader', label: 'Areas (Education)', visible: true, controlType: 'section' },
  {
    key: 'area_education',
    label: 'Add Area',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'autocomplete',
    clickToggleDropdown: true,
    autocomplete: true,
    placeholder: 'Search Area'
    // onEnter: true
  },
  {
    key: 'areasTable',
    label: 'Areas (Education)',
    controlType: 'table',
    visible: true,
    row: [],
    column: [...TABLE_COLUMNS_AREAS],
    config: {
      id: 'areasTableId',
      rowIdKey: 'id',
      loadType: 'normal',
      gridHeight: 100,
      action: {
        enabled: true,
        list: [{ type: 'delete' }]
      }
    }
  },
  { key: 'InstitutionsHeader', label: 'Institutions', visible: true, controlType: 'section' },
  {
    key: 'institution',
    label: 'Add Institution',
    visibe: true,
    controlType: 'text',
    type: 'autocomplete',
    format: 'string',
    clickToggleDropdown: true,
    autocomplete: true,
    placeholder: 'Search Institution'
    // onEnter: true
  },
  {
    key: 'institutionsTable',
    label: 'Institutions',
    controlType: 'table',
    visible: true,
    row: [],
    column: [...TABLE_COLUMNS_INSTITUTIONS],
    config: {
      id: 'institutionsTableId',
      rowIdKey: 'id',
      loadType: 'normal',
      gridHeight: 100,
      action: {
        enabled: true,
        list: [{ type: 'delete' }]
      }
    }
  },
  { key: 'UsersHeader', label: 'Users', visible: true, controlType: 'section' },
  {
    key: 'user',
    label: 'Add User',
    visible: true,
    controlType: 'text',
    type: 'autocomplete',
    format: 'string',
    clickToggleDropdown: true,
    autocomplete: true,
    placeholder: 'Search OpenEMIS ID'
  },
  {
    key: 'users',
    label: 'Users',
    controlType: 'table',
    visible: true,
    row: [],
    column: [...TABLE_COLUMNS_USERS],
    config: {
      id: 'usersTable',
      rowIdKey: 'id',
      loadType: 'normal',
      gridHeight: 100,
      action: {
        enabled: true,
        list: [{ type: 'delete' }]
      }
    }
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
