import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntryComponent } from './entry/entry.component';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
    children: [
      { path: '', redirectTo: 'user-groups', pathMatch: 'full' },
      { path: 'user-groups', loadChildren: './user-groups/user-groups.module#UserGroupsModule' },
      { path: 'system-groups', loadChildren: './system-groups/system-groups.module#SystemGroupsModule' }
    ]
    /* redirectTo: 'user-groups' */
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupsRoutingModule {}
