import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { SecurityRoutingModule } from './security-routing.module';

@NgModule({
  imports: [CommonModule, SecurityRoutingModule, SharedModule],
  declarations: []
})
export class SecurityModule {}
