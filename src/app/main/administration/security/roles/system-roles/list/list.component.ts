import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  ITableApi,
  ITableConfig,
  KdPageBaseEvent,
  KdTableEvent,
  ITableDatasourceParams,
  KdTableDatasourceEvent
} from 'openemis-styleguide-lib';

import { SystemRolesService } from '../system-roles.service';
import { GRID_ID, PAGE_SIZE, TOTAL_ROWS, TABLE_COLUMN_LIST } from './list.config';
import { Subscription } from 'rxjs';
import { RolesSharedService } from '../../roles-shared.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - System Roles';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public showTable: boolean = false;
  public tableApi: ITableApi = {};

  public tableConfig: ITableConfig = {
    id: GRID_ID,
    loadType: 'oneshot',
    gridHeight: 500,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTAL_ROWS },
    action: {
      enabled: true,
      list: [
        {
          name: 'View',
          icon: 'fa fa-eye',
          custom: true,
          callback: (params: any) => {
            this.systemRolesService.setRoleId(params['data']['id']);
            setTimeout(() => {
              this.router.navigate(['main/security/roles/system-roles/view']);
            }, 0);
          }
        },
        {
          name: 'Permissions',
          icon: 'fa fa-lock',
          custom: true,
          callback: (params: any) => {
            this.rolesSharedService.setCurrentRole({ id: params['data']['id'], type: 'system' });
            setTimeout(() => {
              this.router.navigate(['main/security/roles/permissions']);
            }, 0);
          }
        }
      ]
    }
  };
  public tableColumns = [TABLE_COLUMN_LIST.Visible, TABLE_COLUMN_LIST.Name];
  public tableRows: Array<any> = [];

  /* Subsciptions */
  private listSub: Subscription;
  private tableEventSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public tableEvent: KdTableEvent,
    private systemRolesService: SystemRolesService,
    private rolesSharedService: RolesSharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/security/roles/system-roles/add' }]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.systemRolesService.getList();

    this.listSub = this.systemRolesService.listChanged.subscribe((list: any) => {
      if (list && list.length) {
        this.populateTable(list);
      } else {
        this.showTable = false;
      }
    });

    this.tableEventSub = this.tableEvent.onKdTableEventList(GRID_ID).subscribe((event: any): void => {
      console.log(event);

      if (event instanceof KdTableDatasourceEvent) {
      }
    });
  }

  populateTable(list: Array<any>) {
    if (list.length) {
      list.forEach((item) => {
        item.visible = item['visible'] ? 'YES' : 'NO';
      });
      setTimeout(() => {
        this.tableRows = list;
      }, 0);

      this.tableColumns.forEach((column, colIndex) => {
        if (column['filterable']) {
          let filterValues: Array<any> = [];
          list.forEach((item) => {
            if (item[column['field']] && !filterValues.includes(item[column['field']])) {
              filterValues.push(item[column['field']]);
            }
          });
          this.tableColumns[colIndex]['filterValue'] = filterValues;
        }
      });

      setTimeout(() => {
        this.showTable = true;
      }, 0);
    } else {
      this.showTable = false;
    }
  }

  searchList(keyword: string) {
    if (keyword.length) {
      if (keyword.length > 2) {
        this.systemRolesService.searchList(keyword);
      }
    } else {
      this.systemRolesService.searchList(keyword);
    }
  }

  ngOnDestroy() {
    if (this.listSub) {
      this.listSub.unsubscribe();
    }
    this.systemRolesService.resetList();
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
