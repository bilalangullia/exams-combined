interface IColumnList {
  Visible?: any;
  Name?: any;
}

export const GRID_ID: string = 'systemRolesList';
export const PAGE_SIZE: number = 20;
export const TOTAL_ROWS: number = 100;

export const TABLE_COLUMN_LIST: IColumnList = {
  Visible: { headerNme: 'Visible', field: 'visible', visible: true, sortable: true },
  Name: { headerNme: 'Name', field: 'name', visible: true, sortable: true, filterable: true, filterValue: [] }
};
