import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { RolesDataService } from '../roles-data.service';

@Injectable()
export class SystemRolesService implements OnDestroy {
  private list: Array<any> = [];
  private roleId: string = null;
  private details: any = null;
  private permissions: { main?: any; reports?: any; admin?: any } = null;

  public listChanged = new BehaviorSubject<Array<any>>([]);
  public roleIdChanged = new BehaviorSubject<string>(null);
  public detailsChanged = new BehaviorSubject<any>(null);
  public permissionsChanged = new BehaviorSubject<any>(null);

  constructor(private rolesDataService: RolesDataService, private sharedService: SharedService) {}

  getList() {
    this.rolesDataService.getSystemRolesList().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']['roles']) {
          this.list = res['data']['roles'];
          this.listChanged.next(this.list);
        } else {
          this.list = [];
          this.listChanged.next([]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([]);
      }
    );
  }

  searchList(keyword: string) {
    this.rolesDataService.searchSystemRolesList(keyword).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']['roles']) {
          this.list = res['data']['roles'];
          this.listChanged.next([...this.list]);
        } else {
          this.list = [];
          this.listChanged.next([]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([]);
      }
    );
  }

  resetList() {
    this.list = [];
    this.listChanged.next([]);
  }

  setRoleId(systemRoleId: string) {
    this.roleId = systemRoleId;
    this.roleIdChanged.next(this.roleId);
  }

  resetRoleId() {
    this.roleId = null;
    this.roleIdChanged.next(null);
  }

  addRole(payload: any) {
    return this.rolesDataService.addSystemRole(payload);
  }

  getDetails(systemRoleId: string) {
    this.rolesDataService.getSystemRoleDetails(systemRoleId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.details = res['data'];

          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = null;
          this.detailsChanged.next(null);
        }
      },
      (err) => {
        this.details = null;
        this.detailsChanged.next(null);
      }
    );
  }

  resetDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  updateDetails(payload: any) {
    return this.rolesDataService.updateSystemRole(payload);
  }

  getPermissions(roleId: string, moduleName: string = 'main') {
    this.rolesDataService.getPermissions(roleId, moduleName).subscribe(
      (res: any) => {
        if (res && res['data']) {
          if (res['data']['main']) {
            this.permissions['main'] = res['data']['main'];
          }
          if (res['data']['reports']) {
            this.permissions['reports'] = res['data']['reports'];
          }
          if (res['data']['admin']) {
            this.permissions['admin'] = res['data']['admin'];
          }
          this.permissionsChanged.next({ ...this.permissions });
        } else {
          this.permissions = null;
        }
        console.log(res);
      },
      (err) => {}
    );
  }

  updateUserRolePermissions(roleId: string, payload: any) {
    return this.rolesDataService.updateUserRolePermissions({ ...payload, roleId });
  }

  updateSystemRolePermissions(roleId: string, payload: any) {
    return this.rolesDataService.updateSystemRolePermissions({ ...payload, roleId });
  }

  ngOnDestroy() {
    console.log('Destroying service - system-roles.service');
  }
}
