import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { SystemRolesService } from '../system-roles.service';
import { QUESTION_BASE } from './view.config';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - System Roles';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public questionBase: Array<any> = QUESTION_BASE;

  /* Subscriptions */
  private roleIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private systemRolesService: SystemRolesService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/security/roles/system-roles' },
      { type: 'edit', path: 'main/security/roles/system-roles/edit' }
    ]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;

    this.roleIdSub = this.systemRolesService.roleIdChanged.subscribe((systemRoleId: string) => {
      if (systemRoleId) {
        this.systemRolesService.getDetails(systemRoleId);

        this.detailsSub = this.systemRolesService.detailsChanged.subscribe((details: any) => {
          if (details) {
            this.setDetails(details);
          }
        });
      }
    });
  }

  setDropdownOptions(options: Array<any>) {
    this.questionBase.forEach((question: any) => {});
  }

  setDetails(details: any) {
    if (details['visible']) {
      details['visible'] = details['visible'] ? 'YES' : 'NO';
    }
    this.questionBase.forEach((question: any) => {
      question['value'] = details[question['key']]
        ? details[question['key']]['value']
          ? details[question['key']['value']]
          : details[question['key']]
        : '';
    });
    setTimeout(() => {
      this.showForm = true;
    }, 0);
  }

  ngOnDestroy() {
    if (this.roleIdSub) {
      this.roleIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.systemRolesService.resetDetails();
    super.destroyPageBaseSub();
  }
}
