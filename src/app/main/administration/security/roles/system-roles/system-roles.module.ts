import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { SystemRolesRoutingModule } from './system-roles-routing.module';
import { SystemRolesService } from './system-roles.service';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';

@NgModule({
  imports: [CommonModule, SystemRolesRoutingModule, SharedModule],
  declarations: [ListComponent, AddComponent, ViewComponent, EditComponent],
  providers: [SystemRolesService]
})
export class SystemRolesModule {}
