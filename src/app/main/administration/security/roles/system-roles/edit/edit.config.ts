import { ITableFormInput } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array<ITableFormInput> = [
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string', readonly: true },
  { key: 'visible', label: 'Visible', visible: true, controlType: 'dropdown', type: 'string' }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
