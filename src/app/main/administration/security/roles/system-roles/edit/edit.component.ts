import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { SystemRolesService } from '../system-roles.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Roles';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public questionBase = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  private roleId: string = null;
  private visibleOptions: Array<any> = [
    { id: null, name: '-- Select --' },
    { id: 0, name: 'NO' },
    { id: 1, name: 'YES' }
  ];

  /* Subscriptions */
  private roleIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private systemRolesService: SystemRolesService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setToolbarMainBtns([]);
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.roleIdSub = this.systemRolesService.roleIdChanged.subscribe((roleId: string) => {
      if (!roleId) {
        this.router.navigate(['main/security/roles/system-roles']);
      } else {
        this.roleId = roleId;
        this.systemRolesService.getDetails(roleId);

        this.setDropdownOptions('visible', this.visibleOptions);

        this.detailsSub = this.systemRolesService.detailsChanged.subscribe((details: any = null) => {
          if (details) {
            setTimeout(() => {
              this.setDetails(details);
            }, 0);
          }
        });
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this.questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this.questionBase[quesIndex]['options'] = [...options];
    }
  }

  setDetails(details) {
    this.questionBase.forEach((question) => {
      if (question['key'] == 'visible') {
        question['value'] = details[question['key']];
      } else {
        question['value'] = details[question['key']]
          ? details[question['key']]['key']
            ? details[question['key']]['key']
            : details[question['key']]
          : '';
      }
    });

    setTimeout(() => {
      this.showForm = true;
    }, 0);
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      const toasterConfig: IToasterConfig = {
        title: 'Missing required fields',
        type: 'error',
        timeout: 1000,
        showCloseButton: true,
        tapToDismiss: true
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = { ...form, id: this.roleId };
      this.systemRolesService.updateDetails(payload).subscribe(
        (res: any) => {
          if (res && res['data']) {
            let success: IToasterConfig = { type: 'success', title: 'System Role Updated', timeout: 1000 };
            this.sharedService.setToaster(success);
            setTimeout(() => {
              this.router.navigate(['main/security/roles/system-roles']);
            }, 500);
          } else {
            let error: IToasterConfig = { type: 'error', title: 'Error while saving.', timeout: 1000 };
            this.sharedService.setToaster(error);
          }
        },
        (err) => {
          let error: IToasterConfig = { type: 'error', title: 'Error!', timeout: 1000 };
          this.sharedService.setToaster(error);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/security/roles/system-roles']);
  }

  ngOnDestroy() {
    if (this.roleIdSub) {
      this.roleIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
