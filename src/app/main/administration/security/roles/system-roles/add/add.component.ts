import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';
import { SystemRolesService } from '../system-roles.service';
import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - System Roles';

  public loading: boolean = true;
  public questionBase: Array<any> = QUESTION_BASE;
  public formButtons: Array<any> = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private systemRolesService: SystemRolesService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question: any) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 0);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      /* Show toaster */
      let error: IToasterConfig = { type: 'error', title: 'Missing required fields', timeout: 1000 };
      this.sharedService.setToaster(error);
    } else {
      let payload = { ...form };
      // console.log(payload);
      this.systemRolesService.addRole(payload).subscribe(
        (res: any) => {
          if (res && res['data']) {
            let success: IToasterConfig = { type: 'success', title: 'System Role Added', timeout: 1000 };
            this.sharedService.setToaster(success);
            setTimeout(() => {
              this.router.navigate(['main/security/roles/system-roles']);
            }, 500);
          } else {
            let error: IToasterConfig = { type: 'error', title: 'Error while saving.', timeout: 1000 };
            this.sharedService.setToaster(error);
          }
        },
        (err) => {
          /* error message */
          let error: IToasterConfig = { type: 'error', title: 'Error!', timeout: 1000 };
          this.sharedService.setToaster(error);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/security/roles/system-roles']);
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
