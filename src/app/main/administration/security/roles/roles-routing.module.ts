import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntryComponent } from './entry/entry.component';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
    children: [
      { path: '', redirectTo: 'user-roles', pathMatch: 'full' },
      { path: 'user-roles', loadChildren: './user-roles/user-roles.module#UserRolesModule' },
      { path: 'system-roles', loadChildren: './system-roles/system-roles.module#SystemRolesModule' }
    ]
  },
  { path: 'permissions', loadChildren: './permissions/permissions.module#PermissionsModule' }
];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class RolesRoutingModule {}
