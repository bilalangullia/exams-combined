export interface IFilterGroups {
  security_group?: { id?: number; name?: string };
}

export interface IFilterOptions {
  security_group?: Array<{ key: number; value: string }>;
}
