import { ITableFormInput } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array<ITableFormInput> = [
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string' },
  { key: 'visible', label: 'Visible', visible: true, controlType: 'text', type: 'string' },
  { key: 'security_group', label: 'Security Group', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_by', label: 'Modified By', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_on', label: 'Modified On', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_by', label: 'Created By', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_on', label: 'Created On', visible: true, controlType: 'text', type: 'string' }
];
