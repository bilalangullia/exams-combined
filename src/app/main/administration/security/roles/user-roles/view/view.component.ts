import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { UserRolesService } from '../user-roles.service';
import { QUESTION_BASE } from './view.config';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Roles';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public questionBase: Array<any> = QUESTION_BASE;

  /* Subscriptions */
  private userRoleIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private userRolesService: UserRolesService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/security/roles/user-roles' },
      { type: 'edit', path: 'main/security/roles/user-roles/edit' }
    ]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;

    this.userRoleIdSub = this.userRolesService.roleIdChanged.subscribe((userRoleId: string) => {
      if (userRoleId) {
        this.userRolesService.getDetails(userRoleId);
      }
    });

    this.detailsSub = this.userRolesService.detailsChanged.subscribe((details: any) => {
      if (details) {
        this.setDetails(details);
      }
    });
  }

  setDetails(details: any) {
    this.showForm = false;

    this.questionBase.forEach((question) => {
      if (question['key'] === 'visible') {
        question['value'] = details[question['key']] ? 'YES' : 'NO';
      } else {
        question['value'] = details[question['key']]
          ? details[question['key']]['value']
            ? details[question['key']['value']]
            : details[question['key']]
          : '';
      }
    });

    timer(100).subscribe(() => {
      this.showForm = true;
    });
  }

  ngOnDestroy() {
    if (this.userRoleIdSub) {
      this.userRoleIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.userRolesService.resetDetails();
    super.destroyPageBaseSub();
  }
}
