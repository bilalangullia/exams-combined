import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, ITableApi, ITableConfig, KdPageBaseEvent, KdView, KdFilter } from 'openemis-styleguide-lib';

import { RolesSharedService } from '../../roles-shared.service';
import { UserRolesService } from '../user-roles.service';
import { IFilterOptions, IFilterGroups } from '../user-roles.interfaces';
import { GRID_ID, PAGE_SIZE, TOTAL_ROWS, TABLE_COLUMN_LIST, FILTER_INPUTS } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Roles';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public showTable: boolean = false;
  public tableApi: ITableApi = {};
  public inputs = FILTER_INPUTS;

  public tableConfig: ITableConfig = {
    id: GRID_ID,
    loadType: 'oneshot',
    gridHeight: 500,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTAL_ROWS },
    action: {
      enabled: true,
      list: [
        {
          name: 'View',
          icon: 'fa fa-eye',
          custom: true,
          callback: (params: any) => {
            this.userRolesService.setRoleId(params['data']['id']);
            this.router.navigate(['main/security/roles/user-roles/view']);
          }
        },
        {
          name: 'Permissions',
          icon: 'fa fa-lock',
          custom: true,
          callback: (params: any) => {
            this.rolesSharedService.setCurrentRole({ id: params['data']['id'], type: 'user' });
            setTimeout(() => {
              this.router.navigate(['main/security/roles/permissions']);
            }, 0);
          }
        }
      ]
    }
  };
  public tableColumns = [TABLE_COLUMN_LIST.Visible, TABLE_COLUMN_LIST.Name];
  public tableRows: Array<any> = [];

  private filter: IFilterGroups = {};

  @ViewChild('filters') rolesFilter: KdFilter;

  /* Subscriptions */
  private filterOptionsChangedSub: Subscription;
  private filterChangedSub: Subscription;
  private listChangedSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private userRolesService: UserRolesService,
    private rolesSharedService: RolesSharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/security/roles/user-roles/add' }]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.userRolesService.getFilterOptions();

    this.filterOptionsChangedSub = this.userRolesService.filterOptionsChanged.subscribe((options: IFilterOptions) => {
      if (options && options['security_group']) {
        this.setDropdownOptions('security_group', options['security_group']);
      } else {
        this.setDropdownOptions('security_group', []);
      }
    });

    this.filterChangedSub = this.userRolesService.filterChanged.subscribe((filter: IFilterGroups) => {
      if (filter && filter['security_group'] && filter['security_group']['id']) {
        this.filter = { ...filter };
        this.userRolesService.getList(filter['security_group']['id']);
      }
    });

    this.listChangedSub = this.userRolesService.listChanged.subscribe((list: Array<any>) => {
      if (list && list.length) {
        this.populateTable(list);
      } else {
        this.showTable = false;
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      options = data.map((item) => ({ key: item.id, value: item.name }));
      this.rolesFilter.setInputProperty(key, 'options', options);
      setTimeout(() => {
        this.rolesFilter.setInputProperty(key, 'value', options[0]['key']);
      }, 0);
    } else {
      options.unshift({ key: null, value: '-- Select --' });
      this.rolesFilter.setInputProperty(key, 'options', options);
    }
  }

  detectValue(event) {
    let filter: IFilterGroups = {};
    if (event['value']) {
      filter[event['key']] = { id: event['value'] ? +event['value'] : null };
    }
    let value = event['options'].find((item) => item['key'] == event['value']);
    if (value) {
      filter[event['key']] = { ...filter[event['key']], value };
    }
    this.userRolesService.setFilters(filter);
  }

  populateTable(list: Array<any>) {
    if (list.length) {
      list.forEach((item) => {
        item.visible = item['visible'] ? 'YES' : 'NO';
      });
      setTimeout(() => {
        this.tableRows = list;
      }, 0);

      this.tableColumns.forEach((column, colIndex) => {
        if (column['filterable']) {
          list.forEach((item) => {
            if (item[column['field']]) {
              this.tableColumns[colIndex]['filterValue'].push(item[column['field']]);
            }
          });
        }
      });
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    } else {
      this.showTable = false;
    }
  }

  searchList(keyword: string) {
    if (keyword.length) {
      if (keyword.length > 2) {
        this.userRolesService.searchList(this.filter['security_group']['id'], keyword);
      }
    } else {
      this.userRolesService.searchList(this.filter['security_group']['id'], keyword);
    }
  }

  ngOnDestroy() {
    if (this.filterOptionsChangedSub) {
      this.filterOptionsChangedSub.unsubscribe();
    }
    if (this.filterChangedSub) {
      this.filterChangedSub.unsubscribe();
    }
    if (this.listChangedSub) {
      this.listChangedSub.unsubscribe();
    }

    this.userRolesService.resetFilters();
    this.userRolesService.resetList();
    super.destroyPageBaseSub();
  }
}
