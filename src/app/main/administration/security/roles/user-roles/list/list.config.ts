interface IColumnList {
  Visible?: any;
  Name?: any;
}

export const GRID_ID: string = 'userRolesList';
export const PAGE_SIZE: number = 20;
export const TOTAL_ROWS: number = 100;

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'security_group',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  }
];

export const TABLE_COLUMN_LIST: IColumnList = {
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    type: 'text',
    visible: true,
    sortable: true,
    filterable: true,
    filterValue: []
  },
  // config: { image: { width: 50, height: 50, isExpandable: true, icon: 'kd-close' } }
  Name: { headerName: 'Name', field: 'name', visible: true, sortable: true, filterable: true, filterValue: [] }
};
