import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { UserRolesService } from '../user-roles.service';
import { IFilterOptions } from '../user-roles.interfaces';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Roles';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public questionBase = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  private userRoleId: string = null;
  private visibleOptions: Array<any> = [
    { id: 0, name: 'NO' },
    { id: 1, name: 'YES' }
  ];

  /* Susbscriptions */
  private groupOptionsSub: Subscription;
  private userRoleIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private userRolesService: UserRolesService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setToolbarMainBtns([]);
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.userRoleIdSub = this.userRolesService.roleIdChanged.subscribe((userRoleId: string) => {
      if (userRoleId) {
        this.userRoleId = userRoleId;
        this.groupOptionsSub = this.userRolesService.filterOptionsChanged.subscribe((options: IFilterOptions) => {
          if (options && options['security_group']) {
            this.setDropdownOptions('visible', this.visibleOptions);
            this.setDropdownOptions('security_group_id', options['security_group']);
          }
        });

        this.userRolesService.getDetails(userRoleId);

        setTimeout(() => {
          this.detailsSub = this.userRolesService.detailsChanged.subscribe((details: any) => {
            if (details) {
              this.setDetails(details);
            }
          });
        }, 0);
      }
      /* 
      else
       */
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    if (data.length) {
      let options: Array<any> = data.map((item) => ({ key: item['id'], value: item['name'] }));
      options.unshift({ key: null, value: '-- Select --' });
      const questionIndex = this.questionBase.findIndex((item) => item['key'] === key);
      if (questionIndex && questionIndex !== -1) {
        this.questionBase[questionIndex]['options'] = options;
      }
    }
  }

  setDetails(details) {
    this.questionBase.forEach((question) => {
      if (question['key'] === 'visible') {
        question['value'] = details[question['key']];
      } else {
        question['value'] = details[question['key']]
          ? details[question['key']]['key']
            ? details[question['key']]['key']
            : details[question['key']]
          : '';
      }
    });
    setTimeout(() => {
      this.showForm = true;
    }, 0);
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      const errorMessage: IToasterConfig = {
        title: 'Missing required fields',
        type: 'error',
        timeout: 1000
      };
      this.sharedService.setToaster(errorMessage);
    } else {
      let payload = { ...form, id: this.userRoleId };
      this.userRolesService.updateDetails(payload).subscribe(
        (res: any) => {
          if (res['data']) {
            this.router.navigate(['main/security/roles/user-roles']);
            let successMessage: IToasterConfig = {
              title: 'Record Updated Successfully.',
              type: 'success',
              timeout: 1000
            };
            this.sharedService.setToaster(successMessage);
          }
        },
        (err) => {
          let errorMessage: IToasterConfig = {
            title: 'An error occurred.',
            type: 'error',
            timeout: 1000
          };
          this.sharedService.setToaster(errorMessage);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/security/roles/user-roles']);
  }

  ngOnDestroy() {
    if (this.userRoleIdSub) {
      this.userRoleIdSub.unsubscribe();
    }
    if (this.groupOptionsSub) {
      this.groupOptionsSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.userRolesService.resetRoleId();
    this.userRolesService.resetFilters();

    super.destroyPageBaseSub();
  }
}
