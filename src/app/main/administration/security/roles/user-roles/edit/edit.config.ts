import { ITableFormInput } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array<ITableFormInput> = [
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string', required: true },
  {
    key: 'visible',
    label: 'Visible',
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    value: null
  },
  {
    key: 'security_group_id',
    label: 'Security Group',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    value: null,
    readonly: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
