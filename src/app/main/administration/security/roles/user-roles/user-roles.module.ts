import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { UserRolesRoutingModule } from './user-roles-routing.module';
import { UserRolesService } from './user-roles.service';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';

@NgModule({
  imports: [CommonModule, UserRolesRoutingModule, SharedModule],
  declarations: [ListComponent, AddComponent, ViewComponent, EditComponent],
  providers: [UserRolesService]
})
export class UserRolesModule {}
