import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { UserRolesService } from '../user-roles.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - User Roles';

  public loading: boolean = true;
  public showForm: boolean = false;
  public questionBase: Array<any> = QUESTION_BASE;
  public formButtons: Array<any> = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  /* Subscriptions */
  private groupOptionsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private userRolesService: UserRolesService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;
    this.showForm = true;
    this.userRolesService.getFilterOptions();

    this.userRolesService.filterOptionsChanged.subscribe((options) => {
      if (options && options['security_group']) {
        setTimeout(() => {
          this.setDropdownOptions('security_group', options['security_group']);
        }, 0);
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      options = data.map((item) => ({ key: item.id, value: item.name }));
      options.unshift({ key: null, value: '-- Select --' });
      this.api.setProperty(key, 'options', options);
    } else {
      options.unshift({ key: null, value: '-- Select --' });
      this.api.setProperty(key, 'options', options);
    }
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question: any) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 0);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      /* Show toaster */
      let warning: IToasterConfig = {
        title: 'Missing Required Fields',
        type: 'error',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 1000
      };
      this.sharedService.setToaster(warning);
    } else {
      let payload = { ...form };
      this.userRolesService.addRole(payload).subscribe(
        (res: any) => {
          if (res['data']) {
            let success: IToasterConfig = {
              title: 'Role Added Successfully',
              type: 'success',
              showCloseButton: true,
              tapToDismiss: true,
              timeout: 1000
            };
            this.sharedService.setToaster(success);
            setTimeout(() => {
              this.router.navigate(['main/security/roles/user-roles']);
            }, 500);
          }
        },
        (err) => {
          console.log(err);
          let error: IToasterConfig = {
            title: 'Error while saving.',
            type: 'error',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(error);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/security/roles/user-roles']);
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
