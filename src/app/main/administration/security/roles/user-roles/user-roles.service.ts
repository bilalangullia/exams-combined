import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { RolesDataService } from '../roles-data.service';
import { IFilterOptions, IFilterGroups } from './user-roles.interfaces';

@Injectable()
export class UserRolesService {
  private filterOptions: IFilterOptions = {};
  private filter: IFilterGroups = null;
  private list: Array<any> = [];
  private roleId: string = null;
  private details: any = null;

  public filterOptionsChanged = new BehaviorSubject<IFilterOptions>(this.filterOptions);
  public filterChanged = new BehaviorSubject<IFilterGroups>(null);
  public listChanged = new BehaviorSubject<Array<any>>([]);
  public roleIdChanged = new BehaviorSubject<string>(null);
  public detailsChanged = new BehaviorSubject<any>(null);

  constructor(private rolesDataService: RolesDataService) {}

  getFilterOptions() {
    this.rolesDataService.getFilterOptions().subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.filterOptions['security_group'] = res['data'];
          this.filterOptionsChanged.next(this.filterOptions);
        } else {
          this.filterOptions['security_group'] = [];
          this.filterOptionsChanged.next(this.filterOptions);
        }
      },
      (err) => {
        this.filterOptions['security_group'] = [];
        this.filterOptionsChanged.next(this.filterOptions);
      }
    );
  }

  setFilters(filter: IFilterGroups) {
    this.filter = { ...filter };
    this.filterChanged.next({ ...this.filter });
  }

  resetFilters() {
    this.filter = {};
    this.filterChanged.next({});
  }

  getList(userRoleId: number) {
    this.rolesDataService.getUserRolesList(userRoleId).subscribe(
      (res: any) => {
        if (res['data'] && res['data']['roles']) {
          this.list = res['data']['roles'];
          this.listChanged.next(this.list);
        } else {
          this.list = [];
          this.listChanged.next([]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([]);
      }
    );
  }

  searchList(securityGroupId: number, keyword: string) {
    this.rolesDataService.searchUserRolesList(securityGroupId, keyword).subscribe(
      (res: any) => {
        if (res['data'] && res['data']['roles']) {
          this.list = res['data']['roles'];
          this.listChanged.next(this.list);
        } else {
          this.list = [];
          this.listChanged.next([]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([]);
      }
    );
  }
  resetList() {
    this.list = [];
    this.listChanged.next([]);
  }

  setRoleId(systemRoleId: string) {
    this.roleId = systemRoleId;
    this.roleIdChanged.next(this.roleId);
  }

  resetRoleId() {
    this.roleId = null;
    this.roleIdChanged.next(null);
  }

  addRole(payload: any) {
    return this.rolesDataService.addUserRole(payload);
  }

  getDetails(userRoleId: string) {
    this.rolesDataService.getUsesRolesViewDetails(userRoleId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.details = res['data'];
          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = null;
          this.detailsChanged.next(null);
        }
      },
      (err) => {
        this.details = null;
        this.detailsChanged.next(null);
      }
    );
  }

  resetDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  updateDetails(payload: any) {
    return this.rolesDataService.updateUserRole(payload);
  }
}
