import { IKdTabs } from '../../../../../shared/kdComponents/kdInterfaces';

const ROUTER_BASE_PATH: string = 'main/security/roles';

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'User Roles', routerPath: ROUTER_BASE_PATH + '/user-roles', isActive: true },
  { tabName: 'System Roles', routerPath: ROUTER_BASE_PATH + '/system-roles' }
];
