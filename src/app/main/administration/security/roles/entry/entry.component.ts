import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  IBreadcrumbConfig,
  IPageheaderConfig,
  IPageheaderApi,
  KdPageBaseEvent
} from 'openemis-styleguide-lib';

import { IKdTabs } from '../../../../../shared/kdComponents/kdInterfaces';
import { TABS_LIST } from './entry.config';
import { RolesSharedService } from '../roles-shared.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security';

  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public tabsList: Array<IKdTabs> = TABS_LIST;
  public isTabActive: boolean = true;

  /* Subscriptions */
  private currentTabSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private rolesSharedService: RolesSharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj) => {
      this.breadcrumbList = breadcrumbObj;
    });
  }

  ngOnInit() {
    this.currentTabSub = this.rolesSharedService.currentTabChanged.subscribe((tabName: string) => {
      if (tabName) {
        if (tabName === 'user') {
          this.tabsList[0]['isActive'] = true;
        } else if (tabName === 'system') {
          this.tabsList[1]['isActive'] = true;
        } else {
          this.tabsList[0]['isActive'] = true;
        }
      }
    });

    super.updateBreadcrumb();
    super.updatePageHeader();
  }

  selectedTab(event) {
    // console.log('EntryComponent -> selectedTab -> event', event);
  }

  ngOnDestroy() {
    if (this.currentTabSub) {
      this.currentTabSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
