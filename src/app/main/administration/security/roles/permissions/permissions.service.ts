import { Injectable } from '@angular/core';
import { IRole } from '../roles.interfaces';
import { BehaviorSubject } from 'rxjs';
import { RolesSharedService } from '../roles-shared.service';
import { RolesDataService } from '../roles-data.service';

@Injectable()
export class PermissionsService {
  private currentRole: IRole = null;

  public currentRoleChanged = new BehaviorSubject<IRole>(this.currentRole);

  constructor(private rolesSharedService: RolesSharedService, private rolesDataService: RolesDataService) {}

  getCurrentRole() {
    this.rolesSharedService.currentRoleChanged.subscribe((currentRole: IRole) => {
      if (currentRole) {
        this.currentRole = currentRole;
        this.currentRoleChanged.next(this.currentRole);
      }
    });
  }

  getPermissions(roleId, moduleName) {
    this.rolesDataService.getPermissions(roleId, moduleName).subscribe((permissions: any) => {
      if (permissions['main']) {
      }
    });
  }
}
