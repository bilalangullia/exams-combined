import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { PermissionsRoutingModule } from './permissions-routing.module';
import { PermissionsService } from './permissions.service';
import { ViewComponent } from './view/view.component';
import { MainComponent as ViewMainComponent } from './view/main/main.component';
import { ReportsComponent as ViewReportsComponent } from './view/reports/reports.component';
import { AdminComponent as ViewAdminComponent } from './view/admin/admin.component';
import { EditComponent } from './edit/edit.component';
import { MainComponent as EditMainComponent } from './edit/main/main.component';
import { ReportsComponent as EditReportsComponent } from './edit/reports/reports.component';
import { AdminComponent as EditAdminComponent } from './edit/admin/admin.component';

@NgModule({
  imports: [CommonModule, PermissionsRoutingModule, SharedModule],
  declarations: [
    ViewComponent,
    ViewMainComponent,
    ViewReportsComponent,
    ViewAdminComponent,
    EditComponent,
    EditMainComponent,
    EditReportsComponent,
    EditAdminComponent
  ],
  providers: [PermissionsService]
})
export class PermissionsModule {}
