import { ITableFormInput } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array<ITableFormInput> = [
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string' }
];
