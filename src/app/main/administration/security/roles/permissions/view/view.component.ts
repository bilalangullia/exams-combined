import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  IBreadcrumbConfig,
  IPageheaderConfig,
  IPageheaderApi,
  KdPageBaseEvent
} from 'openemis-styleguide-lib';

import { TABS_LIST } from './view.config';
import { IKdTabs } from '../../../../../../shared/kdComponents/kdInterfaces';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - Permissions';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public tabsList: Array<IKdTabs> = TABS_LIST;

  /* Subscriptions */
  private currentRoleSub: Subscription;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public pageEvent: KdPageBaseEvent) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/security/roles/' },
      { type: 'edit', path: 'main/security/roles/permissions/edit' }
    ]);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });
    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj) => {
      this.breadcrumbList = breadcrumbObj;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.tabsList[0]['isActive'] = true;
    this.loading = false;
  }

  selectedTab(event) {
    console.log('Selected Tab', event);
  }

  ngOnDestroy() {
    if (this.currentRoleSub) {
      this.currentRoleSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
