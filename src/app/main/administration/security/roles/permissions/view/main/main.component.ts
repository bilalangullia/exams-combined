import { Component, OnInit, OnDestroy } from '@angular/core';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';
import { Router, ActivatedRoute } from '@angular/router';
import { QUESTION_BASE } from './main.config';
import { Subscription } from 'rxjs';
import { RolesSharedService } from '../../../roles-shared.service';
import { PermissionsService } from '../../permissions.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - Permissions';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public showForm: boolean = false;
  public questionBase: Array<any> = QUESTION_BASE;
  public api: IDynamicFormApi = {};

  /* Subscriptions */
  private roleIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private rolesSharedService: RolesSharedService,
    private permissionsService: PermissionsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;
    this.showForm = true;
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
