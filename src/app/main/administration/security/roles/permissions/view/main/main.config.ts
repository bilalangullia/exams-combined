import { ITableFormInput } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array</* ITableFormInput */ any> = [
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string' },
  {
    key: 'formcheck',
    label: 'Form Checkbox',
    visible: true,
    controlType: 'checkbox',
    type: 'checkbox',
    list: [{ title: 'a', value: 1 }],
    value: { 1: true }
  },
  {
    key: 'table',
    label: 'table checkbox',
    visible: true,
    controlType: 'table',
    row: [{ id: 1 }, { id: 2 }],
    column: [
      {
        headerName: 'testCheck',
        field: 'check',
        sortable: false,
        filterable: false,
        visible: true,
        controlType: 'checkbox',
        type: 'checkbox',
        list: [{ title: 'a', value: 1 }]
      },
      {
        headerName: 'testCheck2',
        field: 'check2',
        sortable: false,
        filterable: false,
        visible: true,
        type: 'input',
        config: {
          controlType: 'checkbox',
          list: [{ title: 'b', value: 1 }]
        }
      }
    ],
    config: {
      id: 'formNormal',
      gridHeight: 300,
      loadType: 'normal'
    }
  }
];
