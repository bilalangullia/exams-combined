import { IKdTabs } from '../../../../../../shared/kdComponents/kdInterfaces';

const ROUTER_BASE_PATH: string = 'main/security/roles/permissions/view';

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'Main', routerPath: ROUTER_BASE_PATH + '/main', isActive: true },
  { tabName: 'Reports', routerPath: ROUTER_BASE_PATH + '/reports' },
  { tabName: 'Administration', routerPath: ROUTER_BASE_PATH + '/admin' }
];
