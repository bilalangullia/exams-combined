import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewComponent } from './view/view.component';
import { MainComponent as ViewMainComponent } from './view/main/main.component';
import { ReportsComponent as ViewReportsComponent } from './view/reports/reports.component';
import { AdminComponent as ViewAdminComponent } from './view/admin/admin.component';
import { EditComponent } from './edit/edit.component';
import { MainComponent as EditMainComponent } from './edit/main/main.component';
import { ReportsComponent as EditReportsComponent } from './edit/reports/reports.component';
import { AdminComponent as EditAdminComponent } from './edit/admin/admin.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'view' },
  {
    path: 'view',
    component: ViewComponent,
    children: [
      { path: '', redirectTo: 'main' },
      { path: 'main', component: ViewMainComponent },
      { path: 'reports', component: ViewReportsComponent },
      { path: 'admin', component: ViewAdminComponent }
    ]
  },
  {
    path: 'edit',
    component: EditComponent,
    children: [
      { path: '', redirectTo: 'main' },
      { path: 'main', component: EditMainComponent },
      { path: 'reports', component: EditReportsComponent },
      { path: 'admin', component: EditAdminComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissionsRoutingModule {}
