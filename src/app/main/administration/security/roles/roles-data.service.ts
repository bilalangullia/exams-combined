import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class RolesDataService {
  constructor(private http: HttpClient, private router: Router) {}

  private setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      // 'Content-Type': 'application/json'
      return { headers: headers };
    }
  }

  private handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
      // case 0:
      //   console.log(0);
      //   break;
    }
    console.log(error);
    return throwError(error);
  };

  /********** USER ROLES APIS - START **********/

  /* User Roles | List | Filter Dropdown Options */
  getFilterOptions() {
    return this.http
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.groups}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /* User Roles | Add | Save New */
  addUserRole(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-roles/store
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.userRoles}/${urls.store}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* User Roles | List | User Roles Listing */
  getUserRolesList(securityGroupId: number, limitParams?: any, sortParams?: any, filterParams?: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-roles/list/21?start=0&end=20
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.userRoles}/${urls.list}/${securityGroupId}?start=0&end=20`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* User Roles | List | Search Listing */
  searchUserRolesList(securityGroupId: number, keyword) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-roles/list/21?start=0&end=20
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.userRoles}/${urls.list}/${securityGroupId}?start=0&end=20&search=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* User Roles | View | Get User Role Details */
  getUsesRolesViewDetails(userRoleId: string) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-roles/view/2
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.userRoles}/${urls.view}/${userRoleId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* User Roles | Edit | Submit Updated User Role Details */
  updateUserRole(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-roles/update	{"id":"12","name":"Test role","visible":"1"}
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.userRoles}/${urls.update}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** USER ROLES APIS - END **********/
  /*****************************************/
  /********** SYSTEM ROLES APIS - START **********/

  getSystemRolesList() {
    // http://openemis.n2.iworklab.com/api/administration/security/system-roles/list?start=0&end=20&keyword=st
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.systemRoles}/${urls.list}?start=0&end=20`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  searchSystemRolesList(keyword: string) {
    // http://openemis.n2.iworklab.com/api/administration/security/system-roles/list?start=0&end=20&keyword=st
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.systemRoles}/${urls.list}?start=0&end=20&${urls.keyword}=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  addSystemRole(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/system-roles/store
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.systemRoles}/${urls.store}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getSystemRoleDetails(systemRoleId: string) {
    // http://openemis.n2.iworklab.com/api/administration/security/system-roles/view/3
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.systemRoles}/${urls.view}/${systemRoleId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  updateSystemRole(payload) {
    // http://openemis.n2.iworklab.com/api/administration/security/system-roles/update
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.systemRoles}/${urls.update}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** PERMISSIONS **********/

  getPermissions(roleId: string, moduleName: string = '') {
    // http://openemis.n2.iworklab.com/api/administration/security/user-roles/permission-list/1?module=reports
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.role}s/${urls.permission}-${urls.list}/${roleId}?$module=${moduleName}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  updateUserRolePermissions(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/user-roles/permission-list/update
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.user}-${urls.role}s/${urls.permission}-${urls.list}/${urls.update}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  updateSystemRolePermissions(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/security/system-roles/permission-list/update
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.security}/${urls.system}-${urls.role}s/${urls.permission}-${urls.list}/${urls.update}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /********** SYSTEM ROLES APIS - END **********/
}
