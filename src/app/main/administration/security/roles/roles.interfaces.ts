export interface IRole {
  id: string;
  type: 'user' | 'system' | null;
}
