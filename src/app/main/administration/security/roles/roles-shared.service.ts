import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RolesDataService } from './roles-data.service';
import { IRole } from './roles.interfaces';

@Injectable()
export class RolesSharedService {
  private currentRole: IRole = null;
  private currentTab: 'user' | 'system' = 'user';

  public currentRoleChanged = new BehaviorSubject<IRole>(this.currentRole);
  public currentTabChanged = new BehaviorSubject<string>(this.currentTab);

  constructor() {}

  setCurrentRole(role: IRole) {
    this.currentRole = { ...role };
    this.currentRoleChanged.next(this.currentRole);
    console.log('currentRole -> ', this.currentRole);
  }

  resetCurrentRole() {
    this.currentRole = null;
    this.currentRoleChanged.next(null);
  }

  setcurrentTab(tabName: 'user' | 'system') {
    this.currentTab = tabName;
    this.currentTabChanged.next(this.currentTab);
  }

  resetCurrentTab() {
    this.currentTab = 'user';
    this.currentTabChanged.next(this.currentTab);
  }
}
