import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { RolesRoutingModule } from './roles-routing.module';
import { EntryComponent } from './entry/entry.component';
import { RolesDataService } from './roles-data.service';
import { RolesSharedService } from './roles-shared.service';

@NgModule({
  imports: [CommonModule, RolesRoutingModule, SharedModule],
  declarations: [EntryComponent],
  providers: [RolesDataService, RolesSharedService]
})
export class RolesModule {}
