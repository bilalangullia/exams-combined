import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersViewRoutingModule } from './users-view-routing.module';
import { EntryComponent } from './entry/entry.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { UsersViewService } from './users-view.service';
import { UsersViewDataService } from './users-view-data.service';

@NgModule({
  imports: [CommonModule, UsersViewRoutingModule, SharedModule],
  declarations: [EntryComponent],
  providers: [UsersViewService, UsersViewDataService]
})
export class UsersViewModule {}
