import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../../shared/shared.service';
import { UsersService } from '../../../users.service';
import { AccountsService } from '../accounts.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
  providers: [AccountsService]
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public userName: any;
  public openEMISId: any;

  /* Subscriptions */
  private userNameSub: Subscription;
  private accountViewDetailsSub: Subscription;
  private openEMISIdSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private sharedService: SharedService,
    private usersService: UsersService,
    private accountsService: AccountsService
  ) {
    super({ router: router, activatedRoute: activatedRoute, pageEvent: pageEvent });
    super.setPageTitle('Security - Accounts', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.openEMISIdSub = this.usersService.openEmisIdChanged.subscribe((data) => {
      if (data) {
        this.openEMISId = data;
      } else {
        this.router.navigate(['main/security/users/list']);
      }
    });

    this.userNameSub = this.usersService.userNameChanged.subscribe((data) => {
      this.userName = data;
      this.accountsService.getAccountsViewDetails(this.openEMISId);
      this.accountViewDetailsSub = this.accountsService.accountsTabViewDetailsChanged.subscribe((details) => {
        if (details.length) {
          this.setAccountDetails(details[0]);
        }
      });
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields',
        tapToDismiss: true,
        showCloseButton: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = {
        username: event.username,
        password: event.new_password,
        confirm_password: event.retype_password
      };
      if (payload.password !== payload.confirm_password) {
        let toasterConfig: any = {
          type: 'error',
          title: 'Password Missmatch',
          tapToDismiss: true,
          showCloseButton: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      } else {
        this.accountsService.updateAccountDetails(this.openEMISId, payload);
      }
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this.router.navigate(['main/security/users/view/accounts/view']);
  }

  setAccountDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        this.api.setProperty(
          this._questionBase[i].key,
          'value',
          !data[this._questionBase[i].key]
            ? ''
            : data[this._questionBase[i].key].value
            ? data[this._questionBase[i].key].value
            : data[this._questionBase[i].key]
        );
      }
      this.loading = false;
    });
  }

  ngOnDestroy() {
    if (this.userNameSub) {
      this.userNameSub.unsubscribe();
    }
    if (this.accountViewDetailsSub) {
      this.accountViewDetailsSub.unsubscribe();
    }
    if (this.openEMISIdSub) {
      this.openEMISIdSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
