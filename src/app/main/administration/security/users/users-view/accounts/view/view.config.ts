export const VIEWNODE_INPUT: any = [
  {
    key: 'username',
    label: 'Username',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'last_login',
    label: 'Last Login',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'role',
    label: 'Roles',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Groups',
        field: 'security_group_name',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Roles',
        field: 'security_role_name',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'options',
      rowIdkey: 'id',
      gridHeight: 150,
      loadType: 'normal'
    }
  }
];
