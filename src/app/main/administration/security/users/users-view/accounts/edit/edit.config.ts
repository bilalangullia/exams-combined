export const QUESTION_BASE: Array<any> = [
  {
    key: 'username',
    label: 'Username',
    required: true,
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'new_password',
    label: 'New Password',
    required: true,
    visible: true,
    controlType: 'password',
    type: 'password'
  },
  {
    key: 'retype_password',
    label: 'Retype Password',
    required: true,
    visible: true,
    controlType: 'password',
    type: 'password'
  }
];

export const FORM_BUTTONS = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-check'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
