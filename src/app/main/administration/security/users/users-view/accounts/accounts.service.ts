import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { UsersViewService } from '../users-view.service';
@Injectable()
export class AccountsService {
  private accountsTabViewDetails: any = [];
  public accountsTabViewDetailsChanged = new Subject<any>();
  constructor(
    private router: Router,
    private sharedService: SharedService,
    private usersViewService: UsersViewService
  ) {}

  /*** VIEW/EDIT PAGE | Get account details for populating the view fields. ***/
  getAccountsViewDetails(id) {
    this.usersViewService.getAccountsView(id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.accountsTabViewDetails = res.data;
          this.accountsTabViewDetailsChanged.next([...this.accountsTabViewDetails]);
        }
      },
      (err) => {
        this.accountsTabViewDetails = [];
        this.accountsTabViewDetailsChanged.next([...this.accountsTabViewDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Accounts Details Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/security/users/list']);
      }
    );
  }

  /**  Accounts | EDIT   **/
  updateAccountDetails(openemis_id, data) {
    this.usersViewService.updateAccountDetails(openemis_id, data).subscribe(
      (res) => {
        let toasterConfig: any = {
          type: 'success',
          title: 'Success',
          body: 'Account details updated',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/security/users/list']);
      },
      (err) => {
        let toasterConfig: any = {
          type: 'error',
          title: 'Failed',
          body: 'Error updating user details.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }
}
