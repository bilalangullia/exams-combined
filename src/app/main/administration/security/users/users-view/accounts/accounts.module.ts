import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsRoutingModule } from './accounts-routing.module';
import { SharedModule } from '../../../../../../shared/shared.module';
import { EditComponent } from './edit/edit.component';
import { ViewComponent } from './view/view.component';

@NgModule({
  imports: [CommonModule, AccountsRoutingModule, SharedModule],
  declarations: [EditComponent, ViewComponent]
})
export class AccountsModule {}
