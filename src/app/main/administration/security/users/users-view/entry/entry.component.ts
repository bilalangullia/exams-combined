import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBaseEvent,
  IPageheaderConfig,
  IPageheaderApi,
  IBreadcrumbConfig,
  KdPageBase,
} from 'openemis-styleguide-lib';
import { IKdTabs } from '../../../../../../shared/kdComponents/kdInterfaces';
import { TABS_LIST } from './entry.config';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css'],
})
export class EntryComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Security - Users';
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' }, list: [] };
  public tabsList: Array<IKdTabs>;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public pageEvent: KdPageBaseEvent) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });
    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbArray: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbArray;
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.tabsList = TABS_LIST;
    this.tabsList[0]['isActive'] = true;
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
