import { IKdTabs } from '../../../../../../shared/kdComponents/kdInterfaces';

const ROUTER_BASE_PATH = 'main/security/users/view';

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'Details', routerPath: ROUTER_BASE_PATH + '/details', isActive: true },
  { tabName: 'Accounts', routerPath: ROUTER_BASE_PATH + '/accounts' }
];
