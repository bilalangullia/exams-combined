export const VIEWNODE_INPUT: any = [
  {
    key: 'openemis_no',
    label: 'OpenEMIS ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'middle_name',
    label: 'Middle Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'third_name',
    label: 'Third Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'gender',
    label: 'Gender',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'status',
    label: 'Status',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'nationality',
    label: 'Nationality',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'identity_type',
    label: 'Identity Type',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'identity_number',
    label: 'Identity Number',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'email',
    label: 'Email',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'role',
    label: 'Roles',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Groups',
        field: 'security_group_name',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Roles',
        field: 'security_role_name',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'options',
      rowIdkey: 'id',
      gridHeight: 150,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];
