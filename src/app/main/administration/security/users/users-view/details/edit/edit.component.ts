import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../../shared/shared.service';
import { UsersService } from '../../../users.service';
import { DetailsService } from '../details.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
  providers: [DetailsService]
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public openEMISId;

  /* Subscriptions */
  private openEMISIdSub: Subscription;
  private userViewDetailsSub: Subscription;
  private genderOptionsSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private detailsService: DetailsService,
    private usersService: UsersService,
    private sharedService: SharedService
  ) {
    super({ router: router, activatedRoute: activatedRoute, pageEvent: pageEvent });
    super.setPageTitle('Security - Users', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.openEMISIdSub = this.usersService.openEmisIdChanged.subscribe((data) => {
      if (data) {
        this.openEMISId = data;
        this.detailsService.getUsersEditDetails(this.openEMISId);
        this.userViewDetailsSub = this.detailsService.userEditDetailsChanged.subscribe((data) => {
          if (data) {
            this.setDetails(data[0]);
          }
        });
      } else {
        this.router.navigate(['main/security/users/list']);
      }
    });
    this.setDropDownValue();
  }

  setDropDownValue() {
    timer(200).subscribe((): void => {
      this.detailsService.getGenderDropDownOptions('gender');
      this.initializeSubscriptions();
    });
  }

  initializeSubscriptions() {
    this.genderOptionsSub = this.detailsService.genderOptionsChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: '', value: '--select--' });
        this._updateView.setInputProperty('gender', 'options', tempOption);
      }
    });
  }

  setDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'status') {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i]['key']]['key']);
        } else if (this._questionBase[i].key == 'gender') {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i]['key']]['key']);
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }

      this.loading = false;
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields',
        tapToDismiss: true,
        showCloseButton: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = {
        openemis_no: this.openEMISId,
        first_name: event.first_name,
        middle_name: event.middle_name,
        third_name: event.third_name,
        last_name: event.last_name,
        gender_id: event.gender,
        date_of_birth: event.date_of_birth.text,
        status: event.status
      };
      this.detailsService.updateUserDetails(this.openEMISId, payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this.router.navigate(['main/security/users/view/details/view']);
  }

  ngOnDestroy() {
    if (this.openEMISIdSub) {
      this.openEMISIdSub.unsubscribe();
    }
    if (this.userViewDetailsSub) {
      this.userViewDetailsSub.unsubscribe();
    }
    if (this.genderOptionsSub) {
      this.genderOptionsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
