import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';
import { UsersService } from '../../../users.service';
import { DetailsService } from '../details.service';
import { VIEWNODE_INPUT } from './view.config';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  providers: [DetailsService]
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public openEMISId;

  /* Subscriptions */
  private openEMISIdSub: Subscription;
  private userViewDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private detailsService: DetailsService,
    private usersService: UsersService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Security - Users', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/security/users/list' },
      { type: 'edit', path: 'main/security/users/view/details/edit' }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.openEMISIdSub = this.usersService.openEmisIdChanged.subscribe((data) => {
      if (data) {
        this.openEMISId = data;
        this.detailsService.getUsersViewDetails(this.openEMISId);
        this.userViewDetailsSub = this.detailsService.userViewDetailsChanged.subscribe((data) => {
          if (data) {
            this.setDetails(data[0]);
          }
        });
      } else {
        this._router.navigate(['main/security/users/list']);
      }
    });
  }

  setDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'role') {
          let role = data.role.map((element) => {
            let roleData = {};
            roleData['security_group_name'] = element.security_group_name;
            roleData['security_role_name'] = element.security_role.security_role_name;
            return roleData;
          });

          this.api.setProperty(this._questionBase[i].key, 'row', role);
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ' '
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
      this.loading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.openEMISIdSub) {
      this.openEMISIdSub.unsubscribe();
    }
    if (this.userViewDetailsSub) {
      this.userViewDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
