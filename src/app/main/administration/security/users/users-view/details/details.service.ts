import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { UsersService } from '../../users.service';
import { UsersViewService } from '../users-view.service';

@Injectable()
export class DetailsService {
  private userViewDetails: any = [];
  public userViewDetailsChanged = new Subject<any>();
  private userEditDetails: any = [];
  public userEditDetailsChanged = new Subject<any>();
  private genderOptions: any;
  public genderOptionsChanged = new Subject();
  constructor(
    private router: Router,
    private sharedService: SharedService,
    private usersService: UsersService,
    private usersViewService: UsersViewService
  ) {}

  /*** VIEW PAGE | Get user details for populating the view fields. ***/
  getUsersViewDetails(openemis_id) {
    this.usersViewService.getDetailsView(openemis_id).subscribe(
      (res: any) => {
        if (res.data) {
          this.userViewDetails = res.data;
          this.userViewDetailsChanged.next([...this.userViewDetails]);
        }
      },
      (err) => {
        this.userViewDetails = [];
        this.userViewDetailsChanged.next([...this.userViewDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Details Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/security/users/list']);
      }
    );
  }

  /*** EDIT PAGE | Get user details for populating the view fields. ***/
  getUsersEditDetails(openemis_id) {
    this.usersViewService.getDetailsEdit(openemis_id).subscribe(
      (res: any) => {
        if (res.data) {
          this.userEditDetails = res.data;
          this.userEditDetailsChanged.next([...this.userEditDetails]);
        }
      },
      (err) => {
        this.userEditDetails = [];
        this.userEditDetailsChanged.next([...this.userEditDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Details Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/security/users/list']);
      }
    );
  }

  /** Get Gender DropDown Options**/
  getGenderDropDownOptions(entities) {
    this.usersService.getGenderDropDownOptions(entities).subscribe(
      (res: any) => {
        this.genderOptions = res.data.gender_id;
        this.genderOptionsChanged.next([...this.genderOptions]);
      },
      (err) => {
        this.genderOptionsChanged.next({ ...this.genderOptions });
      }
    );
  }

  /**  DETAILS | EDIT   **/
  updateUserDetails(openemis_id, data) {
    this.usersViewService.updateUserDetails(openemis_id, data).subscribe(
      (res) => {
        let toasterConfig: any = {
          type: 'success',
          title: 'Success',
          body: 'User details updated',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/security/users/list']);
      },
      (err) => {
        let toasterConfig: any = {
          type: 'error',
          title: 'Failed',
          body: 'Error updating user details.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }
}
