import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsRoutingModule } from './details-routing.module';
import { SharedModule } from '../../../../../../shared/shared.module';
import { EditComponent } from './edit/edit.component';
import { ViewComponent } from './view/view.component';
import { DetailsService } from './details.service';

@NgModule({
  imports: [CommonModule, DetailsRoutingModule, SharedModule],
  declarations: [EditComponent, ViewComponent],
  providers: [DetailsService]
})
export class DetailsModule {}
