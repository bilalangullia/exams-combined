export const QUESTION_BASE: any = [
  {
    key: 'openemis_no',
    label: 'OpenEMIS ID',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'middle_name',
    label: 'Middle Name',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'third_name',
    label: 'Third Name',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'gender',
    label: 'Gender',
    visible: true,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select--'
      }
    ],
    events: true
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    visible: true,
    controlType: 'date',
    type: 'date'
  },
  {
    key: 'status',
    label: 'Status',
    visible: true,
    controlType: 'dropdown',
    options: [
      { key: 0, value: 'Inactive' },
      { key: 1, value: 'Active' }
    ],
    events: true
  },
  {
    key: 'nationality',
    label: 'Nationality',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'identity_type',
    label: 'Identity Type',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'identity_number',
    label: 'Identity Number',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'email',
    label: 'Email',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  }
];

export const FORM_BUTTONS = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-check'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
