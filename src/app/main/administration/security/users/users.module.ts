import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { UsersDataService } from './users-data.service';
import { UsersService } from './users.service';

@NgModule({
  imports: [CommonModule, UsersRoutingModule, SharedModule],
  declarations: [],
  providers: [UsersDataService, UsersService]
})
export class UsersModule {}
