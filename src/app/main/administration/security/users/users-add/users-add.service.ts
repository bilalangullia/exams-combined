import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { UsersService } from '../users.service';

@Injectable()
export class UsersAddService {
  private addOptions: any;
  public addOptionsChanged = new Subject();
  private openEmisId: any;
  public openEmisIdChanged = new Subject();
  constructor(private router: Router, private usersService: UsersService, private sharedService: SharedService) {}

  /** Get  DropDowns**/
  getDropdownOptions(entities: Array<string>) {
    this.usersService.getDropdownOptions(entities).subscribe(
      (res: any) => {
        this.addOptions = { ...this.addOptions, ...res['data'] };
        this.addOptionsChanged.next({ ...this.addOptions });
      },
      (err) => {
        this.addOptionsChanged.next({ ...this.addOptions });
      }
    );
  }

  /** Get  OpenEMIS Id**/
  generateEmisID() {
    this.usersService.generateOpenEmisID().subscribe(
      (res: any) => {
        this.openEmisId = res['data'];
        this.openEmisIdChanged.next({ ...this.openEmisId });
      },
      (err) => {
        this.openEmisId = {};
        this.openEmisIdChanged.next({ ...this.openEmisId });
      }
    );
  }

  /**  ADD New User Details  **/
  addUserDetails(payload) {
    this.usersService.addUser(payload).subscribe(
      (res) => {
        let toasterConfig: any = {
          type: 'success',
          title: 'Success',
          body: 'Details Saved',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/security/users/list']);
      },
      (err) => {
        let toasterConfig: any = {
          type: 'error',
          title: 'Failed',
          body: 'Error adding user details.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }
}
