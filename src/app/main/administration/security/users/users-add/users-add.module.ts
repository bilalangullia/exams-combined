import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersAddRoutingModule } from './users-add-routing.module';
import { AddComponent } from './add/add.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { UsersAddService } from './users-add.service';

@NgModule({
  imports: [CommonModule, UsersAddRoutingModule, SharedModule],
  declarations: [AddComponent],
  providers: [UsersAddService]
})
export class UsersAddModule {}
