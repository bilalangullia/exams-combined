export const QUESTION_BASE: Array<any> = [
  {
    key: 'openemis_no',
    label: 'OpenEMIS ID',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    required: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'middle_name',
    label: 'Middle Name',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'third_name',
    label: 'Third Name',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    required: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'gender_id',
    label: 'Gender',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- select --' }],
    events: true
  },
  {
    key: 'date_of_birth',
    label: 'Date of birth',
    visible: true,
    controlType: 'date',
    required: true
  },
  {
    key: 'status',
    label: 'Status',
    visible: true,
    controlType: 'dropdown',
    options: [
      { key: null, value: '-- select --' },
      { key: 0, value: 'Inactive' },
      { key: 1, value: 'Active' }
    ],
    events: true
  },
  {
    key: 'username',
    label: 'Username',
    visible: true,
    required: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'password',
    label: 'Password',
    visible: true,
    required: true,
    controlType: 'password',
    type: 'string'
  },
  {
    key: 'nationality_id',
    label: 'Nationality',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- select --' }],
    events: true
  },
  {
    key: 'identity_type_id',
    label: 'Identity Type',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- select --' }],
    events: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];

export interface IAddOptions {
  gender_id?: Array<Object>;
  nationality_id?: Array<Object>;
  identity_type_id?: Array<Object>;
}
