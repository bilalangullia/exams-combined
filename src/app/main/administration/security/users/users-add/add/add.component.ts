import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdView,
  IPageheaderApi,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IDynamicFormApi
} from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { UsersAddService } from '../users-add.service';
import { QUESTION_BASE, FORM_BUTTONS, IAddOptions } from './add.config';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [UsersAddService]
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public pageheader: IPageheaderConfig;
  public _pageHeaderApi: IPageheaderApi = {};
  public api: IDynamicFormApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };

  public userName: any;
  public openEmisId: any;

  /* Subscriptions */
  private addOptionsSub: Subscription;
  private openEmisIDSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private usersAddService: UsersAddService,
    private sharedService: SharedService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: pageEvent });
    super.setPageTitle('Security - Users', false);

    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.setDropDownValue();
    this.generateOpenEmisID();
  }

  generateOpenEmisID() {
    this.usersAddService.generateEmisID();
    this.openEmisIDSub = this.usersAddService.openEmisIdChanged.subscribe((res) => {
      if (res) {
        (this.userName = res['username']), (this.openEmisId = res['openemis_no']);
        this.setUserDetails(res);
      } else {
        this._router.navigate(['main/security/users/list']);
      }
    });
  }

  setDropDownValue() {
    timer(200).subscribe((): void => {
      this.usersAddService.getDropdownOptions(['gender', 'nationality', 'identityTypes']);
      this.initializeSubscriptions();
    });
  }

  setUserDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        this.api.setProperty(
          this._questionBase[i].key,
          'value',
          !data[this._questionBase[i].key]
            ? ''
            : data[this._questionBase[i].key].value
            ? data[this._questionBase[i].key].value
            : data[this._questionBase[i].key]
        );
      }
      this.loading = false;
    });
  }

  initializeSubscriptions() {
    this.addOptionsSub = this.usersAddService.addOptionsChanged.subscribe((options: IAddOptions) => {
      if (options && options['gender_id'] && options['nationality_id'] && options['identity_type_id']) {
        setTimeout(() => {
          this.setOptions(options);
        }, 0);
      } else {
        this.loading = false;
      }
    });
  }

  setOptions(options: IAddOptions) {
    this._questionBase.forEach((question) => {
      if (question['controlType'] === 'dropdown' && question['key'] !== 'status') {
        this.api.setProperty(
          question['key'],
          'options',
          options[question['key']]
            ? [
                { key: null, value: '-- select --' },
                ...options[question['key']].map((item) => ({ key: item['id'], value: item['name'] }))
              ]
            : [{ key: null, value: '-- select --' }]
        );
      } else if (question['key'] == 'status') {
        this.api.setProperty(question['key'], 'options', [
          { key: null, value: '-- select --' },
          { key: 0, value: 'Inactive' },
          { key: 1, value: 'Active' }
        ]);
      }
    });
  }

  submitVal(event) {
    console.log(event);

    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields',
        tapToDismiss: true,
        showCloseButton: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = {
        first_name: event.first_name,
        last_name: event.last_name,
        gender_id: event.gender_id,
        username: event.username,
        password: event.password,
        nationality_id: event.nationality_id,
        identity_type_id: event.identity_type_id,
        date_of_birth: event.date_of_birth,
        status: event.status,
        openemis_no: event.openemis_no
      };
      this.usersAddService.addUserDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/security/users/list']);
  }

  ngOnDestroy(): void {
    if (this.openEmisIDSub) {
      this.openEmisIDSub.unsubscribe();
    }
    if (this.addOptionsSub) {
      this.addOptionsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
