import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'list' },
  { path: 'list', loadChildren: './users-list/users-list.module#UsersListModule' },
  { path: 'add', loadChildren: './users-add/users-add.module#UsersAddModule' },
  { path: 'view', loadChildren: './users-view/users-view.module#UsersViewModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
