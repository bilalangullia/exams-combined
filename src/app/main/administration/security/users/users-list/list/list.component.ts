import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  ITableApi,
  ITableConfig,
  ITableColumn,
  KdPageBaseEvent,
  KdAlertEvent,
  KdTableEvent,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IPageheaderApi,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';
import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { DataService } from '../../../../../../shared/data.service';
import { UsersService } from '../../users.service';
import { UsersListService } from '../users-list.service';
import { TABLECOLUMN } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [UsersListService, KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listTable';
  public PAGESIZE: number = 20;
  public _row: Array<any>;
  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public pageheader: IPageheaderConfig;
  public _pageHeaderApi: IPageheaderApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };

  private tableEventList: any;
  public showTable: boolean = false;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /*   SUBSCRIPTION */
  private tableEventSub: Subscription;
  private usersListSub: Subscription;

  public invalidToasterConfig = {
    type: 'error',
    title: 'Invalid/Missing Examination ID',
    showCloseButton: true,
    tapToDismiss: true,
    timeout: 3000
  };

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    },
    action: {
      enabled: true,
      list: [
        {
          icon: 'far fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            if (_rowNode['data']['openemis_no']) {
              this.usersService.setOpenEmisId(_rowNode['data']['openemis_no']);
              this.usersService.setUserName(_rowNode['data']['username']);
              timer(200).subscribe(() => {
                this.router.navigate(['main/security/users/view/details/view']);
              });
            } else {
              this.sharedService.setToaster(this.invalidToasterConfig);
            }
          }
        }
      ]
    }
  };

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.OpenEMISID,
    TABLECOLUMN.Name,
    TABLECOLUMN.Status,
    TABLECOLUMN.UserName,
    TABLECOLUMN.IdentityNumber,
    TABLECOLUMN.Email
  ];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    private usersService: UsersService,
    private usersListService: UsersListService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    super.setPageTitle('Security - Users', false);
    super.setToolbarMainBtns([
      {
        type: 'add',
        path: 'main/security/users/add'
      }
    ]);

    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;
    this.showTable = true;

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      listReq = this.usersListService.getUsersListSearch(searchKey, fetchParams['startRow'], fetchParams['endRow']);
    } else {
      listReq = this.usersListService.getUsersList(fetchParams['startRow'], fetchParams['endRow']);
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['userRecords'] && res['data']['userRecords'].length) {
          list = res['data']['userRecords'];
          total =
            this.searchKey && this.searchKey.length ? res['data']['userRecords'].length : res['data']['totalRecords'];
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }
  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue = [];
      }
    }
  }

  ngOnDestroy(): void {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.usersListSub) {
      this.usersListSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
