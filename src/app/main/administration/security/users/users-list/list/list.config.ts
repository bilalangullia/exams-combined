interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  OpenEMISID?: Column;
  Name?: Column;
  Status?: Column;
  UserName?: Column;
  IdentityNumber?: Column;
  Email?: Column;
}

export const TABLECOLUMN: ListColumn = {
  OpenEMISID: {
    headerName: 'OpenEMIS ID',
    field: 'openemis_no',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Status: {
    headerName: 'Status',
    field: 'status',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  UserName: {
    headerName: 'User Name',
    field: 'username',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  IdentityNumber: {
    headerName: 'Identity Number',
    field: 'identity_number',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Email: {
    headerName: 'Email',
    field: 'email',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};
