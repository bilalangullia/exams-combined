import { Injectable } from '@angular/core';

import { UsersService } from '../users.service';

@Injectable()
export class UsersListService {
  constructor(private userService: UsersService) {}

  /*** USERS  List Data ***/
  getUsersList(start?: number, end?: number) {
    return this.userService.getUsersList(start, end);
  }

  /*** Users List Data | SEARCH***/
  getUsersListSearch(keyword: string, start?: number, end?: number) {
    return this.userService.getUsersListSearch(keyword, start, end);
  }
}
