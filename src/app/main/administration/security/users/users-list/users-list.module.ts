import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersListRoutingModule } from './users-list-routing.module';
import { ListComponent } from './list/list.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { UsersListService } from './users-list.service';

@NgModule({
  imports: [CommonModule, UsersListRoutingModule, SharedModule],
  declarations: [ListComponent],
  providers: [UsersListService]
})
export class UsersListModule {}
