import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { AdministrationRoutingModule } from './administration-routing.module';

@NgModule({
  imports: [CommonModule, AdministrationRoutingModule, SharedModule],
  declarations: []
})
export class AdministrationModule {}
