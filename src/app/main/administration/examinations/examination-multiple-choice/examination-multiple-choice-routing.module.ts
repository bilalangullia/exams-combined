import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MultipleChoiceEntryComponent } from './multiple-choice-entry/multiple-choice-entry.component';
import { MultipleChoiceListComponent } from './multiple-choice-entry/multiple-choice-list/multiple-choice-list.component';
import { MultipleChoiceEditComponent } from './multiple-choice-entry/multiple-choice-edit/multiple-choice-edit.component';
import { MultipleChoiceViewComponent } from './multiple-choice-entry/multiple-choice-view/multiple-choice-view.component';

const routes: Routes = [
  {
    path: '',
    component: MultipleChoiceEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: MultipleChoiceListComponent },
      { path: 'edit', component: MultipleChoiceEditComponent },
      { path: 'view', component: MultipleChoiceViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExaminationMultipleChoiceRoutingModule {}
