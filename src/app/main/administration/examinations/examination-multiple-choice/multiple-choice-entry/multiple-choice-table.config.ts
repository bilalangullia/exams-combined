interface IResponsesTableColumns {
  question: any;
  answer: any;
}

export const DEFAULT_RESPONSE_OPTIONS = [
  { key: '', value: '-- Select --' },
  { key: 1, value: 'A' },
  { key: 2, value: 'B' },
  { key: 3, value: 'C' },
  { key: 4, value: 'D' }
];

const QUESTION_NUMBER = {
  headerName: 'Question No.',
  field: 'question',
  type: 'input',
  config: {
    input: {
      controlType: 'text',
      key: 'question',
      visible: true
    }
  }
};

const RESPONSE_INPUT_DROPDOWN = {
  headerName: 'Answer',
  field: 'answer',
  visible: true,
  type: 'input',
  config: {
    input: [
      {
        controlType: 'dropdown',
        key: 'answer',
        visible: true,
        options: DEFAULT_RESPONSE_OPTIONS
      }
    ]
  }
};

export const TABLE_COLUMN_LIST: IResponsesTableColumns = {
  question: QUESTION_NUMBER,
  answer: RESPONSE_INPUT_DROPDOWN
};
