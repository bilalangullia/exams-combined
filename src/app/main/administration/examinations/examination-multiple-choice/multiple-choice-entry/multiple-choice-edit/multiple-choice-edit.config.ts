import { TABLE_COLUMN_LIST } from '../multiple-choice-table.config';
import { ITableActionApi } from 'openemis-styleguide-lib';

interface TableQuestionFormatInterface {
  id?: string;
  questionKey?: string;
  question?: {
    key?: string;
    label?: string;
    required?: boolean;
    visible?: boolean;
    controlType?: string;
    [key: string]: any;
  };
}

export const CREATE_ROW1: (num_rows: number, baseIndex?: number) => Array<any> = (
  num_rows: number,
  baseIndex: number = 0
): Array<any> => {
  let row: Array<any> = [];
  for (let i: number = baseIndex; i < num_rows + baseIndex; i++) {
    let oneRow = { id: i + 1 };
    row.push(oneRow);
  }
  return row;
};

export const CREATE_ROW: (num_rows: number, baseIndex?: number, data?: Array<any>) => Array<any> = (
  num_rows: number,
  baseIndex: number = 0,
  data?: Array<any>
): Array<any> => {
  let row: Array<any> = [];

  for (let i: number = baseIndex; i < num_rows + baseIndex; i++) {
    let oneRow = {
      id: i + 1,
      question: { question: data[i].question },
      answer: { answer: data[i].answer }
    };

    row.push(oneRow);
  }
  return row;
};

export const RESPONSES_FORM_TABLE: TableQuestionFormatInterface = {
  id: 'responseTable',
  questionKey: 'responseTable',
  question: {
    key: 'answers',
    label: 'Answer',
    visible: true,
    required: false,
    controlType: 'table',
    row: [],
    column: [TABLE_COLUMN_LIST.question, TABLE_COLUMN_LIST.answer],
    config: {
      rowId: 'id',
      id: 'answers',
      gridHeight: 300,
      loadType: 'normal',
      action: {
        enabled: true,
        list: [
          {
            type: 'delete',
            callback: (node: any, tableApi: ITableActionApi): void => {
              tableApi.deleteThisRow();
            }
          }
        ]
      },
      paginationConfig: {
        pagesize: 10
      },
      button: [
        {
          label: 'Insert row',
          icon: 'fa fa-plus',
          type: 'insert',
          callback: (): void => {}
        }
      ]
    }
  }
};

export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'option',
    label: 'Option',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'component',
    label: 'Component',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  RESPONSES_FORM_TABLE.question
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
