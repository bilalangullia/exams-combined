import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, ITableFormUpdateParams } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../../shared/shared.toasters';
import { ExaminationMultipleChoiceService } from '../../examination-multiple-choice.service';
import { DEFAULT_RESPONSE_OPTIONS } from '../multiple-choice-table.config';
import { CREATE_ROW, QUESTION_BASE, FORM_BUTTONS, CREATE_ROW1 } from './multiple-choice-edit.config';

@Component({
  selector: 'app-multiple-choice-edit',
  templateUrl: './multiple-choice-edit.component.html',
  styleUrls: ['./multiple-choice-edit.component.css']
})
export class MultipleChoiceEditComponent extends KdPageBase implements OnInit, OnDestroy {
  public _formButtons: any = FORM_BUTTONS;
  public _questionBase: any = QUESTION_BASE;
  public defaultOptions: any = DEFAULT_RESPONSE_OPTIONS;
  public api: IDynamicFormApi = {};
  private academicPeriod: any;
  private examinationId: any;
  public componentId;
  public loading: boolean = true;
  private multipleChoiceQuestions: Array<any> = [];
  private answerKeyMap: Object = { '1': 'A', '2': 'B', '3': 'C', '4': 'D' };
  private answersLength = 0;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private componentIdSub: Subscription;
  private componentEditDetailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private sharedService: SharedService,
    private multipleChoiceService: ExaminationMultipleChoiceService
  ) {
    super({ router, pageEvent, activatedRoute });
  }

  ngOnInit() {
    this.loading = false;
    super.setPageTitle('Examination - Multiple Choices', false);
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.filterValuesSub = this.multipleChoiceService.currentFiltersChanged.subscribe((filters) => {
      if (filters['academic_period_id'] && filters['examination_id']) {
        if ((filters['academic_period_id'] && filters['examination_id']) !== '') {
          this.academicPeriod = filters['academic_period_id'];
          this.examinationId = filters['examination_id'];
        } else {
          this.router.navigate(['main/examinations/multiple-choice/list']);
        }
      }
    });

    this.componentIdSub = this.multipleChoiceService.componentIdChanged.subscribe((componentId) => {
      if (!componentId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this.router.navigate(['main/examinations/multiple-choice/list']);
        }, 0);
      } else {
        this.componentId = componentId;
        this.multipleChoiceService.getEditDetails(this.componentId);
      }
    });

    this.componentEditDetailsSub = this.multipleChoiceService.editDetailsChanged.subscribe((data) => {
      if (data.length) {
        this.setComponentDetails({
          ...data[0],
          academic_period: this.academicPeriod,
          examination_id: this.examinationId
        });
      }
    });
  }

  buttonClicked(event) {
    if (event.formKey == 'answers') this.insertResponsesRow(event.formKey, 1);
  }

  insertResponsesRow(formKey, num_rows = 1) {
    this.api.table(formKey).addRow(CREATE_ROW1(num_rows, this.answersLength), true);
    this.multipleChoiceQuestions.forEach((item, index) => {
      let rowReponse: ITableFormUpdateParams = {
        questionKey: 'answers',
        colId: 'answer',
        rowId: index + 1,
        property: 'value',
        data: 0
      };
      this.answersLength++;
      this.api.table('responseTable').setTableCellInputProperty(rowReponse);
    });
  }

  setComponentDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'option') {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            data['option_code'] && data['option_name'] ? `${data['option_code']} - ${data['option_name']}` : 'NA'
          );
        } else if (data.answers && data.answers.length && this._questionBase[i].key == 'answers') {
          data.answers.sort((a, b) => a.question - b.question);
          this.answersLength = data.answers.length;
          this.api.table('answers').addRow(CREATE_ROW(data.answers.length, 0, data.answers), true);
          data.answers.forEach((answer, index) => {
            let rowReponse: ITableFormUpdateParams = {
              questionKey: 'answer',
              colId: 'answer',
              rowId: index + 1,
              property: 'value',
              data: this.defaultOptions.find((option) => {
                return option.value == answer.answer;
              }).key
            };

            this.api.table('answers').setTableCellInputProperty(rowReponse);
          });
        } else if (this._questionBase[i].key == 'component') {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            data['component_code'] && data['component_name']
              ? `${data['component_code']} - ${data['component_name']}`
              : 'NA'
          );
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ' '
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
  }

  cancel() {
    this.router.navigate(['main/examinations/multiple-choice/view']);
  }

  submitForm(event) {
    let payload = {};
    let responses = event.answers;
    if (responses.length) {
      payload = {
        response: responses.map((item) => ({
          answer: this.answerKeyMap[item['answer']['answer']],
          question: item['question']['question'],
          examination_components_id: this.componentId
        }))
      };
    }
    if (Object.values(payload).length) {
      this.multipleChoiceService.submitResponses(this.componentId, payload);
    }
  }
  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.componentIdSub) {
      this.componentIdSub.unsubscribe();
    }
    if (this.componentEditDetailsSub) {
      this.componentEditDetailsSub.unsubscribe();
      this.multipleChoiceService.emptyEditDetails();
    }
    super.destroyPageBaseSub();
  }
}
