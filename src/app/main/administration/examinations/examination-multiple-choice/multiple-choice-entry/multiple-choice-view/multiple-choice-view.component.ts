import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { ExaminationMultipleChoiceService } from '../../examination-multiple-choice.service';
import { VIEWNODE_INPUT, IModalConfig } from './multiple-choice-view.config';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../../shared/shared.toasters';

@Component({
  selector: 'app-multiple-choice-view',
  templateUrl: './multiple-choice-view.component.html',
  styleUrls: ['./multiple-choice-view.component.css'],
  providers: [KdModalEvent]
})
export class MultipleChoiceViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private academicPeriod: any;
  private examinationId: any;
  public componentId;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private componentIdSub: Subscription;
  private componentDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Subject Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          if (this._modalEvent) {
            this._modalEvent.toggleClose();
          }
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _modalEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private sharedService: SharedService,
    private multipleChoiceService: ExaminationMultipleChoiceService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Examinations - Multiple Choices', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/examinations/multiple-choice/list' },
      { type: 'edit', path: 'main/examinations/multiple-choice/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.filterValuesSub = this.multipleChoiceService.currentFiltersChanged.subscribe((filters) => {
      if (filters['academic_period_id'] && filters['examination_id']) {
        if ((filters['academic_period_id'] && filters['examination_id']) !== '') {
          this.academicPeriod = filters['academic_period_id'];
          this.examinationId = filters['examination_id'];
        } else {
          this._router.navigate(['main/examinations/multiple-choice/list']);
        }
      }
    });

    this.componentIdSub = this.multipleChoiceService.componentIdChanged.subscribe((componentId) => {
      if (!componentId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/examinations/multiple-choice/list']);
        }, 0);
      } else {
        this.componentId = componentId;
        this.multipleChoiceService.getViewDetails(this.componentId);
      }
    });

    this.componentDetailsSub = this.multipleChoiceService.viewDetailsChanged.subscribe((data) => {
      if (data.length) {
        data[0].answers.sort((a, b) => a.question - b.question);
        this.setComponentDetails({
          ...data[0],
          academic_period: this.academicPeriod,
          examination_id: this.examinationId
        });
      }
    });
  }

  setComponentDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'answers') {
          this.api.setProperty(this._questionBase[i].key, 'row', data.answers);
        }
        if (this._questionBase[i].key == 'option') {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            data['option_code'] && data['option_name'] ? `${data['option_code']} - ${data['option_name']}` : 'NA'
          );
        } else if (this._questionBase[i].key == 'component') {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            data['component_code'] && data['component_name']
              ? `${data['component_code']} - ${data['component_name']}`
              : 'NA'
          );
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ' '
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
    this.loading = false;
  }

  open() {
    if (this._modalEvent) {
      this._modalEvent.toggleOpen();
    }
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.componentIdSub) {
      this.componentIdSub.unsubscribe();
    }
    if (this.componentDetailsSub) {
      this.componentDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
