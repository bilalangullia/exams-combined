export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'option',
    label: 'Option',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'component',
    label: 'Component',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'answers',
    label: 'Answers',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Question',
        field: 'question',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Answers',
        field: 'answer',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'options',
      rowIdKey: 'id',
      gridHeight: 250,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_user_id',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_user_id',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];
export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
