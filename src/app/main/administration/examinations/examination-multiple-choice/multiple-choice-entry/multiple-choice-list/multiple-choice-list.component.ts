import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ExcelExportParams } from 'ag-grid';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdFilter,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  IPageheaderApi,
  IPageheaderConfig,
  IDynamicFormApi,
  ITableApi
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { PAGE_SIZE } from '../../../../../../shared/shared.constants';
import { ExaminationMultipleChoiceService } from '../../examination-multiple-choice.service';
import { FILTER_INPUTS, TABLECOLUMN } from './multiple-choice-list.config';

@Component({
  selector: 'app-multiple-choice-list',
  templateUrl: './multiple-choice-list.component.html',
  styleUrls: ['./multiple-choice-list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class MultipleChoiceListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = {};
  public inputs = FILTER_INPUTS;
  public api: IDynamicFormApi = {};
  public defaultList = {};
  private _question1Val: string;
  public academic_period_id: any;
  public examination_id: any;
  public itemFind = {};
  public _questionBase: any = TABLECOLUMN;
  public _tableApi: ITableApi = {};

  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  public isSearch: boolean = false;
  public _row: Array<any>;
  public loading: boolean = true;
  public viewTable: boolean = false;
  public initYear: any;
  public initExamination: any;

  /* Subscriptions */
  private yearFilterSub: Subscription;
  private examFilterSub: Subscription;
  private currentFilterSub: Subscription;
  private listSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    private sharedService: SharedService,
    private multipleChoiceService: ExaminationMultipleChoiceService
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: { pagesize: PAGE_SIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.multipleChoiceService.setComponentId(_rowNode.data.id);
            this.router.navigate(['main/examinations/multiple-choice/view']);
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.Option, TABLECOLUMN.Component];

  ngOnInit() {
    this.loading = false;
    super.setPageTitle('Examinations - Multiple Choices', false);
    super.setToolbarMainBtns([
      { type: 'import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'multipleChoiceSubjects_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });

    super.updatePageHeader();
    super.updateBreadcrumb();
    this.multipleChoiceService.emptyCurrentFilters();
    this.setDropDownValue();

    this.currentFilterSub = this.multipleChoiceService.currentFiltersChanged.subscribe((filters) => {
      if (filters['academic_period_id'] && filters['examination_id']) {
        if ((filters['academic_period_id'] && filters['examination_id']) !== '') {
          this.academic_period_id = filters['academic_period_id']['key'];
          this.examination_id = filters['examination_id']['key'];
          this.multipleChoiceService.getMultipleChoiceList(
            filters['examination_id']['key'],
            filters['academic_period_id']['key']
          );
        }
      }
    });

    this.listSub = this.multipleChoiceService.listChanged.subscribe((list: Array<any>) => {
      if (list['length']) {
        this.populateTable(list);
      } else {
        this.viewTable = false;
      }
    });
  }

  setDropDownValue() {
    timer(100).subscribe((): void => {
      this.multipleChoiceService.getFilterYear();
      this.initializeSubscriptions();
    });
  }

  initializeSubscriptions() {
    this.yearFilterSub = this.multipleChoiceService.listFilterYearChanged.subscribe((data: any) => {
      if (data) {
        let tempOption = [];
        this.initYear = data[0].id;
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.name
          });
        });

        this._updateView.setInputProperty('academic_period_id', 'options', tempOption);
        this._updateView.setInputProperty('academic_period_id', 'value', tempOption[0].key);
        timer(100).subscribe((): void => {
          this.multipleChoiceService.getFilterExam(this.initYear);
        });
      } else {
        this._updateView.setInputProperty('academic_period_id', 'options', [{ key: '', value: '-- Select --' }]);
      }
    });

    this.examFilterSub = this.multipleChoiceService.listFilterExamChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        this.initExamination = data[0].id;
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.name
          });
        });
        this._updateView.setInputProperty('examination_id', 'options', tempOption);
        this._updateView.setInputProperty('examination_id', 'value', tempOption[0].key);
        // timer(100).subscribe((): void => {
        //   this.multipleChoiceService.getMultipleChoiceList(this.initExamination, this.initYear);
        // });
      } else {
        this._updateView.setInputProperty('examination_id', 'options', [{ key: '', value: '-- Select--' }]);
      }
    });
  }

  populateTable(list: Array<any>) {
    if (list.length) {
      this.loading = false;
      this.viewTable = true;
      let temp = list.map((item) => {
        return { component: ` ${item.component_code} -  ${item.component_name} `, option: item.option, id: item.id };
      });
      this._row = temp;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < list.length; j++) {
            if (this.tableColumns[i]['field'] == 'component') {
              this.tableColumns[i].filterValue.push(temp[j].component);
            }
            if (list[j][this.tableColumns[i]['field']] && list[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(list[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(list[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(list[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
    }
  }

  public detectValue(question: any): void {
    this.defaultList[question.key] = question.value;
    let _defaultOptions: Array<any> = [{ key: '', value: '-- Select --' }];
    let newOptions: Array<any> = [
      {
        key: '',
        value: '-- Select From ' + this._question1Val + '-' + question.value + ' --'
      },
      {
        key: '1',
        value: 'Options ' + this._question1Val + '-' + question.value + '-1'
      },
      {
        key: '2',
        value: 'Options ' + this._question1Val + '-' + question.value + '-2'
      }
    ];
    if (question.key === 'academic_period_id') {
      this.academic_period_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);

      if (question['value']) {
        this.multipleChoiceService.setCurrentFilters('academic_period_id', {
          ...this.itemFind
        });

        this._question1Val = '';
        this._question1Val = question.value;
        timer(100).subscribe((): void => {
          this.multipleChoiceService.getFilterExam(this.academic_period_id);
        });
      } else {
        newOptions = _defaultOptions.slice();
        this.multipleChoiceService.setCurrentFilters('academic_period_id', {});
      }
    } else if (question.key === 'examination_id') {
      this._question1Val = '';
      this.examination_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.multipleChoiceService.setCurrentFilters('examination_id', {
          ...this.itemFind
        });
        this._question1Val = question.value;
      } else {
        newOptions = _defaultOptions.slice();
        this.multipleChoiceService.setCurrentFilters('examination_id', {});
      }
    }
  }

  searchList(keyword: string) {
    if (keyword && keyword.length > 2) {
      this.multipleChoiceService.getMultipleChoiceListSearch(this.academic_period_id, this.examination_id, keyword);
    } else if (!keyword) {
      this.multipleChoiceService.getMultipleChoiceListSearch(this.academic_period_id, this.examination_id, '');
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.yearFilterSub) {
      this.yearFilterSub.unsubscribe();
    }
    if (this.examFilterSub) {
      this.examFilterSub.unsubscribe();
    }
    if (this.currentFilterSub) {
      this.currentFilterSub.unsubscribe();
    }
    if (this.listSub) {
      this.listSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
