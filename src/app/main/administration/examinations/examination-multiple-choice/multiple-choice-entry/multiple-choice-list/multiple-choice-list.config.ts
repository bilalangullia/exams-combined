export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: 0,
        value: '-- Select Options 1 --'
      }
    ],
    events: true
  },
  {
    key: 'examination_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: '',
        value: '-- Select --'
      }
    ],
    events: true
  }
];
interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Option?: Column;
  Component?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Option: {
    headerName: 'Option',
    field: 'option',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Component: {
    headerName: 'Component',
    field: 'component',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
