import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class ExaminationMultipleChoiceDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /** MULTIPLE CHOICE LIST**/
  getList(examination_id, academic_period_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentList}/${examination_id}?academic_period_id=${academic_period_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** MULTIPLE CHOICE VIEW**/
  getDetails(component_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentView}/${component_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** MULTIPLE CHOICE EDIT**/
  getEditDetails(component_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentEdit}/${component_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT PAGE | Submit Reponses ****/
  submitResponses(component_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/multiple-choice/update-response/${component_id}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  SEARCH | API  **/
  getListSearch(academic_period_id, examId, keyword) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentList}/${examId}?academic_period_id=${academic_period_id}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
