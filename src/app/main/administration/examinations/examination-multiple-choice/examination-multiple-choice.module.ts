import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { ExaminationMultipleChoiceRoutingModule } from './examination-multiple-choice-routing.module';
import { MultipleChoiceEntryComponent } from './multiple-choice-entry/multiple-choice-entry.component';
import { MultipleChoiceListComponent } from './multiple-choice-entry/multiple-choice-list/multiple-choice-list.component';
import { MultipleChoiceEditComponent } from './multiple-choice-entry/multiple-choice-edit/multiple-choice-edit.component';
import { MultipleChoiceViewComponent } from './multiple-choice-entry/multiple-choice-view/multiple-choice-view.component';
import { ExaminationMultipleChoiceService } from './examination-multiple-choice.service';
import { ExaminationMultipleChoiceDataService } from './examination-multiple-choice-data.service';

@NgModule({
  imports: [CommonModule, SharedModule, ExaminationMultipleChoiceRoutingModule],
  declarations: [
    MultipleChoiceEntryComponent,
    MultipleChoiceListComponent,
    MultipleChoiceEditComponent,
    MultipleChoiceViewComponent
  ],
  providers: [ExaminationMultipleChoiceService, ExaminationMultipleChoiceDataService]
})
export class ExaminationMultipleChoiceModule {}
