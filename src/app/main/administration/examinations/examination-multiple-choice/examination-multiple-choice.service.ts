import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, timer } from 'rxjs';
import { Router } from '@angular/router';

import { DataService } from '../../../../shared/data.service';
import { SharedService } from '../../../../shared/shared.service';
import { detailsNotFound, saveFail, updateSuccess } from '../../../../shared/shared.toasters';
import { ExaminationMultipleChoiceDataService } from './examination-multiple-choice-data.service';

@Injectable()
export class ExaminationMultipleChoiceService {
  private componentId: number;
  private list: any;
  private listFilterYear: any;
  private listFilterExam: any;
  private currentFilters: any = {};
  private viewDetails: any = [];
  private editDetails: any = [];

  public componentIdChanged = new BehaviorSubject<number>(this.componentId);
  public listChanged = new Subject();
  public listFilterYearChanged = new Subject();
  public listFilterExamChanged = new Subject();
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  public viewDetailsChanged = new BehaviorSubject<any>(this.viewDetails);
  public editDetailsChanged = new BehaviorSubject<any>(this.editDetails);

  constructor(
    private router: Router,
    private dataService: DataService,
    private sharedService: SharedService,
    private examinationMultipleChoiceDataService: ExaminationMultipleChoiceDataService
  ) {}

  /*** SET COMPONENT ID ***/
  setComponentId(id: number) {
    this.componentId = id;
    this.componentIdChanged.next(this.componentId);
  }

  /*** LIST PAGE | FILTERS | Year***/
  getFilterYear() {
    let data = 'academicPeriods';
    this.dataService.getOptionsAcademicPeriod(data).subscribe((res: any) => {
      this.listFilterYear = res.data.academic_period_id;
      this.listFilterYearChanged.next([...this.listFilterYear]);
      (err) => {
        this.listFilterYear = [];
        this.listFilterYearChanged.next([...this.listFilterYear]);
      };
    });
  }

  /*** LIST PAGE | FILTERS | Exam Cert ***/
  getFilterExam(academic_id) {
    this.dataService.getOptionsExam(academic_id).subscribe(
      (res: any) => {
        if (res && res.data) {
          this.listFilterExam = res.data;
          this.listFilterExamChanged.next([...this.listFilterExam]);
        } else {
          this.listFilterExam = [];
          this.listFilterExamChanged.next([...this.listFilterExam]);
        }
      },
      (err) => {
        this.listFilterExam = [];
        this.listFilterExamChanged.next([...this.listFilterExam]);
      }
    );
  }

  /*** LIST PAGE | List Data ***/
  getMultipleChoiceList(examination_id, academic_period_id) {
    this.examinationMultipleChoiceDataService.getList(examination_id, academic_period_id).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data']['userRecords']) {
          this.list = res['data']['userRecords'];
          this.listChanged.next([...this.list]);
        } else {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([...this.list]);
      }
    );
  }

  /*** FEE  List Data | SEARCH***/
  getMultipleChoiceListSearch(academic_period_id, examId, keyword) {
    this.examinationMultipleChoiceDataService.getListSearch(academic_period_id, examId, keyword).subscribe(
      (res) => {
        if (res && res['data'] && res['data']['userRecords']) {
          this.list = res['data']['userRecords'];
          this.listChanged.next([...this.list]);
        } else {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([...this.list]);
      }
    );
  }

  /*** VIEW/EDIT PAGE | Get component details for populating the view fields. ***/
  getViewDetails(componentId) {
    this.examinationMultipleChoiceDataService.getDetails(componentId).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.viewDetails = res.data;
          this.viewDetailsChanged.next([...this.viewDetails]);
        } else {
          this.viewDetails = [];
          this.viewDetailsChanged.next([...this.viewDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.viewDetails = [];
        this.viewDetailsChanged.next([...this.viewDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE | Get component details for populating the edit fields. ***/
  getEditDetails(componentId) {
    this.examinationMultipleChoiceDataService.getEditDetails(componentId).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.editDetails = res.data;
          this.editDetailsChanged.next([...this.editDetails]);
        } else {
          this.editDetails = [];
          this.editDetailsChanged.next([...this.editDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.editDetails = [];
        this.editDetailsChanged.next([...this.editDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE | Empty Details ****/
  emptyEditDetails() {
    this.editDetails = [];
    this.editDetailsChanged.next([...this.editDetails]);
  }

  /*** EDIT PAGE | Submit Reponses ****/
  submitResponses(component_id, data) {
    this.examinationMultipleChoiceDataService.submitResponses(component_id, data).subscribe(
      (res) => {
        this.sharedService.setToaster(updateSuccess);
        timer(500).subscribe(() => {
          this.router.navigate(['main/examinations/multiple-choice/list']);
        });
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** SET Current filters ***/
  setCurrentFilters(key: string, value: Object) {
    this.currentFilters[key] = value;
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** FEE  Empty Current filters ***/
  emptyCurrentFilters() {
    this.currentFilters = {};
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }
}
