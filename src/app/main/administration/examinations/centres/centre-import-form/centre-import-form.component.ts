import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdAlertEvent } from 'openemis-styleguide-lib';
import { file_INPUT } from './import-form.config';
import { DataService } from '../../../../../shared/data.service';
import { SharedService } from '../../../../../shared/shared.service';
import {CentreService} from '../centre.service'
import { timer } from 'rxjs';

@Component({
  selector: 'app-centre-import-form',
  templateUrl: './centre-import-form.component.html',
  styleUrls: ['./centre-import-form.component.css']
})
export class CentreImportFormComponent extends KdPageBase implements OnInit, OnDestroy {

  public loading: boolean = true;
  public fileinput: Array<any> = [
    {
      'key': 'academic_period_id',
      'label': 'Academic Period',
      'visible': true,
      'required': true,
      'controlType': 'dropdown',
      'event': true,
      'order': 1,
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
    },
    {
      'key': 'examination_id',
      'label': 'Examination',
      'visible': true,
      'required': true,
      'event': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
    },
    file_INPUT.file_inputs,
  ];
  public button: Array<any> = [
    {
      name: 'Import',
      btnType: 'btn-text',
      type: 'submit',
      icon: 'kd-import',
      key: 'fileinput_double_buttons',
      controlType: 'file-input',
    },
    {
      name: 'Cancel',
      type: 'reset',
      class: 'btn-outline',
      icon: 'kd-cross',
      
    }
  ];
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  public defaultValues:any;
  constructor(
    _pageEvent: KdPageBaseEvent,
    public _router: Router,
    _activatedRoute: ActivatedRoute,
    public _KadAlert: KdAlertEvent,
    public dataService: DataService,
    public sharedService: SharedService,
    public centreService : CentreService
  ) {
    super({
      router: _router,
      pageEvent: _pageEvent,
      activatedRoute: _activatedRoute
    });
  }

  ngOnInit(): void {
    super.setPageTitle('Examinations ­ Centres', false);
    super.setToolbarMainBtns([]);

    super.updatePageHeader();
    super.updateBreadcrumb();

    this.sharedService.getDropdownValues().subscribe(
      (data: any) => {
        this.defaultValues = data;
        if (this.defaultValues) {
          let keyArr = Object.keys(this.defaultValues.data);
          keyArr.forEach((e) => {
            let quesObj = this.fileinput.find((q) => { return q.key == e })
            if (quesObj) {
              quesObj.options.push(...this.defaultValues.data[e].map(item => { return { key: item.id, value: item.name } }));
            }
          });
        }
        this.loading = false;
      }
    )
  }

  public detectValue(question: any): void {
    // console.log('CreateNode - Detect Value from question: ', question);
    this.formValue = question.value;
    if(question.key == 'academic_period_id'){
      this.getExamination(question.value);
    }
  }

  getExamination(id) {
    if(id != 'null'){
      this.dataService.getExamination(id).subscribe(
        (data: any) => {
          if(data.data.length>0){ 
          // this.formValue.examination_id=data.data[0].id
          let temp = [...data.data.map(item => { return { key: item.id, value: item.name } })]
          this.api.setProperty('examination_id', 'options', temp)
          }else{
            let temp = [{
              'key': 'null',
              'value': '--Select--'
            }]
            this.api.setProperty('examination_id', 'options', temp)
            let toasterConfig: any = {
              type: 'error',
              title: ' Examination not found for selected Academic Period',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        },
        err => {
          console.log(err);        
        }
      )
    }else{
      let temp = [{
        'key': 'null',
        'value': '--Select--'
      }]
      this.api.setProperty('examination_id', 'options', temp)
    }
  }
  public _buttonEvent(event: any): void {
    
    const formData = new FormData();
    formData.append('academic_period_id', event.academic_period_id);
    formData.append('examination_id',  event.examination_id);
    formData.append('template', event.fileinput_double_buttons);
    this.dataService.centreImportFile(formData).subscribe(res => {
      this.centreService.setImportDetail(res)
      timer(100).subscribe(()=>{this._router.navigate(['main/examinations/centres/detail'])})
    }, err => {
          let toasterConfig: any = {
        title: 'Something went Wrong',
        body: 'Import template required.',
        showCloseButton: true,
        tapToDismiss: true,
      };
      this._KadAlert.error(toasterConfig);
    })
  }
  reset(){
    this._router.navigate(['main/examinations/centres/list'])
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

}
