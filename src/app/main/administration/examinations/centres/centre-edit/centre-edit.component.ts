import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { QUESTION_BASE } from './config';
import { DataService } from '../../../../../shared/data.service';
import { SharedService } from '../../../../../shared/shared.service';
import { TREE_CONFIG_AREA } from '../../../../registration/candidates/add-candidate/add.config';
import { CentreService } from '../centre.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-centre-edit',
  templateUrl: './centre-edit.component.html',
  styleUrls: ['./centre-edit.component.css']
})
export class CentreEditComponent extends KdPageBase
  implements OnInit, OnDestroy {
  public api: IDynamicFormApi = {};
  public _questionBase: any;
  public formValue: any = {};
  public isValues: boolean = false;
  public centreId: any;

  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline'
    }
  ];

  constructor(
    public _router: Router,
    _activatedRoute: ActivatedRoute,
    _pageEvent: KdPageBaseEvent,
    public dataService: DataService,
    public centreService: CentreService,
    public sharedService: SharedService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  }

  ngOnInit() {
    super.setPageTitle('Examinations ­ Centre', false);
    super.setToolbarMainBtns([]);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this._questionBase = QUESTION_BASE;
    this.setEditValue();
  }

  public setEditValue() {
    let centreId = this.centreService.getCentreId();
    console.log(centreId);
    
    if (centreId != undefined) {
      this.centreId = centreId.id;
      this.dataService.examCentreOverView(centreId).subscribe((data: any) => {
        console.log('data.data.gradingOptions', data.data);
        this.isValues = true;
        for (let i = 0; i < this._questionBase.length; i++) {
          if (data.data[this._questionBase[i].key] != undefined) {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              data.data ? data.data[this._questionBase[i].key] : ''
            );
          } else {
          }
        }
      });
    } else {
      this._router.navigate(['main/examinations/centres/list']);
    }
  }

  callSpecific(data) {
    switch (data.key) {
      case 'academic_period_id': {
        this.getExamination(data.value);
        break;
      }
    }
  }

  getExamination(id) {
    if (id != 'null') {
      this.dataService.getExamination(id).subscribe(
        (data: any) => {
          let temp = [
            ...data.data.map(item => {
              return { key: item.id, value: item.name };
            })
          ];
          this.api.setProperty('examination_id', 'options', temp);
        },
        err => {
          console.log(err);
        }
      );
    } else {
      let temp = [
        {
          key: 'null',
          value: '--Select--'
        }
      ];
      this.api.setProperty('examination_id', 'options', temp);
    }
  }

  detectValue(question: any) {
    this.formValue[question.key] = question.value;
    this.callSpecific(question);
    if (question['required']) {
      if (
        !question['value'] ||
        question['value'] == '' ||
        question['value'] == 'null'
      ) {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', [
            'This field is required'
          ]);
        }, 1000);
      } else {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', []);
        }, 1000);
      }
    }
  }

  getAreaName() {
    this.dataService.getAreaName().subscribe((data: any) => {
      TREE_CONFIG_AREA.list = data.data;
      this.isValues = true;
    });
  }

  submitVal(formValue) {
    if (this.requiredCheck()) {
      let tempData = {
        id: this.centreId,
        academic_period: formValue.academic_period,
        centre_name: formValue.centre_name,
        centre_code: formValue.centre_code,
        emis_code: formValue.emis_code,
        area_name: formValue.area_name,
        examination: formValue.examination,
        address: formValue.address,
        postal_code: formValue.postal_code,
        contact_person: formValue.contact_person,
        telephone: formValue.telephone,
        ownership: formValue.ownership_id
      };
       console.log("tempData",tempData);
       
      this.dataService.examCentreUpdate(tempData).subscribe(
        (data: any) => {
          let toasterConfig: any = {
            type: 'success',
            title: 'Center Detail Edited Successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this._router.navigate(['main/examinations/centres/list']);
        },
        err => {
          let toasterConfig: any = {
            type: 'error',
            title: 'Something Went Wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
    }
  }

  requiredCheck() {
    let hasError: boolean;
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]['required']) {
        if (
          !this.formValue[this._questionBase[i]['key']] ||
          this.formValue[this._questionBase[i]['key']] == '' ||
          this.formValue[this._questionBase[i]['key']] == 'null' ||
          this.formValue[this._questionBase[i]['key']] == {}
        ) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]['key'], 'errors', [
              'This field is required'
            ]);
          }, 1000);
          break;
        }
      }
    }
    if (hasError) {
      return false;
    } else {
      return true;
    }
  }
  reset(){
    this._router.navigate(['main/examinations/centres/list']);

  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
