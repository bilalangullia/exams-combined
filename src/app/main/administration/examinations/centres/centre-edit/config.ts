import { TREE_CONFIG_AREA } from '../../../../registration/candidates/add-candidate/add.config';

export interface ITreeConfig {
  id: string;
  selectionMode: string;
  list?: object;
  expandAll?: boolean;
  expandSelected?: boolean;
}

export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    events: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    required: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    events: true
  },
  {
    key: 'centre_code',
    label: 'Centre Code',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'centre_name',
    label: 'Centre Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'emis_code',
    label: 'EMIS Code',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: false
  },
  {
    key: 'area_name',
    label: 'Area Name',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    events: true
  },
  {
    key: 'address',
    label: 'Address',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: false
  },
  {
    key: 'postal_code',
    label: 'Post Code',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: false
  },
  {
    key: 'contact_person',
    label: 'Contact Person',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: false
  },
  {
    key: 'telephone',
    label: 'Telephone',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'ownership_id',
    label: 'Ownership',
    visible: true,
    controlType: 'dropdown',
    options: [
      {
        key: '',
        value: '--Select--'
      },
      {
        key: 1,
        value: 'Government'
      },
      {
        key: 2,
        value: 'Private'
      },
      {
        key: 3,
        value: 'Namcol'
      }
    ]
  }
];
