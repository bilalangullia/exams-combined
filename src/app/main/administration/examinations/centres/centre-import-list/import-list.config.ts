import { Observable, Subscriber, timer } from 'rxjs';

interface Column {
    headerName?: string;
    field?: string;
    type?: 'input' | 'normal' | 'image';
    sortable?: boolean;
    filterable?: boolean;
    visible?: boolean;
    config?: any;
    class?: string;
    filterValue?: Array<string>;
}

interface ListColumn {
    RowNumber?: Column;
    Address?: Column;
    AreaName?: Column,
    CentreName?: Column,
    ContactPerson?: Column,
    CentreCode?:Column,
    EMISCodeme?:Column,
    Ownership?:Column,
    PostalCode?:Column,
    Telephone?:Column
}

export const TABLECOLUMN: ListColumn = {
    RowNumber: {
        headerName: 'Row Number',
        field: 'row_number',
        sortable: true,
        filterable: false
    },
    Address: {
        headerName: 'Address',
        field: 'address',
        sortable: true,
        filterable: false
    },
    AreaName: {
        headerName: 'Area Name',
        field: 'area_name',
        sortable: true,
        filterable: false
    },
    CentreName: {
      headerName: 'Centre Name',
      field: 'centre_name',
      sortable: true,
      filterable: false
  },
  ContactPerson: {
        headerName: 'Contact Person',
        field: 'contact_person',
        sortable: true,
        filterable: false
    },
    CentreCode: {
        headerName: 'Centre Code',
        field: 'centre_code',
        sortable: true,
        filterable: false
    },
    EMISCodeme: {
        headerName: 'EMIS Codeme',
        field: 'emis_codeme',
        sortable: true,
        filterable: false
    },
    Ownership: {
        headerName: 'Ownership',
        field: 'ownership',
        sortable: true,
        filterable: false
    },
    PostalCode: {
      headerName: 'PostalCode',
      field: 'postal_code',
      sortable: true,
      filterable: false
  },
  Telephone: {
    headerName: 'Telephone',
    field: 'telephone',
    sortable: true,
    filterable: false
}

};
export const CREATE_ROW: (_rowCount: number, _baseIndex?: number, data?: Array<any>) => Array<any> = (_rowCount: number, _baseIndex: number = 0, data?: Array<any>): Array<any> => {
    let row: Array<any> = [];
    for (let i: number = 0; i < data.length; i++) {
        let oneRow: any = {
            id: i,
            row_number:i+1,
            address: data[i].data['Address'],
            area_name:data[i].data['Area Name'],
            centre_name: data[i].data['Centre Name'],
            centre_code: data[i].data['Centre Code'],
            contact_person: data[i].data['Contact Person'],
            emis_codeme: data[i].data['EMIS Code'],
            ownership: data[i].data['Ownership'],
            postal_code:data[i].data['Postal Code'],
            telephone:data[i].data['Telephone'] 
        };
        row.push(oneRow);
    }

    return row;
};


export const CREATE_TABLE_CONFIG: (_id: string, _pagesize: number, _total: number) => any = (_id: string, _pagesize: number, _total: number): any => {
    return {
        id: _id,
        loadType: 'server',
        gridHeight: '600',
        externalFilter: false,
        paginationConfig: {
            pagesize: _pagesize,
            total: _total
        },
        click: {
            type: 'router',
            // pathMap: 'view',
            path: '/registration/view',
            callback: (): void => {
                console.log('ListNode: Demo callback used in when clicking the rowNode.');
            }
        }
    };
};

export const DUMMY_API_CALL: (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }) => Observable<any> =
    (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }): Observable<any> => {
        return new Observable((_observer: Subscriber<any>): void => {
            timer(1000).subscribe((): void => {
                _observer.next(CREATE_ROW(_params.pagesize, _params.startRow));
                _observer.complete();
            });
        });
    };

    export interface IMiniDashboardItem {
        type: string;
        icon?: string;
        label: string;
        value: number | string ;
    }
    
    export interface IMiniDashboardConfig {
        closeButtonDisabled?: boolean;
        rtl?: boolean;
    }
    
    
    export const MINI_DASHBOARD_CONFIG: IMiniDashboardConfig = {
        closeButtonDisabled: true,
        rtl: true,
    };
    
    export const MINI_DASHBOARD_DATA: Array<IMiniDashboardItem> = [
        {
            type: 'text',
            label: 'Total Rows :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Imported :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Update :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Failed :',
            value: '0'
        },
    ]