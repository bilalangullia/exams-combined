  
import { Observable ,  timer ,  Subscriber } from 'rxjs';
import {DataService} from '../../../../../../../shared/data.service'

export const _questionBase: Array<any> = [
  {
        key: 'academic_period',
        label: 'Academic Period',
        visible: true,
        order: 1,
        controlType: 'text',
        type: 'text',
        readonly: true,
        events: true,

  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    events: true,
},
{
  key: 'centre',
  label: 'Centre',
  visible: true,
  required: false,
  order: 1,
  controlType: 'text',
  type: 'text',
  readonly: true,
  events: true,
},
  {
    key: "invigilator",
    label: "Invigilator",
    visible: true,
    order: 1,
    controlType: "text",
    type: "text",
    required: true,

  },
  {
    'key': 'serverload',
    'label': 'Server Load Dropdown',
    'visible': true,
    'required': false,
    'order': 1,
    'controlType': 'text',
    'format': 'string',
    'type': 'autocomplete',
    'autocomplete': true,
    'clickToggleDropdown': true,
    'placeholder': 'Search \'sin\'',
    'dropdownCallback': (params: any): Observable<any> => {
        return new Observable((_observer: Subscriber<any>): any => {
            let data: Array<any> = [];
            console.log('> Params usable:', params);

            timer(2000).subscribe((): void => {
                if (params.query.length >= 4) {
                  DataService.prototype.invigilatorsSearch(params.query).subscribe(
                    (res:any)=>{
                       console.log("res",res);
                       
                  })
                }
               

                _observer.next(data);
                _observer.complete();
            });
        });
    }
}
    
  
];