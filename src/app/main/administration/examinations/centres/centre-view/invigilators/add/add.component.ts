import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { Observable ,  timer ,  Subscriber } from 'rxjs';
import { _questionBase } from './config'
import { DataService } from '../../../../../../../shared/data.service'
import { SharedService } from '../../../../../../../shared/shared.service'
import { CentreService } from '../../../centre.service'


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy  {


  public api: IDynamicFormApi = {};
  public _questionBase: Array<any>=[

    {
      key: 'academic_period',
      label: 'Academic Period',
      visible: true,
      order: 1,
      controlType: 'text',
      type: 'text',
      readonly: true,
      events: true,

},
{
  key: 'examination',
  label: 'Examination',
  visible: true,
  order: 1,
  controlType: 'text',
  type: 'text',
  readonly: true,
  events: true,
},
{
key: 'centre_name',
label: 'Centre',
visible: true,
required: false,
order: 1,
controlType: 'text',
type: 'text',
readonly: true,
events: true,
},

{
  key: 'serverload',
  label: 'invigilator',
  visible: true,
  required: true,
  order: 1,
  controlType: 'text',
  format: 'string',
  type: 'autocomplete',
  autocomplete: true,
  clickToggleDropdown: true,
  'placeholder': '',
  dropdownCallback: (params: any): Observable<any> => {
      return new Observable((_observer: Subscriber<any>): any => {
          let data: Array<any> = [];
          console.log('> Params usable:', params);

          timer(2000).subscribe((): void => {
              if (params.query.length >= 4) {
                this.dataService.invigilatorsDropDownSearch(params.query).subscribe(
                  (res:any)=>{
                     console.log("res",res);
                     let tempKey=[{key:44, value:'john smith'  }]
                     if(res.data.length>0){
                      data=res.data
                      _observer.next(data);
                      _observer.complete();
                     }else{
                      _observer.next([]);
                      _observer.complete();
                     }                                 
                     
                })
              }
          });
      });
  }
}
  

  ];
  public formValue: any = {};
  public isValues: boolean = false;
  public invigilatorDetail: any;

  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline'
    }
  ];


  constructor(public _router: Router,
    _activatedRoute: ActivatedRoute,
    _pageEvent: KdPageBaseEvent,
    public dataService: DataService,
    public centreService: CentreService,
    public sharedService: SharedService) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }
  ngOnInit() {
  
    super.setPageTitle('Centres ­ Invigilators', false);
    super.setToolbarMainBtns([]);
    super.updatePageHeader();
    super.updateBreadcrumb();
  
  this.setEditDetail();
  }


  setEditDetail() {
    this.invigilatorDetail= this.centreService.getCentreDetail()   
    console.log("this.invigilatorDetail..this.invigilatorDetail",this.invigilatorDetail);
     
    setTimeout(() => {     
      if(this.invigilatorDetail != undefined ){
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this.invigilatorDetail[this._questionBase[i].key] !== undefined) {
            this.api.setProperty(this._questionBase[i].key, 'value', this.invigilatorDetail[this._questionBase[i].key]);
          } 
        }
        this.isValues=true;
      }
    }, 500);
  }

 



  
  submitVal(formValue){

    if (this.requiredCheck()) {
      let tempData = {
        examination_centre_id:  this.invigilatorDetail.id,
        invigilator_id: formValue.serverload.key,
      };
      this.dataService.invigilatorsAdd(tempData).subscribe(
        (data: any) => {
          let toasterConfig: any = {
            type: 'success',
            title: 'Invigilators added Successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this._router.navigate(['main/examinations/centres/view/invigilators/list']);
        },
        err => {
          let toasterConfig: any = {
            type: 'error',
            title: 'Something Went Wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    }    
  }



  detectValue(question: any) {
    this.formValue[question.key] = question.value;
    
    if (question['required']) {
      if (
        !question['value'] ||
        question['value'] == '' ||
        question['value'] == 'null'
      ) {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', [
            'This field is required'
          ]);
        }, 1000);
      } else {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', []);
        }, 1000);
      }
    }
  }


  requiredCheck() {
    let hasError: boolean;
    console.log();
    
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]['required']) {
        if (
          !this.formValue[this._questionBase[i]['key']] ||
          this.formValue[this._questionBase[i]['key']] == '' ||
          this.formValue[this._questionBase[i]['key']] == 'null' ||
          this.formValue[this._questionBase[i]['key']] == {}
        ) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]['key'], 'errors', [
              'This field is required'
            ]);
          }, 1000);
          break;
        }
      }
    }

    if (hasError) {
      return false;
    } else {
      return true;
    }
  }
  reset(){
    this._router.navigate(['main/examinations/centres/view/invigilators/list']);

  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
