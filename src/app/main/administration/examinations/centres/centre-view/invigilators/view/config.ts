export const VIEWNODE_INPUT: Array<any> = [
  {
    'key': 'academic_year',
    'label': 'Academic Period',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'examination',
    'label': 'Examination',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'exam_centre_name',
    'label': 'Centre',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'invigilator',
    'label': 'Invigilator',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_by',
    'label': 'Created By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  
  {
    'key': 'created_on',
    'label': 'Created On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }
]


export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
