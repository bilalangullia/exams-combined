import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { KdPageBase, KdPageBaseEvent, KdToolbarEvent, IDynamicFormApi, KdAdvFilterEvent, KdTableEvent, KdAlertEvent,KdModalEvent } from 'openemis-styleguide-lib';

import{VIEWNODE_INPUT ,IModalConfig} from './config'


import {CentreService} from '../../../centre.service'
import {DataService} from '../../../../../../../shared/data.service'

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  providers: [KdModalEvent]
})
export class ViewComponent  extends KdPageBase implements OnInit, OnDestroy {

  
 public  modalConfig: IModalConfig = {
   title: 'Delete invigilators',
   body:'Are you sure you want to delete this invigilator',
   button: [
     {
       text: 'Delete',
       class: 'btn-text',
       callback: (): void => {
         event.preventDefault();
         this.dataService.invigilatorsDelete(this.detail).subscribe((res:any)=>{
          this._modelEvent.toggleClose();
          this._router.navigate(['main/examinations/centres/view/invigilators/list']);
         },err=>{
         
          let toasterConfig: any = {
            type: 'error',
            title: 'Something went wrong',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this._kdalert.error(toasterConfig); 
          this._modelEvent.toggleClose(); 
        })
     }
     },
     {
       text: 'Cancel',
       class: 'btn-outline',
       callback: (): void => {
         event.preventDefault();
         this._modelEvent.toggleClose();
       }
     }
   ]
};  
  
  public _questionBase=VIEWNODE_INPUT ;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null;
  public detail:any={};
  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _kdalert: KdAlertEvent,
    public centreService:CentreService,
    public dataService:DataService,
    public _modelEvent:KdModalEvent,
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  
  
  }



  ngOnInit() {
    super.setPageTitle('Exam Centres ­ Invigilators', false);

    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => { this._router.navigate(['main/examinations/centres/view/invigilators/list']); }
      },
      {
        type:'delete',
        callback:():void=>{
          this.open();   
        }
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.setDetail();
  }



  setDetail(){
    let tempCentreId = this.centreService.getInvigilatorDetail()
   
    if (tempCentreId != undefined) {
      this.detail=tempCentreId  
      this.dataService.invigilatorsView(tempCentreId).subscribe(
        (data: any) => {
           
             this.setRoomViewDetail(data.data)
        },err=>{
          this.loading = false;
          let toasterConfig: any = {
            type: 'error',
            title: 'Something went wrong',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this._kdalert.error(toasterConfig);
          this._router.navigate(['main/examinations/centres/view/invigilators/list'])
        })
    } else {
      this._router.navigate(['main/examinations/centres/view/invigilators/list'])
    }
    }
    
    
    
      setRoomViewDetail(data: any) {
        console.log(data)
        setTimeout(() => {
          for (let i = 0; i < this._questionBase.length; i++) {
            if(this._questionBase[i].key=='invigilator'){
              let temp =data['openemis_no'] +'-'+data['first_name']+' '+data['last_name']
              this.api.setProperty(this._questionBase[i].key, 'value', temp);
            }
            else{
              this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key] ? data[this._questionBase[i].key] : 'NA');
            }
            }
          this.loading=false;
        }, 500);
      }
  
  editCandidate(){
    this._router.navigate(['main/examinations/centres/view/invigilators/edit']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
  open(){
    this._modelEvent.toggleOpen();       
  }
}
