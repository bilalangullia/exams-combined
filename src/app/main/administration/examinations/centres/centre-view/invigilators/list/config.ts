interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;

}

interface ListColumn {
  OpenEMISID?: Column;
  Invigilator?: Column;
  Examination?: Column;
 
}

export const TABLECOLUMN: ListColumn = {
 

  OpenEMISID: {
      headerName: 'OpenEMIS ID',
      field: 'openemis_no',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  
Examination: {
  headerName: 'Examination',
  field: 'examination',
  sortable: true,
  filterable: true,
  filterValue: []
},
Invigilator: {
  headerName: 'Invigilator',
  field: 'full_name',
  sortable: true,
  filterable: true,
  filterValue: []
}
  
};


export const temData=[
  {candidate_id:'19OE090001',
  student:'Lovelyn Sonia',
  openemis_id:'1571100054',
  nationality:'Namibian',
  institution:'Aligegeo PSS',
  examination:'FO202011 ­ Senior Secondary Certificate Ordinary Level',
  room:'Exam Hall'
}
]



