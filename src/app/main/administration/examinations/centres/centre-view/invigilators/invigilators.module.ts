import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvigilatorsRoutingModule } from './invigilators-routing.module';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../../../../../../shared/shared.module';
import { AddComponent } from './add/add.component';

@NgModule({
  imports: [
    CommonModule,
    InvigilatorsRoutingModule,
    SharedModule
  ],
  declarations: [ViewComponent, ListComponent, EditComponent, AddComponent]
})
export class InvigilatorsModule { }
  