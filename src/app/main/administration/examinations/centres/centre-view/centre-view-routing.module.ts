import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CentreViewComponent } from './centre-view.component';
const routes: Routes = [
  {
    path: '',
    component: CentreViewComponent,
    children: [
      {
        path: '',
        loadChildren: './overview/overview.module#OverviewModule'
      },
      {
        path: 'rooms',
        loadChildren: './rooms/rooms.module#RoomsModule'
      },
      {
        path: 'examinations', 
        loadChildren:
          './centre-examinations/centre-examinations.module#CentreExaminationsModule'
      },
      {
        path: 'subjects',
        loadChildren: './subject/subject.module#SubjectModule'
      },
      {
        path: 'students',
        loadChildren: './students/students.module#StudentsModule'
      },
      {
        path: 'invigilators',
        loadChildren: './invigilators/invigilators.module#InvigilatorsModule'
      },
      {
        path: 'linkedinstitutions',
        loadChildren:
          './linkedinstitutions/linkedinstitutions.module#LinkedinstitutionsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CentreViewRoutingModule {}
