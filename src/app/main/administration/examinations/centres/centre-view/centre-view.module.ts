import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CentreViewRoutingModule } from './centre-view-routing.module';

import { CentreViewComponent } from './centre-view.component';
import { SharedModule } from '../../../../../shared/shared.module';

@NgModule({
  imports: [CommonModule, CentreViewRoutingModule, SharedModule],
  declarations: [CentreViewComponent]
})
export class CentreViewModule {}
