import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ViewComponent} from './view/view.component'
import {ListComponent} from './list/list.component'
import {EditComponent} from './edit/edit.component'
import { CentreExaminationsRoutingModule } from './centre-examinations-routing.module';
import { SharedModule } from '../../../../../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    CentreExaminationsRoutingModule,
    SharedModule
  ],
  declarations: [ViewComponent,ListComponent,EditComponent]
})
export class CentreExaminationsModule { }
