interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;

}

interface ListColumn {
  AcademicPeriod?: Column;
  ExamName?: Column;
  EducationGrade?: Column;
  RegistrationStartDate?:Column;
  RegisttrationEndDate?:Column;
  TotalRegistered?:Column;

}

export const TABLECOLUMN: ListColumn = {
 
  AcademicPeriod: {
      headerName: 'Academic Period',
      field: 'academic_period',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  ExamName: {
      headerName: 'Exam Name',
      field: 'exam_name',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  EducationGrade: {
      headerName: 'Education Grade',
      field: 'education_grade',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  RegistrationStartDate: {
    headerName: 'Registration Start Date',
    field: 'registration_start_date',
    sortable: true,
    filterable: true,
    filterValue: []
},
RegisttrationEndDate: {
  headerName: 'Registtration End Date',
  field: 'registtration_end_date',
  sortable: true,
  filterable: true,
  filterValue: []
},
TotalRegistered: {
  headerName: 'Total Registered',
  field: 'total_registered',
  sortable: true,
  filterable: true,
  filterValue: []
},
  
};


export const temData=[
  {academic_period:'Exam Hall',
  exam_name:'100',
  education_grade:'50',
  registration_start_date:'2020-01-29',
  registtration_end_date:'2020-02-29',
  total_registered:'80'
}
]



