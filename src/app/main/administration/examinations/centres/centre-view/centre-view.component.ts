import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  IDynamicFormApi,
  KdAdvFilterEvent,
  KdTableEvent,
  KdAlertEvent
} from 'openemis-styleguide-lib';
import { TABS_ROUTER } from './tab.config';
@Component({
  selector: 'app-centre-view',
  templateUrl: './centre-view.component.html',
  styleUrls: ['./centre-view.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent]
})
export class CentreViewComponent extends KdPageBase
  implements OnInit, OnDestroy {
  public _questionBase: any;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null;
  public tabsListRouter: Array<any> = TABS_ROUTER;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _kdalert: KdAlertEvent
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  }

  ngOnInit() {
    super.setPageTitle('Examinations ­ Centres', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => {
          this._router.navigate(['main/registration/candidates']);
        }
      },
      {
        type: 'edit',
        callback: (): void => {}
      },
      {
        type: 'delete',
        callback: (): void => {}
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();
  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
