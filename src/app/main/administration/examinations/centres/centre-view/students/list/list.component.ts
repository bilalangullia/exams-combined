import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';

import { DataService } from '../../../../../../../shared/data.service';
import { CentreService } from '../../../centre.service';
import { TABLECOLUMN, temData } from './config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public _tableApi: ITableApi = {};
  public centreId: any;

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    },
    action: {
      enabled: true,
      list: [
        {
          icon: 'far fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.setStudentView(_rowNode.data.id);
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.OpenEMISID, TABLECOLUMN.CandidateID, TABLECOLUMN.Name];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    public _kdalert: KdAlertEvent,
    private _advFilterEvent: KdAdvFilterEvent,
    private _toolbarEvent: KdToolbarEvent,
    public _tableEvent: KdTableEvent,
    private kdtable: KdIntTableEvent,
    public centreService: CentreService,
    public dataService: DataService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }

  ngOnInit() {
    super.setPageTitle('Centres ­ Students', false);
    super.setToolbarMainBtns([
      // {
      //   type: "add",
      //   path: ''
      // }
    ]);

    super.enableToolbarSearch(true, (event: any): void => {
      // console.log("_event.._event", event)
      if (event.length > 2 || event === '') {
        this.searchStudentList(this.centreService.centreDetail, event);
      }
    });

    super.updatePageHeader();
    super.updateBreadcrumb();

    this.setStudentList();
  }

  setStudentList() {
    let tmpId = this.centreService.centerId;

    if (tmpId != undefined) {
      this.centreId = this.centreService.centerId.id;

      this.dataService.studentList(tmpId).subscribe(
        (data: any) => {
          this.setCenterListFilter(data.data);
        },
        (err) => {
          this.loading = false;
          let toasterConfig: any = {
            type: 'error',
            title: 'No records found',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this._kdalert.error(toasterConfig);
        }
      );
    } else {
      this.router.navigate(['main/examinations/centres/list']);
    }
  }
  searchStudentList(data: any, key) {
    this.dataService.studenttSerarch(data, key).subscribe(
      (res: any) => {
        this.setCenterListFilter(res.data);
      },
      (err) => {
        this.loading = false;
        let toasterConfig: any = {
          type: 'error',
          title: 'No records found',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this._kdalert.error(toasterConfig);
      }
    );
  }

  setCenterListFilter(data) {
    this.resetFilter();

    if (data.length > 0) {
      this._row = data;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (data[j][this.tableColumns[i]['field']] && data[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(data[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
      this.loading = false;
    } else {
      this.loading = false;
      let toasterConfig: any = {
        type: 'error',
        title: 'No records found',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this._kdalert.error(toasterConfig);
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }
  setStudentView(data) {
    this.centreService.setStudentView(data);
    this.router.navigate(['/main/examinations/centres/view/students/view']);
  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
