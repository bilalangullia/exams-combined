interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;

}

interface ListColumn {
  CandidateID?: Column;
  OpenEMISID?: Column;
  Student?: Column;
  Nationality?:Column;
  Institution?:Column;
  Examination?:Column;
  Name?:Column;
}

export const TABLECOLUMN: ListColumn = {
 
  CandidateID: {
      headerName: 'Candidate ID',
      field: 'candidate_id',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  OpenEMISID: {
      headerName: 'OpenEMIS ID',
      field: 'open_emis_id',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  Student: {
    headerName: 'Student',
    field: 'student',
    sortable: true,
    filterable: true,
    filterValue: []
},
Nationality: {
      headerName: 'Nationality',
      field: 'nationality',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  Institution: {
    headerName: 'Institution',
    field: 'institution',
    sortable: true,
    filterable: true,
    filterValue: []
}, 
Examination: {
  headerName: 'Examination',
  field: 'examination',
  sortable: true,
  filterable: true,
  filterValue: []
},
Name: {
  headerName: 'Name',
  field: 'name',
  sortable: true,
  filterable: true,
  filterValue: []
}
  
};


export const temData=[
  {candidate_id:'19OE090001',
  student:'Lovelyn Sonia',
  openemis_id:'1571100054',
  nationality:'Namibian',
  institution:'Aligegeo PSS',
  examination:'FO202011 ­ Senior Secondary Certificate Ordinary Level',
  room:'Exam Hall'
}
]



