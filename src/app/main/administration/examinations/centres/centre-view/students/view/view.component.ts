import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { KdPageBase, KdPageBaseEvent, KdToolbarEvent, IDynamicFormApi, KdAdvFilterEvent, KdTableEvent, KdAlertEvent } from 'openemis-styleguide-lib';

import { VIEWNODE_INPUT } from './config';

import { CentreService } from '../../../centre.service'
import { DataService } from '../../../../../../../shared/data.service'
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _kdalert: KdAlertEvent,
    public dataService: DataService,
    public centreService:CentreService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute, 
      pageEvent: _pageEvent
    });

  }

  ngOnInit(): void {
    super.setPageTitle('Registrations - Candidates', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => { this._router.navigate(['/main/examinations/centres/view/students/list']); }
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();
     this.loading=false;
    this.getCandidateView();
  }

  getCandidateView() {
    let tempData = this.centreService.getStudentView() ;
      console.log("tempData..tempData",tempData);
      
    if (tempData != undefined) {
            this.dataService.studentView(tempData).subscribe((data:any)=>{
              this.setStudentDetail(data.data)              
            })
    }
    else {
      this._router.navigate(['/main/examinations/centres/view/students/list'])
    }
  }
 
 
  setStudentDetail(data: any) {
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'options') {
          this.api.setProperty(this._questionBase[i].key, 'row', data.options);
        } else {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key] ? data[this._questionBase[i].key] : 'NA');
        }
      }
    }, 500);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

}

