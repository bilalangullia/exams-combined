import { TABLE_COLUMN_LIST } from '../../../../../../registration/candidates/add-candidate/add.config';

export const VIEWNODE_INPUT: Array<any> = [
  {
    'key': 'academic_year',
    'label': 'Academic Period',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'examination_id',
    'label': 'Examination',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'area',
    'label': 'Area Name',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'exam_centre',
    'label': 'Exam Centre',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'attendance_type',
    'label': 'Attendance Type',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'open_emis_id',
    'label': 'Openemis ID',
    'visible': true,
    'controlType': 'readonly',
    'format': 'readonly',
    'readOnly': true,
    'type': 'string'
  },
  {
    'key': 'candidate_id',
    'label': 'Candidate ID',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'first_name',
    'label': 'First Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'second_name',
    'label': 'Second Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'middle_name',
    'label': 'Middle Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'last_name',
    'label': 'Last Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'gender',
    'label': 'Gender',
    'visible': true,
    'controlType': 'text',
    'format': 'text',
    'disabled': false
  },
  {
    'key': 'date_of_birth',
    'label': 'Date of Birth',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'nationality',
    'label': 'Nationality',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'identityType',
    'label': 'Identity Type',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'identity_number',
    'label': 'Identity Number',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'address',
    'label': 'Address',
    'visible': true,
    'controlType': 'text',
    'format': 'text',
    'disabled': false
  },
  {
    'key': 'postal_code',
    'label': 'Postal Code',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'address_area',
    'label': 'Address Area',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'birthplace_area',
    'label': 'Birthplace Area',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'special_needs_assessment_type',
    'label': 'Special Needs Assessment Type',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'difficulties',
    'label': 'Difficulties',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'options',
    'label': 'Options',
    'visible': true,
    'controlType': 'table',
    'row': [],
    'column': [
      TABLE_COLUMN_LIST.option_code,
      TABLE_COLUMN_LIST.option_name,
      TABLE_COLUMN_LIST.carry_forward
    ],
    'config': {
      'id': 'options',
      'rowIdKey': 'id',
      'gridHeight': 250,
      'loadType': 'normal'
    }
  },
  {
    'key': 'modified_by',
    'label': 'Modified By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }, 
  {
    'key': 'modified_on',
    'label': 'Modified On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_by',
    'label': 'Created By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_on',
    'label': 'Created On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }
];