import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ViewComponent} from './view/view.component'
import {ListComponent} from './list/list.component'
import {EditComponent} from './edit/edit.component'



const routes: Routes = [ {
  path: '',
  children: [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    { path: 'list', component:ListComponent},
    { path: 'view', component:ViewComponent},
    { path: 'edit', component:EditComponent},
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule { }
