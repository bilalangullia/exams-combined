import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { KdPageBase, KdPageBaseEvent, KdToolbarEvent, IDynamicFormApi, KdAdvFilterEvent, KdTableEvent, KdAlertEvent,KdModalEvent } from 'openemis-styleguide-lib';


import  {VIEWNODE_INPUT,IModalConfig} from './config'
import {CentreService} from '../../../centre.service'
import {DataService} from '../../../../../../../shared/data.service'



@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  providers: [KdModalEvent]

})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {

  public  modalConfig: IModalConfig = {
    title: 'Delete centre',
    body:'Are you sure you want to delete this centre',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.dataService.centreDelete(this.centreID).subscribe((res:any)=>{
           this._modelEvent.toggleClose();
           this._router.navigate(['main/examinations/centres/list']);
          },err=>{
 
            this._modelEvent.toggleClose(); 
 
           let toasterConfig: any = {
             type: 'error',
             title: 'Centre can not be deleted because its link to another data ',
             showCloseButton: true,
             tapToDismiss: true,
             timeout: 3000
           };
           this._kdalert.error(toasterConfig); 
         })
      } 
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
 };  
  

  
  public _questionBase=VIEWNODE_INPUT ;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null;
  public  centreID:any;
  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public  dataService:DataService,
    public  centreService:CentreService,
    public _kdalert: KdAlertEvent,
    public _modelEvent:KdModalEvent,

  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }

  ngOnInit() {
    super.setPageTitle('Examinations ­ Centres', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => { this._router.navigate(['main/examinations/centres/list']); }
      }, {
        type: 'edit',
        callback: (): void => { 
            this._router.navigate(['main/examinations/centres/edit']);}
      }, {
        type: 'delete',
        callback: (): void => {this.open() } 
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.setDetail();
  }



setDetail(){

let tempCentreId=this.centreService.getCentreId() 

 
if(tempCentreId!=undefined){
this.centreID=tempCentreId.id
  this.dataService.examCentreOverView(tempCentreId).subscribe(
      (data:any)=>{
        this.centreService.centreDetail={}
        this.centreService.setCentreDetail(data.data)
       this.setOverViewDetail(data.data)
    })
}else{
  this._router.navigate(['main/examinations/centres/list'])
}

}



  setOverViewDetail(data: any) {
    console.log(data)
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'gradingOptions') {
          this.api.setProperty(this._questionBase[i].key, 'row', data.gradingOptions);
        } else {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key] ? data[this._questionBase[i].key] : 'NA');
        }
      }
      this.loading=false;
    }, 500);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

  open(){
    this._modelEvent.toggleOpen();       
  }



}



