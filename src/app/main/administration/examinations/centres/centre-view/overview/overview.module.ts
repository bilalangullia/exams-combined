import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OverviewRoutingModule } from './overview-routing.module';
import { ViewComponent } from './view/view.component';
import {SharedModule} from '../../../../../../shared/shared.module'

@NgModule({
  imports: [
    CommonModule,
    OverviewRoutingModule,
    SharedModule
  ],
  declarations: [ViewComponent]
})
export class OverviewModule { }
