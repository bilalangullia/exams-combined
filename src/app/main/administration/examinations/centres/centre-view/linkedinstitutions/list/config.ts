interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;

}

interface ListColumn {
  LinkedInstitution?: Column;
  Examination?: Column;
 
}

export const TABLECOLUMN: ListColumn = {
 

  LinkedInstitution: {
      headerName: 'Linked Institution',
      field: 'linked_institution',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  
Examination: {
  headerName: 'Examination',
  field: 'examination',
  sortable: true,
  filterable: true,
  filterValue: []
},
  
};


export const temData=[
  {linked_institution:'001',
  examination:'FO202011 ­ Senior Secondary Certificate Ordinary Level',
}
]



