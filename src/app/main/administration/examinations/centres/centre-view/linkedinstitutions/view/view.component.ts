import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { KdPageBase, KdPageBaseEvent, KdToolbarEvent, IDynamicFormApi, KdAdvFilterEvent, KdTableEvent, KdAlertEvent } from 'openemis-styleguide-lib';

import{VIEWNODE_INPUT} from './config'


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {

  public _questionBase=VIEWNODE_INPUT ;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _kdalert: KdAlertEvent,
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  
  
  }



  ngOnInit() {
    super.setPageTitle('Exam Centres ­ Linked Institutions', false);

    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => { this._router.navigate(['main/examinations/centres/view/linkedinstitutions/list']); }
      }, {
        type: 'delete',
        callback: (): void => { }
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading=false;
  }



  setDetail(){
      this._router.navigate(['main/examinations/centres/view/linkedinstitutions/list'])
    }
    
    
    
      setRoomViewDetail(data: any) {
        console.log(data)
        setTimeout(() => {
          for (let i = 0; i < this._questionBase.length; i++) {
            if (this._questionBase[i].key == 'gradingOptions') {
              this.api.setProperty(this._questionBase[i].key, 'row', data.gradingOptions);
            } else {
              this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key] ? data[this._questionBase[i].key] : 'NA');
            }
          }
          this.loading=false;
        }, 500);
      }
    
  
 

  


  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
  
}
