import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LinkedinstitutionsRoutingModule } from './linkedinstitutions-routing.module';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../../../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    LinkedinstitutionsRoutingModule,
    SharedModule
  ],
  declarations: [ViewComponent, ListComponent, EditComponent]
})
export class LinkedinstitutionsModule { }
