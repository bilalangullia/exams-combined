export const VIEWNODE_INPUT: Array<any> = [
  {
    'key': 'linked_institution',
    'label': 'Linked Institution',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'examination',
    'label': 'Examination',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_by',
    'label': 'Created By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  
  {
    'key': 'Created On',
    'label': 'created_on',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }
]