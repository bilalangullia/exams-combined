import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';

import { TABLECOLUMN, temData } from './config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public _tableApi: ITableApi = {};

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    },
    action: {
      enabled: true,
      list: [
        {
          icon: 'far fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.router.navigate(['/main/examinations/centres/view/linkedinstitutions/view']);
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.LinkedInstitution, TABLECOLUMN.LinkedInstitution];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    public _kdalert: KdAlertEvent,
    private _advFilterEvent: KdAdvFilterEvent,
    private _toolbarEvent: KdToolbarEvent,
    public _tableEvent: KdTableEvent,
    private kdtable: KdIntTableEvent
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }

  ngOnInit() {
    super.setPageTitle('Exam Centres ­ Linked Institutions', false);
    super.setToolbarMainBtns([
      {
        type: 'add',
        path: ''
      }
    ]);

    super.enableToolbarSearch(true, (event: any): void => {
      // console.log("_event.._event", event)
      if (event.length > 2 || event === '') {
      }
    });

    super.updatePageHeader();
    super.updateBreadcrumb();

    this._row = temData;
    this.loading = false;
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
