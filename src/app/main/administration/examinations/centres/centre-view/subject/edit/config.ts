 


export const QUESTION_BASE: Array<any> = [
  {
        key: 'academic_period',
        label: 'Academic Period',
        visible: true,
        order: 1,
        controlType: 'text',
        type: 'text',
        readonly: true,
        events: true,

  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    events: true,
},
{
  key: 'centre',
  label: 'Centre',
  visible: true,
  order: 1,
  controlType: 'text',
  type: 'text',
  readonly: true,
  events: true,
},
  {
    key: "subject_code",
    label: "Subject Code",
    visible: true,
    order: 1,
    controlType: "text",
    type: "text",
    readonly: true,

  },
  {
    key: 'subject_name',
    label: 'Subject Name',
    visible: true,
    required: true,
    controlType: 'decimal',
    type: 'number',
    readonly: true,
    float: 1,
  },
  {
    key: 'education_subject',
    label: 'Education Subject',
    visible: true,
    required: true,
    controlType: 'decimal',
    type: 'number',
    readonly: true,
    float: 1,
  },
  {
    key: 'offered',
    label: 'Offered',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{
        'key': 'null',
        'value': '--Select--'
    },{
      'key':'1',
      'value':'Yes'
    },{
      'key':'2',
      'value':'No'
    }],
    events: true
},
  
];