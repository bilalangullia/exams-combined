interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;

}

interface ListColumn {
  AcademicPeriod?: Column;
  ExamTypeCode?: Column;
  SubjectCode?: Column;
  SubjectName?:Column;
  Offered?:Column;
}

export const TABLECOLUMN: ListColumn = {
 
  AcademicPeriod: {
      headerName: 'Academic Period',
      field: 'academic_period',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  ExamTypeCode: {
      headerName: 'Exam Type Code',
      field: 'exam_type_code',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  SubjectName: {
    headerName: 'Subject Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
},
  SubjectCode: {
      headerName: 'Subject Code',
      field: 'code',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  Offered: {
    headerName: 'Offered',
    field: 'offered',
    sortable: true,
    filterable: false,
    filterValue: []
}
  
};


export const temData=[
  {academic_period:'2020',
  exam_type_code:'FO',
  subject_code:'4101',
  subject_name:'First Language Afrikaans',
  group:'4'
}
]



