import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { KdPageBase, KdPageBaseEvent, 
    KdToolbarEvent,
   IDynamicFormApi, KdAdvFilterEvent, 
   KdTableEvent, KdAlertEvent } from 'openemis-styleguide-lib';

import{VIEWNODE_INPUT} from './config'

import { CentreService } from '../../../centre.service'
import { DataService } from '../../../../../../../shared/data.service'

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {

  public _questionBase=VIEWNODE_INPUT ;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _kdalert: KdAlertEvent,
    public centreService:CentreService,
    public dataService:DataService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
 
  }



  ngOnInit() {
    super.setPageTitle('Exam Centres ­ Rooms', false);

    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => { this._router.navigate(['main/examinations/centres/view/subjects/list']); }
      },
      //  {
      //   type: 'edit',
      //   callback: (): void => { this.editCandidate() }
      // }, {
      //   type: 'delete',
      //   callback: (): void => { }
      // }
    ]);
    super.updatePageHeader(); 
    super.updateBreadcrumb();
    this.setDetail();
  }



  setDetail(){
    let tempCentreId = this.centreService.getSubjectView()
    console.log("tempCentreId...tempCentreId",tempCentreId);
    
    if (tempCentreId != undefined) {
      this.dataService.subjectView(tempCentreId).subscribe(
        (data: any) => {
                     console.log("");
                     
             this.setSubjectViewDetail(data.data)
        },err=>{
          this.loading = false;
          let toasterConfig: any = {
            type: 'error',
            title: 'Something went wrong',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this._kdalert.error(toasterConfig);
          this._router.navigate(['main/examinations/centres/view/subjects/list'])
        })
    } else {
       this._router.navigate(['main/examinations/centres/view/subjects/list'])
    }
    }
    
    
    
      setSubjectViewDetail(data: any) {
        console.log(data)
        setTimeout(() => {
          for (let i = 0; i < this._questionBase.length; i++) {
            if (this._questionBase[i].key == 'gradingOptions') {
              this.api.setProperty(this._questionBase[i].key, 'row', data.gradingOptions);
            } else {
              this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key] ? data[this._questionBase[i].key] : 'NA');
            }
          }
          this.loading=false;
        }, 500);
      }
    
  


  editCandidate(){
    this._router.navigate(['main/examinations/centres/view/subjects/edit']);
  }


  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
  
}
