import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi
} from 'openemis-styleguide-lib';


import { QUESTION_BASE } from './config'
import { DataService } from '../../../../../../../shared/data.service'
import { SharedService } from '../../../../../../../shared/shared.service'
import { CentreService } from '../../../centre.service'

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy  {


  public api: IDynamicFormApi = {};
  public _questionBase: any;
  public formValue: any = {};
  public isValues: boolean = false;
  public centreId: any;

  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline'
    }
  ];


  constructor(public _router: Router,
    _activatedRoute: ActivatedRoute,
    _pageEvent: KdPageBaseEvent,
    public dataService: DataService,
    public centreService: CentreService,
    public sharedService: SharedService) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }
  ngOnInit() {
  
    super.setPageTitle('Centres ­ Subjects', false);
    super.setToolbarMainBtns([]);
    super.updatePageHeader();
    super.updateBreadcrumb();
  
  
    this._questionBase = QUESTION_BASE
    this.isValues=true;

  }



  detectValue($event){}
  submitVal($event){}

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
