


export const QUESTION_BASE: Array<any> = [
  {
        key: 'academic_period',
        label: 'Academic Period',
        visible: true,
        order: 1,
        controlType: 'text',
        type: 'text',
        readonly: true,
        events: true,

  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    events: true,
},
{
  key: 'examination_centre',
  label: 'Centre',
  visible: true,
  required: false,
  order: 1,
  controlType: 'text',
  type: 'text',
  readonly: true,
  events: true,
},
  {
    key: "name",
    label: "Room Name",
    visible: true,
    order: 1,
    controlType: "text",
    type: "text",
    required: true,

  },
  {
    key: 'size',
    label: 'Room Size',
    visible: true,
    required: true,
    controlType: 'decimal',
    type: 'number',
    float: 1,
  },
  {
    key: 'number_of_seats',
    label: 'Number Of Seats',
    visible: true,
    required: true,
    controlType: 'decimal',
    type: 'number',
    float: 1,
  },
    
  
];