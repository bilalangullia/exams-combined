interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;

}

interface ListColumn {
  Name?: Column;
  Size?: Column;
  NumberofSeats?: Column;
}

export const TABLECOLUMN: ListColumn = {
 
  Name: {
      headerName: 'Name',
      field: 'name',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  Size: {
      headerName: 'Size',
      field: 'size',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  NumberofSeats: {
      headerName: 'Number of Seats',
      field: 'number_of_seats',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  
};


export const temData=[
  {name:'Exam Hall',
  size:'100',
  number_of_seats:'50' }
]



