import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomsRoutingModule } from './rooms-routing.module';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../../../../../../shared/shared.module';
import { AddComponent } from './add/add.component';


@NgModule({
  imports: [
    CommonModule,
    RoomsRoutingModule,
    SharedModule
  ],
  declarations: [ListComponent, ViewComponent, EditComponent, AddComponent]
})
export class RoomsModule { }
