import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi
} from 'openemis-styleguide-lib';


import { QUESTION_BASE } from './config'
import { DataService } from '../../../../../../../shared/data.service'
import { SharedService } from '../../../../../../../shared/shared.service'
import { CentreService } from '../../../centre.service'


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy  {


  public api: IDynamicFormApi = {};
  public _questionBase: any;
  public formValue: any = {};
  public isValues: boolean = false;
  public centreId: any;
  public roomDetail:any={};


  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline'
    }
  ];


  constructor(public _router: Router,
    _activatedRoute: ActivatedRoute,
    _pageEvent: KdPageBaseEvent,
    public dataService: DataService,
    public centreService: CentreService,
    public sharedService: SharedService) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }
  ngOnInit() {
  
    super.setPageTitle('Centres ­ Rooms', false);
    super.setToolbarMainBtns([]);
    super.updatePageHeader();
    super.updateBreadcrumb();
  
  
    this._questionBase = QUESTION_BASE
 
    this.setRoomsDetail();
  }



setRoomsDetail(){
  let roomId = this.centreService.getCentreRoomId()
    if (roomId != undefined) {
      this.dataService.centreRoomsView(roomId).subscribe(
        (data: any) => {
          this.setEditDetail(data.data)
        })
    } else {
      this._router.navigate(['main/examinations/centres/view/rooms/list'])
    }
}

  setEditDetail(data:any) {
    this.roomDetail= data    
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this.roomDetail[this._questionBase[i].key]) {
          this.api.setProperty(this._questionBase[i].key, 'value', this.roomDetail[this._questionBase[i].key]);
        } 
      }
      this.isValues=true;
    }, 500);
  }


  detectValue(question: any) {
    this.formValue[question.key] = question.value;
    
    if (question['required']) {
      if (
        !question['value'] ||
        question['value'] == '' ||
        question['value'] == 'null'
      ) {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', [
            'This field is required'
          ]);
        }, 1000);
      } else {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', []);
        }, 1000);
      }
    }
  }  
  
  
  submitVal(formValue){

    if (this.requiredCheck()) {
      let tempData = {
        id: this.roomDetail.id,
        name: formValue.name,
        size: formValue.size,
        number_of_seats: formValue.number_of_seats
      };
      this.dataService.centreRoomsUpdate(tempData).subscribe(
        (data: any) => {
          let toasterConfig: any = {
            type: 'success',
            title: 'Center Detail Edited Successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this._router.navigate(['/main/examinations/centres/view/rooms/list']);
        },
        err => {
          let toasterConfig: any = {
            type: 'error',
            title: 'Something Went Wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    }    
  }





  requiredCheck() {
    let hasError: boolean;
    console.log();
    
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]['required']) {
        if (
          !this.formValue[this._questionBase[i]['key']] ||
          this.formValue[this._questionBase[i]['key']] == '' ||
          this.formValue[this._questionBase[i]['key']] == 'null' ||
          this.formValue[this._questionBase[i]['key']] == {}
        ) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]['key'], 'errors', [
              'This field is required'
            ]);
          }, 1000);
          break;
        }
      }
    }
    // if((this.formValue['size']>=this.formValue['number_of_seats'])){
    //   hasError = true;
    // }else{
    //   setTimeout(() => {
    //     this.api.setProperty('number_of_seats', 'errors', [
    //       'This number of seat should be less then or equal to room size'
    //     ]);
    //   }, 1000);
    // }

    if (hasError) {
      return false;
    } else {
      return true;
    }
  }
  reset(){
    this._router.navigate(['/main/examinations/centres/view/rooms/list']);

  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
