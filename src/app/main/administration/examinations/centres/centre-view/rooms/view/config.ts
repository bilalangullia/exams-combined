export const VIEWNODE_INPUT: Array<any> = [
  {
    'key': 'academic_period',
    'label': 'Academic Period',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'examination',
    'label': 'Examination',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },{
    'key': 'examination_centre',
    'label': 'Centre',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'name',
    'label': 'Room Name',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'size',
    'label': 'Room Size',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'number_of_seats',
    'label': 'Number of Seats',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'modified_by',
    'label': 'Modified By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'modified_on',
    'label': 'Modified On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_by',
    'label': 'Created By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'Created On',
    'label': 'created_on',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }
]

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}