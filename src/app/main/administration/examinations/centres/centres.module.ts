import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CentresRoutingModule } from './centres-routing.module';
import { CentreListComponent } from './centre-list/centre-list.component';
import { CentreEditComponent } from './centre-edit/centre-edit.component';
import { CentreMainComponent } from './centre-main.component';
import { SharedModule } from '../../../../shared/shared.module';
import { CentreAddComponent } from './centre-add/centre-add.component';
import { CentreImportFormComponent } from './centre-import-form/centre-import-form.component';
import { CentreImportListComponent } from './centre-import-list/centre-import-list.component';

@NgModule({
  imports: [CommonModule, CentresRoutingModule, SharedModule],
  declarations: [CentreListComponent, CentreEditComponent, CentreMainComponent, CentreAddComponent, CentreImportFormComponent, CentreImportListComponent]
})
export class CentresModule {}
