import { TREE_CONFIG_AREA } from '../../../../registration/candidates/add-candidate/add.config';

export interface ITreeConfig {
    id: string;
    selectionMode: string;
    list?: object;
    expandAll?: boolean;
    expandSelected?: boolean;
  }

export const VIEWNODE_INPUT: Array<any> = [

    {
        key: 'academic_period_id',
        label: 'Academic Period',
        visible: true,
        required: true,
        order: 1,
        controlType: 'dropdown',
        options: [{
            'key': 'null',
            'value': '--Select--'
        }],
        events: true,
    },
    {
        key: 'examination_id',
        label: 'Examination',
        visible: true,
        required: true,
        order: 1,
        controlType: 'dropdown',
        options: [{
            key: null,
            value:' --Select--'
        }],
        events: true,
    },
    {
      key: "centre_code",
      label: "Centre Code",
      visible: true,
      required: true,
      order: 1,
      controlType: "text",
      type: "text"
    },
    {
      key: "centre_name",
      label: "Centre Name",
      visible: true,
      required: true,
      order: 1,
      controlType: "text",
      type: "text"
    },
    {
      key: "emis_code",
      label: "EMIS Code",
      visible: true,
      order: 1,
      controlType: "text",
      type: "text"
    },
    {
        key: 'area_id',
        label: 'Area Name',
        visible: true,
        required: true,
        controlType: 'tree',
        config: TREE_CONFIG_AREA
    },
    {
      key: "address",
      label: "Address",
      visible: true,
      order: 1,
      controlType: "text",
      type: "text"
    },
    {
      key: "postal_code",
      label: "Postal Code",
      visible: true,
      order: 1,
      controlType: "text",
      type: "text"
    },
    {
      key: "contact_person",
      label: "Contact Person",
      visible: true,
      order: 1,
      controlType: "text",
      type: "text"
    },
    {
      key: "telephone",
      label: "Telephone",
      visible: true,
      required: true,
      order: 1,
      controlType: "text",
      type: "text"
    },
  {
    key: 'ownership',
    label: 'Ownership',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{
        'key': 'null',
        'value': '--Select--'
    }],
    events: true
},
 
];


