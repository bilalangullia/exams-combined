import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi,
  KdFormEvent,
  ITreeApi,
  KdTreeDropdownEvent,
  KdTableEvent
} from 'openemis-styleguide-lib';
import { TREE_CONFIG_AREA } from '../../../../registration/candidates/add-candidate/add.config';

import { VIEWNODE_INPUT } from './config'
import { SharedService } from '../../../../../shared/shared.service';
import { DataService } from '../../../../../shared/data.service';
import { Subscription, timer } from 'rxjs';




@Component({
  selector: 'app-centre-add',
  templateUrl: './centre-add.component.html',
  styleUrls: ['./centre-add.component.css']
})
export class CentreAddComponent extends KdPageBase implements OnInit, OnDestroy {

  public defaultValues: any = {};
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  private _treeDropdownSubscription: { [key: string]: Subscription } = {};
  public isValues: boolean = false;

  public _questionBase = VIEWNODE_INPUT;
  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline'
    }
  ];
  constructor(public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private _treeDropdownEvent: KdTreeDropdownEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    private cd: ChangeDetectorRef,
    private kdTableEvent: KdTableEvent, ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }
 
  ngOnInit() {
    super.setPageTitle('Examinations ­ Centres', false);

    super.updatePageHeader();
    super.updateBreadcrumb();

    this.sharedService.getDropdownValues().subscribe(
      (data: any) => {
        this.defaultValues = data;
        if (this.defaultValues) {
          let keyArr = Object.keys(this.defaultValues.data);
          keyArr.forEach((e) => {
            let quesObj = this._questionBase.find((q) => { return q.key == e })
            if (quesObj) {
              quesObj.options.length = 0;
              quesObj.options.push(...this.defaultValues.data[e].map(item => { return { key: item.id, value: item.name } }));               
              this.getExamination(quesObj.options[0].key)
            }
          });
          this.getAreaName();
        }
      }

    )
  }

  requiredCheck() {
    let hasError: boolean;
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]['required']) {
        if ((!this.formValue[this._questionBase[i]['key']]) || (this.formValue[this._questionBase[i]['key']] == '') || (this.formValue[this._questionBase[i]['key']] == 'null') || (this.formValue[this._questionBase[i]['key']] == {})) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]['key'], 'errors', ['This field is required']);
          }, 1000)
          break;
        }
      }
    }
    if (hasError) {
      return false;
    }
    else {
      return true;
    }
  }

  detectValue(question: any) {
    this.formValue[question.key] = question.value;
   
    this.callSpecific(question);
    if (question['required']) {
      if ((!question['value']) || (question['value'] == '') || (question['value'] == 'null')) {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 1000);
      } else {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', []);
        }, 1000);
      }
    }
  }

  callSpecific(data) {
    switch (data.key) {
      case 'academic_period_id': {
        this.getExamination(data.value);
        break;
      }
    }
  }

  getExamination(id) {
    if (id != 'null') {
      this.dataService.getExamination(id).subscribe(
        (data: any) => {
          if(data.data.length>0){
          this.formValue.examination_id=data.data[0].id
          
          let temp = [...data.data.map(item => { return { key: item.id, value: item.name } })]
          this.api.setProperty('examination_id', 'options', temp)
          }
        },
        err => {
          console.log(err);
        }
      )
    } else {
      let temp = [{
        'key': 'null',
        'value': '--Select--'
      }]
      this.api.setProperty('examination_id', 'options', temp)
    }
  }


  getAreaName() {
    this.dataService.getAreaName().subscribe(
      (data: any) => {
        TREE_CONFIG_AREA.list = data.data;
        this.setDropDown();
        this.isValues = true;
      }
    )
  }


  setDropDown() {
    this.dataService.ownerShipList().subscribe((res: any) => {
      this.formValue.ownership=res.data[0].key  
    let tempList = [...res.data.map(item => { return { key: item.key, value: item.value } })];
    (tempList.length > 0) ? this.api.setProperty('ownership', 'options', tempList) : this.api.setProperty('ownership', 'options', [{ 'key': 'null', 'value': '--Select--' }])
    },
      err => {
        let temp = [{
          'key': 'null',
          'value': '--Select--'
        }]
        this.api.setProperty('ownership', 'options', temp)
      })
  }


  submitVal(formVal: any): void {
          
    if (this.requiredCheck()) {
      let tempData = {
        name: formVal.centre_name,
        code: formVal.centre_code,
        examination_id: this.formValue.examination_id,
        emis_code: formVal.emis_code,
        area_id: formVal.area_id[0].id,
        address: formVal.address,
        postal_code: formVal.postal_code,
        contact_person: formVal.contact_person,
        telephone: formVal.telephone,
        examination_centres_ownerships_id: this.formValue.ownership
         }
      this.dataService.examCentreAdd(tempData).subscribe(
        (data: any) => {
          let toasterConfig: any = {
            type: 'success',
            title: 'Centre added successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this.router.navigate(['/main/examinations/centres/list']);
        },
        err => {
          let toasterConfig: any = {
            type: 'error',
            title: 'Something Went Wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      )
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
    }
  }

  reset(){
    this.router.navigate(['main/examinations/centres/list']);

  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

}
