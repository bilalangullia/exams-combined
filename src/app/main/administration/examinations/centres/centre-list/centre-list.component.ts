import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdTableEvent,
  KdAdvFilterEvent,
  KdIntTableEvent,
  KdFilter,
  ITableConfig,
  ITableColumn,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';
import { timer } from 'rxjs';

import { TABLECOLUMN, FILTER_INPUTS } from './config';
import { DataService } from '../../../../../shared/data.service';
import { CentreService } from '../centre.service';
import { SharedService } from '../../../../../shared/shared.service';
import { TABS_ROUTER } from '../centre-view/tab.config';

@Component({
  selector: 'app-center-list',
  templateUrl: './centre-list.component.html',
  styleUrls: ['./centre-list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class CentreListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;

  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;

  public _row: Array<any> = [];
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public _tableApi: ITableApi = {};
  public defautDorpVal: any = {};
  public defaultValues: any = {};
  public Isvalue: boolean = true;

  public inputs = FILTER_INPUTS;

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    },
    action: {
      enabled: true,
      list: [
        {
          icon: 'far fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.setCandidateId(_rowNode.data);
          }
        }
      ]
    }
  };

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CentreCode,
    TABLECOLUMN.CentreName,
    TABLECOLUMN.AreaCode,
    TABLECOLUMN.AreaName
  ];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    private _toolbarEvent: KdToolbarEvent,
    public _kdalert: KdAlertEvent,
    public centreService: CentreService,
    public dataService: DataService,
    public sharedService: SharedService
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }
  ngOnInit() {
    super.setPageTitle('Examinations Centres', false);
    super.setToolbarMainBtns([
      {
        type: 'add',
        path: 'main/examinations/centres/add'
      },{
        type: 'import',
        path: 'main/examinations/centres/import'
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      if (event.length > 2 || event === '') {
        this.searchCentreList(this.defautDorpVal, event);
      }
    });

    super.updatePageHeader();
    super.updateBreadcrumb();

    this.setDropDownValue();
    TABS_ROUTER[0].isActive = true;
  }

  setDropDownValue() {
    timer(100).subscribe(() => {
      this.sharedService.getDropdownValues().subscribe((data: any) => {
        this.defaultValues = data;
        this.defautDorpVal = {
          academic_period_id: data.data.academic_period_id[0].id
        };
        if (this.defaultValues) {
          let keyArr = Object.keys(this.defaultValues.data);
          keyArr.forEach((e) => {
            let quesObj = this.inputs.find((q) => {
              return q.key == e;
            });
            if (quesObj) {
              let quesObj1 = [];
              quesObj1.push(
                ...this.defaultValues.data[e].map((item) => {
                  return { key: item.id, value: item.name };
                })
              );
              this._updateView.setInputProperty('academic_period_id', 'options', quesObj1);
            }
          });
          this.setExamination(this.defautDorpVal.academic_period_id);
        }
      });
    });
  }

  setExamination(data: any) {
    this.dataService.getExamination(data).subscribe((res: any) => {
      if (res.data.length) {
        let examTemp = [];
        this.defautDorpVal.examination_id = res.data[0].id;
        res.data.forEach((item) => {
          examTemp.push({ key: item.id, value: item.name });
        });
        this._updateView.setInputProperty('examination_id', 'options', examTemp);
        this.setCenterList(this.defautDorpVal);
      } else {
        this.loading = false;
        this._updateView.setInputProperty('examination_id', 'options', [{ key: '', value: '-- Select --' }]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No records found',
          body: 'Please select another accedamic periods',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this._kdalert.error(toasterConfig);
      }
    });
  }

  setCenterList(data) {
    this.Isvalue = true;

    this.dataService.examCentreList(data).subscribe((data: any) => {
      if (data.data.length > 0) {
        this.setCenterListFilter(data.data);
      } else {
        this.loading = false;
        let toasterConfig: any = {
          type: 'error',
          title: 'No records found',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this._kdalert.error(toasterConfig);
      }
    });
  }

  setCandidateId(data) {
    this.defautDorpVal.id = data.id;
    this.centreService.centerId = {};
    console.log('this.defautDorpVal...this.defautDorpVal', this.defautDorpVal);
    this.centreService.setCentreId(this.defautDorpVal);
    this.router.navigate(['/main/examinations/centres/view']);
  }

  setCenterListFilter(data) {
    this.resetFilter();

    if (data.length > 0) {
      this._row = data;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (data[j][this.tableColumns[i]['field']] && data[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(data[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
      this.loading = false;
      this.Isvalue = false;
    } else {
      this.loading = false;
      this._row.length = 0;
      let toasterConfig: any = {
        type: 'error',
        title: 'No records found',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this._kdalert.error(toasterConfig);
    }
  }

  detectValue(event: any) {
    this.defautDorpVal[event.key] = event.value;
    console.log('even');
    if (event.key == 'academic_period_id') {
      this.resetDropDown();
      this.setExamination(event.value);
    } else {
      this.setCenterList(this.defautDorpVal);
    }
  }

  searchCentreList(data: any, id) {
    this.dataService.examCentreSearch(data, id).subscribe((res: any) => {
      this.setCenterListFilter(res.data);
    });
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  resetDropDown() {
    for (let i = 0; i < this.inputs.length; i++) {
      if (this.inputs[i].key != 'academic_period_id') {
        this._updateView.setInputProperty(this.inputs[i].key, 'value', '');
      }
    }
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
