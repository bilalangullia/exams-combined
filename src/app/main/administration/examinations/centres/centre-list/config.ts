export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: 0,
        value: '-- Select Options 1 --'
      }
    ],
    events: true
  },
  {
    key: 'examination_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: '',
        value: '-- Select --'
      }
    ],
    events: true
  }
];

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  id?: Column;
  CentreCode?: Column;
  CentreName?: Column;
  EMISCode?: Column;
  AreaCode?: Column;
  AreaName?: Column;
  AcademicPeriodId?: Column;
}

export const TABLECOLUMN: ListColumn = {
  id: {
    headerName: 'ID',
    field: 'id',
    visible: false
  },
  CentreCode: {
    headerName: 'Centre Code',
    field: 'centre_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  CentreName: {
    headerName: 'Centre Name',
    field: 'centre_name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EMISCode: {
    headerName: 'EMIS Code',
    field: 'emis_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  AreaCode: {
    headerName: 'Area Code',
    field: 'area_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  AreaName: {
    headerName: 'Area Name',
    field: 'area_name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  AcademicPeriodId: {
    headerName: 'Academic Year',
    field: 'academic_period_id',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const tempData = [
  {
    centre_code: 'E09',
    centre_name: 'Delta SS',
    emis_code: '123',
    area_code: 'E',
    area_name: 'Khomas'
  }
];
