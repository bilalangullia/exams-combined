import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CentreListComponent } from './centre-list/centre-list.component';
import { CentreMainComponent } from './centre-main.component';
import { CentreEditComponent } from './centre-edit/centre-edit.component';
import{CentreAddComponent} from './centre-add/centre-add.component';
import {CentreImportFormComponent} from './centre-import-form/centre-import-form.component'
import {CentreImportListComponent} from './centre-import-list/centre-import-list.component'

const routes: Routes = [
  {
    path: '',
    component: CentreMainComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: CentreListComponent
      },
      {
        path: 'view',
        loadChildren: './centre-view/centre-view.module#CentreViewModule'
      },
      {
        path: 'edit',
        component: CentreEditComponent
      },
      {
        path: 'add',
        component: CentreAddComponent
      },
      {
        path: 'import',
        component: CentreImportFormComponent
      },{
        path: 'detail',
        component: CentreImportListComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CentresRoutingModule {}
