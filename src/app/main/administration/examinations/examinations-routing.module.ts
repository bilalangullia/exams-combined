import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../../../shared/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'exams' },
  { path: 'exams', loadChildren: './exams/exams.module#ExamsModule' },
  { path: 'centres', loadChildren: './centres/centres.module#CentresModule' },
  { path: 'gradingtypes', loadChildren: './grading-type/grading-type.module#GradingTypeModule' },
  {
    path: 'multiple-choice',
    loadChildren: './examination-multiple-choice/examination-multiple-choice.module#ExaminationMultipleChoiceModule'
  },
  { path: 'markers', loadChildren: './markers/markers.module#MarkersModule' },
  { path: 'fees', loadChildren: './fee/fee.module#FeeModule' }

  // path: '',
  //  redirectTo: 'exams',
  //   canActivate: [AuthGuard],
  // children: [
  //   // {path: 'review-criteria', loadChildren: ''},
  //   // {path: 'scalings', loadChildren: ''},
  //   // {path: 'apportionment', loadChildren: ''}
  // ]
];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class ExaminationsRoutingModule {}
