import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class MarkersDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /* Add token to request */
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      // 'Content-Type': 'application/json'
      return { headers: headers };
    }
  }

  /* Get Dropdown for Academic Period */
  getDropdownAcademicPeriod(): Observable<Object> {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}?entities=academicPeriods`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Get Dropdown for Examination */
  getDropdownExaminationNme(academicPeriodId: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${academicPeriodId}/${urls.examination}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getDropdownExaminationCentre(examination_id) {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${examination_id}/examcentres`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /* Get Dropdown for Education Subject */
  getDropdownEducationSubject() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/education-subject`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getDropdownClassification() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.classification}-${urls.dropdown}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /* Dropdown from entitiy names */
  getDropdownOptions(entities: Array<string>) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/dropdown?entities=${entities.join(',')}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getDropdownAddressRegion() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.address}/${urls.region}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getDropdownCountryRegion() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.country}/${urls.region}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Get Markers List */
  getMarkersList(
    academicPeriodId: number,
    examinationId: number,
    examinationCentreId: number,
    examinationSubjectId: number
  ): Observable<Object> {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${urls.markerList}?academic_period_id=${academicPeriodId}&examination_id=${examinationId}&examination_centre_id=${examinationCentreId}&education_subjects_id=${examinationSubjectId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  searchMarkersList(
    academicPeriodId: number,
    examinationId: number,
    examinationCentreId: number,
    educationSubjectId: number,
    keyword: string
  ) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${urls.markerList}?academic_period_id=${academicPeriodId}&examination_id=${examinationId}&examination_centre_id=${examinationCentreId}&education_subjects_id=${educationSubjectId}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Get Marker View Details */
  getMarkerViewDetails(openEmisNo: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${openEmisNo}/marker-view`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Get Marker Edit Details */
  getMarkerEditDetails(openEmisNo: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${openEmisNo}/marker-edit`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* http://openemis.n2.iworklab.com/api/administration/examinations/markers/abcXYZ001/update-marker */
  /* { "first_name": "test", "last_name":  "testmjf", "gender_id": 1, "date_of_birth": { "text": "2004-2-6", "obj": { "year": 2004, "month": 1, "day": 1 } }, "examination_centre_id":2, "examination_id":2, "academic_period_id": 4, "area_id": 2, "education_subjects_id":1, "marker_id":"abcXYZ001", "openemis_no":"5e6b60cc39dbd", "classification":1, "subjects": [ { "id": 1, "subject_code": "6143", "subject_name": "Accounting" }, "id": 2, "subject_code": "6108", "subject_name" :"Afrikaans as a Second Language" } ] } */
  /* Save Edit Details */
  saveEditMarkerDetails(open_emis_no: string, payload: Object) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${open_emis_no}/${urls.updateMarker}`,
        payload,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Get Autocomplete Examiner ID */
  getAutocompleteExaminerId(searchKey: string): Observable<any> {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${searchKey}/${urls.autoExaminerId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Get Autocomplete Open EMIS ID */
  getAutocompleteOpenEMISNo(searchKey: string): Observable<any> {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.autoEmisId}/${searchKey}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Save new Marker */
  saveMarker(payload: Object) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${urls.addMarker}`,
        payload,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** QUALIFICATION APIS ***/

  /* Listing */
  getQualificationsList(openEmisNo: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.qualifications}/${openEmisNo}/${urls.qualificationList}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  searchQualificationsList(
    academicPeriodId: number,
    examinationId: number,
    examinationCentreId: number,
    educationSubjectId: number,
    keyword: string
  ) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.markers}/${urls.markerList}?academic_period_id=${academicPeriodId}&examination_id=${examinationId}&examination_centre_id=${examinationCentreId}&education_subjects_id=${educationSubjectId}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Qualifications View Details */
  getQualificationsViewDetails(qualificationId: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.qualifications}/${qualificationId}/${urls.qualificationView}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Qualifications Edit Details */
  getQualificationsEditDetails(qualificationId: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.qualifications}/${qualificationId}/${urls.qualificationEdit}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Qualificaition Dropdown Options Title */
  getDropdownTitle() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/qualificationtitlelist`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /* Qualification Dropdown Options Level */
  getDropdownLevel() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/qualificationlevel`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /* Qualification Dropdown Options Field of Study */
  getDropdownFieldOfStudy() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/qualificationfield-of-study`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getDropdownGraduateYear() {
    let startYear = 1950;
    const now = new Date();
    let graduateyear = [];
    for (let i = startYear; i <= now.getFullYear(); i++) {
      graduateyear.push({ id: i, name: startYear });
      startYear++;
    }

    return graduateyear.sort((a, b) => b - a);

    /* PENDING API */
    // return this.httpClient.get(``, this.setHeader()).pipe(catchError(this.handleError));
  }

  getDropdownSpecialisations() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/qualification-specialisation`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  saveEditQualificationDetails(qualificationId: string, payload: Object) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.qualifications}/${qualificationId}/qualification-update`,
        payload,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  saveQualification(openEmisNo: string, payload: Object) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.qualifications}/${openEmisNo}/qualification-add`,
        payload,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
