import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { MarkersRoutingModule } from './markers-routing.module';
import { MarkersService } from './markers.service';
import { MarkersDataService } from './markers-data.service';

@NgModule({
  imports: [CommonModule, MarkersRoutingModule, SharedModule],
  providers: [MarkersService, MarkersDataService]
})
export class MarkersModule {}
