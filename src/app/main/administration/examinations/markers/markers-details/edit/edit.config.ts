import { ITreeConfig } from 'openemis-styleguide-lib';

import { ITableQuestionInterface } from '../../markers-interfaces';
import { TABLE_COLUMN_LIST } from '../../markers.config';

const CURRENT_DATE = new Date();

const TREE_CONFIG_AREA: ITreeConfig = {
  id: 'areaName',
  list: [],
  selectionMode: 'single'
};

const TREE_CONFIG_BIRTHPLACE_AREA: ITreeConfig = {
  id: 'birthplaceArea',
  list: [],
  selectionMode: 'single'
};

const TREE_CONFIG_ADDRESS_AREA: ITreeConfig = {
  id: 'addressArea',
  list: [],
  selectionMode: 'single'
};

const SUBJECTS_TABLE: ITableQuestionInterface = {
  selection: {
    oneshot: {
      question: {
        key: 'subjects',
        label: 'Subjects',
        visible: true,
        controlType: 'table',
        row: [],
        column: [TABLE_COLUMN_LIST.subject_code, TABLE_COLUMN_LIST.subject_name],
        config: {
          id: 'subjectsTable',
          rowIdKey: 'id',
          gridHeight: 400,
          loadType: 'oneshot',
          paginationConfig: { pagesize: 10, total: 100 },
          selection: {
            type: 'all',
            value: [],
            returnKey: ['subject_id', 'subject_code', 'subject_name']
          },
          click: {
            type: 'selection'
          }
        }
      }
    }
  }
};

export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'openemis_no',
    label: 'OpenEMIS ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'marker_id',
    label: 'Marker ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'classification',
    label: 'Classification',
    visible: true,
    controlType: 'dropdown',
    format: 'string',
    type: 'string',
    options: [{ key: null, value: '-- Select --' }]
    // readonly: true
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'middle_name',
    label: 'Middle Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'third_name',
    label: 'Third Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'gender_id',
    label: 'Gender',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }],
    required: true
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    visible: true,
    controlType: 'date',
    format: 'string',
    type: 'string',
    required: true,
    config: {
      firstDayOfWeek: 1
    },
    minDate: '1950-01-01',
    maxDate: `${CURRENT_DATE.getFullYear()}-${CURRENT_DATE.getMonth() + 1}-${CURRENT_DATE.getDate()}`
  },
  {
    key: 'nationality_id',
    label: 'Nationality',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }]
  },
  {
    key: 'identity_type_id',
    label: 'Identity Type',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }]
  },
  {
    key: 'identity_number',
    label: 'Identity Number',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'address',
    label: 'Address',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'postal_code',
    label: 'Postal Code',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'address_area_id',
    label: 'Address Area',
    visible: true,
    controlType: 'tree',
    config: TREE_CONFIG_ADDRESS_AREA
  },
  {
    key: 'birthplace_area_id',
    label: 'Birthplace Area',
    visible: true,
    controlType: 'tree',
    config: TREE_CONFIG_BIRTHPLACE_AREA
  },
  {
    key: 'area_id',
    label: 'Area Name',
    visible: true,
    controlType: 'tree',
    config: TREE_CONFIG_AREA,
    required: true
  },
  {
    key: 'examination_centre_id',
    label: 'Exam Centre',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }]
  },
  SUBJECTS_TABLE['selection']['oneshot']['question']
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
