import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import {
  KdPageBaseEvent,
  KdPageBase,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { MarkersService } from '../../markers.service';
import { MarkersDetailsService } from '../markers-details.service';
import { IFilters, IEditOptions } from '../../markers-interfaces';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  private pageTitle: string = 'Examinations - Marker';
  public pageHeader: IPageheaderConfig;
  public breadcrumbList: IBreadcrumbConfig;
  public questionBase: any = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  public api: IDynamicFormApi = {};
  private openEmisNo: string = null;
  private subjectsList: Array<any> = [];
  private details: Object = {};
  private academicPeriod: any = {};
  private filters: IFilters = {};

  /* Subscriptions */
  private openEmisNoSub: Subscription;
  private filtersSub: Subscription;
  private editOptionsSub: Subscription;
  private subjectsListSub: Subscription;
  private markerDetailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private markersDetailsService: MarkersDetailsService,
    private markersService: MarkersService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbArray: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbArray;
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.openEmisNoSub = this.markersService.openEmisNoChanged.subscribe((openEmisNo: string) => {
      if (!openEmisNo) {
        this.openEmisNo = null;
        this.router.navigate(['main/examinations/markers']);
      } else {
        this.openEmisNo = openEmisNo;
        this.markersDetailsService.getEditDetails(openEmisNo);
      }
    });

    timer(100).subscribe(() => {
      if (this.openEmisNo) {
        this.markersDetailsService.getDropdownOptions(['gender', 'nationality', 'identityTypes']);
        this.markersDetailsService.getDropdownAddressRegion();
        this.markersDetailsService.getDropdownCountryRegion();
        this.markersDetailsService.getDropdownClassification();

        this.markersDetailsService.getOptionsEducationSubject();

        this.filtersSub = this.markersService.filtersChanged.subscribe((filters: IFilters) => {
          if (
            filters['academic_period_id'] &&
            filters['academic_period_id']['value'] &&
            filters['examination_name'] &&
            filters['examination_name']['value']
          ) {
            this.filters = filters;
          }

          if (filters['academic_period_id'] && filters['academic_period_id']['id']) {
            this.academicPeriod = { ...filters['academic_period_id'] };
            this.markersDetailsService.getDropdownExaminationCentre(filters['academic_period_id']['id']);
          }
        });

        this.subjectsListSub = this.markersDetailsService.subjectsListChanged.subscribe((subjects: Array<any>) => {
          if (subjects.length) {
            this.subjectsList = subjects;
          }
        });

        this.editOptionsSub = this.markersDetailsService.editOptionsChanged.subscribe((options: IEditOptions) => {
          if (
            options &&
            options['gender_id'] &&
            options['identity_type_id'] &&
            options['nationality_id'] &&
            options['classification'] &&
            options['examination_centre_id'] &&
            options['address_area_id'] &&
            options['area_id'] &&
            options['birthplace_area_id']
          ) {
            this.setOptions(options);
          }
        });

        this.markerDetailsSub = this.markersDetailsService.markerEditDetailsChanged.subscribe((details: Object) => {
          if (details) {
            this.details = details;
          }
        });
      }
    });
  }

  setOptions(options: IEditOptions) {
    this.questionBase.forEach(question => {
      if (question['controlType'] === 'dropdown') {
        question['options'] = options[question['key']]
          ? [
              { key: null, value: '-- Select --' },
              ...options[question['key']].map(item => ({ key: item['id'], value: item['name'] }))
            ]
          : [{ key: null, value: '-- Select --' }];
      } else if (question['controlType'] == 'tree') {
        question['config']['list'] = options[question['key']];
      } else if (question['controlType'] === 'table') {
        question['row'] = this.subjectsList.map((item: any, index: number) => ({
          id: index,
          subject_id: item['id'],
          subject_code: item['code'],
          subject_name: item['name']
        }));
        question['value'] = [];
      }
    });

    setTimeout(() => {
      this.setDetails();
    }, 0);
  }

  setDetails() {
    this.details['academic_period'] = this.filters['academic_period_id']['value'];
    this.details['examination_id'] = this.filters['examination_name']['value'];

    this.questionBase.forEach(question => {
      if (question['controlType'] == 'dropdown') {
        question['value'] =
          this.details[question['key']] && this.details[question['key']]['key']
            ? this.details[question['key']]['key']
            : null;
      } else if (question['controlType'] == 'tree') {
        // question['value'] = this.details[question['key']]  && this.details[question['key']]['key'] ? this.details[question['key']]['key'] : [];
      } else if (question['controlType'] == 'table') {
        question['value'] = this.details[question['key']];
      } else {
        if (question['key'] === 'academic_period_id') {
          question['value'] = this.academicPeriod['value']['value'];
        } else {
          question['value'] = this.details[question['key']]
            ? this.details[question['key']]['value']
              ? this.details[question['key']]['value']
              : this.details[question['key']]
            : '';
        }
      }
    });
    this.loading = false;
  }

  detectValue($event) {
    // console.log('EditComponent -> detectValue -> $event', $event);
  }

  requiredCheck(formValue) {
    let hasError: boolean = false;
    this.questionBase.forEach(question => {
      if (question['required'] && !formValue[question['key']]) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 0);
      }
    });
    return hasError;
  }

  submit(event) {
    event['area_id'] = event['area_id'].length ? event['area_id'][0]['id'] : null;
    event['subjects'] = event['subjects'].map(item => ({
      id: item['id'],
      subject_code: item['subject_code'],
      subject_name: item['subject_name']
    }));

    if (!this.requiredCheck(event)) {
      let payload = { ...event };
      this.markersDetailsService.saveEditDetails(this.openEmisNo, payload);
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields!',
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    }
  }

  cancel() {
    this.markersDetailsService.resetOptions();
    this.router.navigate(['main/examinations/markers']);
  }

  ngOnDestroy() {
    this.openEmisNoSub.unsubscribe();
    if (this.filtersSub) {
      this.filtersSub.unsubscribe();
    }
    if (this.editOptionsSub) {
      this.editOptionsSub.unsubscribe();
    }
    if (this.subjectsListSub) {
      this.subjectsListSub.unsubscribe();
    }
    if (this.markerDetailsSub) {
      this.markerDetailsSub.unsubscribe();
    }

    this.markersDetailsService.resetOptions();
    super.destroyPageBaseSub();
  }
}
