import { IKdTabs } from '../../../../../../shared/kdComponents/kdInterfaces';

const ROUTER_BASE_PATH = 'main/examinations/markers/details';

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'Marker', routerPath: ROUTER_BASE_PATH + '/view', isActive: true },
  { tabName: 'Qualifications', routerPath: ROUTER_BASE_PATH + '/qualifications' }
];
