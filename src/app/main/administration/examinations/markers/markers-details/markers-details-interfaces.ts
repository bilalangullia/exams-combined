export interface IEditPayload {
  // first_name?: string;
  // "last_name"?:  string; "gender_id": number,
  // "date_of_birth": { "text": string, "obj": { "year": number, "month": number, "day": number } }; "examination_centre_id":2, "examination_id":2, "academic_period_id": 4, "area_id": 2, "education_subjects_id":1, "marker_id":"abcXYZ001", "openemis_no":"5e6b60cc39dbd", "classification":1, "subjects": [ { "id": 1, "subject_code": "6143", "subject_name": "Accounting" }, "id": 2, "subject_code": "6108", "subject_name" :"Afrikaans as a Second Language" } ]
}

/* 
{ "first_name": "test", "last_name":  "testmjf", "gender_id": 1, "date_of_birth": { "text": "2004-2-6", "obj": { "year": 2004, "month": 1, "day": 1 } }, "examination_centre_id":2, "examination_id":2, "academic_period_id": 4, "area_id": 2, "education_subjects_id":1, "marker_id":"abcXYZ001", "openemis_no":"5e6b60cc39dbd", "classification":1, "subjects": [ { "id": 1, "subject_code": "6143", "subject_name": "Accounting" }, "id": 2, "subject_code": "6108", "subject_name" :"Afrikaans as a Second Language" } ] }
*/
