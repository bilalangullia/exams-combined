import { Injectable } from '@angular/core';
import { MarkersService } from '../markers.service';
import { IFilters } from '../markers-interfaces';
import { BehaviorSubject } from 'rxjs';
import { MarkersDataService } from '../markers-data.service';
import { SharedService } from '../../../../../shared/shared.service';
import { Router } from '@angular/router';

@Injectable()
export class MarkersDetailsService {
  private filters: IFilters;
  public filtersChanged = new BehaviorSubject<IFilters>(null);
  private markerViewDetails: Object = null;
  public markerViewDetailsChanged = new BehaviorSubject<Object>(null);
  private markerEditDetails: Object = null;
  public markerEditDetailsChanged = new BehaviorSubject<Object>(null);
  private editOptions: any = null;
  public editOptionsChanged = new BehaviorSubject<any>(this.editOptions);
  private subjectsList: Array<any> = [];
  public subjectsListChanged = new BehaviorSubject<Array<any>>([...this.subjectsList]);

  constructor(
    private markersService: MarkersService,
    private markersDataService: MarkersDataService,
    private sharedService: SharedService,
    private router: Router
  ) {}

  getFilters() {
    this.markersService.filtersChanged.subscribe((filters: IFilters) => {
      this.filters = { ...filters };
      this.filtersChanged.next({ ...this.filters });
    });
  }

  getDropdownExaminationCentre(academicPeriodId: number) {
    this.markersDataService.getDropdownExaminationCentre(academicPeriodId).subscribe(
      (res: any) => {
        this.editOptions = {
          ...this.editOptions,
          examination_centre_id: res['data']
        };
        this.editOptionsChanged.next({ ...this.editOptions });
      },
      err => {
        this.editOptions = [{ id: null, name: '--Select --' }];
        this.editOptions.next({ ...this.editOptions });
      }
    );
  }

  getDropdownClassification() {
    this.markersDataService.getDropdownClassification().subscribe(
      (res: any) => {
        this.editOptions = {
          ...this.editOptions,
          classification: res['data']
        };
        this.editOptionsChanged.next({ ...this.editOptions });
      },
      err => {
        this.editOptions = [{ id: null, name: '--Select --' }];
        this.editOptions.next({ ...this.editOptions });
      }
    );
  }

  getDropdownOptions(entities: Array<string>) {
    this.markersDataService.getDropdownOptions(entities).subscribe(
      (res: any) => {
        this.editOptions = { ...this.editOptions, ...res['data'] };
        this.editOptionsChanged.next({ ...this.editOptions });
      },
      err => {
        this.editOptionsChanged.next({ ...this.editOptions });
      }
    );
  }

  getDropdownAddressRegion() {
    this.markersDataService.getDropdownAddressRegion().subscribe(
      (res: any) => {
        this.editOptions = {
          ...this.editOptions,
          address_area_id: res['data'],
          birthplace_area_id: res['data']
        };
        this.editOptionsChanged.next({ ...this.editOptions });
      },
      err => {
        this.editOptionsChanged.next({ ...this.editOptions });
      }
    );
  }

  getDropdownCountryRegion() {
    this.markersDataService.getDropdownCountryRegion().subscribe(
      (res: any) => {
        this.editOptions = { ...this.editOptions, area_id: res['data'] };
        this.editOptionsChanged.next({ ...this.editOptions });
      },
      err => {
        this.editOptionsChanged.next({ ...this.editOptions });
      }
    );
  }

  getOptionsEducationSubject() {
    this.markersDataService.getDropdownEducationSubject().subscribe(
      (res: any) => {
        this.subjectsList = res['data'];
        this.subjectsListChanged.next([...this.subjectsList]);
      },
      err => {
        this.subjectsList = [];
        this.subjectsListChanged.next([...this.subjectsList]);
      }
    );
  }

  resetOptions() {
    this.editOptions = null;
    this.editOptionsChanged.next({ ...this.editOptions });
  }

  getViewDetails(openEmisNo: string) {
    this.markersDataService.getMarkerViewDetails(openEmisNo).subscribe(
      (res: any) => {
        this.markerViewDetails = res['data'][0];
        this.markerViewDetailsChanged.next({ ...this.markerViewDetails });
      },
      err => {
        this.markerViewDetails = null;
        this.markerViewDetailsChanged.next({ ...this.markerViewDetails });
      }
    );
  }

  getEditDetails(openEmisNo: string) {
    this.markersDataService.getMarkerEditDetails(openEmisNo).subscribe(
      (res: any) => {
        this.markerEditDetails = res['data'][0];
        this.markerEditDetailsChanged.next({ ...this.markerEditDetails });
      },
      err => {
        this.markerEditDetails = null;
        this.markerEditDetailsChanged.next({ ...this.markerEditDetails });
      }
    );
  }

  getAutoSearchExaminerId(searchKey: string) {
    this.markersDataService.getAutocompleteExaminerId(searchKey).subscribe(
      (res: any) => {
        console.log('MarkersDetailsService -> getAutoSearchExaminerId -> res', res);
      },
      err => {
        console.log('MarkersDetailsService -> getAutoSearchExaminerId -> err', err);
      }
    );
  }

  getAutoSearchOpenEmisNo(searchKey: string) {
    this.markersDataService.getAutocompleteOpenEMISNo(searchKey).subscribe(
      (res: any) => {
        console.log('MarkersDetailsService -> getAutoSearchOpenEmisNo -> res', res);
      },
      err => {
        console.log('MarkersDetailsService -> getAutoSearchOpenEmisNo -> err', err);
      }
    );
  }

  saveEditDetails(openEmisNo: string, payload: any) {
    this.markersDataService.saveEditMarkerDetails(openEmisNo, payload).subscribe(
      (res: any) => {
        if (res) {
          let toasterConfig: any = {
            type: 'success',
            title: 'Marker Updated.',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        } else {
          let toasterConfig: any = {
            type: 'error',
            title: 'Error while saving!',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
        this.router.navigate(['main/examinations/markers']);
      },
      err => {
        let toasterConfig: any = {
          type: 'error',
          title: 'Error while saving!',
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }
}
