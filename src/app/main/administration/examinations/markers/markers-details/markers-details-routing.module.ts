import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntryComponent } from './entry/entry.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { QualificationsModule } from './qualifications/qualifications.module';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
    children: [
      { path: '', redirectTo: 'view', pathMatch: 'full' },
      { path: 'view', component: ViewComponent },
      { path: 'edit', component: EditComponent },
      { path: 'qualifications', loadChildren: './qualifications/qualifications.module#QualificationsModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarkersDetailsRoutingModule {}
