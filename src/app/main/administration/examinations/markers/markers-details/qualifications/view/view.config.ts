export const QUESTION_BASE: Array<any> = [
  {
    key: 'title',
    label: 'Title',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'level',
    label: 'Level',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'field_of_study',
    label: 'Field Of Study',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'specialisation',
    label: 'Specialisation',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'subjects',
    label: 'Subjects',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'country',
    label: 'Country',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'institution',
    label: 'Institution',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'document_no',
    label: 'Document No.',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'graduate_year',
    label: 'Graduate Year',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'grade_score',
    label: 'Grade/Score',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_user_id',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_user_id',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];
