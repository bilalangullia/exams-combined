import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBaseEvent,
  KdPageBase,
  IPageheaderConfig,
  IPageheaderApi,
  IBreadcrumbConfig,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { QUESTION_BASE, FORM_BUTTONS } from './add.config';
import { QualificationsService } from '../qualifications.service';
import { IDropdownOptions } from '../qualifications-interfaces';
import { Subscription } from 'rxjs';
import { MarkersService } from '../../../markers.service';
import { SharedService } from '../../../../../../../shared/shared.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public smallLoader: number = null;
  private pageTitle: string = 'Examinations - Qualifications';
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public api: IDynamicFormApi = {};

  public questionBase = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  private openEmisNo: string = null;

  /* Subscriptions */
  private openEmisNoSub: Subscription;
  private dropdownOptionsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private qualificationsService: QualificationsService,
    private markersService: MarkersService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj: IBreadcrumbConfig): void => {
      this.breadcrumbList = breadcrumbObj;
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.openEmisNoSub = this.markersService.openEmisNoChanged.subscribe((openEmisNo: string) => {
      if (!openEmisNo) {
        this.router.navigate(['main/examinations/markers']);
      } else {
        this.loading = false;
        this.openEmisNo = openEmisNo;
        this.qualificationsService.getDropdownTitle();
        this.qualificationsService.getDropdownLevel();
        this.qualificationsService.getDropdownFieldOfStudy();
        this.qualificationsService.getDropdownOptions(['country']);
        this.qualificationsService.getDropdownGraduateYear();
        this.qualificationsService.getOptionsSubjects();
        this.qualificationsService.getOptionsSpecialisations();

        this.dropdownOptionsSub = this.qualificationsService.dropdownOptionsChanged.subscribe(
          (options: IDropdownOptions) => {
            if (
              options &&
              options['title'] &&
              options['level'] &&
              options['field_of_study'] &&
              options['country'] &&
              options['subjects'] &&
              options['specialisation']
              // options['graduate_year']
            ) {
              this.setOptions(options);
            }
          }
        );
      }
    });
  }

  setOptions(options: IDropdownOptions) {
    this.questionBase.forEach(question => {
      if (
        question['key'] == 'title' ||
        question['key'] == 'field_of_study' ||
        question['key'] == 'country' ||
        question['key'] == 'graduate_year'
      ) {
        this.api.setProperty(
          question['key'],
          'options',
          options[question['key']]
            ? [
                { key: null, value: '-- Select --' },
                ...options[question['key']].map(item => ({ key: item['id'], value: item['name'] }))
              ]
            : [{ key: null, value: '-- Select --' }]
        );
      } else if (question['key'] == 'subjects' || question['key'] == 'specialisation') {
        this.api.setProperty(
          question['key'],
          'options',
          options[question['key']].map(item => ({ key: item['id'], value: item['name'] }))
        );
      }
    });
  }

  detectValue(event) {
    // console.log('AddComponent -> detectValue -> event', event);
  }

  cancelAction() {
    this.qualificationsService.resetOptions();
    this.router.navigate(['main/examinations/markers/details/qualifications']);
  }

  requiredCheck(formValue) {
    let hasError: boolean = false;
    this.questionBase.forEach(question => {
      if (question['required'] && !formValue[question['key']]) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 100);
      }
    });
    return hasError;
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields!',
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      const formData = new FormData();
      formData.append('qualification_title_id', event['title']);
      formData.append('level', event['level'] ? event['level'] : null);
      formData.append('education_field_of_study_id', event['field_of_study']);
      formData.append(
        'specialisation',
        event.specialisation ? event.specialisation.map(item => ({ id: item['key'], subject_name: item['value'] })) : []
      );
      formData.append(
        'subjects',
        event.subjects.map(item => ({ id: item['key'], subject_name: item['value'] }))
      );
      formData.append('qualification_country_id', event['country']);
      formData.append('qualification_institution', event['qualification_institution']);
      formData.append('document_no', event['document_no'] ? event['document_no'] : '');
      formData.append('graduate_year', event['graduate_year'] ? event['graduate_year'] : '');
      formData.append('gpa', event['gpa'] ? event['gpa'] : '');
      formData.append('file_name', event['file_name'] ? event['file_name'] : '');
      this.qualificationsService.saveQualification(this.openEmisNo, formData);
    }
  }

  ngOnDestroy() {
    if (this.dropdownOptionsSub) {
      this.dropdownOptionsSub.unsubscribe();
    }
    if (this.openEmisNoSub) {
      this.openEmisNoSub.unsubscribe();
    }
    this.qualificationsService.resetOptions();
    super.destroyPageBaseSub();
  }
}
