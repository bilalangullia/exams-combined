import { IListColumn } from '../qualifications-interfaces';

export const TABLE_ID: string = 'qualificationsList';
export const PAGE_SIZE: number = 10;
export const TOTAL_ROW: number = 1000;

export const TABLE_COLUMNS: IListColumn = {
  GraduateYear: {
    headerName: 'Graduate Year',
    field: 'graduate_year',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  Level: {
    headerName: 'Level',
    field: 'level',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  Title: {
    headerName: 'Title',
    field: 'title',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  DocumentNo: {
    headerName: 'DocumentNo',
    field: 'document_no',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  Institution: {
    headerName: 'Institution',
    field: 'institution',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  FileType: {
    headerName: 'File Type',
    field: 'file_type',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  FieldOfStudy: {
    headerName: 'Field of Study',
    field: 'field_of_study',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  }
};
