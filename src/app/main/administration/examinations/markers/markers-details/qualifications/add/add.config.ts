export const QUESTION_BASE: Array<any> = [
  {
    key: 'title',
    label: 'Title',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true,
    required: true
  },
  {
    key: 'level',
    label: 'Level',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'field_of_study',
    label: 'Field Of Study',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true,
    required: true
  },
  {
    key: 'specialisation',
    label: 'Specialisation',
    visible: true,
    controlType: 'text',
    type: 'multiselect',
    multiselect: true,
    clickToggleDropdown: true,
    lengthToSearch: 1,
    options: [{ key: null, value: '-- Select --' }],
    value: []
  },
  {
    key: 'subjects',
    label: 'Subjects',
    visible: true,
    controlType: 'text',
    type: 'multiselect',
    multiselect: true,
    clickToggleDropdown: true,
    lengthToSearch: 1,
    options: [{ key: null, value: '-- Select --' }],
    value: []
  },
  {
    key: 'country',
    label: 'Country',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true,
    required: true
  },
  {
    key: 'qualification_institution',
    label: 'Institution',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'document_no',
    label: 'Document No.',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'graduate_year',
    label: 'Graduate Year',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'gpa',
    label: 'Grade/Score',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'file_name',
    label: 'Attachment',
    visible: true,
    // required: true,
    controlType: 'file-input',
    type: 'file',
    config: {
      infoText: [
        { text: 'Format Supported: xls, xlsx, ods, zip ' },
        { text: 'File size should not be larger than 512KB.' },
        { text: 'Recommended Maximum Records: 2000' }
      ]
    }
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
