import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { MarkersDataService } from '../../markers-data.service';
import { IDropdownOptions } from './qualifications-interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { Router } from '@angular/router';

@Injectable()
export class QualificationsService {
  private qualificationId: string = null;
  public qualificationIdChanged = new BehaviorSubject<string>(this.qualificationId);
  private qualificationsList: Array<any> = [];
  public qualificationsListChanged = new BehaviorSubject<Array<any>>(this.qualificationsList);
  private viewDetails: Object = null;
  public viewDetailsChanged = new BehaviorSubject<Object>(this.viewDetails);
  private editDetails: Object = null;
  public editDetailsChanged = new BehaviorSubject<Object>(this.editDetails);

  private dropdownOptions: IDropdownOptions = {};
  public dropdownOptionsChanged = new BehaviorSubject<IDropdownOptions>({ ...this.dropdownOptions });

  constructor(
    private markersDataService: MarkersDataService,
    private sharedService: SharedService,
    private router: Router
  ) {}

  setStaffQualificationId(id: string) {
    this.qualificationId = id;
    this.qualificationIdChanged.next(this.qualificationId);
  }

  getQualificationsList(openEmisNo: string) {
    this.markersDataService.getQualificationsList(openEmisNo).subscribe(
      (res: any) => {
        this.qualificationsList = res['data'];
        this.qualificationsListChanged.next([...this.qualificationsList]);
      },
      err => {
        this.qualificationsList = [];
        this.qualificationsListChanged.next([...this.qualificationsList]);
      }
    );
  }

  getQualificationsViewDetails(qualificationId: string) {
    this.markersDataService.getQualificationsViewDetails(qualificationId).subscribe(
      (res: any) => {
        if (res['data']['length']) {
          this.viewDetails = res['data'][0];
          this.viewDetailsChanged.next({ ...this.viewDetails });
        } else {
          this.viewDetails = null;
          this.viewDetailsChanged.next({ ...this.viewDetails });
        }
      },
      err => {
        this.viewDetails = null;
        this.viewDetailsChanged.next({ ...this.viewDetails });
      }
    );
  }

  getQualificationsEditDetails(qualificationId: string) {
    this.markersDataService.getQualificationsEditDetails(qualificationId).subscribe(
      (res: any) => {
        if (res['data'].length) {
          this.editDetails = res['data'][0];
          this.editDetailsChanged.next({ ...this.editDetails });
        } else {
          this.editDetails = null;
          this.editDetailsChanged.next({ ...this.editDetails });
        }
      },
      err => {
        this.editDetails = null;
        this.editDetailsChanged.next({ ...this.editDetails });
      }
    );
  }

  getDropdownOptions(entities: Array<string>) {
    this.markersDataService.getDropdownOptions(entities).subscribe(
      (res: any) => {
        this.dropdownOptions = { ...this.dropdownOptions, ...res['data'] };
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      },
      err => {
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getDropdownTitle() {
    this.markersDataService.getDropdownTitle().subscribe(
      (res: any) => {
        this.dropdownOptions = { ...this.dropdownOptions, title: res['data'] };
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      },
      err => {
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getDropdownLevel() {
    this.markersDataService.getDropdownLevel().subscribe(
      (res: any) => {
        this.dropdownOptions = { ...this.dropdownOptions, level: res['data'] };
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      },
      err => {
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getDropdownFieldOfStudy() {
    this.markersDataService.getDropdownFieldOfStudy().subscribe(
      (res: any) => {
        this.dropdownOptions = { ...this.dropdownOptions, field_of_study: res['data'] };
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      },
      err => {
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getDropdownGraduateYear() {
    // this.markersDataService.getDropdownGraduateYear().subscribe(
    //   (res: any) => {
    //     // let yearOptions =
    //     this.dropdownOptions = { ...this.dropdownOptions, graduate_year: res['data'] };
    //     this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
    //   },
    //   err => {
    //     this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
    //   }
    // );
    let graduate_year = this.markersDataService.getDropdownGraduateYear();
    this.dropdownOptions = { ...this.dropdownOptions, graduate_year: graduate_year };
    this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
  }

  getOptionsSubjects() {
    this.markersDataService.getDropdownEducationSubject().subscribe(
      (res: any) => {
        this.dropdownOptions = { ...this.dropdownOptions, subjects: res['data'] };
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      },
      err => {
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getOptionsSpecialisations() {
    this.markersDataService.getDropdownSpecialisations().subscribe(
      (res: any) => {
        this.dropdownOptions = { ...this.dropdownOptions, specialisation: res['data'] };
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      },
      err => {
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  saveEditQualificationDetails(qualificationId: string, payload: Object) {
    this.markersDataService.saveEditQualificationDetails(qualificationId, payload).subscribe(
      (res: any) => {
        if (res) {
          let toasterConfig = {
            type: 'success',
            title: 'Qualifications Saved.',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
          setTimeout(() => {
            this.router.navigate(['main/examinations/markers/details/qualifications']);
          }, 0);
        } else {
          let toasterConfig = {
            type: 'error',
            title: 'Error while saving.',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      },
      err => {
        let toasterConfig = {
          type: 'error',
          title: 'Error while saving.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  saveQualification(openEmisNo: string, payload: Object) {
    this.markersDataService.saveQualification(openEmisNo, payload).subscribe(
      (res: any) => {
        if (res) {
          let toasterConfig = {
            type: 'success',
            title: 'Qualifications Saved.',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
          setTimeout(() => {
            this.router.navigate(['main/examinations/markers/details/qualifications']);
          }, 0);
        } else {
          let toasterConfig = {
            type: 'error',
            title: 'Error while saving.',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      },
      err => {
        let toasterConfig = {
          type: 'error',
          title: 'Error while saving.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  resetOptions() {
    this.dropdownOptions = null;
    this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
  }
}
