import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  IDynamicFormApi,
  KdPageBaseEvent,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IPageheaderApi
} from 'openemis-styleguide-lib';
import { QUESTION_BASE } from './view.config';
import { QualificationsService } from '../qualifications.service';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  private pageTitle: string = 'Examinations - Qualifications';
  public smallLoader: number = null;
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' }, list: [] };

  public questionBase = QUESTION_BASE;
  public api: IDynamicFormApi = {};
  private staffQualificationsId: string = null;

  /* Subscriptions */
  private staffQualificationsSub: Subscription;
  private viewDetailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private qualificationsService: QualificationsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => {
          this.router.navigate(['main/examinations/markers/details/qualifications']);
        }
      },
      {
        type: 'edit',
        callback: (): void => {
          this.router.navigate(['main/examinations/markers/details/qualifications/edit']);
        }
      },
      { type: 'delete', callback: (): void => {} }
    ]);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });
    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbArray: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbArray;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.staffQualificationsSub = this.qualificationsService.qualificationIdChanged.subscribe(staffQualificationsId => {
      if (!staffQualificationsId) {
        this.router.navigate(['main/examinations/markers/details/qualifications']);
      } else {
        this.staffQualificationsId = staffQualificationsId;
        this.qualificationsService.getQualificationsViewDetails(staffQualificationsId);
      }
    });

    this.viewDetailsSub = this.qualificationsService.viewDetailsChanged.subscribe((details: any) => {
      if (details) {
        this.setDetails(details);
      } else {
        this.loading = false;
      }
    });
  }

  setDetails(details: Object) {
    timer(100).subscribe(() => {
      this.questionBase.forEach(question => {
        if (question['key'] === 'specialisation' || question['key'] === 'subjects') {
          this.api.setProperty(
            question['key'],
            'value',
            details[question['key']] ? details[question['key']].map(item => item.name).join(', ') : []
          );
        } else {
          this.api.setProperty(question['key'], 'value', details[question['key']]);
        }
      });
    });
    this.loading = false;
  }

  detectValue(event) {
    console.log(event);
  }

  submit(event) {
    console.log(event);
    let payload = { ...event };
    this.qualificationsService.saveEditQualificationDetails(this.staffQualificationsId, payload);
  }

  cancelEdit() {}

  ngOnDestroy() {
    this.viewDetailsSub.unsubscribe();
    this.staffQualificationsSub.unsubscribe();

    super.destroyPageBaseSub();
  }
}
