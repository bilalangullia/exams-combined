import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../../shared/shared.module';
import { QualificationsRoutingModule } from './qualifications-routing.module';
import { QualificationsService } from './qualifications.service';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';

@NgModule({
  imports: [CommonModule, SharedModule, QualificationsRoutingModule],
  declarations: [ListComponent, ViewComponent, EditComponent, AddComponent],
  providers: [QualificationsService]
})
export class QualificationsModule {}
