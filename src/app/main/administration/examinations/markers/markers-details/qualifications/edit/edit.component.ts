import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import {
  KdPageBase,
  IPageheaderConfig,
  IPageheaderApi,
  KdPageBaseEvent,
  IBreadcrumbConfig,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { QualificationsService } from '../qualifications.service';
import { FORM_BUTTONS, QUESTION_BASE } from './edit.config';
import { IDropdownOptions, IEditPayload } from '../qualifications-interfaces';
import { SharedService } from '../../../../../../../shared/shared.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  private pageTitle: string = 'Examinations - Qualifications';
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public pageHeader: IPageheaderConfig;
  public pageHeaderApi: IPageheaderApi;
  public api: IDynamicFormApi = {};
  public isSubmit: boolean = false;

  public questionBase = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;

  private details: Object = null;
  // private dropdownOptions: Object = {};
  private staffQualificationId: any = null;

  /* Subscriptions */
  private staffQualificationIdSub: Subscription;
  private editDetailsSub: Subscription;
  public dropdownOptionsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private qualificationsService: QualificationsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbArray: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbArray;
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.qualificationsService.getDropdownTitle();
    this.qualificationsService.getDropdownLevel();
    this.qualificationsService.getDropdownFieldOfStudy();
    this.qualificationsService.getDropdownOptions(['country']);
    this.qualificationsService.getDropdownGraduateYear();
    this.qualificationsService.getOptionsSubjects();
    this.qualificationsService.getOptionsSpecialisations();

    this.staffQualificationIdSub = this.qualificationsService.qualificationIdChanged.subscribe(staffQualificationId => {
      if (!staffQualificationId) {
        this.staffQualificationId = null;
        this.router.navigate(['main/examinations/markers/details/qualifications']);
      } else {
        this.staffQualificationId = staffQualificationId;
        this.qualificationsService.getQualificationsEditDetails(this.staffQualificationId);
      }
    });

    timer(100).subscribe(() => {
      if (this.staffQualificationId) {
        this.editDetailsSub = this.qualificationsService.editDetailsChanged.subscribe((details: any) => {
          this.details = details;
        });

        this.dropdownOptionsSub = this.qualificationsService.dropdownOptionsChanged.subscribe(
          (options: IDropdownOptions) => {
            if (
              options &&
              options['title'] &&
              options['level'] &&
              options['field_of_study'] &&
              options['country'] &&
              options['subjects'] &&
              options['specialisation'] &&
              options['graduate_year']
            ) {
              this.setOptions(options);
            }
          }
        );
      }
    });
  }

  setOptions(options: IDropdownOptions) {
    if (
      options &&
      options['title'] &&
      options['level'] &&
      options['field_of_study'] &&
      options['country'] &&
      options['specialisation'] &&
      options['graduate_year']
    ) {
      this.questionBase.forEach(question => {
        if (question['controlType'] === 'dropdown') {
          question['options'] = options[question['key']]
            ? [
                { key: null, value: '-- Select --' },
                ...options[question['key']].map(item => ({ key: item['id'], value: item['name'] }))
              ]
            : [{ key: null, value: '-- Select --' }];
        } else if (question['type'] === 'multiselect') {
          question['options'] = options[question['key']]
            ? options[question['key']].map(item => ({ key: item['id'], value: item['name'] }))
            : [];
        }
      });
      setTimeout(() => {
        this.setDetails();
      }, 100);
    }
  }

  setDetails() {
    if (this.details) {
      this.questionBase.forEach(question => {
        if (question['controlType'] === 'dropdown') {
          console.log(question['key']);
          console.log(this.details[question['key']]);

          question['value'] = this.details[question['key']] ? this.details[question['key']]['key'] : null;
        } else if (question['key'] === 'specialisation' || question['key'] === 'subjects') {
          question['value'] = this.details[question['key']].map(item => ({ key: item['id'], value: item['name'] }));
        } else {
          question['value'] = this.details[question['key']];
        }
      });
    }

    setTimeout(() => {
      this.loading = false;
    }, 100);
  }

  detectValue(event) {
    // console.log('EditComponent -> detectValue -> event', event);
  }

  requiredCheck(formValue) {
    let hasError: boolean = false;
    this.questionBase.forEach(question => {
      if (question['required'] && !formValue[question['key']]) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 100);
      }
    });
    return hasError;
  }

  submit(event) {
    // this.isSubmit = true;
    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields!',
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      const formData = new FormData();

      formData.append('qualification_title_id', event.title ? event.title : null);
      formData.append('level', event.level ? event.level : null);
      formData.append('education_field_of_study_id', event.field_of_study ? event.field_of_study : null);
      formData.append('specialisation', event.specialisation ? event.specialisation : null);
      formData.append(
        'subjects',
        event.subjects.map(item => ({ id: item['key'], subject_name: item['value'] }))
      );
      formData.append('qualification_country_id', event.country ? event.country : null);
      formData.append('qualification_institution', event.institution ? event.institution : null);
      formData.append('document_no', event.document_no ? event.document_no : null);
      formData.append('graduate_year', event.graduate_year ? event.graduate_year : null);
      formData.append('gpa', event.grade_score ? event.grade_score : null);
      formData.append('file_name', event.attachment ? event.attachment : null);
      this.qualificationsService.saveEditQualificationDetails(this.staffQualificationId, formData);
    }

    // timer(500).subscribe(() => {
    //   this.isSubmit = false;
    // });
  }

  cancelEdit() {
    this.qualificationsService.resetOptions();
    this.router.navigate(['main/examinations/markers/details/qualifications/list']);
  }

  ngOnDestroy() {
    this.details = null;
    this.staffQualificationId = null;
    this.staffQualificationIdSub.unsubscribe();
    if (this.editDetailsSub) {
      this.editDetailsSub.unsubscribe();
    }
    if (this.dropdownOptionsSub) {
      this.dropdownOptionsSub.unsubscribe();
    }
    this.qualificationsService.resetOptions();
    super.destroyPageBaseSub();
  }
}
