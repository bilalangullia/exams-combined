import { ITableColumn } from 'openemis-styleguide-lib';

export interface IListColumn {
  GraduateYear?: ITableColumn;
  Level?: ITableColumn;
  Title?: ITableColumn;
  DocumentNo?: ITableColumn;
  Institution?: ITableColumn;
  FileType?: ITableColumn;
  FieldOfStudy?: ITableColumn;
}

export interface IDropdownOptions {
  title?: Array<Object>;
  level?: Array<Object>;
  field_of_study?: Array<Object>;
  graduate_year?: Array<Object>;
  country?: Array<Object>;
  subjects?: Array<Object>;
  specialisation?: Array<Object>;
}

export interface IEditPayload {
  qualification_title_id: any;
  level?: any;
  education_field_of_study: any;
  specialisation?: Array<any>;
  subjects?: Array<any>;
  qualification_country_id: any;
  qualification_institution: any;
  document_no?: any;
  graduate_year: any;
  gpa?: any;
  file_name?: any;
}

export interface IAddPayload {}
