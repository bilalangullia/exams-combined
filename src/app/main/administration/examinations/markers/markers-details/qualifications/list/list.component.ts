import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IPageheaderApi,
  ITableColumn,
  ITableConfig
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { QualificationsService } from '../qualifications.service';
import { TABLE_COLUMNS, TABLE_ID, PAGE_SIZE, TOTAL_ROW } from './list.config';
import { MarkersService } from '../../../markers.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  private pageTitle: string = 'Examinations - Qualifications';
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public smallLoaderValue: number = null;

  public tableRows: Array<any> = [];
  public tableColumns: Array<ITableColumn> = [
    TABLE_COLUMNS.GraduateYear,
    TABLE_COLUMNS.Level,
    TABLE_COLUMNS.Title,
    TABLE_COLUMNS.DocumentNo,
    TABLE_COLUMNS.Institution,
    TABLE_COLUMNS.FileType,
    TABLE_COLUMNS.FieldOfStudy
  ];
  public tableConfig: ITableConfig = {
    id: TABLE_ID,
    loadType: 'normal',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTAL_ROW },
    action: {
      enabled: true,
      list: [
        {
          type: 'View',
          callback: (rowNode, tableApi): void => {
            if (rowNode['data']['id']) {
              this.qualificationsService.setStaffQualificationId(rowNode['data']['id']);
              this.router.navigate(['main/examinations/markers/details/qualifications/view']);
            } else {
              let toasterConfig: any = {
                type: 'error',
                title: 'Invalid Qualification ID',
                body: 'Select valid qualification ID',
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 1000
              };
              this.sharedService.setToaster(toasterConfig);
            }
          }
        }
      ]
    }
  };

  /* Subscriptions */
  private listSub: Subscription;
  private openEmisNoSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private sharedService: SharedService,
    private qualificationsService: QualificationsService,
    private markersService: MarkersService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'add', name: 'Add', path: 'main/examinations/markers/details/qualifications/add' }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchQualificationsList(event);
    });

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });
    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj: IBreadcrumbConfig): void => {
      this.breadcrumbList = breadcrumbObj;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.openEmisNoSub = this.markersService.openEmisNoChanged.subscribe((openEmisNo: string) => {
      if (!openEmisNo) {
        this.router.navigate(['main/examinations/markers/list']);
      } else {
        this.qualificationsService.getQualificationsList(openEmisNo);
      }
    });

    this.listSub = this.qualificationsService.qualificationsListChanged.subscribe((list: Array<any>) => {
      if (list.length) {
        this.populateTable(list);
      } else {
        this.loading = false;
      }
    });
  }

  populateTable(data: Array<any>) {
    this.tableRows = data;
    /* Populate filterValue */
    this.loading = false;
  }

  searchQualificationsList(keyword: string) {
    /* Call Search Method */
  }

  ngOnDestroy() {
    this.openEmisNoSub.unsubscribe();
    this.listSub.unsubscribe();

    super.destroyPageBaseSub();
  }
}
