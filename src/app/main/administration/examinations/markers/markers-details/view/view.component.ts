import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IPageheaderApi,
  KdView
} from 'openemis-styleguide-lib';

import { MarkersDetailsService } from '../markers-details.service';
import { MarkersService } from '../../markers.service';
import { IFilters } from '../../markers-interfaces';
import { QUESTION_BASE } from './view.config';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public pageTitle: string = 'Examination - Marker';
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' }, list: [] };

  public questionBase: Array<any> = QUESTION_BASE;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi;

  private filterData: IFilters = {};
  private academicPeriodValue: string = '';

  @ViewChild('detailsView') updateView: KdView;

  /* Subscriptions */
  private openEmisNoChanged: Subscription;
  private filtersSub: Subscription;
  private markerDetailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private markersDetailsService: MarkersDetailsService,
    private markersService: MarkersService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => {
          this.markersService.reset();
          this.router.navigate(['main/examinations/markers']);
        }
      },
      {
        type: 'edit',
        callback: (): void => {
          this.router.navigate(['main/examinations/markers/details/edit']);
        }
      },
      { type: 'delete', callback: (): void => {} }
    ]);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });
    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbArray: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbArray;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.openEmisNoChanged = this.markersService.openEmisNoChanged.subscribe((openEmisNo: string) => {
      if (!openEmisNo) {
        this.router.navigate(['main/examinations/markers']);
      } else {
        this.markersDetailsService.getViewDetails(openEmisNo);
      }
    });

    // this.filtersSub = this.markersDetailsService.filtersChanged.subscribe((filters: IFilters) => {
    //   this.filterData = { ...filters };
    // });

    this.markerDetailsSub = this.markersDetailsService.markerViewDetailsChanged.subscribe((details: Object) => {
      if (details) {
        this.setDetails({ ...details });
      }
    });
  }

  setDetails(data: Object) {
    this.questionBase.forEach(question => {
      if (question['key'] === 'subjects') {
        question['row'] = data[question['key']].map(item => ({
          id: item['id'],
          subject_code: item['code'],
          subject_name: item['name']
        }));
        question['column'].forEach(column => {
          if (column['field'] === 'subject_code' && column['filterable']) {
            column['filterValue'] = [data['subject_code']];
          } else if (column['field'] === 'subject_name' && column['filterable']) {
            column['filterValue'] = [data['subject_name']];
          }
        });
      } else {
        question['value'] = data[question['key']] ? data[question['key']] : '';
      }
    });
    this.loading = false;
  }

  ngOnDestroy() {
    this.openEmisNoChanged.unsubscribe();
    this.markerDetailsSub.unsubscribe();
    // this.filtersSub.unsubscribe();
  }
}
