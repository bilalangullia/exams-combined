export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'openemis_no',
    label: 'OpenEMIS ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'marker_id',
    label: 'Marker ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'classfication',
    label: 'Calssification',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'middle_name',
    label: 'Middle Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'third_name',
    label: 'Third Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'gender_id',
    label: 'Gender',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'nationality',
    label: 'Nationality',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: /* 'identity_type' */ 'identityType',
    label: 'Identity Type',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'identity_number',
    label: 'Identity Number',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'address',
    label: 'Address',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'postal_code',
    label: 'Postal Code',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'address_area',
    label: 'Address Area',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'birthplace_area',
    label: 'Birthplace Area',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'area',
    label: 'Area Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_centre',
    label: 'Exam Centre',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  /* Subjects Table */
  {
    key: 'subjects',
    label: 'subjects',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Subject Code',
        field: 'subject_code',
        visible: true,
        sortable: false,
        filterable: true,
        filterValue: [],
        class: 'ag-name'
      },
      {
        headerName: 'Subject Name',
        field: 'subject_name',
        visible: true,
        sortable: false,
        filterable: true,
        filterValue: [],
        class: 'ag-name'
      }
    ],
    config: {
      id: 'subjectsTable',
      rowIdKey: 'id',
      gridHeight: 250,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_user_id',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_user_id',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];

export const QUESTIONS_KEYS: Array<string> = [
  'academic_period_id',
  'examination',
  'openemis_no',
  'examiner_id',
  'first_name',
  'middle_name',
  'third_name',
  'last_name',
  'gender_id',
  'date_of_birth',
  'nationality',
  'identityType' /* 'identity_type', */,
  'identity_number',
  'address',
  'postal_code',
  'address_area',
  'birthplace_area',
  'area',
  'examination_centre' /* Subjects Table */,
  'subjects',
  'modified_user_id',
  'modified',
  'created_user_id',
  'created'
];
