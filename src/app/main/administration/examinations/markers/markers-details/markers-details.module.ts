import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarkersDetailsRoutingModule } from './markers-details-routing.module';
import { EntryComponent } from './entry/entry.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { MarkersDetailsService } from './markers-details.service';

@NgModule({
  imports: [CommonModule, MarkersDetailsRoutingModule, SharedModule],
  declarations: [EntryComponent, ViewComponent, EditComponent],
  providers: [MarkersDetailsService]
})
export class MarkersDetailsModule {}
