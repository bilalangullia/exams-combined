import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer, Observable, Subscriber } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  IPageheaderConfig,
  IPageheaderApi,
  IBreadcrumbConfig,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { MarkersAddService } from '../markers-add.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';
import { IAddOptions } from '../../markers-interfaces';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  private pageTitle: string = 'Examinations - Marker';
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' }, list: [] };
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public api: IDynamicFormApi = {};

  // private self = this;

  public questionBase = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  private subjectsList: Array<any> = [];

  /* Subscriptions */
  private addOptionsSub: Subscription;
  private subjectsListSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private markersAddService: MarkersAddService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbArray: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbArray;
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.markersAddService.getDropdownOptions(['academicPeriods', 'gender', 'nationality', 'identityTypes']);
    this.markersAddService.getDropdownAddressRegion();
    this.markersAddService.getDropdownCountryRegion();
    this.markersAddService.getDropdownClassification();
    this.markersAddService.getOptionsEducationSubject();

    this.addOptionsSub = this.markersAddService.addOptionsChanged.subscribe((options: IAddOptions) => {
      if (
        options &&
        options['academic_period_id'] &&
        options['classification'] &&
        options['gender_id'] &&
        options['nationality_id'] &&
        options['identity_type_id'] &&
        options['address_area_id'] &&
        options['birthplace_area_id'] &&
        options['area_id']
      ) {
        options['academic_period_id'].sort((a, b) => b['name'] - a['name']);
        setTimeout(() => {
          this.setOptions(options);
        }, 0);
      } else {
        this.loading = false;
      }

      if (options && options['examination_id'] && options['examination_centre_id']) {
        this.setExaminationAndCentreOptions(options);
      }
    });

    this.subjectsListSub = this.markersAddService.subjectsListChanged.subscribe((subjects: Array<any>) => {
      if (subjects.length) {
        this.subjectsList = subjects;
      }
    });
  }

  setOptions(options: IAddOptions) {
    this.questionBase.forEach((question) => {
      if (
        question['controlType'] === 'dropdown' &&
        (question['key'] !== 'examination_id' || question['key'] !== 'examination_centre_id')
      ) {
        question['options'] = options[question['key']]
          ? [
              { key: null, value: '-- Select --' },
              ...options[question['key']].map((item) => ({ key: item['id'], value: item['name'] }))
            ]
          : [{ key: null, value: '-- Select --' }];

        if (question['key'] == 'academic_period_id') {
          question['value'] == question['options'][1]['key'];
        }
      } else if (question['controlType'] === 'tree') {
        question.config.list = options[question['key']];
      } else if (question['controlType'] == 'table') {
        question['row'] = this.subjectsList.map((item: any, index: number) => ({
          id: item['id'],
          subject_id: item['id'],
          subject_code: item['code'],
          subject_name: item['name']
        }));
      } else if (question['key'] === 'openemis_no') {
        question['dropdownCallback'] = this.searchOpenEmisId.bind(this);
      } else if (question['key'] === 'examiner_id') {
        question['dropdownCallback'] = this.searchExaminerId.bind(this);
      }
    });

    setTimeout(() => {
      this.showForm = true;
    }, 0);
    // console.log(this.questionBase);

    this.loading = false;
  }

  setExaminationAndCentreOptions(options: IAddOptions) {
    this.questionBase.forEach((question) => {
      if (question['key'] === 'examination_id' || question['key'] === 'examination_centre_id') {
        this.api.setProperty(question['key'], 'options', [
          { key: null, value: '-- Select --' },
          ...options[question['key']].map((item) => ({ key: item.id, value: item.name }))
        ]);
      }
    });
  }

  searchOpenEmisId(params: any): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data = [];
      // timer(2000).subscribe((): void => {
      if (params['query'].length > 2) {
        this.markersAddService.getAutocompleteOpenEmisNo(params['query']).subscribe(
          (res: any) => {
            data = res.data;
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
            // this.setBlankValues();
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
      // });
    });
  }

  searchExaminerId(params: any): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data = [];
      // timer(2000).subscribe((): void => {
      if (params['query'].length > 2) {
        this.markersAddService.getAutocompleteExaminerId(params['query']).subscribe(
          (res: any) => {
            data = res.data;
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
            // this.setBlankValues();
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
      // });
    });
  }

  detectValue(event) {
    if (event['key'] === 'academic_period_id') {
      this.markersAddService.getDropdownExamination(event['value']);
    }
    if (event['key'] === 'examination_id') {
      this.markersAddService.getDropdownExaminationCentre(event['value']);
    }
  }

  requiredCheck(formVal): boolean {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !formVal[question['key']]) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 100);
      }
    });
    return hasError;
  }

  submit(event) {
    delete event.openemis_no;
    delete event.examiner_id;

    event['area_id'] = event['area_id']['id'];
    event['address_area_id'] = event['address_area_id']['id'];
    event['birthplace_area_id'] = event['birthplace_area_id']['id'];

    if (this.requiredCheck(event)) {
      let toasterConfig: IToasterConfig = {
        type: 'error',
        title: 'Missing required fields.',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = { ...event };
      this.markersAddService.saveMarker(payload);
    }
  }

  cancelAction() {
    this.markersAddService.resetOptions();
    this.router.navigate(['main/examinations/markers']);
  }

  ngOnDestroy() {
    this.addOptionsSub.unsubscribe();
    this.subjectsListSub.unsubscribe();
    this.markersAddService.resetOptions();
    super.destroyPageBaseSub();
  }
}
