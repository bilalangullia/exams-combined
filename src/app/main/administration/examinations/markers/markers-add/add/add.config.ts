import { ITreeConfig } from 'openemis-styleguide-lib';

import { TABLE_COLUMN_LIST } from '../../markers.config';

const currentDate = new Date();

export const TREE_CONFIG_AREA: ITreeConfig = {
  id: 'area_id',
  list: [],
  selectionMode: 'single'
};

export const TREE_CONFIG_BIRTHPLACE_AREA: ITreeConfig = {
  id: 'birthplace_area_id',
  list: [],
  selectionMode: 'single'
};

export const TREE_CONFIG_ADDRESS_AREA: ITreeConfig = {
  id: 'address_area_id',
  list: [],
  selectionMode: 'single'
};

export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }],
    required: true
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }],
    required: true
  },
  {
    key: 'openemis_no',
    label: 'OpenEMIS ID',
    visible: true,
    required: false,
    order: 1,
    controlType: 'text',
    format: 'string',
    type: 'autocomplete',
    clickToggleDropdown: true,
    autocomplete: true,
    placeholder: 'Search OpenEMIS ID',
    onEnter: true
    // dropdownCallback: (params: any) => {
    // return new Observable((_observer: Subscriber<any>): any => {
    //   let data: Array<any> = [];
    //   console.log('Params usable:', params);
    //   if (params.query && params.query.length > 2) {
    //     this.dataService.searchOpenEmisID(params.query).subscribe(
    //       (res: any) => {
    //         if (res && res.data.length) {
    //           timer(500).subscribe((): void => {
    //             data = res.data;
    //             _observer.next(data);
    //             _observer.complete();
    //           });
    //         } else {
    //           _observer.next([]);
    //           _observer.complete();
    //           this.setBlankValues();
    //         }
    //       },
    //       err => {
    //         console.log(err);
    //         _observer.next([]);
    //         _observer.complete();
    //         this.setBlankValues();
    //       }
    //     );
    //   }
    // });
    // }
  },
  {
    key: 'examiner_id',
    label: 'Previous Examiner ID',
    visible: true,
    controlType: 'text',
    type: 'autocomplete',
    format: 'string',
    order: 1,
    clickToggleDropdown: true,
    autocomplete: true,
    placeholder: 'Search Examiner ID',
    onEnter: true
    // dropdownCallback: (params: any) /* : Observable<any> */ => {
    /* return new Observable((_observer: Subscriber<any>): any => {
        let data: Array<any> = [];
        // console.log('Params usable:', params);
        if (params.query && params.query.length > 2) {
          this.dataService.searchCandidateId(params.query).subscribe(
            (res: any) => {
              if (res && res.data.length) {
                timer(500).subscribe((): void => {
                  data = res.data;
                  _observer.next(data);
                  _observer.complete();
                });
              } else {
                _observer.next([]);
                _observer.complete();
                this.setBlankValues();
              }
            },
            err => {
              console.log(err);
              _observer.next([]);
              _observer.complete();
              this.setBlankValues();
            }
          );
        }
      }); */
    // }
  },
  {
    key: 'classification',
    label: 'Classification',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }],
    required: true
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'middle_name',
    label: 'Middle Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'third_name',
    label: 'Third Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'gender_id',
    label: 'Gender',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }],
    required: true
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    visible: true,
    controlType: 'date',
    format: 'string',
    type: 'string',
    required: true,
    config: {
      firstDayOfWeek: 1
    },
    minDate: '1950-01-01',
    maxDate: `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`
  },
  {
    key: 'nationality_id',
    label: 'Nationality',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }]
  },
  {
    key: /* 'identity_type' */ 'identity_type_id',
    label: 'Identity Type',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }]
  },
  {
    key: 'identity_number',
    label: 'Identity Number',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'address',
    label: 'Address',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'postal_code',
    label: 'Postal Code',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'address_area_id',
    label: 'Address Area',
    visible: true,
    controlType: 'tree',
    config: TREE_CONFIG_ADDRESS_AREA
  },
  {
    key: 'birthplace_area_id',
    label: 'Birthplace Area',
    visible: true,
    controlType: 'tree',
    config: TREE_CONFIG_BIRTHPLACE_AREA
  },
  {
    key: 'area_id',
    label: 'Area Name',
    visible: true,
    controlType: 'tree',
    config: TREE_CONFIG_AREA,
    // required: true,
    value: null
  },
  {
    key: 'examination_centre_id',
    label: 'Exam Centre',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--No Data--' }],
    required: true
  },
  {
    key: 'subjects',
    label: 'Subjects',
    visible: true,
    controlType: 'table',
    row: [],
    column: [TABLE_COLUMN_LIST.subject_code, TABLE_COLUMN_LIST.subject_name],
    config: {
      id: 'subjectsTable',
      rowIdKey: 'id',
      gridHeight: 400,
      loadType: 'oneshot',
      selection: {
        type: 'all',
        value: [],
        returnKey: ['subject_code', 'subject_name']
      },
      paginationConfig: { pagesize: 10, total: 100 },
      click: {
        type: 'selection'
      }
    }
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
