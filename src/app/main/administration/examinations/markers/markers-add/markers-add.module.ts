import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarkersAddRoutingModule } from './markers-add-routing.module';
import { AddComponent } from './add/add.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { MarkersAddService } from './markers-add.service';

@NgModule({
  imports: [CommonModule, MarkersAddRoutingModule, SharedModule],
  declarations: [AddComponent],
  providers: [MarkersAddService]
})
export class MarkersAddModule {}
