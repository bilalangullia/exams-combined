import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { MarkersService } from '../markers.service';
import { MarkersDataService } from '../markers-data.service';
import { IAddOptions } from '../markers-interfaces';

@Injectable()
export class MarkersAddService {
  private addOptions: IAddOptions = null;
  public addOptionsChanged = new BehaviorSubject<IAddOptions>(this.addOptions);
  private subjectsList: Array<any> = [];
  public subjectsListChanged = new BehaviorSubject<Array<any>>([...this.subjectsList]);

  constructor(
    private router: Router,
    private sharedService: SharedService,
    private markersDataService: MarkersDataService,
    private markersService: MarkersService
  ) {}

  getDropdownOptions(entities: Array<string>) {
    this.markersDataService.getDropdownOptions(entities).subscribe(
      (res: any) => {
        this.addOptions = { ...this.addOptions, ...res['data'] };
        this.addOptionsChanged.next({ ...this.addOptions });
      },
      err => {
        this.addOptionsChanged.next({ ...this.addOptions });
      }
    );
  }

  getDropdownExamination(academicPeriod: number) {
    this.markersDataService.getDropdownExaminationNme(academicPeriod).subscribe(
      (res: any) => {
        this.addOptions = { ...this.addOptions, examination_id: res['data'] };
        this.addOptionsChanged.next({ ...this.addOptions });
      },
      err => {
        this.addOptionsChanged.next({ ...this.addOptions });
      }
    );
  }

  getDropdownExaminationCentre(academicPeriod: number) {
    this.markersDataService.getDropdownExaminationCentre(academicPeriod).subscribe(
      (res: any) => {
        this.addOptions = { ...this.addOptions, examination_centre_id: res['data'] };
        this.addOptionsChanged.next({ ...this.addOptions });
      },
      err => {
        this.addOptionsChanged.next({ ...this.addOptions });
      }
    );
  }

  getDropdownClassification() {
    this.markersDataService.getDropdownClassification().subscribe(
      (res: any) => {
        this.addOptions = { ...this.addOptions, classification: res['data'] };
        this.addOptionsChanged.next({ ...this.addOptions });
      },
      err => {
        this.addOptionsChanged.next({ ...this.addOptions });
      }
    );
  }

  getDropdownAddressRegion() {
    this.markersDataService.getDropdownAddressRegion().subscribe(
      (res: any) => {
        this.addOptions = { ...this.addOptions, address_area_id: res['data'], birthplace_area_id: res['data'] };
        this.addOptionsChanged.next({ ...this.addOptions });
      },
      err => {
        this.addOptionsChanged.next({ ...this.addOptions });
      }
    );
  }

  getDropdownCountryRegion() {
    this.markersDataService.getDropdownCountryRegion().subscribe(
      (res: any) => {
        this.addOptions = { ...this.addOptions, area_id: res['data'] };
        this.addOptionsChanged.next({ ...this.addOptions });
      },
      err => {
        this.addOptionsChanged.next({ ...this.addOptions });
      }
    );
  }

  getOptionsEducationSubject() {
    this.markersDataService.getDropdownEducationSubject().subscribe(
      (res: any) => {
        this.subjectsList = res['data'];
        this.subjectsListChanged.next([...this.subjectsList]);
      },
      err => {
        this.subjectsList = [];
        this.subjectsListChanged.next([...this.subjectsList]);
      }
    );
  }

  resetOptions() {
    // this.addOptions['examination_id'] = [];
    // this.addOptions['examination_centre_id'] = [];
    this.addOptions = null;
    this.addOptionsChanged.next({ ...this.addOptions });
  }

  getAutocompleteOpenEmisNo(keyword: string) {
    return this.markersDataService.getAutocompleteOpenEMISNo(keyword);
  }

  getAutocompleteExaminerId(keyword: string) {
    return this.markersDataService.getAutocompleteExaminerId(keyword);
  }

  saveMarker(payload: any) {
    this.markersDataService.saveMarker(payload).subscribe(
      (res: any) => {
        if (res) {
          let toasterConfig = {
            type: 'success',
            title: 'Marker Saved.',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
          this.markersService.reset();
          setTimeout(() => {
            this.router.navigate(['main/examinations/markers']);
          }, 0);
        } else {
          let toasterConfig = {
            type: 'error',
            title: 'Error while saving.',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
          this.markersService.reset();
        }
      },
      err => {
        console.log('fail');
      }
    );
  }
}
