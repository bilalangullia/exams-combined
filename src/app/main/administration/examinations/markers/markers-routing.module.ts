import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'list' },
  { path: 'list', loadChildren: './markers-list/markers-list.module#MarkersListModule' },
  { path: 'add', loadChildren: './markers-add/markers-add.module#MarkersAddModule' },
  { path: 'details', loadChildren: './markers-details/markers-details.module#MarkersDetailsModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarkersRoutingModule {}
