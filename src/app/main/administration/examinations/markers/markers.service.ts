import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { IFilters } from './markers-interfaces';

@Injectable()
export class MarkersService {
  private filters: IFilters = null;
  public filtersChanged = new BehaviorSubject<IFilters>({ ...this.filters });

  // private examinerId: string;
  // public examinerIdChanged = new BehaviorSubject<string>(null);

  private openEmisNo: string = null;
  public openEmisNoChanged = new BehaviorSubject<string>(this.openEmisNo);

  constructor() {}

  setFilters(filters: IFilters) {
    this.filters = { ...this.filters, ...filters };
    this.filtersChanged.next({ ...this.filters });
  }

  /* setExaminerId(examinerId: string) {
    this.examinerId = examinerId;
    this.examinerIdChanged.next(this.examinerId);
  } */

  setOpenEmisNo(openEmisNo: string) {
    this.openEmisNo = openEmisNo;
    this.openEmisNoChanged.next(this.openEmisNo);
  }

  reset() {
    this.filters = null;
    this.filtersChanged.next({ ...this.filters });
    // this.examinerId = null;
    // this.examinerIdChanged.next(this.examinerId);
    this.openEmisNo = null;
    this.openEmisNoChanged.next(this.openEmisNo);
  }
}
