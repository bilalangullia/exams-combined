import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IPageheaderApi,
  ITableColumn,
  ITableConfig,
  KdFilter,
  ITableApi
} from 'openemis-styleguide-lib';
import { ExcelExportParams } from 'ag-grid';

import { SharedService } from '../../../../../../shared/shared.service';
import { MarkersService } from '../../markers.service';
import { IFilters, IDropdownOptions } from '../../markers-interfaces';
import { MarkersListService } from '../markers-list.service';
import { TABLE_COLUMNS, TABLE_ID, PAGE_SIZE, TOTAL_ROW, FILTER_INPUTS } from './list.config';

@Component({ selector: 'app-list', templateUrl: './list.component.html', styleUrls: ['./list.component.css'] })
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle = 'Examinations - Markers';

  public loading: boolean = true;
  public breadcrumbList: IBreadcrumbConfig;
  public pageHeader: IPageheaderConfig;
  public pageHeaderApi: IPageheaderApi = {};
  public _tableApi: ITableApi = {};

  @ViewChild('filters') listFilter: KdFilter;

  public filterInputs: Array<any> = FILTER_INPUTS;
  public tableRows: Array<any> = [];
  public tableColumns: Array<ITableColumn> = [
    TABLE_COLUMNS.OpenEMISId,
    TABLE_COLUMNS.MarkerId,
    TABLE_COLUMNS.FirstName,
    TABLE_COLUMNS.LastName,
    TABLE_COLUMNS.Gender
  ];
  public tableConfig: ITableConfig = {
    id: TABLE_ID,
    loadType: 'normal',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTAL_ROW },
    action: {
      enabled: true,
      list: [
        {
          type: 'View',
          callback: (rowNode, tableApi): void => {
            if (rowNode['data']['openemis_no']) {
              this.markersService.setOpenEmisNo(rowNode['data']['openemis_no']);
              this.router.navigate(['/main/examinations/markers/details']);
            } else {
              let toasterConfig: any = {
                type: 'error',
                title: 'Invalid OpenEMIS No.',
                body: 'Select a valid OpenEMIS No.',
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 1000
              };
              this.sharedService.setToaster(toasterConfig);
            }
          }
        }
      ]
    }
  };

  private filters: IFilters = null;

  /* Subscriptions */
  private academicPeriodOptionsSub: Subscription;
  private dropdownOptionsSub: Subscription;
  private filtersSub: Subscription;
  private markersListSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private markersService: MarkersService,
    private markersListService: MarkersListService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/examinations/markers/add' },
      { type: 'import', callback: (): void => {} },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'markers_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchMarkersList(event);
    });

    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbObj;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.markersListService.reset();
    timer(100).subscribe(() => {
      this.markersListService.getOptionsAcademicPeriod();

      this.academicPeriodOptionsSub = this.markersListService.optionsAcademicPeriodChanged.subscribe(
        (optionsAcademicPeriod: Array<any>) => {
          if (optionsAcademicPeriod) {
            this.setDropdownOptions('academic_period_id', optionsAcademicPeriod);
          }
        }
      );

      this.dropdownOptionsSub = this.markersListService.dropdownOptionsChanged.subscribe(
        (options: IDropdownOptions) => {
          if (options['examination_name']) {
            this.setDropdownOptions('examination_name', options['examination_name']);
          }
          if (options['examination_centre']) {
            this.setDropdownOptions('examination_centre', options['examination_centre']);
          }
          if (options['education_subject']) {
            this.setDropdownOptions('education_subject', options['education_subject']);
          }
        }
      );

      this.filtersSub = this.markersService.filtersChanged.subscribe((filters: IFilters) => {
        if (
          filters['academic_period_id'] &&
          filters['academic_period_id']['id'] &&
          filters['examination_name'] &&
          filters['examination_name']['id'] &&
          filters['examination_centre'] &&
          filters['examination_centre']['id'] &&
          filters['education_subject'] &&
          filters['education_subject']['id']
        ) {
          this.filters = { ...filters };
          this.markersListService.getMarkersList(
            filters['academic_period_id']['id'],
            filters['examination_name']['id'],
            filters['examination_centre']['id'],
            filters['education_subject']['id']
          );
        } else {
          this.markersListService.resetList();
        }
      });

      this.markersListSub = this.markersListService.markersListChanged.subscribe((list: Array<any>) => {
        if (list.length) {
          this.populateTable(list);
        } else {
          this.tableRows = [];
          this.loading = false;
        }
      });
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];

    if (data.length) {
      if (key === 'academic_period_id') {
        options = data.map((item) => ({ key: item.id, value: item.name })).sort((a, b) => b.key - a.key);
      } else if (key === 'examination_name') {
        options = data.map((item) => ({ key: item.id, value: item.name }));
      } else if (key === 'examination_centre' || key === 'education_subject') {
        options = data.map((item) => ({ key: item.id, value: `${item.code} - ${item.name}` }));
      }

      options.unshift({ key: null, value: '-- Select --' });
      this.listFilter.setInputProperty(key, 'options', options);

      /* Selects 1st option as default */
      if (key === 'academic_period_id') {
        timer(100).subscribe(() => {
          this.listFilter.setInputProperty(key, 'value', options[1]['key']);
        });
      }
    } else {
      options.unshift({ key: null, value: '-- Select --' });
      this.listFilter.setInputProperty(key, 'options', options);
      this.listFilter.setInputProperty(key, 'value', options[0]['key']);
    }
  }

  detectValue(event: { key?: string; value?: number }) {
    let filter: IFilters = {};
    if (event['value']) {
      filter[event['key']] = { id: event['value'] ? +event['value'] : null };
    }
    let value = event['options'].find((item) => item['key'] == event['value']);
    if (value) {
      filter[event['key']] = { ...filter[event['key']], value };
    }
    this.markersListService.setFilters(filter);

    /* Call child filter options */
    if (event['key'] == 'academic_period_id') {
      this.markersListService.getOptionsExaminationName(event['value']);
      this.markersListService.getOptionsExaminationCentre(event['value']);
      this.markersListService.getOptionsEducationSubject();
    } else if (event['key'] == 'examination_name') {
      /* Call child filter options */
      this.markersListService.getOptionsExaminationCentre(event['value']);
    }
  }

  populateTable(data: Array<any>) {
    this.tableRows = data;
    this.tableColumns.forEach((column, colIndex) => {
      if (column['filterable']) {
        data.forEach((data) => {
          if (data[column['field']]) {
            this.tableColumns[colIndex]['filterValue'].push(data[column['field']]);
          }
        });
      }
    });
    this.loading = false;
  }

  searchMarkersList(keyword: string) {
    if (keyword.length) {
      if (keyword.length > 2) {
        this.markersListService.searchMarkersList(
          this.filters['academic_period_id']['id'],
          this.filters['examination_name']['id'],
          this.filters['examination_centre']['id'],
          this.filters['education_subject']['id'],
          keyword
        );
      }
    } else {
      this.markersListService.searchMarkersList(
        this.filters['academic_period_id']['id'],
        this.filters['examination_name']['id'],
        this.filters['examination_centre']['id'],
        this.filters['education_subject']['id'],
        keyword
      );
    }
  }

  ngOnDestroy() {
    this.markersListSub.unsubscribe();
    this.filtersSub.unsubscribe();
    this.dropdownOptionsSub.unsubscribe();
    this.academicPeriodOptionsSub.unsubscribe();
    super.destroyPageBaseSub();
  }
}
