import { IListColumn } from '../../markers-interfaces';

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination_name',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination_centre',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'education_subject',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  }
];

export const TABLE_ID: string = 'markersList';
export const PAGE_SIZE: number = 10;
export const TOTAL_ROW: number = 1000;

export const TABLE_COLUMNS: IListColumn = {
  OpenEMISId: {
    headerName: 'Open EMIS ID',
    field: 'openemis_no',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  MarkerId: {
    headerName: 'Marker ID',
    field: 'marker_id',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  FirstName: {
    headerName: 'First Name',
    field: 'first_name',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  LastName: {
    headerName: 'Last Name',
    field: 'last_name',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  },
  Gender: {
    headerName: 'Gender',
    field: 'gender_id',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: true
  }
};
