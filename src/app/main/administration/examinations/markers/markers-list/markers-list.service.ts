import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { IFilters, IDropdownOptions } from '../markers-interfaces';
import { MarkersService } from '../markers.service';
import { MarkersDataService } from '../markers-data.service';

@Injectable()
export class MarkersListService {
  private optionsAcademicPeriod: Array<any> = [];
  public optionsAcademicPeriodChanged = new BehaviorSubject<Array<any>>(this.optionsAcademicPeriod);
  // private optionsSubject: Array<any> = [];
  // public optionsSubjectChanged = new Subject<Array<any>>();
  private markersList: Array<any> = [];
  public markersListChanged = new BehaviorSubject<Array<any>>([...this.markersList]);

  constructor(private markersDataService: MarkersDataService, private markersService: MarkersService) {}

  // private filters: IFilters = {};
  // public filtersChanged = new BehaviorSubject<IFilters>({ ...this.filters });
  private dropdownOptions: IDropdownOptions = {};
  public dropdownOptionsChanged = new BehaviorSubject<any>(this.dropdownOptions);

  setFilters(filters: IFilters) {
    this.markersService.setFilters(filters);
  }

  reset() {
    this.markersService.reset();
    this.resetList();
  }

  resetList() {
    this.markersList = [];
    this.markersListChanged.next([...this.markersList]);
  }

  // getFilters() {
  //   this.markersService.filtersChanged.subscribe((filters: IFilters) => {
  //     this.filters = { ...filters };
  //     this.filtersChanged.next({ ...this.filters });
  //   });
  // }

  getOptionsAcademicPeriod() {
    this.markersDataService.getDropdownAcademicPeriod().subscribe(
      (res: any) => {
        this.optionsAcademicPeriod = res['data']['academic_period_id'];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      },
      err => {
        this.optionsAcademicPeriod = [];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      }
    );
  }

  getOptionsExaminationName(academicPeriodId: number) {
    this.markersDataService.getDropdownExaminationNme(academicPeriodId).subscribe(
      (res: any) => {
        if (res['data'].length) {
          this.dropdownOptions['examination_name'] = res['data'];
          this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
        } else {
          this.dropdownOptions['examination_name'] = [];
          this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
        }
      },
      err => {
        this.dropdownOptions['examination_name'] = [];
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getOptionsExaminationCentre(academicPeriodId: number) {
    this.markersDataService.getDropdownExaminationCentre(academicPeriodId).subscribe(
      (res: any) => {
        if (res['data'].length) {
          this.dropdownOptions['examination_centre'] = res['data'];
          this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
        } else {
          this.dropdownOptions['examination_centre'] = [];
          this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
        }
      },
      err => {
        this.dropdownOptions['examination_centre'] = [];
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getOptionsEducationSubject() {
    this.markersDataService.getDropdownEducationSubject().subscribe(
      (res: any) => {
        if (res['data'].length) {
          this.dropdownOptions['education_subject'] = res['data'];
          this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
        } else {
          this.dropdownOptions['education_subject'] = [];
          this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
        }
      },
      err => {
        this.dropdownOptions['education_subject'] = [];
        this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
      }
    );
  }

  getMarkersList(
    academicPeriodId: number,
    examinationId: number,
    examinationCentreId: number,
    educationSubjectId: number
  ) {
    this.markersDataService
      .getMarkersList(academicPeriodId, examinationId, examinationCentreId, educationSubjectId)
      .subscribe(
        (res: any) => {
          if (res['data'].length) {
            this.markersList = res['data'];
            this.markersListChanged.next([...this.markersList]);
          } else {
            this.markersList = [];
            this.markersListChanged.next([...this.markersList]);
          }
        },
        err => {
          this.markersList = [];
          this.markersListChanged.next([...this.markersList]);
        }
      );
  }

  searchMarkersList(
    academicPeriodid: number,
    examinationNameId: number,
    examinationCentreId: number,
    educationSubjectId: number,
    keyword: string
  ) {
    this.markersDataService
      .searchMarkersList(academicPeriodid, examinationNameId, examinationCentreId, educationSubjectId, keyword)
      .subscribe(
        (res: any) => {
          if (res && res['data'] && res['data'].length) {
            this.markersList = res['data'];
            this.markersListChanged.next([...this.markersList]);
          } else {
            this.markersList = [];
            this.markersListChanged.next([...this.markersList]);
          }
        },
        err => {
          this.markersList = [];
          this.markersListChanged.next([...this.markersList]);
        }
      );
  }
}
