import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarkersListRoutingModule } from './markers-list-routing.module';
import { ListComponent } from './list/list.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { MarkersListService } from './markers-list.service';

@NgModule({
  imports: [CommonModule, SharedModule, MarkersListRoutingModule],
  declarations: [ListComponent],
  providers: [MarkersListService]
})
export class MarkersListModule {}
