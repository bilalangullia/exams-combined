import { ITableColumn } from 'openemis-styleguide-lib';

export interface ITableQuestionFormatInterface {
  id?: string;
  questionKey?: string;
  question?: {
    key?: string;
    label?: string;
    required?: boolean;
    visible?: boolean;
    controlType?: string;
    [key: string]: any;
  };
}

export interface ITableQuestionInterface {
  selection?: {
    oneshot?: ITableQuestionFormatInterface;
  };
}

export interface IFilters {
  academic_period_id?: { id: number; value?: string };
  examination_name?: { id: number; value?: string };
  examination_centre?: { id: number; value?: string };
  education_subject?: { id: number; value?: string };
}

export interface IDropdownOptions {
  academic_period_id?: Array<any>;
  examination_name?: Array<any>;
  examination_centre?: Array<any>;
  education_subject?: Array<any>;
}

export interface IListColumn {
  OpenEMISId?: ITableColumn;
  MarkerId?: ITableColumn;
  FirstName?: ITableColumn;
  LastName?: ITableColumn;
  Gender?: ITableColumn;
}

export interface IAddOptions {
  academic_period_id?: Array<Object>;
  examination_id?: Array<Object>;
  examination_centre_id?: Array<Object>;
  classification?: any;
  gender_id?: Array<Object>;
  nationality_id?: Array<Object>;
  identity_type_id?: Array<Object>;
  address_area_id?: Array<any>;
  birthplace_area_id?: Array<any>;
  area_id?: Array<any>;
}

export interface IEditOptions {
  classification?: any;
  gender_id?: any;
  nationality_id?: any;
  identity_type_id?: any;
  address_area_id?: Array<any>;
  birthplace_area_id?: Array<any>;
  area_id?: Array<any>;
  examination_centre_id?: any;
}
