import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { DataService } from '../../../../shared/data.service';
import { SharedService } from '../../../../shared/shared.service';
import { detailsNotFound } from '../../../../shared/shared.toasters';
import { FeeDataService } from './fee-data.service';

@Injectable()
export class FeeService {
  private feeId: number;
  private feeList: any;
  private listFilterYear: any;
  private listFilterExam: any;
  private currentFilters: any = {};
  public feeViewDetails: any;

  public feeIdChanged = new BehaviorSubject<number>(this.feeId);
  public feeListChanged = new Subject();
  public listFilterYearChanged = new Subject();
  public listFilterExamChanged = new Subject();
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  public feeViewDetailsChanged = new Subject();

  constructor(
    private router: Router,
    private dataService: DataService,
    private sharedService: SharedService,
    private feeDataService: FeeDataService
  ) {}

  setFeeId(id: number) {
    if (id) {
      this.feeId = id;
      this.feeIdChanged.next(this.feeId);
    }
  }

  /*** LIST PAGE | FILTERS | Year***/
  getFilterYear() {
    let data = 'academicPeriods';
    this.dataService.getOptionsAcademicPeriod(data).subscribe(
      (res: any) => {
        if (res && res.data && res.data.academic_period_id) {
          this.listFilterYear = res.data.academic_period_id;
          this.listFilterYearChanged.next([...this.listFilterYear]);
        } else {
          this.listFilterYear = [];
          this.listFilterYearChanged.next([...this.listFilterYear]);
        }
      },
      (err) => {
        this.listFilterYear = [];
        this.listFilterYearChanged.next([...this.listFilterYear]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Exam Cert ***/
  getFilterExam(academic_id) {
    this.dataService.getOptionsExam(academic_id).subscribe(
      (res: any) => {
        if (res && res.data) {
          this.listFilterExam = res.data;
          this.listFilterExamChanged.next([...this.listFilterExam]);
        } else {
          this.listFilterExam = [];
          this.listFilterExamChanged.next([...this.listFilterExam]);
        }
      },
      (err) => {
        this.listFilterExam = [];
        this.listFilterExamChanged.next([...this.listFilterExam]);
      }
    );
  }

  /*** FEE  List Data | SEARCH***/
  getFeesListSearch(academic_period_id, examId, keyword) {
    this.feeDataService.getListSearch(academic_period_id, examId, keyword).subscribe(
      (data) => {
        if (data && data['data']) {
          this.feeList = data['data'];
          this.feeListChanged.next([...this.feeList]);
        } else {
          this.feeList = [];
          this.feeListChanged.next([...this.feeList]);
        }
      },
      (err) => {
        this.feeList = [];
        this.feeListChanged.next([...this.feeList]);
      }
    );
  }

  /*** FEE  List Data ***/
  getFeesList(academic_period_id, exam_id) {
    this.feeDataService.getList(academic_period_id, exam_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.feeList = res.data;
          this.feeListChanged.next([...this.feeList]);
        } else {
          this.feeList = [];
          this.feeListChanged.next([...this.feeList]);
        }
      },
      (err) => {
        this.feeList = [];
        this.feeListChanged.next([...this.feeList]);
      }
    );
  }

  /*** FEE  Details Data  ***/
  getFeesDetails(fee_id) {
    this.feeDataService.getFeeDetails(fee_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.feeViewDetails = res.data;
          this.feeViewDetailsChanged.next([...this.feeViewDetails]);
        } else {
          this.feeViewDetails = [];
          this.feeViewDetailsChanged.next([...this.feeViewDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.feeViewDetails = [];
        this.feeViewDetailsChanged.next([...this.feeViewDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** DELETE   ***/
  deleteFeeDetail(fee_id) {
    return this.feeDataService.deleteFeeDetail(fee_id);
  }

  /*** ADD PAGE ***/
  addFeeDetail(data) {
    return this.feeDataService.addFeeDetail(data);
  }

  /*** EDIT PAGE SUBMIT  ***/
  updateFeeDetail(fee_id, data) {
    return this.feeDataService.updateFeeDetail(fee_id, data);
  }

  /*** FEE  SET Current filters ***/
  setCurrentFilters(key: string, value: Object) {
    this.currentFilters[key] = value;
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** FEE  Empty Current filters ***/
  emptyCurrentFilters() {
    this.currentFilters = {};
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }
}
