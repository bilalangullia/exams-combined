import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class FeeDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /**  LIST API **/
  getList(academic_period_id, exam_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.fees}/${urls.feeList}?academic_period_id=${academic_period_id}&examId=${exam_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  LIST |  SEARCH | API  **/
  getListSearch(academic_period_id, examId, keyword) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.fees}/${urls.feeList}?academic_period_id=${academic_period_id}&examId=${examId}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW **/
  getFeeDetails(fee_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.fees}/${fee_id}/${urls.feeDetails}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  ADD PAGE **/
  addFeeDetail(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.fees}/${urls.feeAdd}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EDIT PAGE **/
  updateFeeDetail(fee_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.fees}/${urls.feeUpdate}/${fee_id}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW | DELETE API **/
  deleteFeeDetail(fee_id) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.fees}/${urls.feeDelete}/${fee_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
