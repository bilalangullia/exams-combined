import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeeEntryComponent } from './fee-entry/fee-entry.component';
import { FeeListComponent } from './fee-entry/fee-list/fee-list.component';
import { FeeEditComponent } from './fee-entry/fee-edit/fee-edit.component';
import { FeeAddComponent } from './fee-entry/fee-add/fee-add.component';
import { FeeViewComponent } from './fee-entry/fee-view/fee-view.component';

const routes: Routes = [
  {
    path: '',
    component: FeeEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: FeeListComponent },
      { path: 'edit', component: FeeEditComponent },
      { path: 'add', component: FeeAddComponent },
      { path: 'view', component: FeeViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeeRoutingModule {}
