import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { VIEWNODE_INPUT, IModalConfig } from './fee-view.config';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError, deleteSuccess, deleteFail, deleteError } from '../../../../../../shared/shared.toasters';
import { FeeService } from '../../fee.service';
@Component({
  selector: 'app-fee-view',
  templateUrl: './fee-view.component.html',
  styleUrls: ['./fee-view.component.css'],
  providers: [KdModalEvent]
})
export class FeeViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public feeId;

  /* Subscriptions */
  private feeIdSub: Subscription;
  private feeDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Fee Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.feeService.deleteFeeDetail(this.feeId).subscribe(
            (res: any) => {
              if (res.message) {
                event.preventDefault();
                if (this._modalEvent) {
                  this._modalEvent.toggleClose();
                }
                this.sharedService.setToaster(deleteSuccess);
                this._router.navigate(['main/examinations/fees/list']);
              }
            },
            (err) => {
              event.preventDefault();
              if (this._modalEvent) {
                this._modalEvent.toggleClose();
              }
              if (err['status'] == 403) {
                this.sharedService.setToaster({ ...deleteFail, body: err['error']['message'] });
              } else {
                this.sharedService.setToaster(deleteError);
              }
            }
          );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          if (this._modalEvent) {
            this._modalEvent.toggleClose();
          }
        }
      }
    ]
  };
  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private feeService: FeeService,
    public _modalEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private sharedService: SharedService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Examinations - Fees', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/examinations/fees/list' },
      { type: 'edit', path: 'main/examinations/fees/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.feeIdSub = this.feeService.feeIdChanged.subscribe((feeId) => {
      if (!feeId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/examinations/fees/list']);
        }, 0);
      } else {
        this.feeId = feeId;
        this.feeService.getFeesDetails(this.feeId);
        this.feeDetailsSub = this.feeService.feeViewDetailsChanged.subscribe((data) => {
          if (data) {
            this.setFeeDetails(data[0]);
          }
        });
      }
    });
  }

  setFeeDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        this.api.setProperty(
          this._questionBase[i].key,
          'value',
          !data[this._questionBase[i].key]
            ? ''
            : data[this._questionBase[i].key].value
            ? data[this._questionBase[i].key].value
            : data[this._questionBase[i].key]
        );
      }
    });
    this.loading = false;
  }

  open() {
    if (this._modalEvent) {
      this._modalEvent.toggleOpen();
    }
  }

  ngOnDestroy(): void {
    if (this.feeIdSub) {
      this.feeIdSub.unsubscribe();
    }
    if (this.feeDetailsSub) {
      this.feeDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
