export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: 0,
        value: '-- Select  --'
      }
    ],
    events: true
  },
  {
    key: 'examination_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: '',
        value: '-- Select --'
      }
    ],
    events: true
  }
];
interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Name?: Column;
  Amount?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Amount: {
    headerName: 'Amount',
    field: 'amount',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
