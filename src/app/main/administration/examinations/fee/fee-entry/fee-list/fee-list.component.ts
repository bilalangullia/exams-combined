import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdFilter,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi,
  KdAlertEvent,
  IPageheaderApi,
  IPageheaderConfig
} from 'openemis-styleguide-lib';

import { FeeService } from '../../fee.service';
import { FILTER_INPUTS, TABLECOLUMN } from './fee-list.config';

@Component({
  selector: 'app-fee-list',
  templateUrl: './fee-list.component.html',
  styleUrls: ['./fee-list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class FeeListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = {};
  public inputs = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public _row: Array<any>;

  public defaultList = {};
  private _question1Val: string;
  public academic_period_id: any;
  public examination_id: any;
  public itemFind = {};
  public initYear: any;
  public initExamination: any;
  public loading: boolean = true;
  public viewTable: boolean = false;

  /* Subscriptions */
  private yearFilterSub: Subscription;
  private examFilterSub: Subscription;
  private currentFilterSub: Subscription;
  private feeListSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    private feeService: FeeService,
    public _kdalert: KdAlertEvent
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.feeService.setFeeId(_rowNode.data.id);
            this.router.navigate(['main/examinations/fees/view']);
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.Name, TABLECOLUMN.Amount];

  ngOnInit() {
    this.loading = false;
    super.setPageTitle('Examinations - Fees', false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/examinations/fees/add' }]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.feeService.emptyCurrentFilters();
    this.feeService.getFilterYear();
    this.yearFilterSub = this.feeService.listFilterYearChanged.subscribe((data: any) => {
      if (data) {
        let tempOption = [];
        this.initYear = data[0].id;
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.name
          });
        });
        this._updateView.setInputProperty('academic_period_id', 'options', tempOption);
        this._updateView.setInputProperty('academic_period_id', 'value', tempOption[0].key);
        timer(100).subscribe((): void => {
          this.feeService.getFilterExam(this.initYear);
        });
      } else {
        this._updateView.setInputProperty('academic_period_id', 'options', [{ key: '', value: '-- Select --' }]);
      }
    });
    timer(100).subscribe((): void => {
      this.initializeSubscriptions();
    });

    this.currentFilterSub = this.feeService.currentFiltersChanged.subscribe((filters) => {
      if (filters['academic_period_id'] && filters['examination_id']) {
        if ((filters['academic_period_id'] && filters['examination_id']) !== '') {
          this.academic_period_id = filters['academic_period_id']['key'];
          this.examination_id = filters['examination_id']['key'];
          this.feeService.getFeesList(filters['academic_period_id']['key'], filters['examination_id']['key']);
        }
      }
    });

    this.feeListSub = this.feeService.feeListChanged.subscribe((list: Array<any>) => {
      if (list['length']) {
        this.populateTable(list);
      } else {
        this.viewTable = false;
      }
    });
  }

  initializeSubscriptions() {
    this.examFilterSub = this.feeService.listFilterExamChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        this.initExamination = data[0].id;
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.name
          });
        });

        this._updateView.setInputProperty('examination_id', 'options', tempOption);
        this._updateView.setInputProperty('examination_id', 'value', tempOption[0].key);
        // timer(100).subscribe((): void => {
        //   this.feeService.getFeesList(this.initYear, this.initExamination);
        // });
      } else {
        this._updateView.setInputProperty('examination_id', 'options', [{ key: '', value: '-- Select --' }]);
      }
    });
  }

  populateTable(list: Array<any>) {
    this.resetFilter();
    if (list.length) {
      this.loading = false;
      this.viewTable = true;
      this._row = list;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < list.length; j++) {
            if (list[j][this.tableColumns[i]['field']] && list[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(list[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(list[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(list[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
    }
  }

  public detectValue(question: any): void {
    this.defaultList[question.key] = question.value;
    let _defaultOptions: Array<any> = [{ key: '', value: '-- Select --' }];
    let newOptions: Array<any> = [
      {
        key: '',
        value: '-- Select From ' + this._question1Val + '-' + question.value + ' --'
      },
      {
        key: '1',
        value: 'Options ' + this._question1Val + '-' + question.value + '-1'
      },
      {
        key: '2',
        value: 'Options ' + this._question1Val + '-' + question.value + '-2'
      }
    ];
    if (question.key === 'academic_period_id') {
      this.academic_period_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);

      if (question['value']) {
        this.feeService.setCurrentFilters('academic_period_id', {
          ...this.itemFind
        });

        this._question1Val = '';
        this._question1Val = question.value;
        timer(100).subscribe((): void => {
          this.feeService.getFilterExam(this.academic_period_id);
        });
      } else {
        newOptions = _defaultOptions.slice();
        this.feeService.setCurrentFilters('academic_period_id', {});
      }
    } else if (question.key === 'examination_id') {
      this._question1Val = '';
      this.examination_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.feeService.setCurrentFilters('examination_id', {
          ...this.itemFind
        });
        this._question1Val = question.value;
      } else {
        newOptions = _defaultOptions.slice();
        this.feeService.setCurrentFilters('examination_id', {});
      }
    }
  }

  searchList(keyword: string) {
    if (keyword && keyword.length > 2) {
      this.feeService.getFeesListSearch(this.academic_period_id, this.examination_id, keyword);
    } else if (!keyword) {
      this.feeService.getFeesListSearch(this.academic_period_id, this.examination_id, '');
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.yearFilterSub) {
      this.yearFilterSub.unsubscribe();
    }
    if (this.examFilterSub) {
      this.examFilterSub.unsubscribe();
    }
    if (this.currentFilterSub) {
      this.currentFilterSub.unsubscribe();
    }
    if (this.feeListSub) {
      this.feeListSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
