import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { timer, Subscription } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { updateSuccess, saveFail, invalidIdError } from '../../../../../../shared/shared.toasters';
import { FeeService } from '../../fee.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './fee-edit.config';

@Component({
  selector: 'app-fee-edit',
  templateUrl: './fee-edit.component.html',
  styleUrls: ['./fee-edit.component.css']
})
export class FeeEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public formValue;
  public examinationId;
  public feeId;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private feeIdSub: Subscription;
  private feeDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private feeService: FeeService,
    private sharedService: SharedService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  }

  ngOnInit() {
    super.setPageTitle('Examinations - Fees', false);
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.feeIdSub = this.feeService.feeIdChanged.subscribe((feeId) => {
      if (!feeId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/examinations/fees/list']);
        }, 0);
      } else {
        this.feeId = feeId;
        this.feeService.getFeesDetails(this.feeId);
        this.feeDetailsSub = this.feeService.feeViewDetailsChanged.subscribe((data) => {
          if (data) {
            this.setFeeDetails(data[0]);
          }
        });
      }
    });

    this.filterValuesSub = this.feeService.currentFiltersChanged.subscribe((filters) => {
      if (filters['academic_period_id'] && filters['examination_id']) {
        if ((filters['academic_period_id'] && filters['examination_id']) !== '') {
          this.examinationId = filters['examination_id']['key'];
          this.setFeeDetails({
            academic_period: filters['academic_period_id'],
            examination: filters['examination_id']
          });
        } else {
          this._router.navigate(['main/examinations/fees/list']);
        }
      }
    });
  }

  setFeeDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        this.api.setProperty(
          this._questionBase[i].key,
          'value',
          !data[this._questionBase[i].key]
            ? ''
            : data[this._questionBase[i].key].value
            ? data[this._questionBase[i].key].value
            : data[this._questionBase[i].key]
        );
      }
    });
    this.loading = false;
  }

  submitVal(formVal): void {
    let payload = {
      name: formVal.name,
      amount: formVal.amount,
      examinations_id: this.examinationId
    };
    this.feeService.updateFeeDetail(this.feeId, payload).subscribe(
      (data: any) => {
        this.sharedService.setToaster(updateSuccess);
        this._router.navigate(['main/examinations/fees/list']);
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  cancel() {
    this._router.navigate(['main/examinations/fees/view']);
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.feeIdSub) {
      this.feeIdSub.unsubscribe();
    }
    if (this.feeDetailsSub) {
      this.feeDetailsSub.unsubscribe();
    }
    this.feeService.emptyCurrentFilters();
    super.destroyPageBaseSub();
  }
}
