export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text'
  },
  {
    key: 'amount',
    label: 'Amount ($)',
    visible: true,
    controlType: 'integer',
    type: 'number',
    float: 1
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
