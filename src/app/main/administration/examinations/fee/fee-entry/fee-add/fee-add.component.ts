import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { VIEWNODE_INPUT, FORM_BUTTONS } from './fee-add.config';
import { FeeService } from '../../fee.service';
import { SharedService } from '../../../../../../shared/shared.service';
import { saveSuccess, saveFail } from '../../../../../../shared/shared.toasters';

@Component({
  selector: 'app-fee-add',
  templateUrl: './fee-add.component.html',
  styleUrls: ['./fee-add.component.css']
})
export class FeeAddComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public formValue;
  public examinationId;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private feeService: FeeService,
    private sharedService: SharedService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  }
  ngOnInit() {
    super.setPageTitle('Examinations - Fees', false);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.filterValuesSub = this.feeService.currentFiltersChanged.subscribe((filters) => {
      if (filters['academic_period_id'] && filters['examination_id']) {
        if ((filters['academic_period_id'] && filters['examination_id']) !== '') {
          this.examinationId = filters['examination_id']['key'];
          this.viewFeeDetails({
            academic_period: filters['academic_period_id'],
            examination: filters['examination_id']
          });
        }
      } else {
        this._router.navigate(['/main/examinations/fees/list']);
      }
    });
  }

  viewFeeDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        this.api.setProperty(
          this._questionBase[i].key,
          'value',
          !data[this._questionBase[i].key]
            ? ' '
            : data[this._questionBase[i].key].value
            ? data[this._questionBase[i].key].value
            : data[this._questionBase[i].key]
        );
      }
    });
  }

  submitVal(formVal): void {
    let payload = {
      name: formVal.fee_name,
      amount: formVal.amount,
      examinations_id: this.examinationId
    };
    this.feeService.addFeeDetail(payload).subscribe(
      (data: any) => {
        this.sharedService.setToaster(saveSuccess);
        this._router.navigate(['main/examinations/fees/list']);
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  cancel() {
    this._router.navigate(['main/examinations/fees/list']);
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    this.feeService.emptyCurrentFilters();
    super.destroyPageBaseSub();
  }
}
