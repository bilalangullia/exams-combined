import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { FeeRoutingModule } from './fee-routing.module';
import { FeeEntryComponent } from './fee-entry/fee-entry.component';
import { FeeListComponent } from './fee-entry/fee-list/fee-list.component';
import { FeeViewComponent } from './fee-entry/fee-view/fee-view.component';
import { FeeEditComponent } from './fee-entry/fee-edit/fee-edit.component';
import { FeeAddComponent } from './fee-entry/fee-add/fee-add.component';
import { FeeService } from './fee.service';
import { FeeDataService } from './fee-data.service';

@NgModule({
  imports: [CommonModule, FeeRoutingModule, SharedModule],
  declarations: [FeeEntryComponent, FeeListComponent, FeeViewComponent, FeeEditComponent, FeeAddComponent],
  providers: [FeeService, FeeDataService]
})
export class FeeModule {}
