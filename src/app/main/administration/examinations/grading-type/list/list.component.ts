import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';

import { DataService } from '../../../../../shared/data.service';
import { SharedService } from '../../../../../shared/shared.service';
import { TABLECOLUMN, CREATE_ROW } from './config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'normalTable';
  readonly PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public _tableApi: ITableApi = {};

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    rowIdKey: 'id',
    loadType: 'oneshot',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    },
    action: {
      enabled: true,
      list: [
        {
          icon: 'far fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.gridView(_rowNode.data.id);
          }
        }
      ]
    }
  };

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Visible,
    TABLECOLUMN.Code,
    TABLECOLUMN.Name,
    TABLECOLUMN.ResultType,
    TABLECOLUMN.Max
  ];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    public _kdalert: KdAlertEvent,
    private _advFilterEvent: KdAdvFilterEvent,
    private _toolbarEvent: KdToolbarEvent,
    public _tableEvent: KdTableEvent,
    private kdtable: KdIntTableEvent,
    public dataService: DataService,
    public sharedService: SharedService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }
  ngOnInit() {
    super.setPageTitle('Examinations ­ Grading Types', false);
    super.setToolbarMainBtns([
      {
        type: 'add',
        path: '/main/examinations/gradingtypes/add'
      },
      {
        type: 'import',
        path: '/main/examinations/gradingtypes/import'
      }
    ]);

    super.enableToolbarSearch(true, (event: any): void => {
      if (event.length > 2 || event === '') {
        this.searchGradingList(event);
      }
    });

    super.updatePageHeader();
    super.updateBreadcrumb();

    this.setListData();
  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

  setListData() {
    this.dataService.gradingTypeList().subscribe((res: any) => {
      //   let temp =[]
      //  temp = CREATE_ROW(res.data)
      this.setGradingListFilter(res.data);
    });
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  setGradingListFilter(data: any) {
    this.resetFilter();

    if (data.length > 0) {
      this._row = data;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (data[j][this.tableColumns[i]['field']] && data[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(data[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
      this.loading = false;
    } else {
      this.loading = false;
      let toasterConfig: any = {
        type: 'error',
        title: 'No records found',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this._kdalert.error(toasterConfig);
    }
  }

  searchGradingList(data: any) {
    this.dataService.searchGradingTypeList(data).subscribe((res: any) => {
      this.setGradingListFilter(res.data);
    });
  }

  gridView(data) {
    this.sharedService.setGridType(data);
    this.router.navigate(['/main/examinations/gradingtypes/view']);
  }
}
