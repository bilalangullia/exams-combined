

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image' ;
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;

}

interface ListColumn {
    Visible?:Column
    Code?: Column;
    Name?:Column;
    ResultType?:Column;
    Max?:Column;
 
}

export const TABLECOLUMN: ListColumn = {
    Visible: {
        headerName: 'Visible',
        field: 'visible_image',
        type: 'image',
        config: {
          image: {
            width: 80, 
            height: 90,
            isExpandable: false,
            icon: 'fa fa-check',
            borderType: 'box'
         }
      }
    },
 
    Code: {
      headerName: 'Code',
      field: 'code',
      sortable: true,
      filterable: true,
      filterValue: []
  },

 
  Name: {
      headerName: 'Name',
      field: 'name',
      sortable: true,
      filterable: true,
      filterValue: []
  },
  ResultType: {
      headerName: 'Result Type',
      field: 'result_type',
      sortable: true,
      filterable: false,
      filterValue: []
    
  },
  Max: {
      headerName: 'Max',
      field: 'max',
      sortable: true,
      filterable: true,
      filterValue: []
    
  },
};


export const CREATE_ROW: (datalist?:Array<any>) => Array<any> = (datalist?:Array<any>): Array<any> => {
    let row: Array<any> = [];
    for (let i=0; i < datalist.length; i++) {
        let oneRow: any = {
            visible_image:{
                filename: 'tick_icon.png',
                link: {
                    key: 'image3',
                    url:'./image.json'
                }
            },
            code:datalist[i].code,
            name: datalist[i].name,
            result_type: datalist[i].result_type,
            max: datalist[i].max,
        };
		row.push(oneRow);
	}
	return row;
};


