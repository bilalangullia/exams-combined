import { ITableActionApi } from 'openemis-styleguide-lib';

interface IResponsesTableColumns {
  name: any;
  description: any;
  min: any;
  max: any;
  points: any;
}

const Name = {
  headerName: 'Name',
  label: 'Name',
  field: 'name',
  visible: true,
  type: 'input',
  config: {
    input: {
      controlType: 'text',
      key: 'name1',
      visible: true
    }
  }
};
const Description = {
  headerName: 'Description',
  label: 'Description.',
  field: 'description',
  visible: true,
  type: 'input',
  config: {
    input: {
      controlType: 'text',
      key: 'description',
      visible: true
    }
  }
};
const Min = {
  headerName: 'Min',
  label: 'Min',
  field: 'min',
  visible: true,
  type: 'input',
  config: {
    input: {
      controlType: 'text',
      key: 'min',
      visible: true
    }
  }
};
const Max = {
  headerName: 'Max',
  label: 'Max',
  field: 'max',
  visible: true,
  type: 'input',
  config: {
    input: {
      controlType: 'text',
      key: 'max',
      visible: true
    }
  }
};
const Points = {
  headerName: 'Points',
  label: 'Points',
  field: 'points',
  visible: true,
  type: 'input',
  config: {
    input: {
      controlType: 'text',
      key: 'points',
      visible: true
    }
  }
};

export const TABLE_COLUMN_LIST: IResponsesTableColumns = {
  name: Name,
  description: Description,
  min: Min,
  max: Max,
  points: Points
};
interface TableQuestionFormatInterface {
  id?: string;
  questionKey?: string;
  question?: {
    key?: string;
    label?: string;
    required?: boolean;
    visible?: boolean;
    controlType?: string;
    row?: any;
    [key: string]: any;
  };
}

export const CREATE_ROW1: (num_rows: number, baseIndex?: number) => Array<any> = (
  num_rows: number,
  baseIndex: number = 0,
  data?: Array<any>
): Array<any> => {
  let row: Array<any> = [];
  for (let i: number = baseIndex; i < num_rows + baseIndex; i++) {
    let oneRow = { id: i + 1 };
    row.push(oneRow);
  }
  return row;
};

export const CREATE_ROW: (num_rows: number, baseIndex?: number, data?: Array<any>) => Array<any> = (
  num_rows: number,
  baseIndex: number = 0,
  data?: Array<any>
): Array<any> => {
  console.log("num_rows..num_rows",num_rows,data)
  let row: Array<any> = [];
  for (let i: number = baseIndex; i < num_rows + baseIndex; i++) {
    let oneRow = {
      id: i + 1,
      question: i + 1,
      name: { name1: data[i].name },
      description: { description: data[i].description },
      min: { min: data[i].min },
      max: { max: data[i].max },
      points: { points: data[i].points }
    };
    row.push(oneRow);
  }
  return row;
};

export const RESPONSES_FORM_TABLE: TableQuestionFormatInterface = {
  id: 'gradingOptions',
  questionKey: 'gradingOptions',
  question: {
    key: 'gradingOptions',
    label: 'Grading Options',
    visible: true,
    required: false,
    controlType: 'table',
    row: [],
    column: [
      TABLE_COLUMN_LIST.name,
      TABLE_COLUMN_LIST.description,
      TABLE_COLUMN_LIST.min,
      TABLE_COLUMN_LIST.max,
      TABLE_COLUMN_LIST.points
    ],
    config: {
      rowId: 'id',
      id: 'response',
      gridHeight: 300,
      loadType: 'normal',
      action: {
        enabled: true,
        list: [
          {
            type: 'delete',
            callback: (_node: any, _tableApi: ITableActionApi): void => {
              console.log('_node..._node', _node);

              _tableApi.deleteThisRow();
            }
          }
        ]
      },
      paginationConfig: {
        pagesize: 10
      },
      button: [
        { label: 'Insert row', icon: 'fa fa-plus', type: 'insert' }
        // { label: "Delete row", icon: "fa fa-minus", type: "delete" }
      ]
    }
  }
};

export const QUESTION_BASE: Array<any> = [
  {
    key: 'code',
    label: 'Code',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'id',
    label: 'id',
    controlType: 'hidden',
    type: 'hidden'
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'pass_mark',
    label: 'Pass Mark',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'max',
    label: 'Max',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'result_type',
    label: 'Result Type',
    visible: true,
    controlType: 'dropdown',
    options: [
      {
        key: 'GRADES',
        value: 'GRADES'
      },
      {
        key: 'MARKS',
        value: 'MARKS'
      }
    ]
  },
  RESPONSES_FORM_TABLE.question
];

export const _formButtons: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
