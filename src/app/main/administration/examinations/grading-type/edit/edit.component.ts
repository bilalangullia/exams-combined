import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  KdAlertEvent,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { QUESTION_BASE, CREATE_ROW, RESPONSES_FORM_TABLE, _formButtons, CREATE_ROW1 } from './config'
import { DataService } from '../../../../../shared/data.service'
import { SharedService } from '../../../../../shared/shared.service'

import { timer } from 'rxjs';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  public api: IDynamicFormApi = {};
  public _questionBase: any;
  public formButton: any;
  public isValues: boolean = false
  public rowCount: any = 0;
  //public loading: boolean = false;
  public id: any;

  constructor(public _router: Router,
    _activatedRoute: ActivatedRoute,
    _pageEvent: KdPageBaseEvent,
    public dataService: DataService,
    public shareService: SharedService,
    public kdAlert: KdAlertEvent) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }

  ngOnInit() {
    super.setPageTitle('Examinations ­ Grading Types', false);
    super.setToolbarMainBtns([]);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this._questionBase = QUESTION_BASE
    this.formButton = _formButtons

    this.setEditValue();
  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

  public _detectGridButton(event) {
    this.api.table(event.formKey).addRow(CREATE_ROW1(1, this.rowCount), true)
    this.rowCount++;

  }

  public setEditValue() {
    let gridId = this.shareService.getGridType()

    if (gridId != undefined) {
      this.dataService.viewGradingType(gridId).subscribe(
        (data: any) => {
          this.id = data.data.id
            console.log("this._questionBase[i]",this._questionBase);
            
          for (let i = 0; i < this._questionBase.length; i++) {
            if (this._questionBase[i].key != 'gradingOptions' && this._questionBase[i].key != 'result_type' && this._questionBase[i].key != 'id') {
              this.api.setProperty(this._questionBase[i].key, 'value', data.data ? data.data[this._questionBase[i].key] : '');
            }
            else if (this._questionBase[i].key == 'result_type') {
              let tempdata = (data.data['result_type'] == 'MARKS') ? 'MARKS' : 'GRADES';
              this.api.setProperty(this._questionBase[i].key, 'value', tempdata);
            }
            else if(this._questionBase[i].key== 'gradingOptions') {
              timer(100).subscribe(() => {
              
               (this._questionBase[i].row)?this._questionBase[i].row.length = 0:'na';
                this.rowCount = data.data.gradingOptions.length
                this.api.table('gradingOptions').addRow(CREATE_ROW(data.data.gradingOptions.length, 0, data.data.gradingOptions), true)

              })
            }
          }
        })
    }
    else {
      this._router.navigate(['main/examinations/gradingtypes/list']);

    }
  }

  reset() {
    this._router.navigate(['main/examinations/gradingtypes/list']);

  }

  submitEvent(event) {
    let tempdata = {
      id: this.id,
      max: event.max,
      name: event.name,
      pass_mark: event.pass_mark,
      result_type: event.result_type,
      code: event.code,
      gradingOption: event.gradingOptions.map((item) => {
        return {
          //  id:item.id,
          // question:item.question,
          description: item.description['description'],
          max: item.max['max'],
          min: item.min['min'],
          name: item.name['name1'],
          points: item.points['points']
        }
      })
    }

    this.dataService.updateGradingType(tempdata).subscribe((res) => {
      if (res) {
        this._router.navigate(['main/examinations/gradingtypes/list']);

        let toasterConfig: any = {
          type: 'success',
          title: 'Record Updated',
          body: 'response updated successfully.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 500
        };

        this.kdAlert.success(toasterConfig)
      }
    }, err => {
      let toasterConfig: any = {
        type: 'success',
        title: 'Record Updated',
        body: 'response updated unsuccessfully.',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 500
      };

      this.kdAlert.error(toasterConfig)
    }
    )
  }
}