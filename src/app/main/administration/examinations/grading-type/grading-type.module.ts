import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GradingTypeRoutingModule } from './grading-type-routing.module';
import { ListComponent } from './list/list.component';
import { GradinTypeMainComponent } from './gradin-type-main.component';
import { SharedModule } from "../../../../shared/shared.module";
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { ImportFormComponent } from './import-form/import-form.component';
import { ImportDetailComponent } from './import-detail/import-detail.component';


@NgModule({
  imports: [
    CommonModule,
    GradingTypeRoutingModule,
    SharedModule
  ],
  declarations: [ListComponent, GradinTypeMainComponent, ViewComponent, EditComponent, AddComponent, ImportFormComponent, ImportDetailComponent]
})
export class GradingTypeModule { }
