import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdAlertEvent } from 'openemis-styleguide-lib';
import { file_INPUT } from './import-form.config';
import { DataService } from '../../../../../shared/data.service';
import { SharedService } from '../../../../../shared/shared.service';
import { timer } from 'rxjs';
@Component({
  selector: 'app-import-form',
  templateUrl: './import-form.component.html',
  styleUrls: ['./import-form.component.css']
})
export class ImportFormComponent extends KdPageBase implements OnInit, OnDestroy {

  public loading: boolean = true;
  public fileinput: Array<any> = [
    file_INPUT.file_inputs,
  ];
  public button: Array<any> = [
    {
      name: 'Import',
      btnType: 'btn-text',
      type: 'submit',
      icon: 'kd-import',
      key: 'fileinput_double_buttons',
      controlType: 'file-input',
    },
    {
      name: 'Cancel',
      type: 'reset',
      class: 'btn-outline',
      icon: 'kd-cross',
      
    }
  ];
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  public defaultValues:any;
  constructor(
    _pageEvent: KdPageBaseEvent,
    public _router: Router,
    _activatedRoute: ActivatedRoute,
    public _KadAlert: KdAlertEvent,
    public dataService: DataService,
    public sharedService: SharedService,
  ) {
    super({
      router: _router,
      pageEvent: _pageEvent,
      activatedRoute: _activatedRoute
    });
  }

  ngOnInit(): void {
    super.setPageTitle('Examinations ­ Grading Types', false);
    super.setToolbarMainBtns([]);

    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading=false;
 
  }

  public detectValue(question: any): void {
    // console.log('CreateNode - Detect Value from question: ', question);
    this.formValue = question.value; 
  }

 
  public _buttonEvent(event: any): void {
    const formData = new FormData();
    
    formData.append('template', event.fileinput_double_buttons);

    this.dataService.importGradingType(formData).subscribe(res => {

      
      this.sharedService.setGradingType(res)
      
      this._router.navigate(['/main/examinations/gradingtypes/detail'])
    
    }, err => {
          let toasterConfig: any = {
        title: 'Something went Wrong',
        body: 'Import template required.',
        showCloseButton: true,
        tapToDismiss: true,
      };
      this._KadAlert.error(toasterConfig);
    })
  }
  reset(){
    this._router.navigate(['/main/examinations/gradingtypes/list'])
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

}
