import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GradinTypeMainComponent} from './gradin-type-main.component'
import {ListComponent} from '../grading-type/list/list.component'
import {ViewComponent} from '../grading-type/view/view.component'
import {EditComponent} from '../grading-type/edit/edit.component'
import {AddComponent} from '../grading-type/add/add.component'
import {ImportFormComponent} from '../grading-type/import-form/import-form.component'
import {ImportDetailComponent} from '../grading-type/import-detail/import-detail.component'

const routes: Routes =  [ {
  path: '',
  component:GradinTypeMainComponent,
  children: [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    { path: 'list', component:ListComponent},
    { path: 'view', component:ViewComponent},
    { path: 'edit', component:EditComponent},
    { path: 'add', component:AddComponent},
    {path:'import',component:ImportFormComponent},
    {path:'detail',component:ImportDetailComponent}
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradingTypeRoutingModule { }
