import { Observable, Subscriber, timer } from 'rxjs';

interface Column {
    headerName?: string;
    field?: string;
    type?: 'input' | 'normal' | 'image';
    sortable?: boolean;
    filterable?: boolean;
    visible?: boolean;
    config?: any;
    class?: string;
    filterValue?: Array<string>;
}

interface ListColumn {
    RowNumber?: Column;
    Description?: Column;
    Grading_Type_Id?: Column,
    Max?: Column,
    Min?: Column,
    Name?:Column,
    Points?:Column
}

export const TABLECOLUMN: ListColumn = {
    RowNumber: {
        headerName: 'Row Number',
        field: 'row_number',
        sortable: true,
        filterable: false
    },
    Description: {
        headerName: 'description',
        field: 'Description',
        sortable: true,
        filterable: false
    },
    Grading_Type_Id: {
        headerName: 'Grading Type Id',
        field: 'grading_type_id',
        sortable: true,
        filterable: false
    },
    Max: {
      headerName: 'Max',
      field: 'max',
      sortable: true,
      filterable: false
  },
  Min: {
        headerName: 'Min',
        field: 'min',
        sortable: true,
        filterable: false
    },
    Name: {
        headerName: 'Name',
        field: 'name',
        sortable: true,
        filterable: false
    },
    Points: {
        headerName: 'Points',
        field: 'points',
        sortable: true,
        filterable: false
    }

};
export const CREATE_ROW: (_rowCount: number, _baseIndex?: number, data?: Array<any>) => Array<any> = (_rowCount: number, _baseIndex: number = 0, data?: Array<any>): Array<any> => {
    let row: Array<any> = [];
    for (let i: number = 0; i < data.length; i++) {
        let oneRow: any = {
            id: i,
            row_number:i+1,
            description: data[i].data['Description'],
            grading_type_id:data[i].data['Grading Type Id'],
            max: data[i].data['Max'],
            min: data[i].data['Min'],
            name: data[i].data['Name'],
            points: data[i].data['Points'] 
        };
        row.push(oneRow);
    }

    return row;
};


export const CREATE_TABLE_CONFIG: (_id: string, _pagesize: number, _total: number) => any = (_id: string, _pagesize: number, _total: number): any => {
    return {
        id: _id,
        loadType: 'server',
        gridHeight: '600',
        externalFilter: false,
        paginationConfig: {
            pagesize: _pagesize,
            total: _total
        },
        click: {
            type: 'router',
            // pathMap: 'view',
            path: '/registration/view',
            callback: (): void => {
                console.log('ListNode: Demo callback used in when clicking the rowNode.');
            }
        }
    };
};

export const DUMMY_API_CALL: (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }) => Observable<any> =
    (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }): Observable<any> => {
        return new Observable((_observer: Subscriber<any>): void => {
            timer(1000).subscribe((): void => {
                _observer.next(CREATE_ROW(_params.pagesize, _params.startRow));
                _observer.complete();
            });
        });
    };

    export interface IMiniDashboardItem {
        type: string;
        icon?: string;
        label: string;
        value: number | string ;
    }
    
    export interface IMiniDashboardConfig {
        closeButtonDisabled?: boolean;
        rtl?: boolean;
    }
    
    
    export const MINI_DASHBOARD_CONFIG: IMiniDashboardConfig = {
        closeButtonDisabled: true,
        rtl: true,
    };
    
    export const MINI_DASHBOARD_DATA: Array<IMiniDashboardItem> = [
        {
            type: 'text',
            label: 'Total Rows :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Imported :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Update :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Failed :',
            value: '0'
        },
    ]