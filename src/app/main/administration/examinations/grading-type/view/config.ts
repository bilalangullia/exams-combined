interface TableColumns {
  Name?: any;
  Description?: any;
  Min?: any;
  Max?:any;
  Points?:any;
}

const COLUMN_OPTION_Description: any ={
  'headerName': 'Description',
  'field': 'description',
  'sortable': false,
  'filterable': false,
  'visible': true,
  'class': 'ag-id'
};

const COLUMN_OPTION_NAME: any ={
  'headerName': 'Name',
  'field': 'name',
  'sortable': false,
  'filterable': false,
  'visible': true
};

const COLUMN_OPTION_Min: any = {
  'headerName': 'Min',
  'field': 'min',
  'sortable': false,
  'filterable': false,
  'visible': true
};
const COLUMN_OPTION_Max: any = {
  'headerName': 'Max',
  'field': 'max',
  'sortable': false,
  'filterable': false,
  'visible': true
};
const COLUMN_OPTION_Points: any = {
  'headerName': 'Points',
  'field': 'points',
  'sortable': false,
  'filterable': false,
  'visible': true
};
export const TABLE_COLUMN_LIST: TableColumns = {
  Name: COLUMN_OPTION_NAME,
  Description: COLUMN_OPTION_Description,
  Min: COLUMN_OPTION_Min,
  Max:COLUMN_OPTION_Max,
  Points:COLUMN_OPTION_Points
}


export const VIEWNODE_INPUT: Array<any> = [
  {
    'key': 'code',
    'label': 'Code',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'name',
    'label': 'Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'pass_mark',
    'label': 'Pass Mark',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'max',
    'label': 'Max',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'result_type',
    'label': 'Result Type',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'gradingOptions',
    'label': 'Grading Options',
    'visible': true,
    'controlType': 'table',
    'row': [],
    'column': [
      TABLE_COLUMN_LIST.Name,
      TABLE_COLUMN_LIST.Description,
      TABLE_COLUMN_LIST.Min,
      TABLE_COLUMN_LIST.Max,
      TABLE_COLUMN_LIST.Points
    ],
    'config': {
      'id': 'options',
      'rowIdKey': 'id',
      'gridHeight': 250,
      'loadType': 'normal'
    }
  },
  {
    'key': 'modified_by',
    'label': 'Modified By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }, 
  {
    'key': 'modified_on',
    'label': 'Modified On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_by',
    'label': 'Created By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_on',
    'label': 'Created On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }
];