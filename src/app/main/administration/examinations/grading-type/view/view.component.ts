import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { KdPageBase, KdPageBaseEvent, KdToolbarEvent, IDynamicFormApi, KdAdvFilterEvent, KdTableEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { VIEWNODE_INPUT } from './config'
import { SharedService } from '../../../../../shared/shared.service'
import { DataService } from '../../../../../shared/data.service'

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {

  public _questionBase = VIEWNODE_INPUT;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null;

  public gridId: any;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _kdalert: KdAlertEvent,
    public sharedService: SharedService,
    public dataService: DataService

  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }

  ngOnInit() {
    super.setPageTitle('Examinations ­ Grading Types', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => { this._router.navigate(['main/examinations/gradingtypes/list']); }
      }, {
        type: 'edit',
        callback: (): void => {
          this.getEditView()
        }
      }, {
        type: 'delete',
        callback: (): void => { 
          this.dataService.deleteGradingType(this.gridId).subscribe(
            (data:any)=>{
                    console.log("data...data",data);
                    
          })
        }
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.setGridview();

  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }


  setGridview() {

    this.gridId = this.sharedService.getGridType()
    
    if (this.gridId != undefined) {    
      this.dataService.viewGradingType(this.gridId).subscribe(
        (res: any) => {
          this.setGridDetail(res.data)
        }
      )
    }
    else{
      this._router.navigate(['main/examinations/gradingtypes/list']);
    }

  }


  getEditView() {
    
    this._router.navigate(['main/examinations/gradingtypes/edit']);
  }




  setGridDetail(data: any) {
    console.log(data)
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'gradingOptions') {
          this.api.setProperty(this._questionBase[i].key, 'row', data.gradingOptions);
        } else {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key] ? data[this._questionBase[i].key] : 'NA');
        }
      }
    }, 500);
  }
}
