import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  KdAlertEvent,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { DataService } from '../../../../../shared/data.service'
import { SharedService } from '../../../../../shared/shared.service'

import { QUESTION_BASE, RESPONSES_FORM_TABLE, _formButtons, CREATE_ROW1 } from './add-grading-type.config';
import { timer } from 'rxjs';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  public api: IDynamicFormApi = {};
  public _questionBase: any;
  public formButton: any;
  public isValues: boolean = false
  public rowCount: any = 0;
  //public loading: boolean = false;
  public id: any;

  constructor(public _router: Router,
    _activatedRoute: ActivatedRoute,
    _pageEvent: KdPageBaseEvent,
    public dataService: DataService,
    public shareService: SharedService,
    public kdAlert: KdAlertEvent) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }

  ngOnInit() {
    super.setPageTitle('Examinations ­ Grading Types', false);
    super.setToolbarMainBtns([]);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this._questionBase = QUESTION_BASE
    this.formButton = _formButtons

  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

  public _detectGridButton(event) {
    this.api.table(event.formKey).addRow(CREATE_ROW1(1, this.rowCount), true)
    this.rowCount++;

  }

  
 
  reset() {
    this._router.navigate(['main/examinations/gradingtypes/list']);

  }

  requiredCheck(form) {
    let hasError: boolean = false;
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]["required"]) {
        if (
          !form[this._questionBase[i]["key"]] ||
          form[this._questionBase[i]["key"]] == ""
        ) {
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]["key"], "errors", [
              "This field is required",
            ]);
          }, 1000);
          hasError = true;
          break;
        }
      }
    }
    return hasError;
  }

  submitEvent(event) {
    let tempdata = {
      id: this.id,
      max: event.max,
      name: event.name,
      pass_mark: event.pass_mark,
      result_type: event.result_type,
      code: event.code,
      gradingOption: event.gradingOptions.map((item) => {
        return {
          description: item.description['description'],
          max: item.max['max'],
          min: item.min['min'],
          name: item.name['name1'],
          points: item.points['points']
        }
      })
    }
    if(!this.requiredCheck(event)){
      this.dataService.addGradingType(tempdata).subscribe((res) => {
        if (res) {
          this._router.navigate(['main/examinations/gradingtypes/list']);
  
          let toasterConfig: any = {
            type: 'Success',
            title: 'Record added',
            body: 'response added successfully.',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 500
          };
  
          this.kdAlert.success(toasterConfig)
        }
      }, err => {
        let toasterConfig: any = {
          type: 'Error',
          title: 'Something went wrong',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 500
        };
        this.kdAlert.error(toasterConfig)
      }
      )
      
    }  else{
        let toasterConfig: any = {
          type: "error",
          title: "Mandatory Fields",
          body: "Please provide Mandatory Fields data",
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000,
        };
        this.kdAlert.error(toasterConfig);
      
    }
  }
}