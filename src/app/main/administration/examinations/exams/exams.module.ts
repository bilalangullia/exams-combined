import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { ExamsRoutingModule } from './exams-routing.module';
import { ExamsService } from './exams.service';
import { ExamsDataService } from './exams-data.service';
import { ExamsManagerService } from './exams-details/exams/exams-manager.service';

@NgModule({
  imports: [CommonModule, ExamsRoutingModule, SharedModule],
  declarations: [],
  providers: [ExamsService, ExamsDataService, ExamsManagerService]
})
export class ExamsModule {}
