import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class ExamsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /**   EXAMS | LIST | API  **/
  getExamsList(start: number, end: number) {
    //openemis.n2.iworklab.com/api/administration/examinations/exam/exam-list?start=0&end=5
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.examsList}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | SEARCH | API  **/
  getExamsListSearch(keyword: string, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.examsList}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ACADEMIC PERIOD DROPDOWN **/
  getOptionsAcademicPeriod(data) {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.registration}/${urls.candidate}/dropdown?entities=${data}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | EDIT | EDUCATION PROGRAMME DROPDOWN | API  **/
  getEducationProgrammeDropdown() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.educationProgramme}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | EDIT | EDUCATION GRADE DROPDOWN | API  **/
  getEducationGradeDropdown(education_programme_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.educationGrade}/${education_programme_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | EDIT | EXAM SESSION DROPDOWN | API  **/
  getExamSessionDropdown() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.examSession}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | EDIT | EXAM TYPE DROPDOWN | API  **/
  getExamTypeDropdown() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.examType}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | EDIT | ATTENDANCE TYPE DROPDOWN | API  **/
  getAttendanceTypeDetails(attendanceTypes) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}?entities=${attendanceTypes}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  /**  EXAMS | EDIT | EXAM EDIT SUBMIT API  **/
  updateExamDetail(exam_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.examUpdate}/${exam_id}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** VIEW | DELETE API ***/
  deleteExamDetails(examId) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/exam/${urls.deleteExam}/${examId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** ADD API ***/
  addExamDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/exam/${urls.addExam}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** COPY | EXAM DROPDOWN  **/
  getOptionsExam(academic_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/dropdown/${academic_id}/examination`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** COPY API ***/
  copyExamDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/exam/${urls.copyExam}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
