import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer, BehaviorSubject } from 'rxjs';
import { ExcelExportParams } from 'ag-grid';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  ITableDatasourceParams,
  KdTableDatasourceEvent,
  IPageheaderConfig,
  IPageheaderApi,
  IBreadcrumbConfig
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { DataService } from '../../../../../../shared/data.service';
import { SharedService } from '../../../../../../shared/shared.service';
import { ExamsService } from '../../exams.service';
import { ExamsListService } from '../exams-list.service';
import { TABLECOLUMN } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ExamsListService, KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'examsList';
  readonly PAGESIZE: number = 20;
  private tableEventList: any;
  public showTable: boolean = false;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public pageheader: IPageheaderConfig;
  public _pageHeaderApi: IPageheaderApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };

  /*   SUBSCRIPTION */
  private tableEventSub: Subscription;

  public invalidToasterConfig = {
    type: 'error',
    title: 'Invalid/Missing Examination ID',
    showCloseButton: true,
    tapToDismiss: true,
    timeout: 3000
  };
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          type: 'View',
          callback: (_rowNode, _tableApi): void => {
            if (_rowNode['data']['exam_id']) {
              this.examsService.setAcademicPeriod(_rowNode['data']['academic_period']);
              this.examsService.setExaminationId(_rowNode['data']['exam_id']);
              this.examsService.setExaminationValue(
                `${_rowNode['data']['exam_code']} - ${_rowNode['data']['exam_name']}`
              );
              this.examsService.setExamination({
                id: _rowNode['data']['exam_id'],
                name: `${_rowNode['data']['exam_code']} - ${_rowNode['data']['exam_name']}`
              });
              this.examsService.setAcademicPeriodData({
                id: _rowNode['data']['academic_period_id'],
                name: _rowNode['data']['academic_period']
              });
              timer(200).subscribe(() => {
                this.router.navigate(['main/examinations/exams/details/view']);
              });
            } else {
              this.sharedService.setToaster(this.invalidToasterConfig);
            }
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.ExamCode,
    TABLECOLUMN.ExamName,
    TABLECOLUMN.ExamType,
    TABLECOLUMN.ExamSession,
    TABLECOLUMN.AcademicPeriod,
    TABLECOLUMN.EducationGrade
  ];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    public examsService: ExamsService,
    private examsListService: ExamsListService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.setPageTitle('Examinations - Exams', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/examinations/exams/list/add' },
      { custom: true, icon: 'fa fa-copy', tooltip: 'Copy', path: 'main/examinations/exams/list/copy' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'examinations_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });

    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;
    this.showTable = true;
    this.examsService.setExaminationId(null);
    this.examsService.setExaminationValue('');
    // this.examsListService.getExamsList();

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      listReq = this.examsListService.getExamsListSearch(searchKey, fetchParams['startRow'], fetchParams['endRow']);
    } else {
      listReq = this.examsListService.getExamsList(fetchParams['startRow'], fetchParams['endRow']);
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['examList'] && res['data']['examList'].length) {
          list = res['data']['examList'];
          total =
            this.searchKey && this.searchKey.length ? res['data']['examList'].length : res['data']['totalRecords'];
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }
  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue = [];
      }
    }
  }

  ngOnDestroy(): void {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
