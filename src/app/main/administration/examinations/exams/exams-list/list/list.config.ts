interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  ExamCode?: Column;
  ExamName?: Column;
  ExamType?: Column;
  ExamSession?: Column;
  AcademicPeriod?: Column;
  EducationGrade?: Column;
}

export const TABLECOLUMN: ListColumn = {
  ExamCode: {
    headerName: 'Exam Code',
    field: 'exam_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ExamName: {
    headerName: 'Exam Name',
    field: 'exam_name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ExamType: {
    headerName: 'Exam Type',
    field: 'exam_type',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ExamSession: {
    headerName: 'Exam Session',
    field: 'exam_session',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  AcademicPeriod: {
    headerName: 'Academic Period',
    field: 'academic_period',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationGrade: {
    headerName: 'Education Grade',
    field: 'education_grade',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
