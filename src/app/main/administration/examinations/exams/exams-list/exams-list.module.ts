import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamsListRoutingModule } from './exams-list-routing.module';
import { SharedModule } from '../../../../../shared/shared.module';
import { ExamsListService } from './exams-list.service';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { CopyComponent } from './copy/copy.component';

@NgModule({
  imports: [CommonModule, ExamsListRoutingModule, SharedModule],
  declarations: [ListComponent, AddComponent, CopyComponent],
  providers: [ExamsListService]
})
export class ExamsListModule {}
