export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'from_academic_period',
    label: 'From Academic Period',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'from_examination',
    label: 'From Examination',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'exam_code',
    label: 'Exam Code',
    visible: true,
    required: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'exam_name',
    label: 'Exam Name',
    visible: true,
    required: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'exam_type',
    label: 'Exam Type Name',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'attendance_name',
    label: 'Attendance Type Name',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'exam_session',
    label: 'Exam Session',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'education_programme',
    label: 'Education Programme',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'education_grade',
    label: 'Education Grade',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  },
  {
    key: 'registration_start_date',
    label: 'Registration Start Date',
    visible: true,
    controlType: 'date',
    required: true
  },
  {
    key: 'registration_end_date',
    label: 'Registration End Date',
    visible: true,
    controlType: 'date',
    required: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
