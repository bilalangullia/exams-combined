import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdView,
  IPageheaderApi,
  IPageheaderConfig,
  IBreadcrumbConfig,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { ExamsListService } from '../exams-list.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './copy.config';

@Component({
  selector: 'app-copy',
  templateUrl: './copy.component.html',
  styleUrls: ['./copy.component.css'],
  providers: [ExamsListService]
})
export class CopyComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public pageheader: IPageheaderConfig;
  public _pageHeaderApi: IPageheaderApi = {};
  public api: IDynamicFormApi = {};
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };

  /* Subscriptions */
  private educationProgrammeSub: Subscription;
  private examSessionSub: Subscription;
  private examTypeSub: Subscription;
  private educationGradeSub: Subscription;
  private academicYearSub: Subscription;
  private examSub: Subscription;
  private attendanceTypeSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private examsListService: ExamsListService,
    private sharedService: SharedService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: pageEvent });
    super.setPageTitle('Examinations - Exams', false);

    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;
    this.examsListService.getEducationProgramme();
    this.examsListService.getExamSession();
    this.examsListService.getExamType();
    this.examsListService.getOptionsAcademicPeriod();
    this.examsListService.getAttendanceType('attendanceTypes');
    this.initializeSubscriptions();
  }

  initializeSubscriptions() {
    this.educationProgrammeSub = this.examsListService.educationProgrammeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_programme', 'options', tempOption);
      }
    });

    this.examSessionSub = this.examsListService.examSessionChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('exam_session', 'options', tempOption);
      }
    });

    this.examTypeSub = this.examsListService.examTypeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('exam_type', 'options', tempOption);
      }
    });

    this.educationGradeSub = this.examsListService.educationGradeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_grade', 'options', tempOption);
      }
    });

    this.academicYearSub = this.examsListService.yearChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('academic_period', 'options', tempOption);
        this._updateView.setInputProperty('from_academic_period', 'options', tempOption);
      }
    });

    this.examSub = this.examsListService.examChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('from_examination', 'options', tempOption);
      }
    });

    this.attendanceTypeSub = this.examsListService.attendanceTypeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('attendance_name', 'options', tempOption);
      }
    });
  }

  detectValue(event) {
    if (event['key'] === 'from_academic_period') {
      this.examsListService.getExamOptions(event['value']);
    }
    if (event['key'] === 'education_programme') {
      this.examsListService.getEducationGrade(event['value']);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }
  submitVal(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload: any = {
        from_academic_period: event.from_academic_period,
        from_examination: event.from_examination,
        name: event.exam_name,
        code: event.exam_code,
        academic_period_id: event.academic_period,
        examination_types_id: event.exam_type,
        attendance_type_id: event.attendance_name,
        exam_session_types_id: event.exam_session,
        education_grade_id: event.education_grade,
        registration_start_date: event.registration_start_date.text,
        registration_end_date: event.registration_end_date.text
      };
      this.examsListService.copyExamDetails(payload);
    }
  }

  cancel() {
    this._router.navigate(['main/examinations/exams/list']);
  }

  ngOnDestroy(): void {
    if (this.educationProgrammeSub) {
      this.educationProgrammeSub.unsubscribe();
    }
    if (this.examSessionSub) {
      this.examSessionSub.unsubscribe();
    }
    if (this.examTypeSub) {
      this.examTypeSub.unsubscribe();
    }
    if (this.educationGradeSub) {
      this.educationGradeSub.unsubscribe();
    }
    if (this.academicYearSub) {
      this.academicYearSub.unsubscribe();
    }
    if (this.attendanceTypeSub) {
      this.attendanceTypeSub.unsubscribe();
    }
    if (this.examSub) {
      this.examSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
