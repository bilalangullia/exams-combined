export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true,
    required: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true,
    required: true
  },
  {
    key: 'subject',
    label: 'Subject',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [{ key: null, value: '--select--' }],
    events: true
  },
  {
    key: 'option',
    label: 'Option ',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [{ key: null, value: '--select--' }],
    events: true
  },
  {
    key: 'component_code',
    label: 'Component Code',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'component_name',
    label: 'Component Name',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'component_type',
    label: 'Component Type',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [{ key: null, value: '--select--' }],
    events: true
  },
  {
    key: 'mark_type',
    label: 'Mark Types',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [{ key: null, value: '--select--' }],
    events: true
  },
  {
    key: 'examination_grading_type',
    label: 'Grading Type',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [{ key: null, value: '--select--' }],
    events: true
  },
  {
    key: 'weighting',
    label: 'Weighting (%)',
    visible: true,
    required: false,
    controlType: 'integer',
    type: 'string',
    events: true
  },
  {
    key: 'max_raw_mark',
    label: 'Max Raw Mark',
    visible: true,
    required: true,
    controlType: 'integer',
    type: 'string',
    events: true
  },
  {
    key: 'max_con_mark',
    label: 'Max Converted Mark',
    visible: true,
    required: true,
    controlType: 'integer',
    type: 'string',
    events: true
  },
  {
    key: 'carry_forward',
    label: 'Carry Forward',
    controlType: 'dropdown',
    required: true,
    options: [
      { key: null, value: '-- select --' },
      { key: 0, value: 'No' },
      { key: 1, value: 'Yes' }
    ],
    events: true
  },
  {
    key: 'date',
    label: 'Date',
    visible: true,
    controlType: 'date',
    type: 'date',
    required: true
  },
  {
    key: 'start_time',
    label: 'Time Start',
    visible: true,
    controlType: 'time',
    type: 'time',
    required: true,
    config: {
      disabled: false,
      // hourStep: 2,
      meridian: true,
      // minuteStep: 2,
      readonlyInputs: false
      // seconds: true,
      // secondStep: 2,
      // spinners: false
    }
  },
  {
    key: 'end_time',
    label: 'Time End',
    visible: true,
    controlType: 'time',
    type: 'time',
    required: true,
    config: {
      disabled: false,
      // hourStep: 2,
      meridian: true,
      // minuteStep: 2,
      readonlyInputs: false
      // seconds: true,
      // secondStep: 2,
      // spinners: false
    }
  }
];
export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
