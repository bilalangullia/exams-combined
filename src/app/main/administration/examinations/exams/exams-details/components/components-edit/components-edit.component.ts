import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { ComponentsManagerService } from '../components-manager.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './components-edit.config';
@Component({
  selector: 'app-components-edit',
  templateUrl: './components-edit.component.html',
  styleUrls: ['./components-edit.component.css'],
  providers: [ComponentsManagerService]
})
export class ComponentsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public showForm: boolean = false;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  public componentId: any;
  private academicPeriod: any;
  private examinationId: any;
  private examinationValue: any;

  /* Subscription */
  public componentTypeSub: Subscription;
  public gradingTypeSub: Subscription;
  public markTypeSub: Subscription;
  private filterValuesSub: Subscription;
  private componentEditDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public componentsManagerService: ComponentsManagerService,
    public sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Examinations - Components', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (
        !(
          filters &&
          filters['examinationId'] &&
          filters['examinationValue'] &&
          filters['academicPeriod'] &&
          filters['componentId']
        )
      ) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/examinations/exams/details/components/list']);
        }, 0);
      } else {
        this.academicPeriod = filters['academicPeriod'];
        this.examinationId = filters['examinationId'];
        this.examinationValue = filters['examinationValue'];
        this.componentId = filters['componentId']['id'];
        this.componentsManagerService.editComponentsDetails(filters['componentId']['id']);
      }
    });

    this.componentEditDetailsSub = this.componentsManagerService.componentsEditDetailsChanged.subscribe((details) => {
      if (details) {
        this.setComponentDetails({
          ...details[0],
          academic_period: this.academicPeriod,
          examination: this.examinationValue
        });
      }
    });
    this.setDropDownValue();
  }

  setDropDownValue() {
    timer(100).subscribe((): void => {
      this.componentsManagerService.getComponentType();
      this.componentsManagerService.getMarkType();
      this.componentsManagerService.getGradingTypeOptions();
      this.initializeSubscriptions();
    });
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  setComponentDetails(data: any) {
    if (data) {
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'component_type') {
            this._questionBase[i]['value'] = data[this._questionBase[i]['key']]['key'];
          } else if (this._questionBase[i].key == 'option') {
            this._questionBase[i]['value'] =
              data['option_code'] && data['option_name'] ? `${data['option_code']} - ${data['option_name']}` : '';
          } else if (this._questionBase[i].key == 'subject') {
            this._questionBase[i]['value'] =
              data['subject_code'] && data['subject_name'] ? `${data['subject_code']} - ${data['subject_name']}` : '';
          } else if (this._questionBase[i].key == 'examination_grading_type') {
            this._questionBase[i]['value'] = data[this._questionBase[i]['key']]['key'];
          } else if (this._questionBase[i].key == 'mark_type') {
            this._questionBase[i]['value'] = data[this._questionBase[i]['key']]['key'];
          } else if (this._questionBase[i].key == 'carry_forward') {
            this._questionBase[i]['value'] = data[this._questionBase[i]['key']]['key'];
          } else if (this._questionBase[i].key == 'start_time' || this._questionBase[i].key == 'end_time') {
            this._questionBase[i]['value'] = this.timeStringToObj(data[this._questionBase[i].key]);
          } else if (this._questionBase[i].key == 'max_raw_mark' || this._questionBase[i].key == 'max_con_mark') {
            this._questionBase[i]['value'] = data[this._questionBase[i].key];
          } else {
            this._questionBase[i]['value'] = !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key];
          }
        }
      });
      setTimeout(() => {
        this.showForm = true;
      }, 50);
    }
  }

  initializeSubscriptions() {
    this.componentTypeSub = this.componentsManagerService.componentTypeChanged.subscribe((data: any) => {
      if (data.length) {
        this.setDropdownOptions('component_type', data);
      }
    });

    this.gradingTypeSub = this.componentsManagerService.gradingTypeChanged.subscribe((data: any) => {
      if (data.length) {
        this.setDropdownOptions('examination_grading_type', data);
      }
    });

    this.markTypeSub = this.componentsManagerService.markTypeChanged.subscribe((data: any) => {
      if (data.length) {
        this.setDropdownOptions('mark_type', data);
      }
    });
    this.loading = false;
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      options.unshift({ key: null, value: '--select--' });
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  submit(event) {
    if (event.date['text'] == 'undefined-undefined-undefined') {
      event.date = '';
    }
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let startTimeParts = event.start_time;
      let endTimeParts = event.end_time;
      let dateParts = event.date.obj;

      let startTime: string =
        `${startTimeParts.hour < 10 ? '0' + +startTimeParts.hour : +startTimeParts.hour}` +
        ':' +
        `${startTimeParts.minute < 10 ? '0' + +startTimeParts.minute : +startTimeParts.minute}` +
        ':' +
        `${startTimeParts.second < 10 ? '0' + +startTimeParts.second : +startTimeParts.second}`;
      let endTime: string =
        `${endTimeParts.hour < 10 ? '0' + +endTimeParts.hour : +endTimeParts.hour}` +
        ':' +
        `${endTimeParts.minute < 10 ? '0' + +endTimeParts.minute : +endTimeParts.minute}` +
        ':' +
        `${endTimeParts.second < 10 ? '0' + +endTimeParts.second : +endTimeParts.second}`;
      let date: string =
        `${dateParts.year}` +
        '-' +
        `${dateParts.month < 10 ? '0' + dateParts.month : dateParts.month}` +
        '-' +
        `${dateParts.day < 10 ? '0' + dateParts.day : dateParts.day}`;

      let payload = {
        code: event.component_code,
        name: event.component_name,
        component_types_id: event.component_type,
        carry_forward: event.carry_forward,
        examination_options_id: 1,
        examination_grading_type_id: 1,
        weight: event.weighting,
        start_time: startTime,
        end_time: endTime,
        examination_date: date,
        mark_types_id: event.mark_type,
        max_raw_mark: event.max_raw_mark,
        max_con_mark: event.max_con_mark
      };

      this.componentsManagerService.updateComponentsDetail(this.componentId, payload);
    }
  }

  timeStringToObj(time: string) {
    let timeParts = time.split(':');
    return { hour: timeParts[0], minute: timeParts[1], second: timeParts[2] };
  }

  cancel() {
    this._router.navigate(['main/examinations/exams/details/components/view']);
  }

  ngOnDestroy(): void {
    if (this.componentTypeSub) {
      this.componentTypeSub.unsubscribe();
    }
    if (this.gradingTypeSub) {
      this.gradingTypeSub.unsubscribe();
    }
    if (this.markTypeSub) {
      this.markTypeSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.componentEditDetailsSub) {
      this.componentEditDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
