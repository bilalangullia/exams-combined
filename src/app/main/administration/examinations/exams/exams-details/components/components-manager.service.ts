import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { detailsNotFound, saveFail, updateSuccess, saveSuccess } from '../../../../../../shared/shared.toasters';
import { ExamsDetailsService } from '../exams-details.service';

@Injectable()
export class ComponentsManagerService {
  private subjectOptions: Array<any> = [];
  public subjectOptionsChanged = new BehaviorSubject<Array<any>>([...this.subjectOptions]);
  private optionOptions: Array<any> = [];
  public optionOptionsChanged = new BehaviorSubject<Array<any>>([...this.optionOptions]);
  private componentType: any;
  public componentTypeChanged = new Subject();
  private markType: any;
  public markTypeChanged = new Subject();
  private componentsViewDetails: any = [];
  public componentsViewDetailsChanged = new BehaviorSubject<any>(this.componentsViewDetails);
  private componentsEditDetails: any;
  public componentsEditDetailsChanged = new BehaviorSubject<any>(this.componentsEditDetails);
  private gradingType: any;
  public gradingTypeChanged = new Subject();
  public importComponentDetail: any;

  constructor(
    private router: Router,
    private sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService
  ) {}

  /*** LIST PAGE | List Data ***/
  getList(exam_id, start?: number, end?: number) {
    return this.examsDetailsService.getComponentsList(exam_id, start, end);
  }

  /*** List Data | SEARCH***/
  searchList(exam_id, keyword: string, start?: number, end?: number) {
    return this.examsDetailsService.getComponentsListSearch(exam_id, keyword, start, end);
  }

  /*** EDIT PAGE | Get component  details for populating the view fields. ***/
  editComponentsDetails(component_id) {
    this.examsDetailsService.editComponentsDetails(component_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.componentsEditDetails = res.data;
          this.componentsEditDetailsChanged.next({
            ...this.componentsEditDetails
          });
        } else {
          this.componentsEditDetails = [];
          this.componentsEditDetailsChanged.next({
            ...this.componentsEditDetails
          });
          this.sharedService.setToaster(detailsNotFound);
          this.router.navigate(['main/examinations/exams/details/components/list']);
        }
      },
      (err) => {
        this.componentsEditDetails = [];
        this.componentsEditDetailsChanged.next({
          ...this.componentsEditDetails
        });
        this.sharedService.setToaster(detailsNotFound);
        this.router.navigate(['main/examinations/exams/details/components/list']);
      }
    );
  }

  /*** VIEW PAGE | Get component  details for populating the view fields. ***/
  viewComponentsDetails(component_id) {
    this.examsDetailsService.viewComponentsDetails(component_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.componentsViewDetails = res.data;
          this.componentsViewDetailsChanged.next([...this.componentsViewDetails]);
        } else {
          this.componentsViewDetails = [];
          this.componentsViewDetailsChanged.next([...this.componentsViewDetails]);
          this.sharedService.setToaster(detailsNotFound);
          this.router.navigate(['main/examinations/exams/details/components/list']);
        }
      },
      (err) => {
        this.componentsViewDetails = [];
        this.componentsViewDetailsChanged.next([...this.componentsViewDetails]);
        this.sharedService.setToaster(detailsNotFound);
        this.router.navigate(['main/examinations/exams/details/components/list']);
      }
    );
  }

  /*** EDIT PAGE | COMPONENT TYPE  DROPDOWN ***/
  getComponentType() {
    this.examsDetailsService.getComponentType().subscribe(
      (res: any) => {
        if (res.data.length) {
          this.componentType = res.data;
          this.componentTypeChanged.next([...this.componentType]);
        } else {
          this.componentType = [];
          this.componentTypeChanged.next([...this.componentType]);
        }
      },
      (err) => {
        this.componentType = [];
        this.componentTypeChanged.next([...this.componentType]);
      }
    );
  }

  /*** EDIT PAGE | MARK TYPE  DROPDOWN ***/
  getMarkType() {
    this.examsDetailsService.getMarkType().subscribe(
      (res: any) => {
        if (res.data.length) {
          this.markType = res.data;
          this.markTypeChanged.next([...this.markType]);
        } else {
          this.markType = [];
          this.markTypeChanged.next([...this.markType]);
        }
      },
      (err) => {
        this.markType = [];
        this.markTypeChanged.next([...this.markType]);
      }
    );
  }

  /***  OPTIONS TAB | GRADING TYPE DROPDOWN  API**/
  getGradingTypeOptions() {
    this.examsDetailsService.getGradingType().subscribe(
      (res: any) => {
        if (res.data.length) {
          this.gradingType = res.data;
          this.gradingTypeChanged.next([...this.gradingType]);
        } else {
          this.gradingType = [];
          this.gradingTypeChanged.next([...this.gradingType]);
        }
      },
      (err) => {
        this.gradingType = [];
        this.gradingTypeChanged.next([...this.gradingType]);
      }
    );
  }

  /*** EDIT PAGE | COMPONENTS UPDATE  ***/
  updateComponentsDetail(component_id, data) {
    this.examsDetailsService.updateComponentsDetail(component_id, data).subscribe(
      (res) => {
        if (res) {
          this.sharedService.setToaster(updateSuccess);
          this.router.navigate(['main/examinations/exams/details/components']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** VIEW PAGE | COMPONENTS DELETE  ***/
  deleteComponentDetails(component_id) {
    return this.examsDetailsService.deleteComponentDetails(component_id);
  }

  /*** ADD PAGE | Subjects Dropdown ***/
  getSubjectOptions(examinationId) {
    this.examsDetailsService.getSubjectOptions(examinationId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.subjectOptions = res['data'];
          this.subjectOptionsChanged.next([...this.subjectOptions]);
        } else {
          this.subjectOptions = [];
          this.subjectOptionsChanged.next([]);
        }
      },
      (err) => {
        this.subjectOptions = [];
        this.subjectOptionsChanged.next([]);
      }
    );
  }

  /*** ADD  PAGE | Options Dropdown ***/
  getOptionOptions(subjectId) {
    this.examsDetailsService.getOptionOptions(subjectId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.optionOptions = res['data'];
          this.optionOptionsChanged.next([...this.optionOptions]);
        } else {
          this.optionOptions = [];
          this.optionOptionsChanged.next([]);
        }
      },
      (err) => {
        this.optionOptions = [];
        this.optionOptionsChanged.next([]);
      }
    );
  }

  /*** ADD PAGE | COMPONENTS ADD  ***/
  addComponentsDetail(data) {
    this.examsDetailsService.addComponentsDetail(data).subscribe(
      (res) => {
        if (res) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/examinations/exams/details/components']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  setComponentDetail(data) {
    this.importComponentDetail = data;
  }

  getComponentDetail() {
    return this.importComponentDetail;
  }
}
