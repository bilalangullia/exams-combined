import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../../shared/shared.service';
import { invalidIdError, deleteFail, deleteSuccess, deleteError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService } from '../../exams-details.service';
import { ComponentsManagerService } from '../components-manager.service';
import { VIEWNODE_INPUT, IModalConfig } from './components-view.config';

@Component({
  selector: 'app-components-view',
  templateUrl: './components-view.component.html',
  styleUrls: ['./components-view.component.css'],
  providers: [ComponentsManagerService, KdModalEvent]
})
export class ComponentsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public _questionBase: any = VIEWNODE_INPUT;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public componentId: any;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private componentIdSub: Subscription;
  private componentDetailsSub: Subscription;
  private componentViewDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Component Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          this.examsDetailsService.deleteComponentDetails(this.componentId).subscribe(
            (res: any) => {
              if (res.message) {
                event.preventDefault();
                if (this._modalEvent) {
                  this._modalEvent.toggleClose();
                }
                this.sharedService.setToaster(deleteSuccess);
                this._router.navigate(['main/examinations/exams/details/components/list']);
              }
            },
            (err) => {
              event.preventDefault();
              if (this._modalEvent) {
                this._modalEvent.toggleClose();
              }
              if (err['status'] == 403) {
                this.sharedService.setToaster({ ...deleteFail, body: err['error']['message'] });
              } else {
                this.sharedService.setToaster(deleteError);
              }
            }
          );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          if (this._modalEvent) {
            this._modalEvent.toggleClose();
          }
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _modalEvent: KdModalEvent,
    private sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService,
    public componentsManagerService: ComponentsManagerService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Examinations - Components', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/examinations/exams/details/components/list' },
      { type: 'edit', path: 'main/examinations/exams/details/components/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters) => {
      if (!(filters && filters['examinationId'] && filters['academicPeriod'] && filters['componentId'])) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/examinations/exams/details/components/list']);
        }, 0);
      } else {
        this.componentViewDetailsSub = this.examsDetailsService
          .viewComponentsDetails(filters['componentId']['id'])
          .subscribe((details) => {
            if (details['data']) {
              this.setComponentDetails({
                ...details['data'][0],
                academic_period: filters['academicPeriod'],
                examination: filters['examinationValue']
              });
            }
          });
      }
    });

    this.componentIdSub = this.examsDetailsService.componentIdChanged.subscribe((data) => {
      this.componentId = data;
      if (!this.componentId) {
        this.sharedService.setToaster(invalidIdError);
        this._router.navigate(['main/examinations/exams/details/components/list']);
      } else {
        this.componentsManagerService.viewComponentsDetails(data);
      }
    });
  }

  setComponentDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'option') {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            data['option_code'] && data['option_name'] ? `${data['option_code']} - ${data['option_name']}` : ''
          );
        } else if (this._questionBase[i].key == 'max_raw_mark' || this._questionBase[i].key == 'max_con_mark') {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key]);
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
    this.loading = false;
  }

  open() {
    if (this._modalEvent) {
      this._modalEvent.toggleOpen();
    }
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.componentIdSub) {
      this.componentIdSub.unsubscribe();
    }
    if (this.componentDetailsSub) {
      this.componentDetailsSub.unsubscribe();
    }
    if (this.componentViewDetailsSub) {
      this.componentViewDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
