export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'subject_name',
    label: 'Subject',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'option',
    label: 'Option',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'component_code',
    label: 'Component Code',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'component_name',
    label: 'Component Name',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'component_type',
    label: 'Component Type',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'mark_type',
    label: 'Mark Types',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'examination_grading_type',
    label: 'Grading Type',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'weighting',
    label: 'Weighting (%)',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'max_raw_mark',
    label: 'Max Raw Mark',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'max_con_mark',
    label: 'Max Converted Mark',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'carry_forward',
    label: 'Carry Forward',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'date',
    label: 'Date',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'start_time',
    label: 'Time Start',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'end_time',
    label: 'Time End',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'modified_user_id',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'modified',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'created_user_id',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'created',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    type: 'string'
  }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
