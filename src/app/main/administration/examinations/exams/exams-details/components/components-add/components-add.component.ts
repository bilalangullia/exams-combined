import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { Subscription, timer } from 'rxjs';

import { VIEWNODE_INPUT, FORM_BUTTONS } from './components-add.config';
import { SharedService } from '../../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../../shared/shared.toasters';
import { ComponentsManagerService } from '../components-manager.service';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
@Component({
  selector: 'app-components-add',
  templateUrl: './components-add.component.html',
  styleUrls: ['./components-add.component.css'],
  providers: [ComponentsManagerService]
})
export class ComponentsAddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  private academicPeriod: any;
  private examinationValue: any;
  private examinationId: any;

  /* Subscription */
  private componentTypeSub: Subscription;
  private gradingTypeSub: Subscription;
  private subjectsSub: Subscription;
  private optionsSub: Subscription;
  private filterValuesSub: Subscription;
  private componentEditDetailsSub: Subscription;
  public markTypeSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public componentsManagerService: ComponentsManagerService,
    public sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Examinations - Components', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (!(filters && filters['examinationId'] && filters['examinationValue'] && filters['academicPeriod'])) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/examinations/exams/details/components/list']);
        }, 0);
      } else {
        this.academicPeriod = filters['academicPeriod'];
        this.examinationValue = filters['examinationValue'];
        this.examinationId = filters['examinationId'];
        this.setComponentDetails();
      }
    });

    timer(100).subscribe((): void => {
      this.componentsManagerService.getComponentType();
      this.componentsManagerService.getGradingTypeOptions();
      this.componentsManagerService.getSubjectOptions(this.examinationId);
      this.componentsManagerService.getMarkType();
      this.initializeSubscriptions();
    });
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  setComponentDetails() {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'academic_period') {
          this.api.setProperty(this._questionBase[i].key, 'value', this.academicPeriod);
        } else if (this._questionBase[i].key == 'examination') {
          this.api.setProperty(this._questionBase[i].key, 'value', this.examinationValue);
        }
      }
    });
  }

  initializeSubscriptions() {
    this.componentTypeSub = this.componentsManagerService.componentTypeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('component_type', 'options', tempOption);
      }
    });

    this.gradingTypeSub = this.componentsManagerService.gradingTypeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('examination_grading_type', 'options', tempOption);
      }
    });

    this.subjectsSub = this.componentsManagerService.subjectOptionsChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('subject', 'options', tempOption);
      }
    });

    this.optionsSub = this.componentsManagerService.optionOptionsChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('option', 'options', tempOption);
      }
    });
    this.markTypeSub = this.componentsManagerService.markTypeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('mark_type', 'options', tempOption);
      }
    });
    this.loading = false;
  }

  submit(event) {
    if (event.date['text'] == 'undefined-undefined-undefined') {
      event.date = '';
    }
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let startTimeParts = event.start_time;
      let endTimeParts = event.end_time;
      let dateParts = event.date.obj;
      let startTime: string =
        `${startTimeParts.hour < 10 ? '0' + +startTimeParts.hour : +startTimeParts.hour}` +
        ':' +
        `${startTimeParts.minute < 10 ? '0' + +startTimeParts.minute : +startTimeParts.minute}` +
        ':' +
        `${startTimeParts.second < 10 ? '0' + +startTimeParts.second : +startTimeParts.second}`;
      let endTime: string =
        `${endTimeParts.hour < 10 ? '0' + +endTimeParts.hour : +endTimeParts.hour}` +
        ':' +
        `${endTimeParts.minute < 10 ? '0' + +endTimeParts.minute : +endTimeParts.minute}` +
        ':' +
        `${endTimeParts.second < 10 ? '0' + +endTimeParts.second : +endTimeParts.second}`;
      let date: string =
        `${dateParts.year}` +
        '-' +
        `${dateParts.month < 10 ? '0' + dateParts.month : dateParts.month}` +
        '-' +
        `${dateParts.day < 10 ? '0' + dateParts.day : dateParts.day}`;

      let payload = {
        code: event.component_code,
        name: event.component_name,
        component_types_id: event.component_type,
        carry_forward: event.carry_forward,
        examination_options_id: event.option,
        examination_grading_type_id: event.examination_grading_type,
        weight: event.weighting,
        start_time: startTime,
        end_time: endTime,
        examination_date: date,
        mark_types_id: event.mark_type,
        max_raw_mark: event.max_raw_mark,
        max_con_mark: event.max_con_mark
      };

      this.componentsManagerService.addComponentsDetail(payload);
    }
  }

  detectValue(event) {
    if (event['key'] == 'subject' && (event['value'] || event['value'] != 'null')) {
      this.componentsManagerService.getOptionOptions(event['value']);
      setTimeout(() => {
        this.api.setProperty('option', 'value', null);
      }, 0);
    }
  }

  timeStringToObj(time: string) {
    let timeParts = time.split(':');
    return { hour: timeParts[0], minute: timeParts[1], second: timeParts[2] };
  }

  cancel() {
    this._router.navigate(['main/examinations/exams/details/components/list']);
  }

  ngOnDestroy(): void {
    if (this.componentTypeSub) {
      this.componentTypeSub.unsubscribe();
    }
    if (this.gradingTypeSub) {
      this.gradingTypeSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.componentEditDetailsSub) {
      this.componentEditDetailsSub.unsubscribe();
    }
    if (this.subjectsSub) {
      this.subjectsSub.unsubscribe();
    }
    if (this.optionsSub) {
      this.optionsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
