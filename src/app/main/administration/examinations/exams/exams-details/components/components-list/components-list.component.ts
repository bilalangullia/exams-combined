import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { ExcelExportParams } from 'ag-grid';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  ITableDatasourceParams,
  KdTableDatasourceEvent
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../../../shared/shared.interfaces';
import { DataService } from '../../../../../../../shared/data.service';
import { SharedService } from '../../../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService } from '../../exams-details.service';
import { ComponentsManagerService } from '../components-manager.service';
import { TABLECOLUMN } from './components-list.config';

@Component({
  selector: 'app-components-list',
  templateUrl: './components-list.component.html',
  styleUrls: ['./components-list.component.css'],
  providers: [ComponentsManagerService, KdTableEvent]
})
export class ComponentsListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'areaLevelEducationList';
  readonly PAGESIZE: number = 20;
  private tableEventList: any;

  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public examinationId: any;
  public showTable: boolean = false;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  private tableEventSub: Subscription;
  private examinationIdSub: Subscription;

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.examsDetailsService.setComponentId(_rowNode['data']['id'], _rowNode['data']['component']);
            this.router.navigate(['main/examinations/exams/details/components/view']);
          }
        }
      ]
    }
  };

  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.Option, TABLECOLUMN.ComponentCode, TABLECOLUMN.ComponentName];
  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    public componentsManagerService: ComponentsManagerService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
    super.setPageTitle('Examinations - Components', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/examinations/exams/details/components/add' },
      { type: 'import', path: 'main/examinations/exams/details/components/import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'components_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.examinationIdSub = this.examsDetailsService.examinationIdChanged.subscribe((id) => {
      if (!id) {
        this.sharedService.setToaster(invalidIdError);
      } else {
        this.examinationId = id;
        this.showTable = false;
        setTimeout(() => {
          this.showTable = true;
        }, 0);
        this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
          if (event instanceof KdTableDatasourceEvent) {
            this.tableEventList = event;
            setTimeout(() => {
              this.fetchList(this.tableEventList, this.searchKey);
            }, 0);
          }
        });
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      if (this.examinationId) {
        listReq = this.componentsManagerService.searchList(
          this.examinationId,
          searchKey,
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    } else {
      if (this.examinationId) {
        listReq = this.componentsManagerService.getList(
          this.examinationId,
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['userRecords'] && res['data']['userRecords'].length) {
          list = res['data']['userRecords'];
          total = this.searchKey && this.searchKey.length ? res['data']['userRecords'].length : res['data']['total'];
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  ngOnDestroy(): void {
    if (this.examinationIdSub) {
      this.examinationIdSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
