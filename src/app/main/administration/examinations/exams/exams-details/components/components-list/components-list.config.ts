interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Option?: Column;
  ComponentCode?: Column;
  ComponentName?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Option: {
    headerName: 'Option',
    field: 'option',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ComponentCode: {
    headerName: 'Component Code',
    field: 'component_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ComponentName: {
    headerName: 'Component Name',
    field: 'component_name',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
