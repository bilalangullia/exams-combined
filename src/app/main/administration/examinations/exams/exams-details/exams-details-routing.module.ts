import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntryComponent } from './entry/entry.component';
import { ExamsViewComponent } from './exams/exams-view/exams-view.component';
import { ExamsEditComponent } from './exams/exams-edit/exams-edit.component';
import { SubjectsListComponent } from './subjects/subjects-list/subjects-list.component';
import { SubjectsViewComponent } from './subjects/subjects-view/subjects-view.component';
import { SubjectsEditComponent } from './subjects/subjects-edit/subjects-edit.component';
import { SubjectsAddComponent } from './subjects/subjects-add/subjects-add.component';
import { ImportFormComponent as SubjectsImportForm } from './subjects/import-form/import-form.component';
import { ImportListComponent as SubjectsImportList } from './subjects/import-list/import-list.component';
import { OptionsListComponent } from './options/options-list/options-list.component';
import { OptionsViewComponent } from './options/options-view/options-view.component';
import { OptionsEditComponent } from './options/options-edit/options-edit.component';
import { OptionsAddComponent } from './options/options-add/options-add.component';
import { ImportFormComponent as OptionsImportForm } from './options/import-form/import-form.component';
import { ImportListComponent as OptionsImportList } from './options/import-list/import-list.component';
import { ComponentsListComponent } from './components/components-list/components-list.component';
import { ComponentsViewComponent } from './components/components-view/components-view.component';
import { ComponentsEditComponent } from './components/components-edit/components-edit.component';
import { ComponentsAddComponent } from './components/components-add/components-add.component';
import { ImportFormComponent as ComponentsImportForm } from './components/import-form/import-form.component';
import { ImportListComponent as ComponentsImportList } from './components/import-list/import-list.component';
import { ItemsListComponent } from './items/items-list/items-list.component';
import { ItemsViewComponent } from './items/items-view/items-view.component';
import { ItemsEditComponent } from './items/items-edit/items-edit.component';
import { ItemsAddComponent } from './items/items-add/items-add.component';
import { ImportFormComponent as ItemsImportForm } from './items/import-form/import-form.component';
import { ImportListComponent as ItemsImportList } from './items/import-list/import-list.component';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
    children: [
      // { path: "", redirectTo: "view" },
      { path: 'view', component: ExamsViewComponent },
      { path: 'edit', component: ExamsEditComponent },
      {
        path: 'subjects',
        children: [
          { path: '', redirectTo: 'list' },
          { path: 'list', component: SubjectsListComponent },
          { path: 'view', component: SubjectsViewComponent },
          { path: 'edit', component: SubjectsEditComponent },
          { path: 'add', component: SubjectsAddComponent },
          {
            path: 'import',
            children: [
              { path: '', redirectTo: 'form', pathMatch: 'full' },
              { path: 'form', component: SubjectsImportForm },
              { path: 'list', component: SubjectsImportList }
            ]
          }
        ]
      },
      {
        path: 'options',
        children: [
          { path: '', redirectTo: 'list' },
          { path: 'list', component: OptionsListComponent },
          { path: 'view', component: OptionsViewComponent },
          { path: 'edit', component: OptionsEditComponent },
          { path: 'add', component: OptionsAddComponent },
          {
            path: 'import',
            children: [
              { path: '', redirectTo: 'form', pathMatch: 'full' },
              { path: 'form', component: OptionsImportForm },
              { path: 'list', component: OptionsImportList }
            ]
          }
        ]
      },
      {
        path: 'components',
        children: [
          { path: '', redirectTo: 'list' },
          { path: 'list', component: ComponentsListComponent },
          { path: 'view', component: ComponentsViewComponent },
          { path: 'edit', component: ComponentsEditComponent },
          { path: 'add', component: ComponentsAddComponent },
          {
            path: 'import',
            children: [
              { path: '', redirectTo: 'form', pathMatch: 'full' },
              { path: 'form', component: ComponentsImportForm },
              { path: 'list', component: ComponentsImportList }
            ]
          }
        ]
      },
      {
        path: 'items',
        children: [
          { path: '', redirectTo: 'list' },
          { path: 'list', component: ItemsListComponent },
          { path: 'view', component: ItemsViewComponent },
          { path: 'edit', component: ItemsEditComponent },
          { path: 'add', component: ItemsAddComponent },
          {
            path: 'import',
            children: [
              { path: '', redirectTo: 'form', pathMatch: 'full' },
              { path: 'form', component: ItemsImportForm },
              { path: 'list', component: ItemsImportList }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class ExamsDetailsRoutingModule {}
