import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { detailsNotFound, saveFail, updateSuccess } from '../../../../../../shared/shared.toasters';
import { ExamsService } from '../../exams.service';
import { ExamsDetailsService } from '../exams-details.service';

@Injectable()
export class ExamsManagerService {
  private educationProgramme: any;
  public educationProgrammeChanged = new Subject();
  private educationGrade: any;
  public educationGradeChanged = new Subject();
  private examType: any;
  public examTypeChanged = new Subject();
  private examSession: any;
  public examSessionChanged = new Subject();
  private examViewDetails: any = [];
  public examViewDetailsChanged = new Subject<any>();

  constructor(
    private router: Router,
    private sharedService: SharedService,
    private examsService: ExamsService,
    private examsDetailsService: ExamsDetailsService
  ) {}

  /*** VIEW/EDIT PAGE | Get exam  details for populating the view fields. ***/
  getExaminationDetails(id) {
    this.examsDetailsService.getExaminationDetails(id).subscribe(
      (res: any) => {
        if (res.data) {
          this.examViewDetails = res.data;
          this.examViewDetailsChanged.next([...this.examViewDetails]);
        } else {
          this.examViewDetails = [];
          this.examViewDetailsChanged.next([...this.examViewDetails]);
          this.sharedService.setToaster(detailsNotFound);
          this.router.navigate(['main/examinations/exams/list']);
        }
      },
      (err) => {
        this.examViewDetails = [];
        this.examViewDetailsChanged.next([...this.examViewDetails]);
        this.sharedService.setToaster(detailsNotFound);
        this.router.navigate(['main/examinations/exams/list']);
      }
    );
  }

  /*** EDIT PAGE | EDUCATION PROGRAMME DROPDOWN ***/
  getEducationProgramme() {
    this.examsService.getEducationProgramme().subscribe(
      (res: any) => {
        if (res.data.length) {
          this.educationProgramme = res.data;
          this.educationProgrammeChanged.next([...this.educationProgramme]);
        } else {
          this.educationProgramme = [];
          this.educationProgrammeChanged.next([...this.educationProgramme]);
        }
      },
      (err) => {
        this.educationProgramme = [];
        this.educationProgrammeChanged.next([...this.educationProgramme]);
      }
    );
  }

  /*** EDIT PAGE | EDUCATION GRADE DROPDOWN ***/
  getEducationGrade(education_programme_id) {
    this.examsService.getEducationGrade(education_programme_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.educationGrade = res.data;
          this.educationGradeChanged.next([...this.educationGrade]);
        } else {
          this.educationGrade = [];
          this.educationGradeChanged.next([...this.educationGrade]);
        }
      },
      (err) => {
        this.educationGrade = [];
        this.educationGradeChanged.next([...this.educationGrade]);
      }
    );
  }

  /*** EDIT PAGE | EXAM TYPE DROPDOWN ***/
  getExamType() {
    this.examsService.getExamType().subscribe(
      (res: any) => {
        if (res.data.length) {
          this.examType = res.data;
          this.examTypeChanged.next([...this.examType]);
        } else {
          this.examType = [];
          this.examTypeChanged.next([...this.examType]);
        }
      },
      (err) => {
        this.examType = [];
        this.examTypeChanged.next([...this.examType]);
      }
    );
  }

  /*** EDIT PAGE | EXAM SESSION DROPDOWN ***/
  getExamSession() {
    this.examsService.getExamSession().subscribe(
      (res: any) => {
        if (res.data.length) {
          this.examSession = res.data;
          this.examSessionChanged.next([...this.examSession]);
        } else {
          this.examSession = [];
          this.examSessionChanged.next([...this.examSession]);
        }
      },
      (err) => {
        this.examSession = [];
        this.examSessionChanged.next([...this.examSession]);
      }
    );
  }

  /*** EDIT PAGE | EXAM UPDATE  ***/
  updateExamDetail(exam_id, data) {
    return this.examsService.updateExamDetail(exam_id, data).subscribe(
      (data: any) => {
        if (data) {
          this.sharedService.setToaster(updateSuccess);
          this.router.navigate(['main/examinations/exams/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteExamDetails(exam_id) {
    return this.examsService.deleteExamDetails(exam_id);
  }
}
