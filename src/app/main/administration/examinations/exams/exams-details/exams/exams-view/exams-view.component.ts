import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModal, KdModalEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../../shared/shared.service';
import { invalidIdError, deleteFail, deleteSuccess, deleteError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { ExamsManagerService } from '../exams-manager.service';
import { VIEWNODE_INPUT, IModalConfig } from './exams-view.config';

@Component({
  selector: 'app-exams-view',
  templateUrl: './exams-view.component.html',
  styleUrls: ['./exams-view.component.css'],
  providers: [ExamsManagerService, KdModalEvent]
})
export class ExamsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public _questionBase: any = VIEWNODE_INPUT;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public examId: any;

  /* Subscriptions */
  private examinationIdSub: Subscription;
  private examinationDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Exams Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          this.examsManagerService.deleteExamDetails(this.examId).subscribe(
            (res: any) => {
              if (res.message) {
                event.preventDefault();
                if (this._modalEvent) {
                  this._modalEvent.toggleClose();
                }
                this.sharedService.setToaster(deleteSuccess);
                this._router.navigate(['main/examinations/exams/list']);
              }
            },
            (err) => {
              event.preventDefault();
              if (this._modalEvent) {
                this._modalEvent.toggleClose();
              }
              if (err['status'] == 403) {
                this.sharedService.setToaster({ ...deleteFail, body: err['error']['message'] });
              } else {
                this.sharedService.setToaster(deleteError);
              }
            }
          );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          if (this._modalEvent) {
            this._modalEvent.toggleClose();
          }
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _modalEvent: KdModalEvent,
    private sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService,
    private examsManagerService: ExamsManagerService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });

    super.setPageTitle('Examinations - Exams', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/examinations/exams/list' },
      { type: 'edit', path: 'main/examinations/exams/details/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.examinationIdSub = this.examsDetailsService.filterValuesChanged.subscribe((data: IFilter) => {
      if (!(data.examinationId && data.examinationValue && data.academicPeriod)) {
        this.sharedService.setToaster(invalidIdError);
        this._router.navigate(['main/examinations/exams/list']);
      } else {
        this.examId = data.examinationId;
        this.examsManagerService.getExaminationDetails(this.examId);
      }
    });

    this.examinationDetailsSub = this.examsManagerService.examViewDetailsChanged.subscribe((details) => {
      if (details.length) {
        this.setExamDetails(details[0]);
      } else {
        this.loading = false;
      }
    });
  }

  setExamDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'attendance_name') {
          data[this._questionBase[i].key]['key']
            ? this.api.setProperty('attendance_name', 'value', data[this._questionBase[i].key]['value'])
            : this.api.setProperty('attendance_name', 'value', '');
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
      this.loading = false;
    });
  }

  open() {
    if (this._modalEvent) {
      this._modalEvent.toggleOpen();
    }
  }

  ngOnDestroy(): void {
    if (this.examinationIdSub) {
      this.examinationIdSub.unsubscribe();
    }
    if (this.examinationDetailsSub) {
      this.examinationDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
