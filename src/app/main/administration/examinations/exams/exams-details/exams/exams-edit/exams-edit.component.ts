import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, KdView, IDynamicFormApi } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../../shared/shared.service';
import { invalidIdError, missingFieldsError } from '../../../../../../../shared/shared.toasters';
import { ExamsService } from '../../../exams.service';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { ExamsManagerService } from '../exams-manager.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './exams-edit.config';

@Component({
  selector: 'app-exams-edit',
  templateUrl: './exams-edit.component.html',
  styleUrls: ['./exams-edit.component.css'],
  providers: [ExamsManagerService]
})
export class ExamsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  private pageTitle: string = 'Examinations - Exams';
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public examinationId: any;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private examinationDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    public examsService: ExamsService,
    public examsManagerService: ExamsManagerService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (!(filters && filters['examinationId'] && filters['examinationValue'] && filters['academicPeriod'])) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/examinations/exams/list']);
        }, 0);
      } else {
        this.examinationId = filters['examinationId'];
        this.examsManagerService.getExaminationDetails(this.examinationId);
      }
    });

    this.examinationDetailsSub = this.examsManagerService.examViewDetailsChanged.subscribe((data) => {
      if (data.length) {
        this.setExamDetails(data[0]);
      } else {
        this.loading = false;
      }
    });
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  submitVal(formVal) {
    if (formVal.registration_start_date['text'] == 'undefined-undefined-undefined') {
      formVal.registration_start_date = '';
    }
    if (formVal.registration_end_date['text'] == 'undefined-undefined-undefined') {
      formVal.registration_end_date = '';
    }
    if (
      formVal.registration_start_date['text'] == 'undefined-undefined-undefined' &&
      formVal.registration_end_date['text'] == 'undefined-undefined-undefined'
    ) {
    }
    if (this.requiredCheck(formVal)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload: any = {
        registration_start_date: formVal.registration_start_date.text,
        registration_end_date: formVal.registration_end_date.text
      };
      this.examsManagerService.updateExamDetail(this.examinationId, payload);
    }
  }

  setExamDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'attendance_name') {
          data[this._questionBase[i].key]['key']
            ? this.api.setProperty('attendance_name', 'value', data[this._questionBase[i].key]['value'])
            : this.api.setProperty('attendance_name', 'value', '');
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
      this.loading = false;
    });
  }

  cancel() {
    this._router.navigate(['main/examinations/exams/details/view']);
  }

  ngOnDestroy(): void {
    if (this.examinationDetailsSub) {
      this.examinationDetailsSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
