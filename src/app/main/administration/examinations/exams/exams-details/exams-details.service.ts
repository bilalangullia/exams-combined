import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { DataService } from '../../../../../shared/data.service';
import { ExamsService } from '../exams.service';
import { ExamsDetailsDataService } from './exams-details-data.service';
import { IFilterItem } from '../../../../../shared/shared.interfaces';

export interface IFilter {
  academicPeriod?: string;
  /* Adding a temp academicPeriod to hold key-values */
  academicPeriodData?: IFilterItem;
  examinationId?: number;
  examinationValue?: string;
  /* Adding a temp examination to hold key-values */
  examination?: IFilterItem;
  subjectId?: { id: number; value: string };
  optionId?: { id: number; value: string };
  componentId?: { id: number; value: string };
}

@Injectable()
export class ExamsDetailsService {
  private academicPeriod: string = '';
  /* Adding a temp academicPeriod to hold key-values */
  private academicPeriodData: IFilterItem = null;
  private examinationId: number;
  /* Adding a temp examination to hold key-values */
  private examination: IFilterItem;
  private subjectId: number;
  private optionId: number;
  private componentId: number;
  private itemId: number;
  private examinationValue: string;
  private filterValues: IFilter = null;

  public examinationIdChanged = new BehaviorSubject<number>(this.examinationId);
  public examinationValueChanged = new BehaviorSubject<string>(this.examinationValue);
  /* Adding a temp examination to hold key-values */
  public examinationChanged = new BehaviorSubject<IFilterItem>({ ...this.examination });
  /* Adding a temp examination to hold key-values */
  public academicPeriodDataChanged = new BehaviorSubject<IFilterItem>({ ...this.academicPeriodData });
  public subjectIdChanged = new BehaviorSubject<number>(this.subjectId);
  public optionIdChanged = new BehaviorSubject<number>(this.optionId);
  public componentIdChanged = new BehaviorSubject<number>(this.componentId);
  public itemIdChanged = new BehaviorSubject<number>(this.itemId);
  public filterValuesChanged = new BehaviorSubject<object>({ ...this.filterValues });

  constructor(
    private examsService: ExamsService,
    private dataService: DataService,
    private examsDetailsDataService: ExamsDetailsDataService
  ) {}

  getExaminationId() {
    this.examsService.examinationIdChanged.subscribe((data) => {
      this.examinationId = data;
      this.examinationIdChanged.next(this.examinationId);
      this.setFilterValues({ examinationId: this.examinationId });
    });
  }

  setExaminationValue() {
    this.examsService.examinationValueChanged.subscribe((data) => {
      this.examinationValue = data;
      this.examinationValueChanged.next(this.examinationValue);
      this.setFilterValues({ examinationValue: this.examinationValue });
    });
  }

  /* Adding a temp examination to hold key-values */
  getExamination() {
    this.examsService.examinationChanged.subscribe((examination: IFilterItem) => {
      this.examination = { ...examination };
      this.examinationChanged.next({ ...this.examination });
      this.setFilterValues({ examination: { ...this.examination } });
    });
  }

  getAcademicPeriod() {
    this.examsService.academicPeriodChanged.subscribe((data) => {
      this.academicPeriod = data;
      this.setFilterValues({ academicPeriod: this.academicPeriod });
    });
  }

  /* Adding a temp academicPeriod to hold key-values */
  getAcademicPeriodData() {
    this.examsService.academicPeriodDataChanged.subscribe((data: any) => {
      this.academicPeriodData = data;
      this.academicPeriodDataChanged.next({ ...this.academicPeriodData });
      this.setFilterValues({ academicPeriodData: { ...this.academicPeriodData } });
    });
  }

  setSubjectId(id: number, value: string) {
    this.subjectId = id;
    this.subjectIdChanged.next(this.subjectId);
    this.setFilterValues({ subjectId: { id: this.subjectId, value } });
  }

  setOptionId(id: number, value: string) {
    this.optionId = id;
    this.optionIdChanged.next(this.optionId);
    this.setFilterValues({ optionId: { id: this.optionId, value } });
  }

  setComponentId(id: number, value: string) {
    this.componentId = id;
    this.componentIdChanged.next(this.componentId);
    this.setFilterValues({ componentId: { id: this.componentId, value } });
  }

  setItemId(id: number) {
    this.itemId = id;
    this.itemIdChanged.next(this.itemId);
  }

  setFilterValues(filterObj?: IFilter) {
    this.filterValues = { ...this.filterValues, ...filterObj };
    this.filterValuesChanged.next({ ...this.filterValues });
  }

  resetData() {
    this.examinationId = null;
    this.examinationIdChanged.next(this.examinationId);
    this.subjectId = null;
    this.subjectIdChanged.next(this.subjectId);
    this.optionId = null;
    this.optionIdChanged.next(this.optionId);
    this.componentId = null;
    this.componentIdChanged.next(this.componentId);
    this.itemId = null;
    this.itemIdChanged.next(this.itemId);
    this.filterValues = {};
    this.filterValuesChanged.next(this.filterValues);
  }

  /**
   * NOTE: All api calls using dataService should be moved to this file for modularity!!!
   **/

  getExaminationDetails(id) {
    return this.examsDetailsDataService.getExamsView(id);
  }

  addSubject(payload: any) {
    return this.examsDetailsDataService.addSubject(payload);
  }

  getSubjectsList(examinationId: string, start: number, end: number) {
    return this.examsDetailsDataService.getSubjectsList(examinationId, start, end);
  }

  getSubjectsListSearch(examinationId: string, keyword: string, start: number, end: number) {
    return this.examsDetailsDataService.getSubjectsListSearch(examinationId, keyword, start, end);
  }

  getSubjectsViewDetails(subject_id, examination_id) {
    return this.examsDetailsDataService.viewSubject(subject_id, examination_id);
  }

  getSubjectEditDetails(subject_id, examination_id) {
    return this.examsDetailsDataService.editSubject(subject_id, examination_id);
  }

  getEducationSubjectDropdown() {
    return this.examsDetailsDataService.getEducationSubjectDropdown();
  }

  updateSubjectDetail(subjectId, data) {
    return this.examsDetailsDataService.updateSubjectDetail(subjectId, data);
  }

  deleteSubject(subjectId: number) {
    return this.examsDetailsDataService.deleteSubject(subjectId);
  }

  addOption(payload: any) {
    return this.examsDetailsDataService.addOption(payload);
  }

  /*** OPTIONS LIST  PAGE | List Data ***/
  getOptionsList(exam_id, start: number, end: number) {
    return this.examsDetailsDataService.getOptionsList(exam_id, start, end);
  }

  /***  OPTIONS TAB | ATTENDANCE TYPE DROPDOWN  API**/
  getAttendanceType(attendanceTypes) {
    return this.examsDetailsDataService.getAttendanceTypeDetails(attendanceTypes);
  }

  /***  OPTIONS TAB | GRADING TYPE DROPDOWN  API**/
  getGradingType() {
    return this.examsDetailsDataService.getGradingTypeOptions();
  }

  getSubjectOptions(examinationId: number) {
    return this.examsDetailsDataService.getSubjectOptions(examinationId);
  }

  /*** OPTIONS VIEW | EDIT |  VIEW | API***/
  getOptionsDetails(option_id) {
    return this.examsDetailsDataService.getExaminationsOptionsDetails(option_id);
  }

  /*** OPTIONS EDIT  PAGE | SUBMIT API ***/
  updateOptionsDetail(option_id, data) {
    return this.examsDetailsDataService.updateOptionsDetail(option_id, data);
  }

  /*** OPTIONS EDIT  PAGE | SEARCH***/
  getOptionsListSearch(examination_id, keyword, start: number, end: number) {
    return this.examsDetailsDataService.getOptionsSearch(examination_id, keyword, start, end);
  }

  deleteOption(optionId) {
    return this.examsDetailsDataService.deleteOption(optionId);
  }

  /*** OPTIONS TAB APIs END ***/

  /*** COMPONENTS TAB APIs START ***/

  /*** COMPONENTS LIST  PAGE | List Data ***/
  getComponentsList(exam_id: number, start: number, end: number) {
    return this.examsDetailsDataService.getComponentsList(exam_id, start, end);
  }

  getComponentsListSearch(exam_id: number, keyword: string, start: number, end: number) {
    return this.examsDetailsDataService.getComponentsListSearch(exam_id, keyword, start, end);
  }
  /*** COMPONENTS EDIT | EDIT API***/
  editComponentsDetails(component_id) {
    return this.examsDetailsDataService.editComponentsDetails(component_id);
  }

  /*** COMPONENTS VIEW | VIEW API***/
  viewComponentsDetails(component_id) {
    return this.examsDetailsDataService.viewComponentsDetails(component_id);
  }

  /*** COMPONENTS EDIT  | COMPONENT TYPE DROPDOWN API***/
  getComponentType() {
    return this.examsDetailsDataService.getComponentTypeDropDown();
  }

  /*** COMPONENTS EDIT  | MARK TYPE DROPDOWN API***/
  getMarkType() {
    return this.examsDetailsDataService.getMarkTypeDropDown();
  }

  /*** COMPONENTS EDIT  PAGE | SUBMIT API ***/
  updateComponentsDetail(component_id, data) {
    return this.examsDetailsDataService.updateComponentsDetail(component_id, data);
  }

  /*** COMPONENTS VIEW PAGE | DELETE API ***/
  deleteComponentDetails(component_id) {
    return this.examsDetailsDataService.deleteComponentDetails(component_id);
  }

  /*** COMPONENTS ADD  | SUBJECTS DROPDOWN API***/
  getSubjectsDropdown() {
    return this.examsDetailsDataService.getSubjectsDropDown();
  }

  /*** COMPONENTS ADD  | OPTIONS DROPDOWN API***/
  getOptionsDropdown() {
    return this.examsDetailsDataService.getOptionsDropDown();
  }

  /*** COMPONENTS ADD  PAGE | SUBMIT API ***/
  addComponentsDetail(data) {
    return this.examsDetailsDataService.addComponentsDetail(data);
  }
  /*** COMPONENTS TAB APIs END   ***/
  /*   ***************************************************************************************************** */

  /*** ITEMS TAB APIs START ***/

  addItem(payload: any) {
    return this.examsDetailsDataService.addItem(payload);
  }

  getOptionOptions(subjectId) {
    return this.examsDetailsDataService.getOptionOptions(subjectId);
  }

  getComponentOptions(optionId) {
    return this.examsDetailsDataService.getComponentOptions(optionId);
  }

  getItemOptions() {
    return this.examsDetailsDataService.getItemOptions();
  }

  /*** ITEMS LIST  PAGE | List Data ***/
  getItemsList(exam_id, start, end) {
    return this.examsDetailsDataService.getItemsList(exam_id, start, end);
  }

  /*** ITEMS VIEW | EDIT |  VIEW | API***/
  getItemsDetail(item_id) {
    return this.examsDetailsDataService.getItemsDetails(item_id);
  }

  /*** ITEMS EDIT  PAGE | SUBMIT API ***/
  updateItemsDetail(item_id, data) {
    return this.examsDetailsDataService.updateItemsDetail(item_id, data);
  }

  /***  ITEMS  PAGE | SEARCH***/
  getItemsListSearch(examination_id, keyword, start, end) {
    return this.examsDetailsDataService.getItemsSearch(examination_id, keyword, start, end);
  }

  getItemType() {
    return this.examsDetailsDataService.getItemDropDown();
  }

  deleteItem(itemId) {
    return this.examsDetailsDataService.deleteItem(itemId);
  }
  /*** ITEMS TAB APIs END ***/
}
