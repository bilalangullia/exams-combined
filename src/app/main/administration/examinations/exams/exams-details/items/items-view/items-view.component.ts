import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { invalidIdError, deleteSuccess, deleteFail, serverError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { ItemsManagerService } from '../items-manager.service';
import { VIEWNODE_INPUT, IModalConfig } from './items-view.config';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-items-view',
  templateUrl: './items-view.component.html',
  styleUrls: ['./items-view.component.css'],
  providers: [ItemsManagerService, KdModalEvent]
})
export class ItemsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public _questionBase: any = VIEWNODE_INPUT;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  private itemId: any = null;
  private currentFilters: any = null;

  public modalConfig: IModalConfig = {
    title: 'Delete Item Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.deleteItem();
          this.modalEvent.toggleClose();
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  /* Subscriptions */
  private itemIdSub: Subscription;
  private itemDetailsSub: Subscription;
  private filterValuesSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public modalEvent: KdModalEvent,
    private sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService,
    public itemsManagerService: ItemsManagerService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Examinations - Items', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/examinations/exams/details/items/list' },
      { type: 'edit', path: 'main/examinations/exams/details/items/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.modalEvent.toggleOpen();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.itemIdSub = this.examsDetailsService.itemIdChanged.subscribe((itemId) => {
      if (!itemId) {
        this.sharedService.setToaster(invalidIdError);

        setTimeout(() => {
          this.router.navigate(['main/examinations/exams/details/items']);
        }, 100);
      } else {
        this.itemId = itemId;
        this.itemsManagerService.getItemsDetails(itemId);

        this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
          if (filters['examination'] && filters['academicPeriodData']) {
            this.currentFilters = { ...filters };

            setTimeout(() => {
              this.itemDetailsSub = this.itemsManagerService.itemsViewDetailsChanged.subscribe((details) => {
                if (details) {
                  this.setItemDetails({
                    ...details,
                    academic_period: this.currentFilters['academicPeriodData']['name'],
                    examination: this.currentFilters['examination']['name']
                  });
                }
              });
            }, 0);
          }
        });
      }
    });
  }

  setItemDetails(data: any) {
    this._questionBase.forEach((question) => {
      if (question['key'] == 'academic_period') {
        this.api.setProperty(question['key'], 'value', data['academic_period']);
      } else if (question['key'] == 'examination') {
        this.api.setProperty(question['key'], 'value', data['examination']);
      }
      this.api.setProperty(
        question['key'],
        'value',
        data[question['key']]
          ? data[question['key']]['value']
            ? data[question['key']]['value']
            : data[question['key']]
          : ''
      );
    });
  }

  deleteItem() {
    this.itemsManagerService.deleteItem(this.itemId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster({ ...deleteSuccess, body: res['message'] });
          this.router.navigate(['main/examinations/exams/details/items']);
        } else {
          this.sharedService.setToaster({ ...deleteFail, body: res['message'] });
        }
      },
      (err: HttpErrorResponse) => {
        if (err['status'] == 403) {
          this.sharedService.setToaster({ ...deleteFail, body: err['error']['message'] });
        } else {
          this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.itemIdSub) {
      this.itemIdSub.unsubscribe();
    }
    if (this.itemDetailsSub) {
      this.itemDetailsSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
