export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    required: true,
    type: 'string',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    required: true,
    type: 'string',
    readonly: true
  },
  {
    key: 'subject',
    label: 'Subject',
    visible: true,
    controlType: 'text',
    required: true,
    type: 'string',
    readonly: true
  },
  {
    key: 'option',
    label: 'Option',
    visible: true,
    controlType: 'text',
    required: true,
    type: 'string',
    readonly: true
  },
  {
    key: 'component',
    label: 'Component',
    visible: true,
    controlType: 'text',
    required: true,
    type: 'string',
    readonly: true
  },
  {
    key: 'item_name',
    label: 'Item',
    visible: true,
    order: 1,
    required: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '--no data--' }],
    events: true
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    type: 'string'
  }
];
export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
