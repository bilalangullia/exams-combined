import { Observable, Subscriber, timer } from 'rxjs';

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
    RowNumber?: Column;
    Components?: Column;
    AcademicPeriod?: Column,
    Examination?: Column,
    Items?: Column,
}

export const TABLECOLUMN: ListColumn = {
    RowNumber: {
        headerName: 'Row Number',
        field: 'row_number',
        sortable: true,
        filterable: false
    },
    Components: {
        headerName: 'Components',
        field: 'components',
        sortable: true,
        filterable: false
    },
    AcademicPeriod: {
        headerName: 'Academic Period',
        field: 'academic_period',
        sortable: true,
        filterable: false
    },
    Examination: {
      headerName: 'Examination',
      field: 'examination',
      sortable: true,
      filterable: false
  },
  Items: {
    headerName: 'Items',
    field: 'items',
    sortable: true,
    filterable: false
}

  
};
export const CREATE_ROW: (_rowCount: number, _baseIndex?: number, data?: Array<any>) => Array<any> = (_rowCount: number, _baseIndex: number = 0, data?: Array<any>): Array<any> => {
    let row: Array<any> = [];
    for (let i: number = 0; i < data.length; i++) {
        let oneRow: any = {
            id: i,
            row_number:i+1,
            academic_period: data[i].data['Academic Period'],
            components:data[i].data['Components'],
            examination: data[i].data['Examination'],
            items: data[i].data['Items'],
        };
        row.push(oneRow);
    }
    return row;
};

export interface IMiniDashboardItem {
  type: string;
  icon?: string;
  label: string;
  value: number | string;
}

export interface IMiniDashboardConfig {
  closeButtonDisabled?: boolean;
  rtl?: boolean;
}

export const MINI_DASHBOARD_CONFIG: IMiniDashboardConfig = {
  closeButtonDisabled: true,
  rtl: true
};

export const MINI_DASHBOARD_DATA: Array<IMiniDashboardItem> = [
  {
    type: 'text',
    label: 'Total Rows :',
    value: '8'
  },
  {
    type: 'text',
    label: 'Row Imported :',
    value: '8'
  },
  {
    type: 'text',
    label: 'Row Update :',
    value: '8'
  },
  {
    type: 'text',
    label: 'Row Failed :',
    value: '0'
  }
];
