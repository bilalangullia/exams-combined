import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { ItemsManagerService } from '../items-manager.service';
import { FORM_BUTTONS, QUESTION_BASE } from './items-edit.config';
import { IToasterConfig } from '../../../../../../../shared/shared.interfaces';
import { missingFieldsError, invalidIdError } from '../../../../../../../shared/shared.toasters';

@Component({
  selector: 'app-items-edit',
  templateUrl: './items-edit.component.html',
  styleUrls: ['./items-edit.component.css'],
  providers: [ItemsManagerService]
})
export class ItemsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;

  public examinationId: any;
  public _questionBase: Array<any> = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public formValue: any = {};

  public itemId: any;
  private currentFilters: any = null;
  private details: any = null;

  /* Subscriptions */
  public itemTypeSub: Subscription;
  private itemIdSub: Subscription;
  public itemDetailsSub: Subscription;
  public itemOptionsSub: Subscription;
  private filterValuesSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public itemsManagerService: ItemsManagerService,
    public sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Examinations - Items', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.itemIdSub = this.examsDetailsService.itemIdChanged.subscribe((itemId) => {
      if (!itemId) {
        this.sharedService.setToaster(invalidIdError);

        setTimeout(() => {
          this.router.navigate(['main/examinations/exams/details/items']);
        }, 100);
      } else {
        this.itemId = itemId;
        this.itemsManagerService.getItemsDetails(itemId);
        this.itemsManagerService.getItemOptions();

        setTimeout(() => {
          this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
            if (filters['examination'] && filters['academicPeriodData']) {
              this.currentFilters = { ...filters };

              this.itemOptionsSub = this.itemsManagerService.itemOptionsChanged.subscribe((options) => {
                if (options) {
                  this.setDropdownOptions('item', options);
                }
              });

              this.itemDetailsSub = this.itemsManagerService.itemsViewDetailsChanged.subscribe((details) => {
                if (details) {
                  this.details = details;
                  setTimeout(() => {
                    this.setItemDetails({
                      ...details,
                      academic_period: this.currentFilters['academicPeriodData']['name'],
                      examination: this.currentFilters['examination']['name']
                    });
                  }, 0);
                }
              });
            }
          });
        }, 100);
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesObj = this._questionBase.find((question) => question['key'] === key);
    if (quesObj) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      options.unshift({ key: null, value: '--select--' });
      this.api.setProperty(quesObj['key'], 'options', [...options]);
    }
  }

  setItemDetails(data: any) {
    this._questionBase.forEach((question) => {
      if (question['key'] == 'item') {
        this.api.setProperty(question['key'], 'value', data[question['key']] ? data[question['key']]['key'] : null);
      } else {
        this.api.setProperty(
          question['key'],
          'value',
          data[question['key']]
            ? data[question['key']]['value']
              ? data[question['key']]['value']
              : data[question['key']]
            : ''
        );
      }
    });
  }

  requiredCheck(form: any) {
    console.log(form);

    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 0);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload: any = { items_id: form['item'], examination_components_id: this.details['component']['key'] };
      this.itemsManagerService.updateItemsDetail(this.itemId, payload);
    }
  }

  cancel() {
    this.router.navigate(['/main/examinations/exams/details/items/view']);
  }

  ngOnDestroy(): void {
    if (this.itemTypeSub) {
      this.itemTypeSub.unsubscribe();
    }
    if (this.itemIdSub) {
      this.itemIdSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
