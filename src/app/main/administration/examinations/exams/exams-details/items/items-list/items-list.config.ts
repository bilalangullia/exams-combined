interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Component?: Column;
  ItemCode?: Column;
  ItemName?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Component: {
    headerName: 'Component',
    field: 'component',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ItemCode: {
    headerName: 'Item Code',
    field: 'item_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ItemName: {
    headerName: 'ItemName',
    field: 'item_name',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
