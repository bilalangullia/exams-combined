import { DEFAULT_OPTIONS_EMPTY } from '../../../../../../../shared/shared.constants';

export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    readonly: true
  },
  {
    key: 'subject',
    label: 'Subject',
    visible: true,
    controlType: 'dropdown',
    options: [...DEFAULT_OPTIONS_EMPTY],
    required: true
  },
  {
    key: 'option',
    label: 'Option',
    visible: true,
    controlType: 'dropdown',
    options: [...DEFAULT_OPTIONS_EMPTY],
    required: true
  },
  {
    key: 'component',
    label: 'Component',
    visible: true,
    controlType: 'dropdown',
    options: [...DEFAULT_OPTIONS_EMPTY],
    required: true
  },
  {
    key: 'item',
    label: 'Item',
    visible: true,
    controlType: 'dropdown',
    options: [...DEFAULT_OPTIONS_EMPTY],
    required: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
