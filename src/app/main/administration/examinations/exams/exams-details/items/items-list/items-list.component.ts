import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ExcelExportParams } from 'ag-grid';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdTableDatasourceEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { DataService } from '../../../../../../../shared/data.service';
import { SharedService } from '../../../../../../../shared/shared.service';
import { PAGE_SIZE } from '../../../../../../../shared/shared.constants';
import { IFetchListParams } from '../../../../../../../shared/shared.interfaces';
import { invalidIdError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService } from '../../exams-details.service';
import { ItemsManagerService } from '../../items/items-manager.service';
import { TABLECOLUMN } from './items-list.config';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css'],
  providers: [ItemsManagerService]
})
export class ItemsListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  private GRID_ID: string = 'listNode';

  public _row: Array<any>;
  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public showTable: boolean = false;
  public examinationId: any;

  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.Component, TABLECOLUMN.ItemCode, TABLECOLUMN.ItemName];
  public tableConfig: ITableConfig = {
    id: this.GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: PAGE_SIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.examsDetailsService.setItemId(_rowNode['data']['id']);
            this.router.navigate(['/main/examinations/exams/details/items/view']);
          }
        }
      ]
    }
  };

  private tableEventList: KdTableDatasourceEvent;
  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  public examinationIdSub: Subscription;
  public itemDetailsSub: Subscription;
  private tableEventSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    public itemsManagerService: ItemsManagerService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.setPageTitle('Examinations - Items', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/examinations/exams/details/items/add' },
      { type: 'import', path: 'main/examinations/exams/details/items/import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'items_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce();
    });
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.examinationIdSub = this.examsDetailsService.examinationIdChanged.subscribe((id) => {
      if (!id) {
        this.sharedService.setToaster(invalidIdError);
      } else {
        this.examinationId = id;

        this.showTable = false;
        setTimeout(() => {
          this.showTable = true;
        }, 0);

        this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRID_ID).subscribe((event: any): void => {
          if (event instanceof KdTableDatasourceEvent) {
            this.tableEventList = event;
            setTimeout(() => {
              this.fetchList(this.tableEventList, this.searchKey);
            }, 0);
          }
        });
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      console.log('searching...');
      listReq = this.itemsManagerService.getItemsListSearch(
        this.examinationId,
        searchKey,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    } else {
      console.log('fetching...');
      listReq = this.itemsManagerService.getItemsList(
        this.examinationId,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    }

    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['itemList']) {
          list = res['data']['itemList'].map((item) => ({ ...item, component: item['component_name'] }));
          total =
            this.searchKey && this.searchKey.length ? res['data']['itemList'].length : res['data']['totalRecords'];
        } else {
          list = [];
          total = 0;
        }
        setTimeout(() => {
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
          this.loading = false;
        }, 0);
      },
      (err) => {
        list = [];
        total = 0;
        setTimeout(() => {
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
          this.loading = false;
        }, 0);
      }
    );
  }

  debounce() {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  ngOnDestroy(): void {
    if (this.examinationIdSub) {
      this.examinationIdSub.unsubscribe();
    }
    if (this.itemDetailsSub) {
      this.itemDetailsSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
