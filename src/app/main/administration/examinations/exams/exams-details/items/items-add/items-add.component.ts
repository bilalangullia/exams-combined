import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { ItemsManagerService } from '../items-manager.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './items-add.config';
import { missingFieldsError } from '../../../../../../../shared/shared.toasters';
import { DEFAULT_OPTIONS_EMPTY } from '../../../../../../../shared/shared.constants';

@Component({
  selector: 'app-items-add',
  templateUrl: './items-add.component.html',
  styleUrls: ['./items-add.component.css'],
  providers: [ItemsManagerService]
})
export class ItemsAddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;

  public examinationId: any;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  public itemId: any;

  private currentFilters: IFilter = null;

  /* Subscriptions */
  public itemTypeSub: Subscription;
  private itemIdSub: Subscription;
  private filterValuesSub: Subscription;
  private subjectOptionsSub: Subscription;
  private optionOptionsSub: Subscription;
  private componentOptionsSub: Subscription;
  private itemOptionsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public itemsManagerService: ItemsManagerService,
    public sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Examinations - Items', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.itemIdSub = this.examsDetailsService.optionIdChanged.subscribe((data) => {
      if (data) {
        this.itemId = data;
        this.itemsManagerService.getItemsDetails(data);
      } else {
        this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
          if (filters['examination'] && filters['academicPeriodData']) {
            this.currentFilters = { ...filters };

            setTimeout(() => {
              this.setDetails();
              this.itemsManagerService.getSubjectOptions(this.currentFilters['examination']['id']);
              this.itemsManagerService.getItemOptions();
            }, 100);
          }
        });

        setTimeout(() => {
          this.subjectOptionsSub = this.itemsManagerService.subjectOptionsChanged.subscribe((options) => {
            if (options) {
              this.setDropdownOptions('subject', options);
            }
          });
          this.optionOptionsSub = this.itemsManagerService.optionOptionsChanged.subscribe((options) => {
            if (options) {
              this.setDropdownOptions('option', options);
            }
          });
          this.componentOptionsSub = this.itemsManagerService.componentOptionsChanged.subscribe((options) => {
            if (options) {
              this.setDropdownOptions('component', options);
            }
          });
          this.itemOptionsSub = this.itemsManagerService.itemOptionsChanged.subscribe((options) => {
            if (options) {
              this.setDropdownOptions('item', options);
            }
          });
        }, 100);
      }
    });
  }

  setDetails() {
    this._questionBase.forEach((question) => {
      if (question['key'] == 'academic_period') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['academicPeriodData']['name']);
      } else if (question['key'] == 'examination') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['examination']['name']);
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    options = data.map((item) => ({ key: item['id'], value: item['name'] }));
    options.unshift(...DEFAULT_OPTIONS_EMPTY);

    this.api.setProperty(key, 'options', [...options]);

    setTimeout(() => {
      this.api.setProperty(key, 'value', null);
    }, 0);
  }

  detectValue(event) {
    if (event['key'] == 'subject' && (event['value'] || event['value'] != 'null')) {
      this.itemsManagerService.getOptionOptions(event['value']);

      this.api.setProperty('option', 'options', [...DEFAULT_OPTIONS_EMPTY]);
      this.api.setProperty('component', 'options', [...DEFAULT_OPTIONS_EMPTY]);
      setTimeout(() => {
        this.api.setProperty('option', 'value', null);
        this.api.setProperty('component', 'value', null);
      }, 0);
    } else if (event['key'] == 'option' && (event['value'] || event['value'] != 'null')) {
      this.itemsManagerService.getComponentOptions(event['value']);

      this.api.setProperty('component', 'options', [...DEFAULT_OPTIONS_EMPTY]);
      setTimeout(() => {
        this.api.setProperty('component', 'value', null);
      }, 0);
    }
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form: any): void {
    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        academic_period_id: this.currentFilters['academicPeriodData']['id'],
        examination_id: this.currentFilters['examination']['id'],
        examination_subject_id: form['subject'],
        examination_options_id: form['option'],
        examination_components_id: form['component'],
        items_id: form['item']
      };

      this.itemsManagerService.addItem(payload);
    }
  }

  cancel() {
    this._router.navigate(['main/examinations/exams/details/items']);
  }

  ngOnDestroy(): void {
    if (this.subjectOptionsSub) {
      this.subjectOptionsSub.unsubscribe();
    }
    if (this.optionOptionsSub) {
      this.optionOptionsSub.unsubscribe();
    }
    if (this.componentOptionsSub) {
      this.componentOptionsSub.unsubscribe();
    }
    if (this.itemOptionsSub) {
      this.itemOptionsSub.unsubscribe();
    }
    if (this.itemTypeSub) {
      this.itemTypeSub.unsubscribe();
    }
    if (this.itemIdSub) {
      this.itemIdSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
