import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { saveSuccess, serverError, saveFail } from '../../../../../../shared/shared.toasters';
import { ExamsService } from '../../exams.service';
import { ExamsDetailsService } from '../exams-details.service';

@Injectable()
export class ItemsManagerService {
  private subjectOptions: Array<any> = [];
  private optionOptions: Array<any> = [];
  private componentOptions: Array<any> = [];
  private itemOptions: Array<any> = [];

  private itemsViewDetails: any = null;
  public itemImportDetail: any;
  private itemType: any;

  public subjectOptionsChanged = new BehaviorSubject<Array<any>>([...this.subjectOptions]);
  public optionOptionsChanged = new BehaviorSubject<Array<any>>([...this.optionOptions]);
  public componentOptionsChanged = new BehaviorSubject<Array<any>>([...this.componentOptions]);
  public itemOptionsChanged = new BehaviorSubject<Array<any>>([this.itemOptions]);

  public itemTypeChanged = new Subject();
  public itemsViewDetailsChanged = new BehaviorSubject<any>({ ...this.itemsViewDetails });

  constructor(
    private router: Router,
    private sharedService: SharedService,
    private examsService: ExamsService,
    private examsDetailsService: ExamsDetailsService
  ) {}

  addItem(payload: any) {
    this.examsDetailsService.addItem({ ...payload }).subscribe(
      (res: any) => {
        if (res) {
          this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/examinations/exams/details/items']);
          }, 500);
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  getSubjectOptions(examinationId) {
    this.examsDetailsService.getSubjectOptions(examinationId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.subjectOptions = res['data'];
          this.subjectOptionsChanged.next([...this.subjectOptions]);
        }
      },
      (err) => {
        this.subjectOptions = [];
        this.subjectOptionsChanged.next([]);
      }
    );
  }

  getOptionOptions(subjectId) {
    this.examsDetailsService.getOptionOptions(subjectId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.optionOptions = res['data'];
          this.optionOptionsChanged.next([...this.optionOptions]);
        }
      },
      (err) => {
        this.optionOptions = [];
        this.optionOptionsChanged.next([]);
      }
    );
  }

  getComponentOptions(optionId) {
    this.examsDetailsService.getComponentOptions(optionId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.componentOptions = res['data'];
          this.componentOptionsChanged.next([...this.componentOptions]);
        }
      },
      (err) => {
        this.componentOptions = [];
        this.componentOptionsChanged.next([]);
      }
    );
  }

  getItemOptions() {
    this.examsDetailsService.getItemOptions().subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.itemOptions = res['data'];
          this.itemOptionsChanged.next([...this.itemOptions]);
        }
      },
      (err) => {
        this.itemOptions = [];
        this.itemOptionsChanged.next([]);
      }
    );
  }

  /*** LIST  PAGE | API  ***/
  getItemsList(exam_id, start: number, end: number) {
    return this.examsDetailsService.getItemsList(exam_id, start, end);
  }

  /*** LIST PAGE | SEARCH  ***/
  getItemsListSearch(examination_id, keyword, start: number, end: number) {
    return this.examsDetailsService.getItemsListSearch(examination_id, keyword, start, end);
  }

  /*** EDIT PAGE | EXAM UPDATE  ***/
  updateItemsDetail(item_id, data) {
    this.examsDetailsService.updateItemsDetail(item_id, data).subscribe(
      (res) => {
        if (res && res['data']) {
          this.sharedService.setToaster({ ...saveSuccess, body: res['messae'] });
          this.router.navigate(['main/examinations/exams/details/items']);
        } else {
          this.sharedService.setToaster({ ...saveFail, body: res['messae'] });
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  /*** EDIT PAGE | ITEM TYPE  DROPDOWN ***/
  getItem() {
    this.examsDetailsService.getItemType().subscribe((res: any) => {
      if (res.data.length) {
        this.itemType = res.data;
        this.itemTypeChanged.next([...this.itemType]);
      } else {
        this.itemType = [];
        this.itemTypeChanged.next([...this.itemType]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Item Exists',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  /*** VIEW/EDIT PAGE | Get exam  details for populating the view fields. ***/
  getItemsDetails(item_id) {
    this.examsDetailsService.getItemsDetail(item_id).subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.itemsViewDetails = res.data[0];
        this.itemsViewDetailsChanged.next({ ...this.itemsViewDetails });
      } else {
        this.itemsViewDetails = null;
        this.itemsViewDetailsChanged.next({ ...this.itemsViewDetails });
        let toasterConfig: any = {
          type: 'error',
          title: 'No Items Details Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  setItemImportDetail(data) {
    this.itemImportDetail = data;
  }

  getItemImportDetail() {
    return this.itemImportDetail;
  }

  deleteItem(itemId) {
    return this.examsDetailsService.deleteItem(itemId);
  }
}
