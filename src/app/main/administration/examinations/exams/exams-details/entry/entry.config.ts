import { IKdTabs } from '../../../../../../shared/kdComponents/kdInterfaces';

const ROUTER_BASE_PATH: string = 'main/examinations/exams/details';

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'Exams', routerPath: ROUTER_BASE_PATH + '/view', isActive: true },
  { tabName: 'Subjects', routerPath: ROUTER_BASE_PATH + '/subjects' },
  { tabName: 'Options', routerPath: ROUTER_BASE_PATH + '/options' },
  { tabName: 'Components', routerPath: ROUTER_BASE_PATH + '/components' },
  { tabName: 'Items', routerPath: ROUTER_BASE_PATH + '/items' }
];
