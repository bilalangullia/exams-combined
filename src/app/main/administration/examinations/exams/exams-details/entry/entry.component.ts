import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, IPageheaderApi, IPageheaderConfig, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { ExamsDetailsService } from '../exams-details.service';
import { TABS_LIST } from './entry.config';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent extends KdPageBase implements OnInit, OnDestroy {
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = { home: { name: 'Home', path: '/' } };
  public IsTabActive: boolean = true;
  public tabsList: Array<any>;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router, activatedRoute, pageEvent });

    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });

    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => {
          this.router.navigate(['main/examinations/exams/list']);
        }
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();
  }

  ngOnInit() {
    this.tabsList = TABS_LIST;
    this.tabsList[0]['isActive'] = true;
    this.examsDetailsService.getExaminationId();
    this.examsDetailsService.setExaminationValue();
    this.examsDetailsService.getAcademicPeriod();
    this.examsDetailsService.getExamination();
    this.examsDetailsService.getAcademicPeriodData();
  }

  selectedTab(event) {
    // console.log('EntryComponent -> selectedTab -> event', event);
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
