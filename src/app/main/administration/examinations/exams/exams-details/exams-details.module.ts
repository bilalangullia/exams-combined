import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { ExamsDetailsRoutingModule } from './exams-details-routing.module';
import { ExamsDetailsService } from './exams-details.service';
import { EntryComponent } from './entry/entry.component';
import { ExamsViewComponent } from './exams/exams-view/exams-view.component';
import { ExamsEditComponent } from './exams/exams-edit/exams-edit.component';
import { SubjectsListComponent } from './subjects/subjects-list/subjects-list.component';
import { SubjectsViewComponent } from './subjects/subjects-view/subjects-view.component';
import { ComponentsListComponent } from './components/components-list/components-list.component';
import { ComponentsViewComponent } from './components/components-view/components-view.component';
import { SubjectsEditComponent } from './subjects/subjects-edit/subjects-edit.component';
import { ComponentsEditComponent } from './components/components-edit/components-edit.component';
import { OptionsListComponent } from './options/options-list/options-list.component';
import { OptionsViewComponent } from './options/options-view/options-view.component';
import { OptionsEditComponent } from './options/options-edit/options-edit.component';
import { ItemsListComponent } from './items/items-list/items-list.component';
import { ItemsViewComponent } from './items/items-view/items-view.component';
import { ItemsEditComponent } from './items/items-edit/items-edit.component';
import { ItemsAddComponent } from './items/items-add/items-add.component';
import { ComponentsAddComponent } from './components/components-add/components-add.component';
import { OptionsAddComponent } from './options/options-add/options-add.component';
import { SubjectsAddComponent } from './subjects/subjects-add/subjects-add.component';
import { ExamsDetailsDataService } from './exams-details-data.service';
import { ImportFormComponent as SubjectsImportForm } from './subjects/import-form/import-form.component';
import { ImportListComponent as SubjectsImportList } from './subjects/import-list/import-list.component';
import { ImportFormComponent as OptionsImportForm } from './options/import-form/import-form.component';
import { ImportListComponent as OptionsImportList } from './options/import-list/import-list.component';
import { ImportFormComponent as ComponentsImportForm } from './components/import-form/import-form.component';
import { ImportListComponent as ComponentsImportList } from './components/import-list/import-list.component';
import { ImportFormComponent as ItemsImportForm } from './items/import-form/import-form.component';
import { ImportListComponent as ItemsImportList } from './items/import-list/import-list.component';
import  {OptionsManagerService} from './options/options-manager.service';
import {ItemsManagerService} from './items/items-manager.service'
import { ComponentsManagerService } from './components/components-manager.service';
import { SubjectsManagerService } from './subjects/subjects-manager.service';
@NgModule({
  imports: [CommonModule, ExamsDetailsRoutingModule, SharedModule],
  declarations: [
    EntryComponent,
    ExamsViewComponent,
    ExamsEditComponent,
    SubjectsListComponent,
    SubjectsViewComponent,
    SubjectsEditComponent,
    SubjectsAddComponent,
    OptionsListComponent,
    OptionsViewComponent,
    OptionsEditComponent,
    OptionsAddComponent,
    ComponentsListComponent,
    ComponentsViewComponent,
    ComponentsEditComponent,
    ComponentsAddComponent,
    ItemsListComponent,
    ItemsViewComponent,
    ItemsEditComponent,
    ItemsAddComponent,
    SubjectsImportForm,
    SubjectsImportList,
    OptionsImportForm,
    OptionsImportList,
    ComponentsImportForm,
    ComponentsImportList,
    ItemsImportForm,
    ItemsImportList
  ],
  providers: [ExamsDetailsService, ExamsDetailsDataService,OptionsManagerService,ItemsManagerService,ComponentsManagerService,SubjectsManagerService]
})
export class ExamsDetailsModule {}
