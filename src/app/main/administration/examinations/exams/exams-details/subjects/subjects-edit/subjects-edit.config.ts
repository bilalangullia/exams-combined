export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'code',
    label: 'Subject Code',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'name',
    label: 'Subject Name',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'education_subject',
    label: 'Education Subject',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    required: true,
    options: [{ key: '', value: '--select--' }],
    events: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
