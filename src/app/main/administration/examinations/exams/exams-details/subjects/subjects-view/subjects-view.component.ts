import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent } from 'openemis-styleguide-lib';

import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { SubjectsManagerService } from '../subjects-manager.service';
import { VIEWNODE_INPUT, IModalConfig } from './subjects-view.config';

@Component({
  selector: 'app-subjects-view',
  templateUrl: './subjects-view.component.html',
  styleUrls: ['./subjects-view.component.css'],
  providers: [SubjectsManagerService, KdModalEvent]
})
export class SubjectsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private academicPeriod: any;
  private examinationValue: any;

  private subjectId: number = null;
  private examinationId: number = null;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private subjectViewDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Subject Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.deleteSubject();
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public modalEvent: KdModalEvent,
    private examsDetailsService: ExamsDetailsService,
    private subjectsManagerService: SubjectsManagerService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Examinations - Subjects', false);
    super.setToolbarMainBtns([
      { type: 'back', path: '/main/examinations/exams/details/subjects/list' },
      { type: 'edit', path: '/main/examinations/exams/details/subjects/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (
        filters['academicPeriod'] &&
        filters['examinationId'] &&
        filters['examinationValue'] &&
        filters['subjectId']['id']
      ) {
        this.subjectId = filters['subjectId']['id'];
        this.examinationId = filters['examination']['id'];
        this.examinationValue = filters['examinationValue'];
        this.academicPeriod = filters['academicPeriod'];
        this.subjectsManagerService.getSubjectViewDetails(this.subjectId, this.examinationId);
      }
    });

    this.subjectViewDetailsSub = this.subjectsManagerService.subjectsViewDetailsChanged.subscribe((data) => {
      if (data.length) {
        this.setSubjectDetails({
          ...data[0],
          academic_period: this.academicPeriod,
          examination: this.examinationValue
        });
      } else {
        this.loading = false;
      }
    });
  }

  setSubjectDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        this.api.setProperty(
          this._questionBase[i].key,
          'value',
          !data[this._questionBase[i].key]
            ? ''
            : data[this._questionBase[i].key].value
            ? data[this._questionBase[i].key].value
            : data[this._questionBase[i].key]
        );
      }
      this.loading = false;
    });
  }

  deleteSubject() {
    this.subjectsManagerService.deleteSubject(this.subjectId).subscribe(
      (res: any) => {
        if (res) {
          console.log(res);
        }
      },
      (err: HttpErrorResponse) => {
        console.log(err['error']);
      }
    );
  }

  open() {
    this.modalEvent.toggleOpen();
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.subjectViewDetailsSub) {
      this.subjectViewDetailsSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
