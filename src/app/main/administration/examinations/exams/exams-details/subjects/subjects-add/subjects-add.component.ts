import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';

import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { SubjectsManagerService } from '../subjects-manager.service';
import { QUESTION_BASE, FORM_BUTTONS } from './subjects-add.config';
import { IFilterItem } from '../../../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../../shared/shared.toasters';

@Component({
  selector: 'app-subjects-add',
  templateUrl: './subjects-add.component.html',
  styleUrls: ['./subjects-add.component.css'],
  providers: [SubjectsManagerService]
})
export class SubjectsAddComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private examinationId: number = null;
  private subjectId: number = null;

  @ViewChild('editForm') _updateView: KdView;

  private currentFilters: IFilter = null;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private educationSubjectSub: Subscription;
  private subjectDetailsSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService,
    private subjectsManagerService: SubjectsManagerService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Examinations - Subjects', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.subjectsManagerService.getEducationSubjectDropdown();

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (filters && filters['academicPeriodData'] && filters['examination']) {
        this.currentFilters = { ...filters };
      }
    });

    this.educationSubjectSub = this.subjectsManagerService.educationSubjectChanged.subscribe((options: Array<any>) => {
      this.setDropdownOptions('education_subject', options);
    });

    setTimeout(() => {
      this.setDetails();
    }, 100);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
    }
    options.unshift({ key: null, value: '-- Select --' });
    const questionObj = this._questionBase.find((item) => item['key'] === key);
    if (questionObj) {
      this.api.setProperty(questionObj['key'], 'options', options);
    }
  }

  setDetails() {
    timer(200).subscribe(() => {
      this._questionBase.forEach((question) => {
        if (question['key'] == 'academic_period_id') {
          this.api.setProperty(question['key'], 'value', this.currentFilters['academicPeriodData']['name']);
        } else if (question['key'] == 'examination') {
          this.api.setProperty(question['key'], 'value', this.currentFilters['examination']['name']);
        }
      });
    });
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck({ ...form })) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload: any = {
        code: form['code'],
        name: form['name'],
        education_subject_id: form['education_subject'],
        examination_id: this.currentFilters['examination']['id'],
        academic_period_id: this.currentFilters['academicPeriodData']['id']
      };

      this.subjectsManagerService.addSubject(payload);
    }
  }

  cancel() {
    this.router.navigate(['main/examinations/exams/details/subjects']);
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.subjectDetailsSub) {
      this.subjectDetailsSub.unsubscribe();
    }
    if (this.educationSubjectSub) {
      this.educationSubjectSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
