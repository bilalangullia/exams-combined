import { ITableColumn } from 'openemis-styleguide-lib';

export const TOTALROW: number = 1000;
export const GRID_ID: string = 'listNode';

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Examination?: Column;
  SubjectCode?: Column;
  SubjectName?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Examination: {
    headerName: 'Examination',
    field: 'examination',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  SubjectCode: {
    headerName: 'Subject Code',
    field: 'code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  SubjectName: {
    headerName: 'Subject Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export interface ITableActionApi {
  deleteThisRow?: () => void;
}

export const _tableColumns: Array<ITableColumn> = [
  TABLECOLUMN.Examination,
  TABLECOLUMN.SubjectCode,
  TABLECOLUMN.SubjectName
];
