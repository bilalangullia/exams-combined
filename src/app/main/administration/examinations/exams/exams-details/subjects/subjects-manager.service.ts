import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { saveSuccess, serverError, updateSuccess } from '../../../../../../shared/shared.toasters';
import { ExamsDetailsService } from '../exams-details.service';

@Injectable()
export class SubjectsManagerService {
  private subjectsList: any;
  public subjectsListChanged = new Subject();
  private subjectsViewDetails: any = [];
  public subjectsViewDetailsChanged = new Subject<any>();
  private subjectsEditDetails: any = [];
  public subjectsEditDetailsChanged = new Subject<any>();
  private educationSubject: any;
  public educationSubjectChanged = new Subject();
  public importDetail: any;
  constructor(
    private router: Router,
    private sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService
  ) {}

  addSubject(payload: any) {
    this.examsDetailsService.addSubject(payload).subscribe(
      (res: any) => {
        if (res) {
          console.log(res);
          this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/examinations/exams/details/subjects']);
          }, 500);
        }
      },
      (err: HttpErrorResponse) => {
        console.log(err);
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  getSubjectsList(examinationId: string, start: number, end: number) {
    return this.examsDetailsService.getSubjectsList(examinationId, start, end);
  }

  getSubjectsListSearch(examinationId: string, keyword: string, start: number, end: number) {
    return this.examsDetailsService.getSubjectsListSearch(examinationId, keyword, start, end);
  }

  getSubjectViewDetails(subjectId, examinationId) {
    this.examsDetailsService.getSubjectsViewDetails(subjectId, examinationId).subscribe((details) => {
      if (details['data'].length) {
        this.subjectsViewDetails = details['data'];
        this.subjectsViewDetailsChanged.next([...this.subjectsViewDetails]);
      } else {
        this.subjectsViewDetails = [];
        this.subjectsViewDetailsChanged.next([...this.subjectsViewDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Subject Details Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  /*** VIEW  PAGE | Get subject  details for populating the view fields. ***/
  editSubjectDetails(subject_id, examination_id) {
    this.examsDetailsService.getSubjectEditDetails(subject_id, examination_id).subscribe((details: any) => {
      if (details.data.length) {
        this.subjectsEditDetails = details.data;
        this.subjectsEditDetailsChanged.next([...this.subjectsEditDetails]);
      } else {
        this.subjectsEditDetails = [];
        this.subjectsEditDetailsChanged.next([...this.subjectsEditDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Subject Details Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  /*** SUBJECTS EDIT  PAGE |EDUCATION SUBJECT DROPDOWN | API ***/
  getEducationSubjectDropdown() {
    this.examsDetailsService.getEducationSubjectDropdown().subscribe((dropdowns: any) => {
      if (dropdowns.data.length) {
        this.educationSubject = dropdowns.data;
        this.educationSubjectChanged.next([...this.educationSubject]);
      } else {
        this.educationSubject = [];
        this.educationSubjectChanged.next([...this.educationSubject]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Education Subject Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  updateSubjectDetails(subjectId, data) {
    this.examsDetailsService.updateSubjectDetail(subjectId, data).subscribe(
      (res) => {
        if (res) {
          this.sharedService.setToaster({ ...updateSuccess, body: res['message'] });
          this.router.navigate(['main/examinations/exams/details/subjects']);
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  deleteSubject(subjectId: number) {
    return this.examsDetailsService.deleteSubject(subjectId);
  }

  setSubjectImportDetail(data: any) {
    this.importDetail = data;
  }

  getSubjectImportDetail() {
    return this.importDetail;
  }
}
