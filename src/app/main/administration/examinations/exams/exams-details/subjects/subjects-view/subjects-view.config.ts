export const VIEWNODE_INPUT: Array<any> = [
  { key: 'academic_period', label: 'Academic Period', visible: true, controlType: 'text', type: 'string' },
  { key: 'examination', label: 'Examination', visible: true, controlType: 'text', type: 'string' },
  { key: 'code', label: 'Subject Code', visible: true, controlType: 'text', type: 'string' },
  { key: 'name', label: 'Subject Name', visible: true, controlType: 'text', type: 'string' },
  { key: 'education_subject', label: 'Education Subject', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_user_id', label: 'Modified By', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified', label: 'Modified On', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_user_id', label: 'Created By', visible: true, controlType: 'text', type: 'string' },
  { key: 'created', label: 'Created On', visible: true, controlType: 'text', type: 'string' }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
