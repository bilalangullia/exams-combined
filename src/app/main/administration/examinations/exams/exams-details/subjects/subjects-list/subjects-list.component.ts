import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ExcelExportParams } from 'ag-grid';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableApi,
  ITableConfig,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { IFetchListParams } from '../../../../../../../shared/shared.interfaces';
import { PAGE_SIZE } from '../../../../../../../shared/shared.constants';
import { invalidIdError } from '../../../../../../../shared/shared.toasters';
import { ExamsService } from '../../../exams.service';
import { ExamsDetailsService } from '../../exams-details.service';
import { SubjectsManagerService } from '../subjects-manager.service';
import { _tableColumns, GRID_ID, TOTALROW } from './subjects-list.config';

@Component({
  selector: 'app-subjects-list',
  templateUrl: './subjects-list.component.html',
  styleUrls: ['./subjects-list.component.css'],
  providers: [SubjectsManagerService]
})
export class SubjectsListComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: Boolean = true;
  public tableConfig: ITableConfig = {
    id: GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.examsDetailsService.setSubjectId(_rowNode['data']['id'], _rowNode['data']['examination']);
            timer(200).subscribe(() => {
              this.router.navigate(['main/examinations/exams/details/subjects/view']);
            });
          }
        }
      ]
    }
  };
  public tableColumns: Array<any> = _tableColumns;
  public _row: Array<any>;
  public _tableApi: ITableApi = {};
  private examinationId: any;
  public showTable: boolean = false;

  private tableEventList: KdTableDatasourceEvent;
  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  private examinationIdSub: Subscription;
  private tableEventSub: Subscription;

  constructor(
    private pageEvent: KdPageBaseEvent,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _kdalert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    private subjectsManagerService: SubjectsManagerService,
    private examsService: ExamsService,
    private examsDetailsService: ExamsDetailsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Examinations - Subjects', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/examinations/exams/details/subjects/add' },
      { type: 'import', path: 'main/examinations/exams/details/subjects/import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'subjects_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce();
    });
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.examinationIdSub = this.examsDetailsService.examinationIdChanged.subscribe((id) => {
      if (!id) {
        this.sharedService.setToaster(invalidIdError);
        // this.router.navigate
      } else {
        this.examinationId = id;
        this.showTable = false;
        setTimeout(() => {
          this.showTable = true;
        }, 0);

        this.tableEventSub = this.tableEvent.onKdTableEventList(GRID_ID).subscribe((event: any): void => {
          if (event instanceof KdTableDatasourceEvent) {
            this.tableEventList = event;
            setTimeout(() => {
              this.fetchList(this.tableEventList, this.searchKey);
            }, 0);
          }
        });
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      console.log('searching...');
      listReq = this.subjectsManagerService.getSubjectsListSearch(
        this.examinationId,
        searchKey,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    } else {
      console.log('fetching...');
      listReq = this.subjectsManagerService.getSubjectsList(
        this.examinationId,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['userRecords']) {
          list = res['data']['userRecords'];
          total = this.searchKey && this.searchKey.length ? res['data']['userRecords'].length : res['data']['total'];
        } else {
          list = [];
          total = 0;
        }

        setTimeout(() => {
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
          this.loading = false;
        }, 0);
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce() {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.examinationIdSub) {
      this.examinationIdSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
