import { DEFAULT_OPTIONS_EMPTY } from '../../../../../../../shared/shared.constants';

export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    // options: [...DEFAULT_OPTIONS_EMPTY],
    // required: true,
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    // options: [...DEFAULT_OPTIONS_EMPTY],
    // required: true,
    readonly: true
  },
  {
    key: 'code',
    label: 'Subject Code',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'name',
    label: 'Subject Name',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'education_subject',
    label: 'Education Subject',
    visible: true,
    controlType: 'dropdown',
    options: [...DEFAULT_OPTIONS_EMPTY],
    required: true,
    events: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
