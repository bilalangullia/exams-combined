import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { SubjectsManagerService } from '../subjects-manager.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './subjects-edit.config';

@Component({
  selector: 'app-subjects-edit',
  templateUrl: './subjects-edit.component.html',
  styleUrls: ['./subjects-edit.component.css'],
  providers: [SubjectsManagerService]
})
export class SubjectsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: Array<any> = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private academicPeriodId: any = null;
  private examinationId: number = null;
  private subjectId: number = null;

  @ViewChild('editForm') _updateView: KdView;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private educationSubjectSub: Subscription;
  private subjectDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private examsDetailsService: ExamsDetailsService,
    private subjectsManagerService: SubjectsManagerService,
    private sharedService: SharedService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Examinations - Subjects', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.subjectsManagerService.getEducationSubjectDropdown();

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (
        filters['academicPeriod'] &&
        filters['examinationId'] &&
        filters['examinationValue'] &&
        filters['subjectId']['id']
      ) {
        this.examinationId = filters['examinationId'];
        this.academicPeriodId = filters['academicPeriod'];
        this.subjectId = filters['subjectId']['id'];
        this.subjectDetailsSub = this.examsDetailsService
          .getSubjectEditDetails(filters['subjectId']['id'], filters['examinationId'])
          .subscribe((details) => {
            if (details['data']) {
              this.setSubjectDetails({
                ...details['data'][0],
                academic_period: filters['academicPeriod'],
                examination: filters['examinationValue']
              });
            }
          });
      }
    });

    this.educationSubjectSub = this.subjectsManagerService.educationSubjectChanged.subscribe((options: Array<any>) => {
      let newOptions = options.map((item) => ({ key: item.id, value: item.name }));
      this._updateView.setInputProperty('education_subject', 'options', newOptions);
    });
  }

  setSubjectDetails(data: any) {
    timer(200).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'education_subject') {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i]['key']]['key']);
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
    this.loading = false;
  }

  requiredCheck(formVal: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formVal[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload: any = {
        ...event,
        code: event['code'],
        name: event['name'],
        education_subject_id: event['education_subject'],
        examination_id: this.examinationId,
        academic_period_id: this.academicPeriodId
      };
      this.subjectsManagerService.updateSubjectDetails(this.subjectId, payload);
    }
  }

  cancel() {
    this._router.navigate(['/main/examinations/exams/details/subjects/view']);
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.subjectDetailsSub) {
      this.subjectDetailsSub.unsubscribe();
    }
    if (this.educationSubjectSub) {
      this.educationSubjectSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
