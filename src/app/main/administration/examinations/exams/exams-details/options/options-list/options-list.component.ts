import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ExcelExportParams } from 'ag-grid';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableConfig,
  ITableColumn,
  ITableApi,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { DataService } from '../../../../../../../shared/data.service';
import { SharedService } from '../../../../../../../shared/shared.service';
import { IFetchListParams } from '../../../../../../../shared/shared.interfaces';
import { PAGE_SIZE } from '../../../../../../../shared/shared.constants';
import { invalidIdError } from '../../../../../../../shared/shared.toasters';
import { ExamsListService } from '../../../exams-list/exams-list.service';
import { ExamsDetailsService } from '../../exams-details.service';
import { OptionsManagerService } from '../options-manager.service';
import { TABLECOLUMN } from './options-list.config';

@Component({
  selector: 'app-options-list',
  templateUrl: './options-list.component.html',
  styleUrls: ['./options-list.component.css'],
  providers: [OptionsManagerService, ExamsListService]
})
export class OptionsListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRID_ID: string = 'listNode';

  public _row: Array<any>;
  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public examinationId: any;
  public showTable: boolean = false;

  private tableEventList: KdTableDatasourceEvent;
  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /*   SUBSCRIPTION */
  private optionListSub: Subscription;
  private examinationIdSub: Subscription;
  private tableEventSub: Subscription;

  public tableConfig: ITableConfig = {
    id: this.GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: PAGE_SIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.examsDetailsService.setOptionId(_rowNode['data']['option_id'], _rowNode['data']['subject']);
            timer(200).subscribe(() => {
              this.router.navigate(['/main/examinations/exams/details/options/view']);
            });
          }
        }
      ]
    }
  };

  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.Subject, TABLECOLUMN.OptionCode, TABLECOLUMN.OptionName];
  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    public optionsManagerService: OptionsManagerService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.setPageTitle('Examinations - Options', false);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce();
    });

    super.setToolbarMainBtns([
      { type: 'add', path: 'main/examinations/exams/details/options/add' },
      { type: 'import', path: 'main/examinations/exams/details/options/import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'options_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.examinationIdSub = this.examsDetailsService.examinationIdChanged.subscribe((id) => {
      if (!id) {
        this.sharedService.setToaster(invalidIdError);
      } else {
        this.examinationId = id;
        this.showTable = false;
        setTimeout(() => {
          this.showTable = true;
        }, 0);

        this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRID_ID).subscribe((event: any): void => {
          if (event instanceof KdTableDatasourceEvent) {
            this.tableEventList = event;
            setTimeout(() => {
              this.fetchList(this.tableEventList, this.searchKey);
            }, 0);
          }
        });
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      console.log('searching...');
      listReq = this.optionsManagerService.getOptionsListSearch(
        this.examinationId,
        searchKey,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    } else {
      console.log('fetching...');
      listReq = this.optionsManagerService.getOptionsList(
        this.examinationId,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['optionList']) {
          list = res['data']['optionList'];
          total =
            this.searchKey && this.searchKey.length ? res['data']['optionList'].length : res['data']['totalRecords'];
        } else {
          list = [];
          total = 0;
        }
        setTimeout(() => {
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
          this.loading = false;
        }, 0);
      },
      (err) => {
        list = [];
        total = 0;
        setTimeout(() => {
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
          this.loading = false;
        }, 0);
      }
    );
  }

  debounce() {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  ngOnDestroy(): void {
    if (this.optionListSub) {
      this.optionListSub.unsubscribe();
    }
    if (this.examinationIdSub) {
      this.examinationIdSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
