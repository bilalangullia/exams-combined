import { Observable, Subscriber, timer } from 'rxjs';

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
    RowNumber?: Column;
    AcademicPeriod?: Column;
    AttendanceType?: Column,
    CarryForward?: Column,
    Examination?: Column,
    GradingType?:Column,
    MaxOptionMark?:Column,
    OptioneCode?:Column,
    OptioneName?:Column,
    Subject?:Column

}

export const TABLECOLUMN: ListColumn = {
    RowNumber: {
        headerName: 'Row Number',
        field: 'row_number',
        sortable: true,
        filterable: false
    },
    AcademicPeriod: {
        headerName: 'Academic Period',
        field: 'academic_period',
        sortable: true,
        filterable: false
    },
    AttendanceType: {
        headerName: 'Attendance Type',
        field: 'attendance_type',
        sortable: true,
        filterable: false
    },
    CarryForward: {
      headerName: 'Carry Forward',
      field: 'carry_forward',
      sortable: true,
      filterable: false
  },
  Examination: {
        headerName: 'Examination',
        field: 'examination',
        sortable: true,
        filterable: false
    },
    GradingType: {
        headerName: 'Grading Type',
        field: 'grading_type',
        sortable: true,
        filterable: false
    },
    MaxOptionMark: {
        headerName: 'Max Option Mark',
        field: 'max_option_mark',
        sortable: true,
        filterable: false
    },
    OptioneCode: {
        headerName: 'Option Code',
        field: 'option_code',
        sortable: true,
        filterable: false
    },
    OptioneName: {
      headerName: 'Option Name',
      field: 'option_name',
      sortable: true,
      filterable: false
  },
  Subject: {
    headerName: 'Subject',
    field: 'subject',
    sortable: true,
    filterable: false
}

};
export const CREATE_ROW: (_rowCount: number, _baseIndex?: number, data?: Array<any>) => Array<any> = (_rowCount: number, _baseIndex: number = 0, data?: Array<any>): Array<any> => {
    let row: Array<any> = [];
    for (let i: number = 0; i < data.length; i++) {
        let oneRow: any = {
            id: i,
            row_number:i+1,
            academic_period: data[i].data['Academic Period'],
            attendance_type:data[i].data['Attendance Type'],
            carry_forward: data[i].data['Carry Forward'],
            examination: data[i].data['Examination'],
            grading_type: data[i].data['Grading Type'],
            max_option_mark: data[i].data['Max Option Mark'],
            option_code: data[i].data['Option Code'],
            option_name: data[i].data['Option Name'],
            subject:data[i].data['subject'] 
        };
        row.push(oneRow);
    }
    return row;

  };


export const DUMMY_API_CALL: (_params: {
  startRow: number;
  endRow: number;
  filterModel: any;
  sortModel: any;
  pagesize: number;
}) => Observable<any> = (_params: {
  startRow: number;
  endRow: number;
  filterModel: any;
  sortModel: any;
  pagesize: number;
}): Observable<any> => {
  return new Observable((_observer: Subscriber<any>): void => {
    timer(1000).subscribe((): void => {
      _observer.next(CREATE_ROW(_params.pagesize, _params.startRow));
      _observer.complete();
    });
  });
};

export interface IMiniDashboardItem {
  type: string;
  icon?: string;
  label: string;
  value: number | string;
}

export interface IMiniDashboardConfig {
  closeButtonDisabled?: boolean;
  rtl?: boolean;
}

export const MINI_DASHBOARD_CONFIG: IMiniDashboardConfig = {
  closeButtonDisabled: true,
  rtl: true
};

export const MINI_DASHBOARD_DATA: Array<IMiniDashboardItem> = [
  {
    type: 'text',
    label: 'Total Rows :',
    value: '8'
  },
  {
    type: 'text',
    label: 'Row Imported :',
    value: '8'
  },
  {
    type: 'text',
    label: 'Row Update :',
    value: '8'
  },
  {
    type: 'text',
    label: 'Row Failed :',
    value: '0'
  }
];
