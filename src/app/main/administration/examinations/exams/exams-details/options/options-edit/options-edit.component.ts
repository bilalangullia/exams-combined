import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, KdView, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { ExamsDetailsService } from '../../exams-details.service';
import { OptionsManagerService } from '../options-manager.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './options-edit.config';

@Component({
  selector: 'app-options-edit',
  templateUrl: './options-edit.component.html',
  styleUrls: ['./options-edit.component.css'],
  providers: [OptionsManagerService]
})
export class OptionsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public attendanceTypeSub: Subscription;
  public gradingTypeSub: Subscription;
  public formValue: any = {};
  public examinationId: any;
  public defaultList = {};
  public optionId: any;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private optionDetailsSub: Subscription;
  private optionIdSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    public optionsManagerService: OptionsManagerService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Examinations - Options', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.optionIdSub = this.examsDetailsService.optionIdChanged.subscribe((data) => {
      if (data) {
        this.optionId = data;
        this.optionsManagerService.getOptionsDetails(data);
      } else {
        let toasterConfig: any = {
          type: 'error',
          title: 'Invalid option ID',
          body: 'Please select valid option id.',
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters) => {
      if (filters['examinationId'] && filters['academicPeriod'] && filters['optionId']) {
        this.optionDetailsSub = this.examsDetailsService
          .getOptionsDetails(filters['optionId']['id'])
          .subscribe((details) => {
            if (details['data']) {
              this.setOptionDetails({
                ...details['data'][0],
                academic_period: filters['academicPeriod'],
                examination: filters['examinationValue']
              });
            }
          });
      }
    });
    this.setDropDownValue();
  }

  setDropDownValue() {
    timer(100).subscribe((): void => {
      this.optionsManagerService.getAttendanceType('attendanceTypes');
      this.optionsManagerService.getGradingTypeDetail();
      this.initializeSubscriptions();
    });
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  initializeSubscriptions() {
    this.attendanceTypeSub = this.optionsManagerService.attendanceTypeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: '', value: '--select--' });
        this._updateView.setInputProperty('attendance_type', 'options', tempOption);
      }
    });

    this.gradingTypeSub = this.optionsManagerService.gradingTypeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: '', value: '--select--' });
        this._updateView.setInputProperty('grading_type', 'options', tempOption);
      }
    });
  }

  submitVal(formVal: any): void {
    if (this.requiredCheck(formVal)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = {
        code: formVal.option_code,
        name: formVal.option_name,
        attendance_type_id: formVal.attendance_type,
        examination_grading_type_id: formVal.grading_type
      };
      this.optionsManagerService.updateOptionDetail(this.optionId, payload);
    }
  }

  setOptionDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'attendance_type') {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key].key);
        } else if (this._questionBase[i].key == 'grading_type') {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key].key);
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
    this.loading = false;
  }

  cancel() {
    this._router.navigate(['/main/examinations/exams/details/options/view']);
  }

  ngOnDestroy(): void {
    if (this.attendanceTypeSub) {
      this.attendanceTypeSub.unsubscribe();
    }
    if (this.gradingTypeSub) {
      this.gradingTypeSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.optionIdSub) {
      this.optionIdSub.unsubscribe();
    }
    if (this.optionDetailsSub) {
      this.optionDetailsSub.unsubscribe();
    }
  }
}
