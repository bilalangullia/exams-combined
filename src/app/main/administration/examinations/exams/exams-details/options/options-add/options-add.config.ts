import { DEFAULT_OPTIONS_EMPTY, DEFAULT_OPTIONS_YES_NO } from '../../../../../../../shared/shared.constants';

export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    controlType: 'text',
    type: 'string',
    visible: true,
    readonly: true
  },
  {
    key: 'subject',
    label: 'Subject',
    visible: true,
    controlType: 'dropdown',
    options: [...DEFAULT_OPTIONS_EMPTY],
    required: true
  },
  {
    key: 'option_code',
    label: 'Option Code',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },

  {
    key: 'option_name',
    label: 'Option Name',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'grading_type',
    label: 'Grading Type',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [...DEFAULT_OPTIONS_EMPTY]
  },
  {
    key: 'max_option_mark',
    label: 'Max Option Mark',
    visible: true,
    controlType: 'integer',
    type: 'number',
    min: 1,
    max: 1000,
    required: true
  },
  {
    key: 'attendance_type',
    label: 'Attendance Type',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [...DEFAULT_OPTIONS_EMPTY]
  },
  {
    key: 'carry_forward',
    label: 'Carry Forward',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [...DEFAULT_OPTIONS_YES_NO]
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
