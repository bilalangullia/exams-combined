export const file_INPUT={
    file_inputs:   {
   
           'key': 'fileinput_double_buttons',
           'label': 'Select File To Import',
           'visible': true,
           'required': true,
           'controlType': 'file-input',
           'type': 'file',
           'config': { 'infoText': [
              {
                  'text': 'Format Supported: xls, xlsx, ods, zip '
              },{
                  'text':'File size should not be larger than 512KB.'
              },{
                  'text':'Recommended Maximum Records: 2000'
              }
          ],
               'leftToolbar': true,
               'leftButton': [
                   {
                       'icon': 'kd-download',
                       'label': 'Download',
                       'callback': (): void => {
                           exportToExcel();
                       }
                   }
               ]
           }
       }
       
   }
   
 const exportToExcel= ()=> {
    
   var uri = '/exams/assets/excelformat/OpenEMIS_ Exams_Import_Exam_Options_Template.xlsx';
   var link = document.createElement("a");
   link.href = uri;
   link.download = 'OpenEMIS_ Exams_Import_Exam_Options_Template.xlsx';
   document.body.appendChild(link);
   link.click();
   document.body.removeChild(link);
 }