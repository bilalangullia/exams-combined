export const VIEWNODE_INPUT: Array<any> = [
  { key: 'academic_period', label: 'Academic Period', visible: true, controlType: 'text', type: 'string' },
  { key: 'examination', label: 'Examination', visible: true, controlType: 'text', type: 'string' },
  { key: 'subject', label: 'Subject', visible: true, controlType: 'textbox', type: 'string' },
  { key: 'option_code', label: 'Option Code', visible: true, controlType: 'text', type: 'string' },
  { key: 'option_name', label: 'Option Name', visible: true, controlType: 'text', type: 'string' },
  { key: 'grading_type', label: 'Grading Type', visible: true, controlType: 'text', type: 'string' },
  { key: 'attendance_type', label: 'Attendance Type', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_by', label: 'Modified By', visible: true, controlType: 'textbox', type: 'string' },
  { key: 'modified_on', label: 'Modified On', visible: true, controlType: 'textbox', type: 'string' },
  { key: 'created_by', label: 'Created By', visible: true, controlType: 'textbox', type: 'string' },
  { key: 'created_on', label: 'Created On', visible: true, controlType: 'textbox', type: 'string' }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
