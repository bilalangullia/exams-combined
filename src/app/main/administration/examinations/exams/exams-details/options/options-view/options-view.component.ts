import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent } from 'openemis-styleguide-lib';

import { IFilterItem, IModalConfig } from '../../../../../../../shared/shared.interfaces';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { OptionsManagerService } from '../options-manager.service';
import { VIEWNODE_INPUT } from './options-view.config';
import { SharedService } from '../../../../../../../shared/shared.service';
import { deleteSuccess, deleteFail, serverError } from '../../../../../../../shared/shared.toasters';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-options-view',
  templateUrl: './options-view.component.html',
  styleUrls: ['./options-view.component.css'],
  providers: [OptionsManagerService, KdModalEvent]
})
export class OptionsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  private optionId: IFilterItem;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private optionDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Option Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.deleteOption();
          setTimeout(() => {
            this.modalEvent.toggleClose();
          }, 0);
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public modalEvent: KdModalEvent,
    private sharedService: SharedService,
    private examsDetailsService: ExamsDetailsService,
    private optionsManagerService: OptionsManagerService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Examinations - Options', false);
    super.setToolbarMainBtns([
      { type: 'back', path: '/main/examinations/exams/details/options/list' },
      { type: 'edit', path: '/main/examinations/exams/details/options/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.modalEvent.toggleOpen();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (filters['examination'] && filters['academicPeriodData'] && filters['optionId']) {
        this.optionId = { id: filters['optionId']['id'], name: filters['optionId']['value'] };

        setTimeout(() => {
          this.optionDetailsSub = this.examsDetailsService
            .getOptionsDetails(filters['optionId']['id'])
            .subscribe((details) => {
              if (details['data']) {
                this.setDetails({
                  ...details['data'][0],
                  academic_period: filters['academicPeriodData']['id'],
                  examination: filters['examination']['id']
                });
              }
            });
        }, 100);
      }
    });
  }

  setDetails(data: any) {
    this._questionBase.forEach((question: any) => {
      this.api.setProperty(
        question['key'],
        'value',
        data[question['key']]
          ? data[question['key']]['value']
            ? data[question['key']].value
            : data[question['key']]
          : ''
      );
    });
  }

  deleteOption() {
    this.optionsManagerService.deleteOption(this.optionId['id']);
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.optionDetailsSub) {
      this.optionDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
