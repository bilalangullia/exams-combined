export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'subject',
    label: 'Subject',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'option_code',
    label: 'Option Code',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },

  {
    key: 'option_name',
    label: 'Option Name',
    visible: true,
    controlType: 'text',
    type: 'string',
    required: true
  },
  {
    key: 'grading_type',
    label: 'Grading Type',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [
      {
        key: '',
        value: '--select--'
      }
    ],
    events: true
  },
  {
    key: 'attendance_type',
    label: 'Attendance Type',
    visible: true,
    controlType: 'dropdown',
    required: true,
    options: [
      {
        key: '',
        value: '--select--'
      }
    ],
    events: true
  }
];
export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
