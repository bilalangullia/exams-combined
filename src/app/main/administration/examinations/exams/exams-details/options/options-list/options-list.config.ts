interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Subject?: Column;
  OptionCode?: Column;
  OptionName?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Subject: {
    headerName: 'Subject',
    field: 'subject',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  OptionCode: {
    headerName: 'Option Code',
    field: 'option_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  OptionName: {
    headerName: 'Option Name',
    field: 'option_name',
    sortable: true,
    filterable: false,
    filterValue: []
  }
};

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
