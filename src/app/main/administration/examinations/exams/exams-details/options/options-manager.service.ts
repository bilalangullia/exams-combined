import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../../shared/shared.service';
import { ExamsService } from '../../exams.service';
import { ExamsDetailsService } from '../exams-details.service';
import { saveSuccess, serverError, deleteSuccess, deleteFail } from '../../../../../../shared/shared.toasters';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class OptionsManagerService {
  private optionsList: any;
  public optionsViewDetails: any;
  private selectedOptionId: number;
  private attendanceType: any;
  private gradingType: any;
  private subjectOptions: Array<any> = [];

  public optionImportDetail: any;
  public optionsListChanged = new Subject();
  public optionsViewDetailsChanged = new Subject();
  public selectedOptionIdChanged = new Subject<number>();
  public attendanceTypeChanged = new Subject();
  public gradingTypeChanged = new Subject();
  public subjectOptionsChanged = new BehaviorSubject<Array<any>>([...this.subjectOptions]);

  constructor(
    private router: Router,
    private sharedService: SharedService,
    private examsService: ExamsService,
    private examsDetailsService: ExamsDetailsService
  ) {}

  /*** Set Exam ID when navigating away from list page -> view/edit pages ***/
  setOptionId(_rowData: any) {
    if (_rowData && _rowData.exam_id) {
      this.examsService.setExamId(_rowData.exam_id);
      this.selectedOptionId = this.examsService.getExamId();
      this.selectedOptionIdChanged.next(this.selectedOptionId);
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Option NotFound',
        body: 'Option Id not found please select valid id.',
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
      setTimeout(() => {
        this.router.navigate['main/examinations/exams/details/options/list'];
      }, 500);
    }
  }

  /*** LIST  PAGE | API  ***/
  getOptionsList(exam_id, start: number, end: number) {
    return this.examsDetailsService.getOptionsList(exam_id, start, end);
  }

  /*** VIEW/EDIT PAGE | Get option  details for populating the view fields. ***/
  getOptionsDetails(option_id) {
    this.examsDetailsService.getOptionsDetails(option_id).subscribe((res: any) => {
      if (res.data.length) {
        this.optionsViewDetails = res.data;
        this.optionsViewDetailsChanged.next([...this.optionsViewDetails]);
      } else {
        this.optionsViewDetails = [];
        this.optionsViewDetailsChanged.next([...this.optionsViewDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Option Details Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  /*** EDIT PAGE | ATTENDANCE TYPE  DROPDOWN ***/
  getAttendanceType(entities) {
    this.examsDetailsService.getAttendanceType(entities).subscribe((res: any) => {
      if (res.data.examination_attendance_types_id.length) {
        this.attendanceType = res.data.examination_attendance_types_id;
        this.attendanceTypeChanged.next([...this.attendanceType]);
      } else {
        this.attendanceType = [];
        this.attendanceTypeChanged.next([...this.attendanceType]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Attendance Type Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  /*** LIST PAGE | SEARCH  ***/
  getOptionsListSearch(examination_id, keyword, start: number, end: number) {
    return this.examsDetailsService.getOptionsListSearch(examination_id, keyword, start, end);
  }

  /*** EDIT PAGE | EXAM UPDATE  ***/
  updateOptionDetail(option_id, data) {
    this.examsDetailsService.updateOptionsDetail(option_id, data).subscribe(
      (res) => {
        let toasterConfig: any = {
          type: 'success',
          title: 'Success',
          body: 'Options details updated',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/examinations/exams/details/options']);
      },
      (err) => {
        let toasterConfig: any = {
          type: 'error',
          title: 'Failed',
          body: 'Error updating options details.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  /***  OPTIONS TAB | GRADING TYPE DROPDOWN  API ***/
  getGradingTypeDetail() {
    this.examsDetailsService.getGradingType().subscribe((res: any) => {
      if (res.data.length) {
        this.gradingType = res.data;
        this.gradingTypeChanged.next([...this.gradingType]);
      } else {
        this.gradingType = [];
        this.gradingTypeChanged.next([...this.gradingType]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No Grading Type Exists',
          body: 'Please select another values',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    });
  }

  getSubjectOptions(examinationId: number) {
    this.examsDetailsService.getSubjectOptions(examinationId).subscribe(
      (res) => {
        if (res && res['data']) {
          this.subjectOptions = [...res['data']];
          this.subjectOptionsChanged.next([...this.subjectOptions]);
        }
      },
      (err) => {
        this.subjectOptions = [];
        this.subjectOptionsChanged.next([...this.subjectOptions]);
      }
    );
  }

  addOption(payload: any) {
    this.examsDetailsService.addOption(payload).subscribe(
      (res: any) => {
        if (res) {
          this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/examinations/exams/details/options']);
          }, 500);
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  deleteOption(optionId: any) {
    this.examsDetailsService.deleteOption(optionId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster({ ...deleteSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/examination/exams/details/options']);
          }, 100);
        } else {
          this.sharedService.setToaster({ ...deleteFail, body: res['message'] });
        }
      },
      (err: HttpErrorResponse) => {
        if (err['status'] == 403) {
          this.sharedService.setToaster({ ...deleteFail, body: err['error']['message'] });
        } else {
          this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
        }
      }
    );
  }

  setOptionImportDetail(data) {
    this.optionImportDetail = data;
  }

  getOptionImportDetail() {
    return this.optionImportDetail;
  }
}
