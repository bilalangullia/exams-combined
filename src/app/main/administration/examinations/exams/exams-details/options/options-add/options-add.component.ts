import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../../shared/shared.toasters';
import { ExamsDetailsService, IFilter } from '../../exams-details.service';
import { OptionsManagerService } from '../options-manager.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './options-add.config';

@Component({
  selector: 'app-options-add',
  templateUrl: './options-add.component.html',
  styleUrls: ['./options-add.component.css'],
  providers: [OptionsManagerService]
})
export class OptionsAddComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  public examinationId: any;
  public defaultList = {};

  private currentFilters: IFilter = null;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private subjectSub: Subscription;
  public attendanceTypeSub: Subscription;
  public gradingTypeSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    public optionsManagerService: OptionsManagerService,
    private examsDetailsService: ExamsDetailsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Examinations - Options', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.filterValuesSub = this.examsDetailsService.filterValuesChanged.subscribe((filters: IFilter) => {
      if (filters && filters['academicPeriodData'] && filters['examination']) {
        this.currentFilters = { ...filters };

        this.optionsManagerService.getAttendanceType('attendanceTypes');
        this.optionsManagerService.getGradingTypeDetail();
        this.optionsManagerService.getSubjectOptions(this.currentFilters['examination']['id']);
      }
    });

    setTimeout(() => {
      this.subjectSub = this.optionsManagerService.subjectOptionsChanged.subscribe((options: Array<any>) => {
        if (options) {
          this.setDropdownOptions('subject', options);
        }
      });

      this.gradingTypeSub = this.optionsManagerService.gradingTypeChanged.subscribe((options: any) => {
        if (options) {
          this.setDropdownOptions('grading_type', options);
        }
      });

      this.attendanceTypeSub = this.optionsManagerService.attendanceTypeChanged.subscribe((options: any) => {
        if (options) {
          this.setDropdownOptions('attendance_type', options);
        }
      });
    }, 100);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    options = data.map((item) => ({ key: item['id'], value: item['name'] }));
    options.unshift({ key: null, value: '-- Select --' });

    this.api.setProperty(key, 'options', [...options]);

    setTimeout(() => {
      this.setDetails();
    }, 0);
  }

  setDetails() {
    this._questionBase.forEach((question) => {
      if (question['key'] === 'academic_period') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['academicPeriodData']['name']);
      } else if (question['key'] === 'examination') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['examination']['name']);
      }
    });
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form: any): void {
    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        code: form.option_code,
        name: form.option_name,
        attendance_type_id: form.attendance_type,
        examination_subject_id: this.currentFilters['examination']['id'],
        examination_grading_type_id: form.grading_type,
        max_opt_mark: form['max_option_mark'],
        carry_forward: form['carry_forward']
      };
      this.optionsManagerService.addOption(payload);
    }
  }

  cancel() {
    this._router.navigate(['main/examinations/exams/details/options']);
  }

  ngOnDestroy(): void {
    if (this.attendanceTypeSub) {
      this.attendanceTypeSub.unsubscribe();
    }
    if (this.gradingTypeSub) {
      this.gradingTypeSub.unsubscribe();
    }
    if (this.subjectSub) {
      this.subjectSub.unsubscribe();
    }
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
