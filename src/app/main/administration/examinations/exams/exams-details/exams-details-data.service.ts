import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class ExamsDetailsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers };
    }
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };

  /**  EXAMS | VIEW  API  **/
  getExamsView(id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.examDetail}/${id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | SUBJECTS TAB | ADD **/
  addSubject(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/subject/add-examinationsubject
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${urls.add}-${urls.examination}${urls.subject}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | SUBJECTS TAB  LIST | API  **/
  getSubjectsList(examination_id: string, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${urls.subjectList}/${examination_id}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | SUBJECTS TAB  LIST | SEARCH | API  **/
  getSubjectsListSearch(examinationId: string, keyword: string, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${urls.subjectList}/${examinationId}?keyword=${keyword}&start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMINATIONS |EXAMS |  SUBJECT TAB  | VIEW  API**/
  viewSubject(subject_id, examination_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${urls.subjectView}/${subject_id}?examination_id=${examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS |  SUBJECT TAB  | EDIT | SUBJECT EDIT API  **/
  editSubject(subject_id, examination_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${urls.subjectEdit}/${subject_id}?examination_id=${examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | SUBJECT TAB | EDUCATION SUBJECT DROPDOWN | API  **/
  getEducationSubjectDropdown() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${urls.educationSubject}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMINATIONS |EXAMS |  SUBJECT TAB  | EDIT | SUBJECT EDIT | SUBMIT API  **/
  updateSubjectDetail(subjectId, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${urls.subjectUpadte}/${subjectId}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | SUBJECTS TAB | DELETE **/
  deleteSubject(subjectId: number) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/subject/43/destroy
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/${subjectId}/${urls.destroy}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Exams | Options Tab | Add */
  addOption(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/option/add-option
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.option}/${urls.add}-${urls.option}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | OPTIONS TAB | LIST API**/
  getOptionsList(exam_id, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/option/3/option-list?start=0&end=5
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.option}/${exam_id}/${urls.optionList}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Exams | Options Tab | Add | Subjects Dropdown Options */
  getSubjectOptions(examinationId: number) {
    // http://openemis.n2.iworklab.com/api/registration/candidate/dropdown/3/examination-subject
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${examinationId}/${urls.examination}-${urls.subject}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | OPTIONS TAB | ATTENDANCE TYPE DROPDOWN  API**/
  getAttendanceTypeDetails(attendanceTypes) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}?entities=${attendanceTypes}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | OPTIONS TAB | GRADING TYPE DROPDOWN  API**/
  getGradingTypeOptions() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.examinationGradingType}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | OPTIONS TAB | EDIT | VIEW  API  **/
  getExaminationsOptionsDetails(option_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.option}/${option_id}/${urls.optionDetails}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | OPTIONS TAB | EDIT | OPTION EDIT SUBMIT API  **/
  updateOptionsDetail(option_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.option}/${urls.update}/${option_id}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  OPTIONS  TAB |  EDIT SEARCH | API  **/
  getOptionsSearch(examination_id, keyword, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/option/3/option-list?start=0&end=5&keyword=art
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.option}/${examination_id}/${urls.optionList}?keyword=${keyword}&start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Options Tab | Delete */
  deleteOption(optionId: string) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/option/40/destroy
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.option}/${optionId}/${urls.destroy}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMINATIONS  | EXAMS | COMPONENTS TAB| LIST API**/
  getComponentsList(exam_id: number, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentList}/${exam_id}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMINATIONS  | EXAMS | COMPONENTS TAB| SEARCH API**/
  getComponentsListSearch(exam_id: number, keyword: string, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentList}/${exam_id}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | COMPONENTS TAB | EDIT   API  **/
  editComponentsDetails(component_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentEdit}/${component_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | COMPONENTS TAB | EDIT   API  **/
  viewComponentsDetails(component_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentView}/${component_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | COMPONENTS TAB | COMPONENT TYPE  DROPDOWN API  **/
  getComponentTypeDropDown() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentType}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | COMPONENTS TAB | MARK TYPE  DROPDOWN API  **/
  getMarkTypeDropDown() {
    return this.httpClient.get(`${environment.baseUrl}/marks/${urls.component}/${urls.markType}`, this.setHeader());
  }

  /**  EXAMS | COMPONENTS TAB | SUBJECT DROPDOWN API  **/
  getSubjectsDropDown() {
    //openemis.n2.iworklab.com/api/registration/candidate/dropdown/3/examination-subject
    http: return this.httpClient.get(`${environment.baseUrl}/${urls.dropdown}/${urls.subjects}`, this.setHeader());
  }

  /**  EXAMS | COMPONENTS TAB | OPTION DROPDOWN API  **/
  getOptionsDropDown() {
    return this.httpClient.get(`${environment.baseUrl}/${urls.dropdown}/${urls.options}`, this.setHeader());
  }

  /**  EXAMS | COMPONENTS TAB | EDIT | COMPONENTS EDIT API  **/
  updateComponentsDetail(component_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentUpdate}/${component_id}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EXAMS | COMPONENTS TAB | VIEW | DELETE API ***/
  deleteComponentDetails(componentId) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${componentId}/destroy`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | COMPONENTS TAB | COMPONENTS ADD API  **/
  addComponentsDetail(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/${urls.componentAdd}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Exams | Items Tab | Add */
  addItem(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/items/add-item
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.items}/${urls.add}-${urls.item}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getOptionOptions(subjectId) {
    // http://openemis.n2.iworklab.com/api/registration/candidate/dropdown/1/examination-options
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${subjectId}/${urls.examination}-${urls.option}s`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getComponentOptions(optionId) {
    // http://openemis.n2.iworklab.com/api/registration/candidate/dropdown/1/examination-components
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${optionId}/${urls.examination}-${urls.component}s`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getItemOptions() {
    // http://openemis.n2.iworklab.com/api/registration/candidate/dropdown/examination-itemsdropdown
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.examination}-${urls.item}s${urls.dropdown}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMINATIONS  | EXAMS | ITEMS TAB| LIST API**/
  getItemsList(exam_id, start, end) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/items/3/items-list?start=0&end=5
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.items}/${exam_id}/${urls.itemsList}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | ITEMS TAB | EDIT | VIEW  API  **/
  getItemsDetails(item_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.items}/${item_id}/${urls.itemsDetail}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | ITEMS TAB | EDIT | ITEM EDIT SUBMIT API  **/
  updateItemsDetail(item_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.items}/${item_id}/${urls.update}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  EXAMS | ITEMS TAB | EDIT | ITEM TAB DROPDOWN API  **/
  getItemDropDown() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.itemDropDown}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**import services EXAM | ITEM | SUBJECT | OPTION | COMPONENTS */
  optionImport(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.option}/import-option`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  itemImport(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.items}/import-item`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  componentsImport(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.component}/import`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  subjectImport(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.subject}/import`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
  /**  EXAMS | ITEMS  TAB |   SEARCH | API  **/
  getItemsSearch(examination_id, keyword, start, end) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/items/3/items-list?start=0&end=5&keyword=book
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.items}/${examination_id}/${urls.itemsList}?keyword=${keyword}&start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Exams | Items Tab | Delete */
  deleteItem(itemId: string) {
    // http://openemis.n2.iworklab.com/api/administration/examinations/items/destroy/1
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.examinations}/${urls.items}/${urls.destroy}/${itemId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
}
