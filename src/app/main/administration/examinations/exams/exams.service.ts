import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import { ExamsDataService } from './exams-data.service';
import { IFilterItem } from '../../../../shared/shared.interfaces';

@Injectable()
export class ExamsService {
  private dropdownOptions = {};
  public dropdownOptionsChanged = new Subject<any>();

  private examinationId: number;
  public examinationIdChanged = new BehaviorSubject<number>(this.examinationId);
  private examinationValue: string;
  public examinationValueChanged = new BehaviorSubject<string>(this.examinationValue);
  /* Adding a temp examination to hold key-values */
  private examination: IFilterItem = null;
  public examinationChanged = new BehaviorSubject<IFilterItem>(this.examination);
  private academicPeriod: string;
  public academicPeriodChanged = new BehaviorSubject<string>(this.academicPeriod);
  /* Adding a temp academicPeriod to hold key-values */
  private academicPeriodData: IFilterItem = null;
  public academicPeriodDataChanged = new BehaviorSubject<IFilterItem>({ ...this.academicPeriodData });

  public examId: number;
  public subjectId: number;
  public optionId: number;
  public componentId: number;

  constructor(private examsDataService: ExamsDataService) {}

  setExaminationId(id: number): void {
    this.examinationId = id;
    this.examinationIdChanged.next(this.examinationId);
  }

  setExaminationValue(value: string): void {
    this.examinationValue = value;
    this.examinationValueChanged.next(this.examinationValue);
  }

  /* Adding a temp examination to hold key-values */
  setExamination(examination: IFilterItem) {
    this.examination = { ...examination };
    this.examinationChanged.next({ ...this.examination });
  }

  setAcademicPeriod(academicPeriod: string) {
    this.academicPeriod = academicPeriod;
    this.academicPeriodChanged.next(this.academicPeriod);
  }

  /* Adding a temp academicPeriod to hold key-values */
  setAcademicPeriodData(academicPeriodData: IFilterItem) {
    this.academicPeriodData = academicPeriodData;
    this.academicPeriodDataChanged.next({ ...this.academicPeriodData });
  }

  setExamId(id: number) {
    this.examId = id;
    this.dropdownOptions['examination_id'] = this.examId;
    this.dropdownOptionsChanged.next({ ...this.dropdownOptions });
  }

  getExamId() {
    return this.examId;
  }

  setSubjectId(id: number) {
    this.subjectId = id;
    this.dropdownOptions['subject_id'];
  }

  getSubjectId() {
    return this.subjectId;
  }

  setOptionId(id: number) {
    this.optionId = id;
  }

  getOptionId() {
    return this.optionId;
  }

  setComponentId(id: number) {
    this.componentId = id;
  }

  getComponentId() {
    return this.componentId;
  }

  /*   ***************************************************************************************************** */
  /*** EXAMS TAB APIs START ***/

  /*** LIST PAGE | List Data ***/
  getExamsList(start?: number, end?: number) {
    return this.examsDataService.getExamsList(start, end);
  }

  /*** EXAMS  TAB | List Data | SEARCH***/
  getExamsListSearch(keyword: string, start?: number, end?: number) {
    return this.examsDataService.getExamsListSearch(keyword, start, end);
  }

  /*** EXAMS TAB | EDIT PAGE | EDUCATION PROGRAMME DROPDOWN ***/
  getEducationProgramme() {
    return this.examsDataService.getEducationProgrammeDropdown();
  }

  /*** EXAMS TAB | EDIT PAGE | EDUCATION GRADE DROPDOWN ***/
  getEducationGrade(education_programme_id) {
    return this.examsDataService.getEducationGradeDropdown(education_programme_id);
  }

  /*** EXAMS TAB | EDIT PAGE | EXAM TYPE DROPDOWN ***/
  getExamType() {
    return this.examsDataService.getExamTypeDropdown();
  }

  /*** EXAMS TAB | EDIT PAGE | EXAM SESSION DROPDOWN ***/
  getExamSession() {
    return this.examsDataService.getExamSessionDropdown();
  }

  /** ACADEMIC PERIOD DROPDOWN **/
  getOptionsAcademicPeriod(data) {
    return this.examsDataService.getOptionsAcademicPeriod(data);
  }

  /** ACADEMIC PERIOD DROPDOWN **/
  getAttendanceTypeDropdown(data) {
    return this.examsDataService.getAttendanceTypeDetails(data);
  }

  /*** EXAMS TAB | EDIT PAGE | UPDATE API ***/
  updateExamDetail(exam_id, data) {
    return this.examsDataService.updateExamDetail(exam_id, data);
  }

  /*** EXAMS TAB | EDIT PAGE | DELETE API ***/
  deleteExamDetails(exam_id) {
    return this.examsDataService.deleteExamDetails(exam_id);
  }

  /*** EXAMS TAB | ADD API ***/
  addExamDetails(data) {
    return this.examsDataService.addExamDetails(data);
  }

  /*** EXAMS TAB | COPY API ***/
  copyExamDetails(data) {
    return this.examsDataService.copyExamDetails(data);
  }

  /*** EXAMS TAB | COPY | EXAM DROPDOWN API ***/
  getExamOptions(examination_id) {
    return this.examsDataService.getOptionsExam(examination_id);
  }

  /*** EXAMS TAB APIs END ***/

  /*   ***************************************************************************************************** */
}
