import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      { path: 'system-setup', loadChildren: './system-setup/system-setup.module#SystemSetupModule' },
      { path: 'security', loadChildren: './security/security.module#SecurityModule' },
      { path: 'examinations', loadChildren: './examinations/examinations.module#ExaminationsModule' }
    ]
  }
];

@NgModule({ imports: [RouterModule.forChild(routes)], exports: [RouterModule] })
export class AdministrationRoutingModule {}
