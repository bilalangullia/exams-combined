import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class SystemConfigurationsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /*** LIST | System Configuration dropdown  ***/
  getSystemConfigurationDropDown() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.systemConfiguration}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /***  LIST API ***/
  getList(systemConfigurationId: number, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.systemConfiguration}/${systemConfigurationId}/list?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(systemConfigurationId: number, keyword: string, start?: number, end?: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${systemConfigurationId}/list?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getSystemConfigurationDetails(systemConfiguration_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.systemConfiguration}/${systemConfiguration_id}/view`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW | DELETE API **/
  deleteSystemConfigurationDetails(system_id) {
    return;
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${system_id}/${urls.systemDelete}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateSystemConfigurationDetails(systemConfiguration_id, data) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/system-configuration/1/update
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.systemConfiguration}/${systemConfiguration_id}/update`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
