import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SystemConfigurationsEntryComponent } from './system-configurations-entry/system-configurations-entry.component';
import { SystemConfigurationsListComponent } from './system-configurations-entry/system-configurations-list/system-configurations-list.component';
import { SystemConfigurationsEditComponent } from './system-configurations-entry/system-configurations-edit/system-configurations-edit.component';
import { SystemConfigurationsViewComponent } from './system-configurations-entry/system-configurations-view/system-configurations-view.component';

const routes: Routes = [
  {
    path: '',
    component: SystemConfigurationsEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: SystemConfigurationsListComponent },
      { path: 'edit', component: SystemConfigurationsEditComponent },
      { path: 'view', component: SystemConfigurationsViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemConfigurationsRoutingModule {}
