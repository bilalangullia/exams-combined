import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { SystemConfigurationsRoutingModule } from './system-configurations-routing.module';
import { SystemConfigurationsEntryComponent } from './system-configurations-entry/system-configurations-entry.component';
import { SystemConfigurationsListComponent } from './system-configurations-entry/system-configurations-list/system-configurations-list.component';
import { SystemConfigurationsViewComponent } from './system-configurations-entry/system-configurations-view/system-configurations-view.component';
import { SystemConfigurationsEditComponent } from './system-configurations-entry/system-configurations-edit/system-configurations-edit.component';
import { SystemConfigurationsService } from './system-configurations.service';
import { SystemConfigurationsDataService } from './system-configurations-data.service';

@NgModule({
  imports: [CommonModule, SystemConfigurationsRoutingModule, SharedModule],
  declarations: [
    SystemConfigurationsEntryComponent,
    SystemConfigurationsListComponent,
    SystemConfigurationsViewComponent,
    SystemConfigurationsEditComponent
  ],
  providers: [SystemConfigurationsService, SystemConfigurationsDataService]
})
export class SystemConfigurationsModule {}
