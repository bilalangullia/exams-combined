export interface IFilters {
  system_configuration?: { key: number; value: string };
}

export interface IUpdatePayloadSysConfig {
  prefix_value?: string;
  value?: string;
}
