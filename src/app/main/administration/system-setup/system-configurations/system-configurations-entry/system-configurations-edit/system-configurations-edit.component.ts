import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView, ITableConfig, ITableApi } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError, missingFieldsError } from '../../../../../../shared/shared.toasters';
import { SystemConfigurationsService } from '../../system-configurations.service';
import { IUpdatePayloadSysConfig } from '../../system-configurations.interfaces';
import { QUESTION_BASE, FORM_BUTTONS, GRID_ID, TABLE_COLUMNS } from './system-configurations-edit.config';

@Component({
  selector: 'app-system-configurations-edit',
  templateUrl: './system-configurations-edit.component.html',
  styleUrls: ['./system-configurations-edit.component.css']
})
export class SystemConfigurationsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;

  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public showForm: boolean = true;
  public showTable: boolean = false;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public tableApi: ITableApi = {};
  public tableConfig: ITableConfig = {
    id: GRID_ID,
    rowIdKey: 'id',
    loadType: 'normal',
    gridHeight: 200
  };
  public tableColumn: Array<any> = [...TABLE_COLUMNS];
  public tableRow: Array<any> = [{ keyword: 'test', remarks: 'tests' }];

  private id: string = null;
  private prefix_value: string = null;

  /* Subscriptions */
  private idSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private systemConfigurationService: SystemConfigurationsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('System Configurations', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;
    this.showTable = true;

    this.idSub = this.systemConfigurationService.systemConfigurationIdChanged.subscribe((id: string) => {
      if (!id) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/system-configurations']);
        }, 100);
      } else {
        this.id = id;
        this.systemConfigurationService.getSystemConfigurationDetails(id);

        setTimeout(() => {
          this.detailsSub = this.systemConfigurationService.systemConfigurationViewDetailsChanged.subscribe(
            (details: any) => {
              if (details) {
                this.setDetails(details[0]);
              }
            }
          );
        }, 100);
      }
    });
  }

  setDetails(details: any) {
    this.prefix_value = details['code']
      ? details['code']
          .split('_')
          .map((item) => `\$\{${item}\}`)
          .join('')
      : '';

    if (Object.keys(details).length) {
      this._questionBase.forEach((question: any) => {
        if (question['key'] == 'value') {
          this.api.setProperty(question['key'], 'row', [{ prefix_value: this.prefix_value, id: '1' }]);
          setTimeout(() => {
            if (details['value'] && details['value'] == 'Enable') {
              this.api.setProperty(question['key'], 'value', ['1']);
            }
          }, 0);
        } else {
          this.api.setProperty(question['key'], 'value', details[question['key']]);
        }
      });
    }

    let keywords: Array<any>;

    setTimeout(() => {
      this.showTable = true;
      // this.tableApi.general.addRows([...keywords]);
    }, 100);
  }

  requiredCheck(form: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This field is required.']);
      }
    });
    return hasError;
  }

  submit(event) {
    let form: IUpdatePayloadSysConfig = {
      value: event['value'] && event['value'].length ? '1' : '0',
      prefix_value: this.prefix_value
    };

    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload: IUpdatePayloadSysConfig = { ...form };

      this.systemConfigurationService.updateSystemConfigurationDetails(this.id, {
        ...payload
      });
    }
  }

  cancel() {
    this._router.navigate(['main/system-setup/system-configurations/view']);
  }

  ngOnDestroy() {
    if (this.idSub) {
      this.idSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
