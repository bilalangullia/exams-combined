import { ITableColumn } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array<any> = [
  {
    key: 'type',
    label: 'Type',
    visible: true,
    controlType: 'text',
    // required: true,
    readonly: true
  },
  {
    key: 'label',
    label: 'Label',
    visible: true,
    order: 1,
    controlType: 'text',
    // options: [{ key: 'null', value: '--Select--' }],
    // events: true,
    // required: true,
    readonly: true
  },
  {
    key: 'value',
    label: 'Value',
    visible: true,
    controlType: 'table',
    required: true,
    row: [],
    column: [
      // {
      //   headerName: 'Enable',
      //   field: 'enable',
      //   sortable: false,
      //   filterable: true,
      //   visible: true,
      // },
      {
        headerName: 'Prefix Value',
        field: 'prefix_value',
        sortable: false,
        filterable: false,
        visible: true
      }
    ],
    config: {
      id: 'value-table',
      // rowIdkey: 'id',
      gridHeight: 150,
      loadType: 'normal',
      selection: { type: 'all', returnKey: ['prefix_value'] },
      click: { type: 'selection' }
    }
  }
];

export const GRID_ID: string = 'remarks-table';

export const TABLE_COLUMNS: Array<ITableColumn> = [
  { headerName: 'Keyword', field: 'keyword', filterable: true },
  { headerName: 'Remarks', field: 'remarks', filterable: true }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
