import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter,
  IPageheaderApi,
  IPageheaderConfig,
  ITableDatasourceParams,
  KdTableDatasourceEvent
} from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { SystemConfigurationsService } from '../../system-configurations.service';
import { IFilters } from '../../system-configurations.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './system-configurations-list.config';

@Component({
  selector: 'app-system-configurations-list',
  templateUrl: './system-configurations-list.component.html',
  styleUrls: ['./system-configurations-list.component.css'],
  providers: [KdTableEvent]
})
export class SystemConfigurationsListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('filters') filter: KdFilter;
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = {};

  public filterInputs = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'cycleList';
  readonly PAGESIZE: number = 20;

  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public currentFilters: IFilters;
  public showTable: boolean = false;
  public _tableEvent;
  private currentPage: number = null;

  /* Subscriptions */
  private tableEventSub: Subscription;
  private systemConfigurationFilterSub: Subscription;
  private currentFiltersSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    private systemConfigurationService: SystemConfigurationsService
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Visible,
    TABLECOLUMN.Type,
    TABLECOLUMN.Label,
    TABLECOLUMN.Value
  ];
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.systemConfigurationService.setsystemConfigurationId(_rowNode['data']['id']);
            this.router.navigate(['main/system-setup/system-configurations/view']);
          }
        }
      ]
    }
  };

  ngOnInit() {
    this.loading = false;
    super.setPageTitle('System - Configurations', false);
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.systemConfigurationService.resetFilters();
    this.systemConfigurationService.getFilterSystemConfiguration();
    this.systemConfigurationFilterSub = this.systemConfigurationService.filterSystemConfigurationChanged.subscribe(
      (options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('system_configuration', options);
        }
      }
    );

    this.currentFiltersSub = this.systemConfigurationService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (filters && filters['system_configuration'] && filters['system_configuration']['key']) {
        this.currentFilters = { ...filters };
        this.showTable = false;
        this.systemConfigurationService.getTotalRecords(filters['system_configuration']['key'], 0, 1).subscribe(
          (res: any) => {
            this.showTable = true;
            if (res && res['data'] && res['data']['total']) {
              this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
              this.showTable = true;
            } else {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          },
          (err) => {
            this.tableConfig.paginationConfig.total = 0;
            this.showTable = false;
          }
        );
      }
    });

    setTimeout(() => {
      this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
        if (event instanceof KdTableDatasourceEvent) {
          this._tableEvent = event;
          if (event['rowParams']['endRow'] && !event['rowParams']['sortModel'].length) {
            this.currentPage = event['rowParams']['endRow'] / this.PAGESIZE;
            this.fetchList(event);
          } else if (event['rowParams']['sortModel'].length) {
            this.fetchList(event);
          } else if (event['rowParams']['filterModel'].length) {
            this.fetchList(event);
          }
        }
      });
    }, 500);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<{ key: number; value: string }> = [];
    options = data.map((item) => ({ key: item['id'], value: item['type'] }));
    this.filter.setInputProperty(key, 'options', options);
    setTimeout(() => {
      this.filter.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  fetchList(event) {
    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    if (
      this.currentFilters &&
      this.currentFilters['system_configuration'] &&
      this.currentFilters['system_configuration']['key']
    ) {
      this.systemConfigurationService
        .getList(this.currentFilters['system_configuration']['key'], event.rowParams.startRow, event.rowParams.endRow)
        .toPromise()
        .then(
          (res: any) => {
            if (res && res['data'] && res['data']['response']) {
              list = res['data']['response'];
              total = res['data']['total'];
              list.forEach((item) => {
                item['visible'] = item['visible'] == 1 ? 'Yes' : 'No';
              });
              this.tableColumns.forEach((column, colIndex) => {
                let filterValues: Array<any> = [];
                list.forEach((item) => {
                  if (item[column['field']] && !filterValues.includes(item[column['field']])) {
                    filterValues.push(item[column['field']]);
                  }
                });
                column['filterValue'] = filterValues;
              });
              dataSourceParams = { rows: list, total };
            } else {
              list = [];
              total = 0;
              dataSourceParams = { rows: list, total };
            }
            event.subscriber.next(dataSourceParams);
            event.subscriber.complete();
          },
          (err) => {
            list = [];
            total = 0;
            dataSourceParams = { rows: list, total };
            event.subscriber.next(dataSourceParams);
            event.subscriber.complete();
          }
        );
    }
  }

  searchList(_tableEvent, keyword) {
    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    this.systemConfigurationService
      .searchList(
        this.currentFilters['system_configuration']['key'],
        keyword,
        _tableEvent.rowParams.startRow,
        _tableEvent.rowParams.endRow
      )
      .toPromise()
      .then(
        (res: any) => {
          if (res && res['data'] && res['data']['response']) {
            list = res['data']['response'];
            total = res['data']['total'];
            this.tableColumns.forEach((column, colIndex) => {
              let filterValues: Array<any> = [];
              list.forEach((item) => {
                if (item[column['field']] && !filterValues.includes(item[column['field']])) {
                  filterValues.push(item[column['field']]);
                }
              });
              column['filterValue'] = filterValues;
            });
            dataSourceParams = { rows: list, total };
          } else {
            list = [];
            total = 0;
            dataSourceParams = { rows: list, total };
          }
          _tableEvent.subscriber.next(dataSourceParams);
          _tableEvent.subscriber.complete();
        },
        (err) => {
          list = [];
          total = 0;
          dataSourceParams = { rows: list, total };
          _tableEvent.subscriber.next(dataSourceParams);
          _tableEvent.subscriber.complete();
        }
      );
  }

  detectValue(event) {
    let filter: IFilters;
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj) {
      filter = { [event['key']]: filterObj };
      this.systemConfigurationService.setCurrentFilters(filter);
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.systemConfigurationFilterSub) {
      this.systemConfigurationFilterSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
