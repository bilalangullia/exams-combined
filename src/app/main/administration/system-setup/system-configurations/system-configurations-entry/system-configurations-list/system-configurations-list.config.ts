interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Visible?: Column;
  Type?: Column;
  Label: Column;
  Value: Column;
}

export const TABLECOLUMN: ListColumn = {
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Type: {
    headerName: 'Type',
    field: 'type',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Label: {
    headerName: 'Label',
    field: 'label',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Value: {
    headerName: 'Value',
    field: 'value',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};
export const FILTER_INPUTS: Array<any> = [
  {
    key: 'system_configuration',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--select--'
      }
    ],
    events: true
  }
];
