import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { VIEWNODE_INPUT, IModalConfig } from './system-configurations-view.config';
import { SystemConfigurationsService } from '../../system-configurations.service';

@Component({
  selector: 'app-system-configurations-view',
  templateUrl: './system-configurations-view.component.html',
  styleUrls: ['./system-configurations-view.component.css']
})
export class SystemConfigurationsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public systemConfigurationId: any;

  /* Subscriptions */
  private systemConfigurationIdSub: Subscription;
  private systemConfigurationDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete System Configuration Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          // this.feeService.deleteFeeDetail(this.feeId).subscribe(
          //   (res: any) => {
          //     if (res.message) {
          //       this._modelEvent.toggleClose();
          //       // let toasterConfig: any = {
          //       //   type: 'success',
          //       //   title: res.message,
          //       //   showCloseButton: true,
          //       //   tapToDismiss: true,
          //       //   timeout: 2000
          //       // };
          //       // this.sharedService.setToaster(toasterConfig);
          //       this._router.navigate(['main/examinations/systems-view/list']);
          //     }
          //   },
          //   (err) => {
          //     this._modelEvent.toggleClose();
          //     let toasterConfig: any = {
          //       type: 'error',
          //       title: 'Fee Detail can not be deleted because its link to another data ',
          //       showCloseButton: true,
          //       tapToDismiss: true,
          //       timeout: 3000
          //     };
          //     this._kdalert.error(toasterConfig);
          //   }
          // );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _modelEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private SystemConfigurationsService: SystemConfigurationsService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('System Configurations', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/system-configurations/list' },
      { type: 'edit', path: 'main/system-setup/system-configurations/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  open() {
    this._modelEvent.toggleOpen();
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.systemConfigurationIdSub = this.SystemConfigurationsService.systemConfigurationIdChanged.subscribe((data) => {
      if (data) {
        this.systemConfigurationId = data;
        this.SystemConfigurationsService.getSystemConfigurationDetails(this.systemConfigurationId);
        this.systemConfigurationDetailsSub = this.SystemConfigurationsService.systemConfigurationViewDetailsChanged.subscribe(
          (data: any) => {
            if (data) {
              this.setsystemConfigurationDetails(data[0]);
            }
          }
        );
      } else {
        this._router.navigate(['main/system-setup/system-configurations']);
      }
    });
  }

  setsystemConfigurationDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !details[this._questionBase[i].key]
              ? ''
              : details[this._questionBase[i].key].value
              ? details[this._questionBase[i].key].value
              : details[this._questionBase[i].key]
          );
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.systemConfigurationIdSub) {
      this.systemConfigurationIdSub.unsubscribe();
    }
    if (this.systemConfigurationDetailsSub) {
      this.systemConfigurationDetailsSub.unsubscribe();
    }
    this.SystemConfigurationsService.resetDetails();
    super.destroyPageBaseSub();
  }
}
