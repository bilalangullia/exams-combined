import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';

import { SharedService } from '../../../../shared/shared.service';
import { SystemConfigurationsDataService } from './system-configurations-data.service';
import { IFilters } from './system-configurations.interfaces';
import { saveSuccess, saveFail, serverError } from '../../../../shared/shared.toasters';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class SystemConfigurationsService {
  private filterSystemConfiguration: any;
  private currentFilters: any = {};
  private systemConfigurationId: string = null;
  private systemConfigurationViewDetails: any = [];

  public filterSystemConfigurationChanged = new Subject();
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  public systemConfigurationIdChanged = new BehaviorSubject<string>(null);
  public systemConfigurationViewDetailsChanged = new BehaviorSubject<any>(this.systemConfigurationViewDetails);

  constructor(
    private router: Router,
    private systemConfigurationsDataService: SystemConfigurationsDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | FILTERS | getSystemConfigurationDropDown***/
  getFilterSystemConfiguration() {
    this.systemConfigurationsDataService.getSystemConfigurationDropDown().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.filterSystemConfiguration = res.data;
        this.filterSystemConfigurationChanged.next([...this.filterSystemConfiguration]);
      } else {
        this.filterSystemConfiguration = [];
        this.filterSystemConfigurationChanged.next([...this.filterSystemConfiguration]);
      }
      (err) => {
        this.filterSystemConfiguration = [];
        this.filterSystemConfigurationChanged.next([...this.filterSystemConfiguration]);
      };
    });
  }

  /*** Set Current filters ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }

  /*** LIST PAGE | List Data get total Records ***/
  getTotalRecords(educationLevelId: number, start?: number, end?: number) {
    return this.systemConfigurationsDataService.getList(educationLevelId, start, end);
  }

  /*** LIST PAGE | List Data ***/
  getList(systemConfigurationId: number, start?: number, end?: number) {
    return this.systemConfigurationsDataService.getList(systemConfigurationId, start, end);
  }

  /*** List Data | SEARCH***/
  searchList(systemConfigurationId: number, keyword: string, start?: number, end?: number) {
    return this.systemConfigurationsDataService.getListSearch(systemConfigurationId, keyword, start, end);
  }

  /*** List Page | Set systemConfiguration Id***/
  setsystemConfigurationId(systemConfigurationId: string) {
    this.systemConfigurationId = systemConfigurationId;
    this.systemConfigurationIdChanged.next(this.systemConfigurationId);
  }

  /*** VIEW PAGE | Details Data ***/
  getSystemConfigurationDetails(systemConfiguration_id) {
    this.systemConfigurationsDataService.getSystemConfigurationDetails(systemConfiguration_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.systemConfigurationViewDetails = res.data;
          this.systemConfigurationViewDetailsChanged.next([...this.systemConfigurationViewDetails]);
        } else {
          this.systemConfigurationViewDetails = [];
          this.systemConfigurationViewDetailsChanged.next([...this.systemConfigurationViewDetails]);
          let toasterConfig: any = {
            type: 'error',
            title: 'No details found',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      },
      (err) => {
        this.systemConfigurationViewDetails = [];
        this.systemConfigurationViewDetailsChanged.next([...this.systemConfigurationViewDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No details found',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  resetDetails() {
    this.systemConfigurationViewDetails = null;
    this.systemConfigurationViewDetailsChanged.next(null);
  }

  /*** EDIT PAGE SUBMIT ***/
  updateSystemConfigurationDetails(systemConfiguration_id, payload) {
    this.systemConfigurationsDataService.updateSystemConfigurationDetails(systemConfiguration_id, payload).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/system-configurations']);
          }, 100);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  /*** DELETE ***/
  deleteSystemConfigurationDetails(systemConfiguration_id) {
    return this.systemConfigurationsDataService.deleteSystemConfigurationDetails(systemConfiguration_id);
  }
}
