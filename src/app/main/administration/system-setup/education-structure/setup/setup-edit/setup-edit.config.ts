export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'visible',
    label: 'Visible',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: 'null',
        value: '--Select'
      }
    ],
    events: true,
    required: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
