import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { SetupService } from '../setup.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './setup-edit.config';

@Component({
  selector: 'app-setup-edit',
  templateUrl: './setup-edit.component.html',
  styleUrls: ['./setup-edit.component.css']
})
export class SetupEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Setup', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;
  }

  submit(event) {}

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/setup/view']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
