import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import { SetupDataService } from './setup-data.service';
import { Router } from '@angular/router';
import { SharedService } from '../../../../../shared/shared.service';
import { ISetupFilter } from './setup.interfaces';

@Injectable()
export class SetupService {
  private filter: ISetupFilter = null;

  public filterChanged = new BehaviorSubject<ISetupFilter>({ ...this.filter });

  constructor(
    private router: Router,
    private setupDataService: SetupDataService,
    private sharedService: SharedService
  ) {}

  setFilter(name: ISetupFilter['name']) {
    this.filter['name'] = name;
    this.filterChanged.next({ ...this.filter });
  }
}
