import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetupListComponent } from './setup-list/setup-list.component';
import { SetupViewComponent } from './setup-view/setup-view.component';
import { SetupEditComponent } from './setup-edit/setup-edit.component';
import { SetupAddComponent } from './setup-add/setup-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: SetupListComponent },
  { path: 'view', component: SetupViewComponent },
  { path: 'edit', component: SetupEditComponent },
  { path: 'add', component: SetupAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule {}
