import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { SetupRoutingModule } from './setup-routing.module';
import { SetupViewComponent } from './setup-view/setup-view.component';
import { SetupListComponent } from './setup-list/setup-list.component';
import { SetupEditComponent } from './setup-edit/setup-edit.component';
import { SetupService } from './setup.service';
import { SetupDataService } from './setup-data.service';
import { SetupAddComponent } from './setup-add/setup-add.component';

@NgModule({
  imports: [CommonModule, SetupRoutingModule, SharedModule],
  declarations: [SetupViewComponent, SetupListComponent, SetupEditComponent, SetupAddComponent],
  providers: [SetupService, SetupDataService]
})
export class SetupModule {}
