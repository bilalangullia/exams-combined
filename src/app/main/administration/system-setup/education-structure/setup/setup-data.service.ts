import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';
@Injectable()
export class SetupDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  private setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers };
    }
  }

  /*** Handle Http Errors ***/
  private handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };

  /* 
  Subjects, Stages, Certifications, Field of Studies, Programme Orientations, 
  */

  /* Add Setup */
  add(filter: string, payload: any) {}

  /* Subjects Filter | Field of Studies - Options*/
  getOptionsFieldOfStudies() {}

  /* Field of Studies Filter | Education Programme Orientation - Options */
  getOptionsEducationProgrammeOrientation() {}

  /*** List Page ***/
  getList(filter: string, start: number, end: number) {}

  searchList(filter: string, keyword: string, start: number, end: number) {}

  getDetails(filter: string, id: string) {}

  updateDetails(filter: string, id: string) {}
}
