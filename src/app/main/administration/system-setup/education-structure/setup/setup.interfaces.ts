export interface ISetupFilter {
  name: 'subjects' | 'stages' | 'certifications' | 'field_of_studies' | 'programme_orientations';
}
