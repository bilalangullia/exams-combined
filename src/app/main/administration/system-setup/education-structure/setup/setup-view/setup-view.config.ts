export const VIEWNODE_INPUT = [
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'visible',
    label: 'Visible',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
