import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { SetupService } from '../setup.service';
import { VIEWNODE_INPUT, IModalConfig } from './setup-view.config';
@Component({
  selector: 'app-setup-view',
  templateUrl: './setup-view.component.html',
  styleUrls: ['./setup-view.component.css'],
  providers: [KdModalEvent]
})
export class SetupViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public modalConfig: IModalConfig = {
    title: 'Delete Setup Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          // this.feeService.deleteFeeDetail(this.feeId).subscribe(
          //   (res: any) => {
          //     if (res.message) {
          //       this._modelEvent.toggleClose();
          //       // let toasterConfig: any = {
          //       //   type: 'success',
          //       //   title: res.message,
          //       //   showCloseButton: true,
          //       //   tapToDismiss: true,
          //       //   timeout: 2000
          //       // };
          //       // this.sharedService.setToaster(toasterConfig);
          //       this._router.navigate(['main/examinations/systems-view/list']);
          //     }
          //   },
          //   (err) => {
          //     this._modelEvent.toggleClose();
          //     let toasterConfig: any = {
          //       type: 'error',
          //       title: 'Fee Detail can not be deleted because its link to another data ',
          //       showCloseButton: true,
          //       tapToDismiss: true,
          //       timeout: 3000
          //     };
          //     this._kdalert.error(toasterConfig);
          //   }
          // );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private sharedService: SharedService,
    public _modelEvent: KdModalEvent,
    public _kdalert: KdAlertEvent
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Education - Setup', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/education-structure/setup/list' },
      { type: 'edit', path: 'main/system-setup/education-structure/setup/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
  }

  open() {
    this._modelEvent.toggleOpen();
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
