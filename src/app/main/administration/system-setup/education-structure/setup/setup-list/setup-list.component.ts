import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { SetupService } from '../setup.service';
import { FILTER_INPUTS, TABLE_COLUMNS } from './setup-list.config';

@Component({
  selector: 'app-setup-list',
  templateUrl: './setup-list.component.html',
  styleUrls: ['./setup-list.component.css']
})
export class SetupListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;

  public loading: boolean = true;
  public showTable: boolean = true;
  public inputs: Array<any> = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'list';
  readonly PAGESIZE: number = 20;
  public stableApi: ITableApi = {};
  public tableColumns: Array<ITableColumn> = [];
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, tableApi): void => {
            this.router.navigate(['main/system-setup/education-structure/setup/view']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public _tableEvent: KdTableEvent,
    public sharedService: SharedService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.enableToolbarSearch(true, (event: any): void => {});
    super.setPageTitle('Education - Setup', false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/system-setup/education-structure/setup/add' }]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.showTable = true;
  }

  detectValue(event) {
    console.log(event);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
