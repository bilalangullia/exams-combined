import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { EducationStructureRoutingModule } from './education-structure-routing.module';
import { EntryComponent } from './entry/entry.component';

@NgModule({
  imports: [CommonModule, EducationStructureRoutingModule, SharedModule],
  declarations: [EntryComponent]
})
export class EducationStructureModule {}
