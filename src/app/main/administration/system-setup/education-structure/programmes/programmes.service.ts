import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { ProgrammesDataService } from './programmes-data.service';
import { IFilters } from './programmes.interfaces';
@Injectable()
export class ProgrammesService {
  private filterEducationLevel: any;
  private filterEducationCycle: any;
  private educationField: any;
  private educationCycle: any;
  private educationCertification: any;
  private addNextProgramme: any;
  private cycleProgramme: any;
  private programmeId: string = null;
  public programmesViewDetails: any = [];
  public programmesEditDetails: any = [];
  private currentFilters: any = {};

  public filterEducationLevelChanged = new Subject();
  public filterEducationCycleChanged = new Subject();
  public educationFieldChanged = new Subject();
  public educationCycleChanged = new Subject();
  public educationCertificationChanged = new Subject();
  public cycleProgrammeChanged = new Subject();
  public addNextProgrammeChanged = new Subject();
  public programmeIdChanged = new BehaviorSubject<string>(null);
  public programmesViewDetailsChanged = new BehaviorSubject<any>(this.programmesViewDetails);
  public programmesEditDetailsChanged = new BehaviorSubject<any>(this.programmesEditDetails);
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);

  constructor(
    private router: Router,
    private programmesDataService: ProgrammesDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | FILTERS | Education Level***/
  getFilterEducationLevel() {
    this.programmesDataService.getOptionsEducationLevel().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.filterEducationLevel = res['data'];
          this.filterEducationLevelChanged.next([...this.filterEducationLevel]);
        } else {
          this.filterEducationLevel = [];
          this.filterEducationLevelChanged.next([...this.filterEducationLevel]);
        }
      },
      (err) => {
        this.filterEducationLevel = [];
        this.filterEducationLevelChanged.next([...this.filterEducationLevel]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Education  Cycle***/
  getFilterEducationCycle(level_id) {
    this.programmesDataService.getOptionsEducationCycle(level_id).subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.filterEducationCycle = res['data'];
        this.filterEducationCycleChanged.next([...this.filterEducationCycle]);
      } else {
        this.filterEducationCycle = [];
        this.filterEducationCycleChanged.next([...this.filterEducationCycle]);
      }
      (err) => {
        this.filterEducationLevel = [];
        this.filterEducationLevelChanged.next([...this.filterEducationLevel]);
      };
    });
  }

  /*** EDIT PAGE | FILTERS | Education Field Level***/
  getEducationField() {
    this.programmesDataService.getOptionsEducationField().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.educationField = res.data;
        this.educationFieldChanged.next([...this.educationField]);
      } else {
        this.educationField = [];
        this.educationFieldChanged.next([...this.educationField]);
      }
      (err) => {
        this.educationField = [];
        this.educationFieldChanged.next([...this.educationField]);
      };
    });
  }

  /*** EDIT PAGE | FILTERS | Education Cycle***/
  getEducationCycle() {
    this.programmesDataService.getEducationCycle().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.educationCycle = res.data;
        this.educationCycleChanged.next([...this.educationCycle]);
      } else {
        this.educationCycle = [];
        this.educationCycleChanged.next([...this.educationCycle]);
      }
      (err) => {
        this.educationCycle = [];
        this.educationCycleChanged.next([...this.educationCycle]);
      };
    });
  }

  /*** EDIT PAGE | FILTERS | Education Certification***/
  getEducationCertification() {
    this.programmesDataService.getEducationCertification().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.educationCertification = res.data;
        this.educationCertificationChanged.next([...this.educationCertification]);
      } else {
        this.educationCertification = [];
        this.educationCertificationChanged.next([...this.educationCertification]);
      }
      (err) => {
        this.educationCertification = [];
        this.educationCertificationChanged.next([...this.educationCertification]);
      };
    });
  }

  /*** EDIT PAGE | FILTERS | Add Next Programme***/
  getAddNextProgramme(id) {
    this.programmesDataService.getAddNextProgramme(id).subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.addNextProgramme = res.data;
        this.addNextProgrammeChanged.next([...this.addNextProgramme]);
      } else {
        this.addNextProgramme = [];
        this.addNextProgrammeChanged.next([...this.addNextProgramme]);
      }
      (err) => {
        this.addNextProgramme = [];
        this.addNextProgrammeChanged.next([...this.addNextProgramme]);
      };
    });
  }

  /*** EDIT PAGE | FILTERS | Cycle Programme***/
  getCycleProgrammeOptions(id) {
    this.programmesDataService.getCycleProgrammeOptions(id).subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.cycleProgramme = res.data;
        this.cycleProgrammeChanged.next([...this.cycleProgramme]);
      } else {
        this.cycleProgramme = [];
        this.cycleProgrammeChanged.next([...this.cycleProgramme]);
      }
      (err) => {
        this.cycleProgramme = [];
        this.cycleProgrammeChanged.next([...this.cycleProgramme]);
      };
    });
  }

  /*** Set Current filters ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }

  /*** LIST PAGE | List Data get total Records ***/
  getTotalRecords(educationCycleId: number, start?: number, end?: number) {
    return this.programmesDataService.getList(educationCycleId, start, end);
  }

  /*** LIST PAGE | List Data ***/
  getList(educationCycleId: number, start?: number, end?: number) {
    return this.programmesDataService.getList(educationCycleId, start, end);
  }

  /*** List Data | SEARCH ***/
  searchList(keyword: string, educationCycleId: number, start?: number, end?: number) {
    return this.programmesDataService.getListSearch(keyword, educationCycleId, start, end);
  }

  /*** List Page | Set Programme Id***/
  setProgrammeId(programmeId: string) {
    this.programmeId = programmeId;
    this.programmeIdChanged.next(this.programmeId);
  }

  /*** VIEW | Programmes Details Data  ***/
  getProgrammeDetails(programme_id) {
    this.programmesDataService.getProgrammeDetails(programme_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.programmesViewDetails = res.data;
          this.programmesViewDetailsChanged.next([...this.programmesViewDetails]);
        } else {
          this.programmesViewDetails = [];
          this.programmesViewDetailsChanged.next([...this.programmesViewDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.programmesViewDetails = [];
        this.programmesViewDetailsChanged.next([...this.programmesViewDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT | Programmes Details Data  ***/
  getProgrammeEditDetails(programme_id) {
    this.programmesDataService.getEditProgrammeDetails(programme_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.programmesEditDetails = res.data;
          this.programmesEditDetailsChanged.next([...this.programmesEditDetails]);
        } else {
          this.programmesEditDetails = [];
          this.programmesEditDetailsChanged.next([...this.programmesEditDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.programmesEditDetails = [];
        this.programmesEditDetailsChanged.next([...this.programmesEditDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE SUBMIT  ***/
  updateProgrammeDetail(programme_id, data) {
    this.programmesDataService.updateProgrammeDetails(programme_id, data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/education-structure/programmes/list']);
          }, 500);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD PAGE SUBMIT ***/
  addProgrammeDetails(data) {
    this.programmesDataService.addProgrammeDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/education-structure/programmes/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteCycleDetails(programme_id) {
    return this.programmesDataService.deleteProgrammeDetails(programme_id);
  }
}
