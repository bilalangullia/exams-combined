import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { ProgrammesRoutingModule } from './programmes-routing.module';
import { ProgrammesEditComponent } from './programmes-edit/programmes-edit.component';
import { ProgrammesListComponent } from './programmes-list/programmes-list.component';
import { ProgrammesViewComponent } from './programmes-view/programmes-view.component';
import { ProgrammesService } from './programmes.service';
import { ProgrammesDataService } from './programmes-data.service';
import { ProgrammesAddComponent } from './programmes-add/programmes-add.component';

@NgModule({
  imports: [CommonModule, ProgrammesRoutingModule, SharedModule],
  declarations: [ProgrammesEditComponent, ProgrammesListComponent, ProgrammesViewComponent, ProgrammesAddComponent],
  providers: [ProgrammesService, ProgrammesDataService]
})
export class ProgrammesModule {}
