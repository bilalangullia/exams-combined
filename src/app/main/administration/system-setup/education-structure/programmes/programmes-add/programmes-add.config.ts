export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'duration',
    label: 'Duration',
    visible: true,
    controlType: 'integer',
    type: 'number',
    min: 1,
    max: 100,
    required: true
  },
  {
    key: 'education_field_of_study',
    label: 'Education Field Of Study',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'education_cycle_id',
    label: 'Education Cycle',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'education_certification',
    label: 'Education Certification',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true,
    required: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
