import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { ProgrammesService } from '../programmes.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './programmes-add.config';

@Component({
  selector: 'app-programmes-add',
  templateUrl: './programmes-add.component.html',
  styleUrls: ['./programmes-add.component.css']
})
export class ProgrammesAddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  /* Subscriptions */
  private educationFieldSub: Subscription;
  private educationCycleSub: Subscription;
  private educationCertificationSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private programmesService: ProgrammesService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Programmes', false);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.programmesService.getEducationField();
    this.programmesService.getEducationCycle();
    this.programmesService.getEducationCertification();
    this.initializeSubscriptions();
  }

  initializeSubscriptions() {
    this.educationFieldSub = this.programmesService.educationFieldChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_field_of_study', 'options', tempOption);
      }
    });

    this.educationCycleSub = this.programmesService.educationCycleChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_cycle_id', 'options', tempOption);
      }
    });

    this.educationCertificationSub = this.programmesService.educationCertificationChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_certification', 'options', tempOption);
      }
    });
  }

  submit(event) {
    console.log(event);

    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        name: event.name,
        code: event.code,
        duration: event.duration,
        education_field_of_study_id: event.education_field_of_study,
        education_cycle_id: event.education_cycle_id,
        education_certification_id: event.education_certification
      };

      this.programmesService.addProgrammeDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/programmes/list']);
  }

  ngOnDestroy(): void {
    if (this.educationFieldSub) {
      this.educationFieldSub.unsubscribe();
    }
    if (this.educationCycleSub) {
      this.educationCycleSub.unsubscribe();
    }
    if (this.educationCertificationSub) {
      this.educationCertificationSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
