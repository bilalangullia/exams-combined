import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgrammesListComponent } from './programmes-list/programmes-list.component';
import { ProgrammesViewComponent } from './programmes-view/programmes-view.component';
import { ProgrammesEditComponent } from './programmes-edit/programmes-edit.component';
import { ProgrammesAddComponent } from './programmes-add/programmes-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ProgrammesListComponent },
  { path: 'view', component: ProgrammesViewComponent },
  { path: 'edit', component: ProgrammesEditComponent },
  { path: 'add', component: ProgrammesAddComponent }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgrammesRoutingModule {}
