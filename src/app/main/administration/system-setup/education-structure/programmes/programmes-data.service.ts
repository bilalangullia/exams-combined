import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../.././../../../environments/environment';
import urls from '../../../../../shared/config.urls';
@Injectable()
export class ProgrammesDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token} ` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /** EDUCATION LEVEL DROPDOWN **/
  getOptionsEducationLevel() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.educationLevel}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** EDUCATION CYCLE DROPDOWN **/
  getOptionsEducationCycle(levelId) {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${levelId}/${urls.educationCycle}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /***  LIST API ***/
  getList(educationCycleId: number, start?: number, end?: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${educationCycleId}/${urls.programmesList}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(keyword: string, educationCycleId: number, start?: number, end?: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${educationCycleId}/${urls.programmesList}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getProgrammeDetails(programme_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${programme_id}/${urls.programmeView}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW | DELETE API **/
  deleteProgrammeDetails(programme_id) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${programme_id}/destroy-education-system`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** EDUCATION FIELD OF STUDY DROPDOWN **/
  getOptionsEducationField() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.educationField}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** EDUCATION CYCLE DROPDOWN **/
  getEducationCycle() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.educationCycle}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** EDUCATION CERTIFICATION DROPDOWN **/
  getEducationCertification() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.educationCertification}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** EDUCATION ADD NEXT PROGRAMME DROPDOWN **/
  getAddNextProgramme(id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${urls.dropdown}/${id}/${urls.addNextProgramme}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** CYCLE PROGRAMME DROPDOWN **/
  getCycleProgrammeOptions(id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${id}/${urls.addCycleProgramme}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | EDIT | API  ***/
  getEditProgrammeDetails(programme_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${programme_id}/${urls.programmeEdit}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateProgrammeDetails(programme_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${programme_id}/${urls.programmeUpdate}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ADD | SUBMIT API  **/
  addProgrammeDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${urls.educationProgrammeAdd}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
