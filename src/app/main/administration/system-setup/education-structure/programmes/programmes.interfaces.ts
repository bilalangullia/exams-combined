export interface IFilters {
  education_level?: { key: number; value: string };
  education_cycle?: { key: number; value: string };
}
