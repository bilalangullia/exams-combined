const TABLE_COLUMN_CYCLES: Array<any> = [{ headerName: 'Cycle (Programme)', field: 'cycle_programmes', visible: true }];

export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: 'text',
    type: 'text'
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'duration',
    label: 'Duration',
    visible: true,
    controlType: 'integer',
    type: 'number',
    min: 1,
    max: 100,
    required: true
  },
  {
    key: 'visible',
    label: 'Visible',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true
  },
  {
    key: 'education_field_of_study',
    label: 'Education Field Of Study',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'education_cycle_id',
    label: 'Education Cycle',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'education_certification',
    label: 'Education Certification',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true,
    required: true
  },

  { key: 'Next ProgrammesHeader', label: 'NextProgrammes', visible: true, controlType: 'section' },
  {
    key: 'add_next_programme',
    label: 'Add Next Programme',
    visibe: true,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select'
      }
    ],
    events: true
  },
  {
    key: 'next_programme',
    label: ' ',
    controlType: 'table',
    visible: true,
    row: [],
    column: [{ headerName: 'Cycle-(Programme)', field: 'cycle_programme', visible: true }],
    config: {
      id: 'cyclesTableId',
      rowIdKey: 'id',
      loadType: 'normal',
      gridHeight: 100,
      action: {
        enabled: true,
        list: [{ type: 'delete' }]
      }
    }
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
