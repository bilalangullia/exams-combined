import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer, BehaviorSubject } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { ProgrammesService } from '../programmes.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './programmes-edit.config';

@Component({
  selector: 'app-programmes-edit',
  templateUrl: './programmes-edit.component.html',
  styleUrls: ['./programmes-edit.component.css']
})
export class ProgrammesEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public programmeId: any;
  public addNextProgrammeId: any;
  private addNextProgrammesList: Array<any> = [];
  private addNextProgrammeIds: Array<any> = [];
  public itemFind = {};
  private visibleOptions: Array<any> = [
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];
  private currentNextProgramme: any = {};
  public currentNextProgrammeChanged = new BehaviorSubject<any>(this.currentNextProgramme);

  /* Subscriptions */
  private programmeIdSub: Subscription;
  private programmeDetailsSub: Subscription;
  private educationFieldSub: Subscription;
  private educationCycleSub: Subscription;
  private educationCertificationSub: Subscription;
  private addNextProgrammeSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private programmesService: ProgrammesService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Programmes', false);
  }

  ngOnInit() {
    this.setOptions();
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.programmeIdSub = this.programmesService.programmeIdChanged.subscribe((programmeId) => {
      if (!programmeId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/programmes/list']);
        }, 0);
      } else {
        this.setDropdownOptions('visible', this.visibleOptions);
        this.programmeId = programmeId;
        this.programmesService.getProgrammeEditDetails(this.programmeId);
        this.programmeDetailsSub = this.programmesService.programmesEditDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setProgrammeDetails(data[0]);
          }
        });
      }
    });
    this.programmesService.getEducationField();
    this.programmesService.getEducationCycle();
    this.programmesService.getEducationCertification();
    this.programmesService.getAddNextProgramme(this.programmeId);
    this.initializeSubscriptions();
  }

  setProgrammeDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'visible') {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              details[this._questionBase[i]['key']]['key'] ? details[this._questionBase[i]['key']]['key'] : null
            );
          } else if (this._questionBase[i].key == 'education_field_of_study') {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              details[this._questionBase[i]['key']]['key'] ? details[this._questionBase[i]['key']]['key'] : null
            );
          } else if (this._questionBase[i].key == 'education_cycle_id') {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              details[this._questionBase[i]['key']]['key'] ? details[this._questionBase[i]['key']]['key'] : null
            );
          } else if (this._questionBase[i].key == 'education_certification') {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              details[this._questionBase[i]['key']]['key'] ? details[this._questionBase[i]['key']]['key'] : null
            );
          } else if (this._questionBase[i].key == 'add_next_programme') {
            this.api.setProperty(this._questionBase[i].key, 'value', null);
          } else if (this._questionBase[i].key === 'next_programme') {
            this.addNextProgrammesList = details['next_programme'];
            this.addNextProgrammesList.map((element) => {
              this.addNextProgrammesList['id'] = element.id;
              this.addNextProgrammesList['cycle_programme'] = element.cycle_programme;
              this.addNextProgrammeIds.push({ id: this.addNextProgrammesList['id'] });
            });
            this.api.setProperty(this._questionBase[i].key, 'row', this.addNextProgrammesList);
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  setOptions() {
    this._questionBase.forEach((question: any) => {
      if (question['key'] === 'next_programme') {
        question['config']['action']['list'][0]['callback'] = this.deleteRow.bind(this);
      }
    });
  }

  deleteRow(params: any) {
    if (params['data']['id']) {
      let deleteIndex: number = this.addNextProgrammesList.findIndex((item) => item['id'] === params['data']['id']);
      this.addNextProgrammesList.splice(deleteIndex, 1);
      this.addNextProgrammeIds.splice(deleteIndex, 1);
      this.api.setProperty('next_programme', 'row', [...this.addNextProgrammesList]);
    }
  }

  initializeSubscriptions() {
    this.educationFieldSub = this.programmesService.educationFieldChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_field_of_study', 'options', tempOption);
      }
    });

    this.educationCycleSub = this.programmesService.educationCycleChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_cycle_id', 'options', tempOption);
      }
    });

    this.educationCertificationSub = this.programmesService.educationCertificationChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_certification', 'options', tempOption);
      }
    });

    this.addNextProgrammeSub = this.programmesService.addNextProgrammeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('add_next_programme', 'options', tempOption);
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        code: event.code,
        name: event.name,
        visible: event.visible,
        duration: event.duration,
        education_field_of_study_id: event.education_field_of_study,
        education_cycle_id: event.education_cycle_id,
        education_certification_id: event.education_certification,
        next_programme: this.addNextProgrammeIds && this.addNextProgrammeIds.length ? this.addNextProgrammeIds : null
      };
      this.programmesService.updateProgrammeDetail(this.programmeId, payload);
    }
  }

  detectValue(event) {
    if (event['key'] === 'add_next_programme' && event['options'] && event['options'].length) {
      if (event['value']) {
        this.programmesService.getCycleProgrammeOptions(event['value']);
        this.programmesService.cycleProgrammeChanged.subscribe((res) => {
          if (res) {
            this.itemFind = res[0];
            if (this.itemFind && this.itemFind['id']) {
              if (this.addNextProgrammesList.findIndex((item) => item['id'] === this.itemFind['id']) == -1) {
                this.addNextProgrammesList.push({
                  id: this.itemFind['id'],
                  cycle_programme: this.itemFind['cycle_programme']
                });
                this.addNextProgrammeIds.push({ id: this.itemFind['id'] });
              }
              this.api.setProperty('next_programme', 'row', [...this.addNextProgrammesList]);
            }
          }
        });
      }
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/programmes/view']);
  }

  ngOnDestroy(): void {
    if (this.programmeIdSub) {
      this.programmeIdSub.unsubscribe();
    }
    if (this.programmeDetailsSub) {
      this.programmeDetailsSub.unsubscribe();
    }
    if (this.educationFieldSub) {
      this.educationFieldSub.unsubscribe();
    }
    if (this.educationCycleSub) {
      this.educationCycleSub.unsubscribe();
    }
    if (this.educationCertificationSub) {
      this.educationCertificationSub.unsubscribe();
    }
    if (this.addNextProgrammeSub) {
      this.addNextProgrammeSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
