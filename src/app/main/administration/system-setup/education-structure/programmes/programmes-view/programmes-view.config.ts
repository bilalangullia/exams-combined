export const VIEWNODE_INPUT = [
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'duration',
    label: 'Duration',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'visible',
    label: 'Visible',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_field_of_study',
    label: 'Education Field Of Study',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_cycle_id',
    label: 'Education Cycle',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_certification',
    label: 'Education Certification',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'next_programme',
    label: 'Next Programmes',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Cycle - (Programme)',
        field: 'cycle_programme',
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'options',
      rowIdkey: 'id',
      gridHeight: 150,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
