import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../../shared/shared.toasters';
import { ProgrammesService } from '../programmes.service';
import { VIEWNODE_INPUT, IModalConfig } from './programmes-view.config';

@Component({
  selector: 'app-programmes-view',
  templateUrl: './programmes-view.component.html',
  styleUrls: ['./programmes-view.component.css'],
  providers: [KdModalEvent]
})
export class ProgrammesViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public programmeId: any;

  /* Subscriptions */
  private programmeIdSub: Subscription;
  private programmeDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Programmes Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          // this.feeService.deleteFeeDetail(this.feeId).subscribe(
          //   (res: any) => {
          //     if (res.message) {
          //       this._modelEvent.toggleClose();
          //       // let toasterConfig: any = {
          //       //   type: 'success',
          //       //   title: res.message,
          //       //   showCloseButton: true,
          //       //   tapToDismiss: true,
          //       //   timeout: 2000
          //       // };
          //       // this.sharedService.setToaster(toasterConfig);
          //       this._router.navigate(['main/examinations/systems-view/list']);
          //     }
          //   },
          //   (err) => {
          //     this._modelEvent.toggleClose();
          //     let toasterConfig: any = {
          //       type: 'error',
          //       title: 'Fee Detail can not be deleted because its link to another data ',
          //       showCloseButton: true,
          //       tapToDismiss: true,
          //       timeout: 3000
          //     };
          //     this._kdalert.error(toasterConfig);
          //   }
          // );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private sharedService: SharedService,
    public _modelEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private programmesService: ProgrammesService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Education - Programmes', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/education-structure/programmes/list' },
      { type: 'edit', path: 'main/system-setup/education-structure/programmes/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.programmeIdSub = this.programmesService.programmeIdChanged.subscribe((programmeId) => {
      if (!programmeId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/programmes/list']);
        }, 0);
      } else {
        this.programmeId = programmeId;
        this.programmesService.getProgrammeDetails(this.programmeId);
        this.programmeDetailsSub = this.programmesService.programmesViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setProgrammeDetails(data[0]);
          }
        });
      }
    });
  }

  setProgrammeDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key && this._questionBase[i].key == 'next_programme') {
            this.api.setProperty(this._questionBase[i].key, 'row', details.next_programme);
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  open() {
    this._modelEvent.toggleOpen();
  }

  ngOnDestroy(): void {
    if (this.programmeIdSub) {
      this.programmeIdSub.unsubscribe;
    }
    if (this.programmeDetailsSub) {
      this.programmeDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
