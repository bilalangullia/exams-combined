interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Code?: Column;
  Name?: Column;
  Duration?: Column;
  Visible?: Column;
  EducationFieldOfStudy?: Column;
  EducationCycle?: Column;
  EducationCertification?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Code: {
    headerName: 'Code',
    field: 'code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Duration: {
    headerName: 'Duration',
    field: 'duration',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationFieldOfStudy: {
    headerName: 'Education Field Of Study',
    field: 'education_field_of_study',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationCycle: {
    headerName: 'Education Cycle',
    field: 'education_cycle_id',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationCertification: {
    headerName: 'Education Certification',
    field: 'education_certification',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'education_level',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '-- Select --'
      }
    ],
    events: true
  },
  {
    key: 'education_cycle',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '-- Select --'
      }
    ],
    events: true
  }
];
