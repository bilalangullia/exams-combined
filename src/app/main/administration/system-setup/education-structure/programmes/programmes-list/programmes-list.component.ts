import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { ProgrammesService } from '../programmes.service';
import { IFilters } from '../programmes.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './programmes-list.config';
@Component({
  selector: 'app-programmes-list',
  templateUrl: './programmes-list.component.html',
  styleUrls: ['./programmes-list.component.css'],
  providers: [KdTableEvent]
})
export class ProgrammesListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('filters') filter: KdFilter;
  public filterInputs = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'programmeList';
  readonly PAGESIZE: number = 20;

  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public currentFilters: IFilters = null;
  public showTable: boolean = false;
  private tableEventList: any;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  private tableEventSub: Subscription;
  private educationLevelFilterSub: Subscription;
  private educationCycleFilterSub: Subscription;
  private currentFiltersSub: Subscription;

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Code,
    TABLECOLUMN.Name,
    TABLECOLUMN.Duration,
    TABLECOLUMN.Visible,
    TABLECOLUMN.EducationFieldOfStudy,
    TABLECOLUMN.EducationCycle,
    TABLECOLUMN.EducationCertification
  ];
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.programmesService.setProgrammeId(_rowNode['data']['id']);
            this.router.navigate(['main/system-setup/education-structure/programmes/view']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    private programmesService: ProgrammesService,
    public sharedService: SharedService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
    super.setPageTitle('Education - Programmes', false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/system-setup/education-structure/programmes/add' }]);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.programmesService.resetFilters();
    this.programmesService.getFilterEducationLevel();
    this.educationLevelFilterSub = this.programmesService.filterEducationLevelChanged.subscribe(
      (options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('education_level', options);
        } else {
          this.setDropdownOptions('education_level', []);
        }
      }
    );

    this.educationCycleFilterSub = this.programmesService.filterEducationCycleChanged.subscribe(
      (options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('education_cycle', options);
        } else {
          this.setDropdownOptions('education_cycle', []);
        }
      }
    );

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });
    this.currentFiltersSub = this.programmesService.currentFiltersChanged.subscribe((filters: IFilters) => {
      this.currentFilters = { ...filters };
      this.showTable = false;
      if (filters && filters['education_cycle'] && filters['education_cycle']['key']) {
        this.programmesService.getTotalRecords(filters['education_cycle']['key'], 0, 1).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data']['total']) {
              this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
              this.showTable = true;
            } else {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          },
          (err) => {
            console.log(err);
            this.tableConfig.paginationConfig.total = 0;
            this.showTable = false;
          }
        );
      } else {
        this.showTable = false;
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      options = data;
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
    } else {
      options.unshift({ key: null, value: '-- select --' });
    }
    this.filter.setInputProperty(key, 'options', [...options]);
    setTimeout(() => {
      this.filter.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  detectValue(event) {
    if (event['key'] === 'education_level') {
      this.programmesService.setCurrentFilters({ education_cycle: { key: null, value: '-- select --' } });
      this.programmesService.getFilterEducationCycle(event['value']);
    }
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj && filterObj['key']) {
      let filter: IFilters = {};

      filter[event['key']] = filterObj;
      this.programmesService.setCurrentFilters({ ...filter });
    }
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      if (
        this.currentFilters &&
        this.currentFilters['education_cycle'] &&
        this.currentFilters['education_cycle']['key']
      ) {
        listReq = this.programmesService.searchList(
          searchKey,
          this.currentFilters['education_cycle']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    } else {
      if (
        this.currentFilters &&
        this.currentFilters['education_cycle'] &&
        this.currentFilters['education_cycle']['key']
      ) {
        listReq = this.programmesService.getList(
          this.currentFilters['education_cycle']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    }
    listReq.toPromise().then(
      (res: any) => {
        this.loading = false;
        if (res && res['data'] && res['data']['programmeRecords'] && res['data']['programmeRecords'].length) {
          list = res['data']['programmeRecords'];
          total =
            this.searchKey && this.searchKey.length ? res['data']['programmeRecords'].length : res['data']['total'];
          list.forEach((item) => {
            item['visible'] = item['visible'] == 1 ? 'Yes' : 'No';
          });
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }
  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    this.showTable = false;
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.educationLevelFilterSub) {
      this.educationLevelFilterSub.unsubscribe();
    }
    if (this.educationCycleFilterSub) {
      this.educationCycleFilterSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
