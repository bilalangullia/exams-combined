import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import { GradesDataService } from './grades-data.service';
import { Router } from '@angular/router';
import { SharedService } from '../../../../../shared/shared.service';
import { IGradesFilters } from './grades.interfaces';

@Injectable()
export class GradesService {
  private optionsEducationLevel: Array<any> = [];
  private optionsEducationProgramme: Array<any> = [];
  private filters: IGradesFilters = null;
  private gradeId: string = null;
  private details: any = null;
  private optionsEducationStage: Array<any> = [];
  private optionsEducationProgram: Array<any> = [];

  public optionsEducationLevelChanged = new BehaviorSubject<Array<any>>([...this.optionsEducationLevel]);
  public optionsEducationProgrammeChanged = new BehaviorSubject<Array<any>>([...this.optionsEducationProgramme]);
  public filtersChanged = new BehaviorSubject<IGradesFilters>({ ...this.filters });
  public gradeIdChanged = new BehaviorSubject<string>(this.gradeId);
  public detailsChanged = new BehaviorSubject<string>({ ...this.details });
  public optionsEducationStageChanged = new BehaviorSubject<Array<any>>([...this.optionsEducationStage]);
  public optionsEducationProgramChanged = new BehaviorSubject<Array<any>>([...this.optionsEducationProgram]);

  constructor(private gradesDataService: GradesDataService, private sharedService: SharedService) {}

  getOptionsEducationLevel() {
    this.gradesDataService.getOptionsEducationLevel().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationLevel = res['data'];
          this.optionsEducationLevelChanged.next([...this.optionsEducationLevel]);
        } else {
          this.optionsEducationLevel = [];
          this.optionsEducationLevelChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationLevel = [];
        this.optionsEducationLevelChanged.next([]);
      }
    );
  }

  getOptionsEducationProgramme(levelId: string) {
    this.gradesDataService.getOptionsEducationProgramme(levelId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationProgramme = res['data'];
          this.optionsEducationProgrammeChanged.next([...this.optionsEducationProgramme]);
        } else {
          this.optionsEducationProgramme = [];
          this.optionsEducationProgrammeChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationProgramme = [];
        this.optionsEducationProgrammeChanged.next([]);
      }
    );
  }

  setFilters(filter: IGradesFilters) {
    this.filters = { ...this.filters, ...filter };
    this.filtersChanged.next({ ...this.filters });
  }

  resetFilters() {
    this.filters = null;
    this.filtersChanged.next(null);
  }

  getList(programmeId: string, start: number, end: number) {
    return this.gradesDataService.getList(programmeId, start, end);
  }

  searchList(programmeId: string, keyword: string, start: number, end: number) {
    return this.gradesDataService.getListSearch(programmeId, keyword, start, end);
  }

  setGradeId(gradeId: string) {
    this.gradeId = gradeId;
    this.gradeIdChanged.next(this.gradeId);
  }

  resetGradeId() {
    this.gradeId = null;
    this.gradeIdChanged.next(null);
  }

  getDetails(gradeId: string) {
    this.gradesDataService.getDetails(gradeId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.details = res['data'];
          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = null;
          this.detailsChanged.next(null);
        }
      },
      (err) => {
        this.details = null;
        this.detailsChanged.next(null);
      }
    );
  }

  resetDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  getOptionsEducationStage() {
    this.gradesDataService.getOptionsEducationStage().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationStage = res['data'];
          this.optionsEducationStageChanged.next([...this.optionsEducationStage]);
        } else {
          this.optionsEducationStage = [];
          this.optionsEducationStageChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationStage = [];
        this.optionsEducationStageChanged.next([]);
      }
    );
  }

  getOptionsEducationProgram() {
    this.gradesDataService.getOptionsEducationProgram().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationProgram = res['data'];
          this.optionsEducationProgramChanged.next([...this.optionsEducationProgram]);
        } else {
          this.optionsEducationProgram = [];
          this.optionsEducationProgramChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationProgram = [];
        this.optionsEducationProgramChanged.next([]);
      }
    );
  }

  addGrade(payload: any) {
    return this.gradesDataService.addGrade(payload);
  }

  updateGrade(payload: any) {
    return this.gradesDataService.updateGrade(payload);
  }
}
