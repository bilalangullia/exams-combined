import { Component, OnInit, OnDestroy } from '@angular/core';
import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';
import { ActivatedRoute, Router } from '@angular/router';
import { GradesService } from '../grades.service';
import { SharedService } from '../../../../../../shared/shared.service';
import { Subscription } from 'rxjs';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { IAddGradesPayload } from '../grades.interfaces';

@Component({
  selector: 'app-grades-add',
  templateUrl: './grades-add.component.html',
  styleUrls: ['./grades-add.component.css']
})
export class GradesAddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Education - Grades';

  public questionBase: any = QUESTION_BASE;
  public formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  /* Subscriptions */
  private optionsEducationStageSub: Subscription;
  private optionsEducationProgramSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public sharedService: SharedService,
    private gradesService: GradesService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.gradesService.getOptionsEducationStage();
    this.gradesService.getOptionsEducationProgram();

    setTimeout(() => {
      this.optionsEducationStageSub = this.gradesService.optionsEducationStageChanged.subscribe(
        (options: Array<any>) => {
          if (options) {
            this.setDropdownOptions('education_stage', options);
          } else {
            this.setDropdownOptions('education_stage', []);
          }
        }
      );

      this.optionsEducationProgramSub = this.gradesService.optionsEducationProgramChanged.subscribe(
        (options: Array<any>) => {
          if (options) {
            this.setDropdownOptions('education_programme', options);
          } else {
            this.setDropdownOptions('education_programme', []);
          }
        }
      );
    }, 0);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      if (key === 'education_programme') {
        options = data.map((item) => ({ key: item['id'], value: item['name'], code: item['code'] }));
      } else {
        options = [...data];
      }
    }
    options.unshift({ key: null, value: '-- Please Select --' });
    this.api.setProperty(key, 'options', options);
    setTimeout(() => {
      this.api.setProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(event) {
    let form: IAddGradesPayload = { ...event };

    if (this.requiredCheck(form)) {
      const toasterConfig: IToasterConfig = {
        title: 'Missing required fields',
        type: 'error',
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload: IAddGradesPayload = { ...form };
      this.gradesService.addGrade(payload).subscribe(
        (res: any) => {
          if (res && res['data'] && !res['data']['error']) {
            let successMsg: IToasterConfig = {
              title: 'Saved Successfully.',
              type: 'success',
              timeout: 1000
            };
            this.sharedService.setToaster(successMsg);
            setTimeout(() => {
              this.cancel();
            }, 500);
          } else {
            let errorMsg: IToasterConfig = {
              title: 'Error while saving.',
              type: 'error',
              timeout: 1000
            };
            this.sharedService.setToaster(errorMsg);
          }
        },
        (err) => {
          let errorMsg: IToasterConfig = {
            title: 'An Error Occurred.',
            type: 'error',
            timeout: 1000
          };
          this.sharedService.setToaster(errorMsg);
          setTimeout(() => {
            this.cancel();
          }, 500);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/education-structure/grades']);
  }

  ngOnDestroy(): void {
    if (this.optionsEducationProgramSub) {
      this.optionsEducationProgramSub.unsubscribe();
    }
    if (this.optionsEducationStageSub) {
      this.optionsEducationStageSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
