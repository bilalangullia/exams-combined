import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GradesListComponent } from './grades-list/grades-list.component';
import { GradesViewComponent } from './grades-view/grades-view.component';
import { GradesEditComponent } from './grades-edit/grades-edit.component';
import { GradesAddComponent } from './grades-add/grades-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: GradesListComponent },
  { path: 'view', component: GradesViewComponent },
  { path: 'edit', component: GradesEditComponent },
  { path: 'add', component: GradesAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradesRoutingModule {}
