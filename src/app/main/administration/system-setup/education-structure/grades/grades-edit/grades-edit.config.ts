import { IDropdownOptions } from '../../../../../../shared/shared.interfaces';

export const OPTIONS_VISIBLE: Array<IDropdownOptions> = [
  { key: null, value: '-- Please Select --' },
  { key: 1, value: 'YES' },
  { key: 0, value: 'NO' }
];

export const QUESTION_BASE: Array<any> = [
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'education_stage',
    label: 'Education Stage',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: 'null', value: '-- Please Select --' }],
    events: true,
    required: true
  },
  {
    key: 'admission_age',
    label: 'Admission Age',
    visible: true,
    controlType: 'integer',
    type: 'number',
    min: 5,
    // max: 19,
    required: true,
    readonly: true
  },
  {
    key: 'education_program',
    label: 'Education Programme',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: 'null', value: '-- Please Select --' }],
    events: true,
    required: true
  },
  {
    key: 'visible',
    label: 'Visible',
    visible: true,
    controlType: 'dropdown',
    options: OPTIONS_VISIBLE,
    events: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
