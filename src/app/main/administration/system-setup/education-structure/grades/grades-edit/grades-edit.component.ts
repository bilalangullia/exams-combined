import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { GradesService } from '../grades.service';
import { QUESTION_BASE, FORM_BUTTONS, OPTIONS_VISIBLE } from './grades-edit.config';
import { IUpdateGradesPayload } from '../grades.interfaces';

@Component({
  selector: 'app-grades-edit',
  templateUrl: './grades-edit.component.html',
  styleUrls: ['./grades-edit.component.css']
})
export class GradesEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public questionBase: any = QUESTION_BASE;
  public formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public showForm: boolean = false;

  private stageOptions: Array<any> = [];
  private programOptions: Array<any> = [];
  private gradeId: string = null;

  /* Subscriptions */
  private gradeIdSub: Subscription;
  private detailsSub: Subscription;
  private optionsEducationStageSub: Subscription;
  private optionsEducationProgrammeSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private gradesService: GradesService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Education - Grades', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.gradeIdSub = this.gradesService.gradeIdChanged.subscribe((gradeId: string) => {
      if (!gradeId) {
        this.router.navigate(['main/system-setup/education-structure']);
      } else {
        this.gradeId = gradeId;
        this.gradesService.getOptionsEducationStage();
        this.gradesService.getOptionsEducationProgram();

        this.gradesService.getDetails(gradeId);

        setTimeout(() => {
          this.optionsEducationStageSub = this.gradesService.optionsEducationStageChanged.subscribe(
            (options: Array<any>) => {
              if (options) {
                this.stageOptions = options;
                this.setDropdownOptions('education_stage', this.stageOptions);
              }
            }
          );

          this.optionsEducationProgrammeSub = this.gradesService.optionsEducationProgramChanged.subscribe(
            (options: Array<any>) => {
              if (options) {
                this.programOptions = options.map((item) => ({
                  key: item['id'],
                  value: item['name'],
                  code: item['code']
                }));
                this.setDropdownOptions('education_program', this.programOptions);
              }
            }
          );

          setTimeout(() => {
            this.detailsSub = this.gradesService.detailsChanged.subscribe((details: any) => {
              if (details) {
                this.setDetails(details);
              }
            });
          }, 0);
        }, 500);
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      if (key == 'education_program') {
        options = data;
      } else {
        options = data;
      }
    }
    options.unshift({ key: null, value: '-- Please Select --' });
    this.api.setProperty(key, 'options', options);
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      if (question['key'] == 'education_stage') {
        let obj = this.stageOptions.find((item) => item['value'] === details[question['key']]);
        this.api.setProperty(question['key'], 'value', obj ? obj['key'] : null);
      } else if (question['key'] == 'education_program') {
        let obj = this.programOptions.find((item) => item['value'] === details[question['key']]);
        this.api.setProperty(question['key'], 'value', obj ? obj['key'] : null);
      } else {
        this.api.setProperty(question['key'], 'value', details[question['key']]);
      }
    });
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      const toasterConfig: IToasterConfig = {
        title: 'Missing required fields',
        type: 'error',
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload: IUpdateGradesPayload = {
        code: form['code'],
        name: form['name'],
        education_stage: form['education_stage'],
        admission_age: form['admission_age'],
        education_programme: form['education_program'],
        visible: +form['visible'],
        id: this.gradeId
      };
      this.gradesService.updateGrade({ ...payload }).subscribe(
        (res: any) => {
          if (res && res['data'] && !res['data']['error']) {
            let successMsg: IToasterConfig = { title: 'Saved Successfully.', type: 'success', timeout: 1000 };
            this.sharedService.setToaster(successMsg);
            setTimeout(() => {
              this.cancel();
            }, 500);
          } else {
            let errorMsg: IToasterConfig = { title: 'Error while saving', type: 'error', timeout: 1000 };
            this.sharedService.setToaster(errorMsg);
          }
        },
        (err) => {
          let errorMsg: IToasterConfig = { title: 'Error while saving.', type: 'error', timeout: 1000 };
          this.sharedService.setToaster(errorMsg);
          setTimeout(() => {
            this.cancel();
          }, 500);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/education-structure/grades']);
  }

  ngOnDestroy(): void {
    if (this.gradeIdSub) {
      this.gradeIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    if (this.optionsEducationStageSub) {
      this.optionsEducationStageSub.unsubscribe();
    }
    if (this.optionsEducationProgrammeSub) {
      this.optionsEducationProgrammeSub.unsubscribe();
    }
    this.gradesService.resetDetails();
    super.destroyPageBaseSub();
  }
}
