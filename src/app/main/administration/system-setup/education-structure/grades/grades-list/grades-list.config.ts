import { IDropdownOptions } from '../../../../../../shared/shared.interfaces';

const DEFAULT_OPTIONS: Array<IDropdownOptions> = [{ key: null, value: '-- Please Select --' }];

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Visible?: Column;
  Name?: Column;
  Code?: Column;
  EducationProgramme?: Column;
  EducationStage: Column;
  Subjects?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Code: {
    headerName: 'Code',
    field: 'code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationProgramme: {
    headerName: 'Education Programme',
    field: 'education_programme',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationStage: {
    headerName: 'Education Stage',
    field: 'education_stage',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Subjects: {
    headerName: 'Subjects',
    field: 'subjects',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'education_level',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: DEFAULT_OPTIONS,
    events: true
  },
  {
    key: 'education_programme',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: DEFAULT_OPTIONS,
    events: true
  }
];
