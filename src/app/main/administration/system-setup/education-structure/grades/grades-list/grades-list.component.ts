import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { GradesService } from '../grades.service';
import { IGradesFilters } from '../grades.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './grades-list.config';

@Component({
  selector: 'app-grades-list',
  templateUrl: './grades-list.component.html',
  styleUrls: ['./grades-list.component.css'],
  providers: [KdTableEvent]
})
export class GradesListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Education - Grades';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public showTable: boolean = false;
  public inputs: Array<any> = FILTER_INPUTS;

  readonly TOTALROW: number = 1000;
  readonly GRID_ID: string = 'gradesList';
  readonly PAGESIZE: number = 20;
  public tableApi: ITableApi = {};
  public tableConfig: ITableConfig = {
    id: this.GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.gradesService.setGradeId(_rowNode['data']['id']);
            this.router.navigate(['main/system-setup/education-structure/grades/view']);
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Visible,
    TABLECOLUMN.Name,
    TABLECOLUMN.Code,
    TABLECOLUMN.EducationProgramme,
    TABLECOLUMN.EducationStage,
    TABLECOLUMN.Subjects
  ];

  private currentFilters: IGradesFilters = null;
  private tableEventList: any;

  @ViewChild('filters') filters: KdFilter;

  /* Subscriptions */
  private educationLevelSub: Subscription;
  private educationProgrammeSub: Subscription;
  private filtersSub: Subscription;
  private tableEventSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    private gradesService: GradesService,
    public sharedService: SharedService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
    super.setPageTitle('Education - Grades', false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/system-setup/education-structure/grades/add' }]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.gradesService.getOptionsEducationLevel();

    this.educationLevelSub = this.gradesService.optionsEducationLevelChanged.subscribe((options: Array<any>) => {
      if (options && options.length) {
        this.setDropdownOptions('education_level', options);
      } else {
        this.setDropdownOptions('education_level', []);
      }
    });

    this.educationProgrammeSub = this.gradesService.optionsEducationProgrammeChanged.subscribe(
      (options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('education_programme', options);
        } else {
          this.setDropdownOptions('education_programme', []);
        }
      }
    );

    this.filtersSub = this.gradesService.filtersChanged.subscribe((filters: IGradesFilters) => {
      this.currentFilters = { ...filters };
      if (filters && filters['education_programme'] && filters['education_programme']['key']) {
        this.gradesService.getList(filters['education_programme']['key'], 0, 1).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data']['total']) {
              this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
              this.showTable = true;
            } else {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          },
          (err) => {
            console.log(err);
            this.tableConfig.paginationConfig.total = 0;
            this.showTable = false;
          }
        );
      } else {
        this.showTable = false;
      }
    });

    setTimeout(() => {
      this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRID_ID).subscribe((event: any): void => {
        if (event instanceof KdTableDatasourceEvent) {
          this.tableEventList = event;
          this.fetchList(event);
        }
      });
    }, 1000);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      options = data;
    } else {
      options.unshift({ key: null, value: '-- Please Select --' });
    }
    this.filters.setInputProperty(key, 'options', [...options]);
    setTimeout(() => {
      this.filters.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  detectValue(event) {
    if (event['key'] === 'education_level') {
      this.gradesService.getOptionsEducationProgramme(event['value']);
      this.gradesService.setFilters({ education_programme: { key: null, value: '-- Please Select --' } });
    }
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj && filterObj['key']) {
      let filter: IGradesFilters = {};
      filter[event['key']] = filterObj;
      this.gradesService.setFilters({ ...filter });
    }
  }

  fetchList(event: KdTableDatasourceEvent) {
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;

    if (
      this.currentFilters &&
      this.currentFilters['education_programme'] &&
      this.currentFilters['education_programme']['key']
    ) {
      this.gradesService
        .getList(this.currentFilters['education_programme']['key'], fetchParams['startRow'], fetchParams['endRow'])
        .toPromise()
        .then(
          (res: any) => {
            if (res && res['data'] && res['data']['list']) {
              list = res['data']['list'].map((item) => ({ ...item, visible: item['visible'] ? 'YES' : 'NO' }));
              total = res['data']['total'];
            } else {
              list = [];
              total = 0;
            }
            dataSourceParams = { rows: list, total };
            event.subscriber.next(dataSourceParams);
            event.subscriber.complete();
          },
          (err) => {
            list = [];
            total = 0;
            dataSourceParams = { rows: list, total };
            event.subscriber.next(dataSourceParams);
            event.subscriber.complete();
          }
        );
    }
  }

  searchList(keyword: string) {}

  ngOnDestroy(): void {
    if (this.educationLevelSub) {
      this.educationLevelSub.unsubscribe();
    }
    if (this.educationProgrammeSub) {
      this.educationProgrammeSub.unsubscribe();
    }
    if (this.filtersSub) {
      this.filtersSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    this.gradesService.resetFilters();
    super.destroyPageBaseSub();
  }
}
