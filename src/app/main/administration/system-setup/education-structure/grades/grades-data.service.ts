import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class GradesDataService {
  constructor(private http: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  getOptionsEducationLevel() {
    // http://openemis.n2.iworklab.com/api/dropdown/education-level
    return this.http
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.education}-${urls.level}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getOptionsEducationProgramme(educationLevelId: string) {
    // http://openemis.n2.iworklab.com/api/dropdown/education-program/4
    return this.http
      .get(
        `${environment.baseUrl}/${urls.dropdown}/${urls.education}-${urls.program}/${educationLevelId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***   LIST API ***/
  getList(educationProgrammeId: string, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grades/9?start=0&end=20&keyword=primary
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}s/${educationProgrammeId}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(educationProgrammeId: string, keyword: string, start: number, end: number) {
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}s/${educationProgrammeId}?start=${start}&end=${end}&${urls.keyword}=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getDetails(gradeId: string) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grades/view/59
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}s/${urls.view}/${gradeId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getOptionsEducationStage() {
    // http://openemis.n2.iworklab.com/api/dropdown/education-stage
    return this.http
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.education}-${urls.stage}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getOptionsEducationProgram() {
    // http://openemis.n2.iworklab.com/api/registration/candidate/dropdown/education-programme
    return this.http
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.dropdown}/${urls.education}-${urls.programme}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** ADD | Submit API ***/
  addGrade(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grades/store
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}s/store`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** UPDATE | Submit API ***/
  updateGrade(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grades/update
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}s/${urls.update}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  deleteGrade() {
    // return this.http.delete(``).pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
