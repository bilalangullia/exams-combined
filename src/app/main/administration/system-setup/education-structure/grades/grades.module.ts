import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { GradesRoutingModule } from './grades-routing.module';
import { GradesEditComponent } from './grades-edit/grades-edit.component';
import { GradesListComponent } from './grades-list/grades-list.component';
import { GradesViewComponent } from './grades-view/grades-view.component';
import { GradesService } from './grades.service';
import { GradesDataService } from './grades-data.service';
import { GradesAddComponent } from './grades-add/grades-add.component';

@NgModule({
  imports: [CommonModule, GradesRoutingModule, SharedModule],
  declarations: [GradesEditComponent, GradesListComponent, GradesViewComponent, GradesAddComponent],
  providers: [GradesService, GradesDataService]
})
export class GradesModule {}
