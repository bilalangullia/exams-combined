export const VIEWNODE_INPUT = [
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },

  {
    key: 'education_stage',
    label: 'Education Stage',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'admission_age',
    label: 'Admission Age',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_program',
    label: 'Education Program',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'visible',
    label: 'Visible',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'subjects',
    label: 'Subjects',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Name',
        field: 'name',
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Code',
        field: 'code',
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Hours Required',
        field: 'hours_required',
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'subjects',
      rowIdkey: 'id',
      gridHeight: 150,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified',
    label: 'Modified On',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
