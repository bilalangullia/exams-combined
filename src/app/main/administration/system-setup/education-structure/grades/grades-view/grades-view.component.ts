import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { VIEWNODE_INPUT, IModalConfig } from './grades-view.config';
import { GradesService } from '../grades.service';

@Component({
  selector: 'app-grades-view',
  templateUrl: './grades-view.component.html',
  styleUrls: ['./grades-view.component.css']
})
export class GradesViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public modalConfig: IModalConfig = {
    title: 'Delete Grades Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          // this.feeService.deleteFeeDetail(this.feeId).subscribe(
          //   (res: any) => {
          //     if (res.message) {
          //       this.modalEvent.toggleClose();
          //       // let toasterConfig: any = {
          //       //   type: 'success',
          //       //   title: res.message,
          //       //   showCloseButton: true,
          //       //   tapToDismiss: true,
          //       //   timeout: 2000
          //       // };
          //       // this.sharedService.setToaster(toasterConfig);
          //       this._router.navigate(['main/examinations/systems-view/list']);
          //     }
          //   },
          //   (err) => {
          //     this.modalEvent.toggleClose();
          //     let toasterConfig: any = {
          //       type: 'error',
          //       title: 'Fee Detail can not be deleted because its link to another data ',
          //       showCloseButton: true,
          //       tapToDismiss: true,
          //       timeout: 3000
          //     };
          //     this._kdalert.error(toasterConfig);
          //   }
          // );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  /* Subscriptions */
  private gradeIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public modalEvent: KdModalEvent,
    private gradesService: GradesService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle('Education - Grades', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/education-structure/grades' },
      { type: 'edit', path: 'main/system-setup/education-structure/grades/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.gradesService.gradeIdChanged.subscribe((gradeId: string) => {
      if (!gradeId) {
        this.router.navigate(['main/system-setup/education-structure/grades']);
      } else {
        this.gradesService.getDetails(gradeId);
      }
    });

    this.gradesService.detailsChanged.subscribe((details: any) => {
      if (details) {
        // TODO - Example of reference type data types in JS
        this.setDetails({ ...details });
      }
    });
  }

  setDetails(details: any) {
    setTimeout(() => {
      this.questionBase.forEach((question) => {
        if (question['key'] == 'visible') {
          // TODO - Example of reference type data types in JS
          details[question['key']] = details[question['key']] ? 'YES' : 'NO';
        }
        if (question['key'] === 'subjects') {
          this.api.setProperty(
            question['key'],
            'row',
            details[question['key']] && details[question['key']].length ? [...details[question['key']]] : []
          );
        } else {
          this.api.setProperty(question['key'], 'value', details[question['key']] ? details[question['key']] : '');
        }
      });
    }, 0);
  }

  open() {
    this.modalEvent.toggleOpen();
  }

  ngOnDestroy(): void {
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    if (this.gradeIdSub) {
      this.gradeIdSub.unsubscribe();
    }
    this.gradesService.resetDetails();
    super.destroyPageBaseSub();
  }
}
