export interface IGradesFilters {
  education_level?: { key: string; value: string; code?: string };
  education_programme?: { key: string; value: string; code?: string };
}

export interface IAddGradesPayload {
  name: string;
  code: string;
  education_stage: number;
  admission_age: number;
  education_programme: number;
}

export interface IUpdateGradesPayload {
  id: string;
  name: string;
  code: string;
  education_stage: number;
  admission_age: number;
  education_programme: number;
  visible: number;
}
