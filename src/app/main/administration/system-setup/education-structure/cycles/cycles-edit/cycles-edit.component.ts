import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { CyclesService } from '../cycles.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './cycles-edit.config';
@Component({
  selector: 'app-cycles-edit',
  templateUrl: './cycles-edit.component.html',
  styleUrls: ['./cycles-edit.component.css']
})
export class CyclesEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public cycleId: any;
  private visibleOptions: Array<any> = [
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];

  /* Subscriptions */
  private cycleIdSub: Subscription;
  private cycleDetailsSub: Subscription;
  private educationLevelSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private cyclesService: CyclesService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Cycles', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.cyclesService.getFilterEducationLevel();
    this.initializeSubscriptions();

    this.cycleIdSub = this.cyclesService.cycleIdChanged.subscribe((cycleId) => {
      if (!cycleId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/cycles/list']);
        }, 0);
      } else {
        this.setDropdownOptions('visible', this.visibleOptions);
        this.cycleId = cycleId;
        this.cyclesService.getCycleEditDetails(this.cycleId);
        this.cycleDetailsSub = this.cyclesService.cycleEditDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setCycleDetails(data[0]);
          }
        });
      }
    });
  }

  setCycleDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'visible') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i]['key']]['key']);
          } else if (this._questionBase[i].key == 'education_level') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i]['key']]['key']);
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  initializeSubscriptions() {
    this.educationLevelSub = this.cyclesService.filterEducationLevelChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        this._updateView.setInputProperty('education_level', 'options', tempOption);
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        name: event.name,
        visible: event.visible,
        admission_age: event.admission_age,
        education_level_id: event.education_level
      };
      this.cyclesService.updateCycleDetails(this.cycleId, payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/cycles/view']);
  }

  ngOnDestroy(): void {
    if (this.cycleIdSub) {
      this.cycleIdSub.unsubscribe;
    }
    if (this.cycleDetailsSub) {
      this.cycleDetailsSub.unsubscribe();
    }
    if (this.educationLevelSub) {
      this.educationLevelSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
