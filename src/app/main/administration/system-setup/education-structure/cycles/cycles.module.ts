import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { CyclesRoutingModule } from './cycles-routing.module';
import { CyclesViewComponent } from './cycles-view/cycles-view.component';
import { CyclesListComponent } from './cycles-list/cycles-list.component';
import { CyclesEditComponent } from './cycles-edit/cycles-edit.component';
import { CyclesService } from './cycles.service';
import { CyclesDataService } from './cycles-data.service';
import { CyclesAddComponent } from './cycles-add/cycles-add.component';

@NgModule({
  imports: [CommonModule, CyclesRoutingModule, SharedModule],
  declarations: [CyclesViewComponent, CyclesListComponent, CyclesEditComponent, CyclesAddComponent],
  providers: [CyclesService, CyclesDataService]
})
export class CyclesModule {}
