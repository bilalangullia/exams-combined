import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { CyclesDataService } from './cycles-data.service';
import { IFilters } from './cycles.interfaces';

@Injectable()
export class CyclesService {
  private filterEducationLevel: any;
  private cycleId: string = null;
  private cycleViewDetails: any = [];
  private cycleEditDetails: any = [];
  private currentFilters: any = {};

  public filterEducationLevelChanged = new Subject();
  public cycleIdChanged = new BehaviorSubject<string>(null);
  public cycleViewDetailsChanged = new BehaviorSubject<any>(this.cycleViewDetails);
  public cycleEditDetailsChanged = new BehaviorSubject<any>(this.cycleEditDetails);
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);

  constructor(
    private router: Router,
    private cyclesDataService: CyclesDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | FILTERS | EducationLevel***/
  getFilterEducationLevel() {
    this.cyclesDataService.getOptionsEducationLevel().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.filterEducationLevel = res.data;
        this.filterEducationLevelChanged.next([...this.filterEducationLevel]);
      } else {
        this.filterEducationLevel = [];
        this.filterEducationLevelChanged.next([...this.filterEducationLevel]);
      }
      (err) => {
        this.filterEducationLevel = [];
        this.filterEducationLevelChanged.next([...this.filterEducationLevel]);
      };
    });
  }

  /*** Set Current filters ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }

  /*** LIST PAGE | List Data get total Records ***/
  getTotalRecords(educationLevelId: number, start?: number, end?: number) {
    return this.cyclesDataService.getList(educationLevelId, start, end);
  }

  /*** LIST PAGE | List Data ***/
  getList(educationLevelId: number, start?: number, end?: number) {
    return this.cyclesDataService.getList(educationLevelId, start, end);
  }

  /*** List Data | SEARCH ***/
  searchList(keyword: string, educationLevelId: number, start?: number, end?: number) {
    return this.cyclesDataService.getListSearch(keyword, educationLevelId, start, end);
  }

  /*** List Page | Set Cycle Id***/
  setCycleId(cycleId: string) {
    this.cycleId = cycleId;
    this.cycleIdChanged.next(this.cycleId);
  }

  /*** VIEW | Cycle Details ***/
  getCycleViewDetails(cycle_id) {
    this.cyclesDataService.getCycleViewDetails(cycle_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.cycleViewDetails = res.data;
          this.cycleViewDetailsChanged.next([...this.cycleViewDetails]);
        } else {
          this.cycleViewDetails = [];
          this.cycleViewDetailsChanged.next([...this.cycleViewDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.cycleViewDetails = [];
        this.cycleViewDetailsChanged.next([...this.cycleViewDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT | Cycle Details ***/
  getCycleEditDetails(cycle_id) {
    this.cyclesDataService.getCycleEditDetails(cycle_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.cycleEditDetails = res.data;
          this.cycleEditDetailsChanged.next([...this.cycleEditDetails]);
        } else {
          this.cycleEditDetails = [];
          this.cycleEditDetailsChanged.next([...this.cycleEditDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.cycleEditDetails = [];
        this.cycleEditDetailsChanged.next([...this.cycleEditDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE SUBMIT  ***/
  updateCycleDetails(cycle_id, data) {
    this.cyclesDataService.updateCycleDetails(cycle_id, data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/education-structure/cycles/list']);
          }, 500);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD PAGE SUBMIT ***/
  addCycleDetails(data) {
    this.cyclesDataService.addCycleDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/education-structure/cycles/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteCycleDetails(cycle_id) {
    return this.cyclesDataService.deleteCycleDetails(cycle_id);
  }
}
