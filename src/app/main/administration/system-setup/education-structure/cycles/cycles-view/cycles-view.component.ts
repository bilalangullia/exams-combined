import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../../shared/shared.toasters';
import { CyclesService } from '../cycles.service';
import { VIEWNODE_INPUT, IModalConfig } from './cycles-view.config';

@Component({
  selector: 'app-cycles-view',
  templateUrl: './cycles-view.component.html',
  styleUrls: ['./cycles-view.component.css'],
  providers: [KdModalEvent]
})
export class CyclesViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public cycleId: any;

  /* Subscriptions */
  private cycleIdSub: Subscription;
  private cycleDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Cycles Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          // this.feeService.deleteFeeDetail(this.feeId).subscribe(
          //   (res: any) => {
          //     if (res.message) {
          //       this._modelEvent.toggleClose();
          //       // let toasterConfig: any = {
          //       //   type: 'success',
          //       //   title: res.message,
          //       //   showCloseButton: true,
          //       //   tapToDismiss: true,
          //       //   timeout: 2000
          //       // };
          //       // this.sharedService.setToaster(toasterConfig);
          //       this._router.navigate(['main/examinations/systems-view/list']);
          //     }
          //   },
          //   (err) => {
          //     this._modelEvent.toggleClose();
          //     let toasterConfig: any = {
          //       type: 'error',
          //       title: 'Fee Detail can not be deleted because its link to another data ',
          //       showCloseButton: true,
          //       tapToDismiss: true,
          //       timeout: 3000
          //     };
          //     this._kdalert.error(toasterConfig);
          //   }
          // );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private sharedService: SharedService,
    public _modelEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private cyclesService: CyclesService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Education - Cycles', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/education-structure/cycles/list' },
      { type: 'edit', path: 'main/system-setup/education-structure/cycles/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.cycleIdSub = this.cyclesService.cycleIdChanged.subscribe((cycleId) => {
      if (!cycleId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/cycles/list']);
        }, 0);
      } else {
        this.cycleId = cycleId;
        this.cyclesService.getCycleViewDetails(this.cycleId);
        this.cycleDetailsSub = this.cyclesService.cycleViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setCycleDetails(data[0]);
          }
        });
      }
    });
  }

  setCycleDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !details[this._questionBase[i].key]
              ? ''
              : details[this._questionBase[i].key].value
              ? details[this._questionBase[i].key].value
              : details[this._questionBase[i].key]
          );
        }
      });
    }
  }

  open() {
    this._modelEvent.toggleOpen();
  }

  ngOnDestroy(): void {
    if (this.cycleIdSub) {
      this.cycleIdSub.unsubscribe;
    }
    if (this.cycleDetailsSub) {
      this.cycleDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
