interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Visible?: Column;
  Name?: Column;
  AdmissionAge: Column;
  EducationLevel: Column;
}

export const TABLECOLUMN: ListColumn = {
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  AdmissionAge: {
    headerName: 'Admission Age',
    field: 'admission_age',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationLevel: {
    headerName: 'Education Level',
    field: 'education_level',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};
export const FILTER_INPUTS: Array<any> = [
  {
    key: 'education_level',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '-- Select  --'
      }
    ],
    events: true
  }
];
