import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter,
  ITableDatasourceParams,
  KdTableDatasourceEvent
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { CyclesService } from '../cycles.service';
import { IFilters } from '../cycles.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './cycles-list.config';

@Component({
  selector: 'app-cycles-list',
  templateUrl: './cycles-list.component.html',
  styleUrls: ['./cycles-list.component.css'],
  providers: [KdTableEvent]
})
export class CyclesListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('filters') filter: KdFilter;
  public filterInputs = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'cycleList';
  readonly PAGESIZE: number = 20;

  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public currentFilters: IFilters;
  public showTable: boolean = false;
  private tableEventList: any;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  private tableEventSub: Subscription;
  private educationLevelFilterSub: Subscription;
  private currentFiltersSub: Subscription;

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Visible,
    TABLECOLUMN.Name,
    TABLECOLUMN.AdmissionAge,
    TABLECOLUMN.EducationLevel
  ];

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.cyclesService.setCycleId(_rowNode['data']['id']);
            this.router.navigate(['main/system-setup/education-structure/cycles/view']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    private cyclesService: CyclesService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
    super.setPageTitle('Education - Cycles', false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/system-setup/education-structure/cycles/add' }]);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.cyclesService.resetFilters();
    this.cyclesService.getFilterEducationLevel();
    this.educationLevelFilterSub = this.cyclesService.filterEducationLevelChanged.subscribe((options: Array<any>) => {
      if (options && options.length) {
        this.setDropdownOptions('education_level', options);
      }
    });

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });

    this.currentFiltersSub = this.cyclesService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (filters && filters['education_level'] && filters['education_level']['key']) {
        this.currentFilters = { ...filters };
        this.showTable = false;
        this.cyclesService.getTotalRecords(filters['education_level']['key'], 0, 1).subscribe(
          (res: any) => {
            this.showTable = true;
            if (res && res['data'] && res['data']['total']) {
              this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
              this.showTable = true;
            } else {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          },
          (err) => {
            this.tableConfig.paginationConfig.total = 0;
            this.showTable = false;
          }
        );
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<{ key: number; value: string }> = [];
    options = data.map((item) => ({ key: item['id'], value: item['name'] }));
    this.filter.setInputProperty(key, 'options', options);
    setTimeout(() => {
      this.filter.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      if (
        this.currentFilters &&
        this.currentFilters['education_level'] &&
        this.currentFilters['education_level']['key']
      ) {
        listReq = this.cyclesService.searchList(
          searchKey,
          this.currentFilters['education_level']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    } else {
      if (
        this.currentFilters &&
        this.currentFilters['education_level'] &&
        this.currentFilters['education_level']['key']
      ) {
        listReq = this.cyclesService.getList(
          this.currentFilters['education_level']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    }
    listReq.toPromise().then(
      (res: any) => {
        this.loading = false;
        if (res && res['data'] && res['data']['cycleRecords'] && res['data']['cycleRecords'].length) {
          list = res['data']['cycleRecords'];
          total = this.searchKey && this.searchKey.length ? res['data']['cycleRecords'].length : res['data']['total'];
          list.forEach((item) => {
            item['visible'] = item['visible'] == 1 ? 'Yes' : 'No';
          });
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  detectValue(event) {
    let filter: IFilters;
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj) {
      filter = { [event['key']]: filterObj };
      this.cyclesService.setCurrentFilters(filter);
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.educationLevelFilterSub) {
      this.educationLevelFilterSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
