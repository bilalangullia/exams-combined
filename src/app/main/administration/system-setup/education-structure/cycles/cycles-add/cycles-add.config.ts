export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'education_level',
    label: 'Education Level',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: '',
        value: '--Select'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'admission_age',
    label: 'Admission Age',
    visible: true,
    required: true,
    controlType: 'integer',
    type: 'number',
    min: 1,
    max: 100
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
