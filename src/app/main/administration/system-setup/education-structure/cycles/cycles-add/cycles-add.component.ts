import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { CyclesService } from '../cycles.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './cycles-add.config';
@Component({
  selector: 'app-cycles-add',
  templateUrl: './cycles-add.component.html',
  styleUrls: ['./cycles-add.component.css']
})
export class CyclesAddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  /* Subscriptions */
  private educationLevelFilterSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private cyclesService: CyclesService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Cycles', false);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.cyclesService.getFilterEducationLevel();
    this.initializeSubscriptions();
  }

  initializeSubscriptions() {
    this.educationLevelFilterSub = this.cyclesService.filterEducationLevelChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_level', 'options', tempOption);
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        name: event.name,
        admission_age: event.admission_age,
        education_level_id: event.education_level
      };
      this.cyclesService.addCycleDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/cycles/list']);
  }

  ngOnDestroy(): void {
    if (this.educationLevelFilterSub) {
      this.educationLevelFilterSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
