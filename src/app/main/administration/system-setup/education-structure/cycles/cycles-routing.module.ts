import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CyclesListComponent } from './cycles-list/cycles-list.component';
import { CyclesViewComponent } from './cycles-view/cycles-view.component';
import { CyclesEditComponent } from './cycles-edit/cycles-edit.component';
import { CyclesAddComponent } from './cycles-add/cycles-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: CyclesListComponent },
  { path: 'view', component: CyclesViewComponent },
  { path: 'edit', component: CyclesEditComponent },
  { path: 'add', component: CyclesAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CyclesRoutingModule {}
