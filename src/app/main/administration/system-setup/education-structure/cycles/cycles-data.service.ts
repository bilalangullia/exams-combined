import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class CyclesDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /** EDUCATION LEVEL DROPDOWN **/
  getOptionsEducationLevel() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.educationLevel}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /***  LIST API ***/
  getList(educationLevelId: number, start?: number, end?: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${educationLevelId}/${urls.cyclesList}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(keyword: string, educationLevelId: number, start?: number, end?: number) {
    //openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/5/education-cycle-list?start=0&end=2&keyword=Education
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${educationLevelId}/${urls.cyclesList}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getCycleViewDetails(cycle_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${cycle_id}/${urls.cycleView}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | EDIT | API  ***/
  getCycleEditDetails(cycle_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${cycle_id}/${urls.cycleEdit}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW | DELETE API **/
  deleteCycleDetails(cycle_id) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${cycle_id}/destroy-education-system`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateCycleDetails(cycle_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${cycle_id}/${urls.cycleUpdate}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ADD | SUBMIT API  **/
  addCycleDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${urls.cycleAdd}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
