import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SystemsListComponent } from './systems-list/systems-list.component';
import { SystemsViewComponent } from './systems-view/systems-view.component';
import { SystemsEditComponent } from './systems-edit/systems-edit.component';
import { SystemsAddComponent } from './systems-add/systems-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: SystemsListComponent },
  { path: 'view', component: SystemsViewComponent },
  { path: 'edit', component: SystemsEditComponent },
  { path: 'add', component: SystemsAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemsRoutingModule {}
