import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { SystemsService } from '../systems.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './systems-edit.config';

@Component({
  selector: 'app-systems-edit',
  templateUrl: './systems-edit.component.html',
  styleUrls: ['./systems-edit.component.css']
})
export class SystemsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public systemId: any;
  private visibleOptions: Array<any> = [
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];

  /* Subscriptions */
  private systemIdSub: Subscription;
  private systemDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private systemsService: SystemsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Systems', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.systemIdSub = this.systemsService.systemIdChanged.subscribe((systemId) => {
      if (!systemId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/systems/list']);
        }, 0);
      } else {
        this.setDropdownOptions('visible', this.visibleOptions);
        this.systemId = systemId;
        this.systemsService.getSystemDetails(this.systemId);
        this.systemDetailsSub = this.systemsService.systemViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setSystemDetails(data[0]);
          }
        });
      }
    });
  }

  setSystemDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'visible') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i]['key']]['key']);
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        name: event.name,
        visible: event.visible
      };
      this.systemsService.updateSystemDetails(this.systemId, payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/systems/view']);
  }

  ngOnDestroy(): void {
    if (this.systemIdSub) {
      this.systemIdSub.unsubscribe();
    }
    if (this.systemDetailsSub) {
      this.systemDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
