import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../../shared/shared.toasters';
import { SystemsService } from '../systems.service';
import { VIEWNODE_INPUT, IModalConfig } from './systems-view.config';

@Component({
  selector: 'app-systems-view',
  templateUrl: './systems-view.component.html',
  styleUrls: ['./systems-view.component.css'],
  providers: [KdModalEvent]
})
export class SystemsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public systemId: any;

  /* Subscriptions */
  private systemIdSub: Subscription;
  private systemDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete System Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.systemsService.deleteSystemDetails(this.systemId).subscribe(
            (res: any) => {
              if (res.message) {
                this._modelEvent.toggleClose();
                this._router.navigate(['main/system-setup/education-structure/systems/list']);
              }
            },
            (err) => {
              this._modelEvent.toggleClose();
              let toasterConfig: any = {
                type: 'error',
                title: 'Detail can not be deleted because its link to another data ',
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 3000
              };
              this._kdalert.error(toasterConfig);
            }
          );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private sharedService: SharedService,
    public _modelEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private systemsService: SystemsService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Education - Systems', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/education-structure/systems/list' },
      { type: 'edit', path: 'main/system-setup/education-structure/systems/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.systemIdSub = this.systemsService.systemIdChanged.subscribe((systemId) => {
      if (!systemId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/systems/list']);
        }, 0);
      } else {
        this.systemId = systemId;
        this.systemsService.getSystemDetails(this.systemId);
        this.systemDetailsSub = this.systemsService.systemViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setSystemDetails(data[0]);
          }
        });
      }
    });
  }

  setSystemDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'visible') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i]['key']]['value']);
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  open() {
    this._modelEvent.toggleOpen();
  }

  ngOnDestroy(): void {
    if (this.systemIdSub) {
      this.systemIdSub.unsubscribe();
    }
    if (this.systemDetailsSub) {
      this.systemDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
