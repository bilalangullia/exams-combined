import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { SystemsDataService } from './systems-data.service';
@Injectable()
export class SystemsService {
  private systemId: string = null;
  private systemViewDetails: any = [];

  public systemIdChanged = new BehaviorSubject<string>(null);
  public systemViewDetailsChanged = new BehaviorSubject<any>(this.systemViewDetails);

  constructor(
    private router: Router,
    private systemsDataService: SystemsDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | List Data ***/
  getList(start?: number, end?: number) {
    return this.systemsDataService.getList(start, end);
  }

  /*** List Data | SEARCH***/
  searchList(keyword: string, start?: number, end?: number) {
    return this.systemsDataService.getListSearch(keyword, start, end);
  }

  /*** List Page | Set System Id***/
  setSystemId(systemId: string) {
    this.systemId = systemId;
    this.systemIdChanged.next(this.systemId);
  }

  /*** VIEW PAGE | System Details Data ***/
  getSystemDetails(system_id) {
    this.systemsDataService.getSystemDetails(system_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.systemViewDetails = res.data;
          this.systemViewDetailsChanged.next([...this.systemViewDetails]);
        } else {
          this.systemViewDetails = [];
          this.systemViewDetailsChanged.next([...this.systemViewDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.systemViewDetails = [];
        this.systemViewDetailsChanged.next([...this.systemViewDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE SUBMIT ***/
  updateSystemDetails(system_id, data) {
    this.systemsDataService.updateSystemDetails(system_id, data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/education-structure/systems/list']);
          }, 500);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD PAGE SUBMIT ***/
  addSystemDetails(data) {
    this.systemsDataService.addSystemDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/education-structure/systems/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveSuccess);
      }
    );
  }

  /*** DELETE ***/
  deleteSystemDetails(system_id) {
    return this.systemsDataService.deleteSystemDetails(system_id);
  }
}
