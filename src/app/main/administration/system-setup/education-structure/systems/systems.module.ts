import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { SystemsRoutingModule } from './systems-routing.module';
import { SystemsListComponent } from './systems-list/systems-list.component';
import { SystemsViewComponent } from './systems-view/systems-view.component';
import { SystemsEditComponent } from './systems-edit/systems-edit.component';
import { SystemsService } from './systems.service';
import { SystemsDataService } from './systems-data.service';
import { SystemsAddComponent } from './systems-add/systems-add.component';

@NgModule({
  imports: [CommonModule, SystemsRoutingModule, SharedModule],
  declarations: [SystemsListComponent, SystemsViewComponent, SystemsEditComponent, SystemsAddComponent],
  providers: [SystemsService, SystemsDataService]
})
export class SystemsModule {}
