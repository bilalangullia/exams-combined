const ROUTER_BASE_PATH: string = 'main/system-setup/education-structure';

interface IKdTabs {
  tabName?: string;
  tabContent?: string;
  tabId?: string;
  isActive?: boolean;
  disabled?: boolean;
  routerPath?: string;
}

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'Systems', routerPath: ROUTER_BASE_PATH + '/systems' },
  { tabName: 'Levels', routerPath: ROUTER_BASE_PATH + '/levels' },
  { tabName: 'Cycles', routerPath: ROUTER_BASE_PATH + '/cycles' },
  { tabName: 'Programmes', routerPath: ROUTER_BASE_PATH + '/programmes' },
  { tabName: 'Grades', routerPath: ROUTER_BASE_PATH + '/grades' },
  { tabName: 'Grade Subjects', routerPath: ROUTER_BASE_PATH + '/grade-subjects' },
  { tabName: 'Setup', routerPath: ROUTER_BASE_PATH + '/setup' }
];
