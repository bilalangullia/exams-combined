import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { LevelsRoutingModule } from './levels-routing.module';
import { LevelsEditComponent } from './levels-edit/levels-edit.component';
import { LevelsViewComponent } from './levels-view/levels-view.component';
import { LevelsListComponent } from './levels-list/levels-list.component';
import { LevelsService } from './levels.service';
import { LevelsDataService } from './levels-data.service';
import { LevelsAddComponent } from './levels-add/levels-add.component';
@NgModule({
  imports: [CommonModule, LevelsRoutingModule, SharedModule],
  declarations: [LevelsEditComponent, LevelsViewComponent, LevelsListComponent, LevelsAddComponent],
  providers: [LevelsService, LevelsDataService]
})
export class LevelsModule {}
