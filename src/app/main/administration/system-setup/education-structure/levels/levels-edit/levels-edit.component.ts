import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { LevelsService } from '../levels.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './levels-edit.config';

@Component({
  selector: 'app-levels-edit',
  templateUrl: './levels-edit.component.html',
  styleUrls: ['./levels-edit.component.css']
})
export class LevelsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public levelId: any;
  private visibleOptions: Array<any> = [
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];

  /* Subscriptions */
  private levelIdSub: Subscription;
  private levelDetailsSub: Subscription;
  private educationSystemSub: Subscription;
  private educationIscedSub: Subscription;
  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private levelsService: LevelsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Levels', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.levelsService.getFilterEducationSystem();
    this.levelsService.getFilterEducationIsced();
    this.initializeSubscriptions();

    this.levelIdSub = this.levelsService.levelIdChanged.subscribe((levelId) => {
      if (!levelId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/levels/list']);
        }, 0);
      } else {
        this.setDropdownOptions('visible', this.visibleOptions);
        this.levelId = levelId;
        this.levelsService.getLevelDetails(this.levelId);
        this.levelDetailsSub = this.levelsService.levelViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setLevelDetails(data[0]);
          }
        });
      }
    });
  }

  setLevelDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'visible') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i]['key']]['key']);
          } else if (this._questionBase[i].key == 'education_system') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i]['key']]['key']);
          } else if (this._questionBase[i].key == 'education_level_isced') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i]['key']]['key']);
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  initializeSubscriptions() {
    this.educationSystemSub = this.levelsService.filterEducationSystemChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        this._updateView.setInputProperty('education_system', 'options', tempOption);
      }
    });

    this.educationIscedSub = this.levelsService.filterEducationIscedChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        this._updateView.setInputProperty('education_level_isced', 'options', tempOption);
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        education_system_id: event.education_system,
        name: event.name,
        visible: event.visible,
        education_level_isced_id: event.education_level_isced
      };
      this.levelsService.updateLevelDetails(this.levelId, payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/levels/view']);
  }

  ngOnDestroy(): void {
    if (this.levelIdSub) {
      this.levelIdSub.unsubscribe();
    }
    if (this.levelDetailsSub) {
      this.levelDetailsSub.unsubscribe();
    }
    if (this.educationSystemSub) {
      this.educationSystemSub.unsubscribe();
    }
    if (this.educationIscedSub) {
      this.educationIscedSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
