export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'education_system',
    label: 'Education System',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select--'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'visible',
    label: 'Visible',
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select--'
      }
    ],
    events: true
  },
  {
    key: 'education_level_isced',
    label: 'Education Level Isced',
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--Select--'
      }
    ],
    events: true,
    required: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
