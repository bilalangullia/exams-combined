import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../.././../../../environments/environment';
import urls from '../../../../../shared/config.urls';
@Injectable()
export class LevelsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /** EDUCATION SYSTEM DROPDOWN **/
  getOptionsEducationSystem() {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/edu-sys-drpdwn`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST API ***/
  getList(educationSystemId: number, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${urls.levelsList}?education_system_id=${educationSystemId}start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(keyword: string, educationSystemId: number, start?: number, end?: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${urls.levelsList}?education_system_id=${educationSystemId}start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getLevelDetails(level_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${level_id}/${urls.levelView}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW | DELETE API **/
  deleteLevelDetails(level_id) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${level_id}/destroy-education-system`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** EDIT | EDUCATION  Isced Dropdown **/
  getOptionsEducationIsced() {
    http: return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/edu-lvlIsc-drpdwn`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateLevelDetails(level_id, data) {
    return this.httpClient
      .put(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${level_id}/${urls.levelUpdate}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ADD | SUBMIT API  **/
  addLevelDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.educationStructure}/${urls.levelAdd}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
