import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { LevelsDataService } from './levels-data.service';
import { IFilters } from './levels.interfaces';
@Injectable()
export class LevelsService {
  private filterEducationSystem: any;
  private levelId: string = null;
  private levelViewDetails: any = [];
  private currentFilters: any = {};
  private filterEducationIsced: any;

  public filterEducationSystemChanged = new Subject();
  public levelIdChanged = new BehaviorSubject<string>(null);
  public levelViewDetailsChanged = new BehaviorSubject<any>(this.levelViewDetails);
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  public filterEducationIscedChanged = new Subject();

  constructor(
    private router: Router,
    private levelsDataService: LevelsDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | FILTERS | Education Level***/
  getFilterEducationSystem() {
    this.levelsDataService.getOptionsEducationSystem().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.filterEducationSystem = res.data;
        this.filterEducationSystemChanged.next([...this.filterEducationSystem]);
      } else {
        this.filterEducationSystem = [];
        this.filterEducationSystemChanged.next([...this.filterEducationSystem]);
      }
      (err) => {
        this.filterEducationSystem = [];
        this.filterEducationSystemChanged.next([...this.filterEducationSystem]);
      };
    });
  }

  /*** Set Current filters ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }

  /*** LIST PAGE | List Data get total Records ***/
  getTotalRecords(educationSystemId: number, start?: number, end?: number) {
    return this.levelsDataService.getList(educationSystemId, start, end);
  }

  /*** LIST PAGE | List Data ***/
  getList(educationSystemId: number, start?: number, end?: number) {
    return this.levelsDataService.getList(educationSystemId, start, end);
  }

  /*** List Data | SEARCH***/
  searchList(keyword: string, educationSystemId: number, start?: number, end?: number) {
    return this.levelsDataService.getListSearch(keyword, educationSystemId, start, end);
  }

  /*** List Page | Set Level Id***/
  setLevelId(levelId: string) {
    this.levelId = levelId;
    this.levelIdChanged.next(this.levelId);
  }

  /*** VIEW PAGE | Level Details ***/
  getLevelDetails(level_id) {
    this.levelsDataService.getLevelDetails(level_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.levelViewDetails = res.data;
          this.levelViewDetailsChanged.next([...this.levelViewDetails]);
        } else {
          this.levelViewDetails = [];
          this.levelViewDetailsChanged.next([...this.levelViewDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.levelViewDetails = [];
        this.levelViewDetailsChanged.next([...this.levelViewDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** LIST PAGE | FILTERS | EducationLevel***/
  getFilterEducationIsced() {
    this.levelsDataService.getOptionsEducationIsced().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.filterEducationIsced = res.data;
        this.filterEducationIscedChanged.next([...this.filterEducationIsced]);
      } else {
        this.filterEducationIsced = [];
        this.filterEducationIscedChanged.next([...this.filterEducationIsced]);
      }
      (err) => {
        this.filterEducationIsced = [];
        this.filterEducationIscedChanged.next([...this.filterEducationIsced]);
      };
    });
  }

  /*** EDIT PAGE SUBMIT ***/
  updateLevelDetails(level_id, data) {
    this.levelsDataService.updateLevelDetails(level_id, data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/education-structure/levels/list']);
          }, 500);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD **/
  addLevelDetails(data) {
    this.levelsDataService.addLevelDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/education-structure/levels/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteLevelDetails(level_id) {
    return this.levelsDataService.deleteLevelDetails(level_id);
  }
}
