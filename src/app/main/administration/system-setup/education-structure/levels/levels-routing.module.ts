import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LevelsListComponent } from './levels-list/levels-list.component';
import { LevelsViewComponent } from './levels-view/levels-view.component';
import { LevelsEditComponent } from './levels-edit/levels-edit.component';
import { LevelsAddComponent } from './levels-add/levels-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: LevelsListComponent },
  { path: 'view', component: LevelsViewComponent },
  { path: 'edit', component: LevelsEditComponent },
  { path: 'add', component: LevelsAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LevelsRoutingModule {}
