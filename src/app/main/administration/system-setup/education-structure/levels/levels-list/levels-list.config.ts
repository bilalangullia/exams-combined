interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Visible?: Column;
  Name?: Column;
  EducationSystem: Column;
  EducationLevelIsced: Column;
  /*   Reorder: Column; */
}

export const TABLECOLUMN: ListColumn = {
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationSystem: {
    headerName: 'Education System',
    field: 'education_system',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationLevelIsced: {
    headerName: 'Education Level Isced',
    field: 'education_level_isced',
    sortable: true,
    filterable: true,
    filterValue: []
  } /* ,
  Reorder: {
    headerName: 'Reorder',
    field: 'reorder',
    sortable: true,
    filterable: true,
    filterValue: []
  } */
};
export const FILTER_INPUTS: Array<any> = [
  {
    key: 'education_system',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '-- Select  --'
      }
    ],
    events: true
  }
];
