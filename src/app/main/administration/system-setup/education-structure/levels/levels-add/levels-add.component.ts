import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { LevelsService } from '../levels.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './levels-add.config';

@Component({
  selector: 'app-levels-add',
  templateUrl: './levels-add.component.html',
  styleUrls: ['./levels-add.component.css']
})
export class LevelsAddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public levelId: any;

  /* Subscriptions */
  private educationSystemSub: Subscription;
  private educationIscedSub: Subscription;
  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    private levelsService: LevelsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Education - Levels', false);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.levelsService.getFilterEducationSystem();
    this.levelsService.getFilterEducationIsced();
    this.initializeSubscriptions();
  }

  initializeSubscriptions() {
    this.educationSystemSub = this.levelsService.filterEducationSystemChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_system', 'options', tempOption);
      }
    });

    this.educationIscedSub = this.levelsService.filterEducationIscedChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.name });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('education_level_isced', 'options', tempOption);
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        education_system_id: event.education_system,
        name: event.name,
        education_level_isced_id: event.education_level_isced
      };
      console.log(payload);

      this.levelsService.addLevelDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/system-setup/education-structure/levels/list']);
  }

  ngOnDestroy(): void {
    if (this.educationSystemSub) {
      this.educationSystemSub.unsubscribe();
    }
    if (this.educationIscedSub) {
      this.educationIscedSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
