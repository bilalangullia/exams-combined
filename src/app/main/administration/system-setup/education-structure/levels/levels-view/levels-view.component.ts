import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../../shared/shared.toasters';
import { LevelsService } from '../levels.service';
import { VIEWNODE_INPUT, IModalConfig } from './levels-view.config';

@Component({
  selector: 'app-levels-view',
  templateUrl: './levels-view.component.html',
  styleUrls: ['./levels-view.component.css'],
  providers: [KdModalEvent]
})
export class LevelsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public levelId: any;

  /* Subscriptions */
  private levelIdSub: Subscription;
  private levelDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Levels Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          // this.feeService.deleteFeeDetail(this.feeId).subscribe(
          //   (res: any) => {
          //     if (res.message) {
          //       this._modelEvent.toggleClose();
          //       // let toasterConfig: any = {
          //       //   type: 'success',
          //       //   title: res.message,
          //       //   showCloseButton: true,
          //       //   tapToDismiss: true,
          //       //   timeout: 2000
          //       // };
          //       // this.sharedService.setToaster(toasterConfig);
          //       this._router.navigate(['main/examinations/systems-view/list']);
          //     }
          //   },
          //   (err) => {
          //     this._modelEvent.toggleClose();
          //     let toasterConfig: any = {
          //       type: 'error',
          //       title: 'Fee Detail can not be deleted because its link to another data ',
          //       showCloseButton: true,
          //       tapToDismiss: true,
          //       timeout: 3000
          //     };
          //     this._kdalert.error(toasterConfig);
          //   }
          // );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private sharedService: SharedService,
    public _modelEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private levelsService: LevelsService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Education - Levels', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/education-structure/levels/list' },
      { type: 'edit', path: 'main/system-setup/education-structure/levels/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.levelIdSub = this.levelsService.levelIdChanged.subscribe((levelId) => {
      if (!levelId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/system-setup/education-structure/levels/list']);
        }, 0);
      } else {
        this.levelId = levelId;
        this.levelsService.getLevelDetails(this.levelId);
        this.levelDetailsSub = this.levelsService.levelViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setLevelDetails(data[0]);
          }
        });
      }
    });
  }

  setLevelDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !details[this._questionBase[i].key]
              ? ''
              : details[this._questionBase[i].key].value
              ? details[this._questionBase[i].key].value
              : details[this._questionBase[i].key]
          );
        }
      });
    }
  }

  open() {
    this._modelEvent.toggleOpen();
  }

  ngOnDestroy(): void {
    if (this.levelIdSub) {
      this.levelIdSub.unsubscribe();
    }
    if (this.levelDetailsSub) {
      this.levelDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
