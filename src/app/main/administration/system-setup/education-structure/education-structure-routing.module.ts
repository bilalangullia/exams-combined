import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntryComponent } from './entry/entry.component';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
    children: [
      { path: '', redirectTo: 'systems' },
      { path: 'systems', loadChildren: './systems/systems.module#SystemsModule' },
      { path: 'levels', loadChildren: '../education-structure/levels/levels.module#LevelsModule' },
      { path: 'cycles', loadChildren: './cycles/cycles.module#CyclesModule' },
      { path: 'programmes', loadChildren: './programmes/programmes.module#ProgrammesModule' },
      { path: 'grades', loadChildren: './grades/grades.module#GradesModule' },
      { path: 'grade-subjects', loadChildren: './grade-subjects/grade-subjects.module#GradeSubjectsModule' },
      { path: 'setup', loadChildren: './setup/setup.module#SetupModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EducationStructureRoutingModule {}
