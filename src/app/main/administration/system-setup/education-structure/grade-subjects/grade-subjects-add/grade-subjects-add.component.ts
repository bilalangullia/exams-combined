import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { KdPageBase, IDynamicFormApi, KdPageBaseEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { GradeSubjectsService } from '../grade-subjects.service';
import { QUESTION_BASE, FORM_BUTTONS, EMPTY_OPTION } from './add.config';
import { Subscription } from 'rxjs';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { IAddGradeSubjectsPayload } from '../grade-subjects.interfaces';

@Component({
  selector: 'app-grade-subjects-add',
  templateUrl: './grade-subjects-add.component.html',
  styleUrls: ['./grade-subjects-add.component.css']
})
export class GradeSubjectsAddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Education - Grade Subjects';

  public questionBase: any = QUESTION_BASE;
  public formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  /* Subscriptions */
  private optionsLevelSub: Subscription;
  private optionsProgrammeSub: Subscription;
  private optionsGradeSub: Subscription;
  private optionsSubjectSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private gradeSubjectsService: GradeSubjectsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.gradeSubjectsService.getOptionsEducationLevel();

    setTimeout(() => {
      this.optionsLevelSub = this.gradeSubjectsService.optionsEducationLevelChanged.subscribe((options: Array<any>) => {
        if (options) {
          this.setDropdownOptions('education_level', options);
        }
      });

      this.optionsProgrammeSub = this.gradeSubjectsService.optionsEducationProgrammeChanged.subscribe(
        (options: Array<any>) => {
          if (options) {
            this.setDropdownOptions('education_programme', options);
          }
        }
      );

      this.optionsGradeSub = this.gradeSubjectsService.optionsEducationGradeChanged.subscribe((options: Array<any>) => {
        if (options) {
          this.setDropdownOptions('education_grade', options);
        }
      });

      this.optionsSubjectSub = this.gradeSubjectsService.optionsEducationSubjectChanged.subscribe(
        (options: Array<any>) => {
          if (options) {
            this.setDropdownOptions('education_subject', options);
          }
        }
      );
    }, 100);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    options = [...data];
    options.unshift({ key: null, value: '-- Please Select --' });
    this.api.setProperty(key, 'options', [...options]);

    setTimeout(() => {
      this.api.setProperty(key, 'value', null);
      if (data.length) {
        this.api.setProperty(key, 'readonly', false);
      } else {
        this.api.setProperty(key, 'readonly', true);
      }
    }, 0);
  }

  detectValue(event) {
    if (event['value']) {
      if (event['key'] === 'education_level') {
        this.gradeSubjectsService.getOptionsEducationProgramme(event['value']);
        this.api.setProperty('education_programme', 'options', [EMPTY_OPTION]);
        this.api.setProperty('education_programme', 'readonly', true);
        this.api.setProperty('education_grade', 'readonly', true);
        this.api.setProperty('education_subject', 'readonly', true);
      } else if (event['key'] === 'education_programme') {
        this.gradeSubjectsService.getOptionsEducationGrade(event['value']);
        this.api.setProperty('education_grade', 'readonly', true);
        this.api.setProperty('education_subject', 'readonly', true);
      } else if (event['key'] === 'education_grade') {
        this.gradeSubjectsService.getOptionsEducationSubject(event['value']);
        this.api.setProperty('education_subject', 'readonly', true);
      }
    }
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      const toasterConfig: IToasterConfig = {
        title: 'Missing required fields',
        type: 'error',
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload: IAddGradeSubjectsPayload = { ...form, education_grade_id: form['education_grade'] };
      this.gradeSubjectsService.addGradeSubject({ ...payload }).subscribe(
        (res: any) => {
          if (res && res['data'] && !res['data']['error']) {
            let successMsg: IToasterConfig = { title: 'Saved Successfully.', type: 'success', timeout: 1000 };
            this.sharedService.setToaster(successMsg);
            setTimeout(() => {
              this.cancel();
            }, 500);
          } else {
            let errorMsg: IToasterConfig = { title: 'Error while saving', type: 'error', timeout: 1000 };
            this.sharedService.setToaster(errorMsg);
          }
        },
        (err) => {
          let errorMsg: IToasterConfig = { title: 'Error while saving.', type: 'error', timeout: 1000 };
          this.sharedService.setToaster(errorMsg);
          setTimeout(() => {
            this.cancel();
          }, 500);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/education-structure/grade-subjects']);
  }

  ngOnDestroy(): void {
    if (this.optionsLevelSub) {
      this.optionsLevelSub.unsubscribe();
    }
    if (this.optionsProgrammeSub) {
      this.optionsProgrammeSub.unsubscribe();
    }
    if (this.optionsGradeSub) {
      this.optionsGradeSub.unsubscribe();
    }
    if (this.optionsSubjectSub) {
      this.optionsSubjectSub.unsubscribe();
    }
    this.gradeSubjectsService.resetDropdownOptions();
    super.destroyPageBaseSub();
  }
}
