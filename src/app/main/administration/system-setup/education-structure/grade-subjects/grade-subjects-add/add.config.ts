import { IDropdownOptions } from '../../../../../../shared/shared.interfaces';

export const DEFAULT_OPTIONS: Array<IDropdownOptions> = [
  { key: null, value: '-- Please Select --' },
  { key: 1, value: 'YES' },
  { key: 0, value: 'NO' }
];

export const QUESTION_BASE: Array<any> = [
  {
    key: 'education_level',
    label: 'Education Level',
    visible: true,
    controlType: 'dropdown',
    options: [],
    required: true
    // readonly: true
  },
  {
    key: 'education_programme',
    label: 'Education Programme',
    visible: true,
    controlType: 'dropdown',
    options: [],
    required: true,
    readonly: true
  },
  {
    key: 'education_grade',
    label: 'Education Grade',
    visible: true,
    controlType: 'dropdown',
    options: [],
    required: true,
    readonly: true
  },
  {
    key: 'education_subject',
    label: 'Education Subject',
    visible: true,
    controlType: 'dropdown',
    options: [],
    required: true,
    readonly: true
  },
  {
    key: 'hours_required',
    label: 'Hours Required',
    visible: true,
    controlType: 'integer',
    type: 'string',
    events: true
  },
  {
    key: 'auto_allocation',
    label: 'Auto Allocation',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: DEFAULT_OPTIONS,
    events: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];

export const EMPTY_OPTION: IDropdownOptions = { key: null, value: '-- Please Select --' };
