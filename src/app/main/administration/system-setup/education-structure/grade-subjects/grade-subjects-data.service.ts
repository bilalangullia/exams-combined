import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class GradeSubjectsDataService {
  constructor(private http: HttpClient, private router: Router) {}

  /***  Add token to request ** */
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };

  getOptionsEducationLevel() {
    // http://openemis.n2.iworklab.com/api/dropdown/education-level
    return this.http
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.education}-${urls.level}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  getOptionsEducationProgramme(educationLevelId: string) {
    // http://openemis.n2.iworklab.com/api/dropdown/education-program/4
    return this.http
      .get(
        `${environment.baseUrl}/${urls.dropdown}/${urls.education}-${urls.program}/${educationLevelId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getOptionsEducationGrade(educationProgrammeId: string) {
    // http://openemis.n2.iworklab.com/api/dropdown/education-grade/9
    return this.http
      .get(
        `${environment.baseUrl}/${urls.dropdown}/${urls.education}-${urls.grade}/${educationProgrammeId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***   LIST API ***/
  getList(educationGradeId: string, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grade-subject/list/59?start=0&end=20
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}-${urls.subject}/${urls.list}/${educationGradeId}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***   LIST | SEARCH API  ** */
  getListSearch(educationGradeId: string, keyword: string, start: number, end: number) {
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}-${urls.subject}/${urls.list}/${educationGradeId}&start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getAddDetails(gradeId: string) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grade-subject/add/59
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}-${urls.subject}/${urls.add}/${gradeId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getOptionsEducationSubject(gradeId: string) {
    // http://openemis.n2.iworklab.com/api/dropdown/education-subject
    return this.http
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.education}-${urls.subject}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  addGradeSubject(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grade-subject/store
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}-${urls.subject}/store`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  Get Details | VIEW | API  ** */
  getDetails(gradeId: string, subjectId: string) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grade-subject/view/59/1
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}-${urls.subject}/${urls.view}/${gradeId}/${subjectId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateGradeSubjectsDetails(payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/educationstructure/education-grade-subject/update
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.education}${urls.structure}/${urls.education}-${urls.grade}-${urls.subject}/${urls.update}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
}
