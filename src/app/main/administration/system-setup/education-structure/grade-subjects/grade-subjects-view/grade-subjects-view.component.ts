import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent } from 'openemis-styleguide-lib';

import { GradeSubjectsService } from '../grade-subjects.service';
import { QUESTION_BASE } from './grade-subjects-view.config';

import { IGradeSubject } from '../grade-subjects.interfaces';
import { IModalConfig } from '../../../../../../shared/shared.interfaces';

@Component({
  selector: 'app-grade-subjects-view',
  templateUrl: './grade-subjects-view.component.html',
  styleUrls: ['./grade-subjects-view.component.css'],
  providers: [KdModalEvent]
})
export class GradeSubjectsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public questionBase: any = QUESTION_BASE;
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public modalConfig: IModalConfig = {
    title: 'Delete Levels Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          // this.feeService.deleteFeeDetail(this.feeId).subscribe(
          //   (res: any) => {
          //     if (res.message) {
          //       this._modelEvent.toggleClose();
          //       // let toasterConfig: any = {
          //       //   type: 'success',
          //       //   title: res.message,
          //       //   showCloseButton: true,
          //       //   tapToDismiss: true,
          //       //   timeout: 2000
          //       // };
          //       // this.sharedService.setToaster(toasterConfig);
          //       this._router.navigate(['main/examinations/systems-view/list']);
          //     }
          //   },
          //   (err) => {
          //     this._modelEvent.toggleClose();
          //     let toasterConfig: any = {
          //       type: 'error',
          //       title: 'Fee Detail can not be deleted because its link to another data ',
          //       showCloseButton: true,
          //       tapToDismiss: true,
          //       timeout: 3000
          //     };
          //     this._kdalert.error(toasterConfig);
          //   }
          // );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  /* Subscriptions */
  private gradeSubjectIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public modalEvent: KdModalEvent,
    private gradeSubjectsService: GradeSubjectsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Education - Grade Subjects', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/education-structure/grade-subjects' },
      { type: 'edit', path: 'main/system-setup/education-structure/grade-subjects/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  open() {
    this.modalEvent.toggleOpen();
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.gradeSubjectIdSub = this.gradeSubjectsService.gradeSubjectIdChanged.subscribe(
      (gradeSubjectId: IGradeSubject) => {
        if (!gradeSubjectId || !gradeSubjectId['education_grade_id'] || !gradeSubjectId['education_subject_id']) {
          this.router.navigate(['main/system-setup/education-structure/grade-subjects']);
        } else {
          this.gradeSubjectsService.getDetails(
            gradeSubjectId['education_grade_id'],
            gradeSubjectId['education_subject_id']
          );

          setTimeout(() => {
            this.gradeSubjectsService.detailsChanged.subscribe((details: any) => {
              if (details) {
                this.setDetails(details);
              }
            });
          }, 0);
        }
      }
    );
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      if (question['key'] === 'auto_allocation') {
        this.api.setProperty(question['key'], 'value', details[question['key']] ? 'YES' : 'NO');
      } else {
        this.api.setProperty(question['key'], 'value', details[question['key']] ? details[question['key']] : '');
      }
    });
  }

  ngOnDestroy(): void {
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.gradeSubjectsService.resetDetails();

    super.destroyPageBaseSub();
  }
}
