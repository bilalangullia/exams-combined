export const QUESTION_BASE = [
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_subject',
    label: 'Education Subject',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_grade',
    label: 'Education Grade',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_programme',
    label: 'Education Programme',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'education_level',
    label: 'Education Level',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'hours_required',
    label: 'Hours Required',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'auto_allocation',
    label: 'Auto Allocation',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified',
    label: 'Modified On',
    visible: true,
    controlType: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];
