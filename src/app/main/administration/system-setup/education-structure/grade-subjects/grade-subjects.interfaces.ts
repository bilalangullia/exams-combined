export interface IGradeSubjectsFilters {
  education_level?: { key: string; value: string; code?: string };
  education_programme?: { key: string; value: string; code?: string };
  education_grade?: { key: string; value: string; code?: string };
}

export interface IGradeSubject {
  education_grade_id?: string;
  education_subject_id?: string;
}

export interface IAddGradeSubjectsPayload {
  education_level: any;
  education_programme: any;
  education_grade_id: any;
  education_subject: any;
  hours_required?: number;
  auto_allocation?: number;
}

export interface IUpdateGradeSubjectsPayload {
  education_grade_id: any;
  education_subject_id: any;
  auto_allocation: any;
  hours_required: any;
}
