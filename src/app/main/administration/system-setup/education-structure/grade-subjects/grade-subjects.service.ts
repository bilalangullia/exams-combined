import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import { GradeSubjectsDataService } from './grade-subjects-data.service';
import { Router } from '@angular/router';
import { SharedService } from '../../../../../shared/shared.service';
import { IGradeSubjectsFilters, IGradeSubject } from './grade-subjects.interfaces';

@Injectable()
export class GradeSubjectsService {
  private optionsEducationLevel: Array<any> = [];
  private optionsEducationProgramme: Array<any> = [];
  private optionsEducationGrade: Array<any> = [];
  private filters: IGradeSubjectsFilters = null;
  private gradeSubjectId: IGradeSubject = null;
  private details: any = null;
  private optionsEducationSubject: Array<any> = [];
  private addDetails: any = null;

  public optionsEducationLevelChanged = new BehaviorSubject<Array<any>>([...this.optionsEducationLevel]);
  public optionsEducationProgrammeChanged = new BehaviorSubject<Array<any>>([...this.optionsEducationProgramme]);
  public optionsEducationGradeChanged = new BehaviorSubject<Array<any>>([...this.optionsEducationGrade]);
  public filtersChanged = new BehaviorSubject<IGradeSubjectsFilters>({ ...this.filters });
  public gradeSubjectIdChanged = new BehaviorSubject<IGradeSubject>({ ...this.gradeSubjectId });
  public detailsChanged = new BehaviorSubject<string>({ ...this.details });
  public optionsEducationSubjectChanged = new BehaviorSubject<Array<any>>(null);
  public addDetailsChanged = new BehaviorSubject<any>({ ...this.addDetails });

  constructor(
    private router: Router,
    private gradeSubjectsDataService: GradeSubjectsDataService,
    private sharedService: SharedService
  ) {}

  getOptionsEducationLevel() {
    this.gradeSubjectsDataService.getOptionsEducationLevel().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationLevel = res['data'];
          this.optionsEducationLevelChanged.next([...this.optionsEducationLevel]);
        } else {
          this.optionsEducationLevel = [];
          this.optionsEducationLevelChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationLevel = [];
        this.optionsEducationLevelChanged.next([]);
      }
    );
  }

  getOptionsEducationProgramme(levelId: string) {
    this.gradeSubjectsDataService.getOptionsEducationProgramme(levelId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationProgramme = res['data'];
          this.optionsEducationProgrammeChanged.next([...this.optionsEducationProgramme]);
        } else {
          this.optionsEducationProgramme = [];
          this.optionsEducationProgrammeChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationProgramme = [];
        this.optionsEducationProgrammeChanged.next([]);
      }
    );
  }

  getOptionsEducationGrade(programmeId: string) {
    this.gradeSubjectsDataService.getOptionsEducationGrade(programmeId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationGrade = res['data'];
          this.optionsEducationGradeChanged.next([...this.optionsEducationGrade]);
        } else {
          this.optionsEducationGrade = [];
          this.optionsEducationGradeChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationGrade = [];
        this.optionsEducationGradeChanged.next([]);
      }
    );
  }

  setFilters(filter: IGradeSubjectsFilters) {
    this.filters = { ...this.filters, ...filter };
    this.filtersChanged.next({ ...this.filters });
  }

  resetFilters() {
    this.filters = null;
    this.filtersChanged.next(null);
  }

  getList(educationGradeId: string, start: number, end: number) {
    return this.gradeSubjectsDataService.getList(educationGradeId, start, end);
  }

  searchList(educationGradeId: string, keyword: string, start: number, end: number) {
    return this.gradeSubjectsDataService.getListSearch(educationGradeId, keyword, start, end);
  }

  setGradeSubjectId(gradeSubjectId: IGradeSubject) {
    this.gradeSubjectId = gradeSubjectId;
    this.gradeSubjectIdChanged.next({ ...this.gradeSubjectId });
  }

  resetGradeId() {
    this.gradeSubjectId = null;
    this.gradeSubjectIdChanged.next(null);
  }

  getAddDetails(gradeId: string) {
    this.gradeSubjectsDataService.getAddDetails(gradeId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.addDetails = res['data'];
          this.addDetailsChanged.next({ ...this.addDetails });
        } else {
          this.addDetails = null;
          this.addDetailsChanged.next(null);
        }
      },
      (err) => {
        this.addDetails = null;
        this.addDetailsChanged.next(null);
      }
    );
  }

  getOptionsEducationSubject(gradeId: string) {
    this.gradeSubjectsDataService.getOptionsEducationSubject(gradeId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.optionsEducationSubject = res['data'];
          this.optionsEducationSubjectChanged.next([...this.optionsEducationSubject]);
        } else {
          this.optionsEducationSubject = [];
          this.optionsEducationSubjectChanged.next([]);
        }
      },
      (err) => {
        this.optionsEducationSubject = [];
        this.optionsEducationSubjectChanged.next([]);
      }
    );
  }

  resetDropdownOptions() {
    this.optionsEducationLevel = [];
    this.optionsEducationLevelChanged.next([]);
    this.optionsEducationProgramme = [];
    this.optionsEducationProgrammeChanged.next([]);
    this.optionsEducationGrade = [];
    this.optionsEducationGradeChanged.next([]);
    this.optionsEducationSubject = [];
    this.optionsEducationSubjectChanged.next([]);
  }

  addGradeSubject(payload: any) {
    return this.gradeSubjectsDataService.addGradeSubject(payload);
  }

  getDetails(gradeId: string, subjectId: string) {
    this.gradeSubjectsDataService.getDetails(gradeId, subjectId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.details = res['data'];
          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = null;
          this.detailsChanged.next(null);
        }
      },
      (err) => {
        this.details = null;
        this.detailsChanged.next(null);
      }
    );
  }

  resetDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  updateDetails(payload: any) {
    return this.gradeSubjectsDataService.updateGradeSubjectsDetails(payload);
  }
}
