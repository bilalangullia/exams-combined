import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GradeSubjectsListComponent } from './grade-subjects-list/grade-subjects-list.component';
import { GradeSubjectsViewComponent } from './grade-subjects-view/grade-subjects-view.component';
import { GradeSubjectsEditComponent } from './grade-subjects-edit/grade-subjects-edit.component';
import { GradeSubjectsAddComponent } from './grade-subjects-add/grade-subjects-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: GradeSubjectsListComponent },
  { path: 'view', component: GradeSubjectsViewComponent },
  { path: 'edit', component: GradeSubjectsEditComponent },
  { path: 'add', component: GradeSubjectsAddComponent }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradeSubjectsRoutingModule {}
