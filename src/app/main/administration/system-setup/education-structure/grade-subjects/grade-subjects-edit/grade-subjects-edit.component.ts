import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../../shared/shared.interfaces';
import { GradeSubjectsService } from '../grade-subjects.service';
import { IGradeSubject, IUpdateGradeSubjectsPayload } from '../grade-subjects.interfaces';
import { QUESTION_BASE, FORM_BUTTONS } from './grade-subjects-edit.config';

@Component({
  selector: 'app-grade-subjects-edit',
  templateUrl: './grade-subjects-edit.component.html',
  styleUrls: ['./grade-subjects-edit.component.css']
})
export class GradeSubjectsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  public questionBase: any = QUESTION_BASE;
  public formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  private gradeSubjectId: IGradeSubject = null;

  /* Subscriptions */
  private gradeSubjectIdSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private gradeSubjectsService: GradeSubjectsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Education - Grade Subjects', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.gradeSubjectIdSub = this.gradeSubjectsService.gradeSubjectIdChanged.subscribe(
      (gradeSubjectId: IGradeSubject) => {
        if (!gradeSubjectId || !gradeSubjectId['education_grade_id'] || !gradeSubjectId['education_subject_id']) {
          this.router.navigate(['main/system-setup/education-structure/grade-subjects']);
        } else {
          this.gradeSubjectId = gradeSubjectId;
          this.gradeSubjectsService.getDetails(
            gradeSubjectId['education_grade_id'],
            gradeSubjectId['education_subject_id']
          );

          setTimeout(() => {
            this.gradeSubjectsService.detailsChanged.subscribe((details: any) => {
              if (details) {
                this.setDetails(details);
              }
            });
          }, 0);
        }
      }
    );
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      this.api.setProperty(question['key'], 'value', details[question['key']] ? details[question['key']] : '');
    });
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      const toasterConfig: IToasterConfig = {
        title: 'Missing required fields',
        type: 'error',
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload: IUpdateGradeSubjectsPayload = {
        education_grade_id: this.gradeSubjectId['education_grade_id'],
        education_subject_id: this.gradeSubjectId['education_subject_id'],
        hours_required: form['hours_required'],
        auto_allocation: form['auto_allocation']
      };

      this.gradeSubjectsService.updateDetails({ ...payload }).subscribe(
        (res: any) => {
          if (res && res['data'] && !res['data']['error']) {
            let successMsg: IToasterConfig = { title: 'Saved Successfully.', type: 'success', timeout: 1000 };
            this.sharedService.setToaster(successMsg);
            setTimeout(() => {
              this.cancel();
            }, 500);
          } else {
            let errorMsg: IToasterConfig = { title: 'Error while saving', type: 'error', timeout: 1000 };
            this.sharedService.setToaster(errorMsg);
          }
        },
        (err) => {
          let errorMsg: IToasterConfig = { title: 'Error while saving.', type: 'error', timeout: 1000 };
          this.sharedService.setToaster(errorMsg);
          setTimeout(() => {
            this.cancel();
          }, 500);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/education-structure/grade-subjects']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
