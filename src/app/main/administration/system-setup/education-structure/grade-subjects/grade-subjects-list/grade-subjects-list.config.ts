import { IDropdownOptions } from '../../../../../../shared/shared.interfaces';

const DEFAULT_OPTIONS: Array<IDropdownOptions> = [{ key: null, value: '-- Please Select --' }];

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Code?: Column;
  EducationSubject?: Column;
  HoursRequired?: Column;
  AutoAllocation?: Column;
}

export const TABLE_COLUMNS: ListColumn = {
  Code: {
    headerName: 'Code',
    field: 'code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  EducationSubject: {
    headerName: 'Education Subject',
    field: 'education_subject',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  HoursRequired: {
    headerName: 'Hours Required',
    field: 'hours_required',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  AutoAllocation: {
    headerName: 'Auto Allocation',
    field: 'auto_allocation',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'education_level',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: DEFAULT_OPTIONS,
    events: true
  },
  {
    key: 'education_programme',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: DEFAULT_OPTIONS,
    events: true
  },
  {
    key: 'education_grade',
    visible: true,
    required: true,
    controlType: 'dropdown',
    options: DEFAULT_OPTIONS,
    events: true
  }
];
