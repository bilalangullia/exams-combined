import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { GradeSubjectsService } from '../grade-subjects.service';
import { FILTER_INPUTS, TABLE_COLUMNS } from './grade-subjects-list.config';
import { Subscription } from 'rxjs';
import { IGradeSubjectsFilters } from '../grade-subjects.interfaces';
import { IFetchListParams } from '../../../../../../shared/shared.interfaces';

@Component({
  selector: 'app-grade-subjects-list',
  templateUrl: './grade-subjects-list.component.html',
  styleUrls: ['./grade-subjects-list.component.css'],
  providers: [KdTableEvent]
})
export class GradeSubjectsListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Education - Grade Subjects';

  public loading: boolean = true;
  public showTable: boolean = false;
  public inputs: Array<any> = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRID_ID: string = 'gradeSubject-table';
  readonly PAGESIZE: number = 20;
  public tableApi: ITableApi = {};

  public tableColumns: Array<ITableColumn> = [
    TABLE_COLUMNS.Code,
    TABLE_COLUMNS.EducationSubject,
    TABLE_COLUMNS.HoursRequired,
    TABLE_COLUMNS.AutoAllocation
  ];
  public tableConfig: ITableConfig = {
    id: this.GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.gradeSubjectsService.setGradeSubjectId({
              education_grade_id: _rowNode['data']['education_grade_id'],
              education_subject_id: _rowNode['data']['education_subject_id']
            });
            setTimeout(() => {
              this.router.navigate(['main/system-setup/education-structure/grade-subjects/view']);
            }, 0);
          }
        }
      ]
    }
  };
  private tableEventList: any;
  private currentFilters: IGradeSubjectsFilters = null;

  @ViewChild('filters') filters: KdFilter;

  /* Subscriptions */
  private optionsLevelSub: Subscription;
  private optionsProgrammeSub: Subscription;
  private optionsGradeSub: Subscription;
  private filtersSub: Subscription;
  private tableEventSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    private gradeSubjectsService: GradeSubjectsService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.setPageTitle('Education - Grade Subjects', false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/system-setup/education-structure/grade-subjects/add' }]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.gradeSubjectsService.getOptionsEducationLevel();

    this.optionsLevelSub = this.gradeSubjectsService.optionsEducationLevelChanged.subscribe((options: Array<any>) => {
      if (options) {
        this.setDropdownOptions('education_level', options);
      }
    });

    this.optionsProgrammeSub = this.gradeSubjectsService.optionsEducationProgrammeChanged.subscribe(
      (options: Array<any>) => {
        if (options) {
          this.setDropdownOptions('education_programme', options);
        }
      }
    );

    this.optionsGradeSub = this.gradeSubjectsService.optionsEducationGradeChanged.subscribe((options: Array<any>) => {
      if (options) {
        this.setDropdownOptions('education_grade', options);
      }
    });

    this.filtersSub = this.gradeSubjectsService.filtersChanged.subscribe((filters: IGradeSubjectsFilters) => {
      this.currentFilters = { ...filters };
      if (filters && filters['education_grade'] && filters['education_grade']['key']) {
        this.gradeSubjectsService.getList(filters['education_grade']['key'], 0, 1).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data']['total']) {
              this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
              this.showTable = true;
            } else {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          },
          (err) => {
            console.log(err);
            this.tableConfig.paginationConfig.total = 0;
            this.showTable = false;
          }
        );
      } else {
        this.showTable = false;
      }
    });

    setTimeout(() => {
      this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRID_ID).subscribe((event: any): void => {
        if (event instanceof KdTableDatasourceEvent) {
          this.tableEventList = event;
          this.fetchList(event);
        }
      });
    }, 1000);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    if (data.length) {
      options = data;
    } else {
      options.unshift({ key: null, value: '-- Please Select --' });
    }
    this.filters.setInputProperty(key, 'options', [...options]);
    setTimeout(() => {
      this.filters.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  detectValue(event) {
    if (event['key'] === 'education_level') {
      this.gradeSubjectsService.getOptionsEducationProgramme(event['value']);
      this.gradeSubjectsService.setFilters({ education_programme: { key: null, value: '' } });
      this.gradeSubjectsService.setFilters({ education_grade: { key: null, value: '' } });
    } else if (event['key'] === 'education_programme') {
      this.gradeSubjectsService.getOptionsEducationGrade(event['value']);
      this.gradeSubjectsService.setFilters({ education_grade: { key: null, value: '' } });
    }

    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj && filterObj['key']) {
      let filter: IGradeSubjectsFilters = {};
      filter[event['key']] = filterObj;
      this.gradeSubjectsService.setFilters({ ...filter });
    }
  }

  fetchList(event: KdTableDatasourceEvent) {
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;

    if (
      this.currentFilters &&
      this.currentFilters['education_grade'] &&
      this.currentFilters['education_grade']['key']
    ) {
      this.gradeSubjectsService
        .getList(this.currentFilters['education_grade']['key'], fetchParams['startRow'], fetchParams['endRow'])
        .toPromise()
        .then(
          (res: any) => {
            if (res && res['data'] && res['data']['list']) {
              list = res['data']['list'].map((item) => ({
                ...item,
                education_subject: item['name'],
                auto_allocation: item['auto_allocation'] ? 'YES' : 'NO'
              }));
              total = res['data']['total'];
            } else {
              list = [];
              total = 0;
            }
            dataSourceParams = { rows: list, total };
            event.subscriber.next(dataSourceParams);
            event.subscriber.complete();
          },
          (err) => {
            list = [];
            total = 0;
            dataSourceParams = { rows: list, total };
            event.subscriber.next(dataSourceParams);
            event.subscriber.complete();
          }
        );
    }
  }

  searchList(keyword: string) {}

  ngOnDestroy(): void {
    if (this.optionsLevelSub) {
      this.optionsLevelSub.unsubscribe();
    }
    if (this.optionsProgrammeSub) {
      this.optionsProgrammeSub.unsubscribe();
    }
    if (this.optionsGradeSub) {
      this.optionsGradeSub.unsubscribe();
    }
    if (this.filtersSub) {
      this.filtersSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    this.gradeSubjectsService.resetFilters();
    this.gradeSubjectsService.resetDropdownOptions();
    super.destroyPageBaseSub();
  }
}
