import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { GradeSubjectsRoutingModule } from './grade-subjects-routing.module';
import { GradeSubjectsViewComponent } from './grade-subjects-view/grade-subjects-view.component';
import { GradeSubjectsListComponent } from './grade-subjects-list/grade-subjects-list.component';
import { GradeSubjectsEditComponent } from './grade-subjects-edit/grade-subjects-edit.component';
import { GradeSubjectsService } from './grade-subjects.service';
import { GradeSubjectsAddComponent } from './grade-subjects-add/grade-subjects-add.component';
import { GradeSubjectsDataService } from './grade-subjects-data.service';

@NgModule({
  imports: [CommonModule, GradeSubjectsRoutingModule, SharedModule],
  declarations: [
    GradeSubjectsViewComponent,
    GradeSubjectsListComponent,
    GradeSubjectsEditComponent,
    GradeSubjectsAddComponent
  ],
  providers: [GradeSubjectsService, GradeSubjectsDataService]
})
export class GradeSubjectsModule {}
