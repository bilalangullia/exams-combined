import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'administrative-boundaries', pathMatch: 'full' },
  {
    path: 'administrative-boundaries',
    loadChildren: './administrative-boundaries/administrative-boundaries.module#AdministrativeBoundariesModule'
  },
  {
    path: 'academic-period',
    loadChildren: './academic-period/academic-period.module#AcademicPeriodModule'
  },
  {
    path: 'education-structure',
    loadChildren: './education-structure/education-structure.module#EducationStructureModule'
  },
  {
    path: 'field-options',
    loadChildren: './field-options/field-options.module#FieldOptionsModule'
  },
  {
    path: 'system-configurations',
    loadChildren: './system-configurations/system-configurations.module#SystemConfigurationsModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemSetupRoutingModule {}
