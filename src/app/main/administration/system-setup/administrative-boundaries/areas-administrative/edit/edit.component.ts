import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { AreasAdministrativeService } from '../areas-administrative.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private visibleOptions: Array<any> = [
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];
  private isMainCountryOptions: Array<any> = [
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];
  public parentId: any;
  public areaAdministrativeId: any;
  public country: Array<any> = [];
  public countryValue: any;

  /* Subscriptions */
  private areaAdministrativeIdSub: Subscription;
  private areaAdministrativeDetailsSub: Subscription;
  private areaLevelSub: Subscription;
  private countryNameSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private areaAdministrativeService: AreasAdministrativeService
  ) {
    super({ router: router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Area - Administrative', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.areaAdministrativeService.currentParentChanged.subscribe((parent_id) => {
      this.parentId = parent_id['key'];
    });
    this.areaAdministrativeService.getAreaLevel(this.parentId);
    this.areaAdministrativeService.getCountryName();
    this.initializeSubscriptions();

    this.areaAdministrativeIdSub = this.areaAdministrativeService.areasAdministrativeIdChanged.subscribe(
      (areaAdministrativeId) => {
        if (!areaAdministrativeId) {
          this.sharedService.setToaster(invalidIdError);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/administrative-boundaries/areas-administrative/list']);
          }, 0);
        } else {
          this.setDropdownOptions('is_main_country', this.isMainCountryOptions);
          this.setDropdownOptions('visible', this.visibleOptions);

          this.areaAdministrativeId = areaAdministrativeId;
          this.areaAdministrativeService.getAreasAdministrativeDetails(this.areaAdministrativeId);

          timer(50).subscribe(() => {
            this.areaAdministrativeDetailsSub = this.areaAdministrativeService.areasAdministrativeViewDetailsChanged.subscribe(
              (details: any) => {
                if (details) {
                  this.setAreaAdministrativeDetails(details);
                }
              }
            );
          });
        }
      }
    );
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  setAreaAdministrativeDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i] && this._questionBase[i].key && this._questionBase[i].key == 'visible') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i].key].key);
          } else if (this._questionBase[i] && this._questionBase[i].key == 'is_main_country') {
            if (this.parentId === 1) {
              this.api.setProperty(this._questionBase[i].key, 'visible', true);
              this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i].key].key);
            } else {
              this.api.setProperty(
                this._questionBase[i].key,
                'value',
                !details[this._questionBase[i].key]
                  ? ''
                  : details[this._questionBase[i].key].value
                  ? details[this._questionBase[i].key].value
                  : details[this._questionBase[i].key]
              );
            }
          } else if (this._questionBase[i] && this._questionBase[i].key == 'area_level') {
            this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i].key].key);
          } else if (this._questionBase[i] && this._questionBase[i].key == 'name') {
            if (this.parentId == 1) {
              this.api.setProperty(this._questionBase[i].key, 'controlType', 'dropdown');
              this.api.setProperty(this._questionBase[i].key, 'value', null);
              let countryName = this.country.find((item) => item.value == details[this._questionBase[i].key]);
              if (countryName && countryName['key']) {
                this.api.setProperty(this._questionBase[i].key, 'value', countryName['key']);
              }
            } else {
              this.api.setProperty(this._questionBase[i].key, 'value', details[this._questionBase[i].key]);
            }
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  submit(event) {
    console.log('event is', event);

    let countryName = this.country.find((item) => item.key == event.name);
    let payload = {};
    if (event.is_main_country == '') {
      event.is_main_country = '0';
      payload = {
        id: this.areaAdministrativeId,
        parent_id: this.parentId,
        area_administrative_level: event.area_level,
        code: event.code,
        name: event.name,
        is_main_country: event.is_main_country ? event.is_main_country : 0,
        visible: event.visible
      };
    } else {
      payload = {
        id: this.areaAdministrativeId,
        parent_id: this.parentId,
        area_administrative_level: event.area_level,
        code: event.code,
        name: countryName['value'],
        is_main_country: event.is_main_country ? event.is_main_country : 0,
        visible: event.visible
      };
    }
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      this.areaAdministrativeService.updateAreasAdministrativeDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  initializeSubscriptions() {
    this.areaLevelSub = this.areaAdministrativeService.areaLevelChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.key, value: item.value });
        });
        this._updateView.setInputProperty('area_level', 'options', tempOption);
      }
    });

    this.countryNameSub = this.areaAdministrativeService.countryNameChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.key, value: item.value });
        });

        this.country = tempOption;
        this._updateView.setInputProperty('name', 'options', tempOption);
      }
    });
  }

  cancel() {
    this.router.navigate(['main/system-setup/administrative-boundaries/areas-administrative/view']);
  }

  ngOnDestroy(): void {
    if (this.areaLevelSub) {
      this.areaLevelSub.unsubscribe();
    }
    if (this.countryNameSub) {
      this.countryNameSub.unsubscribe();
    }
    if (this.areaAdministrativeIdSub) {
      this.areaAdministrativeIdSub.unsubscribe();
    }
    if (this.areaAdministrativeDetailsSub) {
      this.areaAdministrativeDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
