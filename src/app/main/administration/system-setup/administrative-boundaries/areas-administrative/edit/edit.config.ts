export const QUESTION_BASE: Array<any> = [
  {
    key: 'parent',
    label: 'Parent',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'area_level',
    label: 'Area Administrative Level',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--select--'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'code',
    label: 'Code',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  },
  {
    key: 'is_main_country',
    label: 'Is Main Country',
    visible: false,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--select--'
      }
    ],
    events: true,
    required: true
  },
  {
    key: 'visible',
    label: 'Visible',
    visible: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--select--'
      }
    ],
    events: true,
    required: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
