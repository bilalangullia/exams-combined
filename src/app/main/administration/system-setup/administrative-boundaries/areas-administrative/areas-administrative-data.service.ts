import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class AreasAdministrativeDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /***  LIST API ***/
  getList(parent_id: string, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaAdministrative}/list?start=${start}&end=${end}&parent_id=${parent_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  // http://openemis.n2.iworklab.com/api/administration/systemsetup/administrative-boundaries/area-administrative/list?start=0&end=20&keyword=Region&parent_id=3
  getListSearch(parent_id: string, keyword: string, start?: number, end?: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaAdministrative}/list?start=${start}&end=${end}&keyword=${keyword}&parent_id=${parent_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getAreasAdministrativeDetails(areaAdministrativeId) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaAdministrative}/view/${areaAdministrativeId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | Area levels dropdown  ***/
  getAreaLevelsDropDown(parent_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaAdministrative}/area-level-dropdown/${parent_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | Country Name dropdown  ***/
  getCountryNameDropDown() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.country}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /*** VIEW | DELETE API ***/
  deleteAreasAdministrativeDetails(areaAdministrativeId) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaAdministrative}/${areaAdministrativeId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateAreasAdministrativeDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaAdministrative}/update`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ADD | SUBMIT API  **/
  addAreasAdministrativeDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaAdministrative}/store`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
