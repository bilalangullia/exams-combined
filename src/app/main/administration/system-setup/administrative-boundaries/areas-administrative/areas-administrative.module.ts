import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { AreasAdministrativeRoutingModule } from './areas-administrative-routing.module';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { AreasAdministrativeService } from './areas-administrative.service';
import { AreasAdministrativeDataService } from './areas-administrative-data.service';

@NgModule({
  imports: [CommonModule, AreasAdministrativeRoutingModule, SharedModule],
  declarations: [AddComponent, EditComponent, ListComponent, ViewComponent],
  providers: [AreasAdministrativeService, AreasAdministrativeDataService]
})
export class AreasAdministrativeModule {}
