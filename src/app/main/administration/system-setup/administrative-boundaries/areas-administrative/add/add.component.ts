import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { AreasAdministrativeService } from '../areas-administrative.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private visibleOptions: Array<any> = [
    { id: null, name: '--select--' },
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];
  private isMainCountryOptions: Array<any> = [
    { id: null, name: '--select--' },
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];
  public parentId: any;
  public parentValue: any;
  public country: any;

  /* Subscriptions */
  private areaLevelSub: Subscription;
  private countryNameSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private areaAdministrativeService: AreasAdministrativeService
  ) {
    super({ router: router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Area - Administrative', false);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.areaAdministrativeService.currentParentChanged.subscribe((parent) => {
      this.parentId = parent['key'];
      this.parentValue = parent['value'];
    });

    this.areaAdministrativeService.getAreaLevel(this.parentId);
    this.areaAdministrativeService.getCountryName();
    this.initializeSubscriptions();
    this.setAreaAdministrativeDetails();
    this.setDropdownOptions('is_main_country', this.visibleOptions);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  setAreaAdministrativeDetails() {
    this.loading = false;
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'visible') {
          this.setDropdownOptions('visible', this.visibleOptions);
        } else if (this._questionBase[i].key == 'is_main_country') {
          if (this.parentId === 1) {
            this.api.setProperty(this._questionBase[i].key, 'visible', true);
            this.setDropdownOptions('is_main_country', this.isMainCountryOptions);
            this.api.setProperty(this._questionBase[i].key, 'value', null);
          }
        } else if (this._questionBase[i].key == 'name') {
          if (this.parentId === 1) {
            this.api.setProperty(this._questionBase[i].key, 'controlType', 'dropdown');
            this.api.setProperty(this._questionBase[i].key, 'value', null);
          }
        } else if (this._questionBase[i].key == 'parent') {
          this.api.setProperty(this._questionBase[i].key, 'value', this.parentValue);
        }
      }
    });
  }
  submit(event) {
    let countryName = this.country.find((item) => item.key == event.name);
    let payload = {};
    if (event.is_main_country == '') {
      event.is_main_country = '0';
      payload = {
        parent_id: this.parentId,
        area_administrative_level: event.area_administrative_level,
        code: event.code,
        name: event.name,
        is_main_country: event.is_main_country ? event.is_main_country : 0
      };
    } else {
      payload = {
        parent_id: this.parentId,
        area_administrative_level: event.area_administrative_level,
        code: event.code,
        name: countryName['value'],
        is_main_country: event.is_main_country ? event.is_main_country : 0
      };
    }
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      this.areaAdministrativeService.addAreasAdministrativeDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  initializeSubscriptions() {
    this.areaLevelSub = this.areaAdministrativeService.areaLevelChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.key, value: item.value });
        });
        tempOption.unshift({
          key: null,
          value: '--select--'
        });
        this._updateView.setInputProperty('area_administrative_level', 'options', tempOption);
      }
    });

    this.countryNameSub = this.areaAdministrativeService.countryNameChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.key, value: item.value });
        });
        tempOption.unshift({
          key: null,
          value: '--select--'
        });
        this.country = tempOption;
        this._updateView.setInputProperty('name', 'options', tempOption);
      }
    });
  }
  cancel() {
    this.router.navigate(['main/system-setup/administrative-boundaries/areas-administrative/list']);
  }

  ngOnDestroy(): void {
    if (this.areaLevelSub) {
      this.areaLevelSub.unsubscribe();
    }
    if (this.countryNameSub) {
      this.countryNameSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
