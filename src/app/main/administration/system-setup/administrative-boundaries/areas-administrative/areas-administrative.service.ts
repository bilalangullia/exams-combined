import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { AreasAdministrativeDataService } from './areas-administrative-data.service';

@Injectable()
export class AreasAdministrativeService {
  private areasAdministrativeId: string = null;
  private parentId: string;
  private areasAdministrativeViewDetails: any = [];
  private currentParent: any = {};
  private areaLevel: any;
  private countryName: any;

  public areasAdministrativeIdChanged = new BehaviorSubject<string>(null);
  public parentIdChanged = new BehaviorSubject<string>(null);
  public areasAdministrativeViewDetailsChanged = new BehaviorSubject<any>(this.areasAdministrativeViewDetails);
  public currentParentChanged = new BehaviorSubject<any>(this.currentParent);
  public areaLevelChanged = new Subject();
  public countryNameChanged = new Subject();

  constructor(
    private router: Router,
    private areasAdministrativeDataService: AreasAdministrativeDataService,
    private sharedService: SharedService
  ) {}

  /*** List Page | Set areasAdministrative Id***/
  setareasAdministrativeId(areasAdministrativeId: string) {
    this.areasAdministrativeId = areasAdministrativeId;
    this.areasAdministrativeIdChanged.next(this.areasAdministrativeId);
  }

  /*** List Page | Set parentId Id***/
  setParentId(parentId: string) {
    this.parentId = parentId;
    this.parentIdChanged.next(this.parentId);
  }

  /*** List Page | Reset parentId Id***/
  emptyParentId() {
    this.parentId = null;
    this.parentIdChanged.next(null);
  }

  /*** Set Current Parent ***/
  setCurrentParent(filter) {
    this.currentParent = { ...this.currentParent, ...filter };
    this.currentParentChanged.next({ ...this.currentParent });
  }

  /*** LIST PAGE | List Data ***/
  getList(parent_id: string, start?: number, end?: number) {
    return this.areasAdministrativeDataService.getList(parent_id ? parent_id : '1', start, end);
  }

  /*** List Data | SEARCH***/
  searchList(parent_id: string, keyword: string, start?: number, end?: number) {
    return this.areasAdministrativeDataService.getListSearch(parent_id ? parent_id : '1', keyword, start, end);
  }

  /*** VIEW PAGE | Details Data ***/
  getAreasAdministrativeDetails(areasAdministrative_id) {
    this.areasAdministrativeDataService.getAreasAdministrativeDetails(areasAdministrative_id).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.areasAdministrativeViewDetails = res.data;
          this.areasAdministrativeViewDetailsChanged.next({ ...this.areasAdministrativeViewDetails });
        } else {
          this.areasAdministrativeViewDetails = null;
          this.areasAdministrativeViewDetailsChanged.next(null);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.areasAdministrativeViewDetails = null;
        this.areasAdministrativeViewDetailsChanged.next(null);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE | Area Administrative Level Dropdown***/
  getAreaLevel(parent_id) {
    this.areasAdministrativeDataService.getAreaLevelsDropDown(parent_id).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.areaLevel = res['data'];
          this.areaLevelChanged.next([...this.areaLevel]);
        } else {
          this.areaLevel = [];
          this.areaLevelChanged.next([...this.areaLevel]);
        }
      },
      (err) => {
        this.areaLevel = [];
        this.areaLevelChanged.next([...this.areaLevel]);
      }
    );
  }

  /*** EDIT PAGE | Country Name dropdown***/
  getCountryName() {
    this.areasAdministrativeDataService.getCountryNameDropDown().subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.countryName = res['data'];
          this.countryNameChanged.next([...this.countryName]);
        } else {
          this.countryName = [];
          this.countryNameChanged.next([...this.countryName]);
        }
      },
      (err) => {
        this.countryName = [];
        this.countryNameChanged.next([...this.countryName]);
      }
    );
  }

  /*** EDIT PAGE SUBMIT ***/
  updateAreasAdministrativeDetails(data) {
    this.areasAdministrativeDataService.updateAreasAdministrativeDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/administrative-boundaries/areas-administrative/list']);
          }, 500);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD PAGE SUBMIT ***/
  addAreasAdministrativeDetails(data) {
    this.areasAdministrativeDataService.addAreasAdministrativeDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/administrative-boundaries/areas-administrative/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteAreasAdministrativeDetails(areaAdministrativeId) {
    return this.areasAdministrativeDataService.deleteAreasAdministrativeDetails(areaAdministrativeId);
  }
}
