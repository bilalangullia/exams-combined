interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Visible?: Column;
  Code?: Column;
  Name?: Column;
  AreaAdministrativeLevel?: Column;
  IsMainCountry?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Code: {
    headerName: 'Code',
    field: 'code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  AreaAdministrativeLevel: {
    headerName: 'Area Administrative Level ',
    field: 'area_administrative_level',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  IsMainCountry: {
    headerName: 'Is Main Country',
    field: 'is_main_country',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};
