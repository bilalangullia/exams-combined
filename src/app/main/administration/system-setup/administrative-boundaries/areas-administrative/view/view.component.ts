import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent, KdAlertEvent } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError, deleteError, deleteFail, deleteSuccess } from '../../../../../../shared/shared.toasters';
import { AreasAdministrativeService } from '../areas-administrative.service';
import { VIEWNODE_INPUT, IModalConfig } from './view.config';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  providers: [KdModalEvent]
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public areaAdministrativeId: any;

  /* Subscriptions */
  private areaAdministrativeIdSub: Subscription;
  private areaAdministrativeDetailsSub: Subscription;

  public modalConfig: IModalConfig = {
    title: 'Delete Areas Administrative Details',
    body: 'Are you sure you want to delete the details ?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.areaAdministrativeService.deleteAreasAdministrativeDetails(this.areaAdministrativeId).subscribe(
            (res: any) => {
              if (res.message) {
                event.preventDefault();
                if (this._modalEvent) {
                  this._modalEvent.toggleClose();
                }
                this.sharedService.setToaster(deleteSuccess);
                this.router.navigate(['main/system-setup/administrative-boundaries/areas-administrative/list']);
              }
            },
            (err) => {
              event.preventDefault();
              if (this._modalEvent) {
                this._modalEvent.toggleClose();
              }
              if (err['status'] == 403) {
                this.sharedService.setToaster({ ...deleteFail, body: err['error']['message'] });
              } else {
                this.sharedService.setToaster(deleteError);
              }
            }
          );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          if (this._modalEvent) {
            this._modalEvent.toggleClose();
          }
        }
      }
    ]
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    private sharedService: SharedService,
    public _modalEvent: KdModalEvent,
    public _kdalert: KdAlertEvent,
    private areaAdministrativeService: AreasAdministrativeService
  ) {
    super({
      router: router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Area - Levels', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/administrative-boundaries/areas-administrative/list' },
      { type: 'edit', path: 'main/system-setup/administrative-boundaries/areas-administrative/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.areaAdministrativeIdSub = this.areaAdministrativeService.areasAdministrativeIdChanged.subscribe(
      (areaAdministrativeId) => {
        if (!areaAdministrativeId) {
          this.sharedService.setToaster(invalidIdError);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/administrative-boundaries/areas-administrative/list']);
          }, 0);
        } else {
          this.areaAdministrativeId = areaAdministrativeId;
          this.areaAdministrativeService.getAreasAdministrativeDetails(this.areaAdministrativeId);
          this.areaAdministrativeDetailsSub = this.areaAdministrativeService.areasAdministrativeViewDetailsChanged.subscribe(
            (details: any) => {
              if (details) {
                this.setAreasAdministrativeDetails(details);
              }
            }
          );
        }
      }
    );
  }

  setAreasAdministrativeDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !details[this._questionBase[i].key]
              ? ''
              : details[this._questionBase[i].key].value
              ? details[this._questionBase[i].key].value
              : details[this._questionBase[i].key]
          );
        }
      });
    }
  }
  open() {
    if (this._modalEvent) {
      this._modalEvent.toggleOpen();
    }
  }

  ngOnDestroy(): void {
    if (this.areaAdministrativeIdSub) {
      this.areaAdministrativeIdSub.unsubscribe();
    }
    if (this.areaAdministrativeDetailsSub) {
      this.areaAdministrativeDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
