import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  KdPageBase,
  KdPageBaseEvent,
  IBreadcrumbConfig,
  IPageheaderConfig,
  IPageheaderApi
} from 'openemis-styleguide-lib';
import { Router, ActivatedRoute } from '@angular/router';
import { TABS_LIST } from './entry.config';
@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent extends KdPageBase implements OnInit, OnDestroy {
  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public isTabActive: boolean = true;
  public tabsList: Array<any> = TABS_LIST;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public pageEvent: KdPageBaseEvent) {
    super({ router, activatedRoute, pageEvent });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbObj;
    });
    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig) => {
      let newHeaderObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newHeaderObj;
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.tabsList[0]['isActive'] = true;
  }

  selectedTab(event) {
    // console.log('Current Active Tab - ', event);
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
