const ROUTER_BASE_PATH: string = 'main/system-setup/administrative-boundaries';

interface IKdTabs {
  tabName?: string;
  tabContent?: string;
  tabId?: string;
  isActive?: boolean;
  disabled?: boolean;
  routerPath?: string;
}

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'Area Levels (Education)', routerPath: ROUTER_BASE_PATH + '/area-levels-education' },
  { tabName: 'Areas (Education)', routerPath: ROUTER_BASE_PATH + '/areas-educations' },
  {
    tabName: 'Area Levels (Administrative)',
    routerPath: ROUTER_BASE_PATH + '/area-levels-administrative'
  },
  { tabName: 'Areas (Administrative)', routerPath: ROUTER_BASE_PATH + '/areas-administrative' }
];
