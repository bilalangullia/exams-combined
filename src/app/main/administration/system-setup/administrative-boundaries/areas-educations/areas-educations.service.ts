import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { AreasEducationsDataService } from './areas-educations-data.service';

@Injectable()
export class AreasEducationsService {
  private areasEducationsId: string = null;
  private parentId: string;
  private areasEducationsViewDetails: any = [];
  private areaLevel: any;
  private currentParent: any = {};

  public areasEducationsIdChanged = new BehaviorSubject<string>(null);
  public parentIdChanged = new BehaviorSubject<string>(null);
  public areasEducationsViewDetailsChanged = new BehaviorSubject<any>(this.areasEducationsViewDetails);
  public areaLevelChanged = new Subject();
  public currentParentChanged = new BehaviorSubject<any>(this.currentParent);

  constructor(
    private router: Router,
    private areasEducationsDataService: AreasEducationsDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | List Data ***/
  getList(parent_id: string, start?: number, end?: number) {
    return this.areasEducationsDataService.getList(parent_id ? parent_id : '1', start, end);
  }

  /*** List Data | SEARCH***/
  searchList(parent_id: string, keyword: string, start?: number, end?: number) {
    return this.areasEducationsDataService.getListSearch(parent_id ? parent_id : '1', keyword, start, end);
  }

  /*** List Page | Set areasEducations Id***/
  setAreasEducationsId(areasEducationsId: string) {
    this.areasEducationsId = areasEducationsId;
    this.areasEducationsIdChanged.next(this.areasEducationsId);
  }

  /*** List Page | Set parentId Id***/
  setParentId(parentId: string) {
    this.parentId = parentId;
    this.parentIdChanged.next(this.parentId);
  }

  /*** List Page | Reset parentId Id***/
  emptyParentId() {
    this.parentId = null;
    this.parentIdChanged.next(null);
  }

  /*** Set Current Parent ***/
  setCurrentParent(filter) {
    this.currentParent = { ...this.currentParent, ...filter };
    this.currentParentChanged.next({ ...this.currentParent });
  }

  /*** VIEW PAGE | Details  ***/
  getAreasEducationsDetails(areasEducations_id) {
    this.areasEducationsDataService.getAreasEducationsDetails(areasEducations_id).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.areasEducationsViewDetails = res.data;
          this.areasEducationsViewDetailsChanged.next({ ...this.areasEducationsViewDetails });
        } else {
          this.areasEducationsViewDetails = null;
          this.areasEducationsViewDetailsChanged.next({ ...this.areasEducationsViewDetails });
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.areasEducationsViewDetails = null;
        this.areasEducationsViewDetailsChanged.next({ ...this.areasEducationsViewDetails });
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE |  Area Level Dropdown***/
  getAreaLevel(parent_id) {
    this.areasEducationsDataService.getAreaLevelsDropDown(parent_id).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.areaLevel = res['data'];
          this.areaLevelChanged.next([...this.areaLevel]);
        } else {
          this.areaLevel = [];
          this.areaLevelChanged.next([...this.areaLevel]);
        }
      },
      (err) => {
        this.areaLevel = [];
        this.areaLevelChanged.next([...this.areaLevel]);
      }
    );
  }

  /*** EDIT PAGE SUBMIT ***/
  updateAreasEducationsDetails(data) {
    this.areasEducationsDataService.updateAreasEducationsDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/administrative-boundaries/areas-educations/list']);
          }, 500);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD PAGE SUBMIT ***/
  addAreasEducationsDetails(data) {
    this.areasEducationsDataService.addAreasEducationsDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/administrative-boundaries/areas-educations/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteAreasEducationsDetails(areaEducation_id) {
    return this.areasEducationsDataService.deleteAreasEducationsDetails(areaEducation_id);
  }
}
