import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { AreasEducationsService } from '../areas-educations.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private visibleOptions: Array<any> = [
    { id: 0, name: 'No' },
    { id: 1, name: 'Yes' }
  ];
  public areaEducationId: any;
  public parentId: any;

  /* Subscriptions */
  private areaLevelSub: Subscription;
  private areaEducationIdSub: Subscription;
  private areaEducationDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private areasEducationsService: AreasEducationsService
  ) {
    super({ router: router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Area - Levels', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.areaEducationIdSub = this.areasEducationsService.areasEducationsIdChanged.subscribe((areaEducationId) => {
      if (!areaEducationId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this.router.navigate(['main/system-setup/administrative-boundaries/areas-educations/list']);
        }, 0);
      } else {
        this.setDropdownOptions('visible', this.visibleOptions);
        this.areaEducationId = areaEducationId;
        this.areasEducationsService.getAreasEducationsDetails(this.areaEducationId);
        this.areaEducationDetailsSub = this.areasEducationsService.areasEducationsViewDetailsChanged.subscribe(
          (details: any) => {
            if (details) {
              this.setAreasEducationsDetails(details);
            }
          }
        );
      }
    });
    this.areasEducationsService.getAreaLevel(this.parentId);
    this.initializeSubscriptions();
  }

  initializeSubscriptions() {
    this.areaLevelSub = this.areasEducationsService.areaLevelChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.key, value: item.value });
        });
        this._updateView.setInputProperty('area_level', 'options', tempOption);
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let quesIndex = this._questionBase.findIndex((question) => question['key'] === key);
    if (quesIndex !== -1) {
      let options: Array<any> = [];
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this._questionBase[quesIndex]['options'] = [...options];
    }
  }

  setAreasEducationsDetails(details: any) {
    if (details) {
      this.loading = false;
      this.parentId = details['parent_id'];
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'visible' || this._questionBase[i].key == 'area_level') {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              details[this._questionBase[i]['key']] && details[this._questionBase[i]['key']]['key']
                ? details[this._questionBase[i]['key']]['key']
                : null
            );
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
    }
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        id: this.areaEducationId,
        name: event.name,
        code: event.code,
        visible: event.visible,
        area_level: event.area_level
      };
      this.areasEducationsService.updateAreasEducationsDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this.router.navigate(['main/system-setup/administrative-boundaries/areas-educations/view']);
  }

  ngOnDestroy(): void {
    if (this.areaEducationIdSub) {
      this.areaEducationIdSub.unsubscribe();
    }
    if (this.areaEducationDetailsSub) {
      this.areaEducationDetailsSub.unsubscribe();
    }
    if (this.areaLevelSub) {
      this.areaLevelSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
