import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  ITableDatasourceParams,
  KdTableDatasourceEvent
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { alreadyAtRoot } from '../../../../../../shared/shared.toasters';
import { AreasEducationsService } from '../areas-educations.service';
import { TABLECOLUMN } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'areasEducationsList';
  readonly PAGESIZE: number = 20;
  private tableEventList: any;

  public loading: Boolean = true;
  public tableApi: ITableApi = {};
  public showTable: boolean = false;
  public parentId: string = null;
  public parent: Array<any> = [];

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  private tableEventSub: Subscription;
  private parentIdSub: Subscription;

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Visible,
    TABLECOLUMN.Code,
    TABLECOLUMN.Name,
    TABLECOLUMN.AreaLevel
  ];

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.areaEducationService.setAreasEducationsId(_rowNode['data']['id']);
            this.router.navigate(['main/system-setup/administrative-boundaries/areas-educations/view']);
          }
        },
        {
          icon: 'fa fa-list',
          name: 'View List',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.areaEducationService.setParentId(_rowNode['data']['id']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    private areaEducationService: AreasEducationsService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
    super.setPageTitle('Area - Areas', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/system-setup/administrative-boundaries/areas-educations/add' },
      {
        type: 'back',
        callback: (_rowNode, _tableApi): void => {
          if (this.parent.length < 2) {
            this.sharedService.setToaster(alreadyAtRoot);
          } else {
            this.parent.pop();
            this.areaEducationService.setParentId(
              this.parent[this.parent.length - 1] ? this.parent[this.parent.length - 1] : 1
            );
          }
        }
      }
    ]);
  }

  ngOnInit() {
    this.areaEducationService.emptyParentId();
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;
    this.showTable = true;
    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });

    this.parentIdSub = this.areaEducationService.parentIdChanged.subscribe((parentId) => {
      if (parentId) {
        this.parentId = parentId;
        this.showTable = false;
        setTimeout(() => {
          this.showTable = true;
        }, 0);
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      listReq = this.areaEducationService.searchList(
        this.parentId,
        searchKey,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    } else {
      listReq = this.areaEducationService.getList(
        this.parentId ? this.parentId : '',
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    }
    listReq.toPromise().then(
      (res: any) => {
        this.areaEducationService.setCurrentParent(res['data']['parent']);
        if (this.parent.findIndex((item) => item === res['data']['parent']['key']) == -1) {
          this.parent.push(res['data']['parent']['key']);
        }
        if (res && res['data'] && res['data']['list'] && res['data']['list'].length) {
          list = res['data']['list'];
          total = this.searchKey && this.searchKey.length ? res['data']['list'].length : res['data']['total'];

          list.forEach((item) => {
            item['visible'] = item['visible'] == 1 ? 'Yes' : 'No';
          });
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }

        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.parentIdSub) {
      this.parentIdSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
