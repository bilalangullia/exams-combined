import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class AreasEducationsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /***  LIST API ***/
  getList(parent_id: string, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaEducation}/list?start=${start}&end=${end}&parent_id=${parent_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(parent_id: string, keyword: string, start?: number, end?: number) {
    //  http://openemis.n2.iworklab.com/api/administration/systemsetup/administrative-boundaries/area-education/list?start=0&end=20&keyword=Region&parent_id=1
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaEducation}/list?start=${start}&end=${end}&keyword=${keyword}&parent_id=${parent_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getAreasEducationsDetails(areaEducation_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaEducation}/view/${areaEducation_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW | DELETE API **/
  deleteAreasEducationsDetails(areaEducation_id) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaEducation}/${areaEducation_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | Area level dropdown  ***/
  getAreaLevelsDropDown(parent_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaEducation}/area-level-dropdown/${parent_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateAreasEducationsDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaEducation}/update`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ADD | SUBMIT API  **/
  addAreasEducationsDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaEducation}/store`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
