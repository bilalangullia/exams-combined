import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { AreasEducationsRoutingModule } from './areas-educations-routing.module';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { AreasEducationsService } from './areas-educations.service';
import { AreasEducationsDataService } from './areas-educations-data.service';

@NgModule({
  imports: [CommonModule, AreasEducationsRoutingModule, SharedModule],
  declarations: [AddComponent, EditComponent, ListComponent, ViewComponent],
  providers: [AreasEducationsService, AreasEducationsDataService]
})
export class AreasEducationsModule {}
