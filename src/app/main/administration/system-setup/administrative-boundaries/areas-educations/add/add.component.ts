import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { AreasEducationsService } from '../areas-educations.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public parentId: any;
  public parentValue: any;

  /* Subscriptions */
  private areaLevelSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private areasEducationsService: AreasEducationsService
  ) {
    super({ router: router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Area - Levels', false);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.areasEducationsService.currentParentChanged.subscribe((parent) => {
      if (parent) {
        this.parentId = parent['key'];
        this.parentValue = parent['value'];
        timer(100).subscribe(() => {
          this.setParentValues();
        });
        this.areasEducationsService.getAreaLevel(this.parentId);
        this.initializeSubscriptions();
      }
    });
  }

  setParentValues() {
    this.loading = false;
    timer(200).subscribe(() => {
      this._questionBase.forEach((question: any) => {
        if (question['key'] === 'parent') {
          this.api.setProperty(question['key'], 'value', this.parentValue);
        }
      });
    });
  }

  initializeSubscriptions() {
    this.areaLevelSub = this.areasEducationsService.areaLevelChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.key, value: item.value });
        });

        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('area_level', 'options', tempOption);
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        name: event.name,
        code: event.code,
        area_level: event.area_level,
        parent_id: this.parentId
      };
      this.areasEducationsService.addAreasEducationsDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this.router.navigate(['main/system-setup/administrative-boundaries/areas-educations/list']);
  }

  ngOnDestroy(): void {
    if (this.areaLevelSub) {
      this.areaLevelSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
