import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { AdministrativeBoundariesRoutingModule } from './administrative-boundaries-routing.module';
import { EntryComponent } from './entry/entry.component';

@NgModule({
  imports: [CommonModule, AdministrativeBoundariesRoutingModule, SharedModule],
  declarations: [EntryComponent]
})
export class AdministrativeBoundariesModule {}
