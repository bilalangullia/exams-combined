import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { AreaLevelsEducationService } from '../area-levels-education.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private areaLevelsEducationService: AreaLevelsEducationService
  ) {
    super({ router: router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Area - Levels', false);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        name: event.name
      };
      this.areaLevelsEducationService.addAreaLevelsEducationDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-education/view']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
