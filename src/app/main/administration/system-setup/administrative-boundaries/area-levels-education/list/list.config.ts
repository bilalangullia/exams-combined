interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Level?: Column;
  Name?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Level: {
    headerName: 'Level',
    field: 'level',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};
