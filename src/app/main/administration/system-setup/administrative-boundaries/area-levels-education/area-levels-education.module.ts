import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { AreaLevelsEducationRoutingModule } from './area-levels-education-routing.module';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { AreaLevelsEducationService } from './area-levels-education.service';
import { AreaLevelsEducationDataService } from './area-levels-education-data.service';

@NgModule({
  imports: [CommonModule, AreaLevelsEducationRoutingModule, SharedModule],
  declarations: [AddComponent, EditComponent, ListComponent, ViewComponent],
  providers: [AreaLevelsEducationService, AreaLevelsEducationDataService]
})
export class AreaLevelsEducationModule {}
