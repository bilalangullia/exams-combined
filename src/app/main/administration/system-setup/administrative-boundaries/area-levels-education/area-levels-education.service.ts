import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { AreaLevelsEducationDataService } from './area-levels-education-data.service';

@Injectable()
export class AreaLevelsEducationService {
  private areaLevelsEducationId: string = null;
  private areaLevelsEducationViewDetails: any = [];

  public areaLevelsEducationIdChanged = new BehaviorSubject<string>(null);
  public areaLevelsEducationViewDetailsChanged = new BehaviorSubject<any>(this.areaLevelsEducationViewDetails);

  constructor(
    private router: Router,
    private areaLevelsEducationDataService: AreaLevelsEducationDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | List Data ***/
  getList(start?: number, end?: number) {
    return this.areaLevelsEducationDataService.getList(start, end);
  }

  /*** List Data | SEARCH***/
  searchList(keyword: string, start?: number, end?: number) {
    return this.areaLevelsEducationDataService.getListSearch(keyword, start, end);
  }

  /*** List Page | Set areaLevelsEducation Id***/
  setAreaLevelsEducationId(areaLevelsEducationId: string) {
    this.areaLevelsEducationId = areaLevelsEducationId;
    this.areaLevelsEducationIdChanged.next(this.areaLevelsEducationId);
  }

  /*** VIEW PAGE | Details Data ***/
  getAreaLevelsEducationDetails(areaLevelsEducation_id) {
    this.areaLevelsEducationDataService.getAreaLevelsEducationDetails(areaLevelsEducation_id).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.areaLevelsEducationViewDetails = res['data'];
          this.areaLevelsEducationViewDetailsChanged.next({ ...this.areaLevelsEducationViewDetails });
        } else {
          this.areaLevelsEducationViewDetails = null;
          this.areaLevelsEducationViewDetailsChanged.next(null);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.areaLevelsEducationViewDetails = null;
        this.areaLevelsEducationViewDetailsChanged.next({ ...this.areaLevelsEducationViewDetails });
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE SUBMIT ***/
  updateAreaLevelsEducationDetails(data) {
    this.areaLevelsEducationDataService.updateAreaLevelsEducationDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-education/list']);
          }, 500);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD PAGE SUBMIT ***/
  addAreaLevelsEducationDetails(data) {
    this.areaLevelsEducationDataService.addAreaLevelsEducationDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-education/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteAreaLevelsEducationDetails(areaLevelEducation_id) {
    return this.areaLevelsEducationDataService.deleteAreaLevelsEducationDetails(areaLevelEducation_id);
  }
}
