import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, updateSuccess, saveFail, detailsNotFound } from '../../../../../shared/shared.toasters';
import { AreaLevelsAdministrativeDataService } from './area-levels-administrative-data.service';
import { IFilters } from './area-levels-administrative.interfaces';
@Injectable()
export class AreaLevelsAdministrativeService {
  private filterAreaAdministrativeLevel: any;
  private areaLevelsAdministrativeId: string = null;
  private areaLevelsAdministrativeViewDetails: any = [];
  private currentFilters: any = {};

  public filterAreaAdministrativeLevelChanged = new Subject();
  public areaLevelsAdministrativeIdChanged = new BehaviorSubject<string>(null);
  public areaLevelsAdministrativeViewDetailsChanged = new BehaviorSubject<any>(
    this.areaLevelsAdministrativeViewDetails
  );
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);

  constructor(
    private router: Router,
    private areaLevelsAdministrativeDataService: AreaLevelsAdministrativeDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | FILTERS | Area Administrative***/
  getFilterAreaAdministrative() {
    this.areaLevelsAdministrativeDataService.getOptionsAreaAdministrative().subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.filterAreaAdministrativeLevel = res.data;
        this.filterAreaAdministrativeLevelChanged.next([...this.filterAreaAdministrativeLevel]);
      } else {
        this.filterAreaAdministrativeLevel = [];
        this.filterAreaAdministrativeLevelChanged.next([...this.filterAreaAdministrativeLevel]);
      }
      (err) => {
        this.filterAreaAdministrativeLevel = [];
        this.filterAreaAdministrativeLevelChanged.next([...this.filterAreaAdministrativeLevel]);
      };
    });
  }

  /*** Set Current filters ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }

  /*** LIST PAGE | List Data ***/
  getList(id: number, start?: number, end?: number) {
    return this.areaLevelsAdministrativeDataService.getList(id, start, end);
  }

  /*** List Data | SEARCH***/
  searchList(id: number, keyword: string, start?: number, end?: number) {
    return this.areaLevelsAdministrativeDataService.getListSearch(id, keyword, start, end);
  }

  /*** List Page | Set System Id***/
  setAreaLevelsAdministrativeId(areaLevelsAdministrativeId: string) {
    this.areaLevelsAdministrativeId = areaLevelsAdministrativeId;
    this.areaLevelsAdministrativeIdChanged.next(this.areaLevelsAdministrativeId);
  }

  /*** VIEW PAGE | Details Data ***/
  getAreaLevelsAdministrativeDetails(areaLevelAdministrativeId) {
    this.areaLevelsAdministrativeDataService.getAreaLevelsAdministrativeDetails(areaLevelAdministrativeId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.areaLevelsAdministrativeViewDetails = res['data'];
          this.areaLevelsAdministrativeViewDetailsChanged.next({ ...this.areaLevelsAdministrativeViewDetails });
        } else {
          this.areaLevelsAdministrativeViewDetails = null;
          this.areaLevelsAdministrativeViewDetailsChanged.next(null);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.areaLevelsAdministrativeViewDetails = null;
        this.areaLevelsAdministrativeViewDetailsChanged.next(null);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** EDIT PAGE SUBMIT ***/
  updateAreaLevelsAdministrativeDetails(data) {
    this.areaLevelsAdministrativeDataService.updateAreaLevelsAdministrativeDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(updateSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-administrative/list']);
          }, 500);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** ADD PAGE SUBMIT ***/
  addAreaLevelsAdministrativeDetails(data) {
    this.areaLevelsAdministrativeDataService.addAreaLevelsAdministrativeDetails(data).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-administrative/list']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** DELETE ***/
  deleteAreaLevelsAdministrativeDetails(areaLevelAdministrativeId) {
    return this.areaLevelsAdministrativeDataService.deleteAreaLevelsAdministrativeDetails(areaLevelAdministrativeId);
  }
}
