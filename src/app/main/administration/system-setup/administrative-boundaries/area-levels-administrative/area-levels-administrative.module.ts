import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { AreaLevelsAdministrativeRoutingModule } from './area-levels-administrative-routing.module';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { AreaLevelsAdministrativeService } from './area-levels-administrative.service';
import { AreaLevelsAdministrativeDataService } from './area-levels-administrative-data.service';

@NgModule({
  imports: [CommonModule, AreaLevelsAdministrativeRoutingModule, SharedModule],
  declarations: [AddComponent, EditComponent, ListComponent, ViewComponent],
  providers: [AreaLevelsAdministrativeService, AreaLevelsAdministrativeDataService]
})
export class AreaLevelsAdministrativeModule {}
