import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { AreaLevelsAdministrativeService } from '../area-levels-administrative.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public areaAdministrativeId: any;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private areaLevelAdministrativeService: AreaLevelsAdministrativeService
  ) {
    super({ router: router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Area - Levels', false);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.areaLevelAdministrativeService.currentFiltersChanged.subscribe((filters) => {
      if (filters && filters['areas_administrative'] && filters['areas_administrative']['key']) {
        this.areaAdministrativeId = filters['areas_administrative']['key'];
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        name: event.name,
        area_administrative_id: this.areaAdministrativeId
      };
      this.areaLevelAdministrativeService.addAreaLevelsAdministrativeDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-administrative/list']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
