export const QUESTION_BASE: Array<any> = [
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    required: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
