import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { AreaLevelsAdministrativeService } from '../area-levels-administrative.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public areaLevelsAdministrativeId: any;

  /* Subscriptions */
  private areaLevelsAdministrativeIdSub: Subscription;
  private areaLevelsAdministrativeDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private areaLevelAdministrativeService: AreaLevelsAdministrativeService
  ) {
    super({ router: router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Area - Levels', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.areaLevelsAdministrativeIdSub = this.areaLevelAdministrativeService.areaLevelsAdministrativeIdChanged.subscribe(
      (areaLevelsAdministrativeId) => {
        if (!areaLevelsAdministrativeId) {
          this.sharedService.setToaster(invalidIdError);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-administrative/list']);
          }, 0);
        } else {
          this.areaLevelsAdministrativeId = areaLevelsAdministrativeId;
          this.areaLevelAdministrativeService.getAreaLevelsAdministrativeDetails(this.areaLevelsAdministrativeId);
          this.areaLevelsAdministrativeDetailsSub = this.areaLevelAdministrativeService.areaLevelsAdministrativeViewDetailsChanged.subscribe(
            (details: any) => {
              if (details) {
                this.setAreaLevelsAdministrativeDetails(details);
              }
            }
          );
        }
      }
    );
  }

  setAreaLevelsAdministrativeDetails(details: any) {
    if (details) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !details[this._questionBase[i].key]
              ? ''
              : details[this._questionBase[i].key].value
              ? details[this._questionBase[i].key].value
              : details[this._questionBase[i].key]
          );
        }
      });
    }
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        id: this.areaLevelsAdministrativeId,
        name: event.name
      };
      this.areaLevelAdministrativeService.updateAreaLevelsAdministrativeDetails(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-administrative/view']);
  }

  ngOnDestroy(): void {
    if (this.areaLevelsAdministrativeIdSub) {
      this.areaLevelsAdministrativeIdSub.unsubscribe();
    }
    if (this.areaLevelsAdministrativeDetailsSub) {
      this.areaLevelsAdministrativeDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
