import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class AreaLevelsAdministrativeDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /** AREA ADMINISTRATIVE DROPDOWN **/
  getOptionsAreaAdministrative() {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.dropdown}/${urls.areaAdministrative}`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /***  LIST API ***/
  getList(id: number, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaLevelAdministrative}/list/${id}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(id: number, keyword: string, start?: number, end?: number) {
    //openemis.n2.iworklab.com/api/administration/systemsetup/administrative-boundaries/area-level-administrative/list/2?start=0&end=20&keyword=Region
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaLevelAdministrative}/list/${id}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getAreaLevelsAdministrativeDetails(areaLevelAdministrativeId) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaLevelAdministrative}/view/${areaLevelAdministrativeId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  VIEW | DELETE API **/
  deleteAreaLevelsAdministrativeDetails(areaLevelAdministrativeId) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaLevelAdministrative}/${areaLevelAdministrativeId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateAreaLevelsAdministrativeDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaLevelAdministrative}/update`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ADD | SUBMIT API  **/
  addAreaLevelsAdministrativeDetails(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.administrativeBoundaries}/${urls.areaLevelAdministrative}/store`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
