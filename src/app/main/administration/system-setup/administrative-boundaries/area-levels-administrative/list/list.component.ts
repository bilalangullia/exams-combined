import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter,
  ITableDatasourceParams,
  KdTableDatasourceEvent
} from 'openemis-styleguide-lib';
import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { AreaLevelsAdministrativeService } from '../area-levels-administrative.service';
import { IFilters } from '../area-levels-administrative.interfaces';
import { TABLECOLUMN, FILTER_INPUTS } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('filters') filter: KdFilter;
  public filterInputs = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'areaLevelsAdministrativeList';
  readonly PAGESIZE: number = 20;

  public loading: Boolean = true;
  public currentFilters: IFilters;
  public tableApi: ITableApi = {};
  public showTable: boolean = false;
  private tableEventList: any;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  private tableEventSub: Subscription;
  private areaAdministrativeSub: Subscription;
  private currentFiltersSub: Subscription;

  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.Level, TABLECOLUMN.Name];

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.areaLevelsAdministrative.setAreaLevelsAdministrativeId(_rowNode['data']['id']);
            this.router.navigate(['main/system-setup/administrative-boundaries/area-levels-administrative/view']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    private areaLevelsAdministrative: AreaLevelsAdministrativeService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
    super.setPageTitle('Area - Levels', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/system-setup/administrative-boundaries/area-levels-administrative/add' }
    ]);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.areaLevelsAdministrative.resetFilters();
    this.areaLevelsAdministrative.getFilterAreaAdministrative();
    this.areaAdministrativeSub = this.areaLevelsAdministrative.filterAreaAdministrativeLevelChanged.subscribe(
      (options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('areas_administrative', options);
        }
      }
    );

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });

    this.currentFiltersSub = this.areaLevelsAdministrative.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (filters && filters['areas_administrative'] && filters['areas_administrative']['key']) {
        this.currentFilters = { ...filters };
        this.showTable = false;
        this.areaLevelsAdministrative.getList(filters['areas_administrative']['key'], 0, 1).subscribe(
          (res: any) => {
            this.showTable = true;
            if (res && res['data'] && res['data']['total']) {
              this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
              this.showTable = true;
            } else {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          },
          (err) => {
            this.tableConfig.paginationConfig.total = 0;
            this.showTable = false;
          }
        );
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<{ key: number; value: string }> = [];
    options = data.map((item) => ({ key: item['key'], value: item['value'] }));
    this.filter.setInputProperty(key, 'options', options);
    setTimeout(() => {
      this.filter.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      if (
        this.currentFilters &&
        this.currentFilters['areas_administrative'] &&
        this.currentFilters['areas_administrative']['key']
      ) {
        listReq = this.areaLevelsAdministrative.searchList(
          this.currentFilters['areas_administrative']['key'],
          searchKey,
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    } else {
      if (
        this.currentFilters &&
        this.currentFilters['areas_administrative'] &&
        this.currentFilters['areas_administrative']['key']
      ) {
        listReq = this.areaLevelsAdministrative.getList(
          this.currentFilters['areas_administrative']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['list'] && res['data']['list'].length) {
          list = res['data']['list'];
          total = this.searchKey && this.searchKey.length ? res['data']['list'].length : res['data']['total'];
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }

        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  detectValue(event) {
    let filter: IFilters;
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj) {
      filter = { [event['key']]: filterObj };
      this.areaLevelsAdministrative.setCurrentFilters(filter);
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    if (this.areaAdministrativeSub) {
      this.areaAdministrativeSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
