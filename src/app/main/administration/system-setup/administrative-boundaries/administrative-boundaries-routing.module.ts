import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntryComponent } from './entry/entry.component';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
    children: [
      { path: '', redirectTo: 'area-levels-education' },
      {
        path: 'area-levels-education',
        loadChildren: './area-levels-education/area-levels-education.module#AreaLevelsEducationModule'
      },
      { path: 'areas-educations', loadChildren: './areas-educations/areas-educations.module#AreasEducationsModule' },
      {
        path: 'area-levels-administrative',
        loadChildren: './area-levels-administrative/area-levels-administrative.module#AreaLevelsAdministrativeModule'
      },
      {
        path: 'areas-administrative',
        loadChildren: './areas-administrative/areas-administrative.module#AreasAdministrativeModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeBoundariesRoutingModule {}
