import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemSetupRoutingModule } from './system-setup-routing.module';
import { SharedModule } from '../../../shared/shared.module';
@NgModule({
  imports: [CommonModule, SystemSetupRoutingModule, SharedModule],
  declarations: []
})
export class SystemSetupModule {}
