import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { AcademicPeriodRoutingModule } from './academic-period-routing.module';
import { AcademicPeriodComponent } from './academic-period.component';

@NgModule({
  imports: [CommonModule, AcademicPeriodRoutingModule, SharedModule],
  declarations: [AcademicPeriodComponent]
})
export class AcademicPeriodModule {}
