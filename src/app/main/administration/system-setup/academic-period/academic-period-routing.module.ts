import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcademicPeriodComponent } from './academic-period.component';

const routes: Routes = [
  {
    path: '',
    component: AcademicPeriodComponent,
    children: [
      { path: '', redirectTo: 'levels', pathMatch: 'full' },
      {
        path: 'levels',
        loadChildren: '../academic-period/levels/levels.module#LevelsModule'
      },
      { path: 'periods', loadChildren: './periods/periods.module#PeriodsModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcademicPeriodRoutingModule {}
