import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, ITableFormInput, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { LevelsService } from '../levels.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Levels';
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public questionBase: Array<ITableFormInput> = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private levelsService: LevelsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setToolbarMainBtns([]);
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = { ...form };
      this.levelsService.addLevel(payload);
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/academic-period/levels']);
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
