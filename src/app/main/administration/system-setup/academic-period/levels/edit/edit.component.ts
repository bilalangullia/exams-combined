import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBaseEvent, KdPageBase, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../../shared/shared.toasters';
import { LevelsService } from '../levels.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Levels';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public questionBase = QUESTION_BASE;
  public formButtons = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  private levelId: string = null;

  /* Subscriptions */
  private levelIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private levelsService: LevelsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setToolbarMainBtns([]);
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.levelIdSub = this.levelsService.levelIdChanged.subscribe((levelId: string) => {
      if (!levelId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this.router.navigate(['main/system-setup/academic-period/levels']);
        }, 0);
      } else {
        this.levelId = levelId;

        this.levelsService.getDetails(this.levelId);

        this.detailsSub = this.levelsService.detailsChanged.subscribe((details: any) => {
          if (details) {
            this.setDetails(details);
          }
        });
      }
    });
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      this.api.setProperty(question['key'], 'value', details[question['key']] ? details[question['key']] : '');
    });
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(form) {
    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = { ...form };
      this.levelsService.updateDetails(this.levelId, { ...payload });
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/academic-period/levels']);
  }

  ngOnDestroy() {
    if (this.levelIdSub) {
      this.levelIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }

    this.levelsService.resetDetails();
    super.destroyPageBaseSub();
  }
}
