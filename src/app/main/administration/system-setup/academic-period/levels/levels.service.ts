import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { saveSuccess, saveFail } from '../../../../../shared/shared.toasters';
import { LevelsDataService } from './levels-data.service';

@Injectable()
export class LevelsService {
  private levelId: string = null;
  private details: any = null;

  public levelIdChanged = new BehaviorSubject<string>(this.levelId);
  public detailsChanged = new BehaviorSubject<any>(null);

  constructor(
    private levelsDataService: LevelsDataService,
    private sharedService: SharedService,
    private router: Router
  ) {}

  getList(start: number, end: number) {
    return this.levelsDataService.getList(start, end);
  }

  searchList(keyword: string, start: number, end: number) {
    return this.levelsDataService.searchList(keyword, start, end);
  }

  addLevel(payload: any) {
    this.levelsDataService.addLevel(payload).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          this.router.navigate(['main/system-setup/academic-period/levels']);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  setLevelId(levelId: string) {
    this.levelId = levelId;
    this.levelIdChanged.next(this.levelId);
  }

  resetLevelId() {
    this.levelId = null;
    this.levelIdChanged.next(this.levelId);
  }

  getDetails(levelId: string) {
    this.levelsDataService.getDetails(levelId).subscribe((res: any) => {
      if (res && res['data'] && res['data'].length) {
        this.details = res['data'][0];
        this.detailsChanged.next({ ...this.details });
      } else {
        this.details = null;
        this.detailsChanged.next(null);
      }
    });
  }

  resetDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  updateDetails(levelId: string, payload: any) {
    this.levelsDataService.updateDetails(levelId, payload).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/academic-period/levels/view']);
          }, 500);
        } else {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  deleteLevel(levelId: string) {
    return this.levelsDataService.deleteLevel(levelId);
  }
}
