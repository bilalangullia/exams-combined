import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class LevelsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };

  /* ADMINISTRATION | SYSTEM-SETUP | ACADEMIC PERIOD | LEVELS */
  getList(start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period_level/list-academic-period-level?start=0&end=5
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}_${urls.level}/${urls.list}-${urls.academic}-${urls.period}-${urls.level}?start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  searchList(keyword: string, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period_level/list-academic-period-level?start=0&end=5&keyword=year
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}_${urls.level}/${urls.list}-${urls.academic}-${urls.period}-${urls.level}?start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  addLevel(payload) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period_level/store-academic-period-level
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}_${urls.level}/${urls.store}-${urls.academic}-${urls.period}-${urls.level}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getDetails(levelId: string) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period_level/detail-academic-period-level/1
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}_${urls.level}/${urls.detail}-${urls.academic}-${urls.period}-${urls.level}/${levelId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  updateDetails(levelId: string, payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period_level/update-academic-period-level/3
    return this.httpClient
      .put(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}_${urls.level}/${urls.update}-${urls.academic}-${urls.period}-${urls.level}/${levelId}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  deleteLevel(levelId: string) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period_level/delete-academic-period-level/1
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}_${urls.level}/${urls.delete}-${urls.academic}-${urls.period}-${urls.level}/${levelId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
}
