import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../../shared/shared.module';
import { LevelsRoutingModule } from './levels-routing.module';
import { LevelsService } from './levels.service';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { LevelsDataService } from './levels-data.service';

@NgModule({
  imports: [CommonModule, LevelsRoutingModule, SharedModule],
  declarations: [ListComponent, AddComponent, ViewComponent, EditComponent],
  providers: [LevelsService, LevelsDataService]
})
export class LevelsModule {}
