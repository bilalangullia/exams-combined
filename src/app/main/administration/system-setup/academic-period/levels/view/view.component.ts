import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdModalEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { IModalConfig } from '../../../../../../shared/shared.interfaces';
import { deleteSuccess, deleteError, deleteFail } from '../../../../../../shared/shared.toasters';
import { LevelsService } from '../levels.service';
import { QUESTION_BASE } from './view.config';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Levels';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public questionBase: Array<any> = QUESTION_BASE;

  private levelId: string = null;
  public modalConfig: IModalConfig = {
    title: 'Deleting...',
    body: 'Are you sure you want to delete?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.deleteLevel(this.levelId);
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  /* Subscriptions */
  private levelIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private modalEvent: KdModalEvent,
    private sharedService: SharedService,
    private levelsService: LevelsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/academic-period/levels' },
      { type: 'edit', path: 'main/system-setup/academic-period/levels/edit' },
      {
        type: 'delete',
        callback: (): void => {
          this.modalEvent.toggleOpen();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;

    this.levelIdSub = this.levelsService.levelIdChanged.subscribe((levelId: string) => {
      if (!levelId) {
        this.router.navigate(['main/system-setup/academic-period/levels']);
      } else {
        this.levelId = levelId;
        this.levelsService.getDetails(levelId);

        setTimeout(() => {
          this.detailsSub = this.levelsService.detailsChanged.subscribe((details: any) => {
            if (details) {
              this.setDetails(details);
            }
          });
        }, 0);
      }
    });
  }

  setDetails(details) {
    this.questionBase.forEach((question) => {
      this.api.setProperty(question['key'], 'value', details[question['key']] ? details[question['key']] : '');
    });
  }

  deleteLevel(levelId) {
    this.levelsService.deleteLevel(levelId).subscribe(
      (res: any) => {
        if (res && res['message']) {
          this.sharedService.setToaster({ ...deleteSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/sytem-setup/academic-period/levels']);
          }, 100);
        } else {
          // this.sharedService.setToaster({ ...deleteFail, body: res['message'] });
        }
        this.modalEvent.toggleClose();
      },
      (err: HttpErrorResponse) => {
        if (err.status == 403) {
          this.sharedService.setToaster({ ...deleteFail, body: 'Data is linked to other items.' });
        } else {
          this.sharedService.setToaster({ ...deleteError, body: err['error']['message'] });
        }
        this.modalEvent.toggleClose();
      }
    );
  }

  ngOnDestroy() {
    if (this.levelIdSub) {
      this.levelIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.levelsService.resetDetails();
    super.destroyPageBaseSub();
  }
}
