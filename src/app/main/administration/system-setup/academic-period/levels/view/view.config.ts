import { ITableFormInput } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array<ITableFormInput> = [
  { key: 'level', label: 'Level', visible: true, controlType: 'text', type: 'string' },
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_by', label: 'Modified By', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_on', label: 'Modified On', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_by', label: 'Created By', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_on', label: 'Created On', visible: true, controlType: 'text', type: 'strong' }
];
