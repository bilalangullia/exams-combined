import { ITableColumn } from 'openemis-styleguide-lib';

interface IColumnList {
  Level?: ITableColumn;
  Name?: ITableColumn;
}

export const PAGE_SIZE: number = 20;
export const TOTAL_ROWS: number = 1000;

export const TABLE_COLUMN_LIST: IColumnList = {
  Level: {
    headerName: 'Level',
    field: 'level',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};
