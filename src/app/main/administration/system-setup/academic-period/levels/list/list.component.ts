import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  ITableConfig,
  ITableColumn,
  ITableApi,
  KdTableEvent,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { LevelsService } from '../levels.service';
import { PAGE_SIZE, TOTAL_ROWS, TABLE_COLUMN_LIST } from './list.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Levels';

  public loading: boolean = true;
  public showTable: boolean = false;
  public smallLoaderValue: number = null;
  public tableApi: ITableApi = {};
  private GRID_ID: string = 'academic-levels-list';

  public tableConfig: ITableConfig = {
    id: this.GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    paginationConfig: { pagesize: PAGE_SIZE, total: TOTAL_ROWS },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            /* Set level ID */
            this.levelsService.setLevelId(_rowNode['data']['id']);
            this.router.navigate(['main/system-setup/academic-period/levels/view']);
          }
        }
      ]
    }
  };
  public tableRows: Array<any> = null;
  public tableColumns: Array<ITableColumn> = [TABLE_COLUMN_LIST.Level, TABLE_COLUMN_LIST.Name];
  private tableEventList: KdTableDatasourceEvent;

  /* Subscriptions */
  private tableEventSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public tableEvent: KdTableEvent,
    private levelsService: LevelsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/system-setup/academic-period/levels/add' }]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;
    this.showTable = true;

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRID_ID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList);
        }, 0);
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      console.log('searching...');
      listReq = this.levelsService.searchList(searchKey, fetchParams['startRow'], fetchParams['endRow']);
    } else {
      console.log('fetching...');
      listReq = this.levelsService.getList(fetchParams['startRow'], fetchParams['endRow']);
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['response']) {
          list = res['data']['response'];
          total = res['data']['total'];

          list.sort((a, b) => a['id'] - b['id']);
        } else {
          list = [];
          total = res['data']['total'];
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  searchList(keyword: string) {
    console.log('searching with keyword ', keyword);
    return;
    if (keyword && keyword.length) {
      this.fetchList(this.tableEventList, keyword);
    } else {
      this.fetchList(this.tableEventList);
    }
  }

  ngOnDestroy() {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
