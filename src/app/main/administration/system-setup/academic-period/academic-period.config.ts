import { IKdTabs } from '../../../../shared/shared.interfaces';

const ROUTER_BASE_PATH: string = 'main/system-setup/academic-period';

export const TABS_LIST: Array<IKdTabs> = [
  { tabName: 'Levels', routerPath: ROUTER_BASE_PATH + '/levels', isActive: true },
  { tabName: 'Periods', routerPath: ROUTER_BASE_PATH + '/periods' }
];
