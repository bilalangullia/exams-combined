import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  IBreadcrumbConfig,
  IPageheaderConfig,
  IPageheaderApi,
  KdPageBaseEvent
} from 'openemis-styleguide-lib';

import { TABS_LIST } from './academic-period.config';

@Component({
  selector: 'app-academic-period',
  templateUrl: './academic-period.component.html',
  styleUrls: ['./academic-period.component.css']
})
export class AcademicPeriodComponent extends KdPageBase implements OnInit, OnDestroy {
  public pageTitle: string = 'Academic Periods';

  public breadcrumbList: IBreadcrumbConfig = { home: { name: 'Home', path: '/' } };
  public pageHeader: IPageheaderConfig = {};
  public pageHeaderApi: IPageheaderApi = {};
  public isTabActive: boolean = true;
  public tabsList: Array<any> = TABS_LIST;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public pageEvent: KdPageBaseEvent) {
    super({ router, activatedRoute, pageEvent });

    this.pageEvent.onUpdateBreadcrumb().subscribe((breadcrumbObj: IBreadcrumbConfig) => {
      this.breadcrumbList = breadcrumbObj;
    });
    this.pageEvent.onUpdatePageHeader().subscribe((headerObj: IPageheaderConfig) => {
      let newHeaderObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, headerObj);
      this.pageHeader = newHeaderObj;
    });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.tabsList[0]['isActive'] = true;
  }

  selectedTab(event) {
    // console.log('Current Active Tab - ', event);
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
