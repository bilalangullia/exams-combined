import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, ITableFormInput, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError, missingFieldsError } from '../../../../../../shared/shared.toasters';
import { PeriodsService } from '../periods.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit.config';
import { IUpdatePayloadPeriods } from '../periods.interfaces';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Periods';

  public loading: boolean = true;
  public showForm: boolean = false;
  public smallLoaderValue: number = null;
  public questionBase: Array<ITableFormInput> = QUESTION_BASE;
  public formButtons: Array<any> = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  private periodId: string = null;

  /* Subscriptions */
  private periodIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private periodsService: PeriodsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.periodIdSub = this.periodsService.periodIdChanged.subscribe((periodId: string) => {
      if (!periodId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this.router.navigate(['main/system-setup/academic-period/periods']);
        }, 0);
      } else {
        this.showForm = true;
        this.periodId = periodId;
        this.periodsService.getDetails(this.periodId);
      }
    });

    setTimeout(() => {
      this.detailsSub = this.periodsService.detailsChanged.subscribe((details: any) => {
        if (details) {
          this.setDetails(details);
        }
      });
    }, 0);
  }

  setDetails(details: any) {
    setTimeout(() => {
      this.questionBase.forEach((question) => {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']]
            ? details[question['key']]['value']
              ? details[question['key']]['key']
              : details[question['key']]
            : ''
        );
      });
    }, 0);
  }

  requiredCheck(form) {
    let hasError: boolean = false;

    this.questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(event) {
    let form: IUpdatePayloadPeriods = {
      code: event['code'],
      name: event['name'],
      start_date:
        event['start_date'] &&
        event['start_date']['text'] &&
        event['start_date']['text'].split('-').findIndex((item) => item == 'undefined') === -1
          ? event['start_date']['text']
          : '',
      end_date:
        event['end_date'] &&
        event['end_date']['text'] &&
        event['end_date']['text'].split('-').findIndex((item) => item == 'undefined') === -1
          ? event['end_date']['text']
          : '',
      current: event['current'],
      editable: event['editable'],
      visible: event['visible']
    };

    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = { ...form };
      this.periodsService.updateDetails(this.periodId, payload);
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/academic-period/periods/view']);
  }

  ngOnDestroy() {
    if (this.periodIdSub) {
      this.periodIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    // this.periodsService.resetPeriodId();
    this.periodsService.resetPeriodDetails();
    super.destroyPageBaseSub();
  }
}
