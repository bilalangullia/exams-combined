import { ITableFormInput } from 'openemis-styleguide-lib';

import { DEFAULT_OPTIONS_YES_NO } from '../../../../../../shared/shared.constants';

const currentDate = new Date();

export const QUESTION_BASE: Array<ITableFormInput> = [
  {
    key: 'parent',
    label: 'Parent',
    visible: true,
    controlType: 'text',
    type: 'string',
    readonly: true
  },
  {
    key: 'academic_period_level',
    label: 'Academic Period Level',
    visible: true,
    controlType: 'text',
    type: 'string',
    // required: true,
    readonly: true
  },
  { key: 'code', label: 'Code', visible: true, controlType: 'text', type: 'string', required: true },
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string', required: true },
  {
    key: 'start_date',
    label: 'Start Date',
    controlType: 'date',
    type: 'string',
    minDate: `${currentDate.getFullYear()}-01-01`,
    maxDate: `${currentDate.getFullYear() + 10}-12-31`,
    config: { firstDatOfWeek: 1 },
    required: true
  },
  {
    key: 'end_date',
    label: 'End Date',
    controlType: 'date',
    type: 'string',
    minDate: `${currentDate.getFullYear()}-01-01`,
    maxDate: `${currentDate.getFullYear() + 10}-12-31`,
    config: { firstDatOfWeek: 1 },
    required: true
  },
  {
    key: 'current',
    label: 'Current',
    controlType: 'dropdown',
    type: 'string',
    options: [...DEFAULT_OPTIONS_YES_NO],
    required: true
  },
  {
    key: 'editable',
    label: 'Editable',
    controlType: 'dropdown',
    type: 'string',
    options: [...DEFAULT_OPTIONS_YES_NO]
  },
  {
    key: 'visible',
    label: 'Visible',
    controlType: 'dropdown',
    type: 'string',
    options: [...DEFAULT_OPTIONS_YES_NO]
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
