import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  ITableApi,
  KdPageBaseEvent,
  ITableConfig,
  ITableColumn,
  KdTableEvent,
  KdTableDatasourceEvent,
  ITableDatasourceParams,
  IBreadcrumbConfig,
  IBreadcrumbsApi
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../../shared/shared.interfaces';
import { PeriodsService } from '../periods.service';
import { PAGESIZE, TOTALROW, TABLE_COLUMN_LIST, GRID_ID } from './list.config';
import { IParent } from '../periods.interfaces';
import { SharedService } from '../../../../../../shared/shared.service';
import { alreadyAtRoot } from '../../../../../../shared/shared.toasters';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Periods';

  public breadcrumbsList: IBreadcrumbConfig = { home: {}, list: [] };
  public breadcrumbsApi: IBreadcrumbsApi = {};

  public loading: boolean = true;
  public showTable: boolean = true;
  public smallLoaderValue: number = null;
  public tableApi: ITableApi = {};
  public tableConfig: ITableConfig = {
    id: GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    paginationConfig: { pagesize: PAGESIZE, total: TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-chevron-up',
          name: 'Go to Parent',
          custom: true,
          callback: () => {
            this.goToParent();
          }
        },
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (rowNode, tableApi): void => {
            this.periodsService.setPeriodId(rowNode['data']['id']);
            this.router.navigate(['main/system-setup/academic-period/periods/view']);
          }
        },
        {
          icon: 'fa fa-list',
          name: 'View List',
          custom: true,
          callback: (rowNode, tableApi): void => {
            this.periodsService.setParentId({
              id: rowNode['data']['id'],
              code: rowNode['data']['code'],
              name: rowNode['data']['name']
            });
            this.parentChain.push({
              id: rowNode['data']['id'],
              code: rowNode['data']['code'],
              name: rowNode['data']['name']
            });
            this.periodsService.updateParentList(true, {
              id: rowNode['data']['id'],
              code: rowNode['data']['code'],
              name: rowNode['data']['name']
            });
          }
        }
      ]
    }
  };
  public tableRows: Array<any> = null;
  public tableColumns: Array<ITableColumn> = [
    TABLE_COLUMN_LIST.Visible,
    TABLE_COLUMN_LIST.Current,
    TABLE_COLUMN_LIST.Editable,
    TABLE_COLUMN_LIST.Code,
    TABLE_COLUMN_LIST.Name,
    TABLE_COLUMN_LIST.StartDate,
    TABLE_COLUMN_LIST.EndDate,
    TABLE_COLUMN_LIST.AcademicPeriodLevel
  ];
  private tableEventList: KdTableDatasourceEvent;

  private parentId: IParent = null;
  private searchKey: string = null;
  public parentChain: Array<IParent> = [];

  /* Subscriptions */
  private parentIdSub: Subscription;
  private tableEventSub: Subscription;
  private parentListSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public tableEvent: KdTableEvent,
    private periodsService: PeriodsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/system-setup/academic-period/periods/add' },
      {
        icon: 'fa fa-chevron-left',
        tooltip: 'Go to Parent',
        custom: true,
        callback: () => {
          this.goToParent();
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;

    this.parentIdSub = this.periodsService.parentIdChanged.subscribe((parentId: IParent) => {
      if (parentId && parentId['id']) {
        this.parentId = { ...parentId };
        this.showTable = false;
        setTimeout(() => {
          this.showTable = true;
        }, 0);
      } else {
        this.parentId = null;
      }
    });

    this.parentListSub = this.periodsService.parentListChanged.subscribe((parentList: Array<IParent>) => {
      if (parentList && parentList.length) {
        this.parentChain = [...parentList];
        this.parentId = this.parentChain[this.parentChain.length - 1];
      }
    });

    this.tableEventSub = this.tableEvent.onKdTableEventList(GRID_ID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 500);
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      // this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
      // console.log(event['rowParams']['filterModel']);
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      console.log('searching...');
      listReq = this.periodsService.searchList(
        this.parentId && this.parentId['id'] ? this.parentId['id'] : '',
        searchKey,
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    } else {
      console.log('fetching...');
      listReq = this.periodsService.getList(
        this.parentId && this.parentId['id'] ? this.parentId['id'] : '',
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['response'] && res['data']['response'].length) {
          list = res['data']['response'];
          total = res['data']['total'];

          this.periodsService.setParentId({ ...res['data']['all_data'] });
          if (!this.parentChain.length) {
            this.periodsService.updateParentList(true, { ...res['data']['all_data'] });
          }

          this.tableColumns.forEach((column) => {
            if (column['field'] == 'visible' || column['field'] == 'current' || column['field'] == 'editable') {
              list.forEach((item) => {
                item[column['field']] = item[column['field']] ? 'Yes' : 'No';
              });
            }
          });
          this.showTable = true;
        } else {
          list = [];
          total = res['data']['total'];
        }

        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  searchList(keyword: string) {
    if (keyword.length && keyword.length > 2) {
      this.searchKey = keyword;
      this.fetchList(this.tableEventList, this.searchKey);
    } else if (keyword.length > 0 && keyword.length <= 2) {
      this.searchKey = null;
      this.fetchList(this.tableEventList);
    }
  }

  goToParent() {
    if (this.parentChain.length > 1) {
      // this.parentChain.pop();
      this.periodsService.updateParentList(false);
      this.periodsService.setParentId(this.parentChain[this.parentChain.length - 1]);
    } else {
      this.sharedService.setToaster(alreadyAtRoot);
    }
  }

  ngOnDestroy() {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.parentIdSub) {
      this.parentIdSub.unsubscribe();
    }
    if (this.parentListSub) {
      this.parentListSub.unsubscribe();
    }
    this.searchKey = null;
    super.destroyPageBaseSub();
  }
}
