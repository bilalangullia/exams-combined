import { ITableColumn } from 'openemis-styleguide-lib';

interface IColumnList {
  Visible?: ITableColumn;
  Current?: ITableColumn;
  Editable?: ITableColumn;
  Code?: ITableColumn;
  Name?: ITableColumn;
  StartDate?: ITableColumn;
  EndDate?: ITableColumn;
  AcademicPeriodLevel?: ITableColumn;
}

export const GRID_ID: string = 'academicPeriodsList';
export const PAGESIZE: number = 20;
export const TOTALROW: number = 1000;

export const TABLE_COLUMN_LIST: IColumnList = {
  Visible: { headerName: 'Visible', field: 'visible', sortable: false, filterable: false },
  Current: { headerName: 'Current', field: 'current', sortable: false, filterable: false },
  Editable: { headerName: 'Editable', field: 'editable', sortable: false, filterable: false },
  Code: { headerName: 'Code', field: 'code', sortable: true, filterable: true },
  Name: { headerName: 'Name', field: 'name', sortable: true, filterable: true },
  StartDate: { headerName: 'Start Date', field: 'start_date', sortable: true, filterable: true },
  EndDate: { headerName: 'End Date', field: 'end_date', sortable: true, filterable: true },
  AcademicPeriodLevel: {
    headerName: 'Academic Period Level',
    field: 'academic_period_level',
    sortable: true,
    filterable: true
  }
};
