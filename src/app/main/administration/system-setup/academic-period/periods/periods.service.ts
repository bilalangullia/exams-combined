import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../../shared/shared.service';
import { serverError, saveSuccess, saveFail, alreadyAtRoot } from '../../../../../shared/shared.toasters';
import { PeriodsDataService } from './periods-data.service';
import { IParent } from './periods.interfaces';

@Injectable()
export class PeriodsService {
  private parentId: IParent;
  private parentList: Array<IParent> = [];
  private periodId: string = null;
  private levelOptions: Array<any> = [];
  private details: any = null;

  public parentIdChanged = new BehaviorSubject<IParent>({ ...this.parentId });
  public parentListChanged = new BehaviorSubject<Array<IParent>>([...this.parentList]);
  public periodIdChanged = new BehaviorSubject<string>(this.periodId);
  public detailsChanged = new BehaviorSubject<any>({ ...this.details });
  public levelOptionsChanged = new BehaviorSubject<any>([...this.levelOptions]);

  constructor(
    private router: Router,
    private periodsDataService: PeriodsDataService,
    private sharedService: SharedService
  ) {}

  setParentId(parentId: IParent) {
    this.parentId = { ...parentId };
    this.parentIdChanged.next({ ...this.parentId });
  }

  updateParentList(append: boolean, parentItem?: IParent) {
    if (append) {
      if (this.parentList.findIndex((item) => item['id'] == parentItem['id']) == -1) {
        this.parentList.push(parentItem);
      }
    } else {
      if (this.parentList.length && this.parentList.length > 1) {
        this.parentList.pop();
      } else {
        this.sharedService.setToaster(alreadyAtRoot);
      }
    }
    this.parentListChanged.next([...this.parentList]);
  }

  resetParendId() {
    this.parentId = null;
    this.parentIdChanged.next(null);
  }

  getList(parentId: string, start: number, end: number) {
    return this.periodsDataService.getList(parentId ? parentId : '1', start, end);
  }

  searchList(parentId: string, keyword: string, start: number, end: number) {
    return this.periodsDataService.searchList(parentId, keyword, start, end);
  }

  getOptionsAcademicPeriodLevel() {
    this.periodsDataService.getOptionsAcademicPeriodLevels().subscribe((res: any) => {
      if (res && res['data']) {
        this.levelOptions = res['data'];
        this.levelOptionsChanged.next([...this.levelOptions]);
      }
    });
  }

  addPeriod(payload) {
    this.periodsDataService.addPeriod(payload).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster(saveSuccess);
          setTimeout(() => {
            this.router.navigate(['main/system-setup/academic-period/periods']);
          }, 0);
        } else if (!res || !res['data']) {
          this.sharedService.setToaster(saveFail);
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  setPeriodId(periodId: string) {
    this.periodId = periodId;
    this.periodIdChanged.next(this.periodId);
  }

  resetPeriodId() {
    this.periodId = null;
    this.periodIdChanged.next(null);
  }

  getDetails(periodId: string) {
    this.periodsDataService.getDetails(periodId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.details = res['data'][0];
          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = null;
          this.detailsChanged.next(null);
        }
      },
      (err) => {
        this.details = null;
        this.detailsChanged.next(null);
      }
    );
  }

  resetPeriodDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  updateDetails(periodId: string, payload: any) {
    this.periodsDataService.updateDetails(periodId, payload).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/system-setup/academic-period/periods/view']);
          }, 100);
        } else {
          this.sharedService.setToaster({ ...saveFail, body: res['message'] });
        }
      },
      (err: HttpErrorResponse) => {
        console.log(err);
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  deletePeriod(periodId: string) {
    return this.periodsDataService.deletePeriod(periodId);
  }
}
