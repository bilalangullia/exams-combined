import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, ITableFormInput, KdModalEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { invalidIdError, deleteSuccess, deleteFail, deleteError } from '../../../../../../shared/shared.toasters';
import { IModalConfig } from '../../../../../../shared/shared.interfaces';
import { PeriodsService } from '../periods.service';
import { QUESTION_BASE } from './view.config';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Period';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public questionBase: Array<ITableFormInput> = QUESTION_BASE;

  private periodId: string = null;
  public modalConfig: IModalConfig = {
    title: 'Deleting...',
    body: 'Are you sure you want to delete?',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.deletePeriod(this.periodId);
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this.modalEvent.toggleClose();
        }
      }
    ]
  };

  /* Subscriptions */
  private periodIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private modalEvent: KdModalEvent,
    private periodsService: PeriodsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/academic-period/periods' },
      { type: 'edit', path: 'main/system-setup/academic-period/periods/edit' },
      {
        type: 'delete',
        callback: (params: any): void => {
          this.modalEvent.toggleOpen();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.loading = false;

    setTimeout(() => {
      this.periodIdSub = this.periodsService.periodIdChanged.subscribe((periodId: string) => {
        if (!periodId) {
          this.sharedService.setToaster(invalidIdError);
          this.router.navigate(['main/system-setup/academic-period']);
        } else {
          this.periodId = periodId;
          this.periodsService.getDetails(this.periodId);
          this.detailsSub = this.periodsService.detailsChanged.subscribe((details: any) => {
            if (details) {
              this.setDetails(details);
            }
          });
        }
      });
    }, 0);
  }

  setDetails(details) {
    this.questionBase.forEach((question: any) => {
      this.api.setProperty(
        question['key'],
        'value',
        details[question['key']]
          ? details[question['key']]['value']
            ? details[question['key']]['value']
            : details[question['key']]
          : ''
      );
    });
  }

  deletePeriod(periodId: string) {
    this.periodsService.deletePeriod(periodId).subscribe(
      (res: any) => {
        if (res && res['message']) {
          this.sharedService.setToaster({ ...deleteSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/system-setup/academic-period/periods']);
          }, 100);
        } else {
          // this.sharedService.setToaster({ ...deleteFail, body: res['message'] });
        }
        this.modalEvent.toggleClose();
      },
      (err: HttpErrorResponse) => {
        if (err.status == 403) {
          this.sharedService.setToaster({ ...deleteFail, body: 'Data is linked to other items.' });
        } else {
          this.sharedService.setToaster({ ...deleteError, body: err['error']['message'] });
        }
        this.modalEvent.toggleClose();
      }
    );
  }

  ngOnDestroy() {
    if (this.periodIdSub) {
      this.periodIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.periodsService.resetPeriodDetails();
    super.destroyPageBaseSub();
  }
}
