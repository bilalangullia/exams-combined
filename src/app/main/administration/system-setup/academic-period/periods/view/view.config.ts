import { ITableFormInput } from 'openemis-styleguide-lib';

export const QUESTION_BASE: Array<ITableFormInput> = [
  { key: 'visible', label: 'Visible', visible: true, controlType: 'text', type: 'string' },
  { key: 'current', label: 'Current', visible: true, controlType: 'text', type: 'string' },
  { key: 'editable', label: 'Editable', visible: true, controlType: 'text', type: 'string' },
  { key: 'code', label: 'Code', visible: true, controlType: 'text', type: 'string' },
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string' },
  { key: 'start_date', label: 'Start Date', visible: true, controlType: 'text', type: 'string' },
  { key: 'end_date', label: 'End Date', visible: true, controlType: 'text', type: 'string' },
  { key: 'academic_period_level', label: 'Academic Period Level', visible: true, controlType: 'text', type: 'string' },
  { key: 'parent', label: 'Parent', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_by', label: 'Modified By', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_on', label: 'Modified On', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_by', label: 'Created By', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_on', label: 'Created On', visible: true, controlType: 'text', type: 'string' }
];
