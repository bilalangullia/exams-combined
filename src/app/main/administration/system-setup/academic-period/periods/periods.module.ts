import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeriodsRoutingModule } from './periods-routing.module';
import { PeriodsService } from './periods.service';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { PeriodsDataService } from './periods-data.service';

@NgModule({
  imports: [CommonModule, PeriodsRoutingModule, SharedModule],
  declarations: [ListComponent, AddComponent, ViewComponent, EditComponent],
  providers: [PeriodsService, PeriodsDataService]
})
export class PeriodsModule {}
