import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBaseEvent, KdPageBase, ITableFormInput, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../../shared/shared.service';
import { missingFieldsError } from '../../../../../../shared/shared.toasters';
import { PeriodsService } from '../periods.service';
import { IParent, IAddPayloadPeriods } from '../periods.interfaces';
import { QUESTION_BASE, FORM_BUTTONS } from './add.config';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Periods';
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public questionBase: Array<ITableFormInput> = QUESTION_BASE;
  public formButtons: Array<any> = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  private parentId: IParent;

  /* Subscriptions */
  private parentIdSub: Subscription;
  private levelsOptions: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private periodsService: PeriodsService,
    private sharedService: SharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setToolbarMainBtns([]);
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();
    this.loading = false;

    this.periodsService.getOptionsAcademicPeriodLevel();

    setTimeout(() => {
      this.parentIdSub = this.periodsService.parentIdChanged.subscribe((parentId: IParent) => {
        this.parentId = parentId;
      });

      this.levelsOptions = this.periodsService.levelOptionsChanged.subscribe((options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('academic_period_level_id', [...options]);
        }
      });
    }, 0);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<any> = [];
    options = data.map((item) => ({ key: item['id'], value: item['name'] }));
    options.unshift({ key: null, value: '-- Select --' });
    this.api.setProperty(key, 'options', [...options]);

    setTimeout(() => {
      this.api.setProperty(key, 'value', options[0]['key']);
      this.setDetails();
    }, 0);
  }

  setDetails() {
    this.api.setProperty('parent', 'value', this.parentId['name']);
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if ((question['required'] && !form[question['key']]) || form[question['key']] == 'null') {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This is a required field.']);
      }
    });
    return hasError;
  }

  submit(event) {
    let form: IAddPayloadPeriods = {
      parent: +this.parentId['id'],
      academic_period_level_id: event['academic_period_level_id'],
      code: event['code'],
      name: event['name'],
      start_date:
        event['start_date'] &&
        event['start_date']['text'] &&
        event['start_date']['text'].split('-').findIndex((item) => item == 'undefined') === -1
          ? event['start_date']['text']
          : '',
      end_date:
        event['end_date'] &&
        event['end_date']['text'] &&
        event['end_date']['text'].split('-').findIndex((item) => item == 'undefined') === -1
          ? event['end_date']['text']
          : '',
      current: event['current'],
      editable: event['editable']
    };

    if (this.requiredCheck(form)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = { ...form };
      this.periodsService.addPeriod(payload);
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/academic-period/periods']);
  }

  ngOnDestroy() {
    if (this.parentIdSub) {
      this.parentIdSub.unsubscribe();
    }
    if (this.levelsOptions) {
      this.levelsOptions.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
