export interface IParent {
  id?: string;
  name?: string;
  code?: string;
}

export interface IUpdatePayloadPeriods {
  code: string;
  name: string;
  start_date: string;
  end_date: string;
  current: number;
  editable: number;
  visible: number;
}

export interface IAddPayloadPeriods {
  parent: number;
  academic_period_level_id: number;
  code: string;
  name: string;
  start_date: string;
  end_date: string;
  current: number;
  editable?: number;
}
