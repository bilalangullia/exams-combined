import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import urls from '../../../../../shared/config.urls';

@Injectable()
export class PeriodsDataService {
  constructor(private http: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        if (error['message'].includes('token') || error['message'].includes('expired')) {
          localStorage.removeItem('token');
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };

  getList(parentId: string, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period/list-academic-period?start=0&end=5&parent_id=1
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}/${urls.list}-${urls.academic}-${urls.period}?parent_id=${parentId}&start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  searchList(parentId: string, keyword: string, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period/list-academic-period?start=0&end=5&parent_id=1&keyword=2018
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}/${urls.list}-${urls.academic}-${urls.period}?paretn_id=${parentId}start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getOptionsAcademicPeriodLevels() {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period/academic-period-level-dropdown
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}/${urls.academic}-${urls.period}-${urls.level}-${urls.dropdown}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  addPeriod(payload) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period/store-academic-period
    return this.http
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}/${urls.store}-${urls.academic}-${urls.period}`,
        payload,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getDetails(levelId: string) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period/detail-academic-period/2
    return this.http
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}/${urls.detail}-${urls.academic}-${urls.period}/${levelId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  updateDetails(periodId: string, payload: any) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period/update-academic-period/5
    return this.http
      .put(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}/${urls.update}-${urls.academic}-${urls.period}/${periodId}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  deletePeriod(periodId: string) {
    // http://openemis.n2.iworklab.com/api/administration/systemsetup/academic_period/delete-academic-period/4
    return this.http
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.system}${urls.setup}/${urls.academic}_${urls.period}/${urls.delete}-${urls.academic}-${urls.period}/${periodId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
}
