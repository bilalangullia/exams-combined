export const FILTER_INPUTS: Array<any> = [
  {
    key: 'field-option',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      { key: 1, value: 'Attendance types' },
      { key: 2, value: 'Countries' },
      { key: 3, value: 'Classification' },
      { key: 4, value: 'Component Types' },
      { key: 5, value: 'Exam Session' },
      { key: 6, value: 'Exam Types' },
      { key: 7, value: 'Gender' },
      { key: 8, value: 'Mark Status' },
      { key: 9, value: 'Processing Types' },
      { key: 10, value: 'Special Needs Assessment Type' },
      { key: 11, value: 'Special Needs Difficulties' },
      { key: 12, value: 'Qualification Levels' },
      { key: 13, value: 'Qualification Titles' },
      { key: 14, value: 'Qualification Specialisations' },
      { key: 15, value: 'Mark Types' },
      { key: 16, value: 'Item Types' }
    ],
    events: true
  },
  {
    key: 'field-option2',
    visible: false,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: 1,
        value: 'General'
      },
      {
        key: 2,
        value: 'Vocational'
      }
    ],
    events: true
  }
];
