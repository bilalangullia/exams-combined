import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  ITableApi,
  KdAlertEvent,
  ITableDatasourceParams,
  KdTableDatasourceEvent
} from 'openemis-styleguide-lib';

import { IFetchListParams } from '../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../shared/shared.service';
import { FieldOptionSharedService } from '../field-option-shared.service';
import { TABLECOLUMN } from './list-field.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  private results$ = new Observable<any>();
  private subject = new BehaviorSubject<any>(null);

  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;

  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public currentFilters: any;
  public showTable: boolean = true;
  public _tableEvent;
  public isSearch: boolean = false;
  public _row: Array<any> = [];

  /* Subscriptions */
  private tableEventSub: Subscription;
  private educationSystemFilterSub: Subscription;
  private currentFiltersSub: Subscription;

  public tableEventList: KdTableDatasourceEvent;

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Visible,
    TABLECOLUMN.Default,
    TABLECOLUMN.Editable,
    TABLECOLUMN.Name,
    TABLECOLUMN.EducationFieldOfStudy,
    TABLECOLUMN.ItemDescription,
    TABLECOLUMN.InternaltionalCode,
    TABLECOLUMN.NationalCode,
    TABLECOLUMN.QualificationLevel
  ];
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.fieldOptionService.setFieldId({
              id: _rowNode['data']['id'],
              key: this.currentFilters['field-option']['key'],
              header: this.currentFilters['field-option']['value']
            });
            this.router.navigate(['main/system-setup/field-options/view']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public fieldOptionService: FieldOptionSharedService
  ) {
    super({ router, pageEvent, activatedRoute });
    super.setPageTitle('Education - Levels', false);
    super.setToolbarMainBtns([{ type: 'add', path: 'main/system-setup/field-options/add' }]);
  }

  ngOnInit() {
    this.loading = false;

    super.enableToolbarSearch(true, (event: any): void => {
      if (event.length > 2) {
        this.subject.next(event);
      }
    });

    this.currentFiltersSub = this.fieldOptionService.currentFiltersChanged.subscribe((filter: any) => {
      if (
        filter &&
        filter['field-option'] &&
        (filter['field-option'].value == 'Qualification Titles' || filter['field-option'].value == 'Item Types')
      ) {
        this.currentFilters = { ...filter };
        this.showTable = false;
        setTimeout(() => {
          this.setColumn(filter['field-option'].value);
          this.setHeader(filter['field-option'].value);
        }, 100);
      } else if (
        filter &&
        filter['field-option'] &&
        filter['field-option2'] &&
        filter['field-option'].value == 'Qualification Specialisations' &&
        filter['field-option'].key == '14'
      ) {
        this.currentFilters = { ...filter };
        this.showTable = false;
        setTimeout(() => {
          this.setColumn(filter['field-option'].value);
          this.setHeader(filter['field-option'].value);
        }, 100);
      } else if (filter && filter['field-option']) {
        this.currentFilters = { ...filter };

        this.showTable = false;
        setTimeout(() => {
          this.setColumn(filter['field-option'].value);
          this.setHeader(filter['field-option'].value);
        }, 100);
      }
    });

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList);
        }, 200);
      }
    });
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: boolean, keyword?: string) {
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      listReq = this.fieldOptionService.searchList(
        keyword,
        this.currentFilters['field-option']['key'],
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    } else if (this.currentFilters['field-option']['value'] == 'Qualification Specialisations') {
      listReq = this.fieldOptionService.getList(
        this.currentFilters['field-option']['key'],
        fetchParams['startRow'],
        fetchParams['endRow'],
        this.currentFilters['field-option2']['key']
      );
    } else {
      listReq = this.fieldOptionService.getList(
        this.currentFilters['field-option']['key'],
        fetchParams['startRow'],
        fetchParams['endRow']
      );
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['record'] && res['data']['record'].length) {
          list = res['data']['record'];
          total = res['data']['total'];
          list.forEach((item) => {
            item['visible'] = item['visible'] == 1 ? 'Yes' : 'No';
            item['default'] = item['default'] == 1 ? 'Yes' : 'No';
            item['editable'] = item['editable'] == 1 ? 'Yes' : 'No';
          });
          this.tableColumns.forEach((column, colIndex) => {
            let filterValues: Array<any> = [];
            list.forEach((item) => {
              if (item[column['field']] && !filterValues.includes(item[column['field']])) {
                filterValues.push(item[column['field']]);
              }
            });
            column['filterValue'] = filterValues;
          });
        } else {
          list = [];
          total = res['data']['total'];
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
      }
    );
  }

  searchList(search) {}

  setHeader(data: any) {
    super.setPageTitle('Field Options - ' + data, false);
    super.updatePageHeader();
    super.updateBreadcrumb();
  }

  setColumn(data) {
    if (data == 'Qualification Titles') {
      this.tableColumns.forEach((item, i) => {
        if (item.field == 'itemDescription') {
          item.visible = false;
        }
        if (item.field == 'name') {
          item.visible = true;
        }
        if (item.field == 'qualificationLevel') {
          item.visible = true;
        }
        if (item.field == 'educationfieldofstudy') {
          item.visible = false;
        }
      });
    } else if (data == 'Item Types') {
      this.tableColumns.forEach((item, i) => {
        if (item.field == 'itemDescription') {
          item.visible = true;
        }
        if (item.field == 'name') {
          item.visible = false;
        }
        if (item.field == 'qualificationLevel') {
          item.visible = false;
        }
        if (item.field == 'educationfieldofstudy') {
          item.visible = false;
        }
      });
    } else if (data == 'Qualification Specialisations') {
      this.tableColumns.forEach((item, i) => {
        if (item.field == 'itemDescription') {
          item.visible = false;
        }
        if (item.field == 'name') {
          item.visible = true;
        }
        if (item.field == 'qualificationLevel') {
          item.visible = false;
        }
        if (item.field == 'educationfieldofstudy') {
          item.visible = true;
        }
      });
    } else {
      this.tableColumns.forEach((item, i) => {
        if (item.field == 'itemDescription') {
          item.visible = false;
        }
        if (item.field == 'name') {
          item.visible = true;
        }
        if (item.field == 'qualificationLevel') {
          item.visible = false;
        }
        if (item.field == 'educationfieldofstudy') {
          item.visible = false;
        }
      });
    }
    this.showTable = true;
  }

  debounce(callBack) {
    this.subject.pipe(debounceTime(1500)).subscribe((res: any) => {
      res ? callBack.call(this, this.tableEventList, true, res) : callBack.call(this, this.tableEventList, false);
    });
  }
  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.educationSystemFilterSub) {
      this.educationSystemFilterSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
