interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Visible?: Column;
  Name?: Column;
  Editable?: Column;
  Default?: Column;
  InternaltionalCode?: Column;
  NationalCode?: Column;
  QualificationLevel?: Column;
  EducationFieldOfStudy?: Column;
  ItemDescription?: Column;
}

export const TABLECOLUMN: ListColumn = {
  Visible: {
    headerName: 'Visible',
    field: 'visible',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Default: {
    headerName: 'Default',
    field: 'default',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Editable: {
    headerName: 'Editable',
    field: 'editable',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Name: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  InternaltionalCode: {
    headerName: 'Internaltional Code',
    field: 'international_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  NationalCode: {
    headerName: 'National Code',
    field: 'national_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  QualificationLevel: {
    headerName: 'Qualification Level',
    field: 'qualificationLevel',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: false
  },
  EducationFieldOfStudy: {
    headerName: 'EducationField Of Study',
    field: 'educationfieldofstudy',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: false
  },
  ItemDescription: {
    headerName: 'Item Description',
    field: 'itemDescription',
    sortable: true,
    filterable: true,
    filterValue: [],
    visible: false
  }
};
