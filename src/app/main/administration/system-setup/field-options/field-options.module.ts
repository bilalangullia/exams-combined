import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { FieldOptionsRoutingModule } from './field-options-routing.module';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { FieldOptionComponent } from './field-option.component';
import { FieldOptionsDataService } from './field-options-data.service';
import { FieldOptionSharedService } from './field-option-shared.service';

@NgModule({
  imports: [CommonModule, FieldOptionsRoutingModule, SharedModule],
  declarations: [AddComponent, ViewComponent, ListComponent, EditComponent, FieldOptionComponent],
  providers: [FieldOptionsDataService, FieldOptionSharedService]
})
export class FieldOptionsModule {}
