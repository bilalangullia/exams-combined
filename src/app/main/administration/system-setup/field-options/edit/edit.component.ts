import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, ITableFormInput, IDynamicFormApi } from 'openemis-styleguide-lib';

import { invalidIdError, missingFieldsError, saveSuccess, saveFail } from '../../../../../shared/shared.toasters';
import { SharedService } from '../../../../../shared/shared.service';
import { FieldOptionSharedService } from '../../field-options/field-option-shared.service';
import { QUESTION_BASE, FORM_BUTTONS } from './edit-field.config';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Field Options -';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public questionBase: Array<ITableFormInput> = QUESTION_BASE;
  public formButtons: Array<any> = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  public fieldDetail: any = {};

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private sharedService: SharedService,
    private fieldOptionService: FieldOptionSharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    this.resSetfield();
    this.fieldOptionService.fieldEditDetail.subscribe((fieldData: string) => {
      if (!fieldData) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this.router.navigate(['main/system-setup/field-options/list']);
        }, 0);
      } else {
        this.fieldDetail = fieldData;
        this.setDetails(fieldData);
      }
    });
  }

  setDetails(data: any) {
    if (this.fieldDetail.header == 'Qualification Titles') {
      this.questionBase.forEach((item, i) => {
        if (item.key == 'item_description') {
          item.visible = false;
        }
        if (item.key == 'name') {
          item.visible = true;
        }
        if (item.key == 'qualification_titles') {
          item.visible = true;
        }
        if (item.key == 'education_field') {
          item.visible = false;
          item.required = false;
        }
        if (data[item.key]) {
          if (item['key'] == 'default' || item['key'] == 'visible') {
            item.value = data[item.key].key;
          } else {
            item.value = data[item.key];
          }
        }
      });
    } else if (this.fieldDetail.header == 'Qualification Specialisations') {
      this.questionBase.forEach((item, i) => {
        if (item.key == 'item_description') {
          item.visible = false;
        }
        if (item.key == 'name') {
          item.visible = true;
        }
        if (item.key == 'education_field') {
          item.visible = true;
          item.required = true;
        }
        if (data[item.key]) {
          if (item['key'] == 'default' || item['key'] == 'visible') {
            item.value = data[item.key].key;
          } else if (item['key'] == 'education_field') {
            item.value = data['education_field'] == 'General' ? 1 : 2;
          } else {
            item.value = data[item.key];
          }
        }
      });
    } else {
      this.questionBase.forEach((item, i) => {
        if (item.key == 'item_description') {
          item.visible = false;
        }
        if (item.key == 'name') {
          item.visible = true;
        }
        if (item.key == 'qualification_titles') {
          item.visible = false;
        }
        if (item.key == 'education_field') {
          item.visible = false;
          item.required = false;
        }
        if (data[item.key]) {
          if (item['key'] == 'default' || item['key'] == 'visible') {
            item.value = data[item.key].key;
          } else {
            item.value = data[item.key];
          }
        }
      });
    }
    this.setHeader(this.fieldDetail.header);
    this.loading = false;
  }
  requiredCheck(form) {
    let hasError: boolean = false;
    for (let i = 0; i < this.questionBase.length; i++) {
      if (this.questionBase[i]['required']) {
        if (form[this.questionBase[i]['key']] == '' || form[this.questionBase[i]['key']] == null) {
          setTimeout(() => {
            this.api.setProperty(this.questionBase[i]['key'], 'errors', ['This field is required']);
          }, 1000);
          hasError = true;
          break;
        }
      }
    }
    return hasError;
  }
  setHeader(data: any) {
    super.setPageTitle('Field Options - ' + data, false);
    super.updatePageHeader();
    super.updateBreadcrumb();
  }

  submit(form) {
    console.log('form', form);

    let form1 = {
      default: String(form.default),
      international_code: form.international_code,
      name: form.name,
      national_code: form.national_code,
      visible: String(form.visible),
      education_field: this.fieldDetail.header == 'Qualification Specialisations' ? form.education_field : 'NA'
    };
    if (this.requiredCheck(form1)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        key: this.fieldDetail.key,
        id: this.fieldDetail.id,
        name: form.name,
        default: form.default,
        visible: form.visible,
        international_code: form.international_code,
        national_code: form.national_code,
        education_field_of_study:
          this.fieldDetail.header == 'Qualification Specialisations' ? form.education_field : 'NA'
      };

      this.fieldOptionService.updateFieldOption(payload).subscribe(
        (res: any) => {
          if (res && res['data']) {
            this.sharedService.setToaster(saveSuccess);
            setTimeout(() => {
              this.router.navigate(['main/system-setup/field-options/list']);
            }, 500);
          } else {
            this.sharedService.setToaster(saveFail);
          }
        },
        (err) => {
          this.sharedService.setToaster(saveFail);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/field-options/list']);
  }

  resSetfield() {
    this.questionBase.forEach((item) => {
      item.value = '';
    });
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
