import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi,
  ITableFormInput,
  KdModalEvent,
  KdAlertEvent
} from 'openemis-styleguide-lib';
import { FieldOptionSharedService } from '../../field-options/field-option-shared.service';
import { SharedService } from '../../../../../shared/shared.service';
import { invalidIdError } from '../../../../../shared/shared.toasters';
import { QUESTION_BASE, IModalConfig } from './view-field.config';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
  providers: [KdModalEvent]
})
export class ViewComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Academic Period - Period';

  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public questionBase: Array<ITableFormInput> = QUESTION_BASE;
  public fieldDetail: any;

  public modalConfig: IModalConfig = {
    title: 'Delete fieldoption',
    body: 'Are you sure you want to delete fieldoption',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.fieldOptionService.deleteFieldOption(this.fieldDetail).subscribe(
            (res: any) => {
              this._modelEvent.toggleClose();
              let toasterConfig: any = {
                type: 'Success',
                title: 'Fieldoption deleted successfully ',
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 3000
              };
              this._kdalert.success(toasterConfig);
              this.router.navigate(['main/system-setup/field-options/list']);
            },
            (err: any) => {
              this._modelEvent.toggleClose();

              let toasterConfig: any = {
                type: 'error',
                title: err.error.message,
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 3000
              };
              this._kdalert.error(toasterConfig);
            }
          );
        }
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
  };
  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public fieldOptionService: FieldOptionSharedService,
    private sharedService: SharedService,
    public _modelEvent: KdModalEvent,
    public _kdalert: KdAlertEvent
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Field Options - ', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/system-setup/field-options/list' },
      { type: 'edit', path: 'main/system-setup/field-options/edit' },
      {
        type: 'delete',
        callback: (params: any): void => {
          this.open();
        }
      }
    ]);
  }

  ngOnInit() {
    this.fieldOptionService.fieldDetail.subscribe((fieldata: string) => {
      if (!fieldata) {
        this.sharedService.setToaster(invalidIdError);
        this.loading = false;
        this.router.navigate(['main/system-setup/field-options/list']);
      } else {
        this.fieldDetail = fieldata;
        this.fieldOptionService.setViewDetail(fieldata).subscribe((res: any) => {
          this.fieldDetail.id = res.data.id;
          this.fieldOptionService.fieldEditDetail.next({
            ...this.fieldDetail,
            ...res.data
          });
          this.setDetails(res.data);
        });
      }
    });
  }

  setDetails(data) {
    if (this.fieldDetail.header == 'Qualification Titles') {
      this.questionBase.forEach((item, i) => {
        if (item.key == 'item_description') {
          item.visible = false;
        }
        if (item.key == 'name') {
          item.visible = true;
        }
        if (item.key == 'qualification_titles') {
          item.visible = true;
        }
        if (item.key == 'education_field') {
          item.visible = false;
        }
        if (data[item.key]) {
          if (item['key'] == 'default' || item['key'] == 'visible') {
            item.value = data[item.key].value;
          } else {
            item.value = data[item.key];
          }
        } else {
          item.value = 'NA';
        }
      });
    } else if (this.fieldDetail.header == 'Item Types') {
      this.questionBase.forEach((item, i) => {
        if (item.key == 'item_description') {
          item.visible = true;
        }
        if (item.key == 'name') {
          item.visible = false;
        }
        if (item.key == 'qualification_titles') {
          item.visible = false;
        }
        if (item.key == 'education_field') {
          item.visible = false;
        }
        if (data[item.key]) {
          if (item['key'] == 'default' || item['key'] == 'visible') {
            item.value = data[item.key].value;
          } else {
            item.value = data[item.key];
          }
        } else {
          item.value = 'NA';
        }
      });
    } else if (this.fieldDetail.header == 'Qualification Specialisations') {
      this.questionBase.forEach((item, i) => {
        if (item.key == 'education_field') {
          item.visible = true;
        }
        if (item.key == 'item_description') {
          item.visible = false;
        }
        if (item.key == 'name') {
          item.visible = true;
        }
        if (item.key == 'qualification_titles') {
          item.visible = false;
        }
        if (data[item.key]) {
          if (item['key'] == 'default' || item['key'] == 'visible') {
            item.value = data[item.key].value;
          } else {
            item['key'] == 'education_field' ? (item.value = data['education_field']) : 'NA';
            item.value = data[item.key];
          }
        } else {
          item.value = 'NA';
        }
      });
    } else {
      this.questionBase.forEach((item, i) => {
        if (item.key == 'item_description') {
          item.visible = false;
        }
        if (item.key == 'name') {
          item.visible = true;
        }
        if (item.key == 'qualification_titles') {
          item.visible = false;
        }
        if (item.key == 'education_field') {
          item.visible = false;
        }
        if (data[item.key]) {
          if (item['key'] == 'default' || item['key'] == 'visible') {
            item.value = data[item.key].value;
          } else {
            item.value = data[item.key];
          }
        } else {
          item.value = 'NA';
        }
      });
    }

    this.setHeader(this.fieldDetail.header);
    this.loading = false;
  }

  deletePeriod(periodId: string) {}

  setHeader(data: any) {
    super.setPageTitle('Field Options - ' + data, false);
    super.updatePageHeader();
    super.updateBreadcrumb();
  }

  open() {
    this._modelEvent.toggleOpen();
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
