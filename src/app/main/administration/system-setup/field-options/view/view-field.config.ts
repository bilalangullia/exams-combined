export const QUESTION_BASE: Array<any> = [
  { key: 'education_field', label: 'Educational', visible: false, controlType: 'text', type: 'string' },
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string' },
  { key: 'visible', label: 'Visible', visible: true, controlType: 'text', type: 'string' },
  { key: 'default', label: 'Default', visible: true, controlType: 'text', type: 'string' },
  { key: 'international_code', label: 'International Code', visible: true, controlType: 'text', type: 'string' },
  { key: 'national_code', label: 'National Code', visible: true, controlType: 'text', type: 'string' },
  { key: 'qualification_titles', label: 'Qualification Titles', visible: false, controlType: 'text', type: 'string' },
  { key: 'item_description', label: 'Item Description', visible: false, controlType: 'text', type: 'string' },
  { key: 'modified_by', label: 'Modified By', visible: true, controlType: 'text', type: 'string' },
  { key: 'modified_on', label: 'Modified On', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_by', label: 'Created By', visible: true, controlType: 'text', type: 'string' },
  { key: 'created_on', label: 'Created On', visible: true, controlType: 'text', type: 'string' }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}

export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}
