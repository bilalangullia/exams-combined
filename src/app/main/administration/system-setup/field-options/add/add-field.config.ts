export const QUESTION_BASE: Array<any> = [
  { key: 'name', label: 'Name', visible: true, controlType: 'text', type: 'string', required: true },
  {
    key: 'education_field',
    label: 'Education Field of Study',
    controlType: 'dropdown',
    type: 'string',
    visible: false,
    options: [
      { key: null, value: '-- Select --' },
      {
        key: 1,
        value: 'General'
      },
      {
        key: 2,
        value: 'Vocational'
      }
    ],
    required: false
  },
  {
    key: 'default',
    label: 'Default',
    controlType: 'dropdown',
    type: 'string',
    options: [
      { key: null, value: '-- Select --' },
      { key: 0, value: 'NO' },
      { key: 1, value: 'YES' }
    ],
    required: true
  },
  { key: 'international_code', label: 'International Code', visible: true, controlType: 'text', type: 'string' },
  { key: 'national_code', label: 'National Code', visible: true, controlType: 'text', type: 'string' }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
