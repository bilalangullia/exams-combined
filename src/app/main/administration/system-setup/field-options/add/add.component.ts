import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBaseEvent, KdPageBase, ITableFormInput, IDynamicFormApi } from 'openemis-styleguide-lib';

import { invalidIdError, saveSuccess, saveFail } from '../../../../../shared/shared.toasters';
import { SharedService } from '../../../../../shared/shared.service';
import { FieldOptionSharedService } from '../../field-options/field-option-shared.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add-field.config';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public smallLoaderValue: number = null;
  public questionBase: Array<ITableFormInput> = QUESTION_BASE;
  public formButtons: Array<any> = FORM_BUTTONS;
  public api: IDynamicFormApi = {};
  public fieldDetail: any = {};

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private sharedService: SharedService,
    private fieldOptionService: FieldOptionSharedService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setToolbarMainBtns([]);
    super.setPageTitle('Field Options -', false);
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.updatePageHeader();

    this.fieldOptionService.currentFiltersChanged.subscribe((filedData: string) => {
      if (!filedData) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this.router.navigate(['main/system-setup/field-options/list']);
        }, 0);
      } else {
        this.fieldDetail = filedData;

        this.setHeader(this.fieldDetail['field-option']['value']);
      }
    });
  }

  requiredCheck(form) {
    let hasError: boolean = false;
    for (let i = 0; i < this.questionBase.length; i++) {
      if (this.questionBase[i]['required']) {
        if (!form[this.questionBase[i]['key']] || form[this.questionBase[i]['key']] == '') {
          setTimeout(() => {
            this.api.setProperty(this.questionBase[i]['key'], 'errors', ['This field is required']);
          }, 1000);
          hasError = true;
          break;
        }
      }
    }
    return hasError;
  }

  submit(form) {
    if (!this.requiredCheck(form)) {
      let payload = {
        key: this.fieldDetail['field-option']['key'],
        name: form.name,
        default: form.default,
        international_code: form.international_code,
        national_code: form.national_code,
        education_field_of_study:
          this.fieldDetail['field-option']['value'] == 'Qualification Specialisations' ? form.education_field : 'NA'
      };
      this.fieldOptionService.addFieldOption(payload).subscribe(
        (res: any) => {
          if (res && res['data']) {
            this.sharedService.setToaster(saveSuccess);
            setTimeout(() => {
              this.router.navigate(['main/system-setup/field-options/list']);
            }, 500);
          } else {
            this.sharedService.setToaster(saveFail);
          }
        },
        (err) => {
          this.sharedService.setToaster(saveFail);
        }
      );
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
    }
  }

  cancel() {
    this.router.navigate(['main/system-setup/field-options/list']);
  }

  setHeader(data: any) {
    super.setPageTitle('Field Options - ' + data, false);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.questionBase.forEach((item) => {
      if (data == 'Qualification Specialisations') {
        item['key'] == 'education_field' ? (item.visible = true) : 'na';
        item['key'] == 'education_field' ? (item.value = this.fieldDetail['field-option2']['key']) : (item.value = '');
        item['key'] == 'education_field' ? (item.required = true) : 'na';
      } else {
        item['key'] == 'education_field' ? (item.visible = false) : 'na';
        item['key'] == 'education_field' ? (item.value = '') : 'na';
        item['key'] == 'education_field' ? (item.required = false) : 'na';
      }
    });
    this.loading = false;
  }

  ngOnDestroy() {
    super.destroyPageBaseSub();
  }
}
