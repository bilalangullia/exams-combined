import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, IPageheaderApi, IPageheaderConfig, KdFilter } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../shared/shared.service';
import { FieldOptionSharedService } from './field-option-shared.service';
import { FILTER_INPUTS } from './field-option.config';
import { timer } from 'rxjs';

@Component({
  selector: 'app-field-option',
  templateUrl: './field-option.component.html',
  styleUrls: ['./field-option.component.css']
})
export class FieldOptionComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') filter: KdFilter;
  private pageTitle = 'Field Options - Attendance Types';

  public filterInputs = FILTER_INPUTS;
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = { home: { name: 'Home', path: '/' }, list: [] };

  constructor(
    public pageEvent: KdPageBaseEvent,
    private router: Router,
    activatedRoute: ActivatedRoute,
    public sharedService: SharedService,
    public fieldOprionSrv: FieldOptionSharedService
  ) {
    super({ router, pageEvent, activatedRoute });
    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });
    console.log('dddd...ddd');
  }

  ngOnInit() {
    super.setPageTitle(this.pageTitle, false);
    super.updatePageHeader();
    this.fieldOprionSrv.setCurrentFilters({ 'field-option': { key: '1', value: 'Attendance types' } });
  }

  detectValue(event) {
    let filter: any;
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (event.key == 'field-option') {
      if (event.value != '14') {
        filter = { [event['key']]: filterObj };
        this.fieldOprionSrv.setCurrentFilters(filter);
        this.filter.setInputProperty('field-option2', 'visible', false);
        this.router.navigate(['main/system-setup/field-options/list']);
      } else if (event.value == '14') {
        filter = { [event['key']]: filterObj };

        timer(10).subscribe(() => {
          this.filter.setInputProperty('field-option2', 'visible', true);
          this.fieldOprionSrv.setCurrentFilters(filter);
        });
      }
    } else if (event.key != 'field-option') {
      filter = { [event['key']]: filterObj };
      this.fieldOprionSrv.setCurrentFilters(filter);
      this.router.navigate(['main/system-setup/field-options/list']);
    }
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
