import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../../shared/shared.service';
import { serverError, saveSuccess, saveFail } from '../../../../shared/shared.toasters';
import { FieldOptionsDataService } from './field-options-data.service';

@Injectable({ providedIn: 'root' })
export class FieldOptionSharedService {
  private currentFilters: any = {};

  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  public fieldDetail = new BehaviorSubject<any>(null);
  public fieldEditDetail = new BehaviorSubject<any>(null);

  constructor(private fieldServ: FieldOptionsDataService) {}

  setCurrentFilters(filter: any) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** LIST PAGE | List Data ***/
  getList(fieldId: any, start?: any, end?: any, fieldId2?: any) {
    return this.fieldServ.getList(fieldId, start, end, fieldId2);
  }

  getTotalRecords(fieldId: any, start?: any, end?: any) {
    return this.fieldServ.getList(fieldId, start, end);
  }

  /*** List Data | SEARCH***/
  searchList(keyword: any, fieldId: any, start?: any, end?: any) {
    return this.fieldServ.getListSearch(keyword, fieldId, start, end);
  }

  setFieldId(detail: any) {
    this.fieldDetail.next({ ...detail });
  }

  setViewDetail(data) {
    return this.fieldServ.getViewDetail(data);
  }

  updateFieldOption(data: any) {
    return this.fieldServ.updateDetail(data);
  }

  addFieldOption(data: any) {
    return this.fieldServ.addDetail(data);
  }

  deleteFieldOption(data: any) {
    return this.fieldServ.deleteOption(data);
  }

  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }
}
