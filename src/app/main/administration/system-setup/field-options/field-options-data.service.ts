import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../.././../../environments/environment';
import urls from '../../../../shared/config.urls';

@Injectable()
export class FieldOptionsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  getList(fieldId: any, start: any, end: any, fielId2?: any) {
    return fielId2
      ? this.httpClient
          .get(
            `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.fieldOption}/list?start=${start}&end=${end}&key=${fieldId}&studyId=${fielId2}`,
            this.setHeader()
          )
          .pipe(catchError(this.handleError))
      : this.httpClient
          .get(
            `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.fieldOption}/list?start=${start}&end=${end}&key=${fieldId}`,
            this.setHeader()
          )
          .pipe(catchError(this.handleError));
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(keyword: any, fieldId: any, start?: any, end?: any) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.fieldOption}/list?start=${start}&end=${end}&key=${fieldId}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getViewDetail(data: any) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.fieldOption}/detail/${data.id}?key=${data.key}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  updateDetail(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.fieldOption}/update`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  addDetail(data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.fieldOption}/store`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  deleteOption(data) {
    return this.httpClient
      .delete(
        `${environment.baseUrl}/${urls.administration}/${urls.systemSetup}/${urls.fieldOption}/destory?key=${data.key}&id=${data.id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }

    return throwError(error);
  };
}
