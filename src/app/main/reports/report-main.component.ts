import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent,IPageheaderApi, IPageheaderConfig } from 'openemis-styleguide-lib';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-report-main',
  templateUrl: './report-main.component.html',
  styleUrls: ['./report-main.component.css']
})
export class ReportMainComponent extends KdPageBase implements OnInit, OnDestroy {

  public pageTitle = 'Reports-Registrations';
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = {
    home: {
      "name": "jhgh",
      "path": "/www.google.com",
    }
  }

  constructor(public pageEvent: KdPageBaseEvent, router: Router, activatedRoute: ActivatedRoute, public sharedService: SharedService) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    //  console.log("test...reports")
    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });
    
    this.pageEvent.onUpdateBreadcrumb() .subscribe(_breadcrumbObj => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {
    // super.setPageTitle('Registrations - Reports', false);
    // super.updatePageHeader();
  }

  ngOnDestroy(): void {
    // super.destroyPageBaseSub();
  }
}