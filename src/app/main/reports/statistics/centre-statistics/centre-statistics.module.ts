import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { CentreStatisticsRoutingModule } from './centre-statistics-routing.module';
import { AddCentreStatisticsReportComponent } from './add-centre-statistics-report/add-centre-statistics-report.component';
import { CentreStatisticsListComponent } from './centre-statistics-list/centre-statistics-list.component';

@NgModule({
  imports: [
    CommonModule,
    CentreStatisticsRoutingModule,
    SharedModule
  ],
  declarations: [AddCentreStatisticsReportComponent, CentreStatisticsListComponent]
})
export class CentreStatisticsModule { }
