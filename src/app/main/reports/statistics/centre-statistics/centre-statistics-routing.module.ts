import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CentreStatisticsListComponent} from './centre-statistics-list/centre-statistics-list.component';
import {AddCentreStatisticsReportComponent} from './add-centre-statistics-report/add-centre-statistics-report.component';

const routes: Routes = [ {path:'' ,redirectTo:'list'},    
{ path: 'list', component: CentreStatisticsListComponent}, 
{ path: 'add', component: AddCentreStatisticsReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CentreStatisticsRoutingModule { }
