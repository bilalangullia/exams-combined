import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';

import { ExamStatisticRoutingModule } from './exam-statistic-routing.module';
import { AddExamstatisticReportComponent } from './add-examstatistic-report/add-examstatistic-report.component';
import { ExamstatisticListComponent } from './examstatistic-list/examstatistic-list.component';

@NgModule({
  imports: [
    CommonModule,
    ExamStatisticRoutingModule,
    SharedModule
  ],
  declarations: [AddExamstatisticReportComponent, ExamstatisticListComponent]
})
export class ExamStatisticModule { }
