import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddExamstatisticReportComponent} from './add-examstatistic-report/add-examstatistic-report.component';
import {ExamstatisticListComponent} from './examstatistic-list/examstatistic-list.component';

const routes: Routes = [ {path:'' ,redirectTo:'list'},    
{ path: 'list', component: ExamstatisticListComponent}, 
{ path: 'add', component: AddExamstatisticReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamStatisticRoutingModule { }
