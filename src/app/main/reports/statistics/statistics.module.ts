import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { StatisticsRoutingModule } from './statistics-routing.module';

@NgModule({
  imports: [
    CommonModule,
    StatisticsRoutingModule,
    SharedModule
  ],
  declarations: []
})
export class StatisticsModule { }
