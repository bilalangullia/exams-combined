import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [     
{ path: 'statisticexam', loadChildren:'./exam-statistic/exam-statistic.module#ExamStatisticModule' },
{ path: 'statisticentre', loadChildren:'./centre-statistics/centre-statistics.module#CentreStatisticsModule' }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatisticsRoutingModule { }
