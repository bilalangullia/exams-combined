import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportMainComponent } from './report-main.component';
import { RegistrationComponent } from './registration/registration.component';
import { AddReportComponent } from './add-report/add-report.component';

const routes: Routes = [
  {
    path: '',
    component: ReportMainComponent,
    children:[
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: RegistrationComponent },
      { path: 'add', component: AddReportComponent },
      {path:'form', loadChildren:'./forms/forms.module#FormsModule'},
      {path:'marks',loadChildren:'./reportsmark/reportsmark.module#ReportsmarkModule'} ,
      {path:'results',loadChildren:'./results/report.results.module#ReportResultsModule'},
      {path:'certificates',loadChildren:'./certificates/reports-certificates.module#ReportsCertificatesModule'},
      {path:'statistics',loadChildren:'./statistics/statistics.module#StatisticsModule'}

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
