import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import  {ResultsListComponent} from './results-list/results-list.component';
import  {AddResultsComponent} from './add-results/add-results.component'

const routes: Routes = [
  {path:'' ,redirectTo:'list'},    
  { path: 'list', component: ResultsListComponent}, 
  { path: 'add', component: AddResultsComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultsRoutingModule { }
