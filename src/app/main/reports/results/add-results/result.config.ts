import { TREE_CONFIG_AREA } from '../../../registration/candidates/add-candidate/add.config';

export interface ITreeConfig {
    id: string;
    selectionMode: string;
    list?: object;
    expandAll?: boolean;
    expandSelected?: boolean;
  }

export const VIEWNODE_INPUT: Array<any> = [
    {
        'key': 'name',
        'label': 'Name',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [
            {
                'key': 'null',
                'value': '--Select--'
            }, {
                'key': 1
                ,
                'value': 'Statement Of Results'
            }        
        ],
        'events': true
    },
    {
        'key': 'academic_period_id',
        'label': 'Academic Period',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true,
    },
    {
        'key': 'examination_id',
        'label': 'Examination',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true,
    },
    {
        'key': 'area_id',
        'label': 'Area Name',
        'visible': true,
        'required': true,
        'controlType': 'tree',
        'config': TREE_CONFIG_AREA
    },
    {
        'key': 'examination_centre_id',
        'label': 'Exam Center',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
          'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },{
        'key': 'candidate_id',
        'label': 'Candidate Id',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
          'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
      {
        'key': 'format',
        'label': 'Format',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }, {
            'key': 1,
            'value': 'PDF'
        }],
        'events': true,
    },

];