import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { ResultsRoutingModule } from './results-routing.module';
import { AddResultsComponent } from './add-results/add-results.component';
import { ResultsListComponent } from './results-list/results-list.component';

@NgModule({
  imports: [
    CommonModule,
    ResultsRoutingModule,
    SharedModule
  ],
  declarations: [AddResultsComponent, ResultsListComponent]
})
export class ReportResultsModule { }
