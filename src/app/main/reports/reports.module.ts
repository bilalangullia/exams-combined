import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';
import { ReportMainComponent } from './report-main.component';
import { SharedModule } from '../../shared/shared.module';
import { RegistrationComponent } from './registration/registration.component';
import { AddReportComponent } from './add-report/add-report.component';


@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule
  ],
  declarations: [ReportMainComponent,RegistrationComponent,AddReportComponent]
})
export class ReportsModule { }
