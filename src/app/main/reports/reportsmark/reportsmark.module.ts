import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsmarkRoutingModule } from './reportsmark-routing.module';
import { AddMarkReportComponent } from './add-mark-report/add-mark-report.component';
import { MarkListComponent } from './mark-list/mark-list.component';
import { SharedModule } from '../../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    ReportsmarkRoutingModule,
    SharedModule
  ],
  declarations: [AddMarkReportComponent, MarkListComponent]
})
export class ReportsmarkModule { }
