import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddMarkReportComponent} from '../reportsmark/add-mark-report/add-mark-report.component';
import {MarkListComponent} from '../reportsmark/mark-list/mark-list.component';


const routes: Routes = [     
{path:'' ,redirectTo:'list' },    
{ path: 'list', component: MarkListComponent}, 
{ path: 'add', component: AddMarkReportComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsmarkRoutingModule { }
