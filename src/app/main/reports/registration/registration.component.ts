import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';
import { TABLECOLUMN, tempData } from './registration.config';
import { DataService } from '../../../shared/data.service';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>;
  public isValues: boolean = true;

  public _tableApi: ITableApi = {};

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Name,
    TABLECOLUMN.StartedOn,
    TABLECOLUMN.GeneratedBy,
    TABLECOLUMN.CompletedOn,
    TABLECOLUMN.ExpiresOn,
    TABLECOLUMN.Status
  ];

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-download',
          name: ' Download',
          custom: true,
          callback: (_rowNode: any, _tableApi): void => {
            const found = this._row.find((element) => element.name == _rowNode.data.name);
            this.exportToPdf(found.file_path);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    public _kdalert: KdAlertEvent,
    private _advFilterEvent: KdAdvFilterEvent,
    private _toolbarEvent: KdToolbarEvent,
    public _tableEvent: KdTableEvent,
    public dataServices: DataService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    // setInterval(()=>{
    //   TABLECOLUMN.Status.config.input.config.displayValue +=10;
    //   console.log(TABLECOLUMN.Status.config.input.config.displayValue)
    // },1000)
  }

  ngOnInit() {
    super.setPageTitle('Reports ­- Registrations', false);
    super.setToolbarMainBtns([
      {
        type: 'add',
        path: '/main/reports/add'
      }
    ]);

    super.enableToolbarSearch(true, (_event: any): void => {
      console.log('_event.._event', _event);
    });
    super.updatePageHeader();
    super.updateBreadcrumb();
    this._toolbarEvent.onSendSearchText().subscribe((value: any): void => {});
    this.reportslist();
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
  reportslist() {
    this.dataServices.getRegistrationReportList().subscribe((data: any) => {
      this._row = data.data.reports;

      this.isValues = false;
      this._row.forEach((rowdata) => {
        if (TABLECOLUMN.Name.filterValue.length == 0) {
          TABLECOLUMN.Name.filterValue.push(rowdata.name);
          TABLECOLUMN.StartedOn.filterValue.push(rowdata.started);
          TABLECOLUMN.GeneratedBy.filterValue.push(rowdata.created_user_id);
          TABLECOLUMN.CompletedOn.filterValue.push(rowdata.completed);
          TABLECOLUMN.ExpiresOn.filterValue.push(rowdata.expiry_date);
        } else {
          TABLECOLUMN.Name.filterValue.indexOf(rowdata.name) > -1
            ? 'na'
            : TABLECOLUMN.Name.filterValue.push(rowdata.name);
          TABLECOLUMN.StartedOn.filterValue.indexOf(rowdata.started) > -1
            ? 'na'
            : TABLECOLUMN.StartedOn.filterValue.push(rowdata.started);
          TABLECOLUMN.GeneratedBy.filterValue.indexOf(rowdata.openemis_no) > -1
            ? 'na'
            : TABLECOLUMN.GeneratedBy.filterValue.push(rowdata.openemis_no);
          TABLECOLUMN.CompletedOn.filterValue.indexOf(rowdata.completed) > -1
            ? 'na'
            : TABLECOLUMN.CompletedOn.filterValue.push(rowdata.completed);
          TABLECOLUMN.ExpiresOn.filterValue.indexOf(rowdata.ExpiresOn) > -1
            ? 'na'
            : TABLECOLUMN.ExpiresOn.filterValue.push(rowdata.ExpiresOn);
        }
      });
    });
  }

  exportToPdf = async (data: any) => {
    // const file = new Blob(["dskj"], { type: 'application/pdf' });
    // console.log("file..", file)
    // const fileURL = window.URL.createObjectURL(file);
    // console.log("fileURL..fileURL", fileURL)
    var link = document.createElement('a');
    link.href = data;
    link.target = 'blank';
    link.download = 'registration.pdf';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };
}
