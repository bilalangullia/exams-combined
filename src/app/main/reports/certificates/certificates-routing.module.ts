import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddCertificateComponent} from '../certificates/add-certificate/add-certificate.component';
import  {CertificateListComponent} from '../certificates/certificate-list/certificate-list.component'

const routes: Routes = [      
{path:'' ,redirectTo:'list' },    
{ path: 'list', component: CertificateListComponent }, 
{ path: 'add', component: AddCertificateComponent },
]; 

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificatesRoutingModule { }
