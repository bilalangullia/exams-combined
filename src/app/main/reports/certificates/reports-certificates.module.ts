import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificatesRoutingModule } from './certificates-routing.module';
import { AddCertificateComponent } from './add-certificate/add-certificate.component';
import { CertificateListComponent } from './certificate-list/certificate-list.component';
import { SharedModule } from '../../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    CertificatesRoutingModule,
    SharedModule
  ],
  declarations: [AddCertificateComponent, CertificateListComponent]
})
export class ReportsCertificatesModule { }
