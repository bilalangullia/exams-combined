import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportMainComponent } from '../report-main.component';
import  {FormListComponent} from '../forms/form-list/form-list.component'
import  {AddFormComponent} from  '../forms/add-form/add-form.component'

const routes: Routes = [
      {path:'' ,redirectTo:'list' },    
      { path: 'list', component: FormListComponent }, 
      { path: 'add', component: AddFormComponent },
      {path:'grade',  loadChildren:'./grade/grade.module#GradeModule'},
      {path:'multichoice',loadChildren:'./multichoice/multichoice.module#MultichoiceModule'}
];;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
