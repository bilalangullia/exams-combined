import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsRoutingModule } from './forms-routing.module';
import { FormListComponent } from './form-list/form-list.component';
import { AddFormComponent } from './add-form/add-form.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({ 
  imports: [
    CommonModule,
    FormsRoutingModule,
    SharedModule 
  ],
  declarations: [FormListComponent, AddFormComponent]
})
export class FormsModule { }
