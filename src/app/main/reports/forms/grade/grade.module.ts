import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GradeRoutingModule } from './grade-routing.module';
import { GradeListComponent } from './grade-list/grade-list.component';
import { AddReportComponent } from './add-report/add-report.component';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    GradeRoutingModule,
    SharedModule
  ],
  declarations: [GradeListComponent, AddReportComponent]
})
export class GradeModule { }
