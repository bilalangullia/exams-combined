import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MultichoiceRoutingModule } from './multichoice-routing.module';
import { MultichoiceListComponent } from './multichoice-list/multichoice-list.component';
import { AddReportComponent } from './add-report/add-report.component';
import { SharedModule } from '../../../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    MultichoiceRoutingModule,
    SharedModule
  ],
  declarations: [MultichoiceListComponent, AddReportComponent]
})
export class MultichoiceModule { }
