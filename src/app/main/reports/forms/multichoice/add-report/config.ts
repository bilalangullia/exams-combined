export const VIEWNODE_INPUT: Array<any> = [
    {
        'key': 'name',
        'label': 'Name',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [
            {
                'key': 'null',
                'value': '--Select--' 
            }, {
                'key': 1,
                'value': 'Multiple Choice'
            }
        ],
        'events': true
    },
    {
        'key': 'academic_period_id',
        'label': 'Academic Period',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true,
    },
    {
        'key': 'examination_id',
        'label': 'Examination',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true,
    },
    {
        'key': 'examination_centre_id',
        'label': 'Exam Center',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
          'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
    {
        'key': 'candidate_id',
        'label': 'Candidate ID',
        'visible': true,
        'required': false,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
    {
        'key': 'subject',
        'label': 'Subject', 
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
    {
        'key': 'format',
        'label': 'Format',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }, {
            'key': 1,
            'value': 'PDF'
        }],
        'events': true,
    },

];