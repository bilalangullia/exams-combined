import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import  {MultichoiceListComponent} from './multichoice-list/multichoice-list.component'
import {AddReportComponent} from './add-report/add-report.component'

const routes: Routes = [{path:'' ,redirectTo:'list' },    
{ path: 'list', component: MultichoiceListComponent }, 
{ path: 'add', component: AddReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MultichoiceRoutingModule { }
