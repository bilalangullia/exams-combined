import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase, KdPageBaseEvent, KdToolbarEvent,
  KdAdvFilterEvent, KdTableEvent, ITableConfig, ITableColumn,
  ITableApi, KdAlertEvent, KdTableSelectionUpdateEvent
  , ITableDatasourceParams, KdTableDatasourceEvent, KdIntTableEvent
}
  from 'openemis-styleguide-lib';

  import { SharedService } from '../../../../../shared/shared.service';
  import { DataService } from '../../../../../shared/data.service';  

import { TABLECOLUMN } from './config' 
import { Subscription } from 'rxjs'; 

@Component({
  selector: 'app-multichoice-list',
  templateUrl: './multichoice-list.component.html',
  styleUrls: ['./multichoice-list.component.css']
})
export class MultichoiceListComponent extends KdPageBase implements OnInit, OnDestroy {

  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listTable';
  public PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>=[];
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public _tableApi: ITableApi = {};

  public Isvalue: boolean = true;
  public pageCount: number = 20;
  public Isorted: boolean = false;
  public isChecked: boolean = false;
  public startRow: Number = 0
  public rowFlag: boolean = false
  public searchDetail: any = {};

  private _tableEventSubscription: Subscription;

  public setLimit: any = {};


  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.Name,
    TABLECOLUMN.StartedOn,
    TABLECOLUMN.GeneratedBy,
    TABLECOLUMN.CompletedOn,
    TABLECOLUMN.ExpiresOn,
    TABLECOLUMN.Status
  ]

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    }, action: {
      enabled: true,
      list: [
        {
          icon: "fa fa-download",
          name: " Download",
          custom: true,
          callback: (_rowNode: any, _tableApi): void => {
            const found = this._row.find(element => (element.name == _rowNode.data.name));
            this.exportToPdf(found.file_path)
          }
        }
      ]
    }
  };  

  constructor(public pageEvent: KdPageBaseEvent, public router: Router,
    activatedRoute: ActivatedRoute, public _kdalert: KdAlertEvent,
    private _advFilterEvent: KdAdvFilterEvent,
    private _toolbarEvent: KdToolbarEvent,
    public _tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public dataService: DataService
  ) {

    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }


  ngOnInit() {
    super.setPageTitle('Reports ­- Forms', false);
    super.setToolbarMainBtns([
      {
        type: "add",
        path: '/main/reports/form/multichoice/add'
      }
    ]);

    super.updatePageHeader();
    super.updateBreadcrumb();

   this.setPageLimit({start:0,end:20})
   
    this._tableEventSubscription = this._tableEvent.onKdTableEventList(this.GRIDID).subscribe((_event: any): void => {
      if (_event instanceof KdTableDatasourceEvent) {
        this.hasValue(_event.rowParams.sortModel.length);
        if (_event.rowParams.startRow != this.startRow && this.rowFlag) {
          this.getMultipleChoiceList(_event).then((res: any) => {
            this._row = res;
            let datasourceParams: ITableDatasourceParams = {
              rows: this._row,
              total: this.pageCount
            };
            this.Isorted = false;
            this.startRow = _event.rowParams.startRow;
            _event.subscriber.next(datasourceParams);
            _event.subscriber.complete();
          });
        } else if (this.Isorted) {
          this.sortedList(_event).then((res: any) => {
            this._row = res;
            let datasourceParams: ITableDatasourceParams = {
              rows: this._row,
              total: this.pageCount
            };
            this.rowFlag = false;
            this.startRow = _event.rowParams.startRow;
            _event.subscriber.next(datasourceParams);
            _event.subscriber.complete();
          });
        }  else {
          let datasourceParams: ITableDatasourceParams = {
            rows: this._row,
            total: this.pageCount
          };
          this.rowFlag = true;
          _event.subscriber.next(datasourceParams);
          _event.subscriber.complete();
        }
      } else if (_event instanceof KdTableSelectionUpdateEvent) {
        console.log('KdTableSelectionUpdateEvent', _event);
      }
    });


  }



  getMultipleChoiceList(data) {
    return new Promise((resolve, reject) => {
      this.setLimit = { start: data.rowParams.startRow, end: data.rowParams.endRow };
      this.dataService.getMultipleChoiceFormList(this.setLimit).subscribe((res: any) => {
         resolve(this.setListFilter1(res.data.reports));
        this.pageCount = res.data.total;
      });
    });
  }

  setPageLimit(data) {
    this.dataService.getMultipleChoiceFormList(data).subscribe((res: any) => {
        this.setListFilter(res.data.reports);
        this.pageCount = res.data.total;
    });
  }

 
  setListFilter(data) {
    this.resetFilter();
  
    if (data.length > 0) {
       this._row = data;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (data[j][this.tableColumns[i]['field']] && data[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(data[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
      this.loading = false;
      this.Isvalue=false;
    } else {
      this.loading = false;
      this.Isvalue=false;
      this._row.length = 0;
      let toasterConfig: any = {
        type: 'error',
        title: 'No records found',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this._kdalert.error(toasterConfig);
    }
  }

  setListFilter1(data) {
    this.resetFilter();
    if (data.length > 0) {
      

      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (data[j][this.tableColumns[i]['field']] && data[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(data[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
      this.loading = false;
      return data;
    } else {
      this.loading = false;
      this._row.length = 0;
      let toasterConfig: any = {
        type: 'error',
        title: 'No records found',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this._kdalert.error(toasterConfig);
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  sortedList(data) {
    let sortValue = data.rowParams.sortModel[0].colId;
    var order = data.rowParams.sortModel[0].sort;

    return new Promise((resolve, reject) => {
      this.setLimit = { start: data.rowParams.startRow, end: data.rowParams.endRow };
      this.dataService.getMultipleChoiceFormList(this.setLimit).subscribe((res: any) => {
        
          order == 'asc'
            ? res.data.reports.sort(function (a, b) {
              var keyA = a[sortValue],
                keyB = b[sortValue];
              if (keyA < keyB) return -1;
              if (keyA > keyB) return 1;
              return 0;
            })
            : res.data.reports.sort(function (a, b) {
              var keyA = a[sortValue], 
                keyB = b[sortValue];
              if (keyA > keyB) return -1;
              if (keyA < keyB) return 1;
              return 0;
            });

        resolve(this.setListFilter1(res.data.reports));
        this.Isorted = true;
        this.pageCount = res.data.total;
      });
    });
  }

  hasValue(data) {
    if (data > 0) {
      this.rowFlag = false;
      this.Isorted = true;
    } else {
      this.rowFlag = true;
      this.Isorted = false;
    }
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
    if (typeof this._tableEventSubscription !== 'undefined') {
      this._tableEventSubscription.unsubscribe();
    }
  }

  exportToPdf = async (data: any) => {

    var link = document.createElement("a");
    link.href = data;
    link.target = 'blank'
    link.download = 'multiplechoice.pdf';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

}

