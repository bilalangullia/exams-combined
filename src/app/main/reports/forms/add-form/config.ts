export const VIEWNODE_INPUT: Array<any> = [
    {
        'key': 'name',
        'label': 'Name',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [
            {
                'key': 'null',
                'value': '--Select--'
            }, 
             {
                'key': 2,
                'value': 'MS1 Internal Assement Marks Sheet'
            },
            {
                'key': 3,
                'value': 'MS2 Examiner Mark Sheet'
            },
            {
                'key': 4,
                'value': 'Scaling Form'
            }
        ],
        'events': true
    },
    {
        'key': 'academic_period_id',
        'label': 'Academic Period',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true,
    },
    {
        'key': 'examination_id',
        'label': 'Examination',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true,
    },
    {
        'key': 'examination_centre_id',
        'label': 'Exam Center',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
          'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
    {
        'key': 'subject',
        'label': 'Subject',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
    {
        'key': 'option',
        'label': 'Option',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
    {
        'key': 'component',
        'label': 'Component',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }],
        'events': true
    },
    {
        'key': 'format',
        'label': 'Format',
        'visible': true,
        'required': true,
        'order': 1,
        'controlType': 'dropdown',
        'options': [{
            'key': 'null',
            'value': '--Select--'
        }, {
            'key': 1,
            'value': 'PDF'
        }],
        'events': true,
    },

];