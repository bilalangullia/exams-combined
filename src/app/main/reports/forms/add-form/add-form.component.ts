import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi,

} from 'openemis-styleguide-lib'

import { VIEWNODE_INPUT } from './config';
import { SharedService } from '../../../../shared/shared.service';
import { DataService } from '../../../../shared/data.service';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent extends KdPageBase implements OnInit, OnDestroy {
  public defaultValues: any = {};
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  private _treeDropdownSubscription: { [key: string]: Subscription } = {};
  public isValues: boolean = true;

  public loading: boolean = false;

  public _questionBase = VIEWNODE_INPUT;
  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline'
    }
  ];
  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public sharedService: SharedService,
    public dataService: DataService,
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

  }


  ngOnInit() {
    super.setPageTitle('Reports ­- Form', false);

    super.updatePageHeader();
    super.updateBreadcrumb();

    this.sharedService.getDropdownValues().subscribe((data: any) => {
      this.defaultValues = data;
      if (this.defaultValues) {
        let keyArr = Object.keys(this.defaultValues.data);
        keyArr.forEach(e => {
          let quesObj = this._questionBase.find(q => {
            return q.key == e;
          });

          if (quesObj) {
            quesObj.options.length = 0;
            quesObj.options.push(
              ...this.defaultValues.data[e].map(item => {
                return { key: item.id, value: item.name };
              })
            );
            this.formValue.academic_period_id = quesObj.options[0].key;
            this.getExamination(this.formValue.academic_period_id)
          }
        });
      }
    });
  }



  getExamination(id) {
    this.dataService.getExamination(id).subscribe(
      (data: any) => {
        if (data.data.length > 0) {

          this.isValues = false;
          this.loading = true;

          this.formValue.examination_id = data.data[0].id;
          let temp = [
            ...data.data.map(item => {
              return { key: item.id, value: item.name };
            })
          ];

          setTimeout(() => {
            this.api.setProperty('examination_id', 'options', temp);
            this.getExamCenter(this.formValue);
            this.getSubject(this.formValue.examination_id)
          }, 1000);
        }
        else {
          this.isValues = false
          setTimeout(() => {
            let value = ['examination_id', 'examination_centre_id']
            this.resetDropdown(value)
          }, 1000);
        }
      },
      err => {
        console.log(err);
        setTimeout(() => {
          let value = ['examination_id', 'examination_centre_id']
          this.isValues = false;
          this.resetDropdown(value);
        }, 1000);
      }
    );
  }

  getExamCenter(data) {

    if (data) {
      this.dataService.getOptionsExamCenter(data.examination_id).subscribe(
        (data: any) => {
          if (data.data.length > 0) {

            this.formValue.examination_centre_id = data.data[0].id;

            let temp = [
              ...data.data.map(item => {
                return { key: item.id, value: item.code + ' - ' + item.name };
              })
            ];

            this.api.setProperty('examination_centre_id', 'options', temp);
          }
          else {

            setTimeout(() => {
              let value = ['examination_centre_id','subject']
              this.isValues = false;
              this.resetDropdown(value);
            }, 1000);
            let toasterConfig: any = {
              type: 'error',
              title: 'Centre list not found for selected examintation',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);

          }

        },
        err => {
          console.log(err);
          setTimeout(() => {
            let value = ['examination_centre_id','subject']
            this.isValues = false;
            this.resetDropdown(value);
          }, 1000);

          let toasterConfig: any = {
            type: 'error',
            title: 'something went wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);

        }
      );
    } else {
            setTimeout(() => {
        this.isValues = false;
        let value = [ 'examination_centre_id','subject']
        this.resetDropdown(value);
      }, 1000);
    }
  }

  getSubject(data) {

    if (data) {
      this.dataService.getSubjectByExaminationID(data).subscribe(
        (data: any) => {
          if (data.data.length > 0) {

            this.formValue.subject = data.data[0].id
            let temp = [
              ...data.data.map(item => {
                return { key: item.id, value: item.code + '-' + item.name };
              })
            ];
            this.api.setProperty('subject', 'options', temp);
            this.getOption(this.formValue.subject)
          }
          else {
            setTimeout(() => {
              let value = ['subject', 'option']
              this.resetDropdown(value);
              let toasterConfig: any = {
                type: 'error',
                title: 'No subject found',
                showCloseButton: true,
                tapToDismiss: false,
                timeout: 3000
              };
              this.sharedService.setToaster(toasterConfig);
            })

          }
        },
        err => {
          setTimeout(() => {
            let value = ['subject', 'option']
            this.resetDropdown(value);
          }, 1000);

          let toasterConfig: any = {
            type: 'error',
            title: 'No subject found',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    } else {

      setTimeout(() => {
        let value = ['subject', 'option']
        this.loading = true;
        this.isValues = false
        this.resetDropdown(value);
      }, 1000);
    }
  }

  getOption(data) {

    if (data) {
      this.dataService.getOptionBySubjectId(data).subscribe(
        (data: any) => {
          this.loading = false
          if (data.data.length > 0) {
            this.loading = true
            this.formValue.option = data.data[0].id
            let temp = [
              ...data.data.map(item => {
                return { key: item.id, value: item.code + '-' + item.name };
              })
            ];
            this.api.setProperty('option', 'options', temp);
            this.getComponent(this.formValue.option)
          }
          else {
            this.loading = true
            setTimeout(() => {
              let value = ['option', 'component']
              this.resetDropdown(value);
              let toasterConfig: any = {
                type: 'error',
                title: 'No option found',
                showCloseButton: true,
                tapToDismiss: false,
                timeout: 3000
              };
              this.sharedService.setToaster(toasterConfig);
            })

          }
        },
        err => {
          setTimeout(() => {
            let value = ['option', 'component']
            this.resetDropdown(value);
          }, 1000);

          let toasterConfig: any = {
            type: 'error',
            title: 'No option found',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    } else {
      setTimeout(() => {
        let value = ['option', 'component']
        this.resetDropdown(value);
      }, 1000);
    }
  }


  getComponent(data) {

    if (data) {
      this.dataService.getMarkImportComponentList(data).subscribe(
        (data: any) => {
          this.loading = false
          if (data.data.length > 0) {
            this.loading = true
            this.formValue.component = data.data[0].id
            let temp = [
              ...data.data.map(item => {
                return { key: item.id, value: item.code + '-' + item.name };
              })
            ];
            this.api.setProperty('component', 'options', temp);
          }
          else {
            this.loading = true
            setTimeout(() => {
              let value = ['component']
              this.resetDropdown(value);
              let toasterConfig: any = {
                type: 'error',
                title: 'No component found',
                showCloseButton: true,
                tapToDismiss: false,
                timeout: 3000
              };
              this.sharedService.setToaster(toasterConfig);
            })

          }
        },
        err => {
          setTimeout(() => {
            let value = ['component']
            this.resetDropdown(value);
          }, 1000);

          let toasterConfig: any = {
            type: 'error',
            title: 'No component found',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    } else {
      setTimeout(() => {
        let value = ['component']
        this.resetDropdown(value);
      }, 1000);
    }
  }




  requiredCheck() {
    let hasError: boolean;
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]['required']) {
        if (
          !this.formValue[this._questionBase[i]['key']] ||
          this.formValue[this._questionBase[i]['key']] == '' ||
          this.formValue[this._questionBase[i]['key']] == 'null' ||
          this.formValue[this._questionBase[i]['key']] == {}
        ) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]['key'], 'errors', ['This field is required']);
          }, 1000);
          break;
        }
      }
    }
    if (hasError) {
      return false;
    } else {
      return true;
    }
  }



  detectValue(question: any) {
    this.formValue[question.key] = question.value;
    this.callSpecific(question);
  }

  callSpecific(data) {
    switch (data.key) {
      case 'academic_period_id': {
        this.getExamination(data.value);
        break;
      }
      case 'examination_id': {
        this.getExamCenter(this.formValue);
        this.getSubject(this.formValue.examination_id);

        break;
      }

      case 'subject': {
        this.getOption(this.formValue.subject);
      }
      case 'option': {
        this.getComponent(this.formValue.option);

      }
    }
  }




  submitVal(formVal: any): void {
    console.log("formVal", this.formValue);


    if (this.requiredCheck()) {
      let tempData = {
        report_name: this.formValue.name,
        academic_period_id: this.formValue.academic_period_id,
        examination_id: this.formValue.examination_id,
        examination_centre_id: this.formValue.examination_centre_id,
        subject_id:this.formValue.subject,
        component_id:this.formValue.component,
        option_id:this.formValue.option,
        format: this.formValue.format
      };

      this.dataService.formScalingleMSReport(tempData).subscribe(
        (data: any) => {
          let toasterConfig: any = {
            type: 'success',
            title: 'Report Generated Successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this.router.navigate(['/main/reports/form/list']);
        },
        err => {
          let toasterConfig: any = {
            type: 'error',
            title: 'Something Went Wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
    }
  }

  resetDropdown(data) {

    let temp = [{ key: null, value: '--Select--' }];
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < this._questionBase.length; j++) {
        if (this._questionBase[j].key == data[i]) {
          this.formValue[this._questionBase[j].key] = '';
          this.api.setProperty(this._questionBase[j].key, 'options', temp)
          this.api.setProperty(this._questionBase[j].key, 'value', null)
        }
      }
    }
  }
 
  reset() {
    this.router.navigate(['/main/reports/form/list']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}