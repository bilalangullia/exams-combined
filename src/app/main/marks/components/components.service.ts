import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

import { DataService } from '../../../shared/data.service';

@Injectable()
export class ComponentsService {
  private data: any = {};
  public dataChanged = new BehaviorSubject<any>(this.data);
  private typeDropdown: any;
  public typeDropdownChanged = new Subject();
  private statusDropDown: any;
  public statusDropDownChanged = new Subject();

  constructor(private dataService: DataService) {}

  setValues(data) {
    this.data = data;
    this.dataChanged.next(this.data);
  }

  public getStatusDropdown() {
    this.dataService.getStatusMarksComponent().subscribe((res: any) => {
      if (res.data.length) {
        this.statusDropDown = res.data;
        this.statusDropDownChanged.next([...this.statusDropDown]);
      }
    });
  }

  public getTypeDropdown() {
    this.dataService.getTypeMarksComponent().subscribe((res: any) => {
      if (res.data.length) {
        this.typeDropdown = res.data;
        this.typeDropdownChanged.next([...this.typeDropdown]);
      }
    });
  }
}
