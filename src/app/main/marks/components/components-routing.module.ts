import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddMarksComponent } from "./add-marks/add-marks.component";
import {ImportMarkscomponentsComponent }from './import-markscomponents/import-markscomponents.component';
import {MarkscomponentsDetailComponent }from './markscomponents-detail/markscomponents-detail.component';

const routes: Routes = [
  {
    path: "",
    children: [
      { path: "", redirectTo: "list", pathMatch: "full" },
      { path: "list", loadChildren: "./list/list.module#ListModule" },
      { path: "add", component: AddMarksComponent },
      { path: "import", component: ImportMarkscomponentsComponent },
      { path: "detail", component: MarkscomponentsDetailComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule {}
