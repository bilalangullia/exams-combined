const ROUTER_BASE_PATH: string = "/main/marks/components/list/";

interface IKdTabs {
  tabName?: string;
  tabContent?: string;
  tabId?: string;
  isActive?: boolean;
  disabled?: boolean;
  routerPath?: string;
}
export const FILTER_INPUTS: Array<any> = [
  {
    'key': 'academic_period_id',
    'visible': true,
    'required': true,
    'order': 1,
    'controlType': 'dropdown',
    'options': [{
      'key': 0,
      'value': '-- Select Options 1 --'
    }],
    'events': true
  }, {
    'key': 'examination_id',
    'visible': true,
    'required': true,
    'order': 1,
    'controlType': 'dropdown',
    'options': [{
      'key': '',
      'value': '-- Select --'
    }],
    'events': true
  }, {
    'key': 'examination_centre_id',
    'visible': true,
    'required': true,
    'order': 1,
    'controlType': 'dropdown',
    'options': [{
      'key': '',
      'value': '-- Select --'
    }],
    'events': true
  }
  , {
    'key': 'examination_options_id',
    'visible': true,
    'required': true,
    'order': 1,
    'controlType': 'dropdown',
    'options': [{
      'key': '',
      'value': '-- Select --'
    }],
    'events': true
  }
];

export const TABS_ROUTER: Array<IKdTabs> = [

  {
    tabName: 'Multiplechoice',
    routerPath: ROUTER_BASE_PATH + 'multipleChoice',
    isActive: true,
  },
  {
    tabName: 'Examination',
    routerPath: ROUTER_BASE_PATH + 'examination'
  },
  {
    tabName: 'Student Table',
    routerPath: ROUTER_BASE_PATH + 'courseWork'
  },
]

export const TABS_ROUTER1: Array<IKdTabs> = [

  {
    tabName: 'Multiplechoice',
    routerPath: ROUTER_BASE_PATH + 'multipleChoice',
    isActive: true
  },
  {
    tabName: 'Examination',
    routerPath: ROUTER_BASE_PATH + 'examination'
  },
  {
    tabName: 'Student Table',
    routerPath: ROUTER_BASE_PATH + 'courseWork'
  },
]


export const TABS_HTML: Array<IKdTabs> = [];
