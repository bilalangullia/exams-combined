import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { ComponentsRoutingModule } from './components-routing.module';
import { ComponentsComponent } from '../components/components.component';
import { AddMarksComponent } from './add-marks/add-marks.component';
import { ImportMarkscomponentsComponent } from './import-markscomponents/import-markscomponents.component';
import { MarkscomponentsDetailComponent } from './markscomponents-detail/markscomponents-detail.component';
import { ComponentsService } from './components.service';

@NgModule({
  imports: [CommonModule, ComponentsRoutingModule, SharedModule],
  declarations: [
    ComponentsComponent,
    AddMarksComponent,
    ImportMarkscomponentsComponent,
    MarkscomponentsDetailComponent
  ],
  providers: [ComponentsService]
})
export class ComponentsModule {}
