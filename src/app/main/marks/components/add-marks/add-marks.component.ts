import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { Observable, timer, Subscriber, Subscription } from 'rxjs';
import { SharedService } from '../../../../shared/shared.service';
import { DataService } from '../../../../shared/data.service';
import { QUESTION_BASE, FORM_BUTTONS } from './add-marks.config';
import { ComponentsService } from '../components.service';

@Component({
  selector: 'app-add-marks',
  templateUrl: './add-marks.component.html',
  styleUrls: ['./add-marks.component.css']
})
export class AddMarksComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public maxDate = new Date();
  public candidateData: any;
  public computedGrade: string = 'NA';
  public currentFilterSub: Subscription;
  public academic_period_id: any;
  public academic_period_value: any;
  public examination_id: any;
  public examination_value: any;
  public examination_centre_id: any;
  public examination_centre_value: any;
  public examination_options_id: any;
  public examination_options_value: any;
  public examination_component_id: any;
  public examination_component_value: any;
  public candidateId: any;
  public markersId: any;
  public grades: Array<any> = [];
  public defaultValues: any = {};
  public api: IDynamicFormApi = {};
  public countryRegion: Array<Object> = [];
  public formValue: any = {};
  private _smallLoaderValue: number = null;
  public loading: boolean = true;
  public values = {};

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public sharedService: SharedService,
    public dataService: DataService,
    private componentService: ComponentsService
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }

  ngOnInit() {
    this.loading = false;
    super.updateBreadcrumb();
    super.setPageTitle('Components', false);
    super.updatePageHeader();
    this.setDropDownValue();
    this.setOptions();
    this.currentFilterSub = this.componentService.dataChanged.subscribe((filters) => {
      if (
        filters['academic_period'] &&
        filters['academic_period']['id'] &&
        filters['examination'] &&
        filters['examination']['key'] &&
        filters['examination_options'] &&
        filters['examination_options']['key'] &&
        filters['examination_centre'] &&
        filters['examination_centre']['key'] &&
        filters['examination_component'] &&
        filters['examination_component']['key']
      ) {
        this.academic_period_id = filters['academic_period']['id'];
        this.academic_period_value = filters['academic_period']['name'];
        this.examination_id = filters['examination']['key'];
        this.examination_value = filters['examination']['value'];
        this.examination_options_id = filters['examination_options']['key'];
        this.examination_options_value = filters['examination_options']['value'];
        this.examination_centre_id = filters['examination_centre']['key'];
        this.examination_centre_value = filters['examination_centre']['value'];
        this.examination_component_id = filters['examination_component']['key'];
        this.examination_component_value = filters['examination_component']['value'];

        timer(100).subscribe(() => {
          this.setFilterValues();
        });
      } else {
        this.router.navigate(['main/marks/components/list']);
      }
    });
  }

  setOptions() {
    this._questionBase.forEach((question) => {
      if (question['key'] === 'candidate_id') {
        question['dropdownCallback'] = this.searchCandidate.bind(this);
      }
    });
  }

  searchCandidate(params: any): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data: Array<any> = [];
      if (params['query'].length) {
        this.dataService.getCandidateId(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'];
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  detectValue(event) {
    if (event['key'] === 'marks') {
      if (event['value'] != '') {
        timer(100).subscribe((): void => {
          this.calculateGrade(event.value);
        });
      }
    } else if (event['key'] === 'candidate_id' && event['value'] && event['value']['value']) {
      this.candidateId = event['value']['value'];
      timer(100).subscribe((): void => {
        this.dataService
          .getDetailsByCandidateId(
            this.academic_period_id,
            this.examination_id,
            this.examination_centre_id,
            this.examination_options_id,
            this.examination_component_id,
            event['value']['value']
          )
          .subscribe((res: any) => {
            if (
              res &&
              res['data'] &&
              res['data']['candidatemarks'] &&
              res['data']['candidatemarks'].length &&
              res['data']['grade'].length
            ) {
              this.setCandidateDetails(res['data']['candidatemarks'][0]);
              this.grades = res['data']['grade'];
              if (res && res['data'] && res['data']['markers']) {
                this.markersId = res['data']['markers']['key'] ? res['data']['markers']['key'] : '';
                this.api.setProperty(
                  'markers',
                  'value',
                  res['data']['markers'] ? res['data']['markers']['value'] : null
                );
              }
            } else {
              let toasterConfig: any = {
                type: 'error',
                title: 'No details found',
                body: 'Please select appropriate candidate',
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 3000
              };
              this.sharedService.setToaster(toasterConfig);
            }
          });
      });
    }
  }

  setFilterValues() {
    this.loading = false;
    timer(200).subscribe(() => {
      this._questionBase.forEach((question: any) => {
        if (question['key'] === 'academic_period_id') {
          this.api.setProperty(question['key'], 'value', this.academic_period_value);
        } else if (question['key'] === 'examination_id') {
          this.api.setProperty(question['key'], 'value', this.examination_value);
        } else if (question['key'] === 'examination_options') {
          this.api.setProperty(question['key'], 'value', this.examination_options_value);
        } else if (question['key'] === 'examination_centre') {
          this.api.setProperty(question['key'], 'value', this.examination_centre_value);
        } else if (question['key'] === 'examination_component') {
          let updatedValue = this.examination_component_value
            .toLowerCase()
            .split('-')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join('-');
          this.api.setProperty(question['key'], 'value', updatedValue);
        }
      });
    });
  }

  setCandidateDetails(details: any) {
    this._questionBase.forEach((question: any) => {
      if (question['key'] === 'candidate_id') {
      } else if (question['key'] === 'academic_period_id') {
      } else if (question['key'] === 'examination_id') {
      } else if (question['key'] === 'examination_options') {
      } else if (question['key'] === 'examination_centre') {
      } else if (question['key'] === 'examination_component') {
      } else {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']]
            ? details[question['key']]['key']
              ? details[question['key']]['key']
              : details[question['key']]
            : ''
        );
      }
    });
  }

  calculateGrade(marks) {
    let grade: any = null;

    this.grades.forEach((element) => {
      if (!marks) {
        return;
      }
      if (marks >= element.min && marks <= element.max) {
        grade = element.code;
      }
    });

    if (!grade) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Not a valid grade',
        body: 'Please enter proper marks',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    }
    timer(100).subscribe((): void => {
      this._updateView.setInputProperty('grade', 'value', grade ? grade : null);
    });
  }

  cancel() {
    this.router.navigate(['main/marks/components/list']);
  }

  setDropDownValue() {
    timer(100).subscribe((): void => {
      this.componentService.getStatusDropdown();
      this.componentService.statusDropDownChanged.subscribe((data: any) => {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.name
          });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('status', 'options', tempOption);
      });

      this.componentService.getTypeDropdown();
      this.componentService.typeDropdownChanged.subscribe((data: any) => {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.name
          });
        });
        tempOption.unshift({ key: null, value: '--select--' });
        this._updateView.setInputProperty('mark_type', 'options', tempOption);
      });
    });
  }

  submitVal(event): void {
    if (!event.name) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Cannot save details',
        body: 'Try different candidate Id',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
      return;
    }
    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload: any = {
        mark: event.marks,
        status_id: event.status,
        examination_component_mark_types_id: event.mark_type,
        examination_markers_id: this.markersId,
        examination_components_id: this.examination_component_id,
        candidate_id: event.candidate_id.value,
        academic_period_id: this.academic_period_id,
        examination_id: this.examination_id,
        examination_centre_id: this.examination_centre_id,
        examination_options_id: this.examination_options_id
      };

      this.dataService.addMarksComponent(payload).subscribe(
        (data: any) => {
          let toasterConfig: any = {
            type: 'success',
            title: 'Candidate Details saved successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this.router.navigate(['main/marks/components/list']);
        },
        (err) => {
          let toasterConfig: any = {
            type: 'error',
            title: 'Something Went Wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    }
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
