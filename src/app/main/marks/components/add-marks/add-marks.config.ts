export const QUESTION_BASE: Array<any> = [
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    order: 1,
    controlType: 'text',
    format: 'string',
    clickToggleDropdown: true,
    type: 'autocomplete',
    required: true,
    autocomplete: true,
    placeholder: 'Search Candidate ID',
    onEnter: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_options',
    label: 'Option',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_centre',
    label: 'Exam Center',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_component',
    label: 'Component',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'marks',
    label: 'Marks',
    visible: true,
    required: true,
    order: 1,
    controlType: 'integer',
    min: 0,
    max: 999,
    type: 'number',
    events: true
  },
  {
    key: 'grade',
    label: 'Grade',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'status',
    label: 'Status',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '--select-' }],
    events: true
  },
  {
    key: 'mark_type',
    label: 'Type',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [
      {
        key: null,
        value: '--select--'
      }
    ],
    events: true
  },
  {
    key: 'markers',
    label: 'Marker ID',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
