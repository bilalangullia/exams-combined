import { Observable, Subscriber, timer } from 'rxjs';

interface Column {
    headerName?: string;
    field?: string;
    type?: 'input' | 'normal' | 'image';
    sortable?: boolean;
    filterable?: boolean;
    visible?: boolean;
    config?: any;
    class?: string;
    filterValue?: Array<string>;
}

interface ListColumn {
    RowNumber?: Column;
    CandidateID?: Column;
    AcademicPeriod?: Column,
    ExamCode?: Column,
    CentreCode?: Column,
    Marks?: Column,
    MarkStatus?: Column,
    MarkType?: Column,
}

export const TABLECOLUMN: ListColumn = {
    RowNumber: {
        headerName: 'Row Number',
        field: 'row_number',
        sortable: true,
        filterable: false
    },
    CandidateID: {
        headerName: 'Candidate ID',
        field: 'CandidateID',
        sortable: true,
        filterable: false
    },
    AcademicPeriod: {
        headerName: 'Academic Period',
        field: 'academic_period',
        sortable: true,
        filterable: false
    },
    ExamCode: {
        headerName: 'Exam Code',
        field: 'exam_code',
        sortable: true,
        filterable: false
    },
    CentreCode: {
        headerName: 'Centre Code',
        field: 'centre_code',
        sortable: true,
        filterable: false
    },
    Marks: {
        headerName: 'Marks',
        field: 'marks',
        sortable: true,
        filterable: false
    },
    MarkStatus: {
        headerName: 'Marks Status',
        field: 'marks_status',
        sortable: true,
        filterable: false
    },
    MarkType: {
        headerName: 'Marks Type',
        field: 'marks_type',
        sortable: true,
        filterable: false
    }

};
export const CREATE_ROW: (_rowCount: number, _baseIndex?: number, data?: Array<any>) => Array<any> = (_rowCount: number, _baseIndex: number = 0, data?: Array<any>): Array<any> => {
    let row: Array<any> = [];
    console.log("data",data);
    
    for (let i: number = 0; i < data.length; i++) {
        let oneRow: any = {
            id: i,
            row_number:i+1,
            CandidateID: data[i].data['Candidate Id'],
            academic_period:data[i].data['Academic Period'],
            exam_code: data[i].data['Exam Code'],
            centre_code: data[i].data['Centre Code'],
            marks: data[i].data['Mark'],
            marks_status: data[i].data['Marks Status'],
            marks_type: data[i].data['Marks Type'],
        };

        row.push(oneRow);
    }

    return row;
};


export const CREATE_TABLE_CONFIG: (_id: string, _pagesize: number, _total: number) => any = (_id: string, _pagesize: number, _total: number): any => {
    return {
        id: _id,
        loadType: 'server',
        gridHeight: '600',
        externalFilter: false,
        paginationConfig: {
            pagesize: _pagesize,
            total: _total
        },
        click: {
            type: 'router',
            // pathMap: 'view',
            path: '/registration/view',
            callback: (): void => {
                console.log('ListNode: Demo callback used in when clicking the rowNode.');
            }
        }
    };
};

export const DUMMY_API_CALL: (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }) => Observable<any> =
    (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }): Observable<any> => {
        return new Observable((_observer: Subscriber<any>): void => {
            timer(1000).subscribe((): void => {
                _observer.next(CREATE_ROW(_params.pagesize, _params.startRow));
                _observer.complete();
            });
        });
    };

    export interface IMiniDashboardItem {
        type: string;
        icon?: string;
        label: string;
        value: number | string ;
    }
    
    export interface IMiniDashboardConfig {
        closeButtonDisabled?: boolean;
        rtl?: boolean;
    }
    
    
    export const MINI_DASHBOARD_CONFIG: IMiniDashboardConfig = {
        closeButtonDisabled: true,
        rtl: true,
    };
    
    export const MINI_DASHBOARD_DATA: Array<IMiniDashboardItem> = [
        {
            type: 'text',
            label: 'Total Rows :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Imported :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Update :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Failed :',
            value: '0'
        },
    ]