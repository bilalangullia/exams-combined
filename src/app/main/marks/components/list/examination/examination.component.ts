import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  ITableColumn,
  ITableConfig,
  ITableApi,
  KdAlertEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  KdIntTableEvent
} from 'openemis-styleguide-lib';

import { DataService } from '../../../../../shared/data.service';
import { SharedService } from '../../../../../shared/shared.service';
import { TABLECOLUMN, tempData } from '../multiplechoice/multichoice.config';

@Component({
  selector: 'app-examination',
  templateUrl: './examination.component.html',
  styleUrls: ['./examination.component.css']
})
export class ExaminationComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    }
  };

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.CandidateName,
    TABLECOLUMN.Marks,
    TABLECOLUMN.Status,
    TABLECOLUMN.Type,
    TABLECOLUMN.CreatedOn,
    TABLECOLUMN.CreatedBy
  ];

  public _tableApi: ITableApi = {};

  constructor(
    activatedRoute: ActivatedRoute,
    public _kdalert: KdAlertEvent,
    private _advFilterEvent: KdAdvFilterEvent,
    private _toolbarEvent: KdToolbarEvent,
    public _tableEvent: KdTableEvent,
    private kdtable: KdIntTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    public router: Router,
    public pageEvent: KdPageBaseEvent
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }

  ngOnInit() {
    this.getCandidateList();
    console.log('multiCoice...');
  }

  getCandidateList() {
    timer(100).subscribe(
      () => {
        this.loading = false;

        this._row = tempData;

        if (!this.loading) {
          this._row.forEach((rowdata) => {
            if (TABLECOLUMN.CandidateID.filterValue.length == 0) {
              TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
              TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
              TABLECOLUMN.Marks.filterValue.push(rowdata.marks);
              TABLECOLUMN.Status.filterValue.push(rowdata.status);
              TABLECOLUMN.Type.filterValue.push(rowdata.type);
              TABLECOLUMN.CreatedOn.filterValue.push(rowdata.created_on);
              TABLECOLUMN.CreatedBy.filterValue.push(rowdata.created_by);
            } else {
              TABLECOLUMN.CandidateID.filterValue.indexOf(rowdata.candidate_id) > -1
                ? 'na'
                : TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
              TABLECOLUMN.CandidateName.filterValue.indexOf(rowdata.candidate_name) > -1
                ? 'na'
                : TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
              TABLECOLUMN.Marks.filterValue.indexOf(rowdata.marks) > -1
                ? 'na'
                : TABLECOLUMN.Marks.filterValue.push(rowdata.marks);
              TABLECOLUMN.Status.filterValue.indexOf(rowdata.status) > -1
                ? 'na'
                : TABLECOLUMN.Status.filterValue.push(rowdata.status);
              TABLECOLUMN.Type.filterValue.indexOf(rowdata.type) > -1
                ? 'na'
                : TABLECOLUMN.Type.filterValue.push(rowdata.type);
              TABLECOLUMN.CreatedOn.filterValue.indexOf(rowdata.created_on) > -1
                ? 'na'
                : TABLECOLUMN.CreatedOn.filterValue.push(rowdata.created_on);
              TABLECOLUMN.CreatedBy.filterValue.indexOf(rowdata.created_by) > -1
                ? 'na'
                : TABLECOLUMN.CreatedBy.filterValue.push(rowdata.created_by);
            }
          });
        }
      },
      (err) => {
        this.loading = false;
        let toasterConfig: any = {
          type: 'error',
          title: 'Something went wrong',
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
