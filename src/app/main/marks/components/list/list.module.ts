import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from '../list/list.component';
import { MultiplechoiceComponent } from './multiplechoice/multiplechoice.component';
import { ExaminationComponent } from './examination/examination.component';
import { CourseworkComponent } from './coursework/coursework.component';
import { ListService } from './list.service';

@NgModule({
  imports: [CommonModule, ListRoutingModule, SharedModule],
  declarations: [ListComponent, MultiplechoiceComponent, ExaminationComponent, CourseworkComponent],
  providers: [ListService]
})
export class ListModule {}
