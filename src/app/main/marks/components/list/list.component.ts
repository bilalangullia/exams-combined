import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdTableEvent,
  KdAdvFilterEvent,
  KdIntTableEvent,
  KdFilter,
  ITableConfig,
  ITableColumn,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';

import { FILTER_INPUTS, TABS_HTML } from '../component.config';
import { timer, BehaviorSubject, Subscription } from 'rxjs';
import { TABLECOLUMN, tempData, optionData } from './multiplechoice/multichoice.config';
import { SharedService } from '../../../../shared/shared.service';
import { DataService } from '../../../../shared/data.service';
import { ExcelExportParams } from 'ag-grid';
import { ListService } from './list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class ListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;

  public inputs: Array<any> = FILTER_INPUTS;
  public tabsListHtml: Array<any>;

  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>;
  public loading: boolean = true;
  public IsTabActive: boolean = true;
  public _smallLoaderValue: number = null;

  public _tableApi: ITableApi = {};
  public tempData11: Array<any>;
  public defaultValues: any = {};
  public defautDorpVal: any = {};
  public tempTab: any = '';
  public defaultSubject: Array<any>;
  public defaultTab: Array<any>;
  public defaultList: any = {};
  public count: any = 0;
  public itemFind = {};
  // public selectedValues = {};
  // public selectedValuesChanged = new BehaviorSubject<any>(this.selectedValues);

  private currentFiltersSub: Subscription;
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    }
  };

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.CandidateName,
    TABLECOLUMN.Marks,
    TABLECOLUMN.Status,
    TABLECOLUMN.Type,
    TABLECOLUMN.CreatedOn,
    TABLECOLUMN.CreatedBy
  ];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    private _toolbarEvent: KdToolbarEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    public _KdAlert: KdAlertEvent,
    private listService: ListService
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }
  ngOnInit() {
    this.listService.emptyCurrentFilters();
    super.setPageTitle('Components', false);
    super.setToolbarMainBtns([
      {
        type: 'add',
        // path: 'main/marks/components/add',
        callback: () => {
          if (!Object.keys(this.defaultList).length) {
            let toasterConfig: any = {
              type: 'error',
              title: 'Missing Component Id',
              body: 'Select Active tab from list.',
              showCloseButton: true,
              tapToDismiss: true,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);
          } else {
            this.router.navigate(['main/marks/components/add']);
          }
        }
      },
      {
        type: 'import',
        callback: (): void => {
          this.router.navigate(['/main/marks/components/import']);
        }
      },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'marks_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);

    super.updatePageHeader();
    super.updateBreadcrumb();

    this.setDropDownValue();
  }

  public detectValue(question: any): void {
    this.defautDorpVal[question.key] = question.value;

    // this.selectedValues[question.key] = question.options.find((item) => item['key'] == question.value);

    if (question.key == 'academic_period_id') {
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.listService.setCurrentFilters('academic_period', {
          ...this.itemFind
        });
      } else {
        this.listService.setCurrentFilters('academic_period', {});
      }

      this.resetDropDown();
      this.setExamination(question.value);
    } else if (question.key == 'examination_id') {
      // this.selectedValues["examination_id"] = question.options.find(
      //   item => item["key"] == question.value
      // );
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.listService.setCurrentFilters('examination', {
          ...this.itemFind
        });
      } else {
        this.listService.setCurrentFilters('examination', {});
      }

      let temp = Object.values(this.defautDorpVal).findIndex((item) => {
        return item == '';
      });

      temp == 1 ? 'NA' : this.subjectOption(this.defautDorpVal[question.key]);
    } else if (question.key == 'examination_options_id') {
      // this.selectedValues["examination_options_id"] = question.options.find(
      //   item => item["key"] == question.value
      // );

      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.listService.setCurrentFilters('examination_options', {
          ...this.itemFind
        });
      } else {
        this.listService.setCurrentFilters('examination_options', {});
      }
      this.count = 0;
      this.setComponetList(this.defautDorpVal);
    } else if (question.key == 'examination_centre_id') {
      // this.selectedValues["examination_options_id"] = question.options.find(
      //   item => item["key"] == question.value
      // );

      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.listService.setCurrentFilters('examination_centre', {
          ...this.itemFind
        });
      } else {
        this.listService.setCurrentFilters('examination_centre', {});
      }
      this.count = 0;
      this.setComponetList(this.defautDorpVal);
    } else {
      // this.selectedValues["examination_centre_id"] = question.options.find(
      //   item => item["key"] == question.value
      // );

      this.setComponetList(this.defautDorpVal);
    }
  }

  selectedTab(event: any) {
    let eventValue = event.replace(/[_]+/g, '');

    this.listService.setCurrentFilters('examination_component', {
      key: this.defaultList[eventValue][0].components_id,
      value: eventValue
    });

    this.count++;
    if (this.count == 2) {
      this.defautDorpVal.tabOption = event;
      if (event) {
        /*    this.selectedValues['activeTab'] = {
          key: 1,
          value: event
        }; */
        // localStorage.setItem('selectedValues', JSON.stringify(this.selectedValues));
        // console.log('line158', this.selectedValues);
      } else {
        // this.selectedValues = {};
      }

      if (this.defaultList[eventValue].length != undefined || this.defaultList[eventValue] != undefined) {
        let temRow = [];
        this.defaultList[eventValue].forEach((item) => {
          temRow.push(item);
        });
        this.loading = true;
        this._row = temRow;
        this.setCandidateListFilter(this._row);
      }
    } else if (this.count > 2) {
      this.defautDorpVal.tabOption = event;
      if (event) {
        // this.selectedValues['activeTab'] = {
        //   key: 1,
        //   value: event
        // };
        // localStorage.setItem('selectedValues', JSON.stringify(this.selectedValues));
        // console.log('line158', this.selectedValues);
      } else {
        // this.selectedValues = {};
      }
      if (this.defaultList[eventValue] != undefined && this.defaultList[eventValue].length > 0) {
        let temRow = [];
        this.defaultList[eventValue].forEach((item) => {
          temRow.push(item);
        });
        this.loading = true;
        this._row = temRow;
        this.setCandidateListFilter(this._row);
      }
    }
  }

  setDropDownValue() {
    timer(100).subscribe(() => {
      this.sharedService.getDropdownValues().subscribe((data: any) => {
        this.defaultValues = data;
        this.defautDorpVal = {
          academic_period_id: data.data.academic_period_id[0].id
        };

        // this.selectedValues['academic_period_id'] = {
        //   key: data.data.academic_period_id[0].id,
        //   value: data.data.academic_period_id[0].name
        // };
        this.listService.setCurrentFilters('academic_period', data.data.academic_period_id[0]);

        if (this.defaultValues) {
          let keyArr = Object.keys(this.defaultValues.data);
          keyArr.forEach((e) => {
            let quesObj = this.inputs.find((q) => {
              return q.key == e;
            });
            if (quesObj) {
              let quesObj1 = [];
              quesObj1.push(
                ...this.defaultValues.data[e].map((item) => {
                  return { key: item.id, value: item.name };
                })
              );
              this._updateView.setInputProperty('academic_period_id', 'options', quesObj1);
            }
          });
          this.setExamination(this.defautDorpVal.academic_period_id);
        }
      });
    });
  }

  setCandidateListFilter(data: any) {
    this.resetFilter();
    if (data.length > 0) {
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (data[j][this.tableColumns[i]['field']] && data[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(data[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(data[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
      this.loading = false;
    } else {
      this.loading = false;
      let toasterConfig: any = {
        type: 'error',
        title: 'No records found for selected tab',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this._KdAlert.error(toasterConfig);
    }
  }

  setTab(data: any) {
    // console.log('data..setTab', data);

    TABS_HTML.length = 0;
    this.IsTabActive = true;
    timer(100).subscribe(() => {
      data.forEach((item) => {
        TABS_HTML.length == 0
          ? TABS_HTML.push({
              tabName: item,
              isActive: true
            })
          : TABS_HTML.push({ tabName: item });
      });
      this.tabsListHtml = TABS_HTML;
      this.IsTabActive = false;
    });
  }

  setExamination(data: any) {
    this.dataService.getExamination(data).subscribe((res: any) => {
      if (res.data.length) {
        let examTemp = [];

        this.defautDorpVal.examination_id = res.data[0].id;

        res.data.forEach((item) => {
          examTemp.push({ key: item.id, value: item.name });
        });
        this._updateView.setInputProperty('examination_id', 'options', examTemp);
        this.listService.setCurrentFilters('examination', examTemp[0]);
        this.subjectOption(res.data[0].id);
      } else {
        this._updateView.setInputProperty('examination_id', 'options', [{ key: '', value: '-- Select --' }]);
        this._updateView.setInputProperty('examination_centre_id', 'options', [{ key: '', value: '-- Select --' }]);
        this._updateView.setInputProperty('examination_options_id', 'options', [{ key: '', value: '-- Select --' }]);

        this.IsTabActive = true;
        let toasterConfig: any = {
          type: 'error',
          title: 'No records found',
          body: 'Please select another accedamic periods',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this._KdAlert.error(toasterConfig);
        /*   this.selectedValues['examination_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['examination_centre_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['examination_options_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['activeTab'] = {
          key: '',
          value: ''
        }; */
        // localStorage.removeItem('selectedValues');
      }
    });
  }

  setExamCenter(data: any) {
    this.dataService.getOptionsExamCenter(data.area_id).subscribe((res: any) => {
      if (res.data.length) {
        let tempCenter = [];

        this.defautDorpVal.examination_centre_id = res.data[0].id;
        res.data.forEach((item) => {
          tempCenter.push({ key: item.id, value: item.code + '- ' + item.name });
        });
        this._updateView.setInputProperty('examination_centre_id', 'options', tempCenter);
        this.listService.setCurrentFilters('examination_centre', tempCenter[0]);
        this.setComponetList(this.defautDorpVal);
      } else {
        this.inputs.forEach((item) => {
          if (item.key == 'examination_centre_id') {
            item.value = '';
          }
        });
        this._updateView.setInputProperty('examination_centre_id', 'options', [{ key: '', value: '-- Select --' }]);
        this.IsTabActive = true;
        let toasterConfig: any = {
          type: 'error',
          title: 'No exam center found',
          body: 'Please select another academic periods',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this._KdAlert.error(toasterConfig);
        /*  this.selectedValues['examination_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['academic_period_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['examination_options_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['activeTab'] = {
          key: '',
          value: ''
        }; */
        // localStorage.removeItem('selectedValues');
      }
    });
  }

  subjectOption(data: any) {
    this.dataService.subjectOption(data).subscribe((data: any) => {
      if (data.data.length) {
        let tempOption = [];
        this.defaultSubject = data.data;
        this.defautDorpVal.examination_options_id = data.data[0].id;

        data.data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.code + ' - ' + item.name });
        });

        this._updateView.setInputProperty('examination_options_id', 'options', tempOption);
        let examCentmp = {
          area_id: this.defautDorpVal.examination_id,
          academic_period_id: this.defautDorpVal.academic_period_id
        };
        this.listService.setCurrentFilters('examination_options', tempOption[0]);
        this.setExamCenter(examCentmp);
      } else {
        this.inputs.forEach((item) => {
          if (item.key == 'examination_options_id') {
            item.value = '';
          }
        });

        this._updateView.setInputProperty('examination_options_id', 'options', [{ key: '', value: '-- Select --' }]);
        this._updateView.setInputProperty('examination_centre_id', 'options', [{ key: '', value: '-- Select --' }]);
        this._updateView.setInputProperty('examination_options_id', 'value', '');
        this._updateView.setInputProperty('examination_centre_id', 'value', '');

        this.IsTabActive = true;
        let toasterConfig: any = {
          type: 'error',
          title: 'No subject found',
          body: 'Please select another Examination certificated',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 3000
        };
        this._KdAlert.error(toasterConfig);
        /*  this.selectedValues['examination_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['academic_period_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['examination_centre_id'] = {
          key: '',
          value: ''
        };
        this.selectedValues['activeTab'] = {
          key: '',
          value: ''
        }; */
        // localStorage.removeItem('selectedValues');
      }
    });
  }

  setComponetList(data: any) {
    let isEmpty = Object.values(data).findIndex((item) => {
      return item == '';
    });
    timer(100).subscribe(() => {
      if (isEmpty == -1) {
        this.dataService.getComponentList(data).subscribe(
          (res: any) => {
            if (res.data) {
              this.defaultTab = [];
              Object.keys(res.data).forEach((item) => {
                this.defaultTab.push(item);
              });
              this.setTab(this.defaultTab);
              this.defaultList = res.data;
            } else {
              // localStorage.removeItem('selectedValues');
              this._row = res.data;
              this.defaultList = res.data;
              this.IsTabActive = true;
              let toasterConfig: any = {
                type: 'error',
                title: 'No record found',
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 3000
              };
              this._KdAlert.error(toasterConfig);
            }
          },
          (err) => {
            // localStorage.removeItem('selectedValues');
            this.IsTabActive = true;
            let toasterConfig: any = {
              type: 'error',
              title: 'No record found',
              showCloseButton: true,
              tapToDismiss: true,
              timeout: 3000
            };
            this._KdAlert.error(toasterConfig);
          }
        );
      }
    });
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }
  resetDropDown() {
    for (let i = 0; i < this.inputs.length; i++) {
      if (this.inputs[i].key != 'academic_period_id') {
        this._updateView.setInputProperty(this.inputs[i].key, 'value', '');
      }
    }
  }
  setComponentTab() {
    this.dataService.marksComponents().subscribe((data: any) => {
      this.defaultTab = data.data;
    });
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
