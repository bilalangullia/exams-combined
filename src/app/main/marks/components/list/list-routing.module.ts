import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ListComponent } from "./list.component";
import { MultiplechoiceComponent } from "./multiplechoice/multiplechoice.component";
import { ExaminationComponent } from "./examination/examination.component";
import { CourseworkComponent } from "./coursework/coursework.component";

const routes: Routes = [
  {
    path: "",
    component: ListComponent,
    children: [
      { path: "", redirectTo: "", pathMatch: "full" },
      { path: "multipleChoice", component: MultiplechoiceComponent },
      { path: "examination", component: ExaminationComponent },
      { path: "courseWork", component: CourseworkComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListRoutingModule {}
