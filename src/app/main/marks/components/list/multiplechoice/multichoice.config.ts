interface Column {
    headerName?: string;
    field?: string;
    type?: 'input' | 'normal' | 'image';
    sortable?: boolean;
    filterable?: boolean;
    visible?: boolean;
    config?: any;
    class?: string;
    filterValue?: Array<string>;

}

interface ListColumn {
   
    CandidateID?: Column;
    CandidateName?:Column;
    Marks?:Column;
    Status?:Column;
    Type?:Column;
    CreatedOn?:Column;
    CreatedBy?:Column
}

export const TABLECOLUMN: ListColumn = {
   
    CandidateID: {
        headerName: 'Candidate ID',
        field: 'candidate_id',
        sortable: true,
        filterable: true,
        filterValue: []
    },
  
   
    CandidateName: {
        headerName: 'Candidate Name',
        field: 'name',
        sortable: true,
        filterable: true,
        filterValue: []
    },
    Marks: {
        headerName: 'Marks',
        field: 'mark',
        sortable: true,
        filterable: true,
        filterValue: []
      
    },
    Status: {
        headerName: 'Status',
        field: 'status',
        sortable: true,
        filterable: true,
        filterValue: []
      
    },  Type: {
        headerName: 'Type',
        field: 'mark_type',
        sortable: true,
        filterable: true,
        filterValue: []

    },  CreatedOn: {
        headerName: 'Created On',
        field: 'created_on',
        sortable: true,
        filterable: true,
        filterValue: []

      
    },  CreatedBy: {
        headerName: 'Created By',
        field: 'Created_by',
        sortable: true,
        filterable: true,
        filterValue: []
      
    }
};

export var tempData=[{
    candidate_id:'19OE090010',
    candidate_name:'Candidate Name',
    marks:'31.00',
    status:'Valid',
    type:'Primary',
    created_on:'October 30, 2019 15:38:01',
    created_by:'Administrator'
},{
    candidate_id:'19OE090012',
    candidate_name:'Candidate Name',
    marks:'31.00',
    status:'Valid',
    type:'Primary',
    created_on:'October 30, 2019 15:38:01',
    created_by:'Administrator'
},{
    candidate_id:'19OE090013',
    candidate_name:'Candidate Name',
    marks:'31.00',
    status:'Valid',
    type:'Primary',
    created_on:'October 30, 2019 15:38:01',
    created_by:'Administrator'
},{
    candidate_id:'19OE090014',
    candidate_name:'Candidate Name',
    marks:'31.00',
    status:'Valid',
    type:'Primary',
    created_on:'October 30, 2019 15:38:01',
    created_by:'Administrator'
}]



export const  componentKey='';

export const optionData=[
    {
        "id": 1,
        "code": "6143A",
        "name": "Accounting"
    },
    {
        "id": 2,
        "code": "6108A",
        "name": "Afrikaans as a Second Language"
    },
    {
        "id": 3,
        "code": "6115A",
        "name": "Agricultural Science"
    },
    {
        "id": 4,
        "code": "6149A",
        "name": "Art and Design"
    },
    {
        "id": 5,
        "code": "6149B",
        "name": "Art and Design"
    },
    {
        "id": 6,
        "code": "6116A",
        "name": "Biology"
    },
    {
        "id": 7,
        "code": "6186A",
        "name": "Building Studies"
    },
    {
        "id": 8,
        "code": "6144A",
        "name": "Business Studies"
    },
    {
        "id": 9,
        "code": "6117A",
        "name": "Chemistry "
    },
    {
        "id": 10,
        "code": "6134A",
        "name": "Computer Studies"
    },
    {
        "id": 11,
        "code": "6187A",
        "name": "Design Technology"
    },
    {
        "id": 12,
        "code": "6136A",
        "name": "Development Studies"
    },
    {
        "id": 13,
        "code": "6145A",
        "name": "Economics"
    },
    {
        "id": 14,
        "code": "6109A",
        "name": "English as a Second Language"
    },
    {
        "id": 15,
        "code": "6146A",
        "name": "Entrepreneurship"
    },
    {
        "id": 16,
        "code": "6153A",
        "name": "Fashion and Fabrics"
    },
    {
        "id": 17,
        "code": "6094A",
        "name": "First Language Afrikaans"
    },
    {
        "id": 18,
        "code": "6095A",
        "name": "First Language English"
    },
    {
        "id": 19,
        "code": "6096A",
        "name": "First Language German"
    },
    {
        "id": 20,
        "code": "6098A",
        "name": "First Language Khoekhoegowab"
    },
    {
        "id": 21,
        "code": "6099A",
        "name": "First Language Oshikwanyama"
    },
    {
        "id": 22,
        "code": "6100A",
        "name": "First Language Oshindonga"
    },
    {
        "id": 23,
        "code": "6101A",
        "name": "First Language Otjiherero"
    },
    {
        "id": 24,
        "code": "6102A",
        "name": "First Language Rukwangali"
    },
    {
        "id": 25,
        "code": "6103A",
        "name": "First Language Rumanyo"
    },
    {
        "id": 26,
        "code": "6104A",
        "name": "First Language Setswana"
    },
    {
        "id": 27,
        "code": "6105A",
        "name": "First Language Silozi"
    },
    {
        "id": 28,
        "code": "6106A",
        "name": "First Language Thimbukushu"
    },
    {
        "id": 29,
        "code": "6111A",
        "name": "Foreign Language French"
    },
    {
        "id": 30,
        "code": "6112A",
        "name": "Foreign Language German"
    },
    {
        "id": 31,
        "code": "6113A",
        "name": "Foreign Language Portuguese"
    },
    {
        "id": 32,
        "code": "6137A",
        "name": "Geography"
    },
    {
        "id": 33,
        "code": "6154A",
        "name": "Health and Social Care"
    },
    {
        "id": 34,
        "code": "6138A",
        "name": "History"
    },
    {
        "id": 35,
        "code": "6155A",
        "name": "Home Economics"
    },
    {
        "id": 36,
        "code": "6156A",
        "name": "Hospitality"
    },
    {
        "id": 37,
        "code": "6150A",
        "name": "Integrated Performing Arts"
    },
    {
        "id": 38,
        "code": "6131A",
        "name": "Mathematics"
    },
    {
        "id": 39,
        "code": "6188A",
        "name": "Metalwork and Welding"
    },
    {
        "id": 40,
        "code": "6189A",
        "name": "Motor Mechanics"
    },
    {
        "id": 41,
        "code": "6107A",
        "name": "Namibia Sign Language"
    },
    {
        "id": 42,
        "code": "6157A",
        "name": "Office Practice"
    },
    {
        "id": 43,
        "code": "6118A",
        "name": "Physics"
    },
    {
        "id": 44,
        "code": "6190A",
        "name": "Woodwork"
    }
  ]