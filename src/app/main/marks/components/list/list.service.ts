import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

import { ComponentsService } from '../components.service';

@Injectable()
export class ListService {
  private currentFilters: any = {};
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);

  constructor(private componentService: ComponentsService) {}
  /*** SET Current filters ***/
  setCurrentFilters(key: string, value: Object) {
    this.currentFilters[key] = value;
    this.currentFiltersChanged.next({ ...this.currentFilters });
    this.componentService.setValues({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  emptyCurrentFilters() {
    this.currentFilters = {};
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }
}
