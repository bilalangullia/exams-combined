import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarksMainComponent } from './marks-main.component';
import { AuthGuard } from '../../shared/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MarksMainComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'components', pathMatch: 'full' },
      {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
      },
      {
        path: 'multiple-choice',
        loadChildren: './multiple-choice/multiple-choice.module#MultipleChoiceModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarksRoutingModule {}
