import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarksRoutingModule } from './marks-routing.module';
import { MarksMainComponent } from './marks-main.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [CommonModule, MarksRoutingModule, SharedModule],
  declarations: [MarksMainComponent]
})
export class MarksModule {}
