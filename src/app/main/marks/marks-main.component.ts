import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IPageheaderApi, IPageheaderConfig } from 'openemis-styleguide-lib';

@Component({
  selector: 'app-marks-main',
  templateUrl: './marks-main.component.html',
  styleUrls: ['./marks-main.component.css']
})
export class MarksMainComponent extends KdPageBase implements OnInit, OnDestroy {
  public pageTitle = 'Reports-Registrations';
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = {
    home: { name: 'Home', path: '/' }
  };

  constructor(public pageEvent: KdPageBaseEvent, router: Router, activatedRoute: ActivatedRoute) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }
  ngOnInit() {}
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
