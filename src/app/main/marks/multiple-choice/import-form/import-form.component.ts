import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdAlertEvent } from 'openemis-styleguide-lib';
import { fileInputs, button } from './config';

import { DataService } from '../../../../shared/data.service';
import { SharedService } from '../../../../shared/shared.service';
import { ExcelService } from '../../../../shared/excel.service';

@Component({
  selector: 'app-import-form',
  templateUrl: './import-form.component.html',
  styleUrls: ['./import-form.component.css']
})
export class ImportFormComponent extends KdPageBase implements OnInit, OnDestroy {
  public fileinput: Array<any> = fileInputs;
  public api: IDynamicFormApi = {};
  public formValue: any = {};
  public defaultValues: any;
  public loading = true;
  public button: Array<any> = button;
  constructor(
    _pageEvent: KdPageBaseEvent,
    public _router: Router,
    _activatedRoute: ActivatedRoute,
    public dataService: DataService,
    public sharedService: SharedService,
    public excelSvc: ExcelService,
    public _kdalert: KdAlertEvent
  ) {
    super({
      router: _router,
      pageEvent: _pageEvent,
      activatedRoute: _activatedRoute
    });
  }

  ngOnInit() {
    super.setPageTitle('Multiple Choices ­ Import', false);
    super.setToolbarMainBtns([]);

    super.updatePageHeader();
    super.updateBreadcrumb();
    this.setImportDropDown();
  }

  // generateImportTemplate(){
  //   let dataColumnHeadings = ['Candidate ID', 'Question Num', 'Response'];
  //   let referenceNames = ['Question', 'Response'];
  //   let referenceData = {'question': [{id: 1, name: 'Question 1'},{id: 2, name: 'Question 2'}],'response':[{id: 1, name: 'A'},{id: 2, name: 'B'}]};
  //   this.excelSvc.init('Z data', dataColumnHeadings, referenceNames, referenceData);
  // }

  setImportDropDown() {
    // fileInputs[fileInputs.length-1]['config']['leftButton'][0].callback = this.generateImportTemplate.bind(this);
    this.fileinput = fileInputs;

    this.sharedService.getDropdownValues().subscribe((data: any) => {
      this.defaultValues = data;
      if (this.defaultValues) {
        let keyArr = Object.keys(this.defaultValues.data);
        keyArr.forEach((e) => {
          let quesObj = this.fileinput.find((q) => {
            return q.key == e;
          });
          if (quesObj != undefined) {
            quesObj.options.length = 0;
            quesObj.options.push(
              ...this.defaultValues.data[e].map((item) => {
                return { key: item.id, value: item.name };
              })
            );
            this.formValue.academic_period_id = quesObj.options[0].key;
            
            this.getExamination(this.formValue.academic_period_id)
          }
        });
      }
      this.loading = false;
    });
  }

  public detectValue(question: any): void {
    this.formValue[question.key] = question.value;
    if (question.key == 'academic_period_id') {
      this.getExamination(question.value);
    } else if (question.key == 'examination_id') {
      this.getCentre(question.value);
      this.getOption(question.value);
    } else if (question.key == 'option_id') {
      this.getComponent(question.value);
    }
  }

  getExamination(id) {
    if (id != 'null') {
      this.dataService.getExamination(id).subscribe(
        (data: any) => {
       if(data.data.length>0){
        let temp = [
          ...data.data.map((item) => {
            return { key: item.id, value: item.name };
          })
        ];
        this.api.setProperty('examination_id', 'options', temp);
        this.formValue.examination_id = data.data[0].id;
          this.getCentre( this.formValue.examination_id)
          this.getOption(this.formValue.examination_id);
       }else{
        setTimeout(() => {
          let value = ['examination_id','examination_centre_id','option_id','component_id']
          this.resetDropdown(value);
        }, 1000);
        let toasterConfig: any = {
          type: 'error',
          title: 'Examination not found for selected accademic year',
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
       }
       }  ,(err) => {
          console.log(err);
          
        setTimeout(() => {
          let value = ['examination_id','examination_centre_id','option_id','component_id']
          this.resetDropdown(value);
        }, 1000);
        }
      );
    } else {
      setTimeout(() => {
        let value = ['examination_id','examination_centre_id','option_id','component_id']
        this.resetDropdown(value);
      }, 1000);
    }
  }

  getCentre(id) {
    if (id != 'null' && id != null) {
      this.dataService.getMarkImportCentreList(id).subscribe(
        (data: any) => {
          if(data.data.length>0){
            let temp = [
              ...data.data.map((item) => {
                return { key: item.key, value: item.value };
              })
            ];
            this.api.setProperty('examination_centre_id', 'options', temp);
            this.formValue.examination_centre_id = data.data[0].key;
          }else{

            setTimeout(() => {
              let value = ['examination_centre_id']
              this.resetDropdown(value);
            }, 1000);
            let toasterConfig: any = {
              type: 'error',
              title: 'Centre not found for selected examination',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        },
        (err) => {
          console.log(err);
          setTimeout(() => {
            let value = ['examination_centre_id']
            this.resetDropdown(value);
          }, 1000);
        }
      );
    } 
    else {
      setTimeout(() => {
        let value = ['examination_centre_id']
        this.resetDropdown(value);
      }, 1000);
     }
  }

  getOption(id) {
    if (id != 'null' && id != null) {
      this.dataService.subjectOption(id).subscribe(
        (data: any) => {
          if(data.data.length>0){
            this.formValue.option_id = data.data[0].id;
            let temp = [
              ...data.data.map((item) => {
                return { key: item.id, value: item.name };
              })
            ];
            this.api.setProperty('option_id', 'options', temp);
            this.getComponent(this.formValue.option_id);
          }else{
            setTimeout(() => {
              let value = ['component_id','option_id']
              this.resetDropdown(value);
            }, 1000);
            let toasterConfig: any = {
              type: 'error',
              title: 'Option not found for selected examination',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);
          }
          
        },
        (err) => {
          setTimeout(() => {
            let value = ['component_id','option_id']
            this.resetDropdown(value);
          }, 1000);
        }
      );
    } else {
      setTimeout(() => {
        let value = ['component_id','option_id']
        this.resetDropdown(value);
      }, 1000);
     }
  }

  getComponent(id) {
    if (id != 'null') {
      this.dataService.getMarkImportComponentList(id).subscribe(
        (data: any) => {
          if(data.data.length>0){
            let temp = [
              ...data.data.map((item) => {
                return { key: item.id, value: item.name };
              })
            ];
            this.api.setProperty('component_id', 'options', temp);
            this.formValue.component_id = data.data[0].id;
          }else{
            setTimeout(() => {
              let value = ['component_id']
              this.resetDropdown(value);
            }, 1000);
            let toasterConfig: any = {
              type: 'error',
              title: 'Component not found for selected option',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);
          }
       
        },
        (err) => {
          setTimeout(() => {
            let value = ['component_id']
            this.resetDropdown(value);
          }, 1000);
        }
      );
    } else {
      setTimeout(() => {
        let value = ['component_id']
        this.resetDropdown(value);
      }, 1000);
    }
  }

  _buttonEvent(event: any) {
    if(this.requiredCheck()){
      const formData = new FormData();
      formData.append('academic_period_id', this.formValue.academic_period_id);
      formData.append('examination_id', this.formValue.examination_id);
      formData.append('examination_centre_id', this.formValue.examination_centre_id);
      formData.append('option_id', this.formValue.option_id);
      formData.append('component_id', this.formValue.component_id);
      formData.append('template', event.fileinput_double_buttons);
      this.dataService.importMarkList(formData).subscribe((res) => {
        this.sharedService.setMarkDataList(res);
        this._router.navigate(['/main/marks/multiple-choice/detail']);
      });
    }else{
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
    }

    
  }

  requiredCheck() {
    let hasError: boolean;
    for (let i = 0; i < this.fileinput.length; i++) {
      if (this.fileinput[i]['required']) {
        if (
          !this.formValue[this.fileinput[i]['key']] ||
          this.formValue[this.fileinput[i]['key']] == '' ||
          this.formValue[this.fileinput[i]['key']] == {} ||
          this.formValue[this.fileinput[i]['key']] == 'null'
        ) {
          
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this.fileinput[i]['key'], 'errors', ['This field is required']);
          }, 1000);
          break;
        }
      }
    }
    if (hasError) {
      return false;
    } else {
      return true;
    }
  }

  resetDropdown(data) {
        
    let temp =  [{ key: null, value: '--Select--' }];
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < this.fileinput.length; j++) {
        if (this.fileinput[j].key == data[i]) {
          this.formValue[this.fileinput[j].key] = '';
          this.api.setProperty(this.fileinput[j].key, 'options', temp)
          this.api.setProperty(this.fileinput[j].key, 'value', null)
        }
      }
    }
  }


  
  reset(){
    this._router.navigate(['/main/marks/multiple-choice/list']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

}
