import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject, timer } from 'rxjs';

import { DataService } from '../../../shared/data.service';
import { SharedService } from '../../../shared/shared.service';
import { saveSuccess, saveFail, serverError } from '../../../shared/shared.toasters';
import { MultipleChoiceDataService } from './multiple-choice-data.service';

@Injectable()
export class MultipleChoiceService {
  private filterYear: any;
  private filterExam: any;
  private filterExamCenter: any;
  private filterSubject: any;
  private filterComponent: any;
  private list: any;
  private selectedCandidateId: string = null;
  private currentCandidateDetails: any = null;
  private currentFilters: any = null;
  private generateMarks: any = null;

  public filterYearChanged = new Subject();
  public filterExamChanged = new Subject();
  public filterExamCenterChanged = new Subject();
  public filterSubjectChanged = new Subject();
  public filterComponentChanged = new Subject();
  public listChanged = new Subject();
  public selectedCandidateIdChanged = new BehaviorSubject<string>(this.selectedCandidateId);
  public currentCandidateDetailsChanged = new BehaviorSubject<any>({ ...this.currentCandidateDetails });
  public currentFiltersChanged = new BehaviorSubject<any>({ ...this.currentFilters });

  constructor(
    private router: Router,
    private dataService: DataService,
    private sharedService: SharedService,
    private multipleChoiceDataService: MultipleChoiceDataService
  ) {}

  /*** Set Candidate ID when navigating away from list page -> view/edit pages ***/
  setCandidateId(_rowData: any) {
    if (_rowData && _rowData.candidate_id) {
      this.sharedService.setCandidateId(_rowData.candidate_id);
      this.selectedCandidateId = this.sharedService.getCandidateId();
      this.selectedCandidateIdChanged.next(this.selectedCandidateId);
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Candidate NotFound',
        body: 'Candidate Id not found please select valid candidate.',
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
      setTimeout(() => {
        this.router.navigate['main/marks/multiple-choice/list'];
      }, 500);
    }
  }

  /*** LIST PAGE | FILTERS | Year***/
  getFilterYear() {
    let data = 'academicPeriods';
    this.dataService.getOptionsAcademicPeriod(data).subscribe((res: any) => {
      if (res && res.data && res.data.academic_period_id) {
        this.filterYear = res.data.academic_period_id;
        this.filterYearChanged.next([...this.filterYear]);
      } else {
        this.filterYear = [];
        this.filterYearChanged.next([...this.filterYear]);
      }
      (err) => {
        this.filterYear = [];
        this.filterYearChanged.next([...this.filterYear]);
      };
    });
  }

  /*** LIST PAGE | FILTERS | Exam Cert ***/
  getFilterExam(academic_id) {
    this.dataService.getOptionsExam(academic_id).subscribe(
      (res: any) => {
        if (res && res.data) {
          this.filterExam = res.data;
          this.filterExamChanged.next([...this.filterExam]);
        } else {
          this.filterExam = [];
          this.filterExamChanged.next([...this.filterExam]);
        }
      },
      (err) => {
        this.filterExam = [];
        this.filterExamChanged.next([...this.filterExam]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Exam Center ***/
  getFilterExamCenter(examination_id) {
    this.dataService.getOptionsExamCenter(examination_id).subscribe(
      (res: any) => {
        this.filterExamCenter = res.data;
        this.filterExamCenterChanged.next([...this.filterExamCenter]);
      },
      (err) => {
        this.filterExamCenter = [];
        this.filterExamCenterChanged.next([...this.filterExamCenter]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Subjects ***/
  getFilterSubject(examination_id) {
    this.multipleChoiceDataService.getOptionSubject(examination_id).subscribe(
      (data: any) => {
        this.filterSubject = data.data;
        this.filterSubjectChanged.next([...this.filterSubject]);
      },
      (err) => {
        this.filterSubject = [];
        this.filterSubjectChanged.next([...this.filterSubject]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Components ***/
  getFilterComponent(subject_id) {
    this.multipleChoiceDataService.getOptionComponent(subject_id).subscribe(
      (data: any) => {
        this.filterComponent = data.data;
        this.filterComponentChanged.next([...this.filterComponent]);
      },
      (err) => {
        this.filterComponent = [];
        this.filterComponentChanged.next([...this.filterComponent]);
      }
    );
  }

  /*** LIST PAGE | List Data ***/
  getMultiChoiceList(academic_period_id, examination_id, examination_center_id, options_id, component_id) {
    this.multipleChoiceDataService
      .getList(academic_period_id, examination_id, examination_center_id, options_id, component_id)
      .subscribe(
        (res: any) => {
          if (res.data.length) {
            this.list = res.data;
            this.listChanged.next([...this.list]);
          } else {
            this.list = [];
            this.listChanged.next([...this.list]);
          }
        },
        (err) => {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      );
  }

  /*** List Data | SEARCH***/
  searchMultipleChoiceList(
    academic_period_id,
    examination_id,
    examination_center_id,
    options_id,
    component_id,
    keyword
  ) {
    this.multipleChoiceDataService
      .getListSearch(academic_period_id, examination_id, examination_center_id, options_id, component_id, keyword)
      .subscribe(
        (data) => {
          this.list = data['data'];
          this.listChanged.next([...this.list]);
        },
        (err) => {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      );
  }

  /*** List page | Generating Marks. ***/
  generateMultiChoiceMarks(candidate_id, compId) {
    this.multipleChoiceDataService.generateMarks(candidate_id, compId).subscribe(
      (res: any) => {
        if (res.data) {
          this.generateMarks = res.data;
          let toasterConfig: any = {
            type: 'success',
            title: 'Marks generated successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      },
      (err) => {
        if (err) {
          this.generateMarks = {};
          let toasterConfig: any = {
            type: 'error',
            title: 'Cannot generate Marks',
            body: 'Please try again',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      }
    );
  }

  /*** List page | Generating Marks. ***/
  generateAllMultipleChoiceMarks(examination_centre_id, examination_id, academic_period_id, options_id, compId) {
    this.multipleChoiceDataService
      .generateAllMarks(examination_centre_id, examination_id, academic_period_id, options_id, compId)
      .subscribe(
        (res: any) => {
          if (res.data) {
            this.generateMarks = res.data;
            let toasterConfig: any = {
              type: 'success',
              title: 'Marks generated successfully',
              showCloseButton: true,
              tapToDismiss: true,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        },
        (err) => {
          if (err) {
            this.generateMarks = {};
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot generate Marks',
              body: 'Please try again',
              showCloseButton: true,
              tapToDismiss: true,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      );
  }
  /*** VIEW/EDIT PAGE | Get candidate details for populating the edit fields. ***/
  getCandidateDetails(id, componentId) {
    this.multipleChoiceDataService.getCandidateDetails(id, componentId).subscribe(
      (res: any) => {
        if (res && res['data']) {
          this.currentCandidateDetails = res['data'][0];
          this.currentCandidateDetailsChanged.next({ ...this.currentCandidateDetails });
        }
      },
      (err) => {
        this.currentCandidateDetails = null;
        this.currentCandidateDetailsChanged.next(null);
      }
    );
  }

  /**** EDIT PAGE | Responses Table | Get questions list for response table. ****/
  getMultipleChoiceQuestions(component_id: string) {
    return this.multipleChoiceDataService.getQuestions(component_id);
  }

  /*** EDIT PAGE | Submit Reponses ****/
  submitResponses(candidate_id, componentId, data) {
    this.multipleChoiceDataService.submitResponses(candidate_id, componentId, data).subscribe(
      (res) => {
        if (res && res['data']) {
          this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
          timer(500).subscribe(() => {
            this.router.navigate(['main/marks/multiple-choice/list']);
          });
        } else {
          this.sharedService.setToaster({ ...saveFail, body: res['message'] });
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }

  /*** MULTIPLE CHOICE  SET Current filters ***/
  setCurrentFilters(key: string, value: Object) {
    this.currentFilters[key] = value;
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  resetCandidateDetails() {
    this.currentCandidateDetails = null;
    this.currentCandidateDetailsChanged.next(null);
  }

  /*** MULTIPLE CHOICE  Empty Current filters ***/
  emptyCurrentFilters() {
    this.currentFilters = [];
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }
}
