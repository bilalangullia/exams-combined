import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import urls from '../../../shared/config.urls';

@Injectable()
export class MultipleChoiceDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };

  /*** LIST PAGE | FILTERS | Subjects ***/
  getOptionSubject(examination_id) {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.multiChoiceTypeSubject}/${examination_id}/subjects`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /*** LIST PAGE | FILTERS | Components ***/
  getOptionComponent(option_id) {
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.multiChoiceTypeOption}/${option_id}/components`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /*** LIST PAGE | List Data ***/
  getList(academic_period_id, examination_id, examination_center_id, options_id, compId) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.mulitpleChoiceList}?examination_centre_id=${examination_center_id}&examination_id=${examination_id}&academic_period_id=${academic_period_id}&options_id=${options_id}&compId=${compId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** LIST PAGE | List Data ***/
  // http://openemis.n2.iworklab.com/api/multiple-choice/candidates-list?examination_centre_id=2&examination_id=3&academic_period_id=4&options_id=43&compId=131&keyword=Hatt
  getListSearch(academic_period_id, examination_id, examination_center_id, options_id, compId, keyword) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.mulitpleChoiceList}?examination_centre_id=${examination_center_id}&examination_id=${examination_id}&academic_period_id=${academic_period_id}&options_id=${options_id}&compId=${compId}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** List page | Generating Marks. ***/
  generateMarks(candidate_id, compId) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/multiple-choice/${urls.candidate}/${candidate_id}/queList?compID=${compId}`,
        {},
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** List page | Generating Marks. ***/
  generateAllMarks(examination_centre_id, examination_id, academic_period_id, options_id, compId) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/multiple-choice/generate-all?examination_centre_id=${examination_centre_id}&examination_id=${examination_id}&academic_period_id=${academic_period_id}&options_id=${options_id}&compId=${compId}`,
        {},
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** VIEW/EDIT PAGE | Get candidate details for populating the edit fields. ***/
  getCandidateDetails(id, componentId) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.multipleChoiceView}/${urls.candidate}/${id}/view?compId=${componentId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** EDIT PAGE | Responses Table | Get questions list for response table. ***/
  getQuestions(component_id) {
    return this.httpClient
      .get(`${environment.baseUrl}/multiple-choice/${component_id}/questions`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /*** EDIT PAGE | Submit Reponses ****/
  submitResponses(candidate_id, componentId, data) {
    // http://openemis.n2.iworklab.com/api/multiple-choice/20FOA030001/update-response/12
    return this.httpClient
      .post(
        `${environment.baseUrl}/multiple-choice/${candidate_id}/update-response/${componentId}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }
}
