import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { MultipleChoiceRoutingModule } from './multiple-choice-routing.module';
import { MultipleChoiceViewComponent } from './multiple-choice-view/multiple-choice-view.component';
import { MultipleChoiceListComponent } from './multiple-choice-list/multiple-choice-list.component';
import { MultipleChoiceEditComponent } from './multiple-choice-edit/multiple-choice-edit.component';
import { MultipleChoiceService } from './multiple-choice.service';
import { ImportFormComponent } from './import-form/import-form.component';
import { ImportDetailComponent } from './import-detail/import-detail.component';
import { MultipleChoiceDataService } from './multiple-choice-data.service';

@NgModule({
  imports: [CommonModule, MultipleChoiceRoutingModule, SharedModule],
  declarations: [
    MultipleChoiceViewComponent,
    MultipleChoiceListComponent,
    MultipleChoiceEditComponent,
    ImportFormComponent,
    ImportDetailComponent
  ],
  providers: [MultipleChoiceService, MultipleChoiceDataService]
})
export class MultipleChoiceModule {}
