import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  IDynamicFormApi,
  KdTableButtonEvent,
  ITableFormUpdateParams
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../shared/shared.service';
import { invalidIdError } from '../../../../shared/shared.toasters';
import { MultipleChoiceService } from '../multiple-choice.service';
import { DEFAULT_RESPONSE_OPTIONS } from '../multiple-choice-table.config';
import { QUESTION_BASE, FORM_BUTTONS } from './multiple-choice-edit.config';

@Component({
  selector: 'app-multiple-choice-edit',
  templateUrl: './multiple-choice-edit.component.html',
  styleUrls: ['./multiple-choice-edit.component.css']
})
export class MultipleChoiceEditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Marks - Multiple Choice';

  public loading: boolean = true;
  public showForm: boolean = true;
  public _questionBase: any = QUESTION_BASE;
  public api: IDynamicFormApi = {};
  public _formButtons: any = FORM_BUTTONS;

  private currentFilters: any = null;
  private candidateData: any;
  public responses: any = [];
  private multipleChoiceQuestions: Array<any> = [];
  private candidateId: string = '';
  private component: any;
  private subject: any;
  private answerKeyMap: any = { '1': 'A', '2': 'B', '3': 'C', '4': 'D' };

  /* Subscriptions */
  private candidateIdSub: Subscription;
  private candidateDetailsSubscription: Subscription;
  private currentFiltersSubscription: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private multipleChoiceService: MultipleChoiceService,
    private sharedService: SharedService
  ) {
    super({ router, pageEvent, activatedRoute });

    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.candidateIdSub = this.multipleChoiceService.selectedCandidateIdChanged.subscribe((candidateId) => {
      if (!candidateId) {
        this.sharedService.setToaster(invalidIdError);
        this.router.navigate(['/main/marks/multiple-choice/list']);
      } else {
        this.candidateId = candidateId;

        this.currentFiltersSubscription = this.multipleChoiceService.currentFiltersChanged.subscribe((filters) => {
          if (filters['subject_id'] && filters['component_id']) {
            this.currentFilters = filters;

            this.multipleChoiceService.getCandidateDetails(
              this.candidateId,
              this.currentFilters['component_id']['key']
            );

            this.multipleChoiceService.getMultipleChoiceQuestions(filters['component_id']['key']).subscribe((res) => {
              this.multipleChoiceQuestions = res['data'];
            });
          }
        });

        this.candidateDetailsSubscription = this.multipleChoiceService.currentCandidateDetailsChanged.subscribe(
          (data: any) => {
            if (data) {
              this.candidateData = { ...data };

              setTimeout(() => {
                this.setCandidateDetails(this.candidateData);
              }, 100);
            }
          }
        );
      }
    });
  }

  setCandidateDetails(data: any) {
    this._questionBase.forEach((question) => {
      if (question['key'] == 'responses') {
        this.api
          .table('responses')
          .addRow([...this.multipleChoiceQuestions.map((item) => ({ ...item, id: item['question'] }))]);

        this.multipleChoiceQuestions.forEach((item) => {
          let responseObj = data['response'].find((dataItem) => dataItem['question'] == item['question']);

          let rowResponse: ITableFormUpdateParams = {
            questionKey: 'response',
            colId: 'response',
            rowId: item['question'],
            property: 'value',
            data: null
          };
          if (responseObj) {
            rowResponse['data'] = DEFAULT_RESPONSE_OPTIONS.find((option) => option['value'] == responseObj['response'])[
              'key'
            ];
          }
          this.api.table('responses').setTableCellInputProperty(rowResponse);
        });
      } else if (question['key'] == 'option') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['subject_id']['value']);
      } else if (question['key'] == 'component') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['component_id']['value']);
      } else {
        this.api.setProperty(
          question['key'],
          'value',
          data[question['key']]
            ? data[question['key']]['value']
              ? data[question['key']]['value']
              : data[question['key']]
            : ''
        );
      }
    });
  }

  cancel() {
    this.router.navigate(['main/marks/multiple-choice/view']);
  }

  submitForm(form) {
    let responses = form['responses'].map((item) => ({
      question: item['question'],
      response: item['response']['response'] ? this.answerKeyMap[item['response']['response']] : '',
      examination_components_id: this.currentFilters['component_id']['key']
    }));
    let payload = { response: [...responses] };

    this.multipleChoiceService.submitResponses(this.candidateId, this.currentFilters['component_id']['key'], payload);
  }

  ngOnDestroy() {
    if (this.candidateDetailsSubscription) {
      this.candidateDetailsSubscription.unsubscribe();
    }
    if (this.currentFiltersSubscription) {
      this.currentFiltersSubscription.unsubscribe();
    }
    if (this.candidateIdSub) {
      this.candidateIdSub.unsubscribe();
    }
    this.multipleChoiceService.resetCandidateDetails();
    super.destroyPageBaseSub();
  }
}
