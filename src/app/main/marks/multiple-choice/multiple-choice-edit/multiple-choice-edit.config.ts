import { TABLE_COLUMN_LIST } from '../multiple-choice-table.config';

export interface IMultiChoiceFilters {
  examination_centre_id?: number;
  examination_id?: number;
  academic_period_id?: number;
  options_id?: number;
  component?: number;
}

export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_centre_id',
    label: 'Exam Center',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'option',
    label: 'Options',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'component',
    label: 'Component',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'candidate_name',
    label: 'Candidate Name',
    visible: true,
    required: true,
    order: 1,
    controlType: 'text',
    placeholder: 'Candidate Name',
    type: 'text',
    readonly: true
  },
  {
    key: 'responses',
    rowIdKey: 'id',
    label: 'Response',
    visible: true,
    required: false,
    controlType: 'table',
    row: [],
    column: [TABLE_COLUMN_LIST.question, TABLE_COLUMN_LIST.response],
    config: {
      rowId: 'id',
      id: 'responses',
      gridHeight: 300,
      loadType: 'normal'
    }
  }
];

export const FORM_BUTTONS: Array<any> = [
  { type: 'submit', name: 'Save', icon: 'kd-check', class: 'btn-text' },
  { type: 'reset', name: 'Cancel', icon: 'kd-close', class: 'btn-outline' }
];
