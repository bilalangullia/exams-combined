import { Observable, Subscriber, timer } from 'rxjs';

interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  CandidateID?: Column;
  CandidateName?: Column;
}

export const TABLECOLUMN: ListColumn = {
  CandidateID: {
    headerName: 'Candidate ID',
    field: 'candidate_id',
    sortable: true,
    filterable: true,
    filterValue: []
  },

  CandidateName: {
    headerName: 'Candidate Name',
    field: 'candidate_name',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const CREATE_ROW: (_rowCount: number, _baseIndex?: number, datalist?: Array<any>) => Array<any> = (
  _rowCount: number,
  _baseIndex?: number,
  datalist?: Array<any>
): Array<any> => {
  let row: Array<any> = [];
  let schoolRandom: Array<string> = ['First Primary School', 'Second Primary School', 'Last Primary School'];
  let countryRandom: Array<string> = ['Singapore', 'Malaysia', 'China', 'Japan'];
  let yearRandom: Array<number> = [2016, 2017, 2018, 2019, 2020];
  let gender: Array<string> = ['Male', 'Female'];
  for (let i = 0; i < datalist.length; i++) {
    let oneRow: any = {
      candidate_id: datalist[i].candidate_id,
      first_name: datalist[i].first_name,
      objectitem: {
        level: {
          default: i % 2,
          original: 3
        },
        another: [i, i + 1, i + 2, i + 3]
      },
      inputColumnVal: {
        inputFormVal: i.toString()
      }
    };

    row.push(oneRow);
  }

  return row;
};

export const DUMMY_API_CALL: (_params: {
  startRow: number;
  endRow: number;
  filterModel: any;
  sortModel: any;
  pagesize: number;
  dataList?: Array<any>;
}) => Observable<any> = (_params: {
  startRow: number;
  endRow: number;
  filterModel: any;
  sortModel: any;
  pagesize: number;
  dataList?: Array<any>;
}): Observable<any> => {
  return new Observable((_observer: Subscriber<any>): void => {
    timer(1000).subscribe((): void => {
      _observer.next(CREATE_ROW(_params.pagesize, _params.startRow, _params.dataList));
      _observer.complete();
    });
  });
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: 0, value: '-- Select  --' }],
    events: true
  },
  {
    key: 'examination_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: '', value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination_center_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: '', value: '-- Select --' }],
    events: true
  },
  {
    key: 'subject_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: '', value: '-- Select --' }],
    events: true
  },
  {
    key: 'component_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: '', value: '-- Select --' }],
    events: true
  }
];

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
