import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { ExcelExportParams } from 'ag-grid';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdFilter,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi
} from 'openemis-styleguide-lib';

import { DataService } from '../../../../shared/data.service';
import { SharedService } from '../../../../shared/shared.service';
import { MultipleChoiceService } from '../multiple-choice.service';
import { FILTER_INPUTS, TABLECOLUMN } from '../multiple-choice.config';

@Component({
  selector: 'app-multiple-choice-list',
  templateUrl: './multiple-choice-list.component.html',
  styleUrls: ['./multiple-choice-list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class MultipleChoiceListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;
  public inputs: Array<any> = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public _row: Array<any>;
  public loading: boolean = true;
  public showTable: boolean = false;
  public _smallLoaderValue: number = null;
  public _tableApi: ITableApi = {};

  private _question1Val: string;
  public initYear: any;
  private _question2Val: string;
  public academic_period_id: any;
  public examination_id: any;
  public examination_center_id: any;
  public subject_id: any;
  public component_id: any;
  public candidateId: any;
  public itemFind = {};
  public defaultList = {};

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.multipleChoiceService.setCandidateId(_rowNode.data);
            this.router.navigate(['main/marks/multiple-choice/view']);
          }
        },
        {
          type: 'Edit',
          callback: (_rowNode, _tableApi): void => {
            this.multipleChoiceService.setCandidateId(_rowNode.data);
            this.router.navigate(['main/marks/multiple-choice/edit']);
          }
        },
        {
          icon: 'fa fa-refresh',
          name: 'Generate',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.candidateId = _rowNode.data.candidate_id;
            if (this.candidateId) {
              this.multipleChoiceService.generateMultiChoiceMarks(this.candidateId, this.component_id);
            }
          }
        }
      ]
    }
  };

  public tableColumns: Array<ITableColumn> = [TABLECOLUMN.CandidateID, TABLECOLUMN.CandidateName];

  /* Subscriptions */
  public currentFilterSub: Subscription;
  public examFilterSub: Subscription;
  public examCenterFilterSub: Subscription;
  public subjectFilterSub: Subscription;
  public yearFilterSub: Subscription;
  public componentFilterSub: Subscription;
  public listSub: Subscription;

  constructor(
    private router: Router,
    private pageEvent: KdPageBaseEvent,
    private activatedRoute: ActivatedRoute,
    public tableEvent: KdTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    private multipleChoiceService: MultipleChoiceService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle('Marks - Multiple Choices', false);
    super.setToolbarMainBtns([
      {
        icon: 'fa fa-refresh',
        callback: (): void => {
          this.examination_id,
            this.subject_id,
            this.component_id,
            this.multipleChoiceService.generateAllMultipleChoiceMarks(
              this.examination_center_id,
              this.examination_id,
              this.academic_period_id,
              this.subject_id,
              this.component_id
            );
        }
      },
      {
        type: 'import',
        callback: (): void => {
          this.router.navigate(['main/marks/multiple-choice/import']);
        }
      },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'multipleChoiceSubjects_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.multipleChoiceService.emptyCurrentFilters();
    timer(200).subscribe(() => {
      this.multipleChoiceService.getFilterYear();
      this.yearFilterSub = this.multipleChoiceService.filterYearChanged.subscribe((data: any) => {
        if (data) {
          let tempOption = [];
          this.initYear = data[0].id;
          data.forEach((item) => {
            tempOption.push({
              key: item.id,
              value: item.name
            });
          });
          this._updateView.setInputProperty('academic_period_id', 'options', tempOption);
          this._updateView.setInputProperty('academic_period_id', 'value', tempOption[0].key);
          timer(100).subscribe((): void => {
            this.multipleChoiceService.getFilterExam(this.initYear);
          });
        } else {
          this._updateView.setInputProperty('academic_period_id', 'options', [{ key: '', value: '-- Select --' }]);
        }
      });
    });

    setTimeout(() => {
      this.initializeSubscriptions();
      this.currentFilterSub = this.multipleChoiceService.currentFiltersChanged.subscribe((filters) => {
        if (
          filters['academic_period_id'] &&
          filters['examination_id'] &&
          filters['examination_center_id'] &&
          filters['subject_id'] &&
          filters['component_id'] &&
          filters['academic_period_id']['key'] &&
          filters['examination_id']['key'] &&
          filters['examination_center_id']['key'] &&
          filters['subject_id']['key'] &&
          filters['component_id']['key']
        ) {
          this.multipleChoiceService.getMultiChoiceList(
            filters['academic_period_id']['key'],
            filters['examination_id']['key'],
            filters['examination_center_id']['key'],
            filters['subject_id']['key'],
            filters['component_id']['key']
          );
        }
      });

      this.listSub = this.multipleChoiceService.listChanged.subscribe((list: Array<any>) => {
        if (list['length']) {
          this.populateTable(list);
        } else {
          this.showTable = false;
        }
      });
    }, 0);
  }

  searchList(keyword: string) {
    if (keyword && keyword.length > 2) {
      this.multipleChoiceService.searchMultipleChoiceList(
        this.academic_period_id,
        this.examination_id,
        this.examination_center_id,
        this.subject_id,
        this.component_id,
        keyword
      );
    } else if (!keyword) {
      this.multipleChoiceService.searchMultipleChoiceList(
        this.academic_period_id,
        this.examination_id,
        this.examination_center_id,
        this.subject_id,
        this.component_id,
        ''
      );
    }
  }

  initializeSubscriptions() {
    this.examFilterSub = this.multipleChoiceService.filterExamChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.name
          });
        });
        this._updateView.setInputProperty('examination_id', 'options', tempOption);
        this._updateView.setInputProperty('examination_id', 'value', tempOption[0].key);
        this.multipleChoiceService.setCurrentFilters('examination_id', tempOption[0]);
      } else {
        this._updateView.setInputProperty('examination_id', 'options', [{ key: '', value: '-- Select --' }]);
        this.multipleChoiceService.setCurrentFilters('examination_id', {});
        this._updateView.setInputProperty('examination_center_id', 'options', [{ key: '', value: '-- Select --' }]);
        this._updateView.setInputProperty('subject_id', 'options', [{ key: '', value: '-- Select --' }]);
        this._updateView.setInputProperty('examination_id', 'value', null);
        this._updateView.setInputProperty('examination_center_id', 'value', null);
        this._updateView.setInputProperty('subject_id', 'value', null);
      }
    });

    this.examCenterFilterSub = this.multipleChoiceService.filterExamCenterChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.code + ' - ' + item.name
          });
        });

        this._updateView.setInputProperty('examination_center_id', 'options', tempOption);
        this._updateView.setInputProperty('examination_center_id', 'value', tempOption[0].key);
        this.multipleChoiceService.setCurrentFilters('examination_center_id', tempOption[0]);
      } else {
        this.multipleChoiceService.setCurrentFilters('examination_center_id', {});
        this._updateView.setInputProperty('examination_center_id', 'options', [{ key: '', value: '-- Select --' }]);
      }
    });

    this.subjectFilterSub = this.multipleChoiceService.filterSubjectChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.code + ' - ' + item.name
          });
        });

        this._updateView.setInputProperty('subject_id', 'options', tempOption);
        this._updateView.setInputProperty('subject_id', 'value', tempOption[0].key);
        this.multipleChoiceService.setCurrentFilters('subject_id', tempOption[0]);
      } else {
        this.multipleChoiceService.setCurrentFilters('subject_id', {});
        this._updateView.setInputProperty('subject_id', 'options', [{ key: '', value: '-- Select  --' }]);
        this.multipleChoiceService.setCurrentFilters('component_id', {});
        this._updateView.setInputProperty('component_id', 'options', [{ key: '', value: '-- Select  --' }]);
      }
    });

    this.componentFilterSub = this.multipleChoiceService.filterComponentChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({
            key: item.id,
            value: item.code + ' ' + item.name
          });
        });
        // tempOption.unshift({ key: '', value: '-- Select  --' });
        this._updateView.setInputProperty('component_id', 'options', tempOption);
        this._updateView.setInputProperty('component_id', 'value', tempOption[0].key);
        this.multipleChoiceService.setCurrentFilters('component_id', tempOption[0]);
      } else {
        this.multipleChoiceService.setCurrentFilters('component_id', {});
        this._updateView.setInputProperty('component_id', 'options', [{ key: '', value: '-- Select  --' }]);
      }
    });
  }

  populateTable(data) {
    this.resetFilter();
    if (data.length) {
      this.loading = false;
      this.showTable = true;
      this._row = data;
      this._row.forEach((rowdata) => {
        if (TABLECOLUMN.CandidateName.filterValue.length == 0) {
          TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
          TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
        } else {
          TABLECOLUMN.CandidateName.filterValue.indexOf(rowdata.candidate_name) > -1
            ? 'na'
            : TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
          TABLECOLUMN.CandidateID.filterValue.indexOf(rowdata.candidate_id) > -1
            ? 'na'
            : TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
        }
      });
    }
  }

  public detectValue(question: any): void {
    this.defaultList[question.key] = question.value;
    let _defaultOptions: Array<any> = [{ key: '', value: '-- Select --' }];
    let newOptions: Array<any> = [
      { key: '', value: '-- Select From ' + this._question1Val + '-' + question.value + ' --' },
      { key: '1', value: 'Options ' + this._question1Val + '-' + question.value + '-1' },
      { key: '2', value: 'Options ' + this._question1Val + '-' + question.value + '-2' }
    ];
    if (question.key === 'academic_period_id') {
      this.academic_period_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.multipleChoiceService.setCurrentFilters('academic_period_id', {
          ...this.itemFind
        });
        this._question1Val = '';
        this._question1Val = question.value;
        timer(100).subscribe((): void => {
          this.multipleChoiceService.getFilterExam(this.academic_period_id);
        });
      } else {
        newOptions = _defaultOptions.slice();
        this.multipleChoiceService.setCurrentFilters('academic_period_id', {});
      }
    } else if (question.key === 'examination_id') {
      this.examination_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.multipleChoiceService.setCurrentFilters('examination_id', {
          ...this.itemFind
        });
        this._question1Val = '';
        this._question1Val = question.value;
        timer(100).subscribe((): void => {
          this.multipleChoiceService.getFilterExamCenter(this.examination_id);
          this.multipleChoiceService.getFilterSubject(this.examination_id);
        });
      } else {
        newOptions = _defaultOptions.slice();
        this.multipleChoiceService.setCurrentFilters('examination_id', {});
      }
    } else if (question.key === 'examination_center_id') {
      this._question2Val = '';
      this.examination_center_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.multipleChoiceService.setCurrentFilters('examination_center_id', {
          ...this.itemFind
        });
        this._question2Val = question.value;
      } else {
        newOptions = _defaultOptions.slice();
        this.multipleChoiceService.setCurrentFilters('examination_center_id', {});
      }
    } else if (question.key === 'subject_id') {
      this.subject_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.multipleChoiceService.setCurrentFilters('subject_id', {
          ...this.itemFind
        });
        this._question2Val = '';
        timer(100).subscribe((): void => {
          this.multipleChoiceService.getFilterComponent(this.subject_id);
        });
      } else {
        newOptions = _defaultOptions.slice();
        this.multipleChoiceService.setCurrentFilters('subject_id', {});
      }
    } else if (question.key === 'component_id') {
      this.component_id = question.value;
      this.itemFind = question.options.find((item) => item['key'] == question.value);
      if (question['value']) {
        this.multipleChoiceService.setCurrentFilters('component_id', {
          ...this.itemFind
        });
        this._question2Val = '';
      } else {
        newOptions = _defaultOptions.slice();
        this.multipleChoiceService.setCurrentFilters('component_id', {});
      }
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.listSub) {
      this.listSub.unsubscribe();
    }
    if (this.currentFilterSub) {
      this.currentFilterSub.unsubscribe();
    }
    if (this.examFilterSub) {
      this.examFilterSub.unsubscribe();
    }
    if (this.examCenterFilterSub) {
      this.examCenterFilterSub.unsubscribe();
    }
    if (this.subjectFilterSub) {
      this.subjectFilterSub.unsubscribe();
    }
    if (this.yearFilterSub) {
      this.yearFilterSub.unsubscribe();
    }
    if (this.componentFilterSub) {
      this.componentFilterSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
