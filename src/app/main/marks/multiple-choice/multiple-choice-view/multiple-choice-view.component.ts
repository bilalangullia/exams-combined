import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../shared/shared.service';
import { DataService } from '../../../../shared/data.service';
import { detailsNotFound } from '../../../../shared/shared.toasters';
import { MultipleChoiceService } from '../multiple-choice.service';
import { VIEWNODE_INPUT } from './multiple-choice-view.config';

@Component({
  selector: 'app-multiple-choice-view',
  templateUrl: './multiple-choice-view.component.html',
  styleUrls: ['./multiple-choice-view.component.css']
})
export class MultipleChoiceViewComponent extends KdPageBase implements OnInit {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public candidateData: any;
  public _smallLoaderValue: number = null;
  public subject: any;
  public component: any;
  public componentId: any;

  /* Subscriptions */
  private candidateDetailsSub: Subscription;
  private currentFiltersSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    public dataService: DataService,
    private multipleChoiceService: MultipleChoiceService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Marks - Multiple Choices', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/marks/multiple-choice/list' },
      { type: 'edit', path: 'main/marks/multiple-choice/edit' }
    ]);
  }

  ngOnInit(): void {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.currentFiltersSub = this.multipleChoiceService.currentFiltersChanged.subscribe((filters) => {
      if (Object.keys(filters).length) {
        if (filters['subject_id'] && filters['component_id']) {
          if ((filters['subject_id']['key'] && filters['component_id']['key']) !== '') {
            this.subject = filters['subject_id']['value'];
            this.component = filters['component_id']['value'];
            this.componentId = filters['component_id']['key'];
            timer(200).subscribe(() => {
              this.getCandidateDetails();
            });
          }
        }
      } else {
        this.router.navigate(['main/marks/multiple-choice/list']);
      }
    });
  }

  getCandidateDetails() {
    let candidateID = this.sharedService.getCandidateId();
    if (!candidateID) {
      this.sharedService.setToaster(detailsNotFound);
      this.router.navigate(['/main/marks/multiple-choice/list']);
    } else {
      this.multipleChoiceService.getCandidateDetails(candidateID, this.componentId);
      this.candidateDetailsSub = this.multipleChoiceService.currentCandidateDetailsChanged.subscribe(
        (data: any) => {
          this.candidateData = data;
          if (this.candidateData) {
            this.setCandidateDetail(this.candidateData);
          }
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  setCandidateDetail(data: any) {
    timer(500).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'response') {
          this.api.setProperty(this._questionBase[i].key, 'row', data.response);
        } else if (this._questionBase[i].key == 'option') {
          this.api.setProperty(this._questionBase[i].key, 'value', this.subject);
        } else if (this._questionBase[i].key == 'component') {
          this.api.setProperty(this._questionBase[i].key, 'value', this.component);
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
    this.loading = false;
  }

  ngOnDestroy(): void {
    if (this.candidateDetailsSub) {
      this.candidateDetailsSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    this.multipleChoiceService.resetCandidateDetails();
    super.destroyPageBaseSub();
  }
}
