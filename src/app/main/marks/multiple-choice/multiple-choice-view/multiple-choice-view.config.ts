export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_centre_id',
    label: 'Exam Centre',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'option',
    label: 'Options',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'component',
    label: 'Component',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'candidate_name',
    label: 'Candidate Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'response',
    label: 'Response',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Question',
        field: 'question',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Response',
        field: 'response',
        sortable: false,
        filterable: true,
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'options',
      rowIdKey: 'id',
      gridHeight: 250,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  }
];
