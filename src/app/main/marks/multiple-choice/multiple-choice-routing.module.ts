import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MultipleChoiceListComponent } from './multiple-choice-list/multiple-choice-list.component';
import { MultipleChoiceViewComponent } from './multiple-choice-view/multiple-choice-view.component';
import { MultipleChoiceEditComponent } from './multiple-choice-edit/multiple-choice-edit.component';
import { ImportFormComponent } from './import-form/import-form.component';
import { ImportDetailComponent } from './import-detail/import-detail.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: MultipleChoiceListComponent },
  { path: 'view', component: MultipleChoiceViewComponent },
  { path: 'edit', component: MultipleChoiceEditComponent },
  { path: 'import', component: ImportFormComponent },
  { path: 'detail', component: ImportDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MultipleChoiceRoutingModule {}
