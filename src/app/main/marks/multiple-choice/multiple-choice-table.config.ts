export const DEFAULT_RESPONSE_OPTIONS = [
  { key: null, value: '-- Select --' },
  { key: 1, value: 'A' },
  { key: 2, value: 'B' },
  { key: 3, value: 'C' },
  { key: 4, value: 'D' }
];

interface IResponsesTableColumns {
  question: any;
  response: any;
}

const QUESTION_NUMBER = {
  headerName: 'Question No.',
  label: 'Question No.',
  field: 'question',
  visible: true,
  type: 'text',
  class: 'ag-id'
};

const RESPONSE_INPUT_DROPDOWN = {
  headerName: 'Response',
  field: 'response',
  visible: true,
  type: 'input',
  class: 'ag-id',
  config: {
    input: {
      controlType: 'dropdown',
      key: 'response',
      visible: true,
      options: DEFAULT_RESPONSE_OPTIONS
    }
  }
};

export const TABLE_COLUMN_LIST: IResponsesTableColumns = {
  question: QUESTION_NUMBER,
  response: RESPONSE_INPUT_DROPDOWN
};
