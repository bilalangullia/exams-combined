import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', redirectTo: 'registration', pathMatch: 'full' },
      {
        path: 'registration',
        loadChildren: './registration/registration.module#RegistrationModule'
      },
      { path: 'marks', loadChildren: './marks/marks.module#MarksModule' },
      {
        path: 'results',
        loadChildren: './results/results.module#ResultsModule'
      },
      {
        path: 'certificates',
        loadChildren: './certificates/certificates.module#CertificatesModule'
      },
      {
        path: 'reports',
        loadChildren: './reports/reports.module#ReportsModule'
      },
      {
        path: '',
        loadChildren: './administration/administration.module#AdministrationModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {}
