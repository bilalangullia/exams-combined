import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'forecast-grades' },
  { path: 'forecast-grades', loadChildren: './forecast-grades/forecast-grades.module#ForecastGradesModule' },
  { path: 'grade-reviews', loadChildren: './grade-reviews/grade-reviews.module#GradeReviewsModule' },
  { path: 'final-grades', loadChildren: './final-grades/final-grades.module#FinalGradesModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultsRoutingModule {}
