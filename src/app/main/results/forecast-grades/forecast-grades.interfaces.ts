export interface IFilters {
  academic_period_id?: { key: number; value: string };
  examination_id?: { key: number; value: string };
  examination_centre_id?: { key: number; value: string };
  option_id?: { key: number; value: string };
}
