import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

import { DataService } from '../../../shared/data.service';
import { SharedService } from '../../../shared/shared.service';
import { detailsNotFound, invalidIdError, saveFail, updateSuccess } from '../../../shared/shared.toasters';
import { ForecastGradesDataService } from './forecast-grades-data.service';
import { IFilters } from './forecast-grades.interfaces';
@Injectable()
export class ForecastGradesService {
  private filterYear: any;
  public filterYearChanged = new Subject();
  private filterExam: any;
  public filterExamChanged = new Subject();
  private filterExamCenter: any;
  public filterExamCenterChanged = new Subject();
  private filterOption: any;
  public filterOptionChanged = new Subject();
  private forecastGrade: any;
  public forecastGradeChanged = new Subject();
  private list: any;
  public listChanged = new Subject();
  public candidateEditDetails: any = [];
  public candidateEditDetailsChanged = new BehaviorSubject<any>(this.candidateEditDetails);
  private selectedCandidateId: string;
  public selectedCandidateIdChanged = new Subject<string>();
  private currentFilters: any = {};
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);

  public gradeImportDetail: any;

  constructor(
    private router: Router,
    private dataService: DataService,
    private forecastGradeDataService: ForecastGradesDataService,
    private sharedService: SharedService
  ) {}

  /*** Set Candidate ID when navigating away from list page -> view/edit pages ***/
  setCandidateId(_rowData: any) {
    if (_rowData && _rowData.candidate_id) {
      this.sharedService.setCandidateId(_rowData.candidate_id);
      this.selectedCandidateId = this.sharedService.getCandidateId();
      this.selectedCandidateIdChanged.next(this.selectedCandidateId);
    } else {
      this.sharedService.setToaster(invalidIdError);
      setTimeout(() => {
        this.router.navigate['main/results/forecast-grades/list'];
      }, 500);
    }
  }

  /*** LIST PAGE | FILTERS | Year***/
  getFilterYear() {
    let data = 'academicPeriods';
    this.dataService.getOptionsAcademicPeriod(data).subscribe((res: any) => {
      if (res && res.data && res.data.academic_period_id) {
        this.filterYear = res.data.academic_period_id;
        this.filterYearChanged.next([...this.filterYear]);
      } else {
        this.filterYear = [];
        this.filterYearChanged.next([...this.filterYear]);
      }
      (err) => {
        this.filterYear = [];
        this.filterYearChanged.next([...this.filterYear]);
      };
    });
  }

  /*** LIST PAGE | FILTERS | Exam Cert ***/
  getFilterExam(academic_id) {
    this.dataService.getOptionsExam(academic_id).subscribe(
      (res: any) => {
        if (res && res.data) {
          this.filterExam = res.data;
          this.filterExamChanged.next([...this.filterExam]);
        } else {
          this.filterExam = [];
          this.filterExamChanged.next([...this.filterExam]);
        }
      },
      (err) => {
        this.filterExam = [];
        this.filterExamChanged.next([...this.filterExam]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Exam Center ***/
  getFilterExamCenter(examination_id) {
    this.dataService.getOptionsExamCenter(examination_id).subscribe(
      (res: any) => {
        this.filterExamCenter = res.data;
        this.filterExamCenterChanged.next([...this.filterExamCenter]);
      },
      (err) => {
        this.filterExamCenter = [];
        this.filterExamCenterChanged.next([...this.filterExamCenter]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Options ***/
  getFilterOption(examination_id) {
    this.dataService.getOptionsOptions(examination_id).subscribe(
      (data: any) => {
        this.filterOption = data.data;
        this.filterOptionChanged.next([...this.filterOption]);
      },
      (err) => {
        this.filterOption = [];
        this.filterOptionChanged.next([...this.filterOption]);
      }
    );
  }

  /*** LIST PAGE | List Data ***/
  getForecastGradesList(academic_period_id, examination_id, examination_centre_id, options_id) {
    this.forecastGradeDataService
      .getList(academic_period_id, examination_id, examination_centre_id, options_id)
      .subscribe(
        (res: any) => {
          if (res.data.length) {
            this.list = res.data;
            this.listChanged.next([...this.list]);
          } else {
            this.list = [];
            this.listChanged.next([...this.list]);
          }
        },
        (err) => {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      );
  }

  /*** Candidate  Details Data  ***/
  getCandidateDetails(candidate_id, options_id) {
    this.forecastGradeDataService.getCandidateDetails(candidate_id, options_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.candidateEditDetails = res.data;
          this.candidateEditDetailsChanged.next([...this.candidateEditDetails]);
        } else {
          this.candidateEditDetails = [];
          this.candidateEditDetailsChanged.next([...this.candidateEditDetails]);
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.candidateEditDetails = [];
        this.candidateEditDetailsChanged.next([...this.candidateEditDetails]);
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Options ***/
  getForecastGradeDropdown(option_id) {
    this.forecastGradeDataService.getGradeDropdown(option_id).subscribe(
      (data: any) => {
        if (data.data.length) {
          this.forecastGrade = data.data;
          this.forecastGradeChanged.next([...this.forecastGrade]);
        } else {
          this.forecastGrade = [];
          this.forecastGradeChanged.next([...this.forecastGrade]);
        }
      },
      (err) => {
        this.forecastGrade = [];
        this.forecastGradeChanged.next([...this.forecastGrade]);
      }
    );
  }

  /*** List Data | SEARCH***/
  searchForeCastGradesList(academic_period_id, examination_id, examination_centre_id, options_id, keyword) {
    this.forecastGradeDataService
      .getListSearch(academic_period_id, examination_id, examination_centre_id, options_id, keyword)
      .subscribe(
        (data) => {
          this.list = data['data'];
          this.listChanged.next([...this.list]);
        },
        (err) => {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      );
  }

  /*** EDIT PAGE SUBMIT  ***/
  updateCandidateDetail(candidate_id, data) {
    this.forecastGradeDataService.updateCandidateDetails(candidate_id, data).subscribe(
      (data: any) => {
        this.sharedService.setToaster(updateSuccess);
        this.router.navigate(['main/results/forecast-grades/list']);
      },
      (err) => {
        this.sharedService.setToaster(saveFail);
      }
    );
  }

  /*** SET Current filters ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  emptyCurrentFilters() {
    this.currentFilters = {};
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  setImportDetail(event) {
    this.gradeImportDetail = event;
  }

  getImportDetail() {
    return this.gradeImportDetail;
  }

  gradeImport(data) {
    return this.forecastGradeDataService.gradeImport(data);
  }
}
