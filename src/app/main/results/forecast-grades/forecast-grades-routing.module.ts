import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForecastGradesEntryComponent } from './forecast-grades-entry/forecast-grades-entry.component';
import { ForecastGradesListComponent } from './forecast-grades-entry/forecast-grades-list/forecast-grades-list.component';
import { ForecastImportFormComponent } from './forecast-grades-entry/forecast-import-form/forecast-import-form.component';
import { ForecastImportDetailComponent } from './forecast-grades-entry/forecast-import-detail/forecast-import-detail.component';
import { ForecastGradesEditComponent } from './forecast-grades-entry/forecast-grades-edit/forecast-grades-edit.component';

const routes: Routes = [
  {
    path: '',
    component: ForecastGradesEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: ForecastGradesListComponent },
      { path: 'import', component: ForecastImportFormComponent },
      { path: 'detail', component: ForecastImportDetailComponent },
      { path: 'edit', component: ForecastGradesEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForecastGradesRoutingModule {}
