import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  IMiniDashboardItem,
  IMiniDashboardConfig,
  MINI_DASHBOARD_CONFIG,
  MINI_DASHBOARD_DATA,
  TABLECOLUMN,
  CREATE_ROW,
} from './config';
import {
  KdPageBase,
  KdPageBaseEvent,
  ITableColumn,
  ITableConfig,
  ITableApi,
  KdTableEvent,
  KdToolbarEvent,
  IBtnGroupConfig,
  KdAlertEvent,
} from 'openemis-styleguide-lib';

import { timer, Subscription } from 'rxjs';
import { ForecastGradesService } from '../../forecast-grades.service';

@Component({
  selector: 'app-forecast-import-detail',
  templateUrl: './forecast-import-detail.component.html',
  styleUrls: ['./forecast-import-detail.component.css'],
})
export class ForecastImportDetailComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TABLEID: string = 'normalTable';
  readonly PAGESIZE: number = 10;
  readonly TOTALROWS: number = 100;
  readonly STARTINDEX: number = 0;
  public name: string = 'button-group-sample';
  public config: IBtnGroupConfig = {
    type: 'radio',
    iconOnly: true,
  };
  public loading = true;
  public imprtData: any;
  public csvData: any = [];
  public csvDataSuccess: any = [];
  public value: any = 0;
  public rowErro: any = [];
  public IsError = true;

  public miniDashboardConfig: IMiniDashboardConfig = MINI_DASHBOARD_CONFIG;
  public miniDashboardData: Array<IMiniDashboardItem> = MINI_DASHBOARD_DATA;

  public fileinput: Array<any> = [
    {
      visible: false,
    },
  ];
  public button: Array<any> = [
    {
      name: 'Downloads failed records',
      type: 'button',
      icon: 'kd-download',
      key: 'failed',
      class: 'btn btn-error',
      controlType: 'btn-text',
    },
    {
      name: 'Downloads successfull records',
      type: 'button',
      icon: 'kd-download',
      key: 'success',
      class: 'btn btn-success',
      controlType: 'btn-text',
      id: 'sucess',
    },
  ];

  public _tableApi: ITableApi = {};
  public _row: Array<any>;
  public _column: Array<ITableColumn>;

  public _config: ITableConfig = {
    id: this.TABLEID,
    rowIdKey: 'id',
    gridHeight: 'auto',
    rowContentHeight: 30,
    loadType: 'oneshot',
    externalFilter: false,
    action: {
      enabled: true,
      list: [
        {
          name: 'View Errors',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            if (this.IsError) {
              let errors1 = [];
              let errorsBody = '';
              errors1 = Object.keys(this.imprtData[_rowNode.data.id].errors);
              errors1.forEach((k) => {
                errorsBody += this.imprtData[_rowNode.data.id].errors[k];
              });
              let toasterConfig: any = {
                title: 'Row  failed to import',
                body: errorsBody,
                showCloseButton: true,
                tapToDismiss: false,
              };
              this._KdAlert.error(toasterConfig);
            } else {
              let toasterConfig: any = {
                title: 'Import Success',
                body: 'The file "OpenEMIS_Exams_Import_Forecast_Grade_Template.xlsx" is successfully imported.',
                showCloseButton: true,
                tapToDismiss: false,
              };
              this._KdAlert.success(toasterConfig);
            }
          },
        },
      ],
    },
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROWS,
    },
    click: {
      type: 'none',
      callback: (): void => {},
    },
  };

  private _toolbarSearchSub: Subscription;
  private _tableSub: Subscription;
  constructor(
    _pageEvent: KdPageBaseEvent,
    public _router: Router,
    _activatedRoute: ActivatedRoute,
    private _tableEvent: KdTableEvent,
    private _toolbarEvent: KdToolbarEvent,
    public forecastGradesService: ForecastGradesService,
    public _KdAlert: KdAlertEvent
  ) {
    super({
      router: _router,
      pageEvent: _pageEvent,
      activatedRoute: _activatedRoute,
    });
    var importDataList = this.forecastGradesService.getImportDetail();

    if (importDataList != undefined) {
      this.csvDataSuccess = [];
      this.miniDashboardData[0].value = importDataList.data.total_count;
      this.miniDashboardData[1].value = importDataList.data.records_added.count;
      this.miniDashboardData[2].value = importDataList.data.records_updated.count;
      this.miniDashboardData[3].value = importDataList.data.records_failed.count;
      if (importDataList.data.records_failed.rows.length > 0) {
        this.imprtData = [];
        let toasterConfig: any = {
          title: 'Import Failed',
          body: 'The file OpenEMIS_Exams_Import_Forecast_Grade_Template.xlsx failed to import completely.',
          showCloseButton: true,
          tapToDismiss: false,
        };
        this.imprtData = importDataList.data.records_failed.rows;
        importDataList.data.records_added.rows.forEach((data1) => {
          this.csvDataSuccess.push(data1);
        });
        importDataList.data.records_updated.rows.forEach((data2) => {
          this.csvDataSuccess.push(data2);
        });
        this.loading = false;
        this._KdAlert.error(toasterConfig);
      } else if (importDataList.data.records_failed.count == 0) {
        this.csvDataSuccess = [];

        //this._config.action.list[0].name="View"
        this.csvDataSuccess = [];
        this._config.action.enabled = false;
        this.button[0].disabled = true;
        this.imprtData = [];
        this.IsError = false;
        let toasterConfig: any = {
          title: 'Import Success',
          body: 'The file "OpenEMIS_Exams_Import_Forecast_Grade_Template.xlsx" is successfully imported.',
          showCloseButton: true,
          tapToDismiss: false,
        };
        importDataList.data.records_added.rows.forEach((data1) => {
          this.csvDataSuccess.push(data1);
        });
        importDataList.data.records_updated.rows.forEach((data2) => {
          this.csvDataSuccess.push(data2);
        });
        this.imprtData = this.csvDataSuccess;
        this.loading = false;
        this._KdAlert.success(toasterConfig);
      }
    } else {
      this._router.navigate(['/main/results/forecast-grades/list']);
    }
  }

  ngOnInit(): void {
    console.log(this._config);
    super.setPageTitle('Forecast Grades ­ Import Forecast Grades', false);
    super.setToolbarMainBtns([]);

    super.updatePageHeader();
    super.updateBreadcrumb();
    timer(1000).subscribe((): void => {
      this._column = [
        TABLECOLUMN.RowNumber,
        TABLECOLUMN.AcademicPeriod,
        TABLECOLUMN.ExamCode,
        TABLECOLUMN.CentreCode,
        TABLECOLUMN.OpenEmisID,
        TABLECOLUMN.CandidateID,
        TABLECOLUMN.Name,
        TABLECOLUMN.SubjectCode,
        TABLECOLUMN.ForecastGrade,
      ];
    });

    timer(2000).subscribe((): void => {
      this._row = CREATE_ROW(this.TOTALROWS, this.STARTINDEX, this.imprtData);
    });

    this._toolbarSearchSub = this._toolbarEvent.onSendSearchText().subscribe((_text: string): void => {
      this._tableApi.general.searchRow(_text);
    });

    this._tableSub = this._tableEvent.onKdTableEventList(this.TABLEID).subscribe((_event: any): void => {
      console.log(_event);
    });
  }

  public _buttonEvent(formVal: any): void {
    // console.log("formVal..formVal",formVal)
    let setCsvData = {};
    if (formVal.key === 'failed') {
      this.csvData = [];
      for (let i = 0; i < this.imprtData.length; i++) {
        setCsvData = {
          'Academic Period': this.imprtData[i].data['Academic Period'],
          'Exam Code': this.imprtData[i].data['Exam Code'],
          'Centre Code': this.imprtData[i].data['Centre Code'],
          'OpenEMIS ID': this.imprtData[i].data['OpenEMIS ID'],
          'Candidate ID': this.imprtData[i].data['Candidate ID'],
          Name: this.imprtData[i].data['Name'],
          'Subject Code': this.imprtData[i].data['Subject Code'],
          'Forecast Grade': this.imprtData[i].data['Forecast Grade'],
        };
        let keys = Object.keys(this.imprtData[i].errors);
        keys.forEach((k) => {
          setCsvData[k] = this.imprtData[i].errors[k];
        });
        this.csvData.push(setCsvData);
      }
      if (this.csvData != undefined) {
        this.JSONToCSVConvertor(this.csvData, 'OpenEMIS_Exams_Import_Forecast_Grade_Template', true);
      }
    } else {
      this.csvData = [];
      for (let i = 0; i < this.csvDataSuccess.length; i++) {
        setCsvData = {
          'Candidate ID': this.csvDataSuccess[i].data['Candidate ID'],
          'Academic Period': this.csvDataSuccess[i].data['Academic Period'],
          'Exam Code': this.csvDataSuccess[i].data['Exam Code'],
          'Centre Code': this.csvDataSuccess[i].data['Centre Code'],
          'Question Num': this.csvDataSuccess[i].data['Question Num'],
          Response: this.csvDataSuccess[i].data['Response'],
        };
        this.csvData.push(setCsvData);
      }
      if (this.csvData != undefined) {
        console.log(this.csvData.length);
        this.JSONToCSVConvertor(this.csvData, 'SuccesFully uploaded Import Forecast Grade Template', true);
      }
    }
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

  JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    var CSV = '';
    CSV += ReportTitle + '\r\n\n';
    if (ShowLabel) {
      var row = '';
      for (var index in arrData[0]) {
        row += index + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }
    for (var i = 0; i < arrData.length; i++) {
      var row = '';
      for (var index in arrData[i]) {
        row += '"' + arrData[i][index] + '",';
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      alert('Invalid data');
      return;
    }
    var fileName = 'OpenEmis_';
    fileName += ReportTitle.replace(/ /g, '_');
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    var link = document.createElement('a');
    link.href = uri;
    link.download = fileName + '.csv';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}
