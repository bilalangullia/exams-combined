import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, KdFilter, ITableConfig, ITableColumn, ITableApi } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../shared/shared.interfaces';
import { ForecastGradesService } from '../../forecast-grades.service';
import { IFilters } from '../../forecast-grades.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './forecast-grades-list.config';
import { ExcelExportParams } from 'ag-grid';
@Component({
  selector: 'app-forecast-grades-list',
  templateUrl: './forecast-grades-list.component.html',
  styleUrls: ['./forecast-grades-list.component.css']
})
export class ForecastGradesListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;
  public inputs: Array<any> = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public _row: Array<any>;
  public loading: boolean = true;
  public viewTable: boolean = false;
  public _tableApi: ITableApi = {};

  private currentFilters: IFilters = null;

  /* Subscriptions */
  public currentFiltersSub: Subscription;
  public yearFilterSub: Subscription;
  public examFilterSub: Subscription;
  public examCenterFilterSub: Subscription;
  public optionFilterSub: Subscription;
  public listSub: Subscription;

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.CandidateName,
    TABLECOLUMN.ForecastGrade,
    TABLECOLUMN.ModifiedOn,
    TABLECOLUMN.ModifiedBy
  ];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    private forecastGradesService: ForecastGradesService,
    private sharedService: SharedService
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          type: 'Edit',
          callback: (_rowNode, _tableApi): void => {
            if (_rowNode && _rowNode.data && _rowNode.data['candidate_id']) {
              this.forecastGradesService.setCandidateId(_rowNode.data);
              setTimeout(() => {
                this.router.navigate(['main/results/forecast-grades/edit']);
              }, 0);
            } else {
              let toasterConfig: IToasterConfig = {
                title: 'Invalid Candidate ID',
                type: 'error',
                timeout: 1000,
                showCloseButton: true,
                tapToDismiss: true
              };
              this.sharedService.setToaster(toasterConfig);
            }
          }
        }
      ]
    }
  };

  ngOnInit() {
    this.loading = false;
    super.setPageTitle('Forecast Grades', false);
    super.setToolbarMainBtns([
      { type: 'import', path: 'main/results/forecast-grades/import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'forecastGradesCandidates_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.forecastGradesService.emptyCurrentFilters();

    timer(100).subscribe(() => {
      this.forecastGradesService.getFilterYear();
      this.yearFilterSub = this.forecastGradesService.filterYearChanged.subscribe((options: Array<any>) => {
        this.setFilterOptions('academic_period_id', options);
      });
    });

    this.examFilterSub = this.forecastGradesService.filterExamChanged.subscribe((options: Array<any>) => {
      this.setFilterOptions('examination_id', options);
      if (!options.length) {
        this.setFilterOptions('examination_centre_id', []);
        this.setFilterOptions('option_id', []);
      }
    });

    this.examCenterFilterSub = this.forecastGradesService.filterExamCenterChanged.subscribe((options: Array<any>) => {
      this.setFilterOptions('examination_centre_id', options);
    });

    this.optionFilterSub = this.forecastGradesService.filterOptionChanged.subscribe((options: any) => {
      this.setFilterOptions('option_id', options);
    });

    this.currentFiltersSub = this.forecastGradesService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (
        filters &&
        filters['academic_period_id'] &&
        filters['academic_period_id']['key'] &&
        filters['examination_id'] &&
        filters['examination_id']['key'] &&
        filters['examination_centre_id'] &&
        filters['examination_centre_id']['key'] &&
        filters['option_id'] &&
        filters['option_id']['key']
      ) {
        this.currentFilters = { ...filters };
        this.forecastGradesService.getForecastGradesList(
          filters['academic_period_id']['key'],
          filters['examination_id']['key'],
          filters['examination_centre_id']['key'],
          filters['option_id']['key']
        );
      } else {
        timer(200).subscribe(() => {
          this.viewTable = false;
        });
      }
    });

    timer(200).subscribe(() => {
      this.listSub = this.forecastGradesService.listChanged.subscribe((list: Array<any>) => {
        if (list['length']) {
          this.populateTable(list);
        } else {
          timer(200).subscribe(() => {
            this.viewTable = false;
          });
        }
      });
    });
  }

  populateTable(data) {
    this.resetFilterValues();
    if (data.length) {
      this.loading = false;
      this.viewTable = true;
      this._row = data;
      this._row.forEach((rowdata) => {
        if (!TABLECOLUMN.CandidateName.filterValue.length) {
          TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
          TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
          TABLECOLUMN.ForecastGrade.filterValue.push(rowdata.review_criteria);
          TABLECOLUMN.ModifiedOn.filterValue.push(rowdata.created_on);
          TABLECOLUMN.ModifiedBy.filterValue.push(rowdata.modified_on);
        } else {
          TABLECOLUMN.CandidateID.filterValue.indexOf(rowdata.candidate_id) > -1
            ? 'na'
            : TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
          TABLECOLUMN.CandidateName.filterValue.indexOf(rowdata.candidate_name) > -1
            ? 'na'
            : TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
          TABLECOLUMN.ForecastGrade.filterValue.indexOf(rowdata.review_criteria) > -1
            ? 'na'
            : TABLECOLUMN.ForecastGrade.filterValue.push(rowdata.review_criteria);
          TABLECOLUMN.ModifiedOn.filterValue.indexOf(rowdata.created_on) > -1
            ? 'na'
            : TABLECOLUMN.ModifiedOn.filterValue.push(rowdata.created_on);
          TABLECOLUMN.ModifiedBy.filterValue.indexOf(rowdata.modified_on) > -1
            ? 'na'
            : TABLECOLUMN.ModifiedBy.filterValue.push(rowdata.modified_on);
        }
      });
    }
  }

  setFilterOptions(key: string, data: Array<any>) {
    if (data.length) {
      let options: Array<any> = [];
      if (key === 'academic_period_id' || key === 'examination_id') {
        options = data.map((item) => ({ key: item['id'], value: item['name'] }));
        this._updateView.setInputProperty(key, 'options', options);
      } else if (key === 'examination_centre_id' || key === 'option_id') {
        options = data.map((item) => ({ key: item['id'], value: `${item['code']} - ${item['name']}` }));
        this._updateView.setInputProperty(key, 'options', options);
      }
      setTimeout(() => {
        this._updateView.setInputProperty(key, 'value', options[0]['key']);
      }, 0);
    } else {
      this._updateView.setInputProperty(key, 'options', [{ key: null, value: '-- Select --' }]);
      this._updateView.setInputProperty(key, 'value', null);
    }
  }

  detectValue(question: any): void {
    let value = question['options'].find((item) => item['key'] == question['value']);
    this.forecastGradesService.setCurrentFilters({ [question['key']]: value });
    if (question['key'] === 'academic_period_id' && question['value']) {
      this.forecastGradesService.getFilterExam(question['value']);
    } else if (question['key'] === 'examination_id' && question['value']) {
      this.forecastGradesService.getFilterExamCenter(question['value']);
      this.forecastGradesService.getFilterOption(question['value']);
    }
  }

  searchList(keyword: string) {
    if (keyword && keyword.length > 2) {
      this.forecastGradesService.searchForeCastGradesList(
        this.currentFilters['academic_period_id']['key'],
        this.currentFilters['examination_id']['key'],
        this.currentFilters['examination_centre_id']['key'],
        this.currentFilters['option_id']['key'],
        keyword
      );
    } else if (!keyword) {
      this.forecastGradesService.searchForeCastGradesList(
        this.currentFilters['academic_period_id']['key'],
        this.currentFilters['examination_id']['key'],
        this.currentFilters['examination_centre_id']['key'],
        this.currentFilters['option_id']['key'],
        ''
      );
    }
  }

  resetFilterValues() {
    this.tableColumns.forEach((column) => {
      if (column['filterable']) {
        column.filterValue = [];
      }
    });
  }

  ngOnDestroy(): void {
    if (this.listSub) {
      this.listSub.unsubscribe();
    }
    if (this.yearFilterSub) {
      this.yearFilterSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    if (this.examFilterSub) {
      this.examFilterSub.unsubscribe();
    }
    if (this.examCenterFilterSub) {
      this.examCenterFilterSub.unsubscribe();
    }
    if (this.optionFilterSub) {
      this.optionFilterSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
