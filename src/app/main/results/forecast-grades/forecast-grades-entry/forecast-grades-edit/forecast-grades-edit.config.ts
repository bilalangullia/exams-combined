export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination_centre_id',
    label: 'Exam Center',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'option_id',
    label: 'Option',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'candidate_name',
    label: 'Candidate Name',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'forecast_grade',
    label: 'Forecast Grade',
    visible: true,
    order: 1,
    required: true,
    controlType: 'dropdown',
    options: [{ key: '', value: '--no data--' }],
    events: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
