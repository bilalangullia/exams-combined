import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../shared/shared.service';
import { missingFieldsError, invalidIdError } from '../../../../../shared/shared.toasters';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './forecast-grades-edit.config';
import { ForecastGradesService } from '../../forecast-grades.service';

@Component({
  selector: 'app-forecast-grades-edit',
  templateUrl: './forecast-grades-edit.component.html',
  styleUrls: ['./forecast-grades-edit.component.css']
})
export class ForecastGradesEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public formValue: any;
  public candidateId: any;
  public candidateData: any;
  public optionValue: any;
  public optionId: any;

  /* Subscriptions */
  private filterValuesSub: Subscription;
  private candidateDetailsSub: Subscription;
  private listChangedSub: Subscription;
  private forecastGradeSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private forecastGradesService: ForecastGradesService,
    private sharedService: SharedService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  }
  ngOnInit() {
    super.setPageTitle('Forecast Grades', false);
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.setDropDownValue();

    this.filterValuesSub = this.forecastGradesService.currentFiltersChanged.subscribe((filters) => {
      if (
        filters &&
        filters['academic_period_id'] &&
        filters['academic_period_id']['value'] &&
        filters['examination_id'] &&
        filters['examination_id']['value'] &&
        filters['examination_centre_id'] &&
        filters['examination_centre_id']['value'] &&
        filters['option_id'] &&
        filters['option_id']['value']
      ) {
        if ((filters['academic_period_id'] && filters['examination_id'] && filters['option_id']) !== '') {
          this.optionValue = filters['option_id']['value'];
          this.optionId = filters['option_id']['key'];
          this.candidateDetailsSub = this.forecastGradesService.candidateEditDetailsChanged.subscribe((details) => {
            if (details) {
              this.setCandidateDetails({
                ...details[0],
                academic_period_id: filters['academic_period_id'],
                examination_id: filters['examination_id'],
                examination_centre_id: filters['examination_center_id']
              });
            }
          });
          this.getCandidateDetails();
        }
      } else {
        this._router.navigate(['main/results/forecast-grades/list']);
      }
    });
  }

  setDropDownValue() {
    timer(100).subscribe((): void => {
      this.forecastGradesService.getForecastGradeDropdown(this.optionId);
      this.initializeSubscriptions();
    });
  }

  initializeSubscriptions() {
    this.forecastGradeSub = this.forecastGradesService.forecastGradeChanged.subscribe((data: any) => {
      if (data.length) {
        let tempOption = [];
        data.forEach((item) => {
          tempOption.push({ key: item.id, value: item.option_id });
        });
        tempOption.unshift({ key: '', value: '--select--' });
        this._updateView.setInputProperty('forecast_grade', 'options', tempOption);
      }
    });
    this.loading = false;
  }

  getCandidateDetails() {
    this.loading = false;
    this.candidateId = this.sharedService.getCandidateId();
    if (this.candidateId != undefined) {
      this.forecastGradesService.getCandidateDetails(this.candidateId, this.optionId);
      this.listChangedSub = this.forecastGradesService.candidateEditDetailsChanged.subscribe(
        (res: any) => {
          if (res) {
            this.candidateData = res[0];
            if (this.candidateData) {
              this.setCandidateDetails(this.candidateData);
            }
          }
        },
        (err) => {
          this.sharedService.setToaster(invalidIdError);
          this._router.navigate(['main/results/forecast-grades/list']);
        }
      );
    } else {
      this.sharedService.setToaster(invalidIdError);
      this._router.navigate(['main/results/forecast-grades/list']);
    }
  }

  setCandidateDetails(data: any) {
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'option_id') {
          this.api.setProperty(this._questionBase[i].key, 'value', this.optionValue);
        } else if (this._questionBase[i].key == 'forecast_grade') {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            data[this._questionBase[i]['key']] && data[this._questionBase[i]['key']].length
              ? data[this._questionBase[i]['key']][0].examination_grading_option['id']
              : ''
          );
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
  }

  submitVal(formVal) {
    if (this.requiredCheck(formVal)) {
      this.sharedService.setToaster(missingFieldsError);
    } else {
      let payload = {
        grade_id: formVal.forecast_grade,
        options_id: this.optionId
      };
      this.forecastGradesService.updateCandidateDetail(this.candidateId, payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/results/forecast-grades/list']);
  }

  ngOnDestroy(): void {
    if (this.filterValuesSub) {
      this.filterValuesSub.unsubscribe();
    }
    if (this.listChangedSub) {
      this.listChangedSub.unsubscribe();
    }
    if (this.candidateDetailsSub) {
      this.candidateDetailsSub.unsubscribe();
    }
    if (this.forecastGradeSub) {
      this.forecastGradeSub.unsubscribe();
    }
    this.forecastGradesService.emptyCurrentFilters();
    this.candidateId = '';
    super.destroyPageBaseSub();
  }
}
