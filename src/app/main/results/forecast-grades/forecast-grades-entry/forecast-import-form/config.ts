export const file_INPUT={
  file_inputs:   {
 
         'key': 'fileinput_double_buttons',
         'label': 'Select File To Import',
         'visible': true,
         'required': true,
         'controlType': 'file-input',
         'type': 'file',
         'config': { 'infoText': [
            {
                'text': 'Format Supported: xls, xlsx, ods, zip '
            },{
                'text':'File size should not be larger than 512KB.'
            },{
                'text':'Recommended Maximum Records: 2000'
            }
        ],
             'leftToolbar': true,
             'leftButton': [
                 {
                     'icon': 'kd-download',
                     'label': 'Download',
                     'callback': (): void => {
                         exportToExcel();
                     }
                 }
             ]
         }
     }
     
 }
 

 export const fileInputs =[
  
    {
      'key': 'academic_period_id',
      'label': 'Academic Period',
      'visible': true,
      'required': true,
      'controlType': 'dropdown',
      'event': true,
      'order': 1,
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
    },
    {
      'key': 'examination_id',
      'label': 'Examination',
      'visible': true,
      'required': true,
      'event': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
    },
    {
      'key': 'examination_centre_id',
      'label': 'Centre',
      'visible': true,
      'required': true,
      'event': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
    },
    {
      'key': 'option_id',
      'label': 'Option',
      'visible': true,
      'required': true,
      'event': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
    },
    file_INPUT.file_inputs,
  ];

export const button  = [
    {
      name: 'Import',
      btnType: 'btn-text',
      type: 'submit',
      icon: 'kd-import',
      key: 'fileinput_double_buttons',
      controlType: 'file-input',
    },
    {
      name: 'Cancel',
      type: 'reset',
      class: 'btn-outline',
      icon: 'kd-cross',
      
    }
  ];

 
const exportToExcel= ()=> {
  
 var uri = '/exams/assets/excelformat/OpenEMIS_Exams_Import_Forecast_Grades_Template.xlsx';
 var link = document.createElement("a");
 link.href = uri;
 link.download = 'OpenEMIS_Exams_Import_Forecast_Grades_Template.xlsx';
 document.body.appendChild(link);
 link.click();
 document.body.removeChild(link);
}