import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import urls from '../../../shared/config.urls';

@Injectable()
export class ForecastGradesDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /*** LIST | API  ***/
  getList(academic_period_id, examination_id, examination_centre_id, optId) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.results}/${urls.forecastGrades}/${urls.forecastList}?academic_period_id=${academic_period_id}&examination_id=${examination_id}&examination_centre_id=${examination_centre_id}&optId=${optId}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** SEARCH | API  ***/
  getListSearch(academic_period_id, examination_id, examination_centre_id, options_id, keyword) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.mulitpleChoiceList}?academic_period_id=${academic_period_id}&examination_id=${examination_id}&examination_centre_id=${examination_centre_id}&options_id=${options_id}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** FORECAST GRADE DROPDOWN  ***/
  getGradeDropdown(option_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.results}/${urls.forecastGrades}/${urls.forecastGradeDropDown}/${option_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | EDIT | API  ***/
  getCandidateDetails(candidate_id, options_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.multipleChoice}/candidate/${candidate_id}/view?options_id=${options_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** IMPORT PAGE ***/
  gradeImport(data) {
    return this.httpClient
      .post(`${environment.baseUrl}/results/forecast_grades/import`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /*** EDIT | SUBMIT API  ***/
  updateCandidateDetails(candidate_id, data) {
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.results}/${urls.forecastGrades}/update-forecatGrade/${candidate_id}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
