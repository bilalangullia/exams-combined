import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { ForecastGradesService } from './forecast-grades.service';
import { ForecastGradesRoutingModule } from './forecast-grades-routing.module';
import { ForecastGradesEntryComponent } from './forecast-grades-entry/forecast-grades-entry.component';
import { ForecastGradesListComponent } from './forecast-grades-entry/forecast-grades-list/forecast-grades-list.component';
import { ForecastImportDetailComponent } from './forecast-grades-entry/forecast-import-detail/forecast-import-detail.component';
import { ForecastImportFormComponent } from './forecast-grades-entry/forecast-import-form/forecast-import-form.component';
import { ForecastGradesEditComponent } from './forecast-grades-entry/forecast-grades-edit/forecast-grades-edit.component';
import { ForecastGradesDataService } from './forecast-grades-data.service';

@NgModule({
  imports: [CommonModule, ForecastGradesRoutingModule, SharedModule],
  declarations: [
    ForecastGradesEntryComponent,
    ForecastGradesListComponent,
    ForecastImportDetailComponent,
    ForecastImportFormComponent,
    ForecastGradesEditComponent,
  ],
  providers: [ForecastGradesService, ForecastGradesDataService],
})
export class ForecastGradesModule {}
