import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { GradeReviewsService } from './grade-reviews.service';
import { GradeReviewsRoutingModule } from './grade-reviews-routing.module';
import { GradeReviewsEntryComponent } from './grade-reviews-entry/grade-reviews-entry.component';
import { GradeReviewsListComponent } from './grade-reviews-entry/grade-reviews-list/grade-reviews-list.component';
import { GradeReviewsViewComponent } from './grade-reviews-entry/grade-reviews-view/grade-reviews-view.component';
import { GradeReviewsDataService } from './grade-reviews-data.service';

@NgModule({
  imports: [CommonModule, GradeReviewsRoutingModule, SharedModule],
  declarations: [GradeReviewsEntryComponent, GradeReviewsListComponent, GradeReviewsViewComponent],
  providers: [GradeReviewsService, GradeReviewsDataService],
})
export class GradeReviewsModule {}
