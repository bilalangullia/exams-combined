export interface IOption {
  key: number;
  value: string;
}

export interface IFilterOptions {
  academic_period_id?: Array<IOption>;
  examination_id?: Array<IOption>;
  examination_centre_id?: Array<IOption>;
  option_id?: Array<IOption>;
}

export interface IFilters {
  academic_period_id?: { key: number; value: string };
  examination_id?: { key: number; value: string };
  examination_centre_id?: { key: number; value: string };
  option_id?: { key: number; value: string };
}

export interface IReviewCriteria {
  forecast_deviation?: boolean;
  component_deviation?: boolean;
  mark_deviation?: boolean;
}
