import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import { DataService } from '../../../shared/data.service';
import { SharedService } from '../../../shared/shared.service';
import { IFilters, IReviewCriteria } from './grade-review.interfaces';
import { GradeReviewsDataService } from './grade-reviews-data.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class GradeReviewsService {
  private optionsAcademicPeriod: Array<any> = null;
  private optionsExamination: Array<any> = null;
  private optionsExaminationCentre: Array<any> = null;
  private optionsOption: Array<any> = null;
  private currentFilters: any = null;
  private list: Array<any> = [];
  private selectedCandidateId: string = null;
  private candidateName: string = null;
  private candidateReviewCriteria: IReviewCriteria = null;
  private candidateDetails: any = null;

  public optionsAcademicPeriodChanged = new BehaviorSubject(this.optionsAcademicPeriod);
  public optionsExaminationChanged = new Subject();
  public optionsExaminationCentreChanged = new Subject();
  public optionsOptionChanged = new Subject();
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  public listChanged = new Subject();
  public selectedCandidateIdChanged = new BehaviorSubject<string>(this.selectedCandidateId);
  public candidateNameChanged = new BehaviorSubject<string>(this.candidateName);
  public candidateReviewCriteriaChanged = new BehaviorSubject<any>(this.candidateReviewCriteria);
  public candidateDetailsChanged = new BehaviorSubject<any>(this.candidateDetails);

  constructor(
    private dataService: DataService,
    private sharedService: SharedService,
    private gradeReviewsDataService: GradeReviewsDataService
  ) {}

  /*** Grade Reviews | List Page |  Get Options for Academic Period Filter ***/
  getFilterYear() {
    this.dataService.getOptionsAcademicPeriod('academicPeriods').subscribe(
      (res: any) => {
        this.optionsAcademicPeriod = res['data']['academic_period_id'];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      },
      (err: HttpErrorResponse) => {
        console.log(err['error']['message']);
        this.optionsAcademicPeriod = [];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      }
    );
  }

  /*** Grade Reviews | List Page |  Get Options for Examination Filter ***/
  getFilterExam(academic_id) {
    this.dataService.getOptionsExam(academic_id).subscribe(
      (res: any) => {
        this.optionsExamination = res.data;
        this.optionsExaminationChanged.next([...this.optionsExamination]);
      },
      (err: HttpErrorResponse) => {
        console.log(err['error']['message']);
        this.optionsExamination = [];
        this.optionsExaminationChanged.next([...this.optionsExamination]);
      }
    );
  }

  /*** Grade Reviews | List Page |  Get Options for Examination Center Filter***/
  getFilterExamCenter(examination_id) {
    this.dataService.getOptionsExamCenter(examination_id).subscribe(
      (res: any) => {
        this.optionsExaminationCentre = res.data;
        this.optionsExaminationCentreChanged.next([...this.optionsExaminationCentre]);
      },
      (err: HttpErrorResponse) => {
        console.log(err['error']['message']);
        this.optionsExaminationCentre = [];
        this.optionsExaminationCentreChanged.next([...this.optionsExaminationCentre]);
      }
    );
  }

  /*** Grade Reviews | List Page |  Get Options for Options Filter ***/
  getFilterOption(examination_id) {
    this.dataService.getOptionsOptions(examination_id).subscribe(
      (res: any) => {
        this.optionsOption = res.data;
        this.optionsOptionChanged.next([...this.optionsOption]);
      },
      (err: HttpErrorResponse) => {
        console.log(err['error']['message']);
        this.optionsOption = [];
        this.optionsOptionChanged.next([...this.optionsOption]);
      }
    );
  }

  /*** Grade Reviews | Reset Options for Filters ***/
  resetOptions() {
    this.optionsAcademicPeriod = [];
    this.optionsAcademicPeriodChanged.next([]);
    this.optionsExamination = [];
    this.optionsExaminationChanged.next([]);
    this.optionsExaminationCentre = [];
    this.optionsExaminationCentreChanged.next([]);
    this.optionsOption = [];
    this.optionsOptionChanged.next([]);
  }

  /*** Grade Reviews | Set Current Filter ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Grade Reviews | Reset Filters ***/
  resetFilters() {
    this.setCurrentFilters(null);
    this.resetOptions();
  }

  /*** Set Candidate ID when navigating away from list page -> view/edit pages ***/
  setCandidateId(candidateId: string) {
    this.selectedCandidateId = candidateId;
    this.selectedCandidateIdChanged.next(this.selectedCandidateId);
  }

  resetCandidateId() {
    this.selectedCandidateId = null;
    this.selectedCandidateIdChanged.next(null);
  }

  setCandidateName(candidateName: string) {
    this.candidateName = candidateName;
    this.candidateNameChanged.next(this.candidateName);
  }

  resetCandidateName() {
    this.candidateName = null;
    this.candidateNameChanged.next(null);
  }

  setCandidateReviewCriteria(reviewCriterias: string) {
    this.candidateReviewCriteria = {};
    let criterias: Array<any> = reviewCriterias.split(',');
    criterias.forEach((item) => {
      if (item === 'Forecast') {
        this.candidateReviewCriteria['forecast_deviation'] = true;
      } else if (item === 'Component') {
        this.candidateReviewCriteria['component_deviation'] = true;
      } else if (item === 'Mark') {
        this.candidateReviewCriteria['mark_deviation'] = true;
      }
    });
    this.candidateReviewCriteriaChanged.next({ ...this.candidateReviewCriteria });
  }

  resetCandidateReviewCriteria() {
    this.candidateReviewCriteria = null;
    this.candidateReviewCriteriaChanged.next(null);
  }

  /*** Grade Reviews | List Page | GET List ***/
  getGradeReviewsList(academic_period_id, examination_id, examination_centre_id, option_id) {
    this.gradeReviewsDataService
      .getList(academic_period_id, examination_id, examination_centre_id, option_id)
      .subscribe(
        (res: any) => {
          this.list = res.data;
          this.listChanged.next([...this.list]);
        },
        (err: HttpErrorResponse) => {
          console.log(err['error']['message']);
          this.list = [];
          this.listChanged.next([...this.list]);
          let toasterConfig: any = {
            type: 'error',
            title: 'Error while fetching list',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
  }

  /*** Grade Reviews | List Page | GET Search List ***/
  searchGradeReviewsList(
    academic_period_id: number,
    examination_id: number,
    examination_centre_id: number,
    option_id: number,
    keyword: string
  ) {
    this.gradeReviewsDataService
      .searchList(academic_period_id, examination_id, examination_centre_id, option_id, keyword)
      .subscribe(
        (res: any) => {
          this.list = res['data'];
          this.listChanged.next([...this.list]);
        },
        (err: HttpErrorResponse) => {
          console.log(err['error']['message']);
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      );
  }

  /*** Grade Reviews | Reset List ***/
  resetGradeReviewsList() {
    this.list = [];
    this.listChanged.next([]);
  }

  /*** Grade Reviews | View Page | GET Candidate Details  ***/
  getGradeReviewsDetails(candidate_id) {
    this.gradeReviewsDataService.getDetails(candidate_id).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.candidateDetails = res['data'][0];
          this.candidateDetailsChanged.next({ ...this.candidateDetails });
        } else {
          this.candidateDetails = null;
          this.candidateDetailsChanged.next(null);
        }
      },
      (err: HttpErrorResponse) => {
        console.log(err['error']['message']);
        this.candidateDetails = null;
        this.candidateDetailsChanged.next(null);
      }
    );
  }

  generateGradeReviews() {
    return this.gradeReviewsDataService.generateGradeReviews();
  }

  resetCandidateDetails() {
    this.candidateDetails = null;
    this.candidateDetailsChanged.next(null);
  }
}
