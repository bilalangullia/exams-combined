import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExcelExportParams } from 'ag-grid';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdFilter,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../shared/shared.interfaces';
import { GradeReviewsService } from '../../grade-reviews.service';
import { IFilters } from '../../grade-review.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './grade-reviews-list.config';

@Component({
  selector: 'app-grade-reviews-list',
  templateUrl: './grade-reviews-list.component.html',
  styleUrls: ['./grade-reviews-list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class GradeReviewsListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;

  public filterInputs: Array<any> = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public _row: Array<any>;
  public loading: boolean = true;
  public showTable: boolean = false;
  public _tableApi: ITableApi = {};

  private _question1Val: string;
  private _question2Val: string;
  private currentFilters: IFilters = null;
  public initYear: any;
  public academic_period_id: any;
  public examination_id: any;
  public examination_center_id: any;
  public option_id: any;
  public itemFind = {};
  public defaultList = {};

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.CandidateName,
    TABLECOLUMN.ReviewCriteria,
    TABLECOLUMN.CreatedOn,
    TABLECOLUMN.ModifiedOn
  ];
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'normal',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            if (_rowNode && _rowNode['data'] && _rowNode['data']['candidate_id']) {
              if (_rowNode['data']['review_criteria']) {
                this.gradeReviewsService.setCandidateReviewCriteria(_rowNode['data']['review_criteria']);
              }
              this.gradeReviewsService.setCandidateId(_rowNode['data']['candidate_id']);
              this.gradeReviewsService.setCandidateName(_rowNode['data']['candidate_name']);
              setTimeout(() => {
                this.router.navigate(['main/results/grade-reviews/view']);
              }, 0);
            } else {
              let toasterConfig: IToasterConfig = {
                title: 'Invalid Candidate ID',
                type: 'error',
                timeout: 1000,
                showCloseButton: true,
                tapToDismiss: true
              };
              this.sharedService.setToaster(toasterConfig);
            }
          }
        }
      ]
    }
  };

  /* Subscriptions */
  private optionsAcademicPeriodSub: Subscription;
  private optionsExaminationSub: Subscription;
  private optionsExaminationCentreSub: Subscription;
  private optionOptionSub: Subscription;
  private currentFiltersSub: Subscription;
  private listSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private gradeReviewsService: GradeReviewsService,
    public _kdalert: KdAlertEvent,
    private sharedService: SharedService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    super.setPageTitle('Grade Reviews', false);
    super.setToolbarMainBtns([
      {
        icon: 'fa fa-refresh',
        name: 'Generate',
        custom: true,
        callback: (_rowNode, _tableApi): void => {
          this.generateGradeReviews();
        }
      },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'gradeReviewsCandidates_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.gradeReviewsService.resetFilters();

    this.loading = false;

    this.gradeReviewsService.getFilterYear();

    this.optionsAcademicPeriodSub = this.gradeReviewsService.optionsAcademicPeriodChanged.subscribe(
      (options: Array<any>) => {
        this.setFilterOptions('academic_period_id', options);
      }
    );

    this.optionsExaminationSub = this.gradeReviewsService.optionsExaminationChanged.subscribe((options: Array<any>) => {
      this.setFilterOptions('examination_id', options);
      if (!options.length) {
        this.setFilterOptions('examination_centre_id', []);
        this.setFilterOptions('option_id', []);
      }
    });

    this.optionsExaminationCentreSub = this.gradeReviewsService.optionsExaminationCentreChanged.subscribe(
      (options: Array<any>) => {
        this.setFilterOptions('examination_centre_id', options);
      }
    );

    this.optionOptionSub = this.gradeReviewsService.optionsOptionChanged.subscribe((options: any) => {
      this.setFilterOptions('option_id', options);
    });

    this.currentFiltersSub = this.gradeReviewsService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (
        filters &&
        filters['academic_period_id'] &&
        filters['academic_period_id']['key'] &&
        filters['examination_id'] &&
        filters['examination_id']['key'] &&
        filters['examination_centre_id'] &&
        filters['examination_centre_id']['key'] &&
        filters['option_id'] &&
        filters['option_id']['key']
      ) {
        this.currentFilters = { ...filters };
        this.gradeReviewsService.getGradeReviewsList(
          filters['academic_period_id']['key'],
          filters['examination_id']['key'],
          filters['examination_centre_id']['key'],
          filters['option_id']['key']
        );
      }
    });

    this.listSub = this.gradeReviewsService.listChanged.subscribe((list: Array<any>) => {
      if (list['length']) {
        this.populateTable(list);
      } else {
        let toasterConfig: any = {
          type: 'error',
          title: 'No List Found!',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
        this.showTable = false;
      }
    });
  }

  setFilterOptions(key: string, data: Array<any>) {
    if (data.length) {
      let options: Array<any> = [];

      if (key === 'academic_period_id' || key === 'examination_id') {
        options = data.map((item) => ({ key: item['id'], value: item['name'] }));
        this._updateView.setInputProperty(key, 'options', options);
      } else if (key === 'examination_centre_id' || key === 'option_id') {
        options = data.map((item) => ({ key: item['id'], value: `${item['code']} - ${item['name']}` }));
        this._updateView.setInputProperty(key, 'options', options);
      }
      setTimeout(() => {
        this._updateView.setInputProperty(key, 'value', options[0]['key']);
      }, 0);
    } else {
      this._updateView.setInputProperty(key, 'options', [{ key: null, value: '-- Select --' }]);
      this._updateView.setInputProperty(key, 'value', null);
    }
  }

  detectValue(question: any): void {
    let value = question['options'].find((item) => item['key'] == question['value']);
    this.gradeReviewsService.setCurrentFilters({ [question['key']]: value });

    if (question['key'] === 'academic_period_id' && question['value']) {
      this.gradeReviewsService.getFilterExam(question['value']);
    } else if (question['key'] === 'examination_id' && question['value']) {
      this.gradeReviewsService.getFilterExamCenter(question['value']);
      this.gradeReviewsService.getFilterOption(question['value']);
    }
  }

  resetFilterValues() {
    this.tableColumns.forEach((column) => {
      if (column['filterable']) {
        column.filterValue = [];
      }
    });
  }

  populateTable(list: Array<any>) {
    this.resetFilterValues();
    this._row = list;
    this._row.forEach((rowdata) => {
      if (!TABLECOLUMN.CandidateName.filterValue.length) {
        TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
        TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
        TABLECOLUMN.ReviewCriteria.filterValue.push(rowdata.review_criteria);
        TABLECOLUMN.CreatedOn.filterValue.push(rowdata.created_on);
        TABLECOLUMN.ModifiedOn.filterValue.push(rowdata.modified_on);
      } else {
        TABLECOLUMN.CandidateID.filterValue.indexOf(rowdata.candidate_id) > -1
          ? 'na'
          : TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
        TABLECOLUMN.CandidateName.filterValue.indexOf(rowdata.candidate_name) > -1
          ? 'na'
          : TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
        TABLECOLUMN.ReviewCriteria.filterValue.indexOf(rowdata.review_criteria) > -1
          ? 'na'
          : TABLECOLUMN.ReviewCriteria.filterValue.push(rowdata.review_criteria);
        TABLECOLUMN.CreatedOn.filterValue.indexOf(rowdata.created_on) > -1
          ? 'na'
          : TABLECOLUMN.CreatedOn.filterValue.push(rowdata.created_on);
        TABLECOLUMN.ModifiedOn.filterValue.indexOf(rowdata.modified_on) > -1
          ? 'na'
          : TABLECOLUMN.ModifiedOn.filterValue.push(rowdata.modified_on);
      }
    });
    this.showTable = true;
  }

  searchList(keyword: string) {
    if (keyword && keyword.length > 2) {
      this.gradeReviewsService.searchGradeReviewsList(
        this.currentFilters['academic_period_id']['key'],
        this.currentFilters['examination_id']['key'],
        this.currentFilters['examination_centre_id']['key'],
        this.currentFilters['option_id']['key'],
        keyword
      );
    } else if (!keyword) {
      this.gradeReviewsService.searchGradeReviewsList(
        this.currentFilters['academic_period_id']['key'],
        this.currentFilters['examination_id']['key'],
        this.currentFilters['examination_centre_id']['key'],
        this.currentFilters['option_id']['key'],
        ''
      );
    }
  }

  generateGradeReviews() {
    this.loading = true;

    this.gradeReviewsService.generateGradeReviews().subscribe(
      (res: any) => {
        this.loading = false;
        if (res['data'] == true) {
          const successMessage: IToasterConfig = {
            type: 'success',
            title: 'Success',
            body: 'Grade Reviews Generated Successfully',
            timeout: 1000,
            tapToDismiss: true,
            showCloseButton: true
          };
          this.sharedService.setToaster(successMessage);
        } else {
          const failureMessage: IToasterConfig = {
            type: 'error',
            title: 'Failure',
            body: 'Error while generating Grade Reviews!',
            timeout: 1000,
            tapToDismiss: true,
            showCloseButton: true
          };
          this.sharedService.setToaster(failureMessage);
        }
      },
      (err) => {
        this.loading = false;
        const errorMessage: IToasterConfig = {
          type: 'error',
          title: 'Error',
          // body: res['message'],
          timeout: 1000,
          tapToDismiss: true,
          showCloseButton: true
        };
        this.sharedService.setToaster(errorMessage);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.optionsAcademicPeriodSub) {
      this.optionsAcademicPeriodSub.unsubscribe();
    }
    if (this.optionsExaminationSub) {
      this.optionsExaminationSub.unsubscribe();
    }
    if (this.optionsExaminationCentreSub) {
      this.optionsExaminationCentreSub.unsubscribe();
    }
    if (this.optionOptionSub) {
      this.optionOptionSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    if (this.listSub) {
      this.listSub.unsubscribe();
    }

    // this.gradeReviewsService.resetFilters();
    this.gradeReviewsService.resetGradeReviewsList();
    super.destroyPageBaseSub();
  }
}
