interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  CandidateID?: Column;
  CandidateName?: Column;
  ReviewCriteria?: Column;
  CreatedOn?: Column;
  ModifiedOn?: Column;
}

export const TABLECOLUMN: ListColumn = {
  CandidateID: {
    headerName: 'Candidate ID',
    field: 'candidate_id',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  CandidateName: {
    headerName: 'Candidate Name',
    field: 'candidate_name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ReviewCriteria: {
    headerName: 'Review Criteria',
    field: 'review_criteria',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  CreatedOn: {
    headerName: 'Created On',
    field: 'created_on',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ModifiedOn: {
    headerName: 'Modified On',
    field: 'modified_on',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination_centre_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'option_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  }
];
