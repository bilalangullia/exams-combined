import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';
import { GradeReviewsService } from '../../grade-reviews.service';
import { VIEWNODE_INPUT } from './grade-reviews-view.config';
import { Subscription } from 'rxjs';
import { IFilters, IReviewCriteria } from '../../grade-review.interfaces';

@Component({
  selector: 'app-grade-reviews-view',
  templateUrl: './grade-reviews-view.component.html',
  styleUrls: ['./grade-reviews-view.component.css']
})
export class GradeReviewsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public _questionBase: any = VIEWNODE_INPUT;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  private currentFilters: IFilters = null;
  private candidateId: string = null;
  private candidateName: string = null;
  private candidateReviewCriteria: IReviewCriteria = null;

  /* Susbcriptions */
  private currentFiltersSub: Subscription;
  private candidateIdSub: Subscription;
  private candidateNameSub: Subscription;
  private candidateReviewCriteriaSub: Subscription;
  private candidateDetailsSub: Subscription;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private pageEvent: KdPageBaseEvent,
    private gradeReviewService: GradeReviewsService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Grade Reviews', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        path: 'main/results/grade-reviews/list',
        callback: () => {
          this.reset();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.candidateIdSub = this.gradeReviewService.selectedCandidateIdChanged.subscribe((candidateId: string) => {
      if (!candidateId) {
        this.router.navigate(['main/results/grade-reviews']);
      } else {
        this.candidateId = candidateId;
        this.gradeReviewService.getGradeReviewsDetails(candidateId);

        this.currentFiltersSub = this.gradeReviewService.currentFiltersChanged.subscribe((filters: IFilters) => {
          if (
            filters &&
            filters['academic_period_id'] &&
            filters['academic_period_id']['value'] &&
            filters['examination_id'] &&
            filters['examination_id']['value'] &&
            filters['examination_centre_id'] &&
            filters['examination_centre_id']['value'] &&
            filters['option_id'] &&
            filters['option_id']['value']
          ) {
            this.currentFilters = { ...filters };
          }
        });

        this.candidateNameSub = this.gradeReviewService.candidateNameChanged.subscribe((candidateName: string) => {
          if (candidateName) {
            this.candidateName = candidateName;
          }
        });

        this.candidateReviewCriteriaSub = this.gradeReviewService.candidateReviewCriteriaChanged.subscribe(
          (reviewCriteria: IReviewCriteria) => {
            this.candidateReviewCriteria = { ...reviewCriteria };
          }
        );

        setTimeout(() => {
          this.candidateDetailsSub = this.gradeReviewService.candidateDetailsChanged.subscribe((details: any) => {
            if (details) {
              this.setDetails(details);
            }
          });
        }, 0);
      }
    });
  }

  setDetails(details) {
    details = {
      ...details,
      ...this.currentFilters,
      ...this.candidateReviewCriteria,
      candidate_id: this.candidateId,
      candidate_name: this.candidateName
    };

    this._questionBase.forEach(question => {
      if (
        question['key'] == 'forecast_deviation' ||
        question['key'] == 'component_deviation' ||
        question['key'] == 'mark_deviation'
      ) {
        question['value'] = details[question['key']] ? 'YES' : 'NO';
      } else {
        question['value'] = details[question['key']]
          ? details[question['key']]['value']
            ? details[question['key']]['value']
            : details[question['key']]
          : '';
      }
    });
    this.loading = false;
  }

  reset() {
    this.gradeReviewService.resetCandidateId();
    this.gradeReviewService.resetCandidateDetails();
    this.gradeReviewService.resetGradeReviewsList();
    this.gradeReviewService.resetCandidateReviewCriteria();
    this.gradeReviewService.resetFilters();
  }

  ngOnDestroy(): void {
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    if (this.candidateIdSub) {
      this.candidateIdSub.unsubscribe();
    }
    if (this.candidateNameSub) {
      this.candidateNameSub.unsubscribe();
    }
    if (this.candidateReviewCriteriaSub) {
      this.candidateReviewCriteriaSub.unsubscribe();
    }
    if (this.candidateDetailsSub) {
      this.candidateDetailsSub.unsubscribe();
    }

    this.reset();
    super.destroyPageBaseSub();
  }
}
