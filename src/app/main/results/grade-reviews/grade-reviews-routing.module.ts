import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GradeReviewsEntryComponent } from './grade-reviews-entry/grade-reviews-entry.component';
import { GradeReviewsListComponent } from './grade-reviews-entry/grade-reviews-list/grade-reviews-list.component';
import { GradeReviewsViewComponent } from './grade-reviews-entry/grade-reviews-view/grade-reviews-view.component';

const routes: Routes = [
  {
    path: '',
    component: GradeReviewsEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: GradeReviewsListComponent },
      { path: 'view', component: GradeReviewsViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradeReviewsRoutingModule {}
