import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  IPageheaderApi,
  IPageheaderConfig,
  IBreadcrumbConfig
} from 'openemis-styleguide-lib';

@Component({
  selector: 'app-final-grades-entry',
  templateUrl: './final-grades-entry.component.html',
  styleUrls: ['./final-grades-entry.component.css']
})
export class FinalGradesEntryComponent extends KdPageBase implements OnInit, OnDestroy {
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList: IBreadcrumbConfig = {};

  constructor(public pageEvent: KdPageBaseEvent, router: Router, activatedRoute: ActivatedRoute) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj: IBreadcrumbConfig) => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {}
}
