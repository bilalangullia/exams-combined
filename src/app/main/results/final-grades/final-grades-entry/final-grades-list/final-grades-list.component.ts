import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdFilter,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi,
  KdAlertEvent
} from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../shared/shared.interfaces';
import { FinalGradesService } from '../../final-grades.service';
import { IFilters } from '../../final-grades.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './final-grades-list.config';

import { ExcelExportParams } from 'ag-grid';
@Component({
  selector: 'app-final-grades-list',
  templateUrl: './final-grades-list.component.html',
  styleUrls: ['./final-grades-list.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent, KdIntTableEvent]
})
export class FinalGradesListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;
  public inputs: Array<any> = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public _row: Array<any>;
  public loading: boolean = true;
  public showTable: boolean = false;
  public _tableApi: ITableApi = {};
  private currentFilters: IFilters = null;

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.CandidateName,
    TABLECOLUMN.CreatedOn
  ];
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'normal',
    gridHeight: 'auto',
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            if (_rowNode && _rowNode['data'] && _rowNode['data']['candidate_id']) {
              this.finalGradesService.setCandidateId(_rowNode['data']['candidate_id']);
              setTimeout(() => {
                this.router.navigate(['main/results/final-grades/view']);
              }, 0);
            } else {
              let toasterConfig: IToasterConfig = {
                type: 'error',
                title: 'No Candidate ID',
                showCloseButton: true,
                tapToDismiss: true,
                timeout: 1000
              };
              this.sharedService.setToaster(toasterConfig);
            }
          }
        }
      ]
    }
  };
  private _tableEvent: KdTableEvent;

  /* Subscriptions */
  private otpionsAcademicPeriodSub: Subscription;
  private optionsExaminationSub: Subscription;
  private optionsExaminationCentreSub: Subscription;
  private currentFiltersSub: Subscription;
  private listSub: Subscription;
  private _tableSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdalert: KdAlertEvent,
    private sharedService: SharedService,
    private finalGradesService: FinalGradesService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
    super.setPageTitle('Final Grades', false);
    super.setToolbarMainBtns([
      {
        icon: 'fa fa-refresh',
        name: 'Generate',
        custom: true,
        callback: (_rowNode, _tableApi): void => {
          this.generateFinalGrades();
        }
      },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'finalGardesCandidates_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.finalGradesService.resetFilters();
    this.finalGradesService.getFilterYear();

    this.otpionsAcademicPeriodSub = this.finalGradesService.optionsAcademicPeriodChanged.subscribe(
      (options: Array<any>) => {
        this.setFilterOptions('academic_period_id', options);
        if (!options.length) {
          this.setFilterOptions('examination_id', []);
        }
      }
    );

    this.optionsExaminationSub = this.finalGradesService.optionsExaminationChanged.subscribe((options: Array<any>) => {
      this.setFilterOptions('examination_id', options);
      if (!options.length) {
        this.setFilterOptions('examination_centre_id', []);
      }
    });

    this.optionsExaminationCentreSub = this.finalGradesService.optionsExaminationCentreChanged.subscribe(
      (options: Array<any>) => {
        this.setFilterOptions('examination_centre_id', options);
      }
    );

    this.currentFiltersSub = this.finalGradesService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (
        filters &&
        filters['academic_period_id'] &&
        filters['academic_period_id']['key'] &&
        filters['examination_id'] &&
        filters['examination_id']['key'] &&
        filters['examination_centre_id'] &&
        filters['examination_centre_id']['key']
      ) {
        this.currentFilters = { ...filters };
        this.finalGradesService.getFinalGradesList(
          filters['academic_period_id']['key'],
          filters['examination_id']['key'],
          filters['examination_centre_id']['key']
        );
      }
    });

    this.listSub = this.finalGradesService.listChanged.subscribe((list: Array<any>) => {
      if (list['length']) {
        this.populateTable(list);
        this.loading = false;
      } else {
        this.showTable = false;
      }
    });
  }

  setFilterOptions(key: string, dropdownOptions: Array<any>) {
    if (dropdownOptions.length) {
      let options: Array<any> = [];
      if (key === 'academic_period_id') {
        options = dropdownOptions
          .map((item) => ({ key: item['id'], value: item['name'] }))
          .sort((a, b) => b['value'] - a['value']);
        this._updateView.setInputProperty(key, 'options', options);
        setTimeout(() => {
          this._updateView.setInputProperty(key, 'value', options[0]['key']);
        }, 0);
      } else if (key === 'examination_id') {
        options = dropdownOptions.map((item) => ({ key: item['id'], value: item['name'] }));
        this._updateView.setInputProperty(key, 'options', options);
        setTimeout(() => {
          this._updateView.setInputProperty(key, 'value', options[0]['key']);
        }, 0);
      } else if (key === 'examination_centre_id') {
        options = dropdownOptions.map((item) => ({ key: item['id'], value: `${item['code']} - ${item['name']}` }));
        this._updateView.setInputProperty(key, 'options', options);
        setTimeout(() => {
          this._updateView.setInputProperty(key, 'value', options[0]['key']);
        }, 0);
      }
    } else {
      this._updateView.setInputProperty(key, 'options', [{ key: null, value: '-- Select --' }]);
      this._updateView.setInputProperty(key, 'value', null);
    }
  }

  detectValue(question: any): void {
    let value = question['options'].find((item) => item['key'] == question['value']);

    if (question['key'] === 'academic_period_id' && question['value']) {
      this.finalGradesService.getFilterExam(question['value']);
    } else if (question['key'] === 'examination_id' && question['value']) {
      this.finalGradesService.getFilterExamCenter(question['value']);
      this.setFilterOptions('examination_centre_id', []);
      this.finalGradesService.setCurrentFilters({ examination_centre_id: null });
    }
    this.finalGradesService.setCurrentFilters({ [question['key']]: value });
  }

  resetFilterValues() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  populateTable(list: Array<any>) {
    this.resetFilterValues();
    if (list.length) {
      this._row = list;
      this._row.forEach((rowdata) => {
        if (TABLECOLUMN.CandidateName.filterValue.length == 0) {
          TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
          TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
          TABLECOLUMN.CreatedOn.filterValue.push(rowdata.created_on);
        } else {
          TABLECOLUMN.CandidateID.filterValue.indexOf(rowdata.candidate_id) > -1
            ? 'na'
            : TABLECOLUMN.CandidateID.filterValue.push(rowdata.candidate_id);
          TABLECOLUMN.CandidateName.filterValue.indexOf(rowdata.candidate_name) > -1
            ? 'na'
            : TABLECOLUMN.CandidateName.filterValue.push(rowdata.candidate_name);
          TABLECOLUMN.CreatedOn.filterValue.indexOf(rowdata.created_on) > -1
            ? 'na'
            : TABLECOLUMN.CreatedOn.filterValue.push(rowdata.created_on);
        }
      });
      this.showTable = true;
    }
  }

  searchList(keyword) {
    if (keyword && keyword.length > 2) {
      // this.loading = true;
      this.finalGradesService.searchFinalGradesList(
        this.currentFilters['academic_period_id']['key'],
        this.currentFilters['examination_id']['key'],
        this.currentFilters['examination_centre_id']['key'],
        keyword
      );
    } else if (!keyword) {
      // this.loading = true;
      this.finalGradesService.searchFinalGradesList(
        this.currentFilters['academic_period_id']['key'],
        this.currentFilters['examination_id']['key'],
        this.currentFilters['examination_centre_id']['key'],
        ''
      );
    }
  }

  generateFinalGrades() {
    this.loading = true;
    console.log('FinalGradesListComponent -> generateFinalGrades -> this.currentFilters', this.currentFilters);
    this.finalGradesService
      .generateFinalGrades(
        this.currentFilters['academic_period_id']['key'],
        this.currentFilters['examination_id']['key'],
        this.currentFilters['examination_id']['key']
      )
      .subscribe(
        (res: any) => {
          this.loading = false;
          if (res['data'] == true) {
            const successMessage: IToasterConfig = {
              type: 'success',
              title: 'Success',
              body: 'Grade Reviews Generated Successfully',
              timeout: 1000,
              tapToDismiss: true,
              showCloseButton: true
            };
            this.sharedService.setToaster(successMessage);
          } else {
            const failureMessage: IToasterConfig = {
              type: 'error',
              title: 'Failure',
              body: 'Error while generating Grade Reviews!',
              timeout: 1000,
              tapToDismiss: true,
              showCloseButton: true
            };
            this.sharedService.setToaster(failureMessage);
          }
        },
        (err) => {
          this.loading = false;
          const errorMessage: IToasterConfig = {
            type: 'error',
            title: 'Error',
            // body: res['message'],
            timeout: 1000,
            tapToDismiss: true,
            showCloseButton: true
          };
          this.sharedService.setToaster(errorMessage);
        }
      );
  }

  ngOnDestroy(): void {
    if (this.otpionsAcademicPeriodSub) {
      this.otpionsAcademicPeriodSub.unsubscribe();
    }
    if (this.optionsExaminationSub) {
      this.optionsExaminationSub.unsubscribe();
    }
    if (this.optionsExaminationCentreSub) {
      this.optionsExaminationCentreSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    if (this.listSub) {
      this.listSub.unsubscribe();
    }
    this.finalGradesService.resetFinalGradesList();

    super.destroyPageBaseSub();
  }
}
