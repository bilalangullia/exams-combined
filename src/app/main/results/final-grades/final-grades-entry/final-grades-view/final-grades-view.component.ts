import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdTableEvent, ITableApi } from 'openemis-styleguide-lib';

import { FinalGradesService } from '../../final-grades.service';
import { QUESTION_BASE } from './final-grades-view.config';

@Component({
  selector: 'app-final-grades-view',
  templateUrl: './final-grades-view.component.html',
  styleUrls: ['./final-grades-view.component.css']
})
export class FinalGradesViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public _questionBase: any = QUESTION_BASE;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  private tableApi: ITableApi = {};
  public showForm: boolean = false;

  /* Subscriptions */
  private candidateIdSub: Subscription;
  private candidateDetailsSub: Subscription;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private pageEvent: KdPageBaseEvent,
    private finalGradesService: FinalGradesService,
    private kdTableEvent: KdTableEvent
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Final Grades', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        path: 'main/results/final-grades',
        callback: () => {
          this.finalGradesService.resetCandidateId();
          this.finalGradesService.resetCandidateDetails();
          this.finalGradesService.resetFinalGradesList();
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    // this.kdTableEvent.onKdTableEventList(GRADES_TABLE_ID).subscribe((event: any): void => {
    //   console.log('WATCH FOR THIS EVENT!', event);
    // });

    this.loading = false;

    this.candidateIdSub = this.finalGradesService.selectedCandidateIdChanged.subscribe((candidateId: string) => {
      if (!candidateId) {
        this.router.navigate(['main/results/final-grades']);
      } else {
        this.finalGradesService.getCandidateDetails(candidateId);
        this.candidateDetailsSub = this.finalGradesService.candidateDetailsChanged.subscribe((details: any) => {
          if (details) {
            this.setDetails(details);
          }
        });
      }
    });
  }

  setDetails(details: any) {
    this._questionBase.forEach((question) => {
      if (question['key'] === 'final_grade') {
        let rows: Array<any> = [];
        details[question['key']].forEach((option: any) => {
          rows.push(option);
          let components: Array<any> = [];
          if (option['component'] && option['component'].length) {
            components = option['component'].map((item) => ({
              // option_code: item['component_code'].split('/')[0],
              component_code: item['component_code'],
              option_final_grade: item['component_final_grade']['name'],
              option_final_marks: item['component_final_marks'],
              option_id: item['component_id'],
              option_name: item['component_name'],
              option_points: item['component_points'],
              option_raw_marks: item['component_raw_marks'],
              option_weighting: item['component_weighting']
            }));
          }
          rows.push(...components);
        });
        this.api.setProperty(question['key'], 'row', rows);
      } else {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']]
            ? details[question['key']]['value']
              ? details[question['key']]['value']
              : details[question['key']]
            : ''
        );
      }
    });
  }

  reset() {
    this.finalGradesService.resetCandidateId();
    this.finalGradesService.resetCandidateDetails();
    this.finalGradesService.resetFinalGradesList();
  }

  ngOnDestroy(): void {
    if (this.candidateIdSub) {
      this.candidateIdSub.unsubscribe();
    }
    if (this.candidateDetailsSub) {
      this.candidateDetailsSub.unsubscribe();
    }
    this.reset();

    super.destroyPageBaseSub();
  }
}
