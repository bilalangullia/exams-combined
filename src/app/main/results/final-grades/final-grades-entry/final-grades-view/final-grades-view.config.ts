export const FINAL_GRADES_ID = 'finalGradesTable';

export const FINAL_GRADE_COLUMNS: Array<any> = [
  {
    headerName: 'Option Code',
    field: 'option_code',
    sortable: false,
    filterable: true,
    visible: true
  },
  {
    headerName: 'Component Code',
    field: 'component_code',
    sortable: false,
    filterable: true,
    visible: true
  },
  {
    headerName: 'Option Name',
    field: 'option_name',
    sortable: false,
    filterable: true,
    visible: true,
    class: 'ag-name'
  },
  {
    headerName: 'Raw Marks',
    field: 'option_raw_marks',
    sortable: false,
    filterable: true,
    visible: true,
    class: 'ag-name'
  },
  {
    headerName: 'Weighting',
    field: 'option_weighting',
    sortable: false,
    filterable: true,
    visible: true,
    class: 'ag-name'
  },
  {
    headerName: 'Final Marks',
    field: 'option_final_marks',
    sortable: false,
    filterable: true,
    visible: true,
    class: 'ag-name'
  },
  {
    headerName: 'Final Grade',
    field: 'option_final_grade',
    sortable: false,
    filterable: true,
    visible: true,
    class: 'ag-name'
  },
  { headerName: 'Points', field: 'option_points', sortable: false, filterable: true, visible: true, class: 'ag-name' }
];

export const QUESTION_BASE: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_centre_id',
    label: 'Exam Centre',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'candidate_name',
    label: 'Candidate Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'aggregate_score',
    label: 'Aggregate Score',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'final_grade',
    label: 'Final Grades',
    visible: true,
    controlType: 'table',
    row: [],
    column: FINAL_GRADE_COLUMNS,
    config: {
      id: FINAL_GRADES_ID,
      rowIdKey: 'id',
      gridHeight: 250,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];
