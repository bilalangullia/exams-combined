import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

import { DataService } from '../../../shared/data.service';
import { SharedService } from '../../../shared/shared.service';
import { IFilters } from './final-grades.interfaces';
import { FinalGradesDataService } from './final-grades-data.service';

@Injectable()
export class FinalGradesService {
  private optionsAcademicPeriod: any = null;
  private optionsExamination: any = null;
  private optionsExaminationCentre: any = null;
  private currentFilters: IFilters = null;
  private list: any = null;
  private selectedCandidateId: string = null;
  private candidateDetails: any = null;

  public optionsAcademicPeriodChanged = new Subject();
  public optionsExaminationChanged = new Subject();
  public optionsExaminationCentreChanged = new Subject();
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  public listChanged = new Subject();
  public selectedCandidateIdChanged = new BehaviorSubject<string>(this.selectedCandidateId);
  public candidateDetailsChanged = new BehaviorSubject<any>(this.candidateDetails);

  constructor(
    private router: Router,
    private dataService: DataService,
    private sharedService: SharedService,
    private finalGradesDataService: FinalGradesDataService
  ) {}

  /*** Grade Reviews | List Page |  Get Options for Academic Period Filter ***/
  getFilterYear() {
    this.dataService.getOptionsAcademicPeriod('academicPeriods').subscribe(
      (res: any) => {
        this.optionsAcademicPeriod = res['data']['academic_period_id'];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      },
      (err) => {
        this.optionsAcademicPeriod = [];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      }
    );
  }

  /*** Grade Reviews | List Page |  Get Options for Examination Filter ***/
  getFilterExam(academic_id) {
    this.dataService.getOptionsExam(academic_id).subscribe(
      (res: any) => {
        this.optionsExamination = res.data;
        this.optionsExaminationChanged.next([...this.optionsExamination]);
      },
      (err) => {
        this.optionsExamination = [];
        this.optionsExaminationChanged.next([...this.optionsExamination]);
      }
    );
  }

  /*** Grade Reviews | List Page |  Get Options for Examination Center Filter***/
  getFilterExamCenter(examination_id) {
    this.dataService.getOptionsExamCenter(examination_id).subscribe(
      (res: any) => {
        this.optionsExaminationCentre = res.data;
        this.optionsExaminationCentreChanged.next([...this.optionsExaminationCentre]);
      },
      (err) => {
        this.optionsExaminationCentre = [];
        this.optionsExaminationCentreChanged.next([...this.optionsExaminationCentre]);
      }
    );
  }

  /*** Grade Reviews | Reset Options for Filters ***/
  resetOptions() {
    this.optionsAcademicPeriod = [];
    this.optionsExamination = [];
    this.optionsExaminationCentre = [];
    this.optionsAcademicPeriodChanged.next([]);
    this.optionsExaminationChanged.next([]);
    this.optionsExaminationCentreChanged.next([]);
  }

  /*** Grade Reviews | Set Current Filter ***/
  setCurrentFilters(filter: IFilters) {
    !filter ? (this.currentFilters = null) : (this.currentFilters = { ...this.currentFilters, ...filter });
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Grade Reviews | Reset Filters ***/
  resetFilters() {
    this.resetOptions();
    this.setCurrentFilters(null);
  }

  /*** LIST PAGE | List Data ***/
  getFinalGradesList(academic_period_id, examination_id, examination_centre_id) {
    this.finalGradesDataService.getList(academic_period_id, examination_id, examination_centre_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.list = res.data;
          this.listChanged.next([...this.list]);
        } else {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      },
      (err) => {
        this.list = [];
        this.listChanged.next([...this.list]);
        let toasterConfig: any = {
          type: 'error',
          title: 'Error occurred while fetching list.',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  /*** List Page | Search List ***/
  searchFinalGradesList(academic_period_id, examination_id, examination_centre_id, keyword) {
    this.finalGradesDataService
      .searchList(academic_period_id, examination_id, examination_centre_id, keyword)
      .subscribe(
        (res: any) => {
          if (res && res['data'] && res['data']['length']) {
            this.list = res['data'];
            this.listChanged.next([...this.list]);
          } else {
            this.list = [];
            this.listChanged.next([...this.list]);
            let toasterConfig: any = {
              type: 'error',
              title: 'No Final Grades List Exists',
              body: 'Please select another values',
              showCloseButton: true,
              tapToDismiss: true,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        },
        (err) => {
          this.list = [];
          this.listChanged.next([...this.list]);
        }
      );
  }

  /*** Final Grades | Reset List */
  resetFinalGradesList() {
    this.list = [];
    this.listChanged.next([]);
  }

  /*** Set Candidate ID when navigating away from list page -> view/edit pages ***/
  setCandidateId(candidateId: string) {
    this.selectedCandidateId = candidateId;
    this.selectedCandidateIdChanged.next(this.selectedCandidateId);
  }

  /*** Set Candidate ID when navigating away from list page -> view/edit pages ***/
  resetCandidateId() {
    this.selectedCandidateId = null;
    this.selectedCandidateIdChanged.next(this.selectedCandidateId);
  }

  /*** Candidate  Details Data  ***/
  getCandidateDetails(candidate_id) {
    this.finalGradesDataService.getDetails(candidate_id).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.candidateDetails = res['data'][0];
          this.candidateDetailsChanged.next({ ...this.candidateDetails });
        } else {
          this.candidateDetails = null;
          this.candidateDetailsChanged.next(this.candidateDetails);
          let toasterConfig: any = {
            type: 'error',
            title: 'No candidate details found',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      },
      (err) => {
        this.candidateDetails = null;
        this.candidateDetailsChanged.next(this.candidateDetails);
      }
    );
  }

  resetCandidateDetails() {
    this.candidateDetails = null;
    this.candidateDetailsChanged.next(null);
  }

  generateFinalGrades(academicPeriodId: number, examinationId: number, examinationCentreId: number) {
    return this.finalGradesDataService.generateFinalGrades(academicPeriodId, examinationId, examinationCentreId);
  }
}
