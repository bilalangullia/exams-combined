import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { FinalGradesService } from './final-grades.service';
import { FinalGradesRoutingModule } from './final-grades-routing.module';
import { FinalGradesEntryComponent } from './final-grades-entry/final-grades-entry.component';
import { FinalGradesListComponent } from './final-grades-entry/final-grades-list/final-grades-list.component';
import { FinalGradesViewComponent } from './final-grades-entry/final-grades-view/final-grades-view.component';
import { FinalGradesDataService } from './final-grades-data.service';

@NgModule({
  imports: [CommonModule, FinalGradesRoutingModule, SharedModule],
  declarations: [FinalGradesEntryComponent, FinalGradesListComponent, FinalGradesViewComponent],
  providers: [FinalGradesService, FinalGradesDataService],
})
export class FinalGradesModule {}
