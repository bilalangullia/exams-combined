import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinalGradesEntryComponent } from './final-grades-entry/final-grades-entry.component';
import { FinalGradesListComponent } from './final-grades-entry/final-grades-list/final-grades-list.component';
import { FinalGradesViewComponent } from './final-grades-entry/final-grades-view/final-grades-view.component';

const routes: Routes = [
  {
    path: '',
    component: FinalGradesEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: FinalGradesListComponent },
      { path: 'view', component: FinalGradesViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinalGradesRoutingModule {}
