import { Component, OnInit, SimpleChanges } from "@angular/core";
import { LEFT_NAV } from "../shared/config.left-nav";
import {
  DEFAULT_TEMPLATE,
  updateTheme,
  DEFAULT_VAL
} from "../shared/config.default-val";
import { Router } from "@angular/router";
import { SharedService } from "../shared/shared.service";
import { DataService } from "../shared/data.service";
import { timer } from "rxjs";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {
  public _leftemenuList: any = LEFT_NAV;
  // public headerConfig: any = DEFAULT_TEMPLATE.header;
  public loading: boolean = true;
  public headerConfig: any = {
    brandLogo: {
      // prefix: 'Test',
      name: "OpenEMIS Exams",
      // type: 'icon',
      // src: 'fa kd-students',
      path: "/main"
      // url: 'http://www.yahoo.com.sg',
      // target: '_blank'
    },
    userInfo: {
      // showUser: false,
      name: "Christopher Leonardo Mark Damien",
      // imgType: 'image',
      // imgSrc: DEFAULT_VAL.user.imageData,
      imgType: "icon",
      imgSrc: "kd-role",
      role: "Head of Department, Teacher",
      path: "/"
    },
    btnGroup: [
      {
        btnIcon: "fa fa-home fa-lg",
        dropdownType: "default",
        path: "/"
      },
      {
        btnIcon: "kd-grid",
        dropdownType: "grid",
        dropdownContent: [
          {
            icon: "kd-openemis kd-analyzer",
            text: "Analyzer",
            theme: "openemis-analyzer",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-assessments",
            text: "Assessments",
            theme: "openemis-assessments",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-community",
            text: "Community",
            theme: "openemis-community",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-connect",
            text: "Connect",
            theme: "openemis-connect",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-construction",
            text: "Construction",
            theme: "openemis-construction",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-core",
            text: "Core",
            theme: "openemis-core",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-datamanager",
            text: "DataManager",
            theme: "openemis-datamanager",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-dashboard",
            text: "Dashboard",
            theme: "openemis-dashboard",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-identity",
            text: "Identity",
            theme: "openemis-identity",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-insight",
            text: "Insight",
            theme: "openemis-insight",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-integrator",
            text: "Integrator",
            theme: "openemis-integrator",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-learning",
            text: "Learning",
            theme: "openemis-learning",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-logistics",
            text: "Logistics",
            theme: "openemis-logistics",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-modelling",
            text: "Modelling",
            theme: "openemis-modelling",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-monitoring",
            text: "Monitoring",
            theme: "openemis-monitoring",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-school",
            text: "School",
            theme: "openemis-school",
            callback: (): void => {
              updateTheme();
            }
          },
          {
            icon: "kd-openemis kd-styleguide",
            text: "Styleguide",
            theme: "openemis-styleguide",
            callback: (): void => {
              updateTheme();
            }
          }
        ]
      },
      {
        btnIcon: "kd-ellipsis",
        dropdownType: "list",
        dropdownContent: [
          {
            icon: "fa fa-user",
            text: "About",
            path: "App/About"
          },
          {
            icon: "fa fa-cog",
            text: "Preferences"
          },
          {
            icon: "fa fa-question-circle",
            text: "Help",
            url: "https://www.openemis.org/support/"
          },
          {
            icon: "fa fa-power-off",
            text: "Logout",
            // path: 'Login',
            callback: (): void => {
              this.logout();
            }
          }
        ]
      }
    ]
  };

  constructor(
    public router: Router,
    private sharedService: SharedService,
    private dataService: DataService
  ) {
    this.getUsername();
  }

  ngOnInit() {}

  logout() {
    let temp = localStorage.getItem("token");
    if (temp) {
      localStorage.clear();
      this.router.navigate(["/auth"]);
    }
  }

  getUsername() {
    setTimeout(() => {
      this.loading = true;
      this.dataService.getCurrentUsername().subscribe(
        (res: any) => {
          if (res && res["data"]) {
            if (this.headerConfig.userInfo.name) {
              this.headerConfig.userInfo.name = res.data;
            }
          }
          this.loading = false;
        },
        err => {
          this.loading = false;
        }
      );
    }, 10);
  }
}
