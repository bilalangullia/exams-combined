import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [CommonModule, MainRoutingModule, SharedModule],
  declarations: [MainComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainModule {}
