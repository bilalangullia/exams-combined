import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { VerificationsRoutingModule } from './verifications-routing.module';
import { VerificationsService } from './verifications.service';
import { VerificationsEntryComponent } from './verifications-entry/verifications-entry.component';
import { VerificationsAddComponent } from './verifications-entry/verifications-add/verifications-add.component';
import { VerificationsEditComponent } from './verifications-entry/verifications-edit/verifications-edit.component';
import { VerificationsListComponent } from './verifications-entry/verifications-list/verifications-list.component';
import { VerificationsViewComponent } from './verifications-entry/verifications-view/verifications-view.component';
import { VerificationsDataService } from './verifications-data.service';

@NgModule({
  imports: [CommonModule, VerificationsRoutingModule, SharedModule],
  declarations: [
    VerificationsEntryComponent,
    VerificationsAddComponent,
    VerificationsEditComponent,
    VerificationsListComponent,
    VerificationsViewComponent
  ],
  providers: [VerificationsService, VerificationsDataService]
})
export class VerificationsModule {}
