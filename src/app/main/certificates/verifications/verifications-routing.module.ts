import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerificationsEntryComponent } from './verifications-entry/verifications-entry.component';
import { VerificationsListComponent } from './verifications-entry/verifications-list/verifications-list.component';
import { VerificationsAddComponent } from './verifications-entry/verifications-add/verifications-add.component';
import { VerificationsEditComponent } from './verifications-entry/verifications-edit/verifications-edit.component';
import { VerificationsViewComponent } from './verifications-entry/verifications-view/verifications-view.component';

const routes: Routes = [
  {
    path: '',
    component: VerificationsEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: VerificationsListComponent },
      { path: 'add', component: VerificationsAddComponent },
      { path: 'edit', component: VerificationsEditComponent },
      { path: 'view', component: VerificationsViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificationsRoutingModule {}
