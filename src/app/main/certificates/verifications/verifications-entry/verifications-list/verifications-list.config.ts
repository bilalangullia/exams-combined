interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  CandidateID?: Column;
  CandidateName?: Column;
  Examination?: Column;
  DateReceived?: Column;
  DateSent?: Column;
}

export const TABLECOLUMN: ListColumn = {
  CandidateID: {
    headerName: 'Candidate ID',
    field: 'candidate_id',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  CandidateName: {
    headerName: 'Candidate Name',
    field: 'candidate_name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Examination: {
    headerName: 'Examination',
    field: 'examination',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  DateReceived: {
    headerName: 'Date Received',
    field: 'date_received',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  DateSent: {
    headerName: 'Date Sent',
    field: 'date_sent',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};
