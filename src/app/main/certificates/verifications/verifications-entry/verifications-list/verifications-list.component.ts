import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter
} from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../shared/shared.service';
import { ExcelExportParams } from 'ag-grid';
import { TABLECOLUMN } from './verifications-list.config';

@Component({
  selector: 'app-verifications-list',
  templateUrl: './verifications-list.component.html',
  styleUrls: ['./verifications-list.component.css']
})
export class VerificationsListComponent extends KdPageBase implements OnInit, OnDestroy {
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public _row: Array<any>;
  public loading: Boolean = true;
  public _tableApi: ITableApi = {};

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.CandidateName,
    TABLECOLUMN.Examination,
    TABLECOLUMN.DateReceived,
    TABLECOLUMN.DateSent
  ];
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'oneshot',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.router.navigate(['main/certificates/verifications/view']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public _tableEvent: KdTableEvent,
    private sharedService: SharedService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.enableToolbarSearch(true, (event: any): void => {});
    super.setPageTitle('Certificates - Verifications', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/certificates/verifications/add' },
      { type: 'import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'verificationsCandidates_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;
  }

  populateTable(list: Array<any>) {
    this.resetFilter();
    if (list.length) {
      this.loading = false;
      this._row = list;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]['filterable']) {
          for (let j = 0; j < list.length; j++) {
            if (list[j][this.tableColumns[i]['field']] && list[j][this.tableColumns[i]['field']] != 'undefined') {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(list[j][this.tableColumns[i]['field']]);
              } else {
                this.tableColumns[i].filterValue.indexOf(list[j][this.tableColumns[i]['field']]) > -1
                  ? 'na'
                  : this.tableColumns[i].filterValue.push(list[j][this.tableColumns[i]['field']]);
              }
            }
          }
        }
      }
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
