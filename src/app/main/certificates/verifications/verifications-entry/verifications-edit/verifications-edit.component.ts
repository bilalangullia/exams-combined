import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { QUESTION_BASE, FORM_BUTTONS } from './verifications-edit.config';

@Component({
  selector: 'app-verifications-edit',
  templateUrl: './verifications-edit.component.html',
  styleUrls: ['./verifications-edit.component.css']
})
export class VerificationsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  constructor(public _pageEvent: KdPageBaseEvent, public _activatedRoute: ActivatedRoute, public _router: Router) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Certificates - Verifications', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;
  }

  submit(event) {
    console.log(event);
  }

  cancel() {
    this._router.navigate(['main/verifications/duplicates/view']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
