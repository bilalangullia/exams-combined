export const file_INPUT = {
  file_inputs: {
    key: 'attachment',
    label: 'Attachment',
    visible: true,
    required: true,
    controlType: 'file-input',
    type: 'file',
    config: {
      infoText: [
        {
          text: 'Format Supported: xls, xlsx, ods, zip '
        },
        {
          text: 'File size should not be larger than 512KB.'
        },
        {
          text: 'Recommended Maximum Records: 2000'
        }
      ]
    }
  }
};

export const QUESTION_BASE: Array<any> = [
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'gender',
    label: 'Gender',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'date_received',
    label: 'Date Received',
    visible: true,
    controlType: 'date',
    type: 'date',
    required: true
  },
  {
    key: 'date_sent',
    label: 'Date Sent',
    visible: true,
    controlType: 'date',
    type: 'date',
    required: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'organisation',
    label: 'Organisation',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    required: true
  },
  {
    key: 'address',
    label: 'Address',
    visible: true,
    controlType: 'textarea',
    type: 'text'
  },
  {
    key: 'comments',
    label: 'Comments',
    visible: true,
    controlType: 'textarea',
    type: 'text'
  },
  file_INPUT.file_inputs
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
