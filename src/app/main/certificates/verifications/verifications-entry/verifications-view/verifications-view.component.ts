import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';
import { VIEWNODE_INPUT } from './verifications-view.config';

@Component({
  selector: 'app-verifications-view',
  templateUrl: './verifications-view.component.html',
  styleUrls: ['./verifications-view.component.css']
})
export class VerificationsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  constructor(public _pageEvent: KdPageBaseEvent, public _activatedRoute: ActivatedRoute, public _router: Router) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Certificates - Verifications', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/certificates/verifications/list' },
      { type: 'edit', path: 'main/certificates/verifications/edit' }
    ]);
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
