import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificatesRoutingModule } from './certificates-routing.module';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
  imports: [CommonModule, CertificatesRoutingModule, SharedModule],
  declarations: []
})
export class CertificatesModule {}
