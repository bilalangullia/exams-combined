export interface IFilters {
  academic_period?: { key: number; value: string };
  examination?: { key: number; value: string };
}
