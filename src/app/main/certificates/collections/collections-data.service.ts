import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import urls from '../../../shared/config.urls';

@Injectable()
export class CollectionsDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /**  LIST API **/
  getList(academicPeriodId: number, examinationId: number, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.certificates}/${urls.collections}/${urls.getList}?academic_period_id=${academicPeriodId}&examination_id=${examinationId}&start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  LIST | SEARCH API  **/

  searchCollectionsList(
    keyword: string,
    academicPeriodId: number,
    examinationId: number,
    start: number = 0,
    end: number = 20
  ) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.certificates}/${urls.collections}/${urls.getList}?academic_period_id=${academicPeriodId}&examination_id=${examinationId}&start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** ADD | Autocomplete Canidate ID ***/
  getCandidateId(keyword: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.autocomplete}-${urls.candidate}-id/${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** ADD | Canidate Details***/
  getDetailsByCandidateId(candidateId: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${candidateId}/details-by-id`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** Get Details | VIEW | API  **/
  getCandidateDetails(candidate_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.certificates}/${urls.collections}/get-detail/${candidate_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** EDIT | SUBMIT API  **/
  updateCandidateDetails(candidate_id, data) {
    return this.httpClient
      .put(
        `${environment.baseUrl}/${urls.certificates}/${urls.collections}/update-collection/${candidate_id}`,
        data,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** ADD | SUBMIT API  **/
  addCandidateDetails(data) {
    return this.httpClient
      .post(`${environment.baseUrl}/${urls.certificates}/${urls.collections}/add-collection`, data, this.setHeader())
      .pipe(catchError(this.handleError));
  }
  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
