import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

import { DataService } from '../../../shared/data.service';
import { SharedService } from '../../../shared/shared.service';
import { CollectionsDataService } from './collections-data.service';
import { IFilters } from './collections.interfaces';

@Injectable()
export class CollectionsService {
  public candidateViewDetails: any = [];
  public candidateViewDetailsChanged = new BehaviorSubject<any>(this.candidateViewDetails);
  private filterYear: any;
  public filterYearChanged = new Subject();
  private filterExam: any;
  public filterExamChanged = new Subject();
  private currentFilters: any = {};
  public currentFiltersChanged = new BehaviorSubject<any>(this.currentFilters);
  private collectionId: string = null;
  public collectionIdChanged = new BehaviorSubject<string>(null);

  private list: Array<any> = [];
  public listChanged = new BehaviorSubject<Array<any>>(this.list);
  constructor(
    private router: Router,
    private dataService: DataService,
    private collectionsDataService: CollectionsDataService,
    private sharedService: SharedService
  ) {}

  /*** LIST PAGE | FILTERS | Year***/
  getFilterYear() {
    let data = 'academicPeriods';
    this.dataService.getOptionsAcademicPeriod(data).subscribe((res: any) => {
      this.filterYear = res.data.academic_period_id;
      this.filterYearChanged.next([...this.filterYear]);
      (err) => {
        this.filterYear = [];
        this.filterYearChanged.next([...this.filterYear]);
      };
    });
  }

  /*** LIST | Filters Examination Options ***/
  getFilterExamination(academicPeriodId: number) {
    this.dataService.getOptionsExam(academicPeriodId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.filterExam = res.data;
          this.filterExamChanged.next([...this.filterExam]);
        } else {
          this.filterExam = [];
          this.filterExamChanged.next([...this.filterExam]);
        }
      },
      (err) => {
        this.filterExam = [];
        this.filterExamChanged.next([...this.filterExam]);
      }
    );
  }

  /*** Set Current filters ***/
  setCurrentFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }

  /*** LIST PAGE | List Data get total Records ***/
  getTotalRecords(academicPeriodId: number, examinationId: number, start?: number, end?: number) {
    return this.collectionsDataService.getList(academicPeriodId, examinationId, start, end);
  }

  /*** LIST PAGE | List Data ***/
  getList(academicPeriodId: number, examinationId: number, start?: number, end?: number) {
    return this.collectionsDataService.getList(academicPeriodId, examinationId, start, end);
  }

  /*** List Data | SEARCH***/
  searchList(keyword: string, academicPeriodId: number, examinationId: number, start?: number, end?: number) {
    return this.collectionsDataService.searchCollectionsList(keyword, academicPeriodId, examinationId, start, end);
  }

  /*** List Page | Set Collection Id***/
  setCollectionId(collectionId: string) {
    this.collectionId = collectionId;
    this.collectionIdChanged.next(this.collectionId);
  }

  /*** Candidate  Details Data  ***/
  getCandidateDetails(candidate_id) {
    this.collectionsDataService.getCandidateDetails(candidate_id).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.candidateViewDetails = res.data;
          this.candidateViewDetailsChanged.next([...this.candidateViewDetails]);
        } else {
          this.candidateViewDetails = [];
          this.candidateViewDetailsChanged.next([...this.candidateViewDetails]);
          let toasterConfig: any = {
            type: 'error',
            title: 'No candidate details found',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      },
      (err) => {
        this.candidateViewDetails = [];
        this.candidateViewDetailsChanged.next([...this.candidateViewDetails]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No candidate details found',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  /*** EDIT PAGE SUBMIT  ***/
  updateCandidateDetail(candidate_id, data) {
    this.collectionsDataService.updateCandidateDetails(candidate_id, data).subscribe(
      (data: any) => {
        let toasterConfig: any = {
          type: 'success',
          title: 'Candidate Details updated successfully',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/certificates/collections/list']);
      },
      (err) => {
        let toasterConfig: any = {
          type: 'error',
          title: 'Something Went Wrong',
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  /*** ADD PAGE AUTO COMPLETE SEARCH  ***/
  getAutocompleteCandidateId(keyword: string) {
    return this.collectionsDataService.getCandidateId(keyword);
  }

  /*** ADD PAGE GET DETAILS BY ID  ***/
  getDetailsByCandidateId(candidateId: string) {
    return this.collectionsDataService.getDetailsByCandidateId(candidateId);
  }

  /*** ADD PAGE SUBMIT  ***/
  addCandidateDetail(data) {
    this.collectionsDataService.addCandidateDetails(data).subscribe(
      (data: any) => {
        let toasterConfig: any = {
          type: 'success',
          title: 'Candidate Details added successfully',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['main/certificates/collections/list']);
      },
      (err) => {
        let toasterConfig: any = {
          type: 'error',
          title: err['error']['message'],
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }
}
