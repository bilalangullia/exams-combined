import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { CollectionsRoutingModule } from './collections-routing.module';
import { CollectionsService } from './collections.service';
import { CollectionsEntryComponent } from './collections-entry/collections-entry.component';
import { CollectionsListComponent } from './collections-entry/collections-list/collections-list.component';
import { CollectionsViewComponent } from './collections-entry/collections-view/collections-view.component';
import { CollectionsAddComponent } from './collections-entry/collections-add/collections-add.component';
import { CollectionsEditComponent } from './collections-entry/collections-edit/collections-edit.component';
import { CollectionsDataService } from './collections-data.service';

@NgModule({
  imports: [CommonModule, CollectionsRoutingModule, SharedModule],
  declarations: [
    CollectionsEntryComponent,
    CollectionsListComponent,
    CollectionsViewComponent,
    CollectionsAddComponent,
    CollectionsEditComponent
  ],
  providers: [CollectionsService, CollectionsDataService]
})
export class CollectionsModule {}
