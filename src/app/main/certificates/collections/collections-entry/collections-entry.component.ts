import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, IPageheaderApi, IPageheaderConfig } from 'openemis-styleguide-lib';
@Component({
  selector: 'app-collections-entry',
  templateUrl: './collections-entry.component.html',
  styleUrls: ['./collections-entry.component.css']
})
export class CollectionsEntryComponent extends KdPageBase implements OnInit, OnDestroy {
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = {};

  constructor(public pageEvent: KdPageBaseEvent, router: Router, activatedRoute: ActivatedRoute) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe(_breadcrumbObj => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {}
}
