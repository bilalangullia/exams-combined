import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';
import { CollectionsService } from '../../collections.service';
import { VIEWNODE_INPUT } from './collections-view.config';
@Component({
  selector: 'app-collections-view',
  templateUrl: './collections-view.component.html',
  styleUrls: ['./collections-view.component.css']
})
export class CollectionsViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public candidateId: any;

  /* Subscriptions */
  private candidateIdSub: Subscription;
  private candidateDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private collectionService: CollectionsService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Certificates - Collections', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/certificates/collections/list' },
      { type: 'edit', path: 'main/certificates/collections/edit' }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.candidateIdSub = this.collectionService.collectionIdChanged.subscribe((data) => {
      if (data) {
        this.candidateId = data;
        this.collectionService.getCandidateDetails(this.candidateId);
        this.candidateDetailsSub = this.collectionService.candidateViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.setCandidateDetails(data[0]);
          }
        });
      } else {
        this._router.navigate(['main/certificates/collections/list']);
      }
    });
  }

  setCandidateDetails(data: any) {
    if (data) {
      this.loading = false;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.candidateIdSub) {
      this.candidateIdSub.unsubscribe();
    }
    if (this.candidateDetailsSub) {
      this.candidateDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
