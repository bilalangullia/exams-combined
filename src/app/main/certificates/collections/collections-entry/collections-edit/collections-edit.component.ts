import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../shared/shared.service';
import { CollectionsService } from '../../collections.service';
import { QUESTION_BASE, FORM_BUTTONS } from './collections-edit.config';
@Component({
  selector: 'app-collections-edit',
  templateUrl: './collections-edit.component.html',
  styleUrls: ['./collections-edit.component.css']
})
export class CollectionsEditComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public candidateId: any;
  public collectionId: any;

  /* Subscriptions */
  private candidateIdSub: Subscription;
  private candidateDetailsSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private sharedService: SharedService,
    private collectionsService: CollectionsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Certificates - Collections', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.candidateIdSub = this.collectionsService.collectionIdChanged.subscribe((data) => {
      if (data) {
        this.candidateId = data;
        this.collectionsService.getCandidateDetails(this.candidateId);
        this.candidateDetailsSub = this.collectionsService.candidateViewDetailsChanged.subscribe((data: any) => {
          if (data) {
            this.collectionId = data[0].collection_id;
            this.setCandidateDetails(data[0]);
          }
        });
      } else {
        this._router.navigate(['main/certificates/collections/list']);
      }
    });
  }

  setCandidateDetails(data: any) {
    this.loading = false;
    timer(100).subscribe(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'method') {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i]['key']]['key']);
        } else {
          this.api.setProperty(
            this._questionBase[i].key,
            'value',
            !data[this._questionBase[i].key]
              ? ''
              : data[this._questionBase[i].key].value
              ? data[this._questionBase[i].key].value
              : data[this._questionBase[i].key]
          );
        }
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields',
        tapToDismiss: true,
        showCloseButton: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = {
        candidate_id: event.candidate_id,
        date_received: event.date_received.text,
        method: event.method,
        date_sent: event.date_sent.text,
        postal: event.postal_address,
        comments: event.comments
      };
      this.collectionsService.updateCandidateDetail(this.collectionId, payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  cancel() {
    this._router.navigate(['main/certificates/collections/view']);
  }

  ngOnDestroy(): void {
    if (this.candidateIdSub) {
      this.candidateIdSub.unsubscribe();
    }
    if (this.candidateDetailsSub) {
      this.candidateDetailsSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
