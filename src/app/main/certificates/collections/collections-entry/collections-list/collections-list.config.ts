interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  CandidateID?: Column;
  CandidateName?: Column;
  CertificateNumber: Column;
  Examination?: Column;
  DateReceived?: Column;
  Method?: Column;
  DateSent?: Column;
}

export const TABLECOLUMN: ListColumn = {
  CandidateID: {
    headerName: 'Candidate ID',
    field: 'candidate_id',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  CandidateName: {
    headerName: 'Candidate Name',
    field: 'candidate_name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Examination: {
    headerName: 'Examination',
    field: 'examination',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  CertificateNumber: {
    headerName: 'Certificate Number',
    field: 'certificate_number',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  DateReceived: {
    headerName: 'Date Received',
    field: 'date_received',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Method: {
    headerName: 'Method',
    field: 'method',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  DateSent: {
    headerName: 'Date Sent',
    field: 'date_sent',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  }
];
