import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdFilter,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';
import { IFetchListParams } from '../../../../../shared/shared.interfaces';
import { SharedService } from '../../../../../shared/shared.service';
import { CollectionsService } from '../../collections.service';
import { IFilters } from '../../collections.interfaces';
import { FILTER_INPUTS, TABLECOLUMN } from './collections-list.config';
import { ExcelExportParams } from 'ag-grid';

@Component({
  selector: 'app-collections-list',
  templateUrl: './collections-list.component.html',
  styleUrls: ['./collections-list.component.css'],
  providers: [KdTableEvent]
})
export class CollectionsListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('filters') filter: KdFilter;
  public filterInputs = FILTER_INPUTS;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'collectionList';
  readonly PAGESIZE: number = 20;

  public loading: Boolean = true;
  public _tableApi: ITableApi = {};
  public currentFilters: IFilters;
  public showTable: boolean = false;
  private tableEventList: any;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  /* Subscriptions */
  private tableEventSub: Subscription;
  private yearFilterSub: Subscription;
  private examFilterSub: Subscription;
  private currentFilterSub: Subscription;

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.CandidateName,
    TABLECOLUMN.Examination,
    TABLECOLUMN.CertificateNumber,
    TABLECOLUMN.DateReceived,
    TABLECOLUMN.Method,
    TABLECOLUMN.DateSent
  ];

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGESIZE, total: this.TOTALROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.collectionsService.setCollectionId(_rowNode['data']['collection_id']);
            this.router.navigate(['main/certificates/collections/view']);
          }
        }
      ]
    }
  };

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public tableEvent: KdTableEvent,
    private sharedService: SharedService,
    private collectionsService: CollectionsService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.setPageTitle('Certificates - Collections', false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/certificates/collections/add' },
      { type: 'import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'collectionsCandidates_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.collectionsService.resetFilters();

    this.collectionsService.getFilterYear();
    this.yearFilterSub = this.collectionsService.filterYearChanged.subscribe((options: Array<any>) => {
      if (options && options.length) {
        this.setDropdownOptions('academic_period', options);
      }
    });

    this.examFilterSub = this.collectionsService.filterExamChanged.subscribe((options: Array<any>) => {
      if (options && options.length) {
        this.setDropdownOptions('examination', options);
      } else {
        this.setDropdownOptions('examination', [{ id: null, name: '--select--' }]);
      }
    });

    this.currentFilterSub = this.collectionsService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (
        filters &&
        filters['academic_period'] &&
        filters['academic_period']['key'] &&
        filters['examination'] &&
        filters['examination']['key']
      ) {
        this.currentFilters = { ...filters };
        this.collectionsService
          .getTotalRecords(filters['academic_period']['key'], filters['examination']['key'], 0, 1)
          .subscribe(
            (res: any) => {
              if (res && res['data'] && res['data']['total']) {
                this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
                this.showTable = true;
              } else {
                this.tableConfig.paginationConfig.total = 0;
                this.showTable = false;
              }
            },
            (err) => {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          );
      }
    });

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<{ key: number; value: string }> = [];
    if (data.length) {
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
      this.filter.setInputProperty(key, 'options', options);
      setTimeout(() => {
        this.filter.setInputProperty(key, 'value', options[0]['key']);
      }, 0);
    }
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      if (
        this.currentFilters &&
        this.currentFilters['academic_period'] &&
        this.currentFilters['academic_period']['key'] &&
        this.currentFilters['examination'] &&
        this.currentFilters['examination']['key']
      ) {
        listReq = this.collectionsService.searchList(
          searchKey,
          this.currentFilters['academic_period']['key'],
          this.currentFilters['examination']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    } else {
      if (
        this.currentFilters &&
        this.currentFilters['academic_period'] &&
        this.currentFilters['academic_period']['key'] &&
        this.currentFilters['examination'] &&
        this.currentFilters['examination']['key']
      ) {
        listReq = this.collectionsService.getList(
          this.currentFilters['academic_period']['key'],
          this.currentFilters['examination']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    }
    listReq.toPromise().then(
      (res: any) => {
        this.loading = false;
        if (res && res['data'] && res['data']['collectData'] && res['data']['collectData'].length) {
          list = res['data']['collectData'];
          total = this.searchKey && this.searchKey.length ? res['data']['collectData'].length : res['data']['total'];
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  detectValue(event) {
    if (event['key'] === 'academic_period') {
      this.collectionsService.getFilterExamination(event['value']);
    }
    let filter: IFilters;
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj) {
      filter = { [event['key']]: filterObj };
      this.collectionsService.setCurrentFilters(filter);
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]['filterable']) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.yearFilterSub) {
      this.yearFilterSub.unsubscribe();
    }
    if (this.examFilterSub) {
      this.examFilterSub.unsubscribe();
    }
    if (this.currentFilterSub) {
      this.currentFilterSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    super.destroyPageBaseSub();
  }
}
