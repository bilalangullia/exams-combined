export const QUESTION_BASE: Array<any> = [
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'autocomplete',
    autocomplete: true,
    clickToggleDropdown: true,
    placeholder: 'Search Candidate ID',
    onEnter: true,
    required: true
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'certificate_number',
    label: 'Certificate Number',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'last_name',
    label: 'Last Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'gender_id',
    label: 'Gender',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string',
    readonly: true
  },
  {
    key: 'date_received',
    label: 'Date Received',
    visible: true,
    controlType: 'date',
    type: 'date',
    required: true
  },
  {
    key: 'method',
    label: 'Method',
    visible: true,
    controlType: 'dropdown',
    options: [
      { key: null, value: '--select--' },
      { key: 1, value: 'Collect' },
      { key: 2, value: 'Post' }
    ],
    events: true,
    required: true
  },
  {
    key: 'date_sent',
    label: 'Date Sent',
    visible: true,
    controlType: 'date',
    type: 'date'
  },
  {
    key: 'postal_address',
    label: 'Postal Address',
    visible: true,
    controlType: 'textarea',
    type: 'text'
  },
  {
    key: 'comments',
    label: 'Comments',
    visible: true,
    controlType: 'textarea',
    type: 'text'
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
