import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer, Observable, Subscriber } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdView } from 'openemis-styleguide-lib';
import { SharedService } from '../../../../../shared/shared.service';
import { CollectionsService } from '../../collections.service';
import { QUESTION_BASE, FORM_BUTTONS } from './collections-add.config';

@Component({
  selector: 'app-collections-add',
  templateUrl: './collections-add.component.html',
  styleUrls: ['./collections-add.component.css']
})
export class CollectionsAddComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdView;
  public _questionBase: any = QUESTION_BASE;
  public _formButtons: any = FORM_BUTTONS;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  public candidateId: any;
  public certificateId: any;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    private sharedService: SharedService,
    private collectionsService: CollectionsService
  ) {
    super({ router: _router, activatedRoute: _activatedRoute, pageEvent: _pageEvent });
    super.setPageTitle('Certificates - Collections', false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;
    this.setOptions();
  }

  setOptions() {
    this._questionBase.forEach((question) => {
      if (question['key'] === 'candidate_id') {
        question['dropdownCallback'] = this.searchCandidate.bind(this);
      }
    });
    this.loading = false;
  }

  searchCandidate(params: any): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data: Array<any> = [];
      if (params['query'].length) {
        this.collectionsService.getAutocompleteCandidateId(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'];
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
  }

  submit(event) {
    if (this.requiredCheck(event)) {
      let toasterConfig: any = {
        type: 'error',
        title: 'Missing Required Fields',
        tapToDismiss: true,
        showCloseButton: true,
        timeout: 1000
      };
      this.sharedService.setToaster(toasterConfig);
    } else {
      let payload = {
        candidate_id: event.candidate_id['value'],
        date_received: event.date_received.text,
        date_sent: event.date_sent.text,
        method: event.method,
        examination_students_certificates_id: this.certificateId,
        postal: event.postal_address,
        comments: event.comments
      };
      this.collectionsService.addCandidateDetail(payload);
    }
  }

  requiredCheck(formValue: any) {
    let hasError: boolean = false;
    this._questionBase.forEach((question) => {
      if (question['required']) {
        if (!formValue[question['key']]) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(question['key'], 'errors', ['This field is required']);
          }, 0);
        }
      }
    });
    return hasError;
  }

  detectValue(event) {
    if (event['key'] === 'candidate_id' && event['value'] && event['value']['value']) {
      this.candidateId = event['value']['value'];
      timer(100).subscribe((): void => {
        this.collectionsService.getDetailsByCandidateId(event['value']['value']).subscribe((res: any) => {
          if (res && res['data'] && res['data'].length) {
            this.setCandidateDetails(res['data'][0]);
          }
        });
      });
    }
  }

  setCandidateDetails(details: any) {
    this._questionBase.forEach((question: any) => {
      if (question['key'] === 'candidate_id') {
      } else if (question['controlType'] === 'dropdown') {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']] ? details[question['key']]['key'] : null
        );
      } else if (question['controlType'] === 'date' || question['key'] === 'date_of_birth') {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']] ? details[question['key']].split('T')[0] : ''
        );
      } else if (question['key'] === 'examination_id' || question['key'] === 'gender_id') {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']] ? details[question['key']]['value'] : ''
        );
      } else if (question['key'] === 'certificate_number') {
        this.certificateId = details[question['key']]['key'];
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']] ? details[question['key']]['value'] : ''
        );
      } else {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']]
            ? details[question['key']]['key']
              ? details[question['key']]['key']
              : details[question['key']]
            : ''
        );
      }
    });
  }

  cancel() {
    this._router.navigate(['main/certificates/collections/list']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
