import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollectionsEntryComponent } from './collections-entry/collections-entry.component';
import { CollectionsListComponent } from './collections-entry/collections-list/collections-list.component';
import { CollectionsAddComponent } from './collections-entry/collections-add/collections-add.component';
import { CollectionsEditComponent } from './collections-entry/collections-edit/collections-edit.component';
import { CollectionsViewComponent } from './collections-entry/collections-view/collections-view.component';

const routes: Routes = [
  {
    path: '',
    component: CollectionsEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: CollectionsListComponent },
      { path: 'add', component: CollectionsAddComponent },
      { path: 'edit', component: CollectionsEditComponent },
      { path: 'view', component: CollectionsViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollectionsRoutingModule {}
