import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'collections', pathMatch: 'full' },
  { path: 'collections', loadChildren: './collections/collections.module#CollectionsModule' },
  { path: 'duplicates', loadChildren: './duplicates/duplicates.module#DuplicatesModule' },
  { path: 'verifications', loadChildren: './verifications/verifications.module#VerificationsModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificatesRoutingModule {}
