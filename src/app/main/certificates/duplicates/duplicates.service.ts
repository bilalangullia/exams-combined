import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { SharedService } from '../../../shared/shared.service';
import { DuplicatesDataService } from './duplicates-data.service';
import { DataService } from '../../../shared/data.service';
import { IFilters } from './duplicates.interfaces';

@Injectable()
export class DuplicatesService {
  private optionsAcademicPeriod: Array<any> = [];
  private optionsExamination: Array<any> = [];
  private currentFilters: IFilters = null;
  private list: Array<any> = [];
  private duplicateId: string = null;
  private details: any = null;

  public optionsAcademicPeriodChanged = new BehaviorSubject<Array<any>>([]);
  public optionsExaminationChanged = new BehaviorSubject<Array<any>>([]);
  public currentFiltersChanged = new BehaviorSubject<IFilters>(null);
  public listChanged = new BehaviorSubject<Array<any>>([]);
  public duplicateIdChanged = new BehaviorSubject<string>(null);
  public detailsChanged = new BehaviorSubject<any>(null);

  public candidateEditDetails: any = [];
  public candidateEditDetailsChanged = new BehaviorSubject<any>(this.candidateEditDetails);

  constructor(
    private router: Router,
    private dataService: DataService,
    private sharedService: SharedService,
    private duplicatesDataService: DuplicatesDataService
  ) {}

  /*** LIST | Filters Academic Period Options ***/
  getOptionsAcademicPeriod() {
    this.dataService.getOptionsAcademicPeriod('academicPeriods').subscribe(
      (res: any) => {
        this.optionsAcademicPeriod = res['data']['academic_period_id'];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      },
      (err) => {
        this.optionsAcademicPeriod = [];
        this.optionsAcademicPeriodChanged.next([...this.optionsAcademicPeriod]);
      }
    );
  }

  /*** LIST | Filters Examination Options ***/
  getOptionsExamination(academicPeriodId: number) {
    this.dataService.getOptionsExam(academicPeriodId).subscribe(
      (res: any) => {
        this.optionsExamination = res.data;
        this.optionsExaminationChanged.next([...this.optionsExamination]);
      },
      (err) => {
        this.optionsExamination = [];
        this.optionsExaminationChanged.next([...this.optionsExamination]);
      }
    );
  }

  setFilters(filter: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filter };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  resetFilters() {
    this.currentFilters = null;
    this.currentFiltersChanged.next(null);
  }

  getTotalRecords(academicPeriodId: number, examinationId: number, start: number = 0, end: number = 1) {
    return this.duplicatesDataService.getList(academicPeriodId, examinationId, start, end);
  }

  /*** LIST PAGE | List Data ***/
  getList(academicPeriodId: number, examinationId: number, start: number, end: number) {
    return this.duplicatesDataService.getList(academicPeriodId, examinationId, start, end);
  }

  /*** List Data | SEARCH***/
  searchList(examinationId, keyword, start, end) {
    return this.duplicatesDataService.getListSearch(examinationId, keyword, start, end);
  }

  resetList() {
    this.list = [];
    this.listChanged.next([]);
  }

  getAutocompleteCandiateId(keyword: string) {
    return this.duplicatesDataService.getCandidateId(keyword);
  }

  getDetailsByCandidateId(candidateId: string) {
    return this.duplicatesDataService.getDetailsByCandidateId(candidateId);
  }

  addDuplicate(payload: any) {
    return this.duplicatesDataService.addDuplicate(payload);
  }

  setDuplicateId(duplicateId: string) {
    this.duplicateId = duplicateId;
    this.duplicateIdChanged.next(this.duplicateId);
  }

  resetDuplicateId() {
    this.duplicateId = null;
    this.duplicateIdChanged.next(null);
  }

  /*** Candidate  Details Data  ***/
  getDuplicateDetails(duplicateId) {
    this.duplicatesDataService.getDetails(duplicateId).subscribe(
      (res: any) => {
        if (res.data.length) {
          this.details = res['data'][0];
          this.detailsChanged.next({ ...this.details });
        } else {
          this.details = [];
          this.detailsChanged.next({ ...this.details });
          let toasterConfig: any = {
            type: 'error',
            title: 'No candidate details found',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 1000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      },
      (err) => {
        this.details = [];
        this.detailsChanged.next([...this.details]);
        let toasterConfig: any = {
          type: 'error',
          title: 'No candidate details found',
          showCloseButton: true,
          tapToDismiss: true,
          timeout: 1000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    );
  }

  resetDuplicateDetails() {
    this.details = null;
    this.detailsChanged.next(null);
  }

  /*** EDIT PAGE SUBMIT  ***/
  updateDetails(duplicate, data) {
    return this.duplicatesDataService.updateDetails(duplicate, data);
  }
}
