export interface IFilters {
  academic_period?: { key: number; value: string };
  examination?: { key: number; value: string };
}

export interface IAddPayload {
  candidate_id: string;
  date_received: string;
  certificate_number?: string;
  method: number;
  date_sent?: string;
  postal?: string;
  comments?: string;
}

export interface IUpdatePayload {
  candidate_id: string;
  date_received: string;
  method: number;
  date_sent?: string;
  // certificate_number: string;
  postal?: string;
  comments?: string;
}
