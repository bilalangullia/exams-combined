import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../shared/shared.interfaces';
import { DuplicatesService } from '../../duplicates.service';
import { IUpdatePayload } from '../../duplicates.interfaces';
import { QUESTION_BASE, FORM_BUTTONS, METHOD_OPTIONS } from './duplicates-edit.config';

@Component({
  selector: 'app-duplicates-edit',
  templateUrl: './duplicates-edit.component.html',
  styleUrls: ['./duplicates-edit.component.css']
})
export class DuplicatesEditComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Certificates - Duplicates';

  public loading: boolean = true;
  public showForm: boolean = false;
  public _smallLoaderValue: number = null;
  public questionBase: any = QUESTION_BASE;
  public formButtons: any = FORM_BUTTONS;
  public api: IDynamicFormApi = {};

  private duplicateId: string = null;

  /* Subscriptions */
  private duplicateIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private duplicatesService: DuplicatesService,
    private sharedService: SharedService
  ) {
    super({ router: router, activatedRoute: activatedRoute, pageEvent: pageEvent });
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.duplicateIdSub = this.duplicatesService.duplicateIdChanged.subscribe((duplicateId: string) => {
      if (duplicateId) {
        this.duplicateId = duplicateId;
        this.duplicatesService.getDuplicateDetails(duplicateId);
      } else {
        this.router.navigate(['main/certificates/duplicates']);
      }
    });

    this.detailsSub = this.duplicatesService.detailsChanged.subscribe((details: any) => {
      if (details) {
        this.setDetails(details);
      }
    });
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      if (question['key'] === 'date_of_birth') {
        question['value'] = details[question['key']] ? details[question['key']].split('T')[0] : '';
      } else if (question['key'] === 'method') {
        question['value'] = details[question['key']]
          ? METHOD_OPTIONS.find((item) => item['value'] === details[question['key']])['key']
          : '';
      } else {
        question['value'] = details[question['key']] ? details[question['key']] : '';
      }
    });

    setTimeout(() => {
      this.showForm = true;
    }, 100);
  }

  requiredCheck(form: any) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && (!form[question['key']] || form[question['key']] == 'null')) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This field is required.']);
      }
    });
    return hasError;
  }

  submit(event) {
    let form: IUpdatePayload;
    let badReceivedDate: boolean = false;
    let badSentDate: boolean = false;

    if (
      event['date_received'] &&
      event['date_received']['text'] &&
      event['date_received']['text'].split('-').find((item) => item == 'undefined')
    ) {
      badReceivedDate = true;
    }

    if (
      event['date_sent'] &&
      event['date_sent']['text'] &&
      event['date_sent']['text'].split('-').find((item) => item == 'undefined')
    ) {
      badSentDate = true;
    }

    form = {
      date_received: badReceivedDate ? null : event['date_received']['text'],
      method: event['method'],
      date_sent: badSentDate ? null : event['date_sent']['text'],
      postal: event['postal'],
      comments: event['comments'],
      candidate_id: event['candidate_id']
    };

    if (this.requiredCheck(form)) {
      let warnMessage: IToasterConfig = { title: 'Missing Required Fields', timeout: 2000, type: 'error' };
      this.sharedService.setToaster(warnMessage);
    } else {
      let payload = { ...form };
      this.duplicatesService.updateDetails(this.duplicateId, payload).subscribe(
        (res: any) => {
          console.log(res);
          let successMessage: IToasterConfig = { title: 'Saved Successfully', timeout: 1000, type: 'success' };
          this.sharedService.setToaster(successMessage);
          setTimeout(() => {
            this.cancel();
          }, 100);
        },
        (err) => {
          let errorMessage: IToasterConfig = { title: 'Error while saving.', timeout: 1000, type: 'error' };
          this.sharedService.setToaster(errorMessage);
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/certificates/duplicates']);
  }

  ngOnDestroy(): void {
    if (this.duplicateIdSub) {
      this.duplicateIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.duplicatesService.resetDuplicateDetails();

    super.destroyPageBaseSub();
  }
}
