export const QUESTION_BASE: Array<any> = [
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'first_name',
    label: 'First Name',
    visible: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'last_name',
    label: 'Last Name',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'gender',
    label: 'Gender',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'date_of_birth',
    label: 'Date of Birth',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'date_received',
    label: 'Date Received',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'method',
    label: 'Method',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'date_sent',
    label: 'Date Sent',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'postal' /* _address */,
    label: 'Postal Address',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'comments',
    label: 'Comments',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    controlType: 'text',
    visible: true,
    format: 'string',
    type: 'string'
  }
];
