import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { DuplicatesService } from '../../duplicates.service';
import { QUESTION_BASE } from './duplicates-view.config';

@Component({
  selector: 'app-duplicates-view',
  templateUrl: './duplicates-view.component.html',
  styleUrls: ['./duplicates-view.component.css']
})
export class DuplicatesViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public questionBase: any = QUESTION_BASE;
  public loading: boolean = true;
  public showView: boolean = false;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  /* Subscriptions */
  private duplicateIdSub: Subscription;
  private detailsSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private duplicatesService: DuplicatesService
  ) {
    super({ router, activatedRoute, pageEvent });
    super.setPageTitle('Certificates - Duplicates', false);
    super.setToolbarMainBtns([
      { type: 'back', path: 'main/certificates/duplicates/list' },
      { type: 'edit', path: 'main/certificates/duplicates/edit' }
    ]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.duplicateIdSub = this.duplicatesService.duplicateIdChanged.subscribe((duplicateId: any) => {
      if (duplicateId) {
        this.duplicatesService.getDuplicateDetails(duplicateId);
      } else {
        this.router.navigate(['main/certificates/duplicates']);
      }
    });

    this.detailsSub = this.duplicatesService.detailsChanged.subscribe((details: any) => {
      if (details) {
        this.setDetails(details);
      }
    });
  }

  setDetails(details: any) {
    this.questionBase.forEach((question) => {
      question['value'] = details[question['key']] /* ? details[question['key']] : '' */;
      // this.api.setProperty(question['key'], 'value', details[question['key']] ? details[question['key']] : '');
    });

    setTimeout(() => {
      this.showView = true;
    }, 100);
  }

  ngOnDestroy(): void {
    if (this.duplicateIdSub) {
      this.duplicateIdSub.unsubscribe();
    }
    if (this.detailsSub) {
      this.detailsSub.unsubscribe();
    }
    this.duplicatesService.resetDuplicateDetails();

    super.destroyPageBaseSub();
  }
}
