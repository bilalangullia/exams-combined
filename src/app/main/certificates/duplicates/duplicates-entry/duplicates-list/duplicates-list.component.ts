import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { ExcelExportParams } from 'ag-grid';

import { IFetchListParams } from '../../../../../shared/shared.interfaces';

import {
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  KdAlertEvent,
  KdFilter,
  ITableColumn,
  ITableApi,
  ITableConfig,
  KdTableDatasourceEvent,
  ITableDatasourceParams
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../shared/shared.service';
import { DuplicatesService } from '../../duplicates.service';
import { IFilters } from '../../duplicates.interfaces';
import { TABLE_COLUMN, FILTER_INPUTS } from './duplicates-list.config';

@Component({
  selector: 'app-duplicates-list',
  templateUrl: './duplicates-list.component.html',
  styleUrls: ['./duplicates-list.component.css']
})
export class DuplicatesListComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Certificates - Duplicates';
  readonly GRID_ID: string = 'duplicates-table';
  readonly PAGE_SIZE: number = 20;
  readonly TOTAL_ROW: number = 1000;

  public loading: boolean = true;
  public showTable: boolean = false;
  public smallLoaderValue: number = null;
  public filterInputs = FILTER_INPUTS;

  public tableApi: ITableApi = {};
  public tableConfig: ITableConfig = {
    id: this.GRID_ID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: { pagesize: this.PAGE_SIZE, total: this.TOTAL_ROW },
    action: {
      enabled: true,
      list: [
        {
          icon: 'fa fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.duplicatesService.setDuplicateId(_rowNode['data']['id']);
            setTimeout(() => {
              this.router.navigate(['main/certificates/duplicates/view']);
            }, 100);
          }
        }
      ]
    }
  };
  public tableColumns: Array<ITableColumn> = [
    TABLE_COLUMN.CandidateID,
    TABLE_COLUMN.CandidateName,
    TABLE_COLUMN.Examination,
    TABLE_COLUMN.CertificateNumber,
    TABLE_COLUMN.DateReceived,
    TABLE_COLUMN.Method,
    TABLE_COLUMN.DateSent
  ];
  public tableRows: Array<any> = [];
  private currentPage: number = 0;
  private currentFilters: IFilters = null;

  @ViewChild('filters') filter: KdFilter;

  /* Subscriptions */
  private academicPeriodOptionsSub: Subscription;
  private examinationsOptionsSub: Subscription;
  private filtersSub: Subscription;
  private tableEventSub: Subscription;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public _kdAlert: KdAlertEvent,
    public _tableEvent: KdTableEvent,
    private sharedService: SharedService,
    private duplicatesService: DuplicatesService
  ) {
    super({ router, pageEvent, activatedRoute });

    super.enableToolbarSearch(true, (event: any): void => {});
    super.setPageTitle(this.pageTitle, false);
    super.setToolbarMainBtns([
      { type: 'add', path: 'main/certificates/duplicates/add' },
      { type: 'import' },
      {
        type: 'export',
        callback: () => {
          if (this.tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'duplicatesCandidates_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this.tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchList(event);
    });
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.duplicatesService.getOptionsAcademicPeriod();

    this.academicPeriodOptionsSub = this.duplicatesService.optionsAcademicPeriodChanged.subscribe(
      (options: Array<any>) => {
        if (options && options.length) {
          this.setDropdownOptions('academic_period', options);
        }
      }
    );

    this.examinationsOptionsSub = this.duplicatesService.optionsExaminationChanged.subscribe((options: Array<any>) => {
      if (options) {
        this.setDropdownOptions('examination', options);
      }
    });

    this.filtersSub = this.duplicatesService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (
        filters &&
        filters['academic_period'] &&
        filters['academic_period']['key'] &&
        filters['examination'] &&
        filters['examination']['key']
      ) {
        this.currentFilters = { ...filters };

        this.duplicatesService
          .getTotalRecords(filters['academic_period']['key'], filters['examination']['key'])
          .subscribe(
            (res: any) => {
              if (res && res['data'] && res['data']['total']) {
                this.tableConfig.paginationConfig.total = res['data']['total'];
                this.showTable = true;
              } else {
                this.tableConfig.paginationConfig.total = 0;
                this.showTable = false;
              }
            },
            (err: HttpErrorResponse) => {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          );
      }
    });

    setTimeout(() => {
      this.tableEventSub = this._tableEvent.onKdTableEventList(this.GRID_ID).subscribe((event: any): void => {
        if (event instanceof KdTableDatasourceEvent) {
          this.fetchList(event);
        }
      });
    }, 1000);
  }

  setDropdownOptions(key: string, data: Array<any>) {
    let options: Array<{ key: number; value: string }> = [];
    if (data.length) {
      options = data.map((item) => ({ key: item['id'], value: item['name'] }));
    } else {
      options.unshift({ key: null, value: '-- Please Select --' });
    }
    this.filter.setInputProperty(key, 'options', options);
    setTimeout(() => {
      this.filter.setInputProperty(key, 'value', options[0]['key']);
    }, 0);
  }

  detectValue(event) {
    if (event['key'] === 'academic_period') {
      this.duplicatesService.setFilters({ examination: { key: null, value: '-- Please Select --' } });
      this.duplicatesService.getOptionsExamination(event['value']);
    }

    let filter: IFilters;
    let filterObj = event['options'].find((item) => item['key'] == event['value']);
    if (filterObj) {
      filter = { [event['key']]: filterObj };

      this.duplicatesService.setFilters(filter);
    }
  }

  fetchList(event: KdTableDatasourceEvent) {
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      this.currentPage = event['rowParams']['endRow'] / this.PAGE_SIZE;
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    this.duplicatesService
      .getList(
        this.currentFilters['academic_period']['key'],
        this.currentFilters['examination']['key'],
        /* event['rowParams'] */ fetchParams['startRow'],
        /* event['rowParams'] */ fetchParams['endRow']
      )
      .toPromise()
      .then(
        (res: any) => {
          if (res && res['data'] && res['data'] && res['data']['userRecords'] && res['data']['userRecords'].length) {
            list = res['data']['userRecords'];
            total = res['data']['total'];
          } else {
            list = [];
            total = 0;
          }
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
        },
        (err) => {
          list = [];
          total = 0;
          dataSourceParams = { rows: list, total };
          event.subscriber.next(dataSourceParams);
          event.subscriber.complete();
        }
      );
  }

  searchList(keyword: string) {
    console.log(keyword);
  }

  /* setPreviousFilters(filters: IFilters) {
    Object.keys(filters).forEach((key) => {
      this.filter.setInputProperty(key, 'value', filters[key]['key']);
    });
  } */

  ngOnDestroy(): void {
    if (this.academicPeriodOptionsSub) {
      this.academicPeriodOptionsSub.unsubscribe();
    }
    if (this.examinationsOptionsSub) {
      this.examinationsOptionsSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }
    if (this.filtersSub) {
      this.filtersSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
