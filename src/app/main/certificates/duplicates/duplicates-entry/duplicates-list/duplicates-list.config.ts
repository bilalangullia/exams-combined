interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  CandidateID?: Column;
  CandidateName?: Column;
  Examination?: Column;
  CertificateNumber?: Column;
  DateReceived?: Column;
  Method?: Column;
  DateSent?: Column;
}

export const TABLE_COLUMN: ListColumn = {
  CandidateID: {
    headerName: 'Candidate ID',
    field: 'candidate_id',
    sortable: true,
    filterable: true
  },
  CandidateName: {
    headerName: 'Candidate Name',
    field: 'candidate_name',
    sortable: true,
    filterable: true
  },
  Examination: {
    headerName: 'Examination',
    field: 'examination',
    sortable: true,
    filterable: true
  },
  CertificateNumber: {
    headerName: 'Certificate Number',
    field: 'certificate_no',
    sortable: true,
    filterable: true
  },
  DateReceived: {
    headerName: 'Date Received',
    field: 'date_received',
    sortable: true,
    filterable: true
  },
  Method: {
    headerName: 'Method',
    field: 'method',
    sortable: true,
    filterable: true
  },
  DateSent: {
    headerName: 'Date Sent',
    field: 'date_sent',
    sortable: true,
    filterable: true
  }
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination',
    visible: true,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  }
];
