import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscriber } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../../shared/shared.service';
import { IToasterConfig } from '../../../../../shared/shared.interfaces';
import { saveSuccess, serverError, saveFail } from '../../../../../shared/shared.toasters';
import { DuplicatesService } from '../../duplicates.service';
import { IAddPayload } from '../../duplicates.interfaces';
import { QUESTION_BASE, FORM_BUTTONS } from './duplicates-add.config';

@Component({
  selector: 'app-duplicates-add',
  templateUrl: './duplicates-add.component.html',
  styleUrls: ['./duplicates-add.component.css']
})
export class DuplicatesAddComponent extends KdPageBase implements OnInit, OnDestroy {
  private pageTitle: string = 'Certificates - Duplicates';

  public loading: boolean = true;
  public questionBase: any = QUESTION_BASE;
  public formButtons: any = FORM_BUTTONS;
  public smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};

  private currentDetails: any = null;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private duplicatesService: DuplicatesService,
    private sharedService: SharedService
  ) {
    super({ router: router, activatedRoute: activatedRoute, pageEvent: pageEvent });
    super.setPageTitle(this.pageTitle, false);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.loading = false;

    this.setOptions();
  }

  setOptions() {
    let candidateIdQues = this.questionBase.find((question) => question['key'] === 'candidate_id');
    candidateIdQues['dropdownCallback'] = this.searchCandidateId.bind(this);
  }

  searchCandidateId(params: string): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data = [];
      if (params['query'].length) {
        this.duplicatesService.getAutocompleteCandiateId(params['query']).subscribe(
          (res: any) => {
            if (res && res['data'] && res['data'].length) {
              data = res['data'];
            } else {
              data = [];
            }
            _observer.next(data);
            _observer.complete();
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
          }
        );
      } else {
        _observer.next([]);
        _observer.complete();
      }
    });
  }

  detectValue(event) {
    if (event['key'] === 'candidate_id' && event['value']['value']) {
      this.duplicatesService.getDetailsByCandidateId(event['value']['value']).subscribe((res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.currentDetails = res['data'][0];
          this.setDetails(this.currentDetails);
        }
      });
    }
  }

  setDetails(details: any) {
    this.emptyFields();

    this.questionBase.forEach((question: any) => {
      if (question['key'] === 'candidate_id') {
      } else if (question['controlType'] === 'dropdown') {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']] ? details[question['key']]['key'] : null
        );
      } else if (question['controlType'] === 'date' || question['key'] === 'date_of_birth') {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']] ? details[question['key']].split('T')[0] : ''
        );
      } else if (
        question['key'] === 'examination_id' ||
        question['key'] === 'gender_id' ||
        question['key'] === 'certificate_number'
      ) {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']] ? details[question['key']]['value'] : ''
        );
      } else {
        this.api.setProperty(
          question['key'],
          'value',
          details[question['key']]
            ? details[question['key']]['key']
              ? details[question['key']]['key']
              : details[question['key']]
            : ''
        );
      }
    });
  }

  emptyFields() {
    this.questionBase.forEach((question) => {
      if (question['key'] === 'candidate_id') {
      } /* else if (question['controlType'] === 'dropdown') {
        this.api.setProperty(question['key'], 'value', '');
      }  */ /* else if (question['controlType'] === 'date') {
        this.api.setProperty(question['key'], 'value', '');
      } */ else {
        this.api.setProperty(question['key'], 'value', '');
      }
    });
  }

  requiredCheck(form: any) {
    let hasError: boolean = false;
    this.questionBase.forEach((question) => {
      if (question['required'] && !form[question['key']]) {
        hasError = true;
        this.api.setProperty(question['key'], 'errors', ['This field is required.']);
      }
    });
    return hasError;
  }

  submit(event) {
    let form: IAddPayload;
    form = {
      candidate_id: event['candidate_id']['value'],
      certificate_number: this.currentDetails['certificate_number']['key'],
      date_received:
        event['date_received'] &&
        event['date_received']['text'] &&
        event['date_received']['text'].split('-').findIndex((item) => item == 'undefined') === -1
          ? event['date_received']['text']
          : '',
      method: event['method'],
      date_sent:
        event['date_sent'] &&
        event['date_sent']['text'] &&
        event['date_sent']['text'].split('-').findIndex((item) => item == 'undefined') === -1
          ? event['date_sent']['text']
          : '',
      postal: event['postal'],
      comments: event['comments']
    };

    if (this.requiredCheck(form)) {
      let warnMessage: IToasterConfig = { title: 'Missing Required Fields', timeout: 2000, type: 'error' };
      this.sharedService.setToaster(warnMessage);
    } else {
      let payload = { ...form };
      this.duplicatesService.addDuplicate(payload).subscribe(
        (res: any) => {
          if (res['data']) {
            this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
            setTimeout(() => {
              this.cancel();
            }, 500);
          } else {
            this.sharedService.setToaster({ ...saveFail, body: res['message'] });
          }
        },
        (err: HttpErrorResponse) => {
          console.log(err)['error'];
          this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['main/certificates/duplicates/list']);
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
