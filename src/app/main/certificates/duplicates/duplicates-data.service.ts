import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import urls from '../../../shared/config.urls';

@Injectable()
export class DuplicatesDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /** Add token to request **/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /**  LIST API **/
  getList(academicPeriodId: number, examinationId: number, start: number, end: number) {
    // http://openemis.n2.iworklab.com/api/certificates/duplicates/listing?start=0&end=2&examination_id=3&academic_period_id=4
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.certificates}/${urls.duplicates}/${urls.listing}?academic_period_id=${academicPeriodId}&examination_id=${examinationId}&start=${start}&end=${end}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /**  LIST | SEARCH API  **/
  getListSearch(examinationId: number, keyword: string, start: number, end: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.certificates}/${urls.duplicates}/${urls.listing}/examination_id=${examinationId}&start=${start}&end=${end}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** ADD | Autocomplete Canidate ID ***/
  // http://openemis.n2.iworklab.com/api/registration/candidate/autocomplete-candidate-id/20f
  getCandidateId(keyword: string) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${urls.autocomplete}-${urls.candidate}-id/${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  getDetailsByCandidateId(candidateId: string) {
    // http://openemis.n2.iworklab.com/api/registration/candidate/20FOA030009/details-by-id
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.registration}/${urls.candidate}/${candidateId}/details-by-id`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  addDuplicate(payload: any) {
    /*** ADD ***/
    // http://openemis.n2.iworklab.com/api/certificates/duplicates/add
    // { "method" : 2, "candidate_id" : "20FOA040001", "date_received" : "2020-04-08", "date_sent" : "2020-04-08", "comments" : "test", "postal" : "test" }
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.certificates}/${urls.duplicates}/${urls.add}`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /** Get Details | VIEW | API  **/
  getDetails(duplicateId: string) {
    // http://openemis.n2.iworklab.com/api/certificates/duplicates/2/view
    return this.httpClient
      .get(`${environment.baseUrl}/${urls.certificates}/${urls.duplicates}/${duplicateId}/view`, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /** EDIT | SUBMIT API  **/
  updateDetails(duplicateId, payload) {
    // http://openemis.n2.iworklab.com/api/certificates/duplicates/6/update
    return this.httpClient
      .post(
        `${environment.baseUrl}/${urls.certificates}/${urls.duplicates}/${duplicateId}/update`,
        { ...payload },
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Handle Http Errors */
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        let temp = localStorage.getItem('token');
        if (temp) {
          localStorage.clear();
          this.router.navigate(['/auth']);
        }
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
