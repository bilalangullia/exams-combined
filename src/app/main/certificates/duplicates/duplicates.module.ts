import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { DuplicatesRoutingModule } from './duplicates-routing.module';
import { DuplicatesService } from './duplicates.service';
import { DuplicatesEntryComponent } from './duplicates-entry/duplicates-entry.component';
import { DuplicatesAddComponent } from './duplicates-entry/duplicates-add/duplicates-add.component';
import { DuplicatesEditComponent } from './duplicates-entry/duplicates-edit/duplicates-edit.component';
import { DuplicatesListComponent } from './duplicates-entry/duplicates-list/duplicates-list.component';
import { DuplicatesViewComponent } from './duplicates-entry/duplicates-view/duplicates-view.component';
import { DuplicatesDataService } from './duplicates-data.service';

@NgModule({
  imports: [CommonModule, DuplicatesRoutingModule, SharedModule],
  declarations: [
    DuplicatesEntryComponent,
    DuplicatesAddComponent,
    DuplicatesEditComponent,
    DuplicatesListComponent,
    DuplicatesViewComponent
  ],
  providers: [DuplicatesService, DuplicatesDataService]
})
export class DuplicatesModule {}
