import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DuplicatesEntryComponent } from './duplicates-entry/duplicates-entry.component';
import { DuplicatesListComponent } from './duplicates-entry/duplicates-list/duplicates-list.component';
import { DuplicatesAddComponent } from './duplicates-entry/duplicates-add/duplicates-add.component';
import { DuplicatesEditComponent } from './duplicates-entry/duplicates-edit/duplicates-edit.component';
import { DuplicatesViewComponent } from './duplicates-entry/duplicates-view/duplicates-view.component';

const routes: Routes = [
  {
    path: '',
    component: DuplicatesEntryComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: DuplicatesListComponent },
      { path: 'add', component: DuplicatesAddComponent },
      { path: 'edit', component: DuplicatesEditComponent },
      { path: 'view', component: DuplicatesViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DuplicatesRoutingModule {}
