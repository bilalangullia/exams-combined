import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationMainComponent } from './registration-main.component';
// import { CandidatesListComponent } from './candidates/candidates-list/candidates-list.component';
// import { RegEditFormComponent } from './candidates/reg-edit-form/reg-edit-form.component';
// import { CandidatesViewComponent } from './candidates/candidates-view/candidates-view.component';
// import { wizardRoutes } from './reg-form/reg-form-routing.module';


const routes: Routes = [
  {
    path: '',
    component: RegistrationMainComponent,
    children: [
      { path: '', redirectTo: 'candidates', pathMatch: 'full' },
      { path: 'candidates', loadChildren:'./candidates/candidates.module#CandidatesModule'},
      { path: 'fees', loadChildren:'./fees/fees.module#FeesModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class RegistrationRoutingModule { }
