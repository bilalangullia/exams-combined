import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  Subscription,
  timer,
  Observable,
  Subject,
  BehaviorSubject,
  from,
  Subscriber,
} from "rxjs";
import { ExcelExportParams } from "ag-grid";

import {
  KdPageBase,
  KdPageBaseEvent,
  KdToolbarEvent,
  KdAdvFilterEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  KdIntTableEvent,
  ITableApi,
  KdAlertEvent,
  KdTableDatasourceEvent,
  ITableDatasourceParams,
  KdTableSelectionUpdateEvent,
  KdSplitterEvent,
  IDynamicFormApi,
} from "openemis-styleguide-lib";

import { DataService } from "../../../../shared/data.service";
import { SharedService } from "../../../../shared/shared.service";
import { TABLECOLUMN, CREATE_ROW } from "../../../../shared/config.table";
import { debounceTime, map } from "rxjs/operators";
import { _questionBase } from "./advanceSearch.config";

@Component({
  selector: "app-candidates-list",
  templateUrl: "./candidates-list.component.html",
  styleUrls: ["./candidates-list.component.css"],
  providers: [
    KdTableEvent,
    KdToolbarEvent,
    KdAdvFilterEvent,
    KdIntTableEvent,
    KdSplitterEvent,
  ],
})
export class CandidatesListComponent extends KdPageBase
  implements OnInit, OnDestroy {
  private results$ = new Observable<any>();
  private subject = new BehaviorSubject<any>(null);

  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = "listTable";
  public PAGESIZE: number = 20;
  public isSearch: boolean = false;
  public _row: Array<any>;
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public _tableApi: ITableApi = {};
  public api: IDynamicFormApi = {};

  public Isvalue: boolean = true;
  public pageCount: number = 20;
  public Isorted: boolean = false;
  public isChecked: boolean = false;
  public startRow: Number = 0;
  public rowFlag: boolean = false;
  public searchDetail: any = {};
  public defaultValues: any = {};
  public setDefaultValue: any = {};
  private _tableEventSubscription: Subscription;
  public setLimit: any = {};
  public showSearch = false;
  public formData: Array<any> = _questionBase;
  public tableEvent: any;
  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: "server",
    gridHeight: "auto",
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW,
    },
    action: {
      enabled: true,
      list: [
        {
          icon: "far fa-eye",
          name: "View",
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.setCandidateId(_rowNode.data);
          },
        },
      ],
    },
  };
  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.OpenEMISID,
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.Examination,
    TABLECOLUMN.FirstName,
    TABLECOLUMN.LastName,
    TABLECOLUMN.Gender,
    TABLECOLUMN.DateofBirth,
  ];

  public _formButtons: Array<any> = [
    {
      type: "submit",
      name: "SUBMIT",
      icon: "kd-check",
      class: "btn-text",
    },
    {
      type: "reset",
      name: "Reset",
      icon: "kd-close",
      class: "btn-outline",
    },
  ];

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    activatedRoute: ActivatedRoute,
    public _kdalert: KdAlertEvent,
    private _advFilterEvent: KdAdvFilterEvent,
    private _toolbarEvent: KdToolbarEvent,
    public _tableEvent: KdTableEvent,
    private kdtable: KdIntTableEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    private _kdSplitterEvent: KdSplitterEvent
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute,
    });
  }

  ngOnInit() {
    super.setPageTitle("Registrations - Candidates", false);
    super.setToolbarMainBtns([
      {
        type: "add",
        path: "/main/registration/candidates/add",
      },
      {
        type: "import",
        path: "/main/registration/candidates/import",
      },
      {
        type: "export",
        callback: () => {
          let defaultParams: ExcelExportParams = {
            fileName: "candidate_list",
            allColumns: true,
            suppressTextAsCDATA: true,
          };
          this._tableApi.general.exportToExcel(defaultParams);
        },
      },
      {
        custom: true,
        icon: "fa fa-search-plus",
        tooltip: "Advance Search",
        callback: (): void => {
          this._kdSplitterEvent.toggleSubPane(true);
          this.showSearch = true;
        },
      },
    ]);

    timer(100).subscribe(() => {
      this.setAdvanceDropDown();
      this.debounce(this.searchUserDetail);
    });

    super.enableToolbarSearch(true, (event: any): void => {
      this.subject.next(event);

      // if(event.length>=2){
      //     this.debounce()
      // }
      // if (event.length >= 10) {
      //   this.isSearch = true;
      //   this.rowFlag = false;
      //   this.Isorted = false;
      //   this.Isvalue = true;
      //   this.loading = true;

      //   this.searchCandidateList(event).then((res: any) => {

      //     if (res != 0) {
      //       this._row = res.candidateRecords.length > 0 ? res.candidateRecords : 0;
      //       this._row.length < 20 ? (this.pageCount = this._row.length) : (this.pageCount = res.total);
      //       this.loading = false;
      //       this.Isvalue = false;
      //     }
      //   });
      // } else if (event.length == 0) {
      //   this.isSearch = true;
      //   this.rowFlag = false;
      //   this.Isorted = false;
      //   this.Isvalue = true;
      //   this.loading = true;
      //   this.searchCandidateList(event).then((res: any) => {
      //     this._row = res.candidateRecords;
      //     this.pageCount = res.total;
      //     this.Isvalue = false;
      //     this.loading = false;
      //   });
      // }
    });

    super.updatePageHeader();
    super.updateBreadcrumb();

    // this.setPageLimit({ start: 0, end: 20 });

    this._tableEventSubscription = this._tableEvent
      .onKdTableEventList(this.GRIDID)
      .subscribe((_event: any): void => {
        this.tableEvent = _event;
        if (_event instanceof KdTableDatasourceEvent) {
          this.hasValue(_event.rowParams.sortModel.length);
          this.searchDetail = _event;
          if (_event.rowParams.startRow != this.startRow && this.rowFlag) {
            this.getCandidateList(_event).then((res: any) => {
              this._row = res;
              let datasourceParams: ITableDatasourceParams = {
                rows: this._row,
                total: this.pageCount,
              };
              this.Isorted = false;
              this.startRow = _event.rowParams.startRow;
              _event.subscriber.next(datasourceParams);
              _event.subscriber.complete();
            });
          } else if (this.Isorted) {
            this.sortedList(_event).then((res: any) => {
              this._row = res;
              let datasourceParams: ITableDatasourceParams = {
                rows: this._row,
                total: this.pageCount,
              };
              this.rowFlag = false;
              this.startRow = _event.rowParams.startRow;
              _event.subscriber.next(datasourceParams);
              _event.subscriber.complete();
            });
          } else if (this.isSearch) {
            let datasourceParams: ITableDatasourceParams = {
              rows: this._row,
              total: this.pageCount,
            };
            // this.isSearch = false;
            _event.subscriber.next(datasourceParams);
            _event.subscriber.complete();
          } else {
            console.log();

            let datasourceParams: ITableDatasourceParams = {
              rows: this._row,
              total: this.pageCount,
            };
            this.rowFlag = true;
            _event.subscriber.next(datasourceParams);
            _event.subscriber.complete();
          }
        } else if (_event instanceof KdTableSelectionUpdateEvent) {
          console.log("KdTableSelectionUpdateEvent", _event);
        }
      });

    // this._toolbarEvent.onSendSearchText().subscribe((value: any): void => {
    // });
  }

  getCandidateList(data) {
    return new Promise((resolve, reject) => {
      this.setLimit = {
        start: data.rowParams.startRow,
        end: data.rowParams.endRow,
      };
      this.dataService
        .registrationCandidate(this.setLimit)
        .subscribe((res: any) => {
          if (res.data.candidateRecords)
            resolve(this.setCandidateListFilter(res.data.candidateRecords));
          this.pageCount = res.data.total;
        });
    });
  }

  setPageLimit(data) {
    return new Promise((resolve, reject) => {
      this.dataService.registrationCandidate(data).subscribe((res: any) => {
        if (res.data.candidateRecords) {
          this.setCandidateListFilter1(res.data.candidateRecords);
          this.pageCount = res.data.total;
        }
      });
    });
  }
  setCandidateId(_rowdata?: any) {
    if (_rowdata && _rowdata.candidate_id) {
      this.sharedService.setCandidateId(_rowdata.candidate_id);
      this.router.navigate(["main/registration/candidates/view"]);
    } else {
      let toasterConfig: any = {
        type: "error",
        title: "Candidate NotFound",
        body: "Candidate Id not found please select valid candidate.",
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 3000,
      };
      this.sharedService.setToaster(toasterConfig);
    }
  }

  setCandidateListFilter(data) {
    this.resetFilter();

    if (data.length > 0) {
      // this._row = data;
      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]["filterable"]) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (
              data[j][this.tableColumns[i]["field"]] &&
              data[j][this.tableColumns[i]["field"]] != "undefined"
            ) {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(
                  data[j][this.tableColumns[i]["field"]]
                );
              } else {
                this.tableColumns[i].filterValue.indexOf(
                  data[j][this.tableColumns[i]["field"]]
                ) > -1
                  ? "na"
                  : this.tableColumns[i].filterValue.push(
                      data[j][this.tableColumns[i]["field"]]
                    );
              }
            }
          }
        }
      }
      this.loading = false;
      return data;
    } else {
      this.loading = false;
      this._row.length = 0;
      let toasterConfig: any = {
        type: "error",
        title: "No records found",
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000,
      };
      this._kdalert.error(toasterConfig);
    }
  }

  setCandidateListFilter1(data) {
    this.resetFilter();
    if (data.length > 0) {
      this._row = data;

      for (let i = 0; i < this.tableColumns.length; i++) {
        if (this.tableColumns[i]["filterable"]) {
          for (let j = 0; j < data.length; j++) {
            let count = 0;
            if (
              data[j][this.tableColumns[i]["field"]] &&
              data[j][this.tableColumns[i]["field"]] != "undefined"
            ) {
              if (this.tableColumns[i].filterValue.length == 0) {
                this.tableColumns[i].filterValue.push(
                  data[j][this.tableColumns[i]["field"]]
                );
              } else {
                this.tableColumns[i].filterValue.indexOf(
                  data[j][this.tableColumns[i]["field"]]
                ) > -1
                  ? "na"
                  : this.tableColumns[i].filterValue.push(
                      data[j][this.tableColumns[i]["field"]]
                    );
              }
            }
          }
        }
      }
      this.loading = false;
      this.Isvalue = false;
    } else {
      this.loading = false;
      this._row.length = 0;
      let toasterConfig: any = {
        type: "error",
        title: "No records found",
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000,
      };
      this._kdalert.error(toasterConfig);
    }
  }

  resetFilter() {
    for (let i = 0; i < this.tableColumns.length; i++) {
      if (this.tableColumns[i]["filterable"]) {
        this.tableColumns[i].filterValue.length = 0;
      }
    }
  }

  sortedList(data) {
    let sortValue = data.rowParams.sortModel[0].colId;
    var order = data.rowParams.sortModel[0].sort;

    return new Promise((resolve, reject) => {
      this.setLimit = {
        start: data.rowParams.startRow,
        end: data.rowParams.endRow,
      };
      this.dataService
        .registrationCandidate(this.setLimit)
        .subscribe((res: any) => {
          if (res.data.candidateRecords)
            order == "asc"
              ? res.data.candidateRecords.sort(function (a, b) {
                  var keyA = a[sortValue],
                    keyB = b[sortValue];
                  if (keyA < keyB) return -1;
                  if (keyA > keyB) return 1;
                  return 0;
                })
              : res.data.candidateRecords.sort(function (a, b) {
                  var keyA = a[sortValue],
                    keyB = b[sortValue];
                  if (keyA > keyB) return -1;
                  if (keyA < keyB) return 1;
                  return 0;
                });

          resolve(this.setCandidateListFilter(res.data.candidateRecords));
          this.Isorted = true;
          this.pageCount = res.data.total;
        });
    });
  }

  hasValue(data) {
    if (data > 0) {
      this.rowFlag = false;
      this.Isorted = true;
    } else if (this.isSearch) {
      this.rowFlag = false;
      this.Isorted = false;
    } else {
      this.rowFlag = true;
      this.Isorted = false;
    }
  }

  debounce(callBack) {
    this.subject.pipe(debounceTime(1500)).subscribe((res: any) => {
      res ? callBack.call(this, res) : callBack.call(this, "");
    });
  }

  searchUserDetail(event) {
    console.log("event...", event);

    if (event.length) {
      this.isSearch = true;
      this.rowFlag = false;
      this.Isorted = false;
      this.Isvalue = true;
      this.loading = true;

      this.searchCandidateList(event).then((res: any) => {
        if (res != 0) {
          this._row =
            res.candidateRecords.length > 0 ? res.candidateRecords : 0;
          this._row.length < 20
            ? (this.pageCount = this._row.length)
            : (this.pageCount = res.total);
          this.loading = false;
          this.Isvalue = false;
        }
      });
    } else {
      console.log("event...", event);
      this.isSearch = false;
      this.rowFlag = true;
      this.Isorted = false;
      this.Isvalue = true;
      this.loading = true;
      this.setPageLimit({ start: 0, end: 20 }).then((res: any) => {
        this._row = res.candidateRecords;
        this.pageCount = res.total;
        this.Isvalue = false;
        this.loading = false;
      });
    }
  }

  searchCandidateList(data: any) {
    let detail = {
      start: 0,
      end: 20,
      query: data,
    };

    return new Promise((resolve, reject) => {
      this.dataService.serachCandidateList(detail).subscribe(
        (res: any) => {
          if (res.message == "Record found") {
            resolve(res.data);
          } else if (res.message == "Record Not Found") {
            let toasterConfig: any = {
              type: "error",
              title: "Record not found",
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000,
            };
            this.sharedService.setToaster(toasterConfig);
            this.loading = false;
            this.Isvalue = false;
            resolve(0);
          }
        },
        (err) => {
          let toasterConfig: any = {
            type: "error",
            title: "Record not found",
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000,
          };
          this.sharedService.setToaster(toasterConfig);
          resolve(0);
        }
      );
    });
  }

  closeSearch(): void {
    this._kdSplitterEvent.toggleSubPane(false);
    this.showSearch = false;
  }

  setAdvanceDropDown() {
    this.sharedService.getDropdownValues().subscribe((data) => {
      this.defaultValues = data;
      if (this.defaultValues) {
        let keyArr = Object.keys(this.defaultValues.data);
        keyArr.forEach((e) => {
          let quesObj = this.formData.find((q) => {
            return q.key == e;
          });
          if (quesObj) {
            quesObj.options.push(
              ...this.defaultValues.data[e].map((item) => {
                return { key: item.id, value: item.name };
              })
            );
          }
        });
      }
      this.formData.forEach((item) => {
        if (item.key == "openemis_no") {
          item["dropdownCallback"] = this.searchOpenisId.bind(this);
        }
      });
    });
  }
  getTriggerInput(event) {
    this.setDefaultValue[event.key] = event.value;

    if (event.key == "openemis_no") {
      this.setDefaultValue[event.key] = event.value.value;
    }
  }

  searchOpenisId(params: any): Observable<any> {
    return new Observable((_observer: Subscriber<any>): any => {
      let data: Array<any> = [];

      // console.log('Params usable:', params);
      if (params.query && params.query.length > 2) {
        this.dataService.searchOpenEmisID(params.query).subscribe(
          (res: any) => {
            if (res && res.data.length) {
              timer(500).subscribe((): void => {
                data = res.data;
                _observer.next(data);
                _observer.complete();
              });
            } else {
              _observer.next([]);
              _observer.complete();
            }
          },
          (err) => {
            console.log(err);
            _observer.next([]);
            _observer.complete();
          }
        );
      }
    });
  }
  formSubmitVal(event) {
    this.Isvalue = true;
    let payLoads = {
      first_name: this.setDefaultValue.first_name,
      middle_name: this.setDefaultValue.middle_name,
      third_name: this.setDefaultValue.third_name,
      last_name: this.setDefaultValue.last_name,
      openemis_no: this.setDefaultValue.openemis_no,
      gender_id: this.setDefaultValue.gender_id,
      nationality_id: this.setDefaultValue.nationality_id,
      identity_type_id: this.setDefaultValue.identity_type_id,
      identity_number: this.setDefaultValue.identity_number,
      examination_id: this.setDefaultValue.examination_id,
      examination_centre_id: this.setDefaultValue.examination_centre_id,
      start: 0,
      end: 20,
    };

    this.searchAvance(payLoads).then((res: any) => {
      this._row = res;
      timer(100).subscribe(() => {
        let datasourceParams: ITableDatasourceParams = {
          rows: this._row,
          total: this.pageCount,
        };
        this.rowFlag = false;
        this.startRow = this.tableEvent.rowParams.startRow;
        this.tableEvent.subscriber.next(datasourceParams);
        this.tableEvent.subscriber.complete();
      });
    });
  }
  searchAvance(data) {
    return new Promise((resolve, reject) => {
      this.dataService.advanceSearch(data).subscribe((res: any) => {
        if (res.data.list.length > 0) {
          this.Isvalue = false;
          resolve(this.setCandidateListFilter(res.data.list));
          this.pageCount = res.data.total;
        } else {
          this.setPageLimit({ start: 0, end: 20 }).then((res: any) => {
            let toasterConfig: any = {
              type: "error",
              title: "No records found",
              showCloseButton: true,
              tapToDismiss: true,
              timeout: 3000,
            };
            this._kdalert.error(toasterConfig);
            this.pageCount = res.data.total;

            resolve((this._row = res.candidateRecords));
          });
        }
      });
    });
  }
  reset() {
    this.isSearch = false;
    this.rowFlag = true;
    this.Isorted = false;
    this.Isvalue = true;
    this.loading = true;
    this.setPageLimit({ start: 0, end: 20 }).then((res: any) => {
      this._row = res.candidateRecords;
      this.pageCount = res.total;
      this.Isvalue = false;
      this.loading = false;
    });    // this._kdSplitterEvent.toggleSubPane(false);
    // this.showSearch = false;
  }

  resSetAdvDropDown(){
    this.formData.forEach((item)=>{
      item.value=''
    })
  }
  ngOnDestroy(): void {
    super.destroyPageBaseSub();
    if (typeof this._tableEventSubscription !== "undefined") {
      this._tableEventSubscription.unsubscribe();
    }
  }
}
