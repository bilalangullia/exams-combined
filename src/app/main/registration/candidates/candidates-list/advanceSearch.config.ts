export const  _questionBase= [
    {
      key: 'first_name',
      label: 'First Name',
      visible: true,
      order: 1,
      controlType: 'text',
      placeholder: 'First Name',
      type: 'text'
    },
    {
      key: 'middle_name',
      label: 'Middle Name',
      visible: true,
      order: 1,
      controlType: 'text',
      placeholder: 'Middle Name',
      type: 'text'
    },
    {
      key: 'third_name',
      label: 'Third Name',
      visible: true,
      order: 1,
      controlType: 'text',
      placeholder: 'Third Name',
      type: 'text'
    },
    {
      key: 'last_name',
      label: 'Last Name',
      visible: true,
      order: 1,
      controlType: 'text',
      placeholder: 'Last Name',
      type: 'text'
    },
    {
        key: 'openemis_no',
        label: 'OpenEMIS ID',
        visible: true,
        order: 1,
        controlType: 'text',
        type: 'autocomplete',
        autocomplete: true,
        clickToggleDropdown: true,
        placeholder: 'Search EMIS ID',
        onEnter: true,
        dropdownCallback:true
    },
        
    {
      key: 'gender_id',
      label: 'Gender',
      visible: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
  
    {
      key: 'nationality_id',
      label: 'Nationality',
      visible: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'identity_type_id',
      label: 'Identity Type',
      visible: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'identity_number',
      label: 'Identity Number',
      visible: true,
      order: 1,
      controlType: 'text',
      type: 'text',
      placeholder: 'Identity Number'
    },
    {
        key: 'examination_id',
        label: 'Exam Name',
        visible: true,
        order: 1,
        controlType: 'dropdown',
        options: [
          {
            key: 'null',
            value: '--Select--'
          }
        ],
        events: true
      },
      {
        key: 'examination_centre_id',
        label: 'Exam Center',
        visible: true,
        order: 1,
        controlType: 'dropdown',
        options: [
          {
            key: 'null',
            value: '--Select--'
          }
        ],
        events: true
      }
    ];