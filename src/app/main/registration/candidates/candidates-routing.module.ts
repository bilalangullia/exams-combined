import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidateMainComponent } from './candidate-main.component';
import { CandidatesListComponent } from './candidates-list/candidates-list.component';
import { AddCandidateComponent } from './add-candidate/add-candidate.component';
import { RegEditFormComponent } from './reg-edit-form/reg-edit-form.component';
import { CandidatesViewComponent } from './candidates-view/candidates-view.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: CandidatesListComponent },
      { path: 'add', component: AddCandidateComponent },
      { path: 'edit', component: RegEditFormComponent },
      { path: 'view', component: CandidatesViewComponent },
      { path: 'import', loadChildren:'./import/import.module#ImportModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CandidatesRoutingModule { }
