import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { KdPageBase, KdPageBaseEvent, KdToolbarEvent, IDynamicFormApi, KdAdvFilterEvent, KdTableEvent, KdAlertEvent,KdModalEvent } from 'openemis-styleguide-lib';

import { VIEWNODE_INPUT ,IModalConfig} from './candidates-view.sv';
import { SharedService } from '../../../../shared/shared.service'
import { DataService } from '../../../../shared/data.service'

@Component({
  selector: 'app-candidates-view',
  templateUrl: './candidates-view.component.html',
  styleUrls: ['./candidates-view.component.css'],
  providers: [KdTableEvent, KdToolbarEvent, KdAdvFilterEvent,KdModalEvent],

})

export class CandidatesViewComponent extends KdPageBase implements OnInit, OnDestroy {

  public  modalConfig: IModalConfig = {
    title: 'Delete candidate',
    body:'Are you sure you want to delete this candidate',
    button: [
      {
        text: 'Delete',
        class: 'btn-text',
        callback: (): void => {
          event.preventDefault();
          this.dataService.getCandidateDeleted(this.candidateID).subscribe((res:any)=>{
           this._modelEvent.toggleClose();
           let toasterConfig: any = {
            type: 'Success',
            title: 'Candidate deleted successfully ',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this._kdalert.success(toasterConfig); 
           this._router.navigate(['/main'])
          },(err:any)=>{
 
            this._modelEvent.toggleClose(); 
          
           let toasterConfig: any = {
             type: 'error',
             title: err.error.message,
             body:err.error.data,
             showCloseButton: true,
             tapToDismiss: true,
             timeout: 3000
           };
           this._kdalert.error(toasterConfig); 
         })
      } 
      },
      {
        text: 'Cancel',
        class: 'btn-outline',
        callback: (): void => {
          event.preventDefault();
          this._modelEvent.toggleClose();
        }
      }
    ]
 };  
 
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public api: IDynamicFormApi = {};
  public setData: any;
  public candidateData: any;
  public _smallLoaderValue: number = null; 
  public candidateID:any;  
  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public sharedService: SharedService,
    public _kdalert: KdAlertEvent,
    public dataService: DataService,
    public _modelEvent:KdModalEvent,

  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });

  }

  ngOnInit(): void {
    super.setPageTitle('Registrations - Candidates', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => { this._router.navigate(['main/registration/candidates']); }
      }, {
        type: 'edit',
        callback: (): void => { this.editCandidate() }
      }, {
        type: 'delete',
        callback: (): void => {this.open() } 
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.getCandidateView();
  }

  getCandidateView() {
    let tempData = this.sharedService.getCandidateId();
    if (tempData != undefined) {
      this.candidateID=tempData;
      this.dataService.getCandidateView(tempData).subscribe(
        (res: any) => {
          this.candidateData = res.data[0];
          this.setCandidateDetail(this.candidateData);
          this.loading = false;
        },
        err => {
          console.log(err);
          let toasterConfig: any = {
            title: 'Something went Wrong',
            showCloseButton: true,
            tapToDismiss: true,
          };
          this._kdalert.error(toasterConfig);
         this._router.navigate(['/main'])
        }
      )
    }
    else {
      this._router.navigate(['/main'])
    }
  }

  editCandidate() {
    this.sharedService.setCandidateId(this.candidateData.candidate_id);
    this._router.navigate(['/main/registration/candidates/edit']);
  }

  setCandidateDetail(data: any) {
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'options') {
          this.api.setProperty(this._questionBase[i].key, 'row', data.options.data);
        } else {
          this.api.setProperty(this._questionBase[i].key, 'value', data[this._questionBase[i].key] ? data[this._questionBase[i].key] : 'NA');
        }
      }
    }, 500);
  }

  open(){
    this._modelEvent.toggleOpen();       
  }


  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }

}
