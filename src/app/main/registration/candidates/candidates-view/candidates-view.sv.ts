import { TABLE_COLUMN_LIST } from '../add-candidate/add.config';

export const VIEWNODE_INPUT: Array<any> = [
  {
    'key': 'academic_period_id',
    'label': 'Academic Period',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'examination_id',
    'label': 'Examination',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'area_id',
    'label': 'Area Name',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'examination_centre_id',
    'label': 'Exam Centre',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'examination_attendance_types_id',
    'label': 'Attendance Type',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'openemis_no',
    'label': 'Openemis ID',
    'visible': true,
    'controlType': 'readonly',
    'format': 'readonly',
    'readOnly': true,
    'type': 'string'
  },
  {
    'key': 'candidate_id',
    'label': 'Candidate ID',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'first_name',
    'label': 'First Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'second_name',
    'label': 'Second Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'middle_name',
    'label': 'Middle Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'last_name',
    'label': 'Last Name',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'gender_id',
    'label': 'Gender',
    'visible': true,
    'controlType': 'text',
    'format': 'text',
    'disabled': false
  },
  {
    'key': 'date_of_birth',
    'label': 'Date of Birth',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'nationality_id',
    'label': 'Nationality',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'identity_type_id',
    'label': 'Identity Type',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'identity_number',
    'label': 'Identity Number',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'address',
    'label': 'Address',
    'visible': true,
    'controlType': 'text',
    'format': 'text',
    'disabled': false
  },
  {
    'key': 'postal_code',
    'label': 'Postal Code',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'address_area_id',
    'label': 'Address Area',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'birthplace_area_id',
    'label': 'Birthplace Area',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'special_need_type_id',
    'label': 'Special Needs Assessment Type',
    'visible': true,
    'controlType': 'text',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'special_need_difficulty_id',
    'label': 'Difficulties',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'options',
    'label': 'Options',
    'visible': true,
    'controlType': 'table',
    'row': [],
    'column': [
      TABLE_COLUMN_LIST.option_code,
      TABLE_COLUMN_LIST.option_name,
      TABLE_COLUMN_LIST.carry_forward
    ],
    'config': {
      'id': 'options',
      'rowIdKey': 'id',
      'gridHeight': 250,
      'loadType': 'normal'
    }
  },
  {
    'key': 'modified_by',
    'label': 'Modified By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }, 
  {
    'key': 'modified_on',
    'label': 'Modified On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_by',
    'label': 'Created By',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  },
  {
    'key': 'created_on',
    'label': 'Created On',
    'visible': true,
    'controlType': 'textbox',
    'format': 'string',
    'type': 'string'
  }
];

export interface IModalConfig {
  title?: string;
  body?: string;
  button: Array<IModalButton>;
}


export interface IModalButton {
  text?: string;
  class?: string;
  callback?: () => void;
}