import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CandidatesRoutingModule } from './candidates-routing.module';
import { CandidateMainComponent } from './candidate-main.component';
import { SharedModule } from '../../../shared/shared.module';
import { CandidatesListComponent } from './candidates-list/candidates-list.component';
import { CandidatesViewComponent } from './candidates-view/candidates-view.component';
import { RegEditFormComponent } from './reg-edit-form/reg-edit-form.component';
import { AddCandidateComponent } from './add-candidate/add-candidate.component';

@NgModule({
  imports: [
    CommonModule,
    CandidatesRoutingModule,
    SharedModule
  ],
  declarations: [CandidateMainComponent, CandidatesListComponent, CandidatesViewComponent, RegEditFormComponent, AddCandidateComponent]
})
export class CandidatesModule { }
