import { Observable, Subscriber, timer } from 'rxjs';

interface Column {
    headerName?: string;
    field?: string;
    type?: 'input' | 'normal' | 'image';
    sortable?: boolean;
    filterable?: boolean;
    visible?: boolean;
    config?: any;
    class?: string;
    filterValue?: Array<string>;
}

interface ListColumn {
    ID?: Column;
    CandidateID?: Column;
    OpenEMISID?: Column;
    FirstName?: Column;
    LastName?: Column;
    GenderCode?: Column;
    Nationality?: Column
    RowNumber?: Column,
    MiddleName?: Column,
    ThirdName?: Column,
    DateofBirth?: Column,
    PreferredName?: Column,
    Address?: Column,
    Postal?: Column,
    AddressAreaCode?: Column,
    BirthplaceAreaCode?: Column,
    AccountTypeCode?: Column,
    NationalityId?: Column,
    IdentityTypeCode?: Column,
    AttendanceType?:Column,
    IdentityNumber?: Column,
    ContactType?: Column,
    Contact?: Column,
    SpecialNeedsType?: Column,
    SpecialNeedsAssessmentDate?:Column,
    SpecialNeedsDifficulties?: Column,
    ExamCentre?: Column,
    Option1?: Column,
    CF1?: Column,
    CF2?: Column,
    CF3?: Column,
    CF4?: Column,
    CF5?: Column,
    CF6?: Column,
    Option2?: Column,
    Option3?: Column,
    Option4?: Column,
    Option5?: Column,
    Option6?: Column,
    PaymentDate?:Column,
    RegistrationFee?: Column,
    AmountPaid?:Column,
    ReceiptNumber?: Column
}

export const TABLECOLUMN: ListColumn = {
    ID: {
        headerName: 'Candidate ID (Leave as blank for new entries) ',
        field: 'ID',
        sortable: true,
        filterable: false
    },
    CandidateID: {
        headerName: 'Candidate ID (Leave as blank for new entries) ',
        field: 'CandidateID',
        sortable: true,
        filterable: false
    },
    OpenEMISID: {
        headerName: 'OpenEMIS ID',
        field: 'OpenEMISID',
        sortable: true,
        filterable: false
    },
    FirstName: {
        headerName: 'First Name',
        field: 'FirstName',
        sortable: true,
        filterable: false
    },
    LastName: {
        headerName: 'Last Name',
        field: 'LastName',
        sortable: true,
        filterable: false
    },
    PreferredName: {
        headerName: 'Preferred Name',
        field: 'PreferredName',
        sortable: true,
        filterable: false
    },
    GenderCode: {
        headerName: 'Gender Code (M/F)',
        field: 'Gender',
        sortable: true,
        filterable: false
    },
    Nationality: {
        headerName: 'Nationality',
        field: 'Nationality',
        sortable: true,
        filterable: false
    },
    RowNumber: {
        headerName: 'RowNumber',
        field: 'RowNumber',
        filterable: false

    },
    DateofBirth: {
        headerName: 'DateofBirth',
        field: 'DateofBirth',
        sortable: true,
        filterable: false
    },
    AttendanceType:{
        headerName: 'Attendance Type',
        field: 'AttendanceType',
        sortable: true,
        filterable: false
    },
    MiddleName: {
        headerName: 'MiddleName',
        field: 'MiddleName',
        sortable: true,
        filterable: false
    },
    ThirdName: {
        headerName: 'ThirdName',
        field: 'ThirdName',
        sortable: true,
        filterable: false
    },
    Address: {
        headerName: 'Address',
        field: 'Address',
        sortable: true,
        filterable: false
    },
    Postal: {
        headerName: 'Postal',
        field: 'Postal',
        sortable: true,
        filterable: false
    },
    AddressAreaCode: {
        headerName: 'Address Area Code',
        field: 'AddressAreaCode',
        sortable: true,
        filterable: false
    },
    BirthplaceAreaCode: {
        headerName: 'Birthplace Area Code',
        field: 'BirthplaceAreaCode',
        sortable: true,
        filterable: false
    },
    AccountTypeCode: {
        headerName: 'Account Type Code',
        field: 'AccountTypeCode',
        sortable: true,
        filterable: false
    },
    NationalityId: {
        headerName: 'Nationality Id',
        field: 'NationalityId',
        sortable: true,
        filterable: false
    },
    IdentityTypeCode: {
        headerName: 'Identity Type Code',
        field: 'IdentityTypeCode',
        sortable: true,
        filterable: false
    },
    IdentityNumber: {
        headerName: 'Identity Number',
        field: 'IdentityNumber',
        sortable: true,
        filterable: false
    },
    ContactType: {
        headerName: 'Contact Type',
        field: 'ContactType',
        sortable: true,
        filterable: false
    },
    Contact: {
        headerName: 'Contact',
        field: 'Contact',
        sortable: true,
        filterable: false
    },
    SpecialNeedsType: {
        headerName: 'Special Needs Type(Optional)',
        field: 'SpecialNeedsType',
        sortable: true,
        filterable: false
    },
    SpecialNeedsDifficulties: {
        headerName: 'Special Needs Difficulties(Optional)',
        field: 'SpecialNeedsDifficulties',
        sortable: true,
        filterable: false
    },
    SpecialNeedsAssessmentDate:{
        headerName: 'Special Needs Assessment Date ( DD/MM/YYYY ) (Optional)',
        field: 'SpecialNeedsAssessmentDate',
        sortable: true,
        filterable: false
    },
    ExamCentre: {
        headerName: 'Exam Centre',
        field: 'ExamCentre',
        sortable: true,
        filterable: false
    },
    Option1: {
        headerName: 'Option 1',
        field: 'Option1',
        sortable: true,
        filterable: false
    },
    CF1: {
        headerName: 'CF 1',
        field: 'CF1',
        sortable: true,
        filterable: false
    },
    CF2: {
        headerName: 'CF 2',
        field: 'CF2 ',
        sortable: true,
        filterable: false
    },
    CF3: {
        headerName: 'CF 3',
        field: 'CF3',
        sortable: true,
        filterable: false
    },
    CF4: {
        headerName: 'CF 4',
        field: 'CF4',
        sortable: true,
        filterable: false
    },
    CF5: {
        headerName: 'CF 5',
        field: 'CF5',
        sortable: true,
        filterable: false
    },
    CF6: {
        headerName: 'CF 6',
        field: 'CF6',
        sortable: true,
        filterable: false
    }, Option2: {
        headerName: 'Option 2',
        field: 'Option2',
        sortable: true,
        filterable: false
    }, Option3: {
        headerName: 'Option 3',
        field: 'Option3',
        sortable: true,
        filterable: false
    }, Option4: {
        headerName: 'Option 4',
        field: 'Option4',
        sortable: true,
        filterable: false
    }, Option5: {
        headerName: 'Option 5',
        field: 'Option5',
        sortable: true,
        filterable: false
    }, Option6: {
        headerName: 'Option 6',
        field: 'Option 6',
        sortable: true,
        filterable: false
    },
    PaymentDate:{
        headerName: 'Payment Date ( DD/MM/YYYY ) (Optional)',
        field: 'PaymentDate',
        sortable: true,
        filterable: false
    }, 
    AmountPaid:{
        headerName: 'Amount Paid ($) (Optional)',
        field: 'AmountPaid',
        sortable: true,
        filterable: false
    }
    ,
    RegistrationFee: {
        headerName: 'Registration Fee Type (Optional)',
        field: 'RegistrationFee',
        sortable: true,
        filterable: false
    }, ReceiptNumber: {
        headerName: 'Receipt Number',
        field: 'ReceiptNumber',
        sortable: true,
        filterable: false
    }

};
export const CREATE_ROW: (_rowCount: number, _baseIndex?: number, data?: Array<any>) => Array<any> = (_rowCount: number, _baseIndex: number = 0, data?: Array<any>): Array<any> => {
    let row: Array<any> = [];
    for (let i: number = 0; i < data.length; i++) {
        let oneRow: any = {
            id: i,
            RowNumber:data[i].row_number,
            CandidateID: data[i].data['Candidate ID (Leave as blank for new entries)'],
            FirstName: data[i].data['First Name'],
            MiddleName: data[i].data['Middle Name'],
            ThirdName: data[i].data['Third Name'],
            LastName: data[i].data['Last Name'],
            Gender: data[i].data['Gender Code (M/F)'],
            DateofBirth: data[i].data['Date Of Birth ( DD/MM/YYYY )'],
            NationalityId: data[i].data['Nationality Id'],
            IdentityTypeCode: data[i].data['Identity Type Code'],
            IdentityNumber: data[i].data['Identity Number'],
            ContactType: data[i].data['Contact Type'],
            Contact: data[i].data['Contact'],
            SpecialNeedsType: data[i].data['Special Needs Type (Optional)'],
            SpecialNeedsAssessmentDate:data[i].data['Special Needs Assessment Date ( DD/MM/YYYY ) (Optional)'],
            SpecialNeedsDifficulties: data[i].data['Special Needs Difficulties (Optional)'],
            ExamCentre: data[i].data['Exam Centre'],
            Option1: data[i].data['Option 1'],
            CF1: data[i].data['CF'],
            Option2: data[i].data['Option 2'],
            CF2: data[i].data['CF'],
            Option3: data[i].data['Option 3'],
            CF3: data[i].data['CF'],
            Option4: data[i].data['Option 4'],
            CF4: data[i].data['CF'],
            Option5: data[i].data['Option 5'],
            CF5: data[i].data['CF'],
            Option6: data[i].data['Option 6'],
            CF6: data[i].data['CF'],
            PaymentDate:data[i].data['Payment Date ( DD/MM/YYYY ) (Optional)'],
            AmountPaid: data[i].data['Amount Paid ($) (Optional)'],
            RegistrationFee: data[i].data['Registration Fee Type (Optional)'],
            ReceiptNumber: data[i].data['Receipt Number (Optional)']
        };

        row.push(oneRow);
    }

    return row;
};


export const CREATE_TABLE_CONFIG: (_id: string, _pagesize: number, _total: number) => any = (_id: string, _pagesize: number, _total: number): any => {
    return {
        id: _id,
        loadType: 'server',
        gridHeight: '600',
        externalFilter: false,
        paginationConfig: {
            pagesize: _pagesize,
            total: _total
        },
        click: {
            type: 'router',
            // pathMap: 'view',
            path: '/registration/view',
            callback: (): void => {
                console.log('ListNode: Demo callback used in when clicking the rowNode.');
            }
        }
    };
};

export const DUMMY_API_CALL: (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }) => Observable<any> =
    (_params: { startRow: number, endRow: number, filterModel: any, sortModel: any, pagesize: number }): Observable<any> => {
        return new Observable((_observer: Subscriber<any>): void => {
            timer(1000).subscribe((): void => {
                _observer.next(CREATE_ROW(_params.pagesize, _params.startRow));
                _observer.complete();
            });
        });
    };

    export interface IMiniDashboardItem {
        type: string;
        icon?: string;
        label: string;
        value: number | string ;
    }
    
    export interface IMiniDashboardConfig {
        closeButtonDisabled?: boolean;
        rtl?: boolean;
    }
    
    
    export const MINI_DASHBOARD_CONFIG: IMiniDashboardConfig = {
        closeButtonDisabled: true,
        rtl: true,
    };
    
    export const MINI_DASHBOARD_DATA: Array<IMiniDashboardItem> = [
        {
            type: 'text',
            label: 'Total Rows :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Imported :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Update :',
            value: '8'
        },
        {
            type: 'text',
            label: 'Row Failed :',
            value: '0'
        },
    ]