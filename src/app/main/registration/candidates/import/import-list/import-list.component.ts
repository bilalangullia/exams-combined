import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IMiniDashboardItem, IMiniDashboardConfig, MINI_DASHBOARD_CONFIG, MINI_DASHBOARD_DATA } from './import-candidates-list-temp';
import {
	KdPageBase,
	KdPageBaseEvent,
	ITableColumn,
	ITableConfig,
	ITableApi,
	KdTableEvent,
	KdToolbarEvent,
	IBtnGroupConfig,
	KdAlertEvent,
} from 'openemis-styleguide-lib';
import { timer, Subscription } from 'rxjs';
import { TABLECOLUMN, CREATE_ROW } from './import.table.config';
import { SharedService } from '../../../../../shared/shared.service';

@Component({
	selector: 'app-import-list',
	templateUrl: './import-list.component.html',
	styleUrls: ['./import-list.component.css']
})

export class ImportListComponent extends KdPageBase implements OnInit, OnDestroy {
	readonly TABLEID: string = 'normalTable';
	readonly PAGESIZE: number = 10;
	readonly TOTALROWS: number = 100;
	readonly STARTINDEX: number = 0;
	public name: string = 'button-group-sample';
	public config: IBtnGroupConfig = {
		type: 'radio',
		iconOnly: true
	};
	public loading = true;
	public imprtData: any;
	public csvData: any = [];
	public csvDataSuccess: any = [];
	public value: any = 0;
	public rowErro: any = [];
	public IsError = true;

	public miniDashboardConfig: IMiniDashboardConfig = MINI_DASHBOARD_CONFIG;
	public miniDashboardData: Array<IMiniDashboardItem> = MINI_DASHBOARD_DATA;

	public fileinput: Array<any> = [
		{
			visible: false
		}
	];
	public button: Array<any> = [
		{
			name: 'Downloads failed records',
			type: 'button',
			icon: 'kd-download',
			key: 'failed',
			class: 'btn btn-error',
			controlType: 'btn-text',
		},
		{
			name: 'Downloads successfull records',
			type: 'button',
			icon: 'kd-download',
			key: 'success',
			class: 'btn btn-success',
			controlType: 'btn-text',
			id: 'sucess'

		}
	];

	public _tableApi: ITableApi = {};
	public _row: Array<any>;
	public _column: Array<ITableColumn>;

	public _config: ITableConfig = {
		id: this.TABLEID,
		rowIdKey: 'id',
		gridHeight: 'auto',
		rowContentHeight: 30,
		loadType: 'oneshot',
		externalFilter: false,
		action: {
			enabled: true,
			list: [
				{
					name: "View Errors",
					custom: true,
					callback: (_rowNode, _tableApi): void => {
						if (this.IsError) {
							let errors1 = [];
							let errorsBody = ''
							errors1 = Object.keys(this.imprtData[_rowNode.data.id].errors)
							errors1.forEach(k => {
								errorsBody += this.imprtData[_rowNode.data.id].errors[k]
							})
							let toasterConfig: any = {
								title: 'Row  failed to import',
								body: errorsBody,
								showCloseButton: true,
								tapToDismiss: false,
							};
							this._KdAlert.error(toasterConfig);
						} else {
							let toasterConfig: any = {
								title: 'Import Success',
								body: 'The file "OpenEMIS_Exams_Import_Candidate Template.xlsx" is successfully imported.',
								showCloseButton: true,
								tapToDismiss: false,
							};
							this._KdAlert.success(toasterConfig);
						}

					}
				}
			]
		},
		paginationConfig: {
			pagesize: this.PAGESIZE,
			total: this.TOTALROWS
		},
		click: {
			type: 'none',
			callback: (): void => {
			}
		}
	};


	private _toolbarSearchSub: Subscription;
	private _tableSub: Subscription;
	constructor(
		_pageEvent: KdPageBaseEvent,
		public _router: Router,
		_activatedRoute: ActivatedRoute,
		private _tableEvent: KdTableEvent,
		private _toolbarEvent: KdToolbarEvent,
		public sharedSvc: SharedService,
		public _KdAlert: KdAlertEvent
	) {
		super({
			router: _router,
			pageEvent: _pageEvent,
			activatedRoute: _activatedRoute
		});
		var importDataList = this.sharedSvc.getCandidateImportDetail();

		if (importDataList != undefined) {
			this.csvDataSuccess = [];
			this.miniDashboardData[0].value = importDataList.data.total_count;
			this.miniDashboardData[1].value = importDataList.data.records_added.count;
			this.miniDashboardData[2].value = importDataList.data.records_updated.count;
			this.miniDashboardData[3].value = importDataList.data.records_failed.count;
			if (importDataList.data.records_failed.rows.length > 0) {
				this.imprtData = []
				let toasterConfig: any = {
					title: 'Import Failed',
					body: 'The file OpenEMIS_Exams_Import_Candidate_Template.xlsx failed to import completely.',
					showCloseButton: true,
					tapToDismiss: false,
				};
				this.imprtData = importDataList.data.records_failed.rows
				importDataList.data.records_added.rows.forEach((data1) => {
					this.csvDataSuccess.push(data1)
				})
				importDataList.data.records_updated.rows.forEach((data2) => {
					this.csvDataSuccess.push(data2)

				})
				this.loading = false;
				this._KdAlert.error(toasterConfig);
			}
			else if (importDataList.data.records_failed.count == 0) {
				this.csvDataSuccess = []

				//this._config.action.list[0].name="View"
				this.csvDataSuccess = [];
				this._config.action.enabled = false
				this.button[0].disabled = true;
				this.imprtData = []
				this.IsError = false;
				let toasterConfig: any = {
					title: 'Import Success',
					body: 'The file "OpenEMIS_Exams_Import_Candidate Template.xlsx" is successfully imported.',
					showCloseButton: true,
					tapToDismiss: false,
				};
				importDataList.data.records_added.rows.forEach((data1) => {
					this.csvDataSuccess.push(data1)
				})
				importDataList.data.records_updated.rows.forEach((data2) => {
					this.csvDataSuccess.push(data2)

				})
				this.imprtData = this.csvDataSuccess
				this.loading = false;
				this._KdAlert.success(toasterConfig);
			}

		} else {
			this._router.navigate(['/main'])
		}
	}

	ngOnInit(): void {
		console.log(this._config)
		super.setPageTitle('File Upload', false);
		super.setToolbarMainBtns([]);

		super.updatePageHeader();
		super.updateBreadcrumb();
		timer(1000).subscribe((): void => {
			this._column = [
				TABLECOLUMN.RowNumber,
				TABLECOLUMN.CandidateID,
				// TABLECOLUMN.OpenEMISID,
				TABLECOLUMN.FirstName,
				TABLECOLUMN.MiddleName,
				TABLECOLUMN.ThirdName,
				TABLECOLUMN.LastName,
				TABLECOLUMN.PreferredName,
				TABLECOLUMN.GenderCode,
				TABLECOLUMN.DateofBirth,
				TABLECOLUMN.Address,
				TABLECOLUMN.Postal,
				TABLECOLUMN.AddressAreaCode,
				TABLECOLUMN.BirthplaceAreaCode,
				TABLECOLUMN.AccountTypeCode,
				TABLECOLUMN.NationalityId,
				TABLECOLUMN.IdentityTypeCode,
				TABLECOLUMN.IdentityNumber,
				TABLECOLUMN.ContactType,
				TABLECOLUMN.Contact,
				TABLECOLUMN.SpecialNeedsAssessmentDate,
				TABLECOLUMN.SpecialNeedsType,
				TABLECOLUMN.SpecialNeedsDifficulties,
				TABLECOLUMN.ExamCentre,
				TABLECOLUMN.AttendanceType,
				TABLECOLUMN.Option1,
				TABLECOLUMN.CF1,
				TABLECOLUMN.Option2,
				TABLECOLUMN.CF2,
				TABLECOLUMN.Option3,
				TABLECOLUMN.CF3,
				TABLECOLUMN.Option4,
				TABLECOLUMN.CF4,
				TABLECOLUMN.Option5,
				TABLECOLUMN.CF5,
				TABLECOLUMN.Option6,
				TABLECOLUMN.CF6,
				TABLECOLUMN.RegistrationFee,
				TABLECOLUMN.PaymentDate,
				TABLECOLUMN.AmountPaid,
				TABLECOLUMN.ReceiptNumber
			];
		});

		timer(2000).subscribe((): void => {
			this._row = CREATE_ROW(this.TOTALROWS, this.STARTINDEX, this.imprtData);
		});

		this._toolbarSearchSub = this._toolbarEvent.onSendSearchText().subscribe((_text: string): void => {
			this._tableApi.general.searchRow(_text);
		}
		);

		this._tableSub = this._tableEvent.onKdTableEventList(this.TABLEID).subscribe((_event: any): void => {
			console.log(_event);
		});
	}



	public _buttonEvent(formVal: any): void {
		// console.log("formVal..formVal",formVal)
		let setCsvData = {};
		if (formVal.key === 'failed') {
			this.csvData = [];
			for (let i = 0; i < this.imprtData.length; i++) {
				setCsvData = {
					'Candidate ID (Leave as blank for new entries)': this.imprtData[i].data['Candidate ID (Leave as blank for new entries)'],
					'First Name': this.imprtData[i].data['First Name'],
					'Middle Name': this.imprtData[i].data['Middle Name'],
					'Third Name': this.imprtData[i].data['Third Name'],
					'Last Name': this.imprtData[i].data['Last Name'],
					'Preferred Name': this.imprtData[i].data['Preferred Name'],
					'Gender Code (M/F)': this.imprtData[i].data['Gender Code (M/F)'],
					'Date Of Birth ( DD/MM/YYYY )': this.imprtData[i].data['Date Of Birth ( DD/MM/YYYY )'],
					'Address': this.imprtData[i].data['Address'],
					'Postal': this.imprtData[i].data['Postal'],
					'Address Area Code': this.imprtData[i].data['Address Area Code'],
					'Birthplace Area Code': this.imprtData[i].data['Birthplace Area Code'],
					'Account Type Code': this.imprtData[i].data['Account Type Code'],
					'Nationality Id': this.imprtData[i].data['Nationality Id'],
					'Identity Type Code': this.imprtData[i].data['Identity Type Code'],
					'Identity Number': this.imprtData[i].data['Identity Number'],
					'Contact Type': this.imprtData[i].data['Contact Type'],
					'Contact': this.imprtData[i].data['Contact'],
					'Special Needs Assessment Date ( DD/MM/YYYY ) (Optional)': this.imprtData[i].data['Special Needs Assessment Date ( DD/MM/YYYY ) (Optional)'],
					'Special Needs Difficulties (Optional)': this.imprtData[i].data['Special Needs Type (Optional)'],
					'Special Needs Type (Optional)': this.imprtData[i].data['Special Needs Difficulties (Optional)'],
					'Exam Centre': this.imprtData[i].data['Exam Centre'],
					'Attendance Type': this.imprtData[i].data['Attendance Type'],
					'Option 1': this.imprtData[i].data['Option 1'],
					'CF 1': this.imprtData[i].data['CF'],
					'Option 2': this.imprtData[i].data['Option 2'],
					'CF 2': this.imprtData[i].data['CF'],
					'Option 3': this.imprtData[i].data['Option 3'],
					'CF 3': this.imprtData[i].data['CF'],
					'Option 4': this.imprtData[i].data['Option 4'],
					'CF 4': this.imprtData[i].data['CF'],
					'Option 5': this.imprtData[i].data['Option 5'],
					'CF 5': this.imprtData[i].data['CF'],
					'Option 6': this.imprtData[i].data['Option 6'],
					'CF 6': this.imprtData[i].data['CF'],
					'Registration Fee Type (Optional)': '',// this.imprtData[i].data['Registration Fee ($)'],,
					'Payment Date ( DD/MM/YYYY ) (Optional)': '',
					'Amount Paid ($) (Optional)': '',
					'Receipt Number': ''//this.imprtData[i].data['Receipt Number'],
				}
				let keys = Object.keys(this.imprtData[i].errors);
				keys.forEach(k => {
					setCsvData[k] = this.imprtData[i].errors[k]
				})
				this.csvData.push(setCsvData)
			}
			if (this.csvData != undefined) {
				this.JSONToCSVConvertor(this.csvData, 'Failed Records Candidate Sheet', true)
			}
		} else {
			this.csvData = [];
			for (let i = 0; i < this.csvDataSuccess.length; i++) {
				setCsvData = {
					'Candidate ID (Leave as blank for new entries)': this.csvDataSuccess[i].data['Candidate ID (Leave as blank for new entries)'],
					'First Name': this.csvDataSuccess[i].data['First Name'],
					'Middle Name': this.csvDataSuccess[i].data['Middle Name'],
					'Third Name': this.csvDataSuccess[i].data['Third Name'],
					'Last Name': this.csvDataSuccess[i].data['Last Name'],
					'Preferred Name': this.csvDataSuccess[i].data['Preferred Name'],
					'Gender Code (M/F)': this.csvDataSuccess[i].data['Gender Code (M/F)'],
					'Date Of Birth ( DD/MM/YYYY )': this.csvDataSuccess[i].data['Date Of Birth ( DD/MM/YYYY )'],
					'Address': this.csvDataSuccess[i].data['Address'],
					'Postal': this.csvDataSuccess[i].data['Postal'],
					'Address Area Code': this.csvDataSuccess[i].data['Address Area Code'],
					'Birthplace Area Code': this.csvDataSuccess[i].data['Birthplace Area Code'],
					'Account Type Code': this.csvDataSuccess[i].data['Account Type Code'],
					'Nationality Id': this.csvDataSuccess[i].data['Nationality Id'],
					'Identity Type Code': this.csvDataSuccess[i].data['Identity Type Code'],
					'Identity Number': this.csvDataSuccess[i].data['Identity Number'],
					'Contact Type': this.csvDataSuccess[i].data['Contact Type'],
					'Contact': this.csvDataSuccess[i].data['Contact'],
					'Special Needs Assessment Date ( DD/MM/YYYY ) (Optional)': this.csvDataSuccess[i].data['Special Needs Assessment Date ( DD/MM/YYYY ) (Optional)'],
					'Special Needs Difficulties (Optional)': this.csvDataSuccess[i].data['Special Needs Type (Optional)'],
					'Special Needs Type (Optional)': this.csvDataSuccess[i].data['Special Needs Difficulties (Optional)'],
					'Exam Centre': this.csvDataSuccess[i].data['Exam Centre'],
					'Option 1': this.csvDataSuccess[i].data['Option 1'],
					'CF': this.csvDataSuccess[i].data['CF'],
					'Option 2': this.csvDataSuccess[i].data['Option 2'],
					'CF 2': this.csvDataSuccess[i].data['CF'],
					'Option 3': this.csvDataSuccess[i].data['Option 3'],
					'CF 3': this.csvDataSuccess[i].data['CF'],
					'Option 4': this.csvDataSuccess[i].data['Option 4'],
					'CF 4': this.csvDataSuccess[i].data['CF'],
					'Option 5': this.csvDataSuccess[i].data['Option 5'],
					'CF 5': this.csvDataSuccess[i].data['CF'],
					'Option 6': this.csvDataSuccess[i].data['Option 6'],
					'CF 6': this.csvDataSuccess[i].data['CF'],
					'Registration Fee Type (Optional)': '',// this.imprtData[i].data['Registration Fee ($)'],
					'Payment Date ( DD/MM/YYYY ) (Optional)': '',
					'Amount Paid ($) (Optional)': '',
					'Receipt Number': ''//this.imprtData[i].data['Receipt Number'],
				}
				this.csvData.push(setCsvData)
			}
			if (this.csvData != undefined) {
				console.log(this.csvData.length)
				this.JSONToCSVConvertor(this.csvData, 'SuccesFully uploaded candidate Sheet', true)
			}
		}
	}

	ngOnDestroy(): void {
		super.destroyPageBaseSub();
	}

	JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
		var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
		var CSV = '';
		CSV += ReportTitle + '\r\n\n';
		if (ShowLabel) {
			var row = "";
			for (var index in arrData[0]) {
				row += index + ',';
			}
			row = row.slice(0, -1);
			CSV += row + '\r\n';
		}
		for (var i = 0; i < arrData.length; i++) {
			var row = "";
			for (var index in arrData[i]) {
				row += '"' + arrData[i][index] + '",';
			}
			row.slice(0, row.length - 1);
			CSV += row + '\r\n';
		}

		if (CSV == '') {
			alert("Invalid data");
			return;
		}
		var fileName = "OpenEmis_";
		fileName += ReportTitle.replace(/ /g, "_");
		var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
		var link = document.createElement("a");
		link.href = uri;
		link.download = fileName + ".csv";
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}
}

export interface ITableActionApi {
	deleteThisRow?: () => void;
}