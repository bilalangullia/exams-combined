export interface IMiniDashboardItem {
    type: string;
    icon?: string;
    label: string;
    value: number | string ;
}

export interface IMiniDashboardConfig {
    closeButtonDisabled?: boolean;
    rtl?: boolean;
}


export const MINI_DASHBOARD_CONFIG: IMiniDashboardConfig = {
    closeButtonDisabled: true,
    rtl: true,
};

export const MINI_DASHBOARD_DATA: Array<IMiniDashboardItem> = [
    {
        type: 'text',
        label: 'Total Rows :',
        value: '8'
    },
    {
        type: 'text',
        label: 'Row Imported :',
        value: '8'
    },
    {
        type: 'text',
        label: 'Row Update :',
        value: '8'
    },
    {
        type: 'text',
        label: 'Row Failed :',
        value: '0'
    },
]