import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImportMainComponent } from './import-main.component';
import { ImportFormComponent } from './import-form/import-form.component';
import { ImportListComponent } from './import-list/import-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'form', pathMatch: 'full' },
      { path: 'form', component: ImportFormComponent },
      { path: 'list', component: ImportListComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportRoutingModule { }
