export const file_INPUT={
    file_inputs:   {
   
           'key': 'fileinput_double_buttons',
           'label': 'Select File To Import',
           'visible': true,
           'required': true,
           'controlType': 'file-input',
           'type': 'file',
           'config': { 'infoText': [
              {
                  'text': 'Format Supported: xls, xlsx, ods, zip '
              },{
                  'text':'File size should not be larger than 512KB.'
              },{
                  'text':'Recommended Maximum Records: 2000'
              }
          ],
               'leftToolbar': true,
               'leftButton': [
                   {
                       'icon': 'kd-download',
                       'label': 'Download',
                       'callback': (): void => {
                           exportToExcel();
                       }
                   }
               ]
           }
       }
       
   }
   
const exportToExcel= ()=> {
    
   var uri = '/exams/assets/excelformat/OpenEMIS_Import_candidateList_Template.xlsx';
   var link = document.createElement("a");
   link.href = uri;
   link.download = 'OpenEMIS_Import_candidateList_Template.xlsx';
   document.body.appendChild(link);
   link.click();
   document.body.removeChild(link);
}

// key in the following object is the name of the column and value in the following obj is the keyon which the range will be found in api.
export const VALIDATION_FOR_HEADINGS =
    {
        'Gender Code (M/F)': 'Gender',        
        'Address Area Code': 'AddressArea',        
        'Birthplace Area Code': 'BirthplaceArea',        
        'Account Type Code': 'AccountType',        
        'Nationality Id': 'Nationality',        
        'Identity Type Code': 'IdentityType',        
        'Contact Type': 'ContactType',        
        'Exam Centre': 'ExamCentre',        
        'Attendance Type': 'AttendanceType',        
        'CF': 'CarryForward',        
        'Option 1': 'Option',      
        'Option 2': 'Option',        
        'Option 3': 'Option',        
        'Option 4': 'Option',        
        'Option 5': 'Option',        
        'Option 6': 'Option',        
    };