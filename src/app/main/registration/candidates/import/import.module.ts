import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportRoutingModule } from './import-routing.module';
import { ImportMainComponent } from './import-main.component';
import { ImportFormComponent } from './import-form/import-form.component';
import { ImportListComponent } from './import-list/import-list.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ImportRoutingModule,
    SharedModule
  ],
  declarations: [ImportMainComponent, ImportFormComponent, ImportListComponent ]
})
export class ImportModule { }
