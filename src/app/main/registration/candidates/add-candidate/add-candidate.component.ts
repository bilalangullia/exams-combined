import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, ITreeApi, KdTreeDropdownEvent } from 'openemis-styleguide-lib';
import { Observable, timer, Subscriber, Subscription } from 'rxjs';
import { SharedService } from '../../../../shared/shared.service';
import { DataService } from '../../../../shared/data.service';
import { TREE_CONFIG_AREA, TREE_CONFIG_ADDRESS, TREE_CONFIG_BIRTH, TABLE_COLUMN_LIST } from './add.config';

@Component({
  selector: 'app-add-candidate',
  templateUrl: './add-candidate.component.html',
  styleUrls: ['./add-candidate.component.css']
})
export class AddCandidateComponent extends KdPageBase implements OnInit, OnDestroy {
  public maxDate = new Date();
  public candidateData: any;
  public _questionBase: Array<any> = [
    {
      key: 'academic_period_id',
      label: 'Academic Period',
      visible: true,
      required: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'examination_id',
      label: 'Exam Name',
      visible: true,
      required: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'area_id',
      label: 'Area Name',
      visible: true,
      required: true,
      controlType: 'tree',
      config: TREE_CONFIG_AREA
    },
    {
      key: 'examination_centre_id',
      label: 'Exam Center',
      visible: true,
      required: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'examination_attendance_types_id',
      label: 'Attendance Type',
      visible: true,
      required: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'openemis_no',
      label: 'OpenEMIS ID',
      visible: true,
      order: 1,
      controlType: 'text',
      type: 'autocomplete',
      autocomplete: true,
      clickToggleDropdown: true,
      placeholder: 'Search EMIS ID',
      onEnter: true,
      dropdownCallback: (params: any): Observable<any> => {
        return new Observable((_observer: Subscriber<any>): any => {
          let data: Array<any> = [];

          // console.log('Params usable:', params);
          if (params.query && params.query.length > 2) {
            this.dataService.searchOpenEmisID(params.query).subscribe(
              (res: any) => {
                if (res && res.data.length) {
                  timer(500).subscribe((): void => {
                    data = res.data;
                    _observer.next(data);
                    _observer.complete();
                  });
                } else {
                  _observer.next([]);
                  _observer.complete();
                  this.setBlankValues();
                }
              },
              (err) => {
                console.log(err);
                _observer.next([]);
                _observer.complete();
                this.setBlankValues();
              }
            );
          }
        });
      }
    },
    {
      key: 'candidate_id',
      label: 'Previous Candidate ID',
      visible: true,
      order: 1,
      controlType: 'text',
      format: 'string',
      clickToggleDropdown: true,
      type: 'autocomplete',
      autocomplete: true,
      placeholder: 'Search Candidate ID',
      onEnter: true,
      dropdownCallback: (params: any): Observable<any> => {
        return new Observable((_observer: Subscriber<any>): any => {
          let data: Array<any> = [];
          // console.log('Params usable:', params);
          if (params.query && params.query.length > 2) {
            this.dataService.searchCandidateId(params.query).subscribe(
              (res: any) => {
                if (res && res.data.length) {
                  timer(500).subscribe((): void => {
                    data = res.data;
                    _observer.next(data);
                    _observer.complete();
                  });
                } else {
                  _observer.next([]);
                  _observer.complete();
                  this.setBlankValues();
                }
              },
              (err) => {
                console.log(err);
                _observer.next([]);
                _observer.complete();
                this.setBlankValues();
              }
            );
          }
        });
      }
    },
    {
      key: 'first_name',
      label: 'First Name',
      visible: true,
      required: true,
      order: 1,
      controlType: 'text',
      placeholder: 'First Name',
      type: 'text'
    },
    {
      key: 'middle_name',
      label: 'Middle Name',
      visible: true,
      order: 1,
      controlType: 'text',
      placeholder: 'Middle Name',
      type: 'text'
    },
    {
      key: 'third_name',
      label: 'Third Name',
      visible: true,
      order: 1,
      controlType: 'text',
      placeholder: 'Third Name',
      type: 'text'
    },
    {
      key: 'last_name',
      label: 'Last Name',
      visible: true,
      required: true,
      order: 1,
      controlType: 'text',
      placeholder: 'Last Name',
      type: 'text'
    },
    {
      key: 'gender_id',
      label: 'Gender',
      visible: true,
      required: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'date_of_birth',
      label: 'Date of Birth',
      visible: true,
      required: true,
      order: 1,
      controlType: 'date',
      config: {
        firstDayOfWeek: 1
      },
      minDate: '1970-01-01',
      maxDate: `${this.maxDate.getFullYear()}-${this.maxDate.getMonth() + 1}-${this.maxDate.getDate()}`
    },
    {
      key: 'nationality_id',
      label: 'Nationality',
      visible: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'identity_type_id',
      label: 'Identity Type',
      visible: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'identity_number',
      label: 'Identity Number',
      visible: true,
      order: 1,
      controlType: 'text',
      type: 'text',
      placeholder: 'Identity Number'
    },
    {
      key: 'address',
      label: 'Address',
      visible: true,
      order: 1,
      type: 'text',
      placeholder: 'Address',
      controlType: 'textarea'
    },
    {
      key: 'postal_code',
      label: 'Postal Code',
      visible: true,
      order: 1,
      controlType: 'text',
      placeholder: 'Postal Code',
      type: 'text'
    },
    {
      key: 'address_area_id',
      label: 'Address Area',
      visible: true,
      controlType: 'tree',
      config: TREE_CONFIG_ADDRESS
    },
    {
      key: 'birthplace_area_id',
      label: 'Birthplace Area',
      visible: true,
      controlType: 'tree',
      config: TREE_CONFIG_BIRTH
    },
    {
      key: 'special_need_type_id',
      label: 'Special Needs Assessment Type',
      visible: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'special_need_difficulty_id',
      label: 'Difficulties',
      visible: true,
      order: 1,
      controlType: 'dropdown',
      options: [
        {
          key: 'null',
          value: '--Select--'
        }
      ],
      events: true
    },
    {
      key: 'options',
      label: 'Options',
      visible: true,
      required: false,
      controlType: 'table',
      row: [],
      column: [TABLE_COLUMN_LIST.option_code, TABLE_COLUMN_LIST.option_name, TABLE_COLUMN_LIST.carry_forward],
      config: {
        id: 'optionsTable',
        rowIdKey: 'id',
        gridHeight: 400,
        loadType: 'oneshot',
        selection: {
          type: 'all',
          value: [],
          returnKey: ['option_id', 'option_code', 'option_name', 'carry_forward']
        },
        paginationConfig: {
          pagesize: 10,
          total: 100
        },
        click: {
          type: 'selection'
        }
      }
    }
  ];
  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline',
    }
  ];
  public defaultValues: any = {};
  public api: IDynamicFormApi = {};
  public countryRegion: Array<Object> = [];
  public formValue: any = {};
  private _smallLoaderValue: number = null;
  public isValues: boolean = false;

  constructor(
    public pageEvent: KdPageBaseEvent,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private _treeDropdownEvent: KdTreeDropdownEvent,
    public sharedService: SharedService,
    public dataService: DataService,
    private cd: ChangeDetectorRef
  ) {
    super({
      router: router,
      pageEvent: pageEvent,
      activatedRoute: activatedRoute
    });
  }

  ngOnInit() {
    super.updateBreadcrumb();
    super.setPageTitle('Registrations - Candidates', false);
    super.updatePageHeader();

    this.sharedService.getDropdownValues().subscribe((data) => {
      this.defaultValues = data;
      if (this.defaultValues) {
        let keyArr = Object.keys(this.defaultValues.data);
        keyArr.forEach((e) => {
          let quesObj = this._questionBase.find((q) => {
            return q.key == e;
          });
          if (quesObj) {
            quesObj.options.push(
              ...this.defaultValues.data[e].map((item) => {
                return { key: item.id, value: item.name };
              })
            );
            this.formValue.academic_period_id = quesObj.options[0].id;
          }
        });
        this.getAreaName();
        // this.requiredCheck();
      }
    });
  }

  requiredCheck() {
    let hasError: boolean;
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]['required']) {
        if (
          !this.formValue[this._questionBase[i]['key']] ||
          this.formValue[this._questionBase[i]['key']] == '' ||
          this.formValue[this._questionBase[i]['key']] == 'null' ||
          this.formValue[this._questionBase[i]['key']] == {}
        ) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]['key'], 'errors', ['This field is required']);
          }, 1000);
          break;
        }
      }
    }
    if (hasError) {
      return false;
    } else {
      return true;
    }
  }

  getAreaName() {
    this.dataService.getAreaName().subscribe((data: any) => {
      TREE_CONFIG_AREA.list = data.data;
      this.getAddressArea();
    });
  }

  getAddressArea() {
    this.dataService.getAddressArea().subscribe((data: any) => {
      TREE_CONFIG_ADDRESS.list = TREE_CONFIG_BIRTH.list = data.data;
      this.isValues = true;
    });
  }

  detectValue(question: any): void {
    // console.log('CreateNode - Detect Value from question: ', question);
    this.formValue[question.key] = question.value;
    this.callSpecific(question);
    if (question['required']) {
      if (!question['value'] || question['value'] == '' || question['value'] == 'null') {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 1000);
      } else {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', []);
        }, 1000);
        // this.requiredCheck();
      }
    }
  }

  callSpecific(data) {
    switch (data.key) {
      case 'academic_period_id': {
        this.getExamination(data.value);
        // this.getExamCenter(this.formValue);
        break;
      }

      case 'examination_id': {
        this.getOptions(this.formValue);
        this.getExamCenter(this.formValue);
        break;
      }

      case 'examination_attendance_types_id': {
        this.getOptions(this.formValue);
        break;
      }

      case 'area_id': {
        // this.getExamCenter(this.formValue);
        break;
      }

      case 'candidate_id': {
        if (data.value.key) {
          this.getCandidateDetails('candidate_id', data.value.value);
        }
        break;
      }

      case 'openemis_no': {
        if (data.value.key) {
          this.getCandidateDetails('openemis_no', data.value.value);
        }
        break;
      }
    }
  }

  getExamination(id) {
    if (id != 'null') {
      this.dataService.getExamination(id).subscribe(
        (data: any) => {
          this.formValue.examination_id = data.data[0].id;
          let temp = [
            ...data.data.map((item) => {
              return { key: item.id, value: item.name };
            })
          ];
          this.api.setProperty('examination_id', 'options', temp);
          this.getExamCenter(this.formValue);
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      let temp = [
        {
          key: 'null',
          value: '--Select--'
        }
      ];
      this.api.setProperty('examination_id', 'options', temp);
    }
  }

  getExamCenter(data) {
    if (data) {
      this.dataService.getOptionsExamCenter(data.examination_id).subscribe(
        (data: any) => {
          this.formValue.examination_centre_id = data.data[0].id;
          let temp = [
            ...data.data.map((item) => {
              return { key: item.id, value: item.code + ' - ' + item.name };
            })
          ];
          this.api.setProperty('examination_centre_id', 'options', temp);
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      let temp = [
        {
          key: 'null',
          value: '--Select--'
        }
      ];
      this.api.setProperty('examination_centre_id', 'options', temp);
      return;
    }
  }

  getOptions(data) {
    if (data && data.examination_id && data.examination_attendance_types_id) {
      if (!(data.examination_id == '') && !(data.examination_attendance_types_id == '')) {
        let tempData = {
          examination_id: data.examination_id,
          examination_attendance_types_id: data.examination_attendance_types_id
        };
        this.dataService.getSubjects(tempData).subscribe(
          (data: any) => {
            this.api.setProperty('options', 'row', data.data);
          },
          (err) => {
            this.api.setProperty('options', 'row', []);
            let toasterConfig: any = {
              type: 'error',
              title: 'Subject not found for this selected exam and attendance type ',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        );
      }
    } else {
      this.api.setProperty('options', 'row', []);
      return;
    }
  }

  getCandidateDetails(str, data) {
    switch (str) {
      case 'candidate_id': {
        this.dataService.getPastExamCandidateDetail(data).subscribe(
          (data: any) => {
            if (data) {
              this.provideData(data.data[0]);
            } else {
              this.setBlankValues();
            }
          },
          (err) => {
            console.log(err);
          }
        );
        break;
      }

      case 'openemis_no': {
        this.dataService.getOpenEmisCandidateDetail(data).subscribe(
          (data: any) => {
            if (data) {
              this.provideData(data.data[0]);
            } else {
              this.setBlankValues();
            }
          },
          (err) => {
            console.log(err);
          }
        );
      }
    }
  }

  provideData(data) {
    this.candidateData = {
      first_name: data.first_name,
      middle_name: data.middle_name,
      third_name: data.third_name,
      last_name: data.last_name,
      gender_id: data.gender_id.key,
      date_of_birth: data.date_of_birth,
      nationality_id: data.nationality_id.key,
      identity_type_id: data.identity_type_id.key,
      identity_number: data.identity_number,
      address: data.address,
      postal_code: data.postal_code,
      address_area_id: data.address_area_id.key,
      birthplace_area_id: data.birthplace_area_id.key,
      special_need_type_id: data.special_need_type_id.key,
      special_need_difficulty_id: data.special_need_difficulty_id.key
    };
    this.setValues(this.candidateData);
  }

  setValues(data) {
    let temp = [
      'candidate_id',
      'openemis_no',
      'academic_period_id',
      'examination_id',
      'area_id',
      'examination_centre_id',
      'examination_attendance_types_id',
      'options'
    ];
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        let isFind: boolean = false;
        for (let j = 0; j < temp.length; j++) {
          if (this._questionBase[i].key == temp[j]) {
            isFind = true;
            break;
          }
        }
        if (!isFind) {
          this.api.setProperty(this._questionBase[i].key, 'value', data ? data[this._questionBase[i].key] : '');
        }
      }
    }, 1000);
  }

  setBlankValues() {
    let temp = [
      'candidateId',
      'openemis_no',
      'academic_period_id',
      'examination_id',
      'areaName',
      'examination_centre_id',
      'examination_attendance_types_id',
      'options'
    ];
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        let isFind: boolean = false;
        for (let j = 0; j < temp.length; j++) {
          if (this._questionBase[i].key == temp[j]) {
            isFind = true;
            break;
          }
        }
        if (!isFind) {
          if (this._questionBase[i].controlType == 'dropdown') {
            this.api.setProperty(this._questionBase[i].key, 'value', 'null');
          } else {
            this.api.setProperty(this._questionBase[i].key, 'value', '');
          }
        }
      }
    }, 1000);
  }

  submitVal(formVal: any): void {
    if (this.requiredCheck()) {
      let playLoads = {
        academic_period_id: this.formValue.academic_period_id,
        examination_id: this.formValue.examination_id,
        area_id: formVal.area_id[0].id,
        examination_centre_id: this.formValue.examination_centre_id,
        examination_attendance_types_id: formVal.examination_attendance_types_id,
        openemis_no: formVal.openemis_no,
        candidate_id: formVal.candidate_id,
        first_name: formVal.first_name,
        middle_name: formVal.middle_name,
        third_name: formVal.third_name,
        last_name: formVal.last_name,
        gender_id: formVal.gender_id,
        date_of_birth: formVal.date_of_birth,
        nationality_id: formVal.nationality_id,
        identity_type_id: formVal.identity_type_id,
        identity_number: formVal.identity_number,
        address: formVal.address,
        postal_code: formVal.postal_code,
        address_area_id: formVal.address_area_id,
        birthplace_area_id: formVal.birthplace_area_id,
        special_need_type_id: formVal.special_need_type_id,
        special_need_difficulty_id: formVal.special_need_difficulty_id,
        options: formVal.options
      };
      console.log('CreateNode - Submit Button Clicked with formVal: ', playLoads);
      this.dataService.register(playLoads).subscribe(
        (data: any) => {
          let toasterConfig: any = {
            type: 'success',
            title: 'Registered Successfully',
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this.router.navigate(['/main']);
        },
        (err) => {
          let toasterConfig: any = {
            type: 'error',
            title:
              err.error.message == 'Identity Number Already Exist'
                ? 'Identity Number Already Exist'
                : 'Something went wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
        }
      );
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Mandatory Fields',
        body: 'Please provide Mandatory Fields data',
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
    }
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
  reset() {
    this.router.navigate(['/main/registration/candidates/list']);
  }
}
