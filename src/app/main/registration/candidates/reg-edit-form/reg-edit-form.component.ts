import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IDynamicFormApi, KdFormEvent } from 'openemis-styleguide-lib';
import { SharedService } from "../../../../shared/shared.service"
import { TREE_CONFIG_ADDRESS, TREE_CONFIG_BIRTH, TABLE_COLUMN_LIST } from '../../candidates/add-candidate/add.config';
import { DataService } from 'src/app/shared/data.service';

@Component({
  selector: 'app-reg-form',
  templateUrl: './reg-edit-form.component.html',
  styleUrls: ['./reg-edit-form.component.css'],
  providers: [KdFormEvent]

})
export class RegEditFormComponent extends KdPageBase implements OnInit, OnDestroy {

  public maxDate = new Date();
  public _questionBase: Array<any> = [
    {
      'key': 'academic_period_id',
      'label': 'Academic Period',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'readonly': true
    },
    {
      'key': 'examination_id',
      'label': 'Examination',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'readonly': true,
    },
    {
      'key': 'area_id',
      'label': 'Area Name',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'readonly': true,
    },
    {
      'key': 'examination_centre_id',
      'label': 'Exam Center',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'readonly': true,
    },
    {
      'key': 'examination_attendance_types_id',
      'label': 'Attendance Type',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'readonly': true
    },
    {
      'key': 'openemis_no',
      'label': 'OpenEMIS ID',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'readonly': true
    },
    {
      'key': 'candidate_id',
      'label': 'Candidate ID',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'readonly': true
    },
    {
      'key': 'first_name',
      'label': 'First Name',
      'visible': true,
      'required': true,
      'order': 1,
      'controlType': 'text',
      'placeholder': 'First Name',
      'type': 'text'
    },
    {
      'key': 'middle_name',
      'label': 'Middle Name',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'placeholder': 'Middle Name',
      'type': 'text',
    },
    {
      'key': 'third_name',
      'label': 'Third Name',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'placeholder': 'Third Name',
      'type': 'text',
    },
    {
      'key': 'last_name',
      'label': 'Last Name',
      'visible': true,
      'required': true,
      'order': 1,
      'controlType': 'text',
      'placeholder': 'Last Name',
      'type': 'text',
    },
    {
      'key': 'gender_id',
      'label': 'Gender',
      'visible': true,
      'required': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
      'events': true,
    },
    {
      'key': 'date_of_birth',
      'label': 'Date of Birth',
      'visible': true,
      'required': true,
      'order': 1,
      'controlType': 'date',
      'config': {
        'firstDayOfWeek': 1,
      },
      'minDate': '1970-01-01',
      'maxDate': `${this.maxDate.getFullYear()}-${this.maxDate.getMonth() + 1}-${this.maxDate.getDate()}`
    },
    {
      'key': 'nationality_id',
      'label': 'Nationality',
      'visible': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
      'events': true,
    },
    {
      'key': 'identity_type_id',
      'label': 'Identity Type',
      'visible': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
      'events': true,
    },
    {
      'key': 'identity_number',
      'label': 'Identity Number',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'type': 'text',
      'placeholder': 'Identity Number',
    },
    {
      'key': 'address',
      'label': 'Address',
      'visible': true,
      'order': 1,
      'type': 'text',
      'placeholder': 'Address',
      'controlType': 'textarea',
    },
    {
      'key': 'postal_code',
      'label': 'Postal Code',
      'visible': true,
      'order': 1,
      'controlType': 'text',
      'placeholder': 'Postal Code',
      'type': 'text',
    },
    {
      'key': 'address_area_id',
      'label': 'Address Area',
      'visible': true,
      'controlType': 'tree',
      'config': TREE_CONFIG_ADDRESS
    },
    {
      'key': 'birthplace_area_id',
      'label': 'Birthplace Area',
      'visible': true,
      'controlType': 'tree',
      'config': TREE_CONFIG_BIRTH
    },
    {
      'key': 'special_need_type_id',
      'label': 'Special Needs Assessment Type',
      'visible': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
      'events': true,
    },
    {
      'key': 'special_need_difficulty_id',
      'label': 'Difficulties',
      'visible': true,
      'order': 1,
      'controlType': 'dropdown',
      'options': [{
        'key': 'null',
        'value': '--Select--'
      }],
      'events': true,
    },
    {
      'key': 'options',
      'label': 'Options',
      'visible': true,
      'required': false,
      'controlType': 'table',
      'row': [],
      'column': [
        TABLE_COLUMN_LIST.option_code,
        TABLE_COLUMN_LIST.option_name,
        TABLE_COLUMN_LIST.carry_forward
      ],
      'config': {
        'id': 'optionsTable',
        'rowIdKey': 'id',
        'gridHeight': 400,
        'loadType': 'oneshot',
        'selection': {
          'type': 'all',
          'value': [],
          'returnKey': ['option_id','option_code', 'option_name', 'carry_forward']
        },
        'paginationConfig': {
          'pagesize': 10
        },
        'click': {
          'type': 'selection'
        }
      }
    }
  ];
  public _formButtons: Array<any> = [
    {
      type: 'submit',
      name: 'Save',
      icon: 'kd-check',
      class: 'btn-text'
    },
    {
      type: 'reset',
      name: 'Cancel',
      icon: 'kd-close',
      class: 'btn-outline'
    }
  ];
  public defaultValues: any;
  public formValue: any = {};
  public api: IDynamicFormApi = {};
  public candidateDetails: any;
  public isValues: boolean = false;
  public _smallLoaderValue: number = null;

  constructor(public pageEvent: KdPageBaseEvent, public router: Router, activatedRoute: ActivatedRoute, public sharedService: SharedService, private dataService: DataService) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }

  ngOnInit() {
    super.setPageTitle('Registration - Candidates', false);
    super.setToolbarMainBtns([
      {
        type: "back",
        path: "/main"
      }
    ]);
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.getAddressArea();
    this.sharedService.getDropdownValues().subscribe(
      (data: any) => {
        this.defaultValues = data.data;
        if (this.defaultValues) {
          let keyArr = Object.keys(this.defaultValues);
          for (let i = 0; i < keyArr.length; i++) {
            let quesObj = this._questionBase.find((q) => { return q.key == keyArr[i] });
            if ((quesObj.key == 'academic_period_id') || (quesObj.key == 'examination_attendance_types_id')) {
              continue;
            } else {
              quesObj.options.push(...this.defaultValues[keyArr[i]].map(item => { return { key: item.id, value: item.name } }));
            }
          }
          this.getCandidateEditDetials();
        }
      }
    )

  }

  getAddressArea() {
    this.dataService.getAddressArea().subscribe(
      (data: any) => {
        TREE_CONFIG_ADDRESS.list = TREE_CONFIG_BIRTH.list = data.data;
        // this.isValues = true;
      }
    )
  }

  getCandidateEditDetials() {
    let tempId = this.sharedService.getCandidateId();
    if (tempId != undefined) {
      this.dataService.getCandidateEditDetails(tempId).subscribe(
        (res: any) => {
          this.candidateDetails = res.data[0];
          this.setTableSelected();
          this.getOptions(this.candidateDetails);
        },
        err => {
          console.log(err);
          let toasterConfig: any = {
            type: 'error',
            title: 'Something Went Wrong',
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 3000
          };
          this.sharedService.setToaster(toasterConfig);
          this.router.navigate(['/main']);
        }
      )
    }
    else {
      this.router.navigate(['/main']);
    }
  }

  requiredCheck() {
    let hasError: boolean;
    for (let i = 0; i < this._questionBase.length; i++) {
      if (this._questionBase[i]['required']) {
        if ((!this.formValue[this._questionBase[i]['key']]) || (this.formValue[this._questionBase[i]['key']] == '') || (this.formValue[this._questionBase[i]['key']] == 'null') || (this.formValue[this._questionBase[i]['key']] == {})) {
          hasError = true;
          setTimeout(() => {
            this.api.setProperty(this._questionBase[i]['key'], 'errors', ['This field is required']);
          }, 1000)
          break;
        }
      }
    }
    if (hasError) {
      return false;
    }
    else {
      return true;
    }
  }

  setTableSelected() {
    let tempArr: Array<number> = [];
    this.candidateDetails.options.data.forEach(element => {
      tempArr.push(element.id);
    });
    let quesObj = this._questionBase.find((e) => { return e.key == 'options' });
    quesObj.config.selection.value = [...tempArr];
  }

  getOptions(data) {
    if (data && data.examination_id.key && data.examination_attendance_types_id.key) {
      if (!(data.examination_id.key == '') && !(data.examination_attendance_types_id.key == '')) {
        let tempData = {
          examination_id: data.examination_id.key,
          examination_attendance_types_id: data.examination_attendance_types_id.key
        }
        this.dataService.getSubjects(tempData).subscribe(
          (res: any) => {
            this.isValues = true;
            this.provideData(data, res.data);
          },
          err => {
            console.log(err);
            let toasterConfig: any = {
              type: 'error',
              title: 'Something Went Wrong',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 3000
            };
            this.sharedService.setToaster(toasterConfig);
            this.router.navigate(['/main']);
          }
        )
      }
    } else {
      let toasterConfig: any = {
        type: 'error',
        title: 'Something Went Wrong',
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 3000
      };
      this.sharedService.setToaster(toasterConfig);
      this.router.navigate(['/main']);
      // this.api.setProperty('options', 'row', []);
      return;
    }
  }

  public detectValue(question: any): void {
    // console.log('CreateNode - Detect Value from question: ', question);
    this.formValue[question.key] = question.value;
    if (question['required']) {
      if ((!question['value']) || (question['value'] == '') || (question['value'] == 'null')) {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', ['This field is required']);
        }, 1000);
      } else {
        setTimeout(() => {
          this.api.setProperty(question['key'], 'errors', []);
        }, 1000);
        // this.requiredCheck();
      }
    }
  }

  provideData(data, subjects) {
    let temp = {
      academic_period_id: data.academic_period_id.value,
      examination_id: data.examination_id.value,
      area_id: data.area_id,
      examination_centre_id: data.examination_centre_id,
      examination_attendance_types_id: data.examination_attendance_types_id.value,
      openemis_no: data.openemis_no,
      candidate_id: data.candidate_id.value,
      first_name: data.first_name,
      middle_name: data.middle_name,
      third_name: data.third_name,
      last_name: data.last_name,
      gender_id: data.gender_id.key,
      date_of_birth: data.date_of_birth,
      nationality_id: data.nationality_id.key,
      identity_type_id: data.identity_type_id.key,
      identity_number: data.identity_number,
      address: data.address,
      postal_code: data.postal_code,
      address_area_id: data.address_area_id,
      birthplace_area_id: data.birthplace_area_id,
      special_need_type_id: data.special_need_type_id.key,
      special_need_difficulty_id: data.special_need_difficulty_id.key
    };
    this.setValues(temp, subjects);
  }

  public submitVal(formVal: any): void {
    // console.log('CreateNode - Submit Button Clicked with formVal: ', formVal);
    let tempArr = ['academic_period_id','area_id','candidate_id','examination_attendance_types_id','examination_centre_id','examination_id','openemis_no']
    tempArr.forEach((e)=>{
      delete formVal[e];
    });
    this.dataService.updateCandidate(this.candidateDetails.candidate_id,formVal).subscribe(
      data=>{
        let toasterConfig: any = {
          type: 'success',
          title: 'Candidate details updated successfully',
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
        this.router.navigate(['/main']);
      },
      err=>{
       
        
        let toasterConfig: any = {
          type: 'error',
          title: (err.error.message=='Identity Number Already Exist')?'Identity Number Already Exist':'Something went wrong',
          showCloseButton: true,
          tapToDismiss: false,
          timeout: 3000
        };
        this.sharedService.setToaster(toasterConfig);
      }
    )
  }

  setValues(data, subjects) {
    setTimeout(() => {
      for (let i = 0; i < this._questionBase.length; i++) {
        if (this._questionBase[i].key == 'options') {
          this.api.setProperty('options', 'row', subjects);
        } else {
          this.api.setProperty(this._questionBase[i].key, 'value', data ? data[this._questionBase[i].key] : '');
        }
      }
    }, 500);
  }

  buttonClicked(e) {

  }
  reset(){
    this.router.navigate(['/main']);    
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
