import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent } from 'openemis-styleguide-lib';

@Component({
  selector: 'app-fees-main',
  templateUrl: './fees-main.component.html',
  styleUrls: ['./fees-main.component.css']
})
export class FeesMainComponent extends KdPageBase implements OnInit, OnDestroy {
  constructor(public pageEvent: KdPageBaseEvent, router: Router, activatedRoute: ActivatedRoute) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });
  }

  ngOnInit() {
    super.setPageTitle('Registrations - Fees', false);
    super.updatePageHeader();
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
