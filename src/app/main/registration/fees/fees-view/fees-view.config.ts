export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period_id',
    label: 'Academic Period',
    visible: true,
    required: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_id',
    label: 'Examination',
    visible: true,
    required: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'examination_centre_id',
    label: 'Exam Centre',
    visible: true,
    required: false,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'openEMIS_Id',
    label: 'OpenEMIS ID',
    visible: true,
    required: false,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    required: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    required: false,
    controlType: 'textbox',
    format: 'string',
    type: 'string'
  },
  {
    key: 'fee_type',
    label: 'Fee Types',
    visible: true,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Type',
        field: 'name',
        sortable: false,
        filterable: false,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Quantity',
        field: 'qty',
        sortable: false,
        filterable: false,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Amount ($)',
        field: 'totalamount',
        sortable: false,
        filterable: false,
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'selectionNormal',
      rowIdKey: 'fee_type',
      gridHeight: 210,
      loadType: 'normal'
    }
  },
  {
    key: 'fee_total',
    label: 'Fee Total',
    visible: true,
    required: true,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'payment',
    label: 'Payments',
    visible: true,
    required: false,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Payment Date',
        field: 'date',
        sortable: false,
        filterable: false,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Amount ($)',
        field: 'amount',
        sortable: false,
        filterable: false,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Comment',
        field: 'comment',
        sortable: false,
        filterable: false,
        visible: true,
        class: 'ag-name'
      },
      {
        headerName: 'Created By',
        field: 'created_by',
        sortable: false,
        filterable: false,
        visible: true,
        class: 'ag-name'
      }
    ],
    config: {
      id: 'selectionNormal',
      rowIdKey: 'payment',
      gridHeight: 10,
      loadType: 'normal'
    }
  },
  {
    key: 'modified_by',
    label: 'Modified By',
    visible: true,
    required: false,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'modified_on',
    label: 'Modified On',
    visible: true,
    required: false,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_by',
    label: 'Created By',
    visible: true,
    required: false,
    controlType: 'text',
    format: 'string',
    type: 'string'
  },
  {
    key: 'created_on',
    label: 'Created On',
    visible: true,
    required: false,
    controlType: 'text',
    format: 'string',
    type: 'string'
  }
];
