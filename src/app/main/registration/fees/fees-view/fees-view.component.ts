import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../shared/shared.service';
import { IModalConfig } from '../../../../shared/shared.interfaces';
import { invalidIdError } from '../../../../shared/shared.toasters';
import { FeesService } from '../fees.service';
import { VIEWNODE_INPUT } from './fees-view.config';

@Component({
  selector: 'app-fees-view',
  templateUrl: './fees-view.component.html',
  styleUrls: ['./fees-view.component.css']
})
export class FeesViewComponent extends KdPageBase implements OnInit, OnDestroy {
  public _questionBase: any = VIEWNODE_INPUT;
  public loading: boolean = true;
  public showTable: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public modalConfig: IModalConfig;

  public candidateId: any;
  public candidateName: any;
  public openEMISId: any;
  public examinationId: any;

  /* Subscriptions */
  private candidateIdSub: Subscription;
  private feeDetailsSub: Subscription;
  private currentFiltersSub: Subscription;

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _activatedRoute: ActivatedRoute,
    private _router: Router,
    private sharedService: SharedService,
    private feesService: FeesService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
    super.setPageTitle('Registrations - Fees', false);
    super.setToolbarMainBtns([
      {
        type: 'back',
        callback: (): void => {
          this._router.navigate(['main/registration/fees/list']);
        }
      },
      {
        type: 'edit',
        callback: (): void => {
          this._router.navigate(['main/registration/fees/edit']);
        }
      }
    ]);
  }

  ngOnInit(): void {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.currentFiltersSub = this.feesService.currentFiltersChanged.subscribe((filters: any) => {
      if (
        filters &&
        filters['academic_period_id'] &&
        filters['academic_period_id']['key'] &&
        filters['examination_id'] &&
        filters['examination_id']['key'] &&
        filters['examination_centre_id'] &&
        filters['examination_centre_id']['key']
      ) {
        this.examinationId = filters['examination_id']['key'];
        this.candidateIdSub = this.feesService.candidateIdChanged.subscribe((candidateId) => {
          if (!candidateId) {
            this.sharedService.setToaster(invalidIdError);
            setTimeout(() => {
              this._router.navigate(['main/registration/fees/list']);
            }, 0);
          } else {
            this.candidateId = candidateId;
            this.feesService.getFeeDetails(this.candidateId, this.examinationId);
            this.feesService.candidateNameChanged.subscribe((name) => {
              this.candidateName = name;
            });
            this.feesService.openEMISIdChanged.subscribe((openEMISId) => {
              this.openEMISId = openEMISId;
            });

            this.feeDetailsSub = this.feesService.feeViewDetailsChanged.subscribe((details: any) => {
              if (details) {
                this.setFeeDetails({
                  ...details,
                  academic_period_id: filters['academic_period_id']['value'],
                  examination_id: filters['examination_id']['value'],
                  examination_centre_id: filters['examination_centre_id']['value'],
                  name: this.candidateName ? this.candidateName : '',
                  openEMIS_Id: this.openEMISId ? this.openEMISId : ''
                });
              }
            });
          }
        });
      } else {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this._router.navigate(['main/registration/fees/list']);
        }, 0);
      }
    });
  }

  setFeeDetails(details: any) {
    if (details) {
      let totalamount;
      timer(100).subscribe(() => {
        for (let i = 0; i < this._questionBase.length; i++) {
          if (this._questionBase[i].key == 'fee_type') {
            if (details && details.fee_type) {
              let amount = details.fee_type.reduce((a, b) => ({ totalamount: a.totalamount + b.totalamount }));
              details.fee_type.push(amount);
              if (amount && amount['totalamount']) {
                totalamount = amount['totalamount'];
              }
              this.api.setProperty(this._questionBase[i].key, 'row', details.fee_type ? details.fee_type : []);
            }
          } else if (this._questionBase[i].key == 'fee_total') {
            this.api.setProperty(this._questionBase[i].key, 'value', totalamount);
          } else if (this._questionBase[i].key == 'payment') {
            this.api.setProperty(this._questionBase[i].key, 'row', details.payment ? details.payment : []);
          } else {
            this.api.setProperty(
              this._questionBase[i].key,
              'value',
              !details[this._questionBase[i].key]
                ? ''
                : details[this._questionBase[i].key].value
                ? details[this._questionBase[i].key].value
                : details[this._questionBase[i].key]
            );
          }
        }
      });
      this.loading = false;
    }
  }

  ngOnDestroy(): void {
    if (this.candidateIdSub) {
      this.candidateIdSub.unsubscribe();
    }
    if (this.feeDetailsSub) {
      this.feeDetailsSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    this.feesService.resetFeeDetails();
    super.destroyPageBaseSub();
  }
}
