import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeesRoutingModule } from './fees-routing.module';
import { FeesMainComponent } from './fees-main.component';
import { FeesListComponent } from './fees-list/fees-list.component';
import { SharedModule } from '../../../shared/shared.module';
import { FeesViewComponent } from './fees-view/fees-view.component';
import { FeesEditComponent } from './fees-edit/fees-edit.component';
import { FeesService } from './fees.service';
import { FeesDataService } from './fees-data.service';

@NgModule({
  imports: [CommonModule, FeesRoutingModule, SharedModule],
  declarations: [FeesMainComponent, FeesListComponent, FeesViewComponent, FeesEditComponent],
  providers: [FeesService, FeesDataService]
})
export class FeesModule {}
