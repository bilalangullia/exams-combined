export interface IFilters {
  academic_period_id?: { key: number; value: string };
  examination_id?: { key: number; value: string };
  examination_centre_id?: { key: number; value: string };
}

export interface IFeesPayload {
  fee_type?: Array<{ fee_id: number; quantity: number }>;
  payment?: Array<{ date: string; receipt: 'string'; amount: number }>;
}
