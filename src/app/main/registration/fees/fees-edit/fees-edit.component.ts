import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { KdPageBase, KdPageBaseEvent, ITableFormUpdateParams, IDynamicFormApi } from 'openemis-styleguide-lib';

import { SharedService } from '../../../../shared/shared.service';
import { invalidIdError } from '../../../../shared/shared.toasters';
import { FeesService } from '../fees.service';
import { VIEWNODE_INPUT, FORM_BUTTONS } from './fees-edit.config';
import { IFeesPayload } from '../fees.interfaces';

@Component({
  selector: 'app-fees-edit',
  templateUrl: './fees-edit.component.html',
  styleUrls: ['./fees-edit.component.css']
})
export class FeesEditComponent extends KdPageBase implements OnInit, OnDestroy {
  public loading: boolean = true;
  public _smallLoaderValue: number = null;
  public api: IDynamicFormApi = {};
  public _questionBase: any = VIEWNODE_INPUT;
  public _formButtons: any = FORM_BUTTONS;

  private candidateId: any = null;
  private currentFilters: any = null;
  private candidateName: string = '';
  private openEmisId: any = null;
  private feesTypesList: Array<any> = [];
  private paymentsList: Array<any> = [];

  /* Subscriptions */
  private candidateIdSub: Subscription;
  private candidateNameSub: Subscription;
  private openEmisIdSub: Subscription;
  private currentFiltersSub: Subscription;
  private feeDetailsSub: Subscription;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    private sharedService: SharedService,
    private feesService: FeesService
  ) {
    super({ router, activatedRoute, pageEvent });

    super.setPageTitle('Registrations ­ Fees', false);
    super.setToolbarMainBtns([]);
  }

  ngOnInit() {
    super.updatePageHeader();
    super.updateBreadcrumb();

    this.loading = false;

    this.candidateIdSub = this.feesService.candidateIdChanged.subscribe((candidateId) => {
      if (!candidateId) {
        this.sharedService.setToaster(invalidIdError);
        setTimeout(() => {
          this.router.navigate(['main/registration/fees']);
        }, 500);
      } else {
        this.candidateId = candidateId;

        this.candidateNameSub = this.feesService.candidateNameChanged.subscribe((candidateName) => {
          if (candidateName) {
            this.candidateName = candidateName;
          }
        });

        this.openEmisIdSub = this.feesService.openEMISIdChanged.subscribe((openEmisId) => {
          if (openEmisId) {
            this.openEmisId = openEmisId;
          }
        });

        this.currentFiltersSub = this.feesService.currentFiltersChanged.subscribe((filters: any) => {
          if (
            filters &&
            filters['academic_period_id'] &&
            filters['examination_id'] &&
            filters['examination_centre_id']
          ) {
            this.currentFilters = { ...filters };
            this.feesService.getFeeDetails(this.candidateId, filters['examination_id']['key']);
          }
        });

        this.feeDetailsSub = this.feesService.feeViewDetailsChanged.subscribe((details) => {
          if (details) {
            setTimeout(() => {
              this.setDetails(details);
            }, 500);
          }
        });
      }
    });
  }

  setDetails(details: any) {
    this.feesTypesList = details['fee_type']
      ? [
          ...details['fee_type'].map((item) => ({
            ...item,
            fee_id: item['id'],
            amount: item['amount'],
            totalamount: item['totalamount']
          }))
        ]
      : [];
    this.updateFeeTotal();

    this.paymentsList = details['payment']
      ? [
          ...details['payment'].map((item) => ({
            id: item['fee_id'],
            date: { dateCol: item['date'] },
            amount: { amountCol: item['amount'] },
            receipt: { receiptCol: item['receipt'] }
          }))
        ]
      : [];
    this.updatePaymentTotal();

    this._questionBase.forEach((question) => {
      if (question['key'] == 'academic_period') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['academic_period_id']['value']);
      } else if (question['key'] == 'examination') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['examination_id']['value']);
      } else if (question['key'] == 'exam_centre') {
        this.api.setProperty(question['key'], 'value', this.currentFilters['examination_centre_id']['value']);
      } else if (question['key'] == 'openEMIS_id') {
        this.api.setProperty(question['key'], 'value', this.openEmisId);
      } else if (question['key'] == 'candidate_id') {
        this.api.setProperty(question['key'], 'value', this.candidateId);
      } else if (question['key'] == 'name') {
        this.api.setProperty(question['key'], 'value', this.candidateName);
      } else if (question['key'] == 'fee_type') {
        this.api.table('fee_type').addRow([...this.feesTypesList], true);

        this.feesTypesList.forEach((item) => {
          let rowItem: ITableFormUpdateParams = {
            questionKey: 'quantity',
            colId: 'quantity',
            rowId: item['id'],
            property: 'value',
            data: item['qty'] ? item['qty'] : 0
          };
          setTimeout(() => {
            this.api.table('fee_type').setTableCellInputProperty(rowItem);
          }, 0);
        });
      } else if (question['key'] == 'payment') {
        this.api.table('payment').addRow([...this.paymentsList], true);
      }
    });
  }

  gridButtonClicked(event) {
    if (event['formKey'] == 'payment') {
      this.api.table('payment').addRow([
        { id: this.paymentsList.length }
        // date: { dateCol: '2020-05-29', amount: { amountCol: 0 }, receipt: { receiptCol: '' } }
      ]);
    }
  }

  _detectGridCellQuestion(event: any) {
    if (event['formKey'] === 'fee_type' && event['params']['colId'] === 'quantity') {
      this.updateFeeAmount(event['params']);
    }
    // else if (event['formKey'] === 'payment' && event['params']['colId'] === 'amount') {
    //   this.updatePaymentAmount(event['params']);
    // }
  }

  updateFeeAmount(param: Array<any>) {
    let objIndex = this.feesTypesList.findIndex((item) => item['id'] == param['rowId']);
    this.feesTypesList[objIndex]['totalamount'] = param['value'] * this.feesTypesList[objIndex]['amount'];

    this.updateFeeTotal();
  }

  // updatePaymentAmount(param: Array<any>) {
  //   let objIndex = this.paymentsList.findIndex((item) => item['id'] == param['rowId']);
  //   this.paymentsList[objIndex]['totalamount'] = param['value'] * this.paymentsList[objIndex]['amount'];
  // }

  updateFeeTotal() {
    let feeTotal = this.feesTypesList.reduce((accumulator, item) => accumulator + item['totalamount'], 0);
    this.api.setProperty('fee_total', 'value', feeTotal);
  }

  updatePaymentTotal() {
    let paymentTotal = this.paymentsList.reduce((accumulator, item) => accumulator + item['amount']['amountCol'], 0);
    this.api.setProperty('payment_total', 'value', paymentTotal);
  }

  submit(form) {
    let payload: IFeesPayload = {
      fee_type: form['fee_type'].map((item) => ({ fee_id: item['fee_id'], quantity: item['quantity']['quantity'] })),
      payment: form['payment'].map((item) => ({
        date: item['date']['dateCol']['text'],
        amount: item['amount']['amountCol'],
        receipt: item['receipt']['receiptCol']
      }))
    };

    this.feesService.addFees(this.candidateId, payload);
  }

  cancel() {
    this.router.navigate(['main/registration/fees/view']);
  }

  ngOnDestroy(): void {
    if (this.candidateIdSub) {
      this.candidateIdSub.unsubscribe();
    }
    if (this.candidateNameSub) {
      this.candidateNameSub.unsubscribe();
    }
    if (this.openEmisIdSub) {
      this.openEmisIdSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    if (this.feeDetailsSub) {
      this.feeDetailsSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
