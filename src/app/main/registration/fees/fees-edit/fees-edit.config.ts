import { ITableActionApi } from 'openemis-styleguide-lib';

export const VIEWNODE_INPUT: Array<any> = [
  {
    key: 'academic_period',
    label: 'Academic Period',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'examination',
    label: 'Examination',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    disabled: true
  },
  {
    key: 'exam_centre',
    label: 'Exam Centre',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    disabled: true
  },
  {
    key: 'openEMIS_id',
    label: 'OpenEMIS ID',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    disabled: true
  },
  {
    key: 'candidate_id',
    label: 'Candidate ID',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    disabled: true
  },
  {
    key: 'name',
    label: 'Name',
    visible: true,
    order: 1,
    controlType: 'text',
    type: 'text',
    readonly: true,
    disabled: true
  },
  {
    key: 'fee_type',
    label: 'Fee Types',
    visible: true,
    required: false,
    controlType: 'table',
    row: [],
    column: [
      { headerName: 'Name', field: 'name', visible: true },
      {
        headerName: 'Quantity',
        field: 'quantity',
        type: 'input',
        config: { input: { key: 'quantity', controlType: 'number', visible: true, min: 0, max: 1000 } }
      },
      { headerName: 'Amount ($)', field: 'totalamount', visible: true }
    ],
    config: {
      id: 'fee_type',
      rowIdKey: 'id',
      gridHeight: 210,
      loadType: 'normal',
      action: {
        enabled: true,
        list: [
          {
            type: 'delete',
            callback: (node: any, tableApi: ITableActionApi): void => {
              tableApi.deleteThisRow();
            }
          }
        ]
      }
    }
  },
  {
    key: 'fee_total',
    label: 'Fee Total',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  },
  {
    key: 'payment',
    label: 'Payments',
    visible: true,
    required: false,
    controlType: 'table',
    row: [],
    column: [
      {
        headerName: 'Payment Date',
        field: 'date',
        type: 'input',
        config: { mode: 'edit', input: { key: 'dateCol', controlType: 'date', visible: true } }
      },
      {
        headerName: 'Amount ($)',
        field: 'amount',
        type: 'input',
        config: {
          mode: 'edit',
          input: {
            key: 'amountCol',
            controlType: 'number',
            visible: true,
            min: 0
            /* , max: 1000 */
          }
        }
      },
      {
        headerName: 'Receipt Number',
        field: 'receipt',
        type: 'input',
        config: { mode: 'edit', input: { key: 'receiptCol', controlType: 'text', visible: true } }
      }
    ],
    config: {
      id: 'payment',
      rowIdKey: 'id',
      gridHeight: 10,
      loadType: 'normal',
      action: {
        enabled: true,
        list: [
          {
            type: 'delete',
            callback: (node: any, tableApi: ITableActionApi): void => {
              tableApi.deleteThisRow();
            }
          }
        ]
      },
      button: [
        {
          label: 'Add New Payment',
          icon: 'fa fa-plus',
          type: 'insert'
        }
      ]
    }
  },
  {
    key: 'payment_total',
    label: 'Payment Total',
    visible: true,
    controlType: 'text',
    type: 'text',
    readonly: true
  }
];

export const FORM_BUTTONS: Array<any> = [
  {
    type: 'submit',
    name: 'Save',
    icon: 'kd-check',
    class: 'btn-text'
  },
  {
    type: 'reset',
    name: 'Cancel',
    icon: 'kd-close',
    class: 'btn-outline'
  }
];
