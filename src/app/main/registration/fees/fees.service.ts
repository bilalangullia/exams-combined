import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';

import { DataService } from '../../../shared/data.service';
import { SharedService } from '../../../shared/shared.service';
import { detailsNotFound, saveFail, saveSuccess, serverError } from '../../../shared/shared.toasters';
import { FeesDataService } from './fees-data.service';
import { IFilters } from './fees.interfaces';

@Injectable()
export class FeesService {
  private candidateId: any = null;
  private candidateName: string = null;
  private openEMISId: string = null;
  private currentFilters: any = null;
  private filterYear: any;
  private filterExam: any;
  private filterExamCenter: any;
  private feeViewDetails: any = null;

  public candidateIdChanged = new BehaviorSubject<number>(this.candidateId);
  public candidateNameChanged = new BehaviorSubject<string>(this.candidateName);
  public openEMISIdChanged = new BehaviorSubject<string>(this.openEMISId);
  public currentFiltersChanged = new BehaviorSubject<any>({ ...this.currentFilters });
  public filterYearChanged = new Subject();
  public filterExamChanged = new Subject();
  public filterExamCenterChanged = new Subject();
  public feeViewDetailsChanged = new BehaviorSubject<any>({ ...this.feeViewDetails });

  constructor(
    private router: Router,
    private dataService: DataService,
    private sharedService: SharedService,
    private feesDataService: FeesDataService
  ) {}

  /*** SET Current filters ***/
  setCurrentFilters(filters: IFilters) {
    this.currentFilters = { ...this.currentFilters, ...filters };
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  /*** Empty Current filters ***/
  emptyCurrentFilters() {
    this.currentFilters = {};
    this.currentFiltersChanged.next({ ...this.currentFilters });
  }

  setCandidateId(id: number) {
    if (id) {
      this.candidateId = id;
      this.candidateIdChanged.next(this.candidateId);
    }
  }

  setCandidateName(name: string) {
    if (name) {
      this.candidateName = name;
      this.candidateNameChanged.next(this.candidateName);
    }
  }

  setOpenEMISId(id: string) {
    if (id) {
      this.openEMISId = id;
      this.openEMISIdChanged.next(this.openEMISId);
    }
  }

  /***  LIST API ***/
  getList(academic_period_id, examination_id, examination_centre_id, start: number, end: number) {
    return this.feesDataService.getList(academic_period_id, examination_id, examination_centre_id, start, end);
  }

  /***  LIST | SEARCH API  ***/
  getListSearch(
    academic_period_id,
    examination_id,
    examination_centre_id,
    keyword: string,
    start?: number,
    end?: number
  ) {
    return this.feesDataService.getListSearch(
      academic_period_id,
      examination_id,
      examination_centre_id,
      keyword,
      start,
      end
    );
  }

  /*** LIST PAGE | FILTERS | Year***/
  getFilterYear() {
    let data = 'academicPeriods';
    this.dataService.getOptionsAcademicPeriod(data).subscribe(
      (res: any) => {
        if (res && res.data && res.data.academic_period_id) {
          this.filterYear = res.data.academic_period_id;
          this.filterYearChanged.next([...this.filterYear]);
        } else {
          this.filterYear = [];
          this.filterYearChanged.next([...this.filterYear]);
        }
      },
      (err) => {
        this.filterYear = [];
        this.filterYearChanged.next([...this.filterYear]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Exam Cert ***/
  getFilterExam(academic_id) {
    this.dataService.getOptionsExam(academic_id).subscribe(
      (res: any) => {
        if (res && res.data) {
          this.filterExam = res.data;
          this.filterExamChanged.next([...this.filterExam]);
        } else {
          this.filterExam = [];
          this.filterExamChanged.next([...this.filterExam]);
        }
      },
      (err) => {
        this.filterExam = [];
        this.filterExamChanged.next([...this.filterExam]);
      }
    );
  }

  /*** LIST PAGE | FILTERS | Exam Center ***/
  getFilterExamCenter(examination_id) {
    this.dataService.getOptionsExamCenter(examination_id).subscribe(
      (res: any) => {
        if (res && res.data) {
          this.filterExamCenter = res.data;
          this.filterExamCenterChanged.next([...this.filterExamCenter]);
        } else {
          this.filterExamCenter = [];
          this.filterExamCenterChanged.next([...this.filterExamCenter]);
        }
      },
      (err) => {
        this.filterExamCenter = [];
        this.filterExamCenterChanged.next([...this.filterExamCenter]);
      }
    );
  }

  /*** VIEW PAGE | Details  ***/
  getFeeDetails(candidate_id, examinationId) {
    this.feesDataService.getFeeDetails(candidate_id, examinationId).subscribe(
      (res: any) => {
        if (res && res['data'] && res['data'].length) {
          this.feeViewDetails = res.data[0];
          this.feeViewDetailsChanged.next({ ...this.feeViewDetails });
        } else {
          this.feeViewDetails = null;
          this.feeViewDetailsChanged.next({ ...this.feeViewDetails });
          this.sharedService.setToaster(detailsNotFound);
        }
      },
      (err) => {
        this.feeViewDetails = null;
        this.feeViewDetailsChanged.next({ ...this.feeViewDetails });
        this.sharedService.setToaster(detailsNotFound);
      }
    );
  }

  resetFeeDetails() {
    this.feeViewDetails = null;
    this.feeViewDetailsChanged.next(null);
  }

  addFees(candidateId: string, payload: any) {
    this.feesDataService.addFees(candidateId, payload).subscribe(
      (res) => {
        if (res && res['data']) {
          this.sharedService.setToaster({ ...saveSuccess, body: res['message'] });
          setTimeout(() => {
            this.router.navigate(['main/registration/fees']);
          }, 500);
        } else {
          this.sharedService.setToaster({ ...saveFail, body: res['message'] });
        }
      },
      (err: HttpErrorResponse) => {
        this.sharedService.setToaster({ ...serverError, body: err['error']['message'] });
      }
    );
  }
}
