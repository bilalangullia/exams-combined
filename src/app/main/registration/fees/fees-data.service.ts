import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import urls from '../../../shared/config.urls';

@Injectable()
export class FeesDataService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  /*** Add token to request ***/
  setHeader(): any {
    if (localStorage.getItem('token')) {
      let token = JSON.parse(window.atob(localStorage.getItem('token')));
      let headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      return { headers: headers };
    } else {
      let headers = new HttpHeaders({});
      return { headers: headers };
    }
  }

  /*** LIST | API  ***/
  getList(academic_period_id, examination_id, examination_centre_id, start?: number, end?: number) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.register}/${urls.fee}/list?start=${start}&end=${end}&examination_id=${examination_id}&academic_period_id=${academic_period_id}&examination_centre_id=${examination_centre_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** SEARCH | API  ***/
  getListSearch(
    academic_period_id,
    examination_id,
    examination_centre_id,
    keyword: string,
    start?: number,
    end?: number
  ) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.register}/${urls.fee}/list?start=${start}&end=${end}&examination_id=${examination_id}&academic_period_id=${academic_period_id}&examination_centre_id=${examination_centre_id}&keyword=${keyword}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /*** Get Details | VIEW | API  ***/
  getFeeDetails(candidate_id, examination_id) {
    return this.httpClient
      .get(
        `${environment.baseUrl}/${urls.register}/${urls.fee}/${candidate_id}/details/${examination_id}`,
        this.setHeader()
      )
      .pipe(catchError(this.handleError));
  }

  /* Edit | Save */
  addFees(candidateId: string, payload: any) {
    // http://openemis.n2.iworklab.com/api/register/fee/20FOA030008/add
    return this.httpClient
      .post(`${environment.baseUrl}/${urls.register}/${urls.fee}/${candidateId}/add`, { ...payload }, this.setHeader())
      .pipe(catchError(this.handleError));
  }

  /*** Handle Http Errors ***/
  handleError = (error: HttpErrorResponse | any) => {
    switch (error.status) {
      case 400:
        console.log(400);
        break;
      case 401:
        console.log(401);
        break;
      case 500:
        console.log(500);
        break;
      case 503:
        console.log(503);
        break;
    }
    console.log(error);
    return throwError(error);
  };
}
