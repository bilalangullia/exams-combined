import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeesMainComponent } from './fees-main.component';
import { FeesListComponent } from './fees-list/fees-list.component';
import { FeesViewComponent } from './fees-view/fees-view.component';
import { FeesEditComponent } from './fees-edit/fees-edit.component';
const routes: Routes = [
  {
    path: '',
    component: FeesMainComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: FeesListComponent },
      { path: 'view', component: FeesViewComponent },
      { path: 'edit', component: FeesEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeesRoutingModule {}
