interface Column {
  headerName?: string;
  field?: string;
  type?: 'input' | 'normal' | 'image';
  sortable?: boolean;
  filterable?: boolean;
  visible?: boolean;
  config?: any;
  class?: string;
  filterValue?: Array<string>;
}

interface ListColumn {
  Status?: Column;
  OutstandingFee?: Column;
  CandidateID?: Column;
  OpenEMISID?: Column;
  FirstName?: Column;
  ExamCentre?: Column;
}

export const TABLECOLUMN: ListColumn = {
  CandidateID: {
    headerName: 'Candidate ID',
    field: 'candidate_id',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  OpenEMISID: {
    headerName: 'OpenEMIS ID',
    field: 'openemis_no',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  FirstName: {
    headerName: 'Name',
    field: 'name',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  ExamCentre: {
    headerName: 'Exam Centre',
    field: 'center_code',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  OutstandingFee: {
    headerName: 'Outstanding Fee',
    field: 'outstanding_fee',
    sortable: true,
    filterable: true,
    filterValue: []
  },
  Status: {
    headerName: 'Status',
    field: 'status',
    sortable: true,
    filterable: true,
    filterValue: []
  }
};

export const FILTER_INPUTS: Array<any> = [
  {
    key: 'academic_period_id',

    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select  --' }],
    events: true
  },
  {
    key: 'examination_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'examination_centre_id',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  },
  {
    key: 'status',
    visible: true,
    required: true,
    order: 1,
    controlType: 'dropdown',
    options: [{ key: null, value: '-- Select --' }],
    events: true
  }
];

export interface ITableActionApi {
  deleteThisRow?: () => void;
}
