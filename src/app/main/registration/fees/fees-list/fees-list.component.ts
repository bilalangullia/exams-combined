import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, timer, BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ExcelExportParams } from 'ag-grid';

import {
  KdFilter,
  KdPageBase,
  KdPageBaseEvent,
  KdTableEvent,
  ITableConfig,
  ITableColumn,
  ITableApi,
  KdAlertEvent,
  KdTableDatasourceEvent,
  ITableDatasourceParams,
  IDynamicFormApi
} from 'openemis-styleguide-lib';

import { SharedService } from '../../../../shared/shared.service';
import { IFetchListParams } from '../../../../shared/shared.interfaces';
import { FeesService } from '../fees.service';
import { IFilters } from '../fees.interfaces';
import { TABLECOLUMN, FILTER_INPUTS } from './fees-list.config';

@Component({
  selector: 'app-fees-list',
  templateUrl: './fees-list.component.html',
  styleUrls: ['./fees-list.component.css'],
  providers: [KdTableEvent]
})
export class FeesListComponent extends KdPageBase implements OnInit, OnDestroy {
  @ViewChild('searchForm') _updateView: KdFilter;
  public questionBase: IFilters;
  readonly TOTALROW: number = 1000;
  readonly GRIDID: string = 'listNode';
  readonly PAGESIZE: number = 20;
  public api: IDynamicFormApi = {};

  public inputs: Array<any> = FILTER_INPUTS;
  public currentFilters: IFilters;
  public _row: Array<any>;
  public loading: boolean = true;
  public viewTable: boolean = false;
  public _tableApi: ITableApi = {};
  public showTable: boolean = false;
  private tableEventList: any;

  private searchKey: string = '';
  private searchKeyObservable = new BehaviorSubject<any>(null);

  private statusOptions: Array<any> = [
    { id: null, name: '-- select --' },
    { id: 0, name: 'Unpaid' },
    { id: 1, name: 'Paid' }
  ];

  /* Subscriptions */
  private tableEventSub: Subscription;
  private yearFilterSub: Subscription;
  private examFilterSub: Subscription;
  private examCenterFilterSub: Subscription;
  private currentFiltersSub: Subscription;
  private listSub: Subscription;

  public tableConfig: ITableConfig = {
    id: this.GRIDID,
    loadType: 'server',
    gridHeight: 500,
    externalFilter: false,
    paginationConfig: {
      pagesize: this.PAGESIZE,
      total: this.TOTALROW
    },
    action: {
      enabled: true,
      list: [
        {
          icon: 'far fa-eye',
          name: 'View',
          custom: true,
          callback: (_rowNode, _tableApi): void => {
            this.feesService.setCandidateId(_rowNode['data']['candidate_id']);
            this.feesService.setCandidateName(_rowNode['data']['name']);
            this.feesService.setOpenEMISId(_rowNode['data']['openemis_no']);
            setTimeout(() => {
              this.router.navigate(['main/registration/fees/view']);
            }, 100);
          }
        }
      ]
    }
  };

  public tableColumns: Array<ITableColumn> = [
    TABLECOLUMN.OpenEMISID,
    TABLECOLUMN.CandidateID,
    TABLECOLUMN.FirstName,
    TABLECOLUMN.ExamCentre,
    TABLECOLUMN.Status
  ];

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public pageEvent: KdPageBaseEvent,
    public tableEvent: KdTableEvent,
    private sharedService: SharedService,
    private feesService: FeesService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    super.setPageTitle('Registrations - Fees', false);
    super.setToolbarMainBtns([
      { type: 'import' },
      {
        type: 'export',
        callback: () => {
          if (this._tableApi.general) {
            let defaultParams: ExcelExportParams = {
              fileName: 'feesCandidates_list',
              allColumns: true,
              suppressTextAsCDATA: true
            };
            this._tableApi.general.exportToExcel(defaultParams);
          } else {
            let toasterConfig: any = {
              type: 'error',
              title: 'Cannot export to excel',
              showCloseButton: true,
              tapToDismiss: false,
              timeout: 1000
            };
            this.sharedService.setToaster(toasterConfig);
          }
        }
      }
    ]);
    super.enableToolbarSearch(true, (event: any): void => {
      this.searchKeyObservable.next(event);
      this.debounce(this.fetchList);
    });
  }

  ngOnInit() {
    this.loading = false;
    super.updatePageHeader();
    super.updateBreadcrumb();
    this.feesService.emptyCurrentFilters();

    timer(100).subscribe(() => {
      this.feesService.getFilterYear();
      this.yearFilterSub = this.feesService.filterYearChanged.subscribe((options: Array<any>) => {
        this.setFilterOptions('academic_period_id', options);
        this.setFilterOptions('status', this.statusOptions);
      });
    });

    this.examFilterSub = this.feesService.filterExamChanged.subscribe((options: Array<any>) => {
      this.setFilterOptions('examination_id', options);
      if (!options.length) {
        this.setFilterOptions('examination_id', []);
      }
    });

    this.examCenterFilterSub = this.feesService.filterExamCenterChanged.subscribe((options: Array<any>) => {
      this.setFilterOptions('examination_centre_id', options);
      if (!options.length) {
        this.setFilterOptions('examination_centre_id', []);
      }
    });

    this.tableEventSub = this.tableEvent.onKdTableEventList(this.GRIDID).subscribe((event: any): void => {
      if (event instanceof KdTableDatasourceEvent) {
        this.tableEventList = event;
        setTimeout(() => {
          this.fetchList(this.tableEventList, this.searchKey);
        }, 0);
      }
    });

    this.currentFiltersSub = this.feesService.currentFiltersChanged.subscribe((filters: IFilters) => {
      if (
        filters &&
        filters['academic_period_id'] &&
        filters['academic_period_id']['key'] &&
        filters['examination_id'] &&
        filters['examination_id']['key'] &&
        filters['examination_centre_id'] &&
        filters['examination_centre_id']['key']
      ) {
        this.currentFilters = { ...filters };
        this.showTable = false;
        this.feesService
          .getList(
            filters['academic_period_id']['key'],
            filters['examination_id']['key'],
            filters['examination_centre_id']['key'],
            0,
            1
          )
          .subscribe(
            (res: any) => {
              this.showTable = true;
              if (res && res['data'] && res['data']['total']) {
                this.tableConfig.paginationConfig.total = res['data'] && res['data']['total'];
                this.showTable = true;
              } else {
                this.tableConfig.paginationConfig.total = 0;
                this.showTable = false;
              }
            },
            (err) => {
              this.tableConfig.paginationConfig.total = 0;
              this.showTable = false;
            }
          );
      }
    });
  }

  setFilterOptions(key: string, data: Array<any>) {
    if (data.length) {
      let options: Array<any> = [];
      if (key === 'academic_period_id' || key === 'examination_id') {
        options = data.map((item) => ({ key: item['id'], value: item['name'] }));
        this._updateView.setInputProperty(key, 'options', options);
      } else if (key === 'examination_centre_id') {
        options = data.map((item) => ({ key: item['id'], value: `${item['code']} - ${item['name']}` }));
        this._updateView.setInputProperty(key, 'options', options);
      } else if (key === 'status') {
        options = data.map((item) => ({ key: item['id'], value: `${item['name']}` }));
        this._updateView.setInputProperty(key, 'options', options);
      }
      setTimeout(() => {
        this._updateView.setInputProperty(key, 'value', options[0]['key']);
      }, 0);
    } else {
      this._updateView.setInputProperty(key, 'options', [{ key: null, value: '-- Select --' }]);
      this._updateView.setInputProperty(key, 'value', null);
    }
  }

  fetchList(event: KdTableDatasourceEvent, searchKey?: string) {
    this.loading = true;
    let fetchParams: IFetchListParams = {};

    if (event['rowParams'] && event['rowParams']['startRow'] > -1 && event['rowParams']['endRow'] > -1) {
      fetchParams = { ...fetchParams, startRow: event['rowParams']['startRow'], endRow: event['rowParams']['endRow'] };
    }
    if (event['rowParams'] && event['rowParams']['sortModel'] && event['rowParams']['sortModel'].length) {
      fetchParams = {
        ...fetchParams,
        sort: {
          colId: event['rowParams']['sortModel']['colId'],
          order: event['rowParams']['sortModel']['order']
        }
      };
    }
    if (event['rowParams'] && event['rowParams']['filterModel']) {
    }

    let list: Array<any> = [];
    let total: number = null;
    let dataSourceParams: ITableDatasourceParams;
    let listReq: any;
    if (searchKey) {
      if (
        this.currentFilters &&
        this.currentFilters['academic_period_id'] &&
        this.currentFilters['academic_period_id']['key'] &&
        this.currentFilters['examination_id'] &&
        this.currentFilters['examination_id']['key'] &&
        this.currentFilters['examination_centre_id'] &&
        this.currentFilters['examination_centre_id']['key']
      ) {
        listReq = this.feesService.getListSearch(
          this.currentFilters['academic_period_id']['key'],
          this.currentFilters['examination_id']['key'],
          this.currentFilters['examination_centre_id']['key'],
          searchKey,
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    } else {
      if (
        this.currentFilters &&
        this.currentFilters['academic_period_id'] &&
        this.currentFilters['academic_period_id']['key'] &&
        this.currentFilters['examination_id'] &&
        this.currentFilters['examination_id']['key'] &&
        this.currentFilters['examination_centre_id'] &&
        this.currentFilters['examination_centre_id']['key']
      ) {
        listReq = this.feesService.getList(
          this.currentFilters['academic_period_id']['key'],
          this.currentFilters['examination_id']['key'],
          this.currentFilters['examination_centre_id']['key'],
          fetchParams['startRow'],
          fetchParams['endRow']
        );
      }
    }
    listReq.toPromise().then(
      (res: any) => {
        if (res && res['data'] && res['data']['userRecords'] && res['data']['userRecords'].length) {
          list = res['data']['userRecords'];
          total = this.searchKey && this.searchKey.length ? res['data']['userRecords'].length : res['data']['total'];
          list.forEach((item) => {
            item['center_code'] = `${item['center_code']}` + ' - ' + `${item['center_name']}`;
          });
        } else {
          list = [];
          total = 0;
          this.loading = false;
          this.showTable = false;
        }

        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      },
      (err) => {
        list = [];
        total = 0;
        dataSourceParams = { rows: list, total };
        event.subscriber.next(dataSourceParams);
        event.subscriber.complete();
        this.loading = false;
      }
    );
  }

  debounce(cb) {
    this.searchKeyObservable.pipe(debounceTime(1500)).subscribe((res: any) => {
      this.showTable = false;
      this.searchKey = res ? res : '';
      setTimeout(() => {
        this.showTable = true;
      }, 0);
    });
  }

  detectValue(question: any): void {
    let value = question['options'].find((item) => item['key'] == question['value']);
    this.feesService.setCurrentFilters({ [question['key']]: value });
    if (question['key'] === 'academic_period_id' && question['value']) {
      this.feesService.getFilterExam(question['value']);
    } else if (question['key'] === 'examination_id' && question['value']) {
      this.feesService.getFilterExamCenter(question['value']);
    }
  }

  resetFilterValues() {
    this.tableColumns.forEach((column) => {
      if (column['filterable']) {
        column.filterValue = [];
      }
    });
  }

  ngOnDestroy(): void {
    if (this.listSub) {
      this.listSub.unsubscribe();
    }
    if (this.yearFilterSub) {
      this.yearFilterSub.unsubscribe();
    }
    if (this.currentFiltersSub) {
      this.currentFiltersSub.unsubscribe();
    }
    if (this.examFilterSub) {
      this.examFilterSub.unsubscribe();
    }
    if (this.examCenterFilterSub) {
      this.examCenterFilterSub.unsubscribe();
    }
    if (this.tableEventSub) {
      this.tableEventSub.unsubscribe();
    }

    super.destroyPageBaseSub();
  }
}
