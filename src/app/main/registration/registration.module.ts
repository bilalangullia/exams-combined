import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationMainComponent } from './registration-main.component';
import { SharedModule } from '../../shared/shared.module';
import { RegistrationRoutingModule } from './registration.routing';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RegistrationRoutingModule
  ],
  declarations: [RegistrationMainComponent],
  
  bootstrap:[RegistrationMainComponent]
})
export class RegistrationModule { }
