import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KdPageBase, KdPageBaseEvent, IPageheaderApi, IPageheaderConfig } from 'openemis-styleguide-lib';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-registration-main',
  templateUrl: './registration-main.component.html',
  styleUrls: ['./registration-main.component.css']
})
export class RegistrationMainComponent extends KdPageBase implements OnInit {
  public pageTitle = 'Registrations - Candidates';
  public pageHeaderApi: IPageheaderApi = {};
  public pageheader: IPageheaderConfig = {};
  public breadcrumbList = {
    home: {
      name: 'jhgh',
      path: '/www.google.com'
    }
  };
  constructor(
    public pageEvent: KdPageBaseEvent,
    router: Router,
    activatedRoute: ActivatedRoute,
    public sharedService: SharedService
  ) {
    super({ router: router, pageEvent: pageEvent, activatedRoute: activatedRoute });

    this.pageEvent.onUpdatePageHeader().subscribe((_headerObj: IPageheaderConfig): void => {
      let newObj: IPageheaderConfig = Object.assign<IPageheaderConfig, IPageheaderConfig>({}, _headerObj);
      this.pageheader = newObj;
    });

    this.pageEvent.onUpdateBreadcrumb().subscribe((_breadcrumbObj) => {
      this.breadcrumbList = _breadcrumbObj;
    });
  }

  ngOnInit() {
    // super.updateBreadcrumb();
  }
}
