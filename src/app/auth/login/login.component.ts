import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { KdPageBase, KdPageBaseEvent, ILoginConfig, ILoginApi, KdAlertEvent } from 'openemis-styleguide-lib';

import { SharedService } from '../../shared/shared.service';
import { DataService } from '../../shared/data.service';
import { AuthService } from '../../shared/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [KdAlertEvent],
  host: { '[class.loginWraper]': 'someField' }
})
export class LoginComponent extends KdPageBase implements OnInit, OnDestroy {
  public isValues: boolean = true;
  public _smallLoaderValue: number = null;

  public loginApi: ILoginApi = {};
  someField: boolean = false;
  public config: ILoginConfig = {
    header: {
      icon: 'kd-openemis',
      // img: 'assets/img/background-image/login/bg-datamanager.jpg',
      // imgSize: 'medium',
      title: 'OpenEMIS Exams'
    },
    button: {
      callback: (params: any): void => {
        if (params.formValues.username != '' && params.formValues.password != '') {
          this.submit(params);
        } else {
          this.loginApi.setAlertMessage('danger', 'You have entered an invalid username or password');
        }
      }
    },
    formLinks: {
      forgetUser: {
        path: '#',
        label: 'Forget User'
      },
      forgetPassword: {
        path: '#',
        label: 'Forget Password'
      }
    },
    type: 'login'
  };

  constructor(
    public _pageEvent: KdPageBaseEvent,
    public _router: Router,
    public _activatedRoute: ActivatedRoute,
    public dataService: DataService,
    public sharedService: SharedService,
    private kdAlertEvent: KdAlertEvent,
    private authService: AuthService
  ) {
    super({
      router: _router,
      activatedRoute: _activatedRoute,
      pageEvent: _pageEvent
    });
  }

  ngOnInit() {
    this.someField = true;
  }

  submit(data) {
    this.isValues = false;
    let userData = {
      username: data.formValues.username,
      password: data.formValues.password
    };
    this.dataService.login(userData).subscribe(
      (res: any) => {
        this.isValues = true;
        if (res.data) {
          localStorage.setItem('token', window.btoa(JSON.stringify(res.data['token'])));
          this.authService.getPermissionsList(res['data']['token']);
        }
        this._router.navigate(['/main']);
      },
      (err: any) => {
        this.isValues = true;
        this.loginApi.setAlertMessage('danger', err.error.message);
      }
    );
  }

  ngOnDestroy(): void {
    super.destroyPageBaseSub();
  }
}
