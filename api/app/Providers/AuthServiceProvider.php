<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Policies\CheckIsAdminPolicy;
use App\Models\SecurityUser;
use App\Traits\isAdminTraits;


class AuthServiceProvider extends ServiceProvider
{
    use isAdminTraits;
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         //'App\Model' => 'App\Policies\ModelPolicy',
         SecurityUser::class => CheckIsAdminPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

       Gate::define('can-delete-userGRoup', 'App\Policies\CheckIsAdminPolicy@before');
    }
}
