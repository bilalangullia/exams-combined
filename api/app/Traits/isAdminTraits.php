<?php

namespace App\Traits;

use App\Models\ExaminationOption;
use Illuminate\Http\Request;
use JWTAuth;

trait isAdminTraits
{
    /**
     * Getting subject list
     * @param Request $request
     * @return mixed
     */
    public function isSuperAdmin()
    {
        try {
            $currentUser = JWTAuth::user();
            if ($currentUser->super_admin == 1) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('You Are Not Authorized To Access This Page');
        }
    }

}
