<?php

namespace App\Traits;

use App\Models\ExaminationOption;
use Illuminate\Http\Request;

trait SubjectTraits
{
    /**
     * Getting subject list
     * @param Request $request
     * @return mixed
     */
    public function getSubject($attendanceType, $examId)
    {
        try {
            $list = [];
            $count = 0;
            $subject = ExaminationOption::select('id', 'code', 'name', 'attendance_type_id', 'examination_subject_id', 'carry_forward')
                ->whereHas(
                    'attendanceType',
                    function ($query) use ($attendanceType) {
                        $query->where('id', $attendanceType);
                    }
                )->whereHas(
                    'examinationSubject.examName',
                    function ($query) use ($examId) {
                        $query->where('id', $examId);
                    }
                )->groupBy('id')->orderBy('code')->get()->map(
                    function ($item, $key) {
                        
                            return [
                                'id' => $item['id'].$item->carry_forward,
                                'option_id' => $item['id'],
                                'option_code' => $item['code'],
                                'option_name' => $item['name'],
                                'carry_forward' => ($item['carry_forward'] == 0) ? 'No' : 'Yes'
                            ];
                    
                    }
                );
            if ($subject->count()) {
                return $this->sendSuccessResponse("Subject List Found", $subject);
            } else {
                return $this->sendErrorResponse('Subject Not Found');
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Subject Not Found');
        }
    }

}
