<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use App\Models\ExaminationStudent;
use App\Models\ExaminationCentre;
use Maatwebsite\Excel\Concerns\Exportable;
//use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class CertificateExport implements FromArray//WithHeadings
{
	use Exportable;

	private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
    * @return @array
    */
    public function array(): array
    {	
    	 try{
    	 	$requestData = $this->data;
    	if(empty($requestData['reportData']['candidate_details']) && empty($requestData['reportData']['exam_centre_detail'])){
    	 	$examStudent1 = ExaminationStudent::where('examination_id', $requestData['reportData']['exam_details']['id'])->with('securityUser',
    	 		'examName',
    	 		'examName.academicPeriod',
    	 		'examName.examinationType',
    	 		'examinationStudentsOption',
    	 		'examinationStudentsOption.examinationOption',
    	 		'examinationStudentsOption.examinationOption.examinationSubject',
    	 		'examinationStudentsOption.examinationOption.examinationStudentsOptionsGrade',
                'examinationStudentsCertificate:examination_students_id,certificate_id'
    	 	)->get();
    	 	
    	 	$arr = [];
    	 	foreach ($examStudent1 as $key => $value) {
    	 		$arr[$key][] = $value['examName']['academicPeriod']['name'];
    	 		$arr[$key][] = $value['candidate_id'];
    	 		$arr[$key][] = isset($value['examinationStudentsCertificate']['certificate_id']) ? $value['examinationStudentsCertificate']['certificate_id'] : '';
    	 		$arr[$key][] = $value['securityUser']['full_name'];
    	 		$arr[$key][] = Carbon::parse($value['securityUser']['date_of_birth'])->format('d-M-y');
    	 		foreach ($value['examinationStudentsOption'] as $k => $val) {
					$arr[$key][] = $val['examinationOption']['examinationSubject']['name'];
    	 			$arr[$key][] = $val['examinationOption']['examinationSubject']['examName']['examinationType']['name'];
    	 			$arr[$key][]= isset($valss['examinationOption']['examinationStudentsOptionsGrade']['grade']) ? $valss['examinationOption']['examinationStudentsOptionsGrade']['grade'] : '';
    	 		}	
    	 	}
    	 	return $arr;
    	}

    	  elseif(empty($requestData['reportData']['candidate_details'])) {
    	 	$examStudent2 = ExaminationStudent::where('examination_id', $requestData['reportData']['exam_details']['id'])->where('examination_centre_id', $requestData['reportData']['exam_centre_detail']['id'])->with('securityUser',
    	 		'examName',
    	 		'examName.academicPeriod',
    	 		'examName.examinationType',
    	 		'examinationStudentsOption',
    	 		'examinationStudentsOption.examinationOption',
    	 		'examinationStudentsOption.examinationOption.examinationSubject',
    	 		'examinationStudentsOption.examinationOption.examinationStudentsOptionsGrade',
                'examinationStudentsCertificate:examination_students_id,certificate_id'
    	 	)->get();
    	 	
    	 	$array = [];
    	 	foreach ($examStudent2 as $key => $value) {
    	 		$array[$key][] = $value['examName']['academicPeriod']['name'];
    	 		$array[$key][] = $value['candidate_id'];
    	 		$array[$key][] = isset($value['examinationStudentsCertificate']['certificate_id']) ? $value['examinationStudentsCertificate']['certificate_id'] : '';
    	 		$array[$key][] = $value['securityUser']['full_name'];
    	 		$array[$key][] = Carbon::parse($value['securityUser']['date_of_birth'])->format('d-M-y');
    	 		foreach ($value['examinationStudentsOption'] as $k => $val) {
					$array[$key][] = $val['examinationOption']['examinationSubject']['name'];
    	 			$array[$key][] = $val['examinationOption']['examinationSubject']['examName']['examinationType']['name'];
    	 			$array[$key][]= isset($valss['examinationOption']['examinationStudentsOptionsGrade']['grade']) ? $valss['examinationOption']['examinationStudentsOptionsGrade']['grade'] : '';
    	 		}	
    	 	}
    	 	return $array;
    	 } else {
    	 	
    	 	$examStudent3 = ExaminationStudent::where('id', $requestData['reportData']['candidate_details']['id'])->with('securityUser',
    	 		'examName',
    	 		'examName.academicPeriod',
    	 		'examName.examinationType',
    	 		'examinationStudentsOption',
    	 		'examinationStudentsOption.examinationOption',
    	 		'examinationStudentsOption.examinationOption.examinationSubject',
    	 		'examinationStudentsOption.examinationOption.examinationStudentsOptionsGrade',
                'examinationStudentsCertificate:examination_students_id,certificate_id'
        )->get();

    	 	$array1 = [];
    	 	foreach ($examStudent3 as $key => $value) {
    	 		$array1[$key][] = $value['examName']['academicPeriod']['name'];
    	 		$array1[$key][] = $value['candidate_id'];
    	 		$array1[$key][] = isset($value['examinationStudentsCertificate']['certificate_id']) ? $value['examinationStudentsCertificate']['certificate_id'] : '';
    	 		$array1[$key][] = $value['securityUser']['full_name'];
    	 		$array1[$key][] = Carbon::parse($value['securityUser']['date_of_birth'])->format('d-M-y');
    	 		foreach ($value['examinationStudentsOption'] as $k => $val) {
					$array1[$key][] = $val['examinationOption']['examinationSubject']['name'];
    	 			$array1[$key][] = $val['examinationOption']['examinationSubject']['examName']['examinationType']['name'];
    	 			$array1[$key][]= isset($valss['examinationOption']['examinationStudentsOptionsGrade']['grade']) ? $valss['examinationOption']['examinationStudentsOptionsGrade']['grade'] : '';
    	 		}	
    	 	}
    	 	return $array1;
    	 }
    	} catch (\Exception $e) {
                echo $e;
                    Log::error(
                        'failed to fetch data from db',
                        ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                    );

                    return $this->sendErrorResponse('Certificates Report Not Generated'); 
        }
    }
}
