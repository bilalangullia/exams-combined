<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\ExaminationCentre;
use App\Models\ExaminationSubject;

class SubjectsPerCandidateExport implements FromArray, WithHeadings, WithStrictNullComparison, ShouldAutoSize
{
	/**
     * @var $data
     */
    protected $data;

    /**
     * CandidatePerSubjectExport Constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        ini_set('max_execution_time', 300);
        ini_set('memory_limit','256M');
    }

    /**
     * Generating Subject Per Candidate Report.
     * @return array
     */
    public function array(): array
    {
        try {
        	$data = $this->data;
	        $centres = ExaminationCentre::select(
	                    'id', 
	                    'name', 
	                    'code'
	                )
	        		->with(
	        			[
	        				'students' => function ($q) {
	        					$q->select('id', 'examination_centre_id')
                                    ->withCount('options');
	        				}
	        			]
	        		)
	                ->where('examination_id', $data['examination_id'])
	                ->get()
	                ->toArray();

            $subjects = $this->getSubjects($data['examination_id']);

		    $listData = [];
		    foreach ($centres as $key => $centre) {
		        $listData[$key] = [
		            $centre['code'],
		            $centre['name'],
		        ];

                foreach ($subjects as $k => $subject) {
                    $studentCount = 0;
                    $subjectCount = $k + 1;
                    foreach ($centre['students'] as $student) {
                        if ($student['options_count'] == $subjectCount) {
                            $studentCount = $studentCount + 1;
                        }
                    }
                    array_push($listData[$key], $studentCount);
                }
		    }
            
		    return $listData;
        } catch (\Exception $e) {
        	Log::error(
                'Failed to generate subjects per candidate statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate subjects per candidate statistics report");
        }
    }

    /**
     * Generating headings for Subject Per Candidate Excel Report.
     * @return array
     */
    public function headings(): array
    {
        $column = [];
        $column = [
        	'Exam Centre Code',
        	'Exam Centre Name',
        ];
        $data = $this->data;
        $subjects = $this->getSubjects($data['examination_id']);

        for($i = 1; $i <= count($subjects); $i ++) {
        	$val = 'Number of candidate(s) enrolled for ' . $i . ' subject ';
        	array_push($column, $val);
        }
        return $column;
    }

    /**
     * Getting Subjects List.
     * @return array
     */
    public function getSubjects($examId = 0)
    {
        try {
            $subjects = ExaminationSubject::select('id')
                        ->where('examination_id', $examId)
                        ->get()
                        ->toArray();

            return $subjects;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get subjects list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get subjects list");
        }
    }
}
