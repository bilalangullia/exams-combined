<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Models\ExaminationSubject;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationCentre;

class CandidatePerSubjectExport implements FromArray, WithHeadings, WithStrictNullComparison
{
    /**
     * @var $data
     */
    protected $data;

    /**
     * CandidatePerSubjectExport Constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        ini_set('max_execution_time', 300);
        ini_set('memory_limit','256M');
    }

    /**
     * Generating Candidate Per Subject Report.
     * @return array
     */
    public function array(): array
    {
        try {
        	$data = $this->data;
	        $examinationCentreId = $data['examination_centre_id'];
	        $centresList = ExaminationCentre::select('id', 'name', 'code', 'examination_id')
	                    ->where('examination_id', $data['examination_id']);

	        if ($examinationCentreId != 0) {
	            $centresList->where('id', $data['examination_centre_id']);
	        }
	                    
	        $centres = $centresList->with(
	                    [
	                        'exams' => function ($q) {
	                            $q->select('id', 'code', 'name');
	                        },
	                        'exams.examinationSubject' => function ($q) {
	                            $q->select('id', 'code', 'name', 'examination_id');
	                        },
	                        'exams.examinationSubject.options' => function ($q) {
	                            $q->select('id', 'examination_subject_id');
	                        }
	                    ]
	                )
	                ->get()
	                ->toArray();
	        
	        $listData = [];
	        foreach ($centres as $index => $centre) {
	            $exam_subjects = $centre['exams']['examination_subject'];
	            
	            foreach ($exam_subjects as $key => $subject) {
	                $optionIds = [];
	                foreach ($subject['options'] as $key => $option) {
	                    $optionIds[] = $option['id'];
	                }
	                
	                $studentCount = ExaminationStudentsOption::whereIn('examination_options_id', $optionIds)
	                	->WhereHas(
		                    'examinationStudent',
		                    function ($query) use ($centre) {
		                        $query->where('examination_centre_id', $centre['id']);
		                    }
		                )
	                	->count();
	                
	                $listData[] = [
	                    'centre_code' => $centre['code'],
	                    'centre_name' => $centre['name'],
	                    'subject_code' => $subject['code'],
	                    'subject_name' => $subject['name'],
	                    'candidate_enrolled' => $studentCount,
	                ];
	            }
	        }
	        return $listData;
        } catch (\Exception $e) {
        	Log::error(
                'Failed to generate candidate per subject statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate candidate per subject statistics report");
        }
    }

    public function headings(): array
    {
        return [
            'Exam Centre Code',
            'Exam Centre Name',
            'Subject Code',
            'Subject Name',
            'Candidates Enrolled',
        ];
    }
}
