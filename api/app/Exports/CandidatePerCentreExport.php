<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\ExaminationCentre;

class CandidatePerCentreExport implements FromArray, WithHeadings, WithStrictNullComparison, ShouldAutoSize
{
    /**
     * @var $data
     */
    protected $data;

    /**
     * CandidatePerSubjectExport Constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        ini_set('max_execution_time', 300);
        ini_set('memory_limit','256M');
    }

    /**
    * Generating Candidate Per Centre Report.
    */
    public function array(): array
    {
        try {
        	$data = $this->data;
	        $centres = ExaminationCentre::select(
	                    'id', 
	                    'name', 
	                    'code',
	                    'examination_id',
	                    'examination_centres_ownerships_id',
	                    'area_id'
	                )
	                ->where('examination_id', $data['examination_id'])
	                ->with(
	                    [
	                        'exams' => function ($q) {
	                            $q->select('id', 'code');
	                        },
	                        'area' => function ($q) {
	                            $q->select('id', 'name')
	                                ->where('visible', config('constants.visibleValue.visibleCode'));
	                        },
	                        'ownership' => function ($q) {
	                            $q->select('id', 'name')
	                                ->where('visible', config('constants.visibleValue.visibleCode'));
	                        },
	                        'students' => function ($q) {
	                            $q->select(
	                                'id', 
	                                'examination_centre_id', 
	                                'examination_id', 
	                                'student_id', 
	                                'examination_attendance_types_id'
	                            );
	                        },
	                        'students.securityUser' => function ($q) {
	                            $q->select('id', 'gender_id')
	                                ->where('status', config('constants.students.status'))
	                                ->where('is_student', config('constants.students.isStudent'));
	                        }
	                    ]
	                )
	                ->get()
	                ->toArray();
	    
		    $listData = [];
		    foreach ($centres as $key => $centre) {
		        $totalStudents = 0;
		        $maleStudents = 0;
		        $femaleStudents = 0;
		        $partTimeStudents = 0;
		        $fullTimeStudents = 0;
		        $students = $centre['students'];
		        foreach ($students as $k => $student) {
		            $totalStudents = $totalStudents + 1;
		            if ($student['examination_attendance_types_id'] == 1) {
		                $fullTimeStudents = $fullTimeStudents + 1;
		            } elseif($student['examination_attendance_types_id'] == 2) {
		                $partTimeStudents = $partTimeStudents + 1;
		            }

		            if ($student['security_user']['gender_id'] == 1) {
		                $maleStudents = $maleStudents + 1;
		            }   elseif($student['security_user']['gender_id'] == 2) {
		                $femaleStudents = $femaleStudents + 1;
		            }
		        }
		        $listData[] = [
		            'examCode' => $centre['exams']['code'],
		            'centreCode' => $centre['code'],
		            'centreName' => $centre['name'],
		            'centreArea' => $centre['area']['name'],
		            'ownership' => $centre['ownership']['name'],
		            'femaleStudents' => $femaleStudents,
		            'maleStudents' => $maleStudents,
		            'partTimeStudents' => $partTimeStudents,
		            'fullTimeStudents' => $fullTimeStudents,
		            'totalStudents' => $totalStudents,
		        ];
		    }
		    return $listData;
        } catch (\Exception $e) {
        	Log::error(
                'Failed to generate candidate per centre statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate candidate per centre statistics report");
        }
    }

    public function headings(): array
    {
        return [
            'Exam Code',
            'Centre Code',
            'Centre Name',
            'Centre Area',
            'Ownership',
            'Female Candidates',
            'Male Candidates',
            'Part-time Candidates',
            'Full-time Candidates',
            'Total Candidates',
        ];
    }
}
