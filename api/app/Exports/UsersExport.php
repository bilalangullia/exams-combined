<?php

namespace App\Exports;

use App\Models\AcademicPeriod;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '256M');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $requestData = $this->data;
        $academicPeriod = AcademicPeriod::where('id', $requestData['academic_period_id'])->select(
            'id',
            'start_year'
        )->get();
        return $academicPeriod;
    }

    public function headings(): array
    {
        return [
            'id',
            'start_year',
        ];
    }
}
