<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsOption;
use App\Models\ReportProgress;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;

class FormReportRepository extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function candidateDropdown(Request $request)
    {
        try {
            $list = ExaminationStudent::select('id', 'candidate_id')->with(
                'examinationStudentsOption:examination_students_id,examination_options_id',
                'examinationStudentsOption.examinationOption.examinationComponent:examination_options_id,id'
            )->whereHas(
                'examinationStudentsOption.examinationOption.examinationComponent',
                function ($query) use ($request) {
                    $query->where(
                        'component_types_id',
                        config('constants.componentType.type')
                    );
                }
            )->whereHas(
                'examinationCentre',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_centre_id);
                }
            )->whereHas(
                'examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            )->whereHas(
                'examName.academicPeriod',
                function ($query) use ($request) {
                    $query->where('id', $request->academic_period_id);
                }
            )->get()->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "candidate_id" => $item['candidate_id'],
                    ];
                }
            );

                return $list;

        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }

    /**
     * @param string $candidateId
     * @return \Illuminate\Http\JsonResponse`
     */
    public function candidateSelectSubject(Request $request, string $candidates)
    {
        try {
            $subject = ExaminationStudentsOption::where('examination_students_id', $candidates)->with(
                'examinationOption:id,examination_subject_id',
                'examinationOption.examinationSubject:id,name'
            )->whereHas(
                'examinationOption.examinationSubject.examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            )->get()->map(
                function ($item, $key) {
                    return [
                        "id" => $item['examinationOption']['examinationSubject']['id'],
                        "subject" => $item['examinationOption']['examinationSubject']['name'],
                    ];
                }
            );
            return $subject;
        } catch (\Exception $e) {
            Log::error(
                'Failed to add review into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Subject list not found');
        }
    }
    /**
     * To update the status , error message if any and completion date of report generation
     * @param $lastReportId
     * @param $status
     * @param $error_message
     * @return mixed
     */
    public function updateReportStatus($lastReportId, $status, $error_message)
    {
        $error['msg'] = "";
        try {
            ReportProgress::where('id', $lastReportId)->update(
                [
                    'status' => $status,
                    'error_message' => $error_message,
                    'modified' => Carbon::now()->toDateTimeString(),
                ]
            );
            Log::info(
                'Update status and error message of report',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'reportId' => $lastReportId,
                        'status' => $status,
                        'error_message' => $error_message,
                    ],
                ]
            );
        } catch (\Exception $e) {
            $error['msg'] = "something went wrong";
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $error;
        }
    }

    /**
     * To increment the current records till reaching total records while generating the report
     *
     * @param $lastReportId
     * @return mixed
     */

    public function updateCount($lastReportId)
    {
        $error['msg'] = "";
        try {
            ReportProgress::where('id', $lastReportId)->increment('current_records');
            Log::info(
                'Update count of the records in the report',
                ['method' => __METHOD__, 'data' => ['reportId' => $lastReportId]]
            );
        } catch (\Exception $e) {
            $error['msg'] = "something went wrong";
            Log::error(
                'Unable to update count of records in the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $error;
        }
    }

    /**
     * To create and save new report from report data
     * @param $saveReportData
     * @return mixed
     */
    public function createNewReport($saveReportData)
    {
        $error['msg'] = "";
        try {
            $reportObject = ReportProgress::create($saveReportData);
            Log::info(
                'New Report created in the database',
                ['method' => __METHOD__, 'data' => ['reportId' => $reportObject->id]]
            );
        } catch (\Exception $e) {
            $error['msg'] = "something went wrong";
            Log::error(
                'Failed to create a new report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $error;
        }

        if (!$reportObject) {
            $error['msg'] = "unable to create report";

            return $error;
        }

        return $reportObject->id;
    }

    /**
     * Getting Reports List
     * @param array $data
     * @return array
     */
    public function listReportsData(Request $request)
    {
        try {
            $data = $request->all();
            $total = 0;
            $reports1 = ReportProgress::select(
                'name',
                'expiry_date',
                'file_path',
                'status',
                'created_user_id',
                'created AS started',
                'modified AS completed'
            )
                ->where("module", "LIKE", "%" . $request->module . "%")
                ->orderBy('created', 'desc');

            if (isset($data['keyword'])) {
                $search = $data['keyword'];
                $reports1->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('status', 'LIKE', '%' . config('constants.reportUpdateStatus.' . $search) . '%')
                    ->orWhere('modified', 'LIKE', '%' . $search . '%');
            }

            if (isset($data['start']) && isset($data['end'])) {
                $reportsCount = $reports1;
                $total = $reportsCount->count();
                $reports1->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $reportsCount = $reports1;
                $total = $reportsCount->count();
            }

            $reports = $reports1->get();
            $data['total'] = $total;
            $data['reports'] = $reports;
            $reports = $reports1->get();
            $data['total'] = $total;
            $data['reports'] = $reports;
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list of reports from Database',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
        }
    }

     /**
     * @param $examId
     * @param $examCenterId
     * @param $candidateId
     * @return array
     */
    public function getCandidatesByExamIdAndExamCenterId($examId, $examCenterId, $candidateId)
    {
        if (empty($candidateId)) {
            try {
                $candidates = DB::table('examination_students')
                    ->join('security_users', 'examination_students.student_id', '=', 'security_users.id')
                    ->select(
                        'examination_students.id',
                        'examination_students.candidate_id',
                        'security_users.first_name',
                        'security_users.middle_name',
                        'security_users.third_name',
                        'security_users.last_name'
                    )
                    ->where('examination_students.examination_centre_id', '=', $examCenterId)
                    ->where('examination_students.examination_id', '=', $examId)
                    ->get();
                Log::info(
                    'Successfully Fetched all candidates details by exam id and exam centre id',
                    [
                        'method' => __METHOD__,
                        'data' => [
                            'allCandidatedData' => $candidates,
                            'examId' => $examId,
                            'examCentreId' => $examCenterId
                        ]
                    ]
                );
            } catch (\Exception $e) {
                Log::error(
                    'Failed to fetch all candidates details by exam id and exam centre id',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                return array();
            }
        } else {
            try {
                $candidates = DB::table('examination_students')
                    ->join('security_users', 'examination_students.student_id', '=', 'security_users.id')
                    ->select(
                        'examination_students.id',
                        'examination_students.candidate_id',
                        'security_users.first_name',
                        'security_users.middle_name',
                        'security_users.third_name',
                        'security_users.last_name'
                    )
                    ->where('examination_students.examination_centre_id', '=', $examCenterId)
                    ->where('examination_students.examination_id', '=', $examId)
                    ->where('examination_students.id', '=', $candidateId)
                    ->get();
                Log::info(
                    'Successfully Fetched individual candidate by exam id ,exam centre id and examstudent id',
                    [
                        'method' => __METHOD__,
                        'data' => [
                            'candidatedData' => $candidates,
                            'examId' => $examId,
                            'examCentreId' => $examCenterId,
                            'examStudentId' => $candidateId
                        ]
                    ]
                );
            } catch (\Exception $e) {
                Log::error(
                    'Failed to fetch individual candidate details by exam id,exam centre id and exam student id',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                return array();
            }
        }
        if (!$candidates) {
            return array();
        }
        return $candidates->toArray();
    }
}
