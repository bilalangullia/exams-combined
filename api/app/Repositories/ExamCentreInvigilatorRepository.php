<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationCentre;
use App\Models\SecurityUser;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class ExamCentreInvigilatorRepository extends Controller
{
    /**
     * Getting Exam Centre Invigilator List.
     * @param array $search
     * @param string $examCentreId
     * @return JsonResponse
     */
    public function examCentreInvigilatorList(array $search, string $examCentreId)
    {
        try {
            $invigilators = ExaminationCentre::select('id', 'name', 'examination_id')
                ->with(
                    [
                        'invigilators' => function ($query) {
                            $query->select(
                                'invigilator_id',
                                'openemis_no',
                                'first_name',
                                'middle_name',
                                'third_name',
                                'last_name'
                            )
                            ->where('status', config('constants.invigilatorUser.status'))
                            ->where('is_staff', config('constants.invigilatorUser.isStaff'));
                        }
                    ]
                );

            if (isset($search['keyword'])) {
                $invigilators->WhereHas(
                    'invigilators',
                    function ($query) use ($search) {
                        $query->where('first_name', 'LIKE', "%" . $search['keyword'] . "%")
                            ->orWhere('middle_name', 'LIKE', "%" . $search['keyword'] . "%")
                            ->orWhere('third_name', 'LIKE', "%" . $search['keyword'] . "%")
                            ->orWhere('last_name', 'LIKE', "%" . $search['keyword'] . "%")
                            ->orWhere('openemis_no', 'LIKE', "%" . $search['keyword'] . "%");
                    }
                );
            }

            $invigilators = $invigilators->where('id', $examCentreId)->first();
            
            return $invigilators;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam Centres Invigilators Not Found");
        }
    }

    /**
     * Getting Invigilator Details.
     * @param string $examCentreId
     * @param $invigilatorId
     * @return JsonResponse
     */
    public function getExamCentreInvigilatorDetails(string $examCentreId, $invigilatorId)
    {
        try {
            $invigilator = ExaminationCentre::select(
                'id',
                'name',
                'examination_id',
                'created_user_id',
                'created'
            )
            ->with(
                [
                    'invigilators' => function ($query) use ($invigilatorId) {
                        $query->select(
                            'invigilator_id',
                            'openemis_no',
                            'first_name',
                            'middle_name',
                            'third_name',
                            'last_name'
                        )
                        ->where('status', config('constants.invigilatorUser.status'))
                        ->where('is_staff', config('constants.invigilatorUser.isStaff'))
                        ->where('invigilator_id', $invigilatorId);
                    },
                    'createdByUser' => function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                            ->where('status', config('constants.createdByUser.status'));
                    },
                    'exams' => function ($query) {
                        $query->select('id', 'name', 'code', 'academic_period_id');
                    },
                    'exams.academicPeriod' => function ($query) {
                        $query->select('id', 'start_year');
                    }
                ]
            )
            ->where('id', $examCentreId)
            ->first();
            return $invigilator;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam centre invigilator details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Found");
        }
    }

    /**
     * Getting Exam Centre Invigilator Dropdown
     * @param array $search
     * @return JsonResponse
     */
    public function getInvigilatorDropdown(array $search)
    {
        try {
            $invigilators = SecurityUser::where('status', config('constants.invigilatorUser.status'))
                    ->where('is_staff', config('constants.invigilatorUser.isStaff'))
                    ->where('is_student', config('constants.invigilatorUser.isStudent'))
                    ->where('is_guardian', config('constants.invigilatorUser.isGuardian'))
                    ->select(
                        'id',
                        'openemis_no',
                        'first_name',
                        'middle_name',
                        'third_name',
                        'last_name',
                        'is_staff',
                        'is_student',
                        'is_guardian',
                        'status'
                    );

            if (isset($search['keyword'])) {
                $invigilators->where(function ($q) use ($search) {
                    $q->where('first_name', 'LIKE', "%" . $search['keyword'] . "%")
                        ->orWhere('middle_name', 'LIKE', "%" . $search['keyword'] . "%")
                        ->orWhere('third_name', 'LIKE', "%" . $search['keyword'] . "%")
                        ->orWhere('last_name', 'LIKE', "%" . $search['keyword'] . "%")
                        ->orWhere('openemis_no', 'LIKE', "%" . $search['keyword'] . "%");
                });
            }

            $invigilators = $invigilators->get();
            return $invigilators;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch invigilator dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Found");
        }
    }

    /**
     * Storing Exam Centre Invigilator.
     * @param array $data
     * @return JsonResponse|int
     */
    public function storeExamCentreInvigilator(array $data)
    {
        try {
            $is_exists = ExaminationCentre::where('id', $data['examination_centre_id'])
                ->WhereHas(
                    'invigilators',
                    function ($query) use ($data) {
                        $query->where('invigilator_id', $data['invigilator_id']);
                    }
                )
                ->exists();

            if ($is_exists) {
                return 2;
            } else {
                $insert['id']                   = Str::uuid();
                $insert['created_user_id']      = JWTAuth::user()->id;
                $insert['created']              = Carbon::now()->toDateTimeString();

                $examCentre = ExaminationCentre::find($data['examination_centre_id']);
                if ($examCentre) {
                    $store = $examCentre->invigilators()->attach($data['invigilator_id'], $insert);
                    return 1;
                } else {
                    return 0;
                }
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store invigilator in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Stored");
        }
    }

    /**
     * Deleting Invigilators
     * @param int $examCentreId
     * @param int $invigilatorId
     * @return JsonResponse|int
     */
    public function deleteInvigilator(int $invigilatorId, int $examCentreId)
    {
        try {
            if ($invigilatorId == 0) {
                return DB::table('examination_centre_invigilators')
                ->where('examination_centre_id', $examCentreId)
                ->delete();
            } else {
                return DB::table('examination_centre_invigilators')
                ->where('examination_centre_id', $examCentreId)
                ->where('invigilator_id', $invigilatorId)
                ->delete();
            }
            return 0;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre invigilator.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete invigilator in DB");
        }
    }
}
