<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\QualificationRequest;
use App\Models\Item;
use App\Models\QualificationSpecialisation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class ItemTypeRepository extends Controller
{
    /**
     * @param int $qulilevelId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItemTypeView(int $typeId)
    {
        try {
            $viewdetails = Item::where('id', $typeId)->get()->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "visible" => [
                            "key" => ($item['visible'] == '1') ? '1' : '0',
                            "value" => ($item['visible'] == '1') ? 'YES' : 'NO',
                        ],
                        "default" => [
                            "key" => ($item['visible'] == '1') ? '1' : '0',
                            "value" => ($item['visible'] == '1') ? 'YES' : 'NO',
                        ],
                        "international_code" => $item['international_code'],
                        "national_code" => $item['national_code'],
                        "modified_by" => $item['modifiedBy']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['createdBy']['full_name'],
                        "created_on" => $item['created']
                    ];
                }
            );
            return $viewdetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('View Not Found');
        }
    }

    /**
     * @param QualificationRequest $request
     * @param int $qulilevelId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItemTypeUpdate(QualificationRequest $request, int $typeId)
    {
        try {
            $update = Item::find($typeId);
            $update['name'] = $request->name;
            $update['visible'] = $request->visible;
            $update['default'] = $request->default;
            $update['international_code'] = $request->international_code;
            $update['national_code'] = $request->national_code;
            $update['modified'] = Carbon::now()->toDateTimeString();
            $update['modified_user_id'] = JWTAuth::user()->id;
            $record = $update->save();
            return $record;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Not Updated');
        }
    }
}
