<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\SecurityUserAddRequest;
use App\Http\Requests\SecurityUserUpdateRequest;
use App\Models\SecurityUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class UserRepository extends Controller
{
    /**
     * @param Request $request   'to_be_used_by_user_id', '!=' , 2
     * @return \Illuminate\Http\JsonResponse
     */
    public function userList(Request $request)
    {
        try {
            $data = $request->all();
            $total = 0;
            $listuser = SecurityUser::where('is_staff', config('constants.staffIds.staff'))
                ->where('super_admin', '!=', config('constants.user.super_admin'))->
                orderBy('id', 'desc');
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $listuser;
                $total = $userCount->count();
                $listuser->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $listuser;
                $total = $userCount->count();
            }
            if (isset($request['keyword'])) {
                $userSearch = $listuser->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('openemis_no', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('username', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('email', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('identity_number', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('status', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $userSearch = $listuser->get();
            return array("totalRecord" => $total, "record" => $userSearch);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User list not found");
        }
    }

    /**
     * @param string $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function userDetails(string $openEmisno)
    {
        try {
            $viewId = SecurityUser::select(
                'id',
                'gender_id',
                'nationality_id',
                'username',
                'identity_type_id',
                'first_name',
                'middle_name',
                'last_name',
                'status',
                'email',
                'openemis_no',
                'date_of_birth',
                'identity_number',
                'created_user_id',
                'modified_user_id',
                'modified',
                'created'
            )->where('openemis_no', $openEmisno)->with(
                'gender:id,name',
                'nationality:id,name',
                'identityType:id,name'
            )->get();
            return $viewId;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Details not found");
        }
    }

    /**
     * @param Request $request
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function userDetailsAdded(SecurityUserAddRequest $request)
    {
        try {
            $user = new SecurityUser();
            $user['openemis_no'] = $request->openemis_no;
            $user['username'] =    $request->username;
            $user['first_name'] = $request->first_name;
            $user['middle_name'] = $request->middle_name;
            $user['third_name'] = $request->third_name;
            $user['last_name'] = $request->last_name;
            $user['gender_id'] = $request->gender_id;
            $user['status'] =  $request->status;
            $user['password'] = bcrypt($request->password);
            $user['nationality_id'] = $request->nationality_id;
            $user['identity_type_id'] = $request->identity_type_id;
            $user['is_staff'] = config('constants.staffIds.staff');
            $user['date_of_birth'] = $this->changeDateFormat($request['date_of_birth']['text']);
            $user['created_user_id'] = JWTAuth::user()->id;
            $user['created'] = Carbon::now()->toDateTimeString();
             $user->save();
            return array("openemis_no" => $user['openemis_no']);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Details not found");
        }
    }

    /**
     * @param SecurityUserAddRequest $request
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function userDetailsUpdate(SecurityUserUpdateRequest $request, string $openEmisno)
    {
        try {
            $user_id = securityUser::where('openemis_no', $openEmisno)->first()->id;
            if ($user_id) {
                $data = securityUser::find($user_id);
                $data['first_name'] = $request->first_name;
                $data['middle_name'] = $request->middle_name;
                $data['third_name'] = $request->third_name;
                $data['last_name'] = $request->last_name;
                $data['gender_id'] = $request->gender_id;
                $data['date_of_birth'] = $request->date_of_birth;
                $data['nationality_id'] = $data->nationality_id;
                $data['identity_type_id'] = $data->identity_type_id;
                $data['identity_number'] = $data->identity_number;
                $data['username'] = $data->username;
                $data['email'] = $request->email;
                $user['is_staff'] = config('constants.staffIds.staff');
                $data['status'] =  $request->status;
                $data['modified'] = Carbon::now()->toDateTimeString();
                $data['modified_user_id'] = JWTAuth::user()->id;
                $userdata = $data->save();
                return $userdata;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to add user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User details not updated");
        }
    }

    /**
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function userAccountView(string $openEmisno)
    {
        try {
            $accountview = SecurityUser::select('id', 'username')->where('openemis_no', $openEmisno)->with(
                'securityUserLogin:id,security_user_id,login_date_time'
            )->get();
            return $accountview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to view user account view  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User account view not found");
        }
    }

    /**
     * @param Request $request
     * @param string $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function accountEdit(Request $request, string $openEmisno)
    {
        try {
            $validator = validator::make($request->all(), [
                'username' => 'required|string|unique:security_users|max:255',
                'password' => 'required',
                'confirm_password' => 'required|same:password',

            ]);
            if ($validator->fails()) {
                return $this->sendErrorResponse("Password not changed", $validator->errors());
            }

            $userId = SecurityUser::where('openemis_no', $openEmisno)->first();
            if ($userId) {
                $data = $userId;
                $data['openemis_no'] = $data->openemis_no;
                $data['username'] = $request->username;
                $data['first_name'] = $data->first_name;
                $data['last_name'] = $data->last_name;
                $data['gender_id'] = $data->gender_id;
                $data['date_of_birth'] = $data->date_of_birth;
                $data['password'] = bcrypt($request->password);
                $data['modified'] = Carbon::now()->toDateTimeString();
                $data['modified_user_id'] = JWTAuth::user()->id;
                $savedata = $data->save();
                return $savedata;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update password',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Account Password not changed");
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function generateOpenemisId()
    {
        try {
            $lastId = SecurityUser::orderBy('id', 'DESC')->first();
            if (is_numeric($lastId)) {
                return array("openemis_no" => $lastId->openemis_no + 1, "username" => $lastId->openemis_no + 1);
            } else {
                $string = $lastId->openemis_no;
                $newStr = ++$string;
                return array("openemis_no" => $newStr, "username" => $newStr);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to generated ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("openemis number not generated");
        }
    }
}
