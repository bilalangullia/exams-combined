<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\AcademicPeriodLevel;
use App\Models\AcademicPeriod;
use Carbon\Carbon;
use JWTAuth;

class AcademicPeriodLevelRepository extends Controller
{
	public function getAcademicPeriodLevelList(Request $request)
	{
		try{
			$listing = AcademicPeriodLevel::select('id', 'name', 'level')->orderBy('id', 'desc');

			if (isset($request['start']) && isset($request['end'])) {
                $listCount = $listing;
                $total           = $listCount->count();
                $listing->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $listCount = $listing;
                $total = $listCount->count();
            }

            if (isset($request['keyword'])) {
                $search = $listing->where(function ($query) use ($request){
                    $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                    $query->orwhere('level', 'LIKE', "%" . $request['keyword'] . "%");
                }
            );
        }
            $search = $listing->get();

            return array("record" => $search, "totalRecord" => $total);
		} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level List Not Found');
        }
	}


    public function getAcademicPeriodLevelDetail(int $academicLevel)
    {
        try{
            $detail = AcademicPeriodLevel::where('id', $academicLevel)->get();

            return $detail;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level Detail Not Found');
        }
    }


    public function addAcademicPeriodLevel(array $data)
    {
        try{
            $add = new AcademicPeriodLevel();
            $add['name'] = $data['name'];
            $add['level'] = config('constants.academicPeriodLevel.level');
            $add['editable'] = config('constants.academicPeriodLevel.editable');
            $add['created_user_id'] = JWTAuth::user()->id;
            $add['created'] = Carbon::now()->toDateTimeString();
            $store = $add->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Added');
        }
    }

    public function updateAcademicPeriodLevel(int $academicLevel, array $data)
    {
        try{
            $update = AcademicPeriodLevel::find($academicLevel);
            $update['name'] = $data['name'];
            $update['level'] = config('constants.academicPeriodLevel.level');
            $update['editable'] = config('constants.academicPeriodLevel.editable');
            $update['modified_user_id'] = JWTAuth::user()->id;
            $update['modified'] = Carbon::now()->toDateTimeString();
            $updateData = $update->save();

            return $updateData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Added');
        }
    }

    public function checkIfSafeToDelete(int $academicLevel)
    {
        try{
            $academicLvl = AcademicPeriodLevel::find($academicLevel);

            $acdmcPerd = AcademicPeriod::where('academic_period_level_id', $academicLevel)->exists();
            if ($acdmcPerd) {
                return false;
            }
            
            return true;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete academic level.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }

    /**
     * Deleteing Exam Centres
     * @param int $centreId
     * @return JsonResponse
     */
    public function deleteAcademicLevel(int $academicLevel)
    {
        try{
            $academivLevel = AcademicPeriodLevel::find($academicLevel);
            if ($academivLevel) {
                return $academivLevel->delete();
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete academic level.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }
}

