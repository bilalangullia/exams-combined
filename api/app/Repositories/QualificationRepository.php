<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\StaffQualificationRequest;
use App\Models\SecurityUser;
use App\Models\StaffQualification;
use App\Models\StaffQualificationsSpecialisation;
use App\Models\StaffQualificationsSubject;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class MarkerRepository
 * @package App\Repositories
 */

class QualificationRepository extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */

    public function qualificationListing(Request $request, string $openEmisno)
    {
        try {
            $list = StaffQualification::with(
                'qualificationTitle:id,name,qualification_level_id',
                'qualificationTitle.qualificationLevel:id,name',
                'educationFieldOfStudie:id,name'
            )->whereHas(
                'securityUserStaffId',
                function ($query) use ($request) {
                    $query->where('openemis_no', $request->openEmisno)
                        ->where('is_staff', config('constants.staffIds.staff'))
                        ->where('status', config('constants.staffIds.status'));
                }
            )->get();
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['QualificationList' => $list]]);
            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification list Not Found');
        }
    }

    /**
     * @param string $staffqualificationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function qualificationView(string $staffqualificationId)
    {
        try {
            $view = StaffQualification::with(
                'qualificationTitle:id,name,qualification_level_id',
                'qualificationTitle.qualificationLevel:id,name',
                'educationFieldOfStudie:id,name',
                'countrie:id,name',
                'staffQualificationsSubject:staff_qualification_id,id,education_subject_id',
                'staffQualificationsSubject.educationSubject:id,name',
                'staffQualificationsSpecialisation:staff_qualification_id,id,qualification_specialisation_id',
                'staffQualificationsSpecialisation.qualificationSpecialisation:id,name'
            )->where('id', $staffqualificationId)->get();
            return $view;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification  from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification view Not Found');
        }
    }

    /**
     * @param StaffQualificationRequest $request
     * @param string $openEmisno
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function addStaffQualification(StaffQualificationRequest $request, string $openEmisno)
    {
        DB::beginTransaction();
        try {
            $staffid = SecurityUser::where('openemis_no', $openEmisno)
                        ->where('is_staff', config('constants.staffIds.staff'))->where('status', config('constants.staffIds.status'))->first()->id;
           //dd($staffid);
            $data = new StaffQualification();
            $data['document_no'] = $request->document_no;
            $data['graduate_year'] = $request->graduate_year;
            $data['qualification_institution'] = $request->qualification_institution;
            $data['gpa'] = $request->gpa;
            $data['staff_id'] = $staffid;
            $data['qualification_title_id'] = $request->qualification_title_id;
            $data['qualification_country_id'] = $request->qualification_country_id;
            if ($request->hasFile('file_name')) {
                $name = 'qualification-' . time() . '.' . request()->file_name->getClientOriginalExtension();
                $path_file = request()->file_name->move(public_path('images'), $name);
                $data->file_name = $name;
            }
            $data['file_content'] = $request->file_content;
            $data['education_field_of_study_id'] = $request->education_field_of_study_id;
            $data['created_user_id'] = JWTAuth::user()->id;
            $data['created'] = Carbon::now()->toDateTimeString();
            $store = $data->save();
            if ($store && $request->subjects && count($request->subjects)) {
                $subjectdata = [];
                foreach ($request->subjects as $subject) {
                    if (!empty($subject)) {
                        $staffdata = [];
                        $staffdata['staff_qualification_id'] = $data->id;
                        $staffdata['education_subject_id'] = $subject['id'];
                        $staffdata['created_user_id'] = JWTAuth::user()->id;
                        $staffdata['created'] = Carbon::now()->toDateTimeString();
                        $subjectdata[] = $staffdata;
                    }
                }
                StaffQualificationsSubject::insert($subjectdata);
            }
            if ($store && $request->specialisations && count($request->specialisations)) {
                $specialisationsdata = [];
                foreach ($request->subjects as $specialisation) {
                    if (!empty($specialisation)) {
                        $item = [];
                        $item['staff_qualification_id'] = $data->id;
                        $item['qualification_specialisation_id'] = $specialisation['id'];
                        $item['created_user_id'] = JWTAuth::user()->id;
                        $item['created'] = Carbon::now()->toDateTimeString();
                        $specialisationsdata[] = $item;
                    }
                }
                StaffQualificationsSpecialisation::insert($specialisationsdata);
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to add Qualification  in to  DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification added Not successfully');
        }
    }

    /**
     * @param StaffQualificationRequest $request
     * @param string $staffId
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function updateStaffQualification(StaffQualificationRequest $request, string $staffId)
    {
        DB::beginTransaction();
        try {
            $stafqualification = StaffQualification::find($staffId);
            $stafqualification['document_no'] = $request->document_no;
            $stafqualification['graduate_year'] = $request->graduate_year;
            $stafqualification['qualification_institution'] = $request->qualification_institution;
            $stafqualification['gpa'] = $request->gpa;
            $stafqualification['qualification_title_id'] = $request->qualification_title_id;
            $stafqualification['qualification_country_id'] = $request->qualification_country_id;
            if ($request->hasFile('file_name')) {
                $name = 'qualification-' . time() . '.' . request()->file_name->getClientOriginalExtension();
                $path_file = request()->file_name->move(public_path('images'), $name);
                $stafqualification->file_name = $name;
            }
            $stafqualification['file_content'] = $request->file_content;
            $stafqualification['education_field_of_study_id'] = $request->education_field_of_study_id;
            $stafqualification['modified'] = Carbon::now()->toDateTimeString();
            $stafqualification['modified_user_id'] = JWTAuth::user()->id;
            $savestaff = $stafqualification->save();
            $staffIds = $stafqualification->id;
            $qualification = StaffQualificationsSubject::where('staff_qualification_id', $staffIds)->first();
            if (!$qualification) {
                return array('message' => '');
            } else {
                if ($savestaff && $request->subjects && count($request->subjects)) {
                    $staffsubject = StaffQualificationsSubject::where('staff_qualification_id', $staffIds)->delete();
                    if (count($request->subjects)) {
                        $subjectdata = [];
                        foreach ($request->subjects as $subject) {
                            if (!empty($subject)) {
                                $staffdata = [];
                                $staffdata['staff_qualification_id'] = $staffIds;
                                $staffdata['education_subject_id'] = $subject['id'];
                                $staffdata['created_user_id'] = JWTAuth::user()->id;
                                $staffdata['created'] = Carbon::now()->toDateTimeString();
                                $subjectdata[] = $staffdata;
                            }
                        }
                        StaffQualificationsSubject::insert($subjectdata);
                    }
                }
            }
            $specilasation = StaffQualificationsSpecialisation::where('staff_qualification_id', $staffIds)->first();
            if (!$specilasation) {
                return array('message' => '');
            } else {
                if ($savestaff && $request->specialisations && count($request->specialisations)) {
                    $staffSpecialisation = StaffQualificationsSpecialisation::where('staff_qualification_id', $staffIds)->delete();
                    if (count($request->specialisations)) {
                        $specialisationsdata = [];
                        foreach ($request->specialisations as $specialisation) {
                            if (!empty($specialisation)) {
                                $item = [];
                                $item['staff_qualification_id'] = $staffIds;
                                $item['qualification_specialisation_id'] = $specialisation['id'];
                                $item['created_user_id'] = config('constants.createdByUser.id');
                                $item['created'] = Carbon::now()->toDateTimeString();
                                $specialisationsdata[] = $item;
                            }
                        }
                    }
                    StaffQualificationsSpecialisation::insert($specialisationsdata);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to update Qualification  from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification updated Not successfully');
        }
    }


    /**
     * @param int $staffqualificationId
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function deleteStaffQualification(int $staffqualificationId)
    {
        try {
            $staffqualification = StaffQualification::find($staffqualificationId);
            if ($staffqualification) {
                $staffsub = $staffqualification->staffQualificationsSubject()->delete();
                $staffspeci = $staffqualification->staffQualificationsSpecialisation()->delete();
                return $staffqualification->delete();
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete Qualification',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification not deleted.");
        }
    }
}
