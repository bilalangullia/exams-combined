<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\ExaminationFee;
use App\Models\ExaminationStudentsFee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FeeRepository extends Controller
{
    /**
     * Getting Examination Fee List/filter
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationsFeeList(Request $request)
    {
        try {
            $feesList = ExaminationFee::select('id', 'name', 'amount')->where(
                'examinations_id',
                $request->examId
            )->whereHas(
                'examination',
                function ($query) use ($request) {
                    $query->where('academic_period_id', $request->academic_period_id);
                }
            );

            if (isset($request['keyword'])) {
                $feesFilter = $feesList->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("name", "LIKE", "%".$request['keyword']."%");
                                $query->orwhere("amount", "LIKE", "%".$request['keyword']."%");
                            }
                        );
                    }
                );
            }
            $feesFilter = $feesList->get();
            Log::info(
                'Fetched fee list from DB',
                ['method' => __METHOD__, 'data' => ['feeList' => $feesFilter]]
            );

            return $feesFilter;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee List Not found");
        }
    }

    /**
     * Getting Examination Fee details
     * @param Request $request
     * @param string $examFeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationFeeDetails(Request $request, string $examFeeId)
    {
        try {
            $feeDetails = ExaminationFee::where('id', $request->examFeeId)->with(
                'examination',
                'examination.academicPeriod'
            )->get();

            Log::info(
                'Fetched fee details from DB',
                ['method' => __METHOD__, 'data' => ['feeDetails' => $feeDetails]]
            );

            return $feeDetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee details Not found");
        }
    }

    /**
     * Adding Examination fee
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function addExaminationsFee(Request $request)
    {
        try {
            $data = new ExaminationFee();
            $data->name = $request->name;
            $data->amount = $request->amount;
            $data->examinations_id = $request->examinations_id;
            $data->created_user_id = config('constants.createdByUser.id');
            $data->created = Carbon::now()->toDateTimeString();
            $store = $data->save();
            $responseData = [
                "name" => $data['name'],
                "amount" => $data['amount'],
            ];

            return $responseData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Not Added");
        }
    }

    /**
     * updating examination fee
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateExaminationFee(Request $request, string $examFeeId)
    {
        try {
            $data = ExaminationFee::find($examFeeId);
            $data->name = $request->name;
            $data->amount = $request->amount;
            $data->examinations_id = $request->examinations_id;
            $data->modified_user_id = config('modifiedUser.user_id');
            $data->modified = Carbon::now()->toDateTimeString();
            $updateData = $data->save();

            return $updateData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Not Updated");
        }
    }

    /**
     * Checking examination fee existence in examination student fee table
     * @param string $examFeeId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function checkingExaminationFee(string $examFeeId)
    {
        try {
            $data = ExaminationStudentsFee::where('examination_fees_id', $examFeeId)->first();

            return array('data' => $data, 'examFeeId' => $examFeeId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Not Removed");
        }
    }
}