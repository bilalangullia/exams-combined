<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamOptionUpdationRequest;
use App\Models\ExaminationComponent;
use App\Models\ExaminationStudentsOptionsGrade;
use App\Models\ExaminationStudentsForecastGrade;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationOption;
use App\Models\Examination;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\AddExaminationOptionRequest;
use JWTAuth;
use DB;

class OptionRepository extends Controller
{
    /**
     * Getting examination option list on exam basis
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationOptionList(Request $request, string $examId)
    {
        try {
            $optionList = ExaminationOption::whereHas(
                'examinationSubject.examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examId);
                }
            );

            if (isset($request['start']) && isset($request['end'])) {
                $listCount = $optionList;
                $total = $listCount->count();
                $optionList->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $listCount = $optionList;
                $total = $listCount->count();
            }

            if (isset($request['keyword'])) {
                $optionSearch = $optionList->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("code", "LIKE", "%" . $request['keyword'] . "%");
                                $query->orwhere("name", "LIKE", "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationSubject',
                            function ($query) use ($request) {
                                $query->where('code', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $optionSearch = $optionList->get();
            $data['record'] = $optionSearch;
            $data['totalRecord'] = $total;

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Options List Not Found");
        }
    }

    /**
     * Getting option tab >> view
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationOptionDetails(Request $request, string $optionId)
    {
        try {
            $optionDetail = ExaminationOption::where('id', $request->optionId)->with(
                'examinationSubject',
                'examinationGradingType',
                'securityUser',
                'attendanceType'
            )->get();
            Log::info(
                'Fetched option List from DB',
                ['method' => __METHOD__, 'data' => ['optionDetails' => $optionDetail]]
            );

            return $optionDetail;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Options Detail Not Found");
        }
    }

    /**
     * Update examination >> option tab >> option
     * @param Request $request
     * @param string $optionId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function updateExaminationOptionRecord(ExamOptionUpdationRequest $request, string $optionId)
    {
        try {
            $updateOption = ExaminationOption::find($optionId);
            $updateOption['code'] = $request->code;
            $updateOption['name'] = $request->name;
            $updateOption['examination_grading_type_id'] = $request->examination_grading_type_id;
            $updateOption['attendance_type_id'] = $request->attendance_type_id;
            $updateOption['carry_forward'] = $request->carry_forward;
            $updateOption['max_opt_mark'] = $request->max_opt_mark;
            $updateOption['modified_user_id'] = JWTAuth::user()->id;
            $updateOption['modified'] = Carbon::now()->toDateTimeString();
            $store = $updateOption->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Options Detail Not Updated");
        }
    }

    /**
     * checking option existance
     * @param string $optionId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function checkIfSafeToDelete(int $optionId)
    {
        try {
            $examOption = ExaminationOption::find($optionId);
            
            $examStudentOptionGrade = ExaminationStudentsOptionsGrade::where('examination_options_id', $optionId)->first();
            if ($examStudentOptionGrade) {
                return false;
            }

            $examStudentForecastGrade = ExaminationStudentsForecastGrade::where('examination_options_id', $optionId)->first();
            if ($examStudentForecastGrade) {
                return false;
            }

            $examStudentOptions = ExaminationStudentsOption::where('examination_options_id', $optionId)->first();
            if ($examStudentOptions) {
                return false;
            }

            $examComp = ExaminationComponent::where('examination_options_id', $optionId)->first();
            if ($examComp) {
                return false;
            }

            return true;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam option.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed To Delete Examination Option.");
        }
    }

    public function deleteExamOption(int $optionId)
    {
        try {
            $examOption = ExaminationOption::find($optionId);
            if ($examOption) {
                $dltStdOtp = $examOption->examinationStudentsOption()->delete();
                $dltOtpGrdRvwCrta = $examOption->examinationGradeReviewCriteria()->delete();
                return $examOption->delete();
            }
            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam Option.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Option  not deleted.");
        }
    }

    /**
     * Add->examination->option
     * @param AddExaminationOptionRequest $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function addExaminationOption(AddExaminationOptionRequest $request)
    {
        try {
            $record = new ExaminationOption();
            $record->code = $request->code;
            $record->name = $request->name;
            $record->attendance_type_id  = $request->attendance_type_id;
            $record->examination_subject_id = $request->examination_subject_id;
            $record->examination_grading_type_id = $request->examination_grading_type_id;
            $record->carry_forward = $request->carry_forward;
            $record->max_opt_mark = $request->max_opt_mark;
            $record->created_user_id =  JWTAuth::user()->id;
            $record->created = Carbon::now()->toDateTimeString();
            $store = $record->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to data details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Option Not Added");
        }
    }

    public function optionImport(array $excel, $requestParameter)
    {
        DB::beginTransaction();
        try {
            $i = -1;
            $validation = [];
            $stored_data = [];
            $add_data = [];
            $importResponse = [];
            $academicPeriodId = $requestParameter['academic_period_id'];
            $exam = $requestParameter['examination_id'];
            $examination = Examination::select(
                'id',
                'code',
                'name',
                'academic_period_id'
            )->where('id', $exam)
                ->with(
                    [
                        'academicPeriod' => function ($query) use ($academicPeriodId) {
                            $query->where('id', $academicPeriodId)->select('id', 'name');
                        }
                    ]
                )->first();

            foreach ($excel[0] as  $row) 
            { 
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }
                if (!$row[0]) {//Subject Id
                    $label = $excel[0][1][0];
                    $errors[$label] = 'Subject required';
                }

                if (!$row[1]) {//Response
                    $label = $excel[0][1][1];
                    $errors[$label] = 'Option code required';
                }
                if (!$row[2]) {//Response
                    $label = $excel[0][1][2];
                    $errors[$label] = 'Option name required';
                }
                if (!$row[3]) {//Response
                    $label = $excel[0][1][3];
                    $errors[$label] = 'Grading type required';
                }
                if (!$row[4]) {//Response
                    $label = $excel[0][1][4];
                    $errors[$label] = 'Max Opt Mark required';
                }
                if (!$row[5]) {//Response
                    $label = $excel[0][1][5];
                    $errors[$label] = 'Attendance Type required';
                }
                if (is_null($row[6])) {//Response
                    $label = $excel[0][1][6];
                    $errors[$label] = 'Carry Forward required';
                }

                if (count($errors) > 0) {
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            "Academic Period" => $examination["academicPeriod"]["name"],
                            "Examination" => $examination['code'],
                            "Subject" => "",
                            "Option Code" => "",
                            "Option Name" => "",
                            "Grading Type" => "",
                            "Max Option Mark" => "",
                            "Attendance Type" => "",
                            "Carry Forward" => ""
                        ],
                        'errors' => $errors
                    ];
                } else {
                    if ($row[0]) {
                        $stored = ExaminationOption::updateOrCreate(
                                [
                                    'examination_subject_id' => $row[0],
                                    'code' => $row[1],
                                    'name' => $row[2],
                                    'examination_grading_type_id' => $row[3],
                                    'carry_forward' => ($row[6]) ? $row[6] : 0,
                                    'attendance_type_id' => $row[5],
                                    'max_opt_mark' => $row[4],
                                    'created_user_id' => JWTAuth::user()->id,
                                    'created' => Carbon::now()->toDateTimeString()
                                ]
                            );
                            if ($stored->wasRecentlyCreated) {
                                $add_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        "Academic Period" => $examination["academicPeriod"]["name"],
                                        "Examination" => $examination['code'],
                                        "Subject" => $stored['examinationSubject']['full_name'],
                                        "Option Code" => $stored['code'],
                                        "Option Name" => $stored['name'],
                                        "Grading Type" => $stored['examination_grading_type_id'],
                                        "Max Option Mark" => $stored['max_opt_mark'],
                                        "Attendance Type" => $stored['attendance_type_id'],
                                        "Carry Forward" => $stored['carry_forward'],

                                    ],
                                    'errors' => $errors
                                ];
                            } else {
                                $stored_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        "Academic Period" => $examination["academicPeriod"]["name"],
                                        "Examination" => $examination['code'],
                                        "Subject" => $subject['examinationSubject']['full_name'],
                                        "Option Code" => $subject['code'],
                                        "Option Name" => $subject['name'],
                                        "Grading Type" => $subject['examination_grading_type_id'],
                                        "Max Option Mark" => $subject['max_opt_mark'],
                                        "Attendance Type" => $subject['attendance_type_id'],
                                        "Carry Forward" => $subject['carry_forward'],
                                    ],
                                    'errors' => $errors
                                ];
                            }
                        } else {
                            $label = $excel[0][1][0];
                            $errors[$label] = 'Subject ID does not exists.';
                            $validation[] = [
                                'row_number' => $i,
                                'data' => [
                                    "Academic Period" => $examination["academicPeriod"]["name"],
                                    "Examination" => $examination['code'],
                                    "Subject" => $subject['examinationSubject']['full_name'],
                                    "Option Code" => $student['code'],
                                    "Option Name" => $student['name']
                                ],
                                'errors' => $errors
                            ];
                        } 
                    }  
            }
                $importResponse = [
                            'total_count' => count($excel[0]) - 2,
                            'records_added' => [
                                'count' => count($add_data),
                                'rows' => $add_data,
                            ],
                            'records_updated' => [
                                'count' => count($stored_data),
                                'rows' => $stored_data,
                            ],
                            'records_failed' => [
                                'count' => count($validation),
                                'rows' => $validation,
                            ],
                        ];
                
                    DB::commit();
                    return $importResponse; 
        } catch (\Exception $e) {
            echo $e;
                DB::rollback();
                Log::error(
                    'Failed to import option in DB',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                
                return array();
            }
        }
}

