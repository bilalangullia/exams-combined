<?php

namespace App\Repositories\ExaminationCentres;

use App\Models\ExaminationCentre;
use Illuminate\Support\Facades\Log;

class ExaminationCentreRepository
{

    public function getExamCenterNameCodeByExamCenterId($examCentreId)
    {
        try {
            $examCenterNameCode = ExaminationCentre::select('id','name', 'code')->where('id', $examCentreId)->first();
            Log::info('Successfully Fetched examcentre name and code by examcentre id', ['method' => __METHOD__, 'data' => ['examCentreId' => $examCentreId, 'examCentreData' => $examCenterNameCode]]);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam centre name and code by examcentre id',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return array();
        }
        if (!$examCenterNameCode) {
            return array();
        }
        return $examCenterNameCode->toArray();
    }
}