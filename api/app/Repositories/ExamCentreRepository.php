<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationCentre;
use App\Models\ExaminationCentreRoom;
use App\Models\ExaminationCentreInvigilators;
use App\Models\ExaminationStudent;
use App\Models\ExaminationMarker;
use App\Models\ExaminationCentresExaminationsMarksScaling;
use App\Models\SecurityGroupUser;
use App\Models\SecurityGroup;
use App\Models\SecurityGroupArea;
use App\Models\Examination;
use App\Models\ExaminationCentreOwnership;
use App\Models\Area;
use App\Jobs\DeleteSecurityUserGroup;
use Tymon\JWTAuth\Facades\JWTAuth;

class ExamCentreRepository extends Controller
{
    /**
     * Getting Exam Centre List
     * @param array $search
     * @return JsonResponse
     */
    public function getExamCentreList(array $search)
    {
        try {
            $examCentres = ExaminationCentre::select(['id', 'code', 'name', 'area_id', 'examination_id'])
                ->where('examination_id', $search['examination_id'])
                ->with(
                    [
                        'area' => function ($query) {
                            $query->select('code', 'name', 'id')
                                ->where('visible', config('constants.visibleValue.visibleCode'));
                        }
                    ]
                );

            if (isset($search['keyword'])) {
                $examCentres->where(function ($q) use ($search) {
                    $q->where('code', 'LIKE', "%" . $search['keyword'] . "%")
                        ->orWhere('name', 'LIKE', "%" . $search['keyword'] . "%")
                        ->orWhereHas(
                            'area',
                            function ($query) use ($search) {
                                $query->where('name', 'LIKE', "%" . $search['keyword'] . "%")
                                    ->orWhere('code', 'LIKE', "%" . $search['keyword'] . "%");
                            }
                        );
                });
            }

            $examCentres = $examCentres->orderBy('id', 'DESC')->get();

            return $examCentres;

        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam Centres Not Found");
        }
    }

    /**
     * Getting exam centre overview details
     * @param int $examCentreId
     * @return JsonResponse
     */
    public function examCentreOverview(int $examCentreId)
    {
        try {
            $examCentreOverview = ExaminationCentre::select(
                'id',
                'name',
                'code',
                'address',
                'postal_code',
                'contact_person',
                'area_id',
                'telephone',
                'emis_code',
                'created_user_id',
                'modified_user_id',
                'examination_centres_ownerships_id',
                'modified',
                'created',
                'examination_id'
            )
                ->with(
                    [
                        'area' => function ($query) {
                            $query->select('id', 'code', 'name')
                                ->where('visible', config('constants.visibleValue.visibleCode'));
                        },
                        'createdByUser' => function ($query) {
                            $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                ->where('status', config('constants.createdByUser.status'));
                        },
                        'modifiedUser' => function ($query) {
                            $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                ->where('status', config('constants.modifiedByUser.status'));
                        },
                        'ownership' => function ($query) {
                            $query->select('id', 'name')->where('visible', config('constants.visibleValue.visibleCode'));
                        },
                        'exams' => function ($query) {
                            $query->select('id', 'name', 'code', 'academic_period_id');
                        },
                        'exams.academicPeriod' => function ($query) {
                            $query->select('id', 'start_year')
                                ->where('visible', config('constants.visibleValue.visibleCode'));
                        },
                    ]
                )
                ->where('id', $examCentreId)
                ->first();

            return $examCentreOverview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam center overview from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centres Overview Not Found");
        }
    }

    /**
     * Updating Exam centre data
     * @param array $data
     * @return JsonResponse|int
     */
    public function examCentreUpdate(array $data)
    {
        try {
            $is_exists = ExaminationCentre::where('id', $data['id'])->first();
            if ($is_exists) {
                //$centreData['academic_period_id'] = $data['academic_period'];
                $centreData['name'] = $data['centre_name'];
                $centreData['code'] = $data['centre_code'];
                $centreData['emis_code'] = $data['emis_code']??Null;
                //$centreData['area_id'] = $data['area_name'];
                $centreData['address'] = $data['address']??Null;
                $centreData['postal_code'] = $data['postal_code']??Null;
                $centreData['contact_person'] = $data['contact_person']??Null;
                $centreData['telephone'] = $data['telephone'];
                $centreData['examination_centres_ownerships_id'] = $data['ownership'];
                $centreData['modified_user_id'] = JWTAuth::user()->id;
                $centreData['modified'] = Carbon::now()->toDateTimeString();

                ExaminationCentre::where('id', $data['id'])->update($centreData);
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam centre data not updated.");
        }
    }

    /**
     * Checking if safe to delete
     * @param int $centreId
     * @return JsonResponse
     */
    public function checkIfSafeToDelete(int $centreId)
    {
        try {
            $centre = ExaminationCentre::find($centreId);
            
            $groupIds = [];
            if ($centre) {
                $groupIds = $centre->groups()->pluck('security_group_id')->toArray();
                $groupIds[] = $centre['security_group_id'];
                $groupUsers = SecurityGroupUser::whereIn('security_group_id', $groupIds)->exists();
                if ($groupUsers) {
                    return false;
                }
            }
            
            $examStudent = ExaminationStudent::where('examination_centre_id', $centreId)->exists();
            if ($examStudent) {
                return false;
            }

            $examMarker = ExaminationMarker::where('examination_centre_id', $centreId)->exists();
            if ($examMarker) {
                return false;
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }

    /**
     * Deleteing Exam Centres
     * @param int $centreId
     * @return JsonResponse
     */
    public function deleteExamCentres(int $centreId)
    {
        try {
            $groupIds = [];
            $examCentre = ExaminationCentre::find($centreId);
            if ($examCentre) {
                $dltSpclNeed = $examCentre->specialNeeds()->detach();
                $dltMarkScalling = $examCentre->markScalling()->delete();

                $groupIds = $examCentre->groups()->pluck('security_group_id')->toArray();
                $groupIds[] = $examCentre['security_group_id'];

                foreach ($groupIds as $key => $groupId) {
                    $GroupDlt = new DeleteSecurityUserGroup($groupId, false);
                    $this->dispatchNow($GroupDlt);
                }
                return $examCentre->delete();
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam centre not deleted.");
        }
    }

    /**
     * Storing Exam Centre
     * @param array $data
     * @return JsonResponse|int
     */
    public function examCentreStore(array $data)
    {
        DB::beginTransaction();
        try {
            $created_user = JWTAuth::user()->id;
            $created = Carbon::now()->toDateTimeString();
            
            $groupArr['name'] = $data['code'] . " - " . $data['name'];
            $groupArr['created_user_id'] = $created_user;
            $groupArr['created'] = $created;
            $groupId = SecurityGroup::insertGetId($groupArr);

            $areaArr['security_group_id'] = $groupId;
            $areaArr['area_id'] = $data['area_id'];
            $areaArr['created_user_id'] = $created_user;
            $areaArr['created'] = $created;
            SecurityGroupArea::insert($areaArr);

            $data['security_group_id'] = $groupId;
            $data['created_user_id'] = $created_user;
            $data['created'] = $created;
            $insert = ExaminationCentre::insert($data);

            DB::commit();
            if ($insert) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Exam centre data not stored.");
        }
    }

    /**
     * Copying Exam Centre
     * @param array $data
     * @return JsonResponse|int
     */
    public function examCentreCopy(array $data)
    {
        DB::beginTransaction();
        try {
            $linkAllCentre = $data['link_all_centres'];
            $fromExamId = $data['from_examination_id'];
            $toExamId = $data['to_examination_id'];
            $examcentres = [];
            if ($linkAllCentre == 1) {
                $examcentres = ExaminationCentre::where('examination_id', $fromExamId)
                    ->with(
                        [
                            'rooms' => function ($query) {
                                $query->select('name', 'size', 'number_of_seats', 'examination_centre_id');
                            }
                        ]
                    )
                    ->get();
            } else {
                $centres = $data['centres'];
                $examcentres = ExaminationCentre::whereIn('id', $centres)
                    ->with(
                        [
                            'rooms' => function ($query) {
                                $query->select('name', 'size', 'number_of_seats', 'examination_centre_id');
                            }
                        ]
                    )
                    ->get();
            }
            
            $created_user = JWTAuth::user()->id;
            $created = Carbon::now()->toDateTimeString();
            $centreArr = [];
            $roomArr = [];
            $areaArr = [];
            foreach ($examcentres as $centre) {
                $groupArr['name'] = $centre['code'] . " - " . $centre['name'];
                $groupArr['created_user_id'] = $created_user;
                $groupArr['created'] = $created;
                $groupId = SecurityGroup::insertGetId($groupArr);

                $areaArr[] = [
                    'security_group_id' => $groupId,
                    'area_id' => $centre['area_id'],
                    'created_user_id' => $created_user,
                    'created' => $created,
                ];

                $centreArr = [
                    'name' => $centre['name'],
                    'code' => $centre['code'],
                    'address' => $centre['address'],
                    'postal_code' => $centre['postal_code'],
                    'contact_person' => $centre['contact_person'],
                    'telephone' => $centre['telephone'],
                    'fax' => $centre['fax'],
                    'email' => $centre['email'],
                    'website' => $centre['website'],
                    'emis_code' => $centre['emis_code'],
                    'examination_id' => $toExamId,
                    'examination_centres_ownerships_id' => $centre['examination_centres_ownerships_id'],
                    'institution_id' => $centre['institution_id'],
                    'area_id' => $centre['area_id'],
                    'security_group_id' => $groupId,
                    'created_user_id' => $created_user,
                    'created' => $created,
                ];
                $centreId = ExaminationCentre::insertGetId($centreArr);
                
                foreach ($centre['rooms'] as $room) {
                    $roomArr[] = [
                        'name' => $room['name'],
                        'size' => $room['size'],
                        'number_of_seats' => $room['number_of_seats'],
                        'examination_centre_id' => $centreId,
                        'created_user_id' => JWTAuth::user()->id,
                        'created' => Carbon::now()->toDateTimeString(),
                    ];
                }
            }
            
            ExaminationCentreRoom::insert($roomArr);
            SecurityGroupArea::insert($areaArr);
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            Log::error(
                'Failed to copy exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to copy exam centre data in DB");
        }
    }

    /**
     * Importing Examination Center.
     * @param array $excel
     * @param array $requestParameter
     * @return JsonResponse
    */
    public function examCentreImport(array $excel, array $requestParameter)
    {
        DB::beginTransaction();
        try {
            $i = -1;
            $validation = [];
            $stored_data = [];
            $add_data = [];
            $importResponse = [];
            $insertArr = [];
            $groupArr = [];
            $areaArr = [];
            $academicPeriodId = $requestParameter['academic_period_id'];
            $examId = $requestParameter['examination_id'];

            $exam = Examination::select(
                'id',
                'code',
                'name',
                'academic_period_id'
            )
            ->with(
                [
                    'academicPeriod' => function ($query) {
                        $query->select('id', 'start_year')
                        ->where('visible', config('constants.visibleValue.visibleCode'));
                    }
                ]
            )
            ->where('id', $examId)
            ->first();

            foreach ($excel[0] as $key => $row) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }
               
                if (!$row[0]) {//Centre Code
                    $label = $excel[0][1][0];
                    $errors[$label] = 'Centre Code id required';
                }

                if (!$row[1]) {//Centre Name
                    $label = $excel[0][1][1];
                    $errors[$label] = 'Centre Name required';
                }

                if (!$row[3]) {//Area Name
                    $label = $excel[0][1][3];
                    $errors[$label] = 'Area Name required';
                }

                if (!$row[8]) {//Ownership Name
                    $label = $excel[0][1][8];
                    $errors[$label] = 'Ownership Name required';
                }
                if (isset($row[3])) {
                    $area = $this->getArea($row[3]);
                }
                
                if (isset($row[8])) {
                    $ownership = $this->getOwnership($row[8]);
                }
                
                
                if (count($errors) > 0) {
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            $excel[0][1][0] => $row[0],
                            $excel[0][1][1] => $row[1],
                            $excel[0][1][2] => $row[2],
                            $excel[0][1][3] => $row[3],
                            $excel[0][1][4] => $row[4],
                            $excel[0][1][5] => $row[5],
                            $excel[0][1][6] => $row[6],
                            $excel[0][1][7] => $row[7],
                            $excel[0][1][8] => $row[8],
                        ],
                        'errors' => $errors
                    ];
                } else {
                    if (isset($ownership)) {
                        if (isset($area)) {

                            $groupArr['name'] = $row[0] . " - " . $row[1];
                            $groupArr['created_user_id'] = JWTAuth::user()->id;
                            $groupArr['created'] = Carbon::now()->toDateTimeString();
                            $groupId = SecurityGroup::insertGetId($groupArr);
                            

                            $areaArr[] = [
                                'security_group_id' => $groupId,
                                'area_id' => $area['id'],
                                'created_user_id' => JWTAuth::user()->id,
                                'created' => Carbon::now()->toDateTimeString(),
                            ];

                            $insertArr[] = [
                                'code' => $row[0],
                                'name' => $row[1],
                                'emis_code' => $row[2],
                                'area_id' => $area['id'],
                                'address' => $row[4],
                                'postal_code' => $row[5],
                                'contact_person' => $row[6],
                                'telephone' => $row[7],
                                'examination_centres_ownerships_id' => $ownership['id'],
                                'examination_id' => $exam['id'],
                                'security_group_id' => $groupId,
                                'created_user_id' => JWTAuth::user()->id,
                                'created' => Carbon::now()->toDateTimeString()
                            ];
                            
                            $add_data[] = [
                                'row_number' => $i,
                                'data' => [
                                    $excel[0][1][0] => $row[0],
                                    $excel[0][1][1] => $row[1],
                                    $excel[0][1][2] => $row[2],
                                    $excel[0][1][3] => $row[3],
                                    $excel[0][1][4] => $row[4],
                                    $excel[0][1][5] => $row[5],
                                    $excel[0][1][6] => $row[6],
                                    $excel[0][1][7] => $row[7],
                                    $excel[0][1][8] => $row[8],
                                ],
                                'errors' => $errors
                            ];
                        } else {
                            $label = $excel[0][1][0];
                            $errors[$label] = 'Area does not exists.';
                            $validation[] = [
                                'row_number' => $i,
                                'data' => [
                                    $excel[0][1][0] => $row[0],
                                    $excel[0][1][1] => $row[1],
                                    $excel[0][1][2] => $row[2],
                                    $excel[0][1][3] => $row[3],
                                    $excel[0][1][4] => $row[4],
                                    $excel[0][1][5] => $row[5],
                                    $excel[0][1][6] => $row[6],
                                    $excel[0][1][7] => $row[7],
                                    $excel[0][1][8] => $row[8],
                                ],
                                'errors' => $errors
                            ];
                        } 
                    } else {
                        $label = $excel[0][1][8];
                        $errors[$label] = 'Ownership does not exists';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                $excel[0][1][0] => $row[0],
                                $excel[0][1][1] => $row[1],
                                $excel[0][1][2] => $row[2],
                                $excel[0][1][3] => $row[3],
                                $excel[0][1][4] => $row[4],
                                $excel[0][1][5] => $row[5],
                                $excel[0][1][6] => $row[6],
                                $excel[0][1][7] => $row[7],
                                $excel[0][1][8] => $row[8],
                            ],
                            'errors' => $errors
                        ];
                    }
                }
            }

            $storeCentre = ExaminationCentre::insert($insertArr);
            $storeArea = SecurityGroupArea::insert($areaArr);
            $importResponse = [
                'total_count' => count($excel[0]) - 2,
                'records_added' => [
                    'count' => count($add_data),
                    'rows' => $add_data,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];
            
            DB::commit();
            return $importResponse;
        } catch (\Exception $e) {
            Log::error(
                'Failed to import exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to import exam centre data in DB");
        }
    }

    /**
     * Getting Area Details.
     * @param string $areaName
     * @return JsonResponse
    */
    public function getArea(string $areaName = "")
    {
        try {
            $area = Area::where('name', $areaName)->select('id')->first();
            return $area;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area details");
        }
    }

    /**
     * Getting Ownership Details.
     * @param string $ownership
     * @return JsonResponse
    */
    public function getOwnership(string $ownership = "")
    {
        try {
            $ownership = ExaminationCentreOwnership::where('name', $ownership)->select('id')->first();
            return $ownership;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get ownership details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get ownership details");
        }
    }
}
