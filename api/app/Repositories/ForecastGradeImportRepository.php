<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationStudentsForecastGrade;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\ExaminationCentre;
use App\Models\ExaminationOption;
use App\Models\ExaminationGradingOption;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;

class ForecastGradeImportRepository extends Controller
{
    /**
     * Importing Foreast Grade.
     * @param array $excel
     * @param array $requestParameter
     * @return JsonResponse
    */
    public function forecastGradeImport(array $excel, $requestParameter)
    {
        DB::beginTransaction();
        try {
            $i = -1;
            $validation = [];
            $stored_data = [];
            $add_data = [];
            $importResponse = [];
            $optionId = $requestParameter['option_id'];
            $academicPeriodId = $requestParameter['academic_period_id'];
            $examCentreId = $requestParameter['examination_centre_id'];

            $examCentre = ExaminationCentre::select(
                'id',
                'code',
                'examination_id'
            )
            ->with(
                [
                    'exams' => function ($query) {
                        $query->select('id', 'code', 'academic_period_id');
                    },
                    'exams.academicPeriod' => function ($query) {
                        $query->select('id', 'start_year')
                        ->where('visible', config('constants.visibleValue.visibleCode'));
                    }
                ]
            )
            ->where('id', $examCentreId)
            ->first();
            
            $subject = $this->getOptionSubject($optionId);

            foreach ($excel[0] as $key => $row) {

                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }
                if (!$row[0]) {//Candidate Id
                    $label = $excel[0][1][0];
                    $errors[$label] = 'Candidate id required';
                }

                if (!$row[1]) {//Response
                    $label = $excel[0][1][1];
                    $errors[$label] = 'Forecast grade required';
                }
                $grade = $this->getGradeValue($row[1]);

                if (count($errors) > 0) {
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            $excel[0][1][0] => $row[0],
                            $excel[0][1][1] => $grade['code']??$row[1],
                            "Academic Period" => $examCentre["exams"]["academicPeriod"]["start_year"],
                            "Exam Code" => $examCentre['exams']['code'],
                            "Centre Code" => $examCentre['code'],
                            "OpenEMIS ID" => "",
                            "Name" => "",
                            "Subject Code" => $subject['examinationSubject']['code'],
                        ],
                        'errors' => $errors
                    ];
                } else {
                    $student = $this->getExamStudent($optionId, $row[0]);
                    
                    if (isset($student) && count($student['examinationStudentsOption']) > 0) {
                       
                        if (isset($grade)) {

                            $stored = ExaminationStudentsForecastGrade::updateOrCreate(
                                [
                                    'examination_students_id' => $student->id,
                                    'examination_options_id' => $optionId
                                ],
                                [
                                    'grade' => $row[1],
                                    'modified_user_id' => JWTAuth::user()->id,
                                    'modified' => Carbon::now()->toDateTimeString(),
                                    'created_user_id' => JWTAuth::user()->id,
                                    'created' => Carbon::now()->toDateTimeString(),
                                    'id' => Str::uuid(),
                                ]
                            );
                            if ($stored->wasRecentlyCreated) {
                                $add_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        $excel[0][1][0] => $row[0],
                                        $excel[0][1][1] => $grade['code'],
                                        "Academic Period" => $examCentre["exams"]["academicPeriod"]["start_year"],
                                        "Exam Code" => $examCentre['exams']['code'],
                                        "Centre Code" => $examCentre['code'],
                                        "OpenEMIS ID" => $student['securityUser']['openemis_no'],
                                        "Name" => $student['securityUser']['first_name'] . " " . $student['securityUser']['middle_name'] . " " . $student['securityUser']['third_name'] . " " . $student['securityUser']['last_name'],
                                        "Subject Code" => $subject['examinationSubject']['code'],
                                    ],
                                    'errors' => $errors
                                ];
                            } else {
                                $stored_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        $excel[0][1][0] => $row[0],
                                        $excel[0][1][1] => $grade['code'],
                                        "Academic Period" => $examCentre["exams"]["academicPeriod"]["start_year"],
                                        "Exam Code" => $examCentre['exams']['code'],
                                        "Centre Code" => $examCentre['code'],
                                        "OpenEMIS ID" => $student['securityUser']['openemis_no'],
                                        "Name" => $student['securityUser']['first_name'] . " " . $student['securityUser']['middle_name'] . " " . $student['securityUser']['third_name'] . " " . $student['securityUser']['last_name'],
                                        "Subject Code" => $subject['examinationSubject']['code'],
                                    ],
                                    'errors' => $errors
                                ];
                            }
                        } else {
                            $label = $excel[0][1][0];
                            $errors[$label] = 'Forecast Grade does not exists.';
                            $validation[] = [
                                'row_number' => $i,
                                'data' => [
                                    $excel[0][1][0] => $row[0],
                                    $excel[0][1][1] => $row[1],
                                    "Academic Period" => $examCentre["exams"]["academicPeriod"]["start_year"],
                                    "Exam Code" => $examCentre['exams']['code'],
                                    "Centre Code" => $examCentre['code'],
                                    "OpenEMIS ID" => "",
                                    "Name" => "",
                                    "Subject Code" => $subject['examinationSubject']['code'],
                                ],
                                'errors' => $errors
                            ];
                        } 
                    } else {
                        $label = $excel[0][1][0];
                        $errors[$label] = 'Candidate ID is not linked with selected option';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                $excel[0][1][0] => $row[0],
                                $excel[0][1][1] => $grade['code']??$row[1],
                                "Academic Period" => $examCentre["exams"]["academicPeriod"]["start_year"],
                                "Exam Code" => $examCentre['exams']['code'],
                                "Centre Code" => $examCentre['code'],
                                "OpenEMIS ID" => "",
                                "Name" => "",
                                "Subject Code" => $subject['examinationSubject']['code'],
                            ],
                            'errors' => $errors
                        ];
                    }
                }
            }
            $importResponse = [
                'total_count' => count($excel[0]) - 2,
                'records_added' => [
                    'count' => count($add_data),
                    'rows' => $add_data,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];
            
            DB::commit();
            return $importResponse;
        } catch (\Exception $e) {
            Log::error(
                'Failed to import forecast grades in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to import forecast grades in DB");
        }
    }

    /**
     * Getting Examination Student.
     * @param string $optionId
     * @param string $candidateId
     * @return JsonResponse
    */
    public function getExamStudent(string $optionId, string $candidateId)
    {
        try {
            $student = ExaminationStudent::select('id')
                        ->select(
                            'id',
                            'candidate_id',
                            'student_id'
                        )
                        ->with(
                            [
                                'securityUser' => function ($query) {
                                    $query->select(
                                        'id',
                                        'first_name',
                                        'middle_name',
                                        'third_name',
                                        'last_name',
                                        'openemis_no'
                                    )
                                    ->where('status', config('constants.students.status'));
                                },
                                'examinationStudentsOption' => function ($query) use ($optionId) {
                                    $query->select(
                                        'id',
                                        'examination_students_id',
                                        'examination_options_id'
                                    )
                                    ->where('examination_options_id', $optionId);
                                }
                            ]
                        )
                        ->where('candidate_id', $candidateId)
                        ->first();
            return $student;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get examination student',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get examination student");
        }
    }

    /**
     * Getting Option Subject.
     * @param string $optionId
     * @return JsonResponse
    */
    public function getOptionSubject(string $optionId)
    {
        try {
            $subject = ExaminationOption::select(
                    'id',
                    'examination_subject_id'
                )
                ->where('id', $optionId)
                ->with(
                    [
                        'examinationSubject' => function ($query) {
                            $query->select('id', 'code');
                        }
                    ]
                )
                ->first();
            return $subject;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get examination subject',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get examination subject");
        }
    }

    /**
     * Getting Grading Value.
     * @param string $gradeId
     * @return JsonResponse
    */
    public function getGradeValue($gradeId = 0)
    {
        try {
            $grade = ExaminationGradingOption::where('id', $gradeId)
                    ->where('visible', config('constants.visibleValue.visibleCode'))
                    ->select('id', 'code')
                    ->first();
            return $grade;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get examination subject',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get examination subject");
        }
    }
}
