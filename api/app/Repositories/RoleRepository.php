<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\SecurityRole;
use App\Models\SecurityFunction;
use App\Models\SecurityRoleFunction;
use Tymon\JWTAuth\Facades\JWTAuth;

class RoleRepository extends Controller
{
    /**
     * Getting User Role List
     * @param array $data
     * @param int $groupId
     * @return JsonResponse
     */
    public function userRolesList(array $data, int $groupId)
    {
        try {
            $rolesList = SecurityRole::select('id', 'name', 'visible', 'order')->where('security_group_id', $groupId)
                    ->orderBy('order');

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $rolesList->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%");
                });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $rolesCount = $rolesList;
                $total = $rolesCount->count();
                $rolesList->skip($data['start'])
                                ->take($data['end'] - $data['start']);
            } else {
                $rolesCount = $rolesList;
                $total = $rolesCount->count();
            }
            $roles = $rolesList->get();

            $data['roles'] = $roles;
            $data['total'] = $total;
            return $data; 
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    /**
     * Getting User Role Details
     * @param int $roleId
     * @return JsonResponse
     */
    public function userRoleView(int $roleId)
    {
        try {
            $roleDetails = SecurityRole::select(
                'id',
                'name', 
                'visible', 
                'security_group_id', 
                'modified_user_id', 
                'modified', 
                'created_user_id', 
                'created'
            )
            ->where('id', $roleId)
            ->with(
                [
                    'securityGroup' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'createdByUser' => function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                            ->where('status', config('constants.createdByUser.status'));
                    },
                    'modifiedUser' => function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                            ->where('status', config('constants.modifiedByUser.status'));
                    }
                ]
            )
            ->first();

            return $roleDetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch role details from DB");
        }
    }

    /**
     * Storing User Role Details
     * @param array $data
     * @return int
     */
    public function userRoleStore(array $data)
    {
        DB::beginTransaction();
        try {
            $order = $this->getLargestRoleOrder($data['security_group']);
            $role['name'] = $data['name'];
            $role['security_group_id'] = $data['security_group'];
            $role['order'] = $order;
            $role['created_user_id'] = JWTAuth::user()->id;
            $role['created'] = Carbon::now()->toDateTimeString();
            $store = SecurityRole::insert($role);
            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store role details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store role details in DB");
        }
    }

    /**
     * Getting largest role order
     * @param int $groupId
     * @return int $largestOrder
     */
    public function getLargestRoleOrder(int $groupId = -1)
    {
        try {
            $largestOrder = 0;
            $order = SecurityRole::where('security_group_id', $groupId)->max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest role order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch largest role order from DB");
        }
    }

    /**
     * Updating User Role Details
     * @param array $data
     * @return int
     */
    public function userRoleUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $roleArr['name'] = $data['name'];
            $roleArr['visible'] = $data['visible'];
            $roleArr['modified_user_id'] = JWTAuth::user()->id;
            $roleArr['modified'] = Carbon::now()->toDateTimeString();
            
            $update = SecurityRole::where('id', $data['id'])->update($roleArr);
            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update user role details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update user role details in DB");
        }
    }

    /**
     * Getting System Role List
     * @param array $data
     * @return JsonResponse
     */
    public function systemRolesList(array $data)
    {
        try {
            $systemRole = SecurityRole::select('id', 'name', 'visible', 'order')->where('security_group_id', config('constants.systemRoles.securityGroupId'))
                    ->orderBy('order');

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $systemRole->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%");
                });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $rolesCount = $systemRole;
                $total = $rolesCount->count();
                $systemRole->skip($data['start'])
                                ->take($data['end'] - $data['start']);
            } else {
                $rolesCount = $systemRole;
                $total = $rolesCount->count();
            }
            $roles = $systemRole->get();

            $data['roles'] = $roles;
            $data['total'] = $total;

            return $data; 
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    /**
     * Getting System Role Details
     * @param int $roleId
     * @return JsonResponse
     */
    public function systemRoleView(int $roleId)
    {
        try {
            $roleDetails = SecurityRole::select(
                'id',
                'name', 
                'visible',
                'modified_user_id', 
                'modified', 
                'created_user_id', 
                'created'
            )
            ->where('id', $roleId)
            ->with(
                [
                    'createdByUser' => function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                            ->where('status', config('constants.createdByUser.status'));
                    },
                    'modifiedUser' => function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                            ->where('status', config('constants.modifiedByUser.status'));
                    }
                ]
            )
            ->first();

            return $roleDetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch role details from DB");
        }
    }

    /**
     * Storing System Role Details
     * @param array $data
     * @return int
     */
    public function systemRoleStore(array $data)
    {
        DB::beginTransaction();
        try {
            $order = $this->getLargestRoleOrder();
            $role['name'] = $data['name'];
            $role['security_group_id'] = config('constants.systemRoles.securityGroupId');
            $role['order'] = $order;
            $role['created_user_id'] = JWTAuth::user()->id;
            $role['created'] = Carbon::now()->toDateTimeString();
            $store = SecurityRole::insert($role);
            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store role details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store role details in DB");
        }
    }

    /**
     * Updating System Role Details
     * @param array $data
     * @return int
     */
    public function systemRoleUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $roleArr['visible'] = $data['visible'];
            $roleArr['modified_user_id'] = JWTAuth::user()->id;
            $roleArr['modified'] = Carbon::now()->toDateTimeString();
            
            $update = SecurityRole::where('id', $data['id'])->update($roleArr);
            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest role order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to fetch largest role order from DB");
        }
    }

    /**
     * Updating System Role Orders
     * @param array $rolesOrder
     * @param int $groupId
     * @return int
     */
    public function securityRoleReorder(array $rolesOrder, int $groupId)
    {
        DB::beginTransaction();
        try {
            foreach ($rolesOrder['rolesOrder'] as $key => $order) {
                $update = SecurityRole::where('id', $key)
                            ->where('security_group_id', $groupId)
                            ->update(['order' => $order]);
            }
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update role orders in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update role orders in DB");
        }
    }

    /**
     * Getting Role Permission List
     * @param array $data
     * @param int $roleId
     * @return JsonResponse
     */
    public function permissionList(array $data, int $roleId)
    {
        try {
            $module = "";
            if (isset($data['module'])) {
                if ($data['module'] == 'main') {
                    $module = 'Main';
                } elseif ($data['module'] == 'reports') {
                    $module = 'Reports';
                } elseif ($data['module'] == 'administration') {
                    $module = 'Administration';
                } else {
                    $module = "";
                }
            }
            $function = SecurityRole::select('id')
                        ->with(
                            [
                                'securityFunction' => function ($q) use ($module) {
                                    $q->select(
                                        'name',
                                        'controller',
                                        'module',
                                        'category',
                                        'parent_id',
                                        'security_function_id'
                                    )
                                    ->withPivot(
                                        '_view',
                                        '_edit', 
                                        '_add', 
                                        '_delete', 
                                        '_execute'
                                    )
                                    ->where('visible', config('constants.visibleValue.visibleCode'));
                                    if ($module != "") {
                                        $q->where('module', $module);
                                    }
                                }
                            ]
                        )
                        ->where('id', $roleId)
                        ->first();
            return $function;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role permission list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch role permission list from DB");
        }
    }

    /**
     * Updating Role Permission List
     * @param array $data
     * @return JsonResponse
     */
    public function permissionListUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $permissionArr = [];
            $functionId = [];
            foreach ($data['permissionList'] as $key => $permissionList) {
                $permissionArr[] = [
                    '_view' => $permissionList['view'],
                    '_edit' => $permissionList['edit'],
                    '_add' => $permissionList['add'],
                    '_delete' => $permissionList['delete'],
                    '_execute' => $permissionList['execute'],
                    'security_role_id' => $data['roleId'],
                    'security_function_id' => $permissionList['securityFunctionId'],
                    'created_user_id' => JWTAuth::user()->id,
                    'created' => Carbon::now()->toDateTimeString()
                ];
                $functionId[] = $permissionList['securityFunctionId'];
            }

            $role = SecurityRole::find($data['roleId']);
            if ($role) {
                $permissions = $role->securityFunction()
                                    ->wherePivotIn('security_function_id', $functionId)
                                    ->wherePivot('security_role_id', $data['roleId'])
                                    ->detach();
                
                SecurityRoleFunction::insert($permissionArr);
                DB::commit();
                return 1;
            } else {
                DB::commit();
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update role permission list in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update role permission list in DB");
        }
    }
}
