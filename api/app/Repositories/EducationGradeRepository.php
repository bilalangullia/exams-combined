<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\EducationGrade;
use App\Models\EducationSubject;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Str;

class EducationGradeRepository extends Controller
{
    /**
     * Getting Education Grades List
     * @param array $data
     * @param int $programId
     * @return JsonResponse
     */
    public function educationGrades(array $data, int $programId)
    {
        try {
            $gradeList = EducationGrade::select(
                            'id', 
                            'name', 
                            'code', 
                            'visible', 
                            'order', 
                            'education_programme_id',
                            'education_stage_id'
                        )
                        ->with(
                            [
                                'educationProgramme' => function ($q) {
                                    $q->select('id', 'name');
                                },
                                'educationStage' => function ($q) {
                                    $q->select('id', 'name');
                                }
                            ]
                        )
                        ->withCount('educationGradeSubject')
                        ->where('education_programme_id', $programId);

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $gradeList->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%")
                        ->orWhere('code', 'LIKE', "%" . $keyword . "%")
                        ->orWhereHas(
                            'educationProgramme',
                            function ($query) use ($keyword) {
                                $query->where('name', 'LIKE', "%" . $keyword . "%");
                            }
                        )
                        ->orWhereHas(
                            'educationStage',
                            function ($query) use ($keyword) {
                                $query->where('name', 'LIKE', "%" . $keyword . "%");
                            }
                        );
                    });
            }
        
            if (isset($data['start']) && isset($data['end'])) {
                $gradeCount = $gradeList;
                $total = $gradeCount->count();
                $gradeList->skip($data['start'])
                            ->take($data['end'] - $data['start']);
            } else {
                $gradeCount = $gradeList;
                $total = $gradeCount->count();
            }

            $list = $gradeList->get();
            
            $data['list'] = $list;
            $data['total'] = $total;
            
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade list");
        }
    }

    /**
     * Getting Education Grades Details
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeView(int $gradeId)
    {
        try {
            $gradeData = EducationGrade::select(
                            'id', 
                            'name', 
                            'code', 
                            'visible',
                            'education_programme_id',
                            'education_stage_id',
                            'admission_age',
                            'modified_user_id',
                            'modified',
                            'created_user_id',
                            'created'
                        )
                        ->with(
                            [
                                'educationProgramme' => function ($q) {
                                    $q->select('id', 'name');
                                },
                                'educationStage' => function ($q) {
                                    $q->select('id', 'name');
                                },
                                'GradeSubjects' => function ($q) {
                                    $q->select(
                                        'hours_required',
                                        'education_grade_id',
                                        'education_subject_id',
                                        'name',
                                        'code'
                                    )
                                    ->wherePivot('visible', config('constants.visibleValue.visibleCode'));
                                },
                                'createdByUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.createdByUser.status'));
                                },
                                'modifiedUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.modifiedByUser.status'));
                                }
                            ]
                        )
                        ->where('id', $gradeId)
                        ->first();

            return $gradeData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade list");
        }
    }

    /**
     * Storing Education Grades Details
     * @param array $data
     * @return int
     */
    public function educationGradeStore(array $data)
    {
        DB::beginTransaction();
        try {
            $order = $this->getLargestGradeOrder($data['education_programme']);
            
            $insertArr['name'] = $data['name'];
            $insertArr['code'] = $data['code'];
            $insertArr['admission_age'] = $data['admission_age'];
            $insertArr['education_stage_id'] = $data['education_stage'];
            $insertArr['education_programme_id'] = $data['education_programme'];
            $insertArr['visible'] = config('constants.visibleValue.visibleCode');
            $insertArr['order'] = $order;
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();
            
            $store = EducationGrade::insert($insertArr);
            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store education grade details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store education grade details in DB");
        }
    }

    /**
     * Getting largest grade order
     * @param int $programId
     * @return int $largestOrder
     */
    public function getLargestGradeOrder(int $programId = 0)
    {
        try {
            $largestOrder = 0;
            $order = EducationGrade::where('education_programme_id', $programId)->max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest grade order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch largest grade order from DB");
        }
    }

    /**
     * Updating Education Grades Details
     * @param array $data
     * @return int
     */
    public function educationGradeUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $id = $data['id'];
            $updateArr['name'] = $data['name'];
            $updateArr['code'] = $data['code'];
            $updateArr['education_stage_id'] = $data['education_stage'];
            $updateArr['visible'] = $data['visible'];
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            $updateArr['modified'] = Carbon::now()->toDateTimeString();

            $update = EducationGrade::where('id', $id)->update($updateArr);
            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update education grade details in DB");
        }
    }

    /**
     * Getting Education Grade Subjects
     * @param array $data
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeSubjects(array $data, int $gradeId)
    {
        try {
            $total = 0;
            $subjects = EducationGrade::select('id')
                        ->with(
                            [
                                'GradeSubjects' => function ($q) use ($data, $gradeId, $total) {
                                    $q->select(
                                        'education_grade_id', 
                                        'name', 
                                        'code', 
                                        'hours_required', 
                                        'auto_allocation', 
                                        'education_subject_id'
                                    );
                                    if (isset($data['start']) && isset($data['end'])) {
                                        $q->skip($data['start'])
                                            ->take($data['end'] - $data['start']);
                                    }
                                }
                            ]
                        )
                        ->where('id', $gradeId)
                        ->first();

            $grade  = EducationGrade::find($gradeId);
            
            if ($grade) {
                $total = $grade->GradeSubjects()->count();
            }

            $data['list'] = $subjects;
            $data['total'] = $total;
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects list");
        }
    }

    /**
     * Getting Education Grade Subject Details
     * @param int $gradeId
     * @param int $subjectId
     * @return JsonResponse
     */
    public function educationGradeSubjectView(int $gradeId, int $subjectId)
    {
        try {
            $grade = EducationGrade::select(
                        'id', 
                        'name', 
                        'code',
                        'education_programme_id',
                        'modified_user_id',
                        'created_user_id'
                    )
                    ->with(
                        [
                            'educationProgramme' => function ($q) {
                                $q->select('id', 'code', 'name', 'education_cycle_id');
                            },
                            'educationProgramme.educationCycle.educationLevel' => function ($q) {
                                $q->select('id', 'name');
                            },
                            'GradeSubjects' => function ($q) use ($subjectId) {
                                $q->select(
                                    'education_grade_id', 
                                    'education_subject_id', 
                                    'hours_required', 
                                    'auto_allocation',
                                    'name',
                                    'code'
                                )
                                ->withPivot(
                                    'modified_user_id', 
                                    'modified', 
                                    'created_user_id', 
                                    'created'
                                )
                                ->where('education_subject_id', $subjectId);
                            },
                            'createdByUser' => function ($query) {
                                $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                    ->where('status', config('constants.createdByUser.status'));
                            },
                            'modifiedUser' => function ($query) {
                                $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                    ->where('status', config('constants.modifiedByUser.status'));
                            }
                        ]
                    )
                    ->where('id', $gradeId)
                    ->first();
            
            return $grade;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects details");
        }
    }

    /**
     * Getting Education Grade Details
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeSubjectAdd(int $gradeId)
    {
        try {
            $gradeData = EducationGrade::select(
                        'id', 
                        'name', 
                        'code',
                        'education_programme_id'
                    )
                    ->with(
                        [
                            'educationProgramme.educationCycle.educationLevel' => function ($q) {
                                $q->select('id', 'name');
                            }
                        ]
                    )
                    ->where('id', $gradeId)
                    ->first();

            return $gradeData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects details");
        }
    }

    /**
     * Storing Education Grade Subject Details
     * @param array $data
     * @return JsonResponse
     */
    public function educationGradeSubjectStore(array $data)
    {
        DB::beginTransaction();
        try {
            $insertArr['id'] = Str::uuid();
            $insertArr['hours_required'] = $data['hours_required'];
            $insertArr['auto_allocation'] = $data['auto_allocation'];
            $insertArr['education_subject_id'] = $data['education_subject'];
            $insertArr['hours_required'] = $data['hours_required'];
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();

            $grade = EducationGrade::find($data['education_grade_id']);
            if ($grade) {
                $is_exists = $grade->GradeSubjects()->where('education_subject_id', $data['education_subject'])->exists();
                if ($is_exists) {
                    DB::commit();
                    return 2;
                } else {
                    $store = $grade->GradeSubjects()->attach($data['education_grade_id'], $insertArr);
                    DB::commit();
                    return 1;
                }
            } else {
                DB::commit();
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store education grade subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store education grade subject details");
        }
    }

    /**
     * Updating Education Grade Subject Details
     * @param EducationGradeSubjectUpdateRequest $request
     * @return JsonResponse
     */
    public function educationGradeSubjectUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $updateArr['hours_required'] = $data['hours_required'];
            $updateArr['auto_allocation'] = $data['auto_allocation'];
            $updateArr['modified'] = Carbon::now()->toDateTimeString();
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            
            $grade = EducationGrade::find($data['education_grade_id']);
            if ($grade) {
                $is_exists = $grade->GradeSubjects()->where('education_subject_id', $data['education_subject_id'])->exists();
                if ($is_exists) {
                    $update = $grade->GradeSubjects()->updateExistingPivot($data['education_subject_id'], $updateArr);
                    DB::commit();
                    return 1;
                } else {
                    DB::commit();
                    return 0;
                }
            } else {
                DB::commit();
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update education grade subject details");
        }
    }

    /**
     * Getting Setup list.
     * @param array $data
     * @param int $setupId
     * @return JsonResponse
     */
    public function educationGradeSetUp(array $data, int $setupId)
    {
        try {
            $list = [];
            if ($setupId == 2) {
                $list = $this->getSubjectsList($data);
            } else {
                $list = [];
            }
            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education setup list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education setup list");
        }
    }

    /**
     * Getting Setup Subject list.
     * @param array $data
     * @return JsonResponse
     */
    public function getSubjectsList(array $data)
    {
        try {
            $subjectList = EducationSubject::select('id', 'code', 'name', 'order', 'visible');

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $subjectList->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%")
                        ->orWhere('code', 'LIKE', "%" . $keyword . "%");
                    });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $subjectCount = $subjectList;
                $total = $subjectCount->count();
                $subjectList->skip($data['start'])
                            ->take($data['end'] - $data['start']);
            } else {
                $subjectCount = $subjectList;
                $total = $subjectCount->count();
            }

            $list = $subjectList->orderBy('order')->get()->toArray();
            $subjects['start'] = $data['start'];
            $subjects['end'] = $data['end'];
            $subjects['list'] = $list;
            $subjects['total'] = $total;
            return $subjects; 
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education setup list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education setup list");
        }
    }

    /**
     * Getting Setup subject details.
     * @param int $subjectId
     * @return JsonResponse
     */
    public function educationSetupSubjectView(int $subjectId)
    {
        try {
            $subject = EducationSubject::select(
                            'id', 
                            'name', 
                            'code', 
                            'visible',
                            'modified_user_id',
                            'modified',
                            'created_user_id',
                            'created'
                        )
                        ->with(
                            [
                                'fieldOfStudy' => function ($q) {
                                    $q->select(
                                        'education_subject_id',
                                        'education_field_of_study_id',
                                        'name'
                                    )
                                    ->where('visible', config('constants.visibleValue.visibleCode'));
                                },
                                'createdByUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.createdByUser.status'));
                                },
                                'modifiedUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.modifiedByUser.status'));
                                }
                            ]
                        )
                        ->where('id', $subjectId)
                        ->first();
            return $subject;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get subject details");
        }
    }

    /**
     * Storing Setup subject details.
     * @param array $data
     * @return JsonResponse
     */
    public function educationSetupSubjectStore(array $data)
    {
        DB::beginTransaction();
        try {
            $order = $this->getLargestSubjectOrder();
            $insertArr['name'] = $data['name'];
            $insertArr['code'] = $data['code'];
            $insertArr['order'] = $order;
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();

            $subjectId = EducationSubject::insertGetId($insertArr);
            $subject = EducationSubject::find($subjectId);
            if (count($data['fieldOfStudies']) > 0) {
                foreach ($data['fieldOfStudies'] as $key => $studyId) {
                    $attach['id'] = Str::uuid();
                    $attach['education_field_of_study_id'] = $studyId;
                    $attach['created_user_id'] = JWTAuth::user()->id;
                    $attach['created'] = Carbon::now()->toDateTimeString();
                    $subject->fieldOfStudy()->attach($subjectId, $attach);
                }
            }
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            Log::error(
                'Failed to store subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store subject details");
        }
    }

    /**
     * Getting largest education subject order
     * @return int $largestOrder
     */
    public function getLargestSubjectOrder()
    {
        try {
            $largestOrder = 0;
            $order = EducationSubject::max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest grade order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch largest grade order from DB");
        }
    }

    /**
     * Updating Setup subject details.
     * @param array $data
     * @return JsonResponse
     */
    public function educationSetupSubjectUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $updateArr['name'] = $data['name'];
            $updateArr['code'] = $data['code'];
            $updateArr['visible'] = $data['visible'];
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            $updateArr['modified'] = Carbon::now()->toDateTimeString();

            $update = EducationSubject::where('id', $data['id'])->update($updateArr);

            $subject = EducationSubject::find($data['id']);
            if ($subject) {
                $subject->fieldOfStudy()->detach();
                if (count($data['fieldOfStudies']) > 0) {
                    foreach ($data['fieldOfStudies'] as $key => $studyId) {
                        $attach['id'] = Str::uuid();
                        $attach['education_field_of_study_id'] = $studyId;
                        $attach['created_user_id'] = JWTAuth::user()->id;
                        $attach['modified_user_id'] = JWTAuth::user()->id;
                        $attach['created'] = Carbon::now()->toDateTimeString();
                        $attach['modified'] = Carbon::now()->toDateTimeString();
                        $subject->fieldOfStudy()->attach($data['id'], $attach);
                    }
                }
            }
            
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            Log::error(
                'Failed to store subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store subject details");
        }
    }

    /**
     * Updating Education grades order
     * @param array $data
     * @return JsonResponse
     */
    public function educationGradeReorder(array $data)
    {
        DB::beginTransaction();
        try {
            foreach ($data['gradesOrder'] as $key => $order) {
                $update = EducationGrade::where('id', $key)
                            ->update(['order' => $order]);
            }
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade order details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update education grade order details");
        }
    }

    /**
     * Updating Education subject order
     * @param array $data
     * @return JsonResponse
     */
    public function educationSubjectReorder(array $data)
    {
        DB::beginTransaction();
        try {
            foreach ($data['SubjectOrder'] as $key => $order) {
                $update = EducationSubject::where('id', $key)
                            ->update(['order' => $order]);
            }
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education subject order details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education subject order details");
        }
    }

    /**
     * Checking if Education grade is safe to delete.
     * @param int $gradeId
     * @return JsonResponse
     */
    public function checkIfSafeToDelete(int $gradeId)
    {
        try {
            $grade = EducationGrade::find($gradeId);
            
            if ($grade) {
                $gradeSubject = $grade->GradeSubjects()->exists();
                if ($gradeSubject) {
                    return false;
                }
                $exams = $grade->exams()->exists();
                if ($exams) {
                    return false;
                }
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete education grade',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete education grade");
        }
    }

    /**
     * Deleting Education grade
     * @param int $gradeId
     * @param int $parentId
     * @return JsonResponse
     */
    public function deleteEducationGrade(int $gradeId, int $parentId)
    {
        try {
            if ($parentId == 0) {
                return EducationGrade::where('id', $gradeId)->delete();
            } else {
                return EducationGrade::where('education_programme_id', $parentId)->delete();
            }
            return 0;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete education grade',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete education grade");
        }
    }

    /**
     * Deleting Education grade subject
     * @param int $gradeId
     * @param int $subjectId
     * @return JsonResponse
     */
    public function educationGradeSubjectDelete(int $gradeId, int $subjectId)
    {
        try {
            $educationGrade = EducationGrade::find($gradeId);
            if ($educationGrade) {
                $delete = $educationGrade->educationGradeSubject()->where('education_subject_id', $subjectId)->delete();

                return $delete;
            }
            return 0;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete education grade',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete education grade");
        }
    }
}
