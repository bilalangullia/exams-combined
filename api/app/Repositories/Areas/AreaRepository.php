<?php

namespace App\Repositories\Areas;

use App\Models\Area;
use Illuminate\Support\Facades\Log;

class AreaRepository
{
    public function getAreaNameCodeByAreaId($areaId)
    {
        try {
            $region = Area::select('name', 'code')->find($areaId);
            Log::info('fetched region name and code from by region id', ['method' => __METHOD__, 'data' => ['regionId' => $areaId, 'regionData' => $region]]);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch region name and code by region id',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return array();
        }
        if (!$region) {
            return array();

        }
        return $region->toArray();
    }
}