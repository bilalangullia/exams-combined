<?php

namespace App\Repositories\ExaminationSubjects;

use App\Models\ExaminationSubject;
use DB;
use Illuminate\Support\Facades\Log;

class ExaminationSubjectRepository
{
    public function getSubjectsByExamStudentId($examStudentId)
    {
        try {
            $subjects = DB::table('examination_students')
                ->join('examination_students_options', 'examination_students.id', '=', 'examination_students_options.examination_students_id')
                ->join('examination_options', 'examination_options.id', '=', 'examination_students_options.examination_options_id')
                ->join('examination_subjects', 'examination_subjects.id', '=', 'examination_options.examination_subject_id')
                ->select('examination_subjects.id', 'examination_subjects.code', 'examination_subjects.name')
                ->where('examination_students.id', '=', $examStudentId)
                ->get();
            Log::info('Fetched Subjects details by exam student id', ['method' => __METHOD__, 'data' => ['examStudentId' => $examStudentId, 'subjectData' => $subjects]]);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subject details by exam student id',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return array();
        }
        if (!$subjects) {
            return array();
        }
        return $subjects->toArray();
    }
}