<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\EducationLevel;
use App\Models\EducationSystem;
use App\Models\EducationLevelIsced;
use JWTAuth;
use Carbon\Carbon;

class EducationLevelRepository extends Controller
{
	public function getEducationLevelList(Request $request)
	{
		try{
			$list = EducationLevel::where('education_system_id', $request->education_system_id)
					->select('id', 'name', 'visible', 'education_system_id', 'education_level_isced_id')->with('educationSystem:id,name', 'educationLevelIsced:id,name');

			if (isset($request['start']) && isset($request['end'])) {
				$listCount = $list;
				$total     = $listCount->count();
				$list->skip($request['start'])
				->take($request['end'] - $request['start']);
			} else {
				$listCount = $list;
				$total = $listCount->count();
			}

			if (isset($request['keyword'])) {
                $listSearch = $list->where(function ($q) use ($request){
					$q->where(
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->whereHas(
                            'educationSystem',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
		                        'educationLevelIsced',
		                        function ($query) use ($request) {
		                            $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
		                        }
		                    );
						}
					);
            }
            
            $listSearch = $list->get();
            
            return array("record" => $listSearch, "totalRecord" => $total);
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level List Not Found');
		}
	}


	public function getEducationLevelDetails(int $eduLvlId)
	{
		try{
			$data = EducationLevel::where('id', $eduLvlId)->select('id', 'name', 'visible', 
					'education_system_id', 'education_level_isced_id','modified_user_id','modified','created_user_id','created')
					->with('educationSystem:id,name', 'educationLevelIsced:id,name')->get();
			

			return $data;
			} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level Detail Not Found');
		}
	}

	public function educationSystemDropdown()
	{
		try{
			$eduSysDrpdwn = EducationSystem::select('id', 'name')->get();

			return $eduSysDrpdwn;
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System Dropdown Not Found');
		}
	}

	public function educationLevelIscedDropdown()
	{
		try{
			$eduLvlIscdDrpdwn = EducationLevelIsced::select('id', 'name')->get();

			return $eduLvlIscdDrpdwn;
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level Isced Dropdown Not Found');
		}
	}

	public function getLargestEducationLevelOrder(int $eduSysId = 0)
    {
        try {
            $largestOrder = 0;
            $order = EducationLevel::where('education_system_id', $eduSysId)->max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch largest Education level order from DB");
        }
    }

	public function addEducationLevel(array $data)
	{
		try{
			$order = $this->getLargestEducationLevelOrder($data['education_system_id']);
			$add = new EducationLevel();
			$add['name'] = $data['name'];
			$add['order'] = $order;
			$add['visible'] = config('constants.visibleValue.visibleCode');
			$add['education_system_id'] = $data['education_system_id'];
			$add['education_level_isced_id'] = $data['education_level_isced_id'];
			$add['created_user_id'] = JWTAuth::user()->id;
			$add['created'] = Carbon::now()->toDateTimeString();
			$store = $add->save();

			return $store;
		} catch (\Exception $e) {
			echo $e;
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level not added');
		}
	}

	public function updateEducationLevel(int $eduLevelId, array $data)
	{
		try{
			$updateData = EducationLevel::find($eduLevelId);
			$updateData['name'] = $data['name'];
			$updateData['visible'] = $data['visible'];
			$updateData['education_system_id'] = $data['education_system_id'];
			$updateData['education_level_isced_id'] = $data['education_level_isced_id'];
			$updateData['modified_user_id'] = JWTAuth::user()->id;
			$updateData['modified'] = Carbon::now()->toDateTimeString();
			$update = $updateData->save();

			return $update;
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level not updated');
		}
	}

	public function checkIfSafeToDelete(int $eduLevelId)
	{
		try{
			
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level  Can Not Deleted');
		}

	}

	public function deleteLevel(int $eduLevelId)
	{
		try{
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level  Can Not Deleted');
		}
	}
	
}