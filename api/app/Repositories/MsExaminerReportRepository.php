<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Helper\SlugHelper;
use PDF;
use Storage;
use App\Models\ReportProgress;
use App\Models\AcademicPeriod;
use App\Models\Examination;
use App\Models\ExaminationStudent;
use App\Models\ExaminationCentre;
use App\Models\ExaminationSubject;
use App\Models\ExaminationComponent;
use Tymon\JWTAuth\Facades\JWTAuth;

class MsExaminerReportRepository extends Controller
{
    /**
     * Generating Report
     * @param MsExaminerReportGenerateRequest $request
     * @return JsonResponse
     */
    public function generate(array $data)
    {
        DB::beginTransaction();
        try {
            $reportData = [];
            $requestData = $data;
            if ($requestData['report_name'] == 2) {
                $reportData['reportName'] = "MS1 Internal Assessment Mark Sheet";
                $blade = "pdf.ms_internal";
            } elseif ($requestData['report_name'] == 3) {
                $reportData['reportName'] = "MS2 Examiner Mark Sheet";
                $blade = "pdf.ms_examiner";
            } else {
                $reportData['reportName'] = "Scaling Form";
                $blade = "pdf.scalling_form";
            }

            $academicPeriod = AcademicPeriod::where('id', $requestData['academic_period_id'])->select('id', 'start_year')->first()->toArray();
            $academicPeriod['codeArray'] = str_split($academicPeriod['start_year']);
            
            $exam = Examination::where('id', $requestData['examination_id'])->select('id', 'code', 'name')->first()->toArray();
            $examCentre = ExaminationCentre::select('id', 'name', 'code')->where('id', $requestData['examination_centre_id'])->first()->toArray();
            $examCentre['codeArray'] = str_split($examCentre['code']);
            
            $examSubject = ExaminationSubject::where('id', $requestData['subject_id'])->select('id', 'code', 'name')->first()->toArray();
            $examSubject['codeArray'] = str_split($examSubject['code']);
            
            $examComponent = ExaminationComponent::where('id', $requestData['component_id'])
                            ->select('id', 'code', 'name', 'examination_grading_type_id')
                            ->with(
                                [
                                    'examinationGradingType' => function ($q) {
                                        $q->select('id', 'max');
                                    }
                                ]
                            )
                            ->first()
                            ->toArray();

            $examComponent['codeArray'] = explode("/", $examComponent['code']);
            $examComponent['codeArray'] = str_split($examComponent['codeArray'][1]);

            $reportData['data'] = $requestData;
            $reportData['academicPeriod'] = $academicPeriod;
            $reportData['exam'] = $exam;
            $reportData['examCentre'] = $examCentre;
            $reportData['examSubject'] = $examSubject;
            $reportData['examComponent'] = $examComponent;
            $reportData['candidates'] = $this->getCandidatesList(
                $requestData['examination_id'],
                $requestData['examination_centre_id']
            );

            if (count($reportData['candidates']) > 0) {
                foreach ($reportData['candidates'] as $key => $candidate) {
                    $reportData['candidates'][$key]['codeArray'] = str_split($candidate['candidate_id']);
                    $dob = $reportData['candidates'][$key]['security_user']['date_of_birth'];
                    $reportData['candidates'][$key]['security_user']['date_of_birth'] = date("d-M-y", strtotime($dob));
                }
            }
            $date = Date('Y-m-d');
            
            $reportData['date'] = date("d-M-y", strtotime($date));
            
            $storage_path = 'public/reports';
            $saveDataArr = $this->saveReportData($reportData);

            $file_name = $saveDataArr['file_name'];
            unset($saveDataArr['file_name']);
            $report = ReportProgress::create($saveDataArr);
            if ($report) {
                $id = $report->id;

                $processing = config('constants.reportUpdateStatus.processing');
                ReportProgress::where('id', $id)->update(['status' => $processing]);

                $timeOut = false;
                for ($i = 0; $i < $saveDataArr['total_records']; $i++) {
                    if (Carbon::now()->toDateTimeString() >= $saveDataArr['expiry_date']) {
                        $timeOut = true;
                        break;
                    }
                    ReportProgress::where('id', $id)->increment('current_records');
                }

                if ($timeOut) {
                    $updateArr['status'] = config('constants.reportUpdateStatus.notCompleted');
                    $updateArr['error_message'] = "report can not be generated due to exceed time";
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);
                    $error['msg'] = "report can not be generated due to exceed time";
                    DB::commit();
                    return 2;
                } else {
                    $pdf = PDF::loadView($blade, $reportData);

                    Storage::put($storage_path . '/' . $file_name, $pdf->output());

                    $updateArr['status'] = config('constants.reportUpdateStatus.completed');
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);

                    Log::info('Pdf Report generated and saved in database and directory', ['method' => __METHOD__, 'data' => []]);
                    DB::commit();
                    return 1;
                }

            } else {
                DB::commit();
                return 0;
            }
        } catch (\Exception $e) {
            echo $e;
            log::error(
                'Failed to genrate report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to genrate report");
        }
    }

    public function saveReportData(array $data)
    {
        try {
            $examCentre = $data['examCentre'];
            $exam = $data['exam'];
            $academicPeriod = $data['academicPeriod'];
            $examCentreName = preg_replace('/[^a-zA-Z0-9_ -]/s','',$examCentre['name']);
            $examCentreName = str_replace(" ","_",$examCentreName);
            $examCentreCode = $examCentre['code'];
            $reportName = $data['reportName'];


            $reportName = $reportName . ": " . $exam['code'] . " " . $academicPeriod['start_year'] . $examCentreCode;


            $fileName = $reportName . '_' .$exam['code'] . '_' .$academicPeriod['start_year']. '_' . $examCentreCode . "_" . time();

            $fileName = SlugHelper::slugify($fileName);
            $params = json_encode($data['data']);

            $storage_path = 'public/reports';
            if (!Storage::exists($storage_path)) {
                Storage::makeDirectory($storage_path, 0755);
            }
            $url = asset('storage/reports/' . $fileName. '.pdf');
            $countCandidates = count($data['candidates']);

            $saveArr['id'] = Str::uuid();
            $saveArr['name'] = $reportName;
            $saveArr['module'] = 'Msexaminer';
            $saveArr['params'] = $params;
            $saveArr['file_path'] = $url;
            $saveArr['total_records'] = $countCandidates;
            $saveArr['status'] = config('constants.reportUpdateStatus.notGeneratedOrNew');
            $saveArr['created'] = Carbon::now()->toDateTimeString();
            $saveArr['modified'] = Carbon::now()->toDateTimeString();
            $saveArr['created_user_id'] = JWTAuth::user()->id;
            $saveArr['expiry_date'] = Carbon::now()->addHours(6)->toDateTimeString();
            $saveArr['modified_user_id'] = JWTAuth::user()->id;
            $saveArr['file_name'] = $fileName. '.pdf';

            return $saveArr;
        } catch (\Exception $e) {
            log::error(
                'Failed to genrate report data',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to genrate report data");
        }
    }

    public function getCandidatesList($examId, $examCenterId)
    {
        try {
            $candidateList = ExaminationStudent::select(
                        'id',
                        'candidate_id',
                        'student_id'
                    )
                    ->with(
                        [
                            'securityUser' => function ($q) {
                                $q->select(
                                    'id',
                                    'first_name',
                                    'middle_name',
                                    'third_name',
                                    'last_name',
                                    'date_of_birth',
                                    'gender_id'
                                )
                                ->where('is_student', config('constants.students.isStudent'))
                                ->where('status', config('constants.students.status'));
                            },
                            'securityUser.gender' => function ($q) {
                                $q->select('id', 'name');
                            }
                        ]
                    )
                    ->where('examination_centre_id', $examCenterId)
                    ->where('examination_id', $examId)
                    ->get()
                    ->toArray();

            $list = [];
            foreach ($candidateList as $key => $candidate) {
                $candidateId = $candidate['candidate_id'];
                $candidateId = substr($candidateId, -4);
                $gender = str_split($candidate['security_user']['gender']['name']);
                $candidate['security_user']['gender']['name'] = $gender[0]??"";
                $candidate['candidate_id'] = $candidateId??"";
                $list[] = $candidate;
            }
            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch candidate list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch candidate list");
        }
    }
}
