<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\ExaminationComponentsMultiplechoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MultipleChoiceRepository extends Controller
{
    /**
     * Updating component multiple choice question's answer
     * @param Request $request
     * @param string $componentId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function updateComponentQuestionAnswer(Request $request, string $componentId)
    {
        try {
            $componentId = ExaminationComponentsMultiplechoice::where(
                'examination_components_id',
                $componentId
            )->delete();
            if ($request->response && count($request->response)) {
                $answerList = [];
                foreach ($request->response as $res) {
                    $record_data = [];
                    $record_data['answer'] = $res['answer'];
                    $record_data['question'] = $res['question'];
                    $record_data['examination_components_id'] = $res['examination_components_id'];
                    $record_data['created_user_id'] = 1;
                    $record_data['created'] = Carbon::now()->toDateTimeString();
                    $answerList[] = $record_data;
                    $data = ExaminationComponentsMultiplechoice::insert($record_data);
                }
            }

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Examination Multiple Choice Answer Not Updated');
        }
    }

}