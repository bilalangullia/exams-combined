<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\SecurityGroupUser;
use App\Models\SecurityRole;
use App\Models\SecurityUser;

class UserPermissionRepository extends Controller
{
    /**
     * Getting User Permission List
     * @param int $userId
     * @return JsonResponse
     */
    public function userPermission(int $userId)
    {
        try {
            $userData = SecurityUser::where('id', $userId)->first();
            $is_super_admin = $userData->super_admin??0;

            if ($is_super_admin == 1) {
                return [];
            } else {
                $userRoles = SecurityGroupUser::where('security_user_id', $userId)->groupBy('security_role_id')->pluck('security_role_id')->toArray();
            
                $permissionList = SecurityRole::select('id')
                                    ->with(
                                        [
                                            'securityFunction' => function ($q) {
                                                $q->select(
                                                    'name',
                                                    'controller',
                                                    'module',
                                                    'category',
                                                    'parent_id',
                                                    'security_function_id'
                                                )
                                                ->withPivot(
                                                    '_view',
                                                    '_edit', 
                                                    '_add', 
                                                    '_delete', 
                                                    '_execute'
                                                )
                                                ->where('visible', config('constants.visibleValue.visibleCode'));
                                            }
                                        ]
                                    )
                                    ->whereIn('id', $userRoles)
                                    ->get();
                
                $data = [];
                if (count($permissionList) > 0) {
                    foreach ($permissionList as $key => $permission) {
                        foreach ($permission['securityFunction'] as $key => $value) {
                            $data[$value['module']][$value['category']][] = [
                                $value['name'] => $value['pivot']['_add'] . $value['pivot']['_view'] . $value['pivot']['_edit'] . $value['pivot']['_delete'] . $value['pivot']['_execute'],
                                
                            ];                
                        }
                    }
                }
                if (count($data) > 0) {
                    return $data;
                } else {
                    return Null;
                }
            }
        } catch (\Exception $e) {
            log::error(
                'Failed to get logged in user permission list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get logged in user permission list");
        }
    }
}