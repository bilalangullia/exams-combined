<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\ExaminationComponent;
use App\Models\ExaminationGradeReviewCriteria;
use App\Models\ExaminationGradeReviewStudent;
use App\Models\ExaminationGradingOption;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsComponentsGrade;
use App\Models\ExaminationStudentsComponentsMark;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class GradeReviewRepository extends Controller
{
    /**
     * add forecast, component, mark Grade review
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function addGradeReview()
    {
        try {
            // add marks grade
            $marks = ExaminationStudentsComponentsMark::select(
                'id',
                'examination_students_id',
                'mark',
                'examination_components_id'
            )->with(
                'examinationComponent:id,examination_options_id'
            )->get();
            $markss = json_decode($marks, true);
            foreach ($markss as $key => $value) {
                $markStudentId =  $value['examination_students_id'];
                $studentMark =  $value['mark'];
                $marksoptions =  $value['examination_component']['examination_options_id'];
                $compOptionId = ExaminationComponent::where('examination_options_id', $marksoptions)
                    ->select('id')->get();
                $comIds = $compOptionId->toArray();
                $compGrade = ExaminationGradingOption::whereIn('id', $comIds)->select('max')->get();
                $gradeOption = $compGrade->toArray();
                $maxval = max($gradeOption);
                $marksresult =  (($studentMark / ($maxval['max'])) * 100);
                $deviation = ExaminationGradeReviewCriteria::select('id', 'deviation')
                    ->where('examination_options_id', $marksoptions)->first();
                $findeviation = $deviation['deviation'];
                $criteriaId = $deviation['id'];
                if (!empty($findeviation) && $marksresult > $findeviation) {
                    $componentgradeAdd = ExaminationGradeReviewStudent::updateOrCreate(
                        [
                            'examination_students_id' => $markStudentId,
                            'examination_grade_review_criterias_id' => $criteriaId,
                            'examination_options_id' => $marksoptions,
                        ],
                        [
                            'modified_user_id' => JWTAuth::user()->id,
                            'modified' => Carbon::now()->toDateTimeString(),
                            'created_user_id' => JWTAuth::user()->id,
                            'created' => Carbon::now()->toDateTimeString(),
                        ]
                    );
                }
            }
            //add component Grade
            $datacomponent = ExaminationStudentsComponentsGrade::select('grade', 'examination_students_id', 'examination_components_id')->get();
            $com = json_decode($datacomponent, true);
            foreach ($com as $key => $value) {
                $comStudentId =  $value['examination_students_id'];
                $compvalue =  $value['examination_components_id'];
                $grade =  $value['grade'];
                $componentId = ExaminationComponent::select('examination_options_id')->where('id', $compvalue)->first();
                $getOptionId = $componentId['examination_options_id'];
                $optionVal[] = $getOptionId;
                $optionId = ExaminationComponent::whereIn('examination_options_id', $optionVal)->select('id')->get();
                $comIds = $optionId->toArray();
                $compGrade = ExaminationGradingOption::whereIn('id', $comIds)->select('code')->get();
                $gradeOption = $compGrade->toArray();
                $deviation = ExaminationGradeReviewCriteria::select('id', 'deviation')
                        ->where('examination_options_id', $getOptionId)->first();
                    $findeviation = $deviation['deviation'];
                    $criteriaId = $deviation['id'];
                if (!empty($findeviation)) {
                    $componentgradeAdd = ExaminationGradeReviewStudent::updateOrCreate(
                        [
                            'examination_students_id' => $comStudentId,
                            'examination_grade_review_criterias_id' => $criteriaId,
                            'examination_options_id' => $getOptionId,
                        ],
                        [
                            'modified_user_id' => JWTAuth::user()->id,
                            'modified' => Carbon::now()->toDateTimeString(),
                            'created_user_id' => JWTAuth::user()->id,
                            'created' => Carbon::now()->toDateTimeString(),
                        ]
                    );
                }
            }
            // add forecast Grade
            $dataforecast = DB::table('examination_students_forecast_grades');
            $gradedata = $dataforecast->join(
                'examination_students_options_grades',
                function ($join) {
                    $join->on(
                        'examination_students_options_grades.examination_students_id',
                        '=',
                        'examination_students_options_grades.examination_students_id'
                    );
                    $join->on(
                        'examination_students_forecast_grades.examination_options_id',
                        '=',
                        'examination_students_options_grades.examination_options_id'
                    );
                }
            )->select(
                'examination_students_forecast_grades.grade as gradef',
                'examination_students_options_grades.*'
            )->get();
            $forecast = json_decode($gradedata, true);
            foreach ($forecast as $key => $value) {
                if ($value['gradef'] > $value['grade']) {
                    $gradediff = $value['gradef'] - $value['grade'];
                    $studentId = $value['examination_students_id'];
                    $optionId = $value['examination_options_id'];
                    $deviation = ExaminationGradeReviewCriteria::select('id', 'deviation')
                        ->where('examination_options_id', $optionId)->first();
                    $findeviation = $deviation['deviation'];
                    $criteriaId = $deviation['id'];
                    if (!empty($findeviation) && $gradediff > $findeviation) {
                        $forecastgradeAdd = ExaminationGradeReviewStudent::updateOrCreate(
                            [
                                'examination_students_id' => $studentId,
                                'examination_grade_review_criterias_id' => $criteriaId,
                                'examination_options_id' => $optionId,
                            ],
                            [
                                'modified_user_id' => JWTAuth::user()->id,
                                'modified' => Carbon::now()->toDateTimeString(),
                                'created_user_id' => JWTAuth::user()->id,
                                'created' => Carbon::now()->toDateTimeString(),
                            ]
                        );
                    }
                }
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to add review into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' review Not added');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function gradeReviewList(Request $request)
    {
        try {
            $data = ExaminationGradeReviewStudent::where('examination_options_id', $request->examination_options_id)->with(
                'examinationStudent:id,candidate_id,student_id',
                'examinationStudent.securityUser:id,first_name,middle_name,third_name,last_name',
                'examinationGradeReviewCriteria.gradeReviewCriteriaType:id,name'
            )->whereHas(
                'examinationStudent.examinationCentre',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_centre_id);
                }
            )->whereHas(
                'examinationStudent.examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            )->whereHas(
                'examinationStudent.examName.academicPeriod',
                function ($query) use ($request) {
                    $query->where('id', $request->academic_period_id);
                }
            );
            if (isset($request['keyword'])) {
                $candidateSearch = $data->whereHas(
                    'examinationStudent',
                    function ($query) use ($request) {
                        $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                    }
                )->orWhereHas(
                    'examinationStudent.securityUser',
                    function ($query) use ($request) {
                        $query->orwhere('first_name', 'LIKE', "%" . $request['keyword'] . "%");
                        $query->orwhere('middle_name', 'LIKE', "%" . $request['keyword'] . "%");
                        $query->orwhere('third_name', 'LIKE', "%" . $request['keyword'] . "%");
                        $query->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                    }
                )->orWhereHas(
                    'examinationGradeReviewCriteria.gradeReviewCriteriaType',
                    function ($query) use ($request) {
                        $query->orwhere('name', 'LIKE', "%" . $request['keyword'] . "%");
                    }
                );
            }
            $candidateSearch = $data->get();
            return $candidateSearch;
        } catch (\Exception $e) {
            Log::error($e);
            return $this->sendErrorResponse('Review List Not Found');
        }
    }

    /**
     * @param string $gradeReviewId
     * @return \ExceptiIlluminate\Database\Eloquent\Collection|\Illuminate\Http\JsonResponse
     */
    public function viewGradeReview(string $candidateId)
    {
        try {
            $cid = ExaminationStudent::where('candidate_id', $candidateId)->first()->id;
            $view = ExaminationGradeReviewStudent::where('examination_students_id', $cid)->with(
                'examinationStudent:id,candidate_id,student_id',
                'examinationStudent.securityUser:id,first_name,middle_name,third_name,last_name',
                'examinationGradeReviewCriteria.gradeReviewCriteriaType:id,name'
            )->get()->map(
                function ($item, $key) {
                    return [
                        "created_by" => $item['createdByUser']['full_name'],
                        "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            return $view;
        } catch (\Exception $e) {
            Log::error($e);
            return $this->sendErrorResponse('Review view Not Found');
        }
    }

    /**
     * @param $str
     * @param $n
     * @return int
     */
    protected function getGradePosition($str, $n)
    {
        $a = config('constants.alphabet.position');
        for ($i = 0; $i < $n; $i++) {
            $value = (ord($str[$i]) & ($a));
        }
        return $value;
    }
}
