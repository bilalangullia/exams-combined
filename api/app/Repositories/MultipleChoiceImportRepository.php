<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\ExaminationComponentsMultiplechoice;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsMultiplechoiceResponse;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationCentre;
use Tymon\JWTAuth\Facades\JWTAuth;

class MultipleChoiceImportRepository extends Controller
{
    /**
     * Importing Multiple Choice Excel File.
     * @param array $results
     * @param $requestParameter
     * @return array|JsonResponse
     */
    public function importMulitpleChoice(array $results, $requestParameter)
    {
        try {
            $i = -1;
            $validation = [];
            $stored_data = [];
            $add_data = [];
            $importResponse = [];

            $examCentreDetails = ExaminationCentre::select(
                'id',
                'code',
                'examination_id'
            )
            ->with(
                [
                    'exams' => function ($query) {
                        $query->select('id', 'code', 'academic_period_id');
                    },
                    'exams.academicPeriod' => function ($query) {
                        $query->select('id', 'start_year')
                                ->where('visible', config('constants.visibleValue.visibleCode'));
                    }
                ]
            )
            ->where('id', $requestParameter['examination_centre_id'])
            ->first();


            foreach ($results[0] as $key => $row) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }

                if (!$row[0]) {//Candidate Id
                    $label = $results[0][1][0];
                    $errors[$label] = 'Candidate id required';
                }

                if (!$row[1]) {//Question Number
                    $label = $results[0][1][1];
                    $errors[$label] = 'Question Number required ';
                } elseif (!is_numeric($row[1])) {//Question Number Numeric Value
                    $label = $results[0][1][1];
                    $errors[$label] = 'Question Number should be number ';
                }

                if (!$row[2]) {//Response
                    $label = $results[0][1][2];
                    $errors[$label] = 'Response required';
                } elseif (!is_numeric($row[2])) {//Response Numeric Value
                    $label = $results[0][1][2];
                    $errors[$label] = 'Response should be number only';
                } elseif (!in_array($row[2], range(1, 4))) {//Response Value Range
                    $label = $results[0][1][2];
                    $errors[$label] = 'Response should be in the range of 1 and 4 only';
                }

                if (isset($row[2])) {
                    $resp = [
                        '1' => 'A',
                        '2' => 'B',
                        '3' => 'C',
                        '4' => 'D'
                    ];
                    $response = $resp[$row[2]];
                } else {
                    $response = null;
                }

                if (count($errors) > 0) {
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            $results[0][1][0] => $row[0],
                            $results[0][1][1] => $row[1],
                            $results[0][1][2] => $response,
                            'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                            'Exam Code' => $examCentreDetails['exams']['code'],
                            'Centre Code' => $examCentreDetails['code'],
                        ],
                        'errors' => $errors
                    ];
                } else {
                    $student = ExaminationStudent::where('candidate_id', $row[0])->first();
                    if (isset($student)) {
                        $student_id = $student->id;
                        $component_id = $requestParameter['component_id'];
                        $question = ExaminationComponentsMultiplechoice::where('examination_components_id', $component_id)
                            ->where('question', $row[1])
                            ->first();
                        if (isset($question)) {
                            $is_exists = ExaminationStudentsMultiplechoiceResponse::where('examination_students_id', $student_id)
                                ->where('examination_components_id', $component_id)
                                ->where('question', $row[1])
                                ->first();
                            if (isset($is_exists)) {
                                $updateData['response'] = $response;
                                $updateData['modified_user_id'] = JWTAuth::user()->id;
                                $updateData['modified'] = Carbon::now()->toDateTimeString();
                                $update = ExaminationStudentsMultiplechoiceResponse::where('id', $is_exists->id)
                                    ->update($updateData);
                                $stored_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        $results[0][1][0] => $row[0],
                                        $results[0][1][1] => $row[1],
                                        $results[0][1][2] => $response,
                                        'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                        'Exam Code' => $examCentreDetails['exams']['code'],
                                        'Centre Code' => $examCentreDetails['code'],
                                    ],
                                    'errors' => $errors
                                ];
                            } else {
                                $insertData['examination_students_id'] = $student_id;
                                $insertData['examination_components_id'] = $component_id;
                                $insertData['question'] = $row[1];
                                $insertData['response'] = $response;
                                $insertData['created_user_id'] = JWTAuth::user()->id;
                                $insertData['created'] = Carbon::now()->toDateTimeString();
                                $store = ExaminationStudentsMultiplechoiceResponse::insert($insertData);
                                $add_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        $results[0][1][0] => $row[0],
                                        $results[0][1][1] => $row[1],
                                        $results[0][1][2] => $response,
                                        'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                        'Exam Code' => $examCentreDetails['exams']['code'],
                                        'Centre Code' => $examCentreDetails['code'],
                                    ],
                                    'errors' => $errors
                                ];
                            }
                        } else {
                            $label = $results[0][1][1];
                            $errors[$label] = 'Question does not exist for selected component.';
                            $validation[] = [
                                'row_number' => $i,
                                'data' => [
                                    $results[0][1][0] => $row[0],
                                    $results[0][1][1] => $row[1],
                                    $results[0][1][2] => $response,
                                    'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                    'Exam Code' => $examCentreDetails['exams']['code'],
                                    'Centre Code' => $examCentreDetails['code'],
                                ],
                                'errors' => $errors
                            ];
                        }
                    } else {
                        $label = $results[0][1][0];
                        $errors[$label] = 'Candidate ID does not exist';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                $results[0][1][0] => $row[0],
                                $results[0][1][1] => $row[1],
                                $results[0][1][2] => $response,
                                'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                'Exam Code' => $examCentreDetails['exams']['code'],
                                'Centre Code' => $examCentreDetails['code'],
                            ],
                            'errors' => $errors
                        ];
                    }
                }
            }
            $importResponse = [
                'total_count' => count($results[0]) - 2,
                'records_added' => [
                    'count' => count($add_data),
                    'rows' => $add_data,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];

            return $importResponse;
        } catch (\Exception $e) {
            Log::error(
                'Failed to import multiple choice response in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import multiple choice response in DB");
        }
    }
}
