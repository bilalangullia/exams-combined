<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\SecurityGroup;
use App\Models\SecurityUser;
use App\Models\SecurityRole;
use App\Models\SecurityRoleFunction;
use App\Models\SecurityGroupInstitution;
use App\Models\SecurityGroupArea;
use App\Models\SecurityGroupUser;
use App\Models\Area;
use App\Models\ExaminationCentre;
use App\Http\Requests\SecurityUserGroupAddRequest;
use DB;
use Carbon\Carbon;
use JWTAuth;

class SecurityGroupRepository extends Controller
{
    //getting user group list
    public function securityUserGroupList(Request $request)
    {
        try{
            $user_group_list = SecurityGroup::select('id','name')
                ->with([
                           'securityGroupUser' =>
                               function ($query) {
                                   $query->select('id','security_group_id');
                               }
                       ]
                )->doesntHave('examinationCentre');

            if (isset($request['start']) && isset($request['end'])) {
                $groupCount = $user_group_list;
                $total = $groupCount->count();
                $user_group_list->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $groupCount = $user_group_list;
                $total = $groupCount->count();
            }

            if (isset($request['keyword'])) {
                $filter = $user_group_list->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("name", "LIKE", "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }

            $filter = $user_group_list->get();

            return array("record" => $filter, "totalRecord" => $total);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group List Not Found");
        }
    }

    //autocomplete security user
    public function  autocompleteSecurityUser(string $openemisId)
    {
        try{
            $userRecord = SecurityUser::select('id','openemis_no','first_name','middle_name','third_name','last_name')->where("openemis_no", "LIKE", "%" . $openemisId . "%")
                ->with('securityGroupUser',
                    'securityGroupUser.securityRole')->get();

            return $userRecord;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Details Not Found");
        }
    }

    //autocomplete area
    public function  autocompleteArea(string $areaName)
    {
        try{
            $areaRecord = Area::select('id','code','name','parent_id', 'area_level_id')->where("name", "LIKE", "%" . $areaName . "%")->with('region','areaLevel:id,name')->orwhereHas(
                            'areaLevel',
                            function ($query) use ($areaName) {
                                $query->where('name', 'LIKE', "%" . $areaName . "%");
                            }
                        )->get();

            return $areaRecord;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Area Details Not Found");
        }
    }

    //autocomplete Institutions
    public function  autocompleteInstitutions(string $institutionsName)
    {
        try{
            $institutionsRecord = ExaminationCentre::select('id','code','name')->where("name", "LIKE", "%" . $institutionsName . "%")->orwhere("code", "LIKE", "%" . $institutionsName . "%")->get();

            return $institutionsRecord;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Institution Details Not Found");
        }
    }

    public function addUserGroup(SecurityUserGroupAddRequest $request)
    {
        DB::beginTransaction();
        try{
            $securityGrp = new SecurityGroup();
            $securityGrp['name'] = $request->name;
            $securityGrp['created_user_id'] = JWTAuth::user()->id;
            $securityGrp['created'] = Carbon::now()->toDateTimeString();
            $strSecurityGrp = $securityGrp->save();
            if($strSecurityGrp && $request->area && count($request->area)){
             $area = [];
                foreach ($request->area as $areaData) {
                    $strGrpArea = [];
                    $strGrpArea['security_group_id'] = $securityGrp->id;
                    $strGrpArea['area_id'] = $areaData['area_id'];
                    $strGrpArea['created_user_id'] = JWTAuth::user()->id;
                    $strGrpArea['created'] = Carbon::now()->toDateTimeString();
                    $area[] = $strGrpArea;
                }
                SecurityGroupArea::insert($area);
            }

            if($strSecurityGrp && $request->institution && count($request->institution)){
                $institution = [];
                foreach ($request->institution as $institutionData) {
                    $strGrpInst = [];
                    $strGrpInst['security_group_id'] = $securityGrp->id;
                    $strGrpInst['institution_id'] = $institutionData['institution_id'];
                    $strGrpInst['created_user_id'] = JWTAuth::user()->id;
                    $strGrpInst['created'] = Carbon::now()->toDateTimeString();
                    $institution[] = $strGrpInst;
                }
                
                 SecurityGroupInstitution::insert($institution);
            } 
            if($strSecurityGrp && $request->user && count($request->user)){
                $role_id = SecurityGroupUser::where('security_group_id', $securityGrp->id)->where('security_user_id', $request->user_id)->first();
                $user = [];
                foreach ($request->user as $userData) {
                    $strGrpUser = [];
                    $strGrpUser['id'] = uniqid();
                    $strGrpUser['security_group_id'] = $securityGrp->id;
                    $strGrpUser['security_user_id'] = $userData['user_id'];
                    if(isset($userData['role_id'])){
                         $strGrpUser['security_role_id'] = $userData['role_id'];
                    }else{
                       $strGrpUser['security_role_id'] = isset($role_id) ? $role_id->security_role_id : 0; 
                    }
                    //$strGrpUser['security_role_id'] = ($role_id) ? $role_id : 0;
                    $strGrpUser['created_user_id'] = JWTAuth::user()->id;
                    $strGrpUser['created'] = Carbon::now()->toDateTimeString();
                    $user[] = $strGrpUser;
                }
                    SecurityGroupUser::insert($user);
            } 
           
                DB::commit();

                return $strSecurityGrp;
        } catch (\Exception $e) {
            echo $e;
            
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("User Group Not Updated Successfully");
        }
    }

    //role dropdown 
    public function getRoleDropdown()
    {
        try{

            $roles = SecurityRole::select('id', 'name')->get();

            return $roles;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Role Dropdown Not Found");
        }
    }

    //getting user group details
    public function getSecurityUserGroupDetails(string $groupId)
    {
        try{

            $user_group_details = SecurityGroup::select('id','name','modified_user_id','modified','created_user_id','created')->
            with('securityGroupUser:security_group_id,security_user_id,security_role_id',
                 'securityGroupUser.securityUser:id,openemis_no,first_name,middle_name,third_name,last_name',
                 'securityGroupUser.securityRole:id,name',
                 'securityGroupArea:security_group_id,area_id',
                 'securityGroupArea.area:id,code,name,parent_id,area_level_id',
                 'securityGroupArea.area.areaLevel:id,name',
                 'securityGroupInstitution:security_group_id,institution_id',
                 'securityGroupInstitution.examinationCentre:id,code,name'
            )->where('id', $groupId)->get();

            return  $user_group_details;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group Details Not Found");
        }
    }

    //getting system group list
    public function getSecuritySystemGroupList(Request $request)
    {
        try{
             $system_group_list = SecurityGroup::select('id','name')
                                ->whereHas('examinationCentre.exams', function ($query) use ($request){
                                    $query->where('id', $request->examination_id);
                                })->whereHas('examinationCentre.exams.academicPeriod', function ($query)  use ($request) {
                                    $query->where('id', $request->academic_period_id);
                                })->with([
                                       'securityGroupUser' =>
                                           function ($query) {
                                               $query->select('id','security_group_id');
                                           }
                                   ])->has('examinationCentre')->doesntHave('securityGroupInstitution');

            if (isset($request['start']) && isset($request['end'])) {
                $groupCount = $system_group_list;
                $total = $groupCount->count();
                $system_group_list->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $groupCount = $system_group_list;
                $total = $groupCount->count();
            }

            if (isset($request['keyword'])) {
                $search = $system_group_list->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("name", "LIKE", "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $search = $system_group_list->get();
            return array("record" => $search, "totalRecord" => $total);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("System Group List Not Found");
        }
    }

    //getting system group details
    public function getSystemGroupDetails(string $sysGroupId)
    {
        try{
            $systemGroup_group_details = SecurityGroup::select('id','name','modified_user_id','modified','created_user_id','created')->
            with('securityGroupUser:security_group_id,security_user_id,security_role_id',
                 'securityGroupUser.securityUser:id,openemis_no,first_name,middle_name,third_name,last_name',
                 'securityGroupUser.securityRole:id,name'
            )->where('id', $sysGroupId)->get();

            return  $systemGroup_group_details;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("System Group Details Not Found");
        }
    }


    public function checkIfSafeTodelete(int $userGroupId)
    {
        try{
            $securityUserGrp = SecurityGroupUser::where('security_group_id', $userGroupId)->exists();
            if ($securityUserGrp) {
                return false;
            }

            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group Can Not Delete");
        }
    }


    public function deleteUserGroup(int $userGroupId)
    {
        try{
            $securityGrp = SecurityGroup::find($userGroupId);
            if ($securityGrp) {
                $dltSpclNeed = $securityGrp->securityGroupInstitution()->delete();
                $dltgrpArea = $securityGrp->securityGroupArea()->delete();
                $get = $securityGrp->securityRole()->pluck('id');
                $dltRoleFntc = SecurityRoleFunction::whereIn('security_role_id', $get)->delete();
                $dltrole = $securityGrp->securityRole()->delete();
                return $securityGrp->delete();
            } 
            
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group Can Not Delete");
        }
    }

    public function cannotDeleteView(int $userGroupId)
    {
        try{
            $result = [];
            $counter = 0;
            $data = SecurityGroup::with('securityGroupUser','securityGroupUser.securityUser')->where('id', $userGroupId)->get();

            $noOfUser = SecurityGroupUser::where('security_group_id', $userGroupId)->get();
            $count = $noOfUser->count();

            foreach ($data as $key => $value) {
                if($value['securityGroupUser'][$key]['securityUser']){
                    $feature = "Users";
                }
                   $result[$counter]['error']['to_be_deleted']  = $value->name;
                   $result[$counter]['error']['associated_records'][$key]['feature'] = $feature;
                   $result[$counter]['error']['associated_records'][$key]['no_of_record'] = $count;
                   $counter++;
                }

                return $result;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group Can Not Delete");
        }
    }

    //update user group 
    public function updateUserGroup(SecurityUserGroupAddRequest $request, int $userGroupId)
    {
        DB::beginTransaction();
        try{
            $securityGrp = SecurityGroup::find($userGroupId);
            $securityGrp['name'] = $request->name;
            $securityGrp['modified_user_id'] =  JWTAuth::user()->id;
            $securityGrp['modified'] = Carbon::now()->toDateTimeString();
            $strSecurityGrp = $securityGrp->save();

            if($strSecurityGrp && $request->area && count($request->area)) {
                SecurityGroupArea::where('security_group_id', $userGroupId)->delete();
                $area = [];
                foreach ($request->area as $areaData) {
                    $strGrpArea = [];
                    $strGrpArea['security_group_id'] = $securityGrp->id;
                    $strGrpArea['area_id'] = $areaData['area_id'];
                    $strGrpArea['created_user_id'] = JWTAuth::user()->id;
                    $strGrpArea['created'] = Carbon::now()->toDateTimeString();
                    $area[] = $strGrpArea;
                }
                SecurityGroupArea::insert($area);
            } else {
                SecurityGroupArea::where('security_group_id', $userGroupId)->delete();
            }

            if($strSecurityGrp && $request->institution && count($request->institution)) {
                SecurityGroupInstitution::where('security_group_id', $userGroupId)->delete();
                $institution = [];
                foreach ($request->institution as $institutionData) {
                    $strGrpInst = [];
                    $strGrpInst['security_group_id'] = $securityGrp->id;
                    $strGrpInst['institution_id'] = $institutionData['institution_id'];
                    $strGrpInst['created_user_id'] = JWTAuth::user()->id;
                    $strGrpInst['created'] = Carbon::now()->toDateTimeString();
                    $institution[] = $strGrpInst;
                }
                 SecurityGroupInstitution::insert($institution);
            }  else {
                SecurityGroupInstitution::where('security_group_id', $userGroupId)->delete();
            }

            if($strSecurityGrp && $request->user && count($request->user)){
                SecurityGroupUser::where('security_group_id', $userGroupId)->delete();
                $role_id = SecurityGroupUser::where('security_group_id', $securityGrp->id)->where('security_user_id', $request->user_id)->first();
                $user = [];
                foreach ($request->user as $userData) {
                    $strGrpUser = [];
                    $strGrpUser['id'] = uniqid();
                    $strGrpUser['security_group_id'] = $securityGrp->id;
                    $strGrpUser['security_user_id'] = $userData['user_id'];
                    if(isset($userData['role_id'])){
                         $strGrpUser['security_role_id'] = $userData['role_id'];
                    }else{
                       $strGrpUser['security_role_id'] = isset($role_id) ? $role_id->security_role_id : 0; 
                    }
                    $strGrpUser['created_user_id'] = JWTAuth::user()->id;
                    $strGrpUser['created'] = Carbon::now()->toDateTimeString();
                    $user[] = $strGrpUser;
                }
                 SecurityGroupUser::insert($user);
            }  else{
                SecurityGroupUser::where('security_group_id', $userGroupId)->delete();
            }
            
        DB::commit();

        return $strSecurityGrp;

        } catch (\Exception $e) {
            echo $e;
            DB::rollback();
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group Details Not Updated");
        }
    }

 }

