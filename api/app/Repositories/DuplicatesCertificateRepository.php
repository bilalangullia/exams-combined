<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\DuplicateCertficatImportRequest;
use App\Http\Requests\DuplicatesCertificateAddRequest;
use App\Http\Requests\DuplicatesCertificateUpdateRequest;
use App\Imports\DuplicateCertificateImport;
use App\Models\Examination;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsCertificate;
use App\Models\ExaminationStudentsCertificatesStatuse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class DuplicatesCertificateRepository extends Controller
{
    /**
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function duplicatesList(Request $request)
    {
        try {
            $data = $request->all();
            $list = ExaminationStudentsCertificatesStatuse::select(
                'id',
                'method',
                'date_received',
                'date_sent',
                'examination_students_certificates_id'
            )
                ->where('type', config('constants.certificatetype.duplicate'))
                ->with(
                    'examinationStudentsCertificate:id,certificate_id,examination_students_id',
                    'examinationStudentsCertificate.examinationStudent:id,candidate_id,student_id,examination_id',
                    'examinationStudentsCertificate.examinationStudent.examName:id,name',
                    'examinationStudentsCertificate.examinationStudent.securityUser:id,first_name,last_name,middle_name,third_name'
                )->whereHas(
                    'examinationStudentsCertificate.examinationStudent.examName',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_id);
                    }
                )->whereHas(
                    'examinationStudentsCertificate.examinationStudent.examName.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('id', $request->academic_period_id);
                    }
                );

            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $list;
                $total = $userCount->count();
                $list->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $list;
                $total = $userCount->count();
            }

            if (isset($request['keyword'])) {
                $subjectSearch = $list->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where('method', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('date_received', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('date_sent', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationStudentsCertificate.examinationStudent.examName',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationStudentsCertificate.examinationStudent',
                            function ($query) use ($request) {
                                $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationStudentsCertificate.examinationStudent.securityUser',
                            function ($query) use ($request) {
                                $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('middle_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('third_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $filter = $list->get();
            return array("totalRecord" => $total, "record" => $filter);
        } catch (\Exception $e) {
            Log::error(
                'Failed to add review into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Duplicate Certificate List Not Found');
        }
    }

    /**
     * @param string $certificateId
     * @return mixed
     */
    public function certificateView(string $certificateId)
    {
        try {
            $viewdata = ExaminationStudentsCertificatesStatuse::where('id', $certificateId)
                ->where('type', config('constants.certificatetype.duplicate'))
                ->with(
                    'examinationStudentsCertificate:id,certificate_id,examination_students_id',
                    'examinationStudentsCertificate.examinationStudent:id,candidate_id,student_id,examination_id',
                    'examinationStudentsCertificate.examinationStudent.examName:id,name',
                    'examinationStudentsCertificate.examinationStudent.securityUser:id,first_name,last_name,middle_name,third_name,date_of_birth,gender_id',
                    'examinationStudentsCertificate.examinationStudent.securityUser.gender:id,name'
                )->get();
            return $viewdata;
        } catch (\Exception $e) {
            Log::error(
                'Failed to view',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Duplicate Certificate view Not Found');
        }
    }

    /**
     * @param DuplicatesCertificateAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function duplicatesCertificateAdd(DuplicatesCertificateAddRequest $request)
    {
        try {
            $candId = ExaminationStudent::where('candidate_id', $request['candidate_id'])->first()->id;
            $certificate = ExaminationStudentsCertificate::where('examination_students_id', $candId)->first()->id;
            $duplicateadd = new ExaminationStudentsCertificatesStatuse();
            $exist_certif = ExaminationStudentsCertificatesStatuse::where(
                'examination_students_certificates_id',
                $request['certificate_number']
            )
                ->where('type', config('constants.certificatetype.duplicate'))->first();
            if ($exist_certif) {
                return 1;
            } else {
                $duplicateadd['examination_students_certificates_id'] = $certificate;
                $duplicateadd['type'] = config('constants.certificatetype.duplicate');
                $duplicateadd['method'] = $request['method'];
                $duplicateadd['date_received'] = $request->date_received;
                $duplicateadd['date_sent'] = isset($request['date_sent']) ? $request['date_sent'] : null;
                $duplicateadd['postal'] = isset($request['postal']) ? $request['postal'] : null;
                $duplicateadd['comments'] = isset($request['comments']) ? $request['comments'] : null;
                $duplicateadd['created_user_id'] = JWTAuth::user()->id;
                $duplicateadd['created'] = Carbon::now()->toDateTimeString();
                $record = $duplicateadd->save();
                return 2;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to add Certificate into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Duplicate  Certificate Not Added');
        }
    }

    /**
     * @param DuplicatesCertificateAddRequest $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function duplicatesCertificateUpdate(DuplicatesCertificateUpdateRequest $request, string $duplicateId)
    {
        try {
            $candId = ExaminationStudent::where('candidate_id', $request['candidate_id'])->first()->id;
            $cert_id = ExaminationStudentsCertificate::where('examination_students_id', $candId)->first()->id;
            $duplicate = ExaminationStudentsCertificatesStatuse::find($duplicateId);
            $duplicate['examination_students_certificates_id'] = $cert_id;
            $duplicate['type'] = $duplicate->type;
            $duplicate['method'] = $request['method'];
            $duplicate['date_received'] = $request->date_received;
            $duplicate['date_sent'] = isset($request['date_sent']) ? $request['date_sent'] : null;
            $duplicate['postal'] = isset($request['postal']) ? $request['postal'] : null;
            $duplicate['comments'] = isset($request['comments']) ? $request['comments'] : null;
            $duplicate['modified'] = Carbon::now()->toDateTimeString();
            $duplicate['modified_user_id'] = JWTAuth::user()->id;
            $record = $duplicate->save();
            return $record;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update  into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Duplicate  Certificate Not Updated');
        }
    }

    /**
     * @param DuplicateCertficatImportRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */

    public function duplicateImport(DuplicateCertficatImportRequest $request)
    {
        try {
            $excelData = Excel::toArray(new DuplicateCertificateImport(), $request->file('file'));
            $i = -1;
            $validation = [];
            $add_record = [];
            $stored_data = [];
            $insert = [];
            $inserta = [];
            $insertb = [];
            $examId = $request['examination_id'];
            $examDetails = Examination::where('id', $request['examination_id'])->with('academicPeriod')->first();

            foreach ($excelData[0] as $data) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }

                if (!$data[0]) { //Candidate Id
                    $label = $excelData[0][1][0];
                    $errors[$label] = 'Candidate Id Required';
                }

                if (!$data[1]) { //Date Received
                    $label = $excelData[0][1][1];
                    $errors[$label] = 'Date Received Required';
                }
                if (!$data[2]) { //Method
                    $label = $excelData[0][1][2];
                    $errors[$label] = 'Method Required';
                }
                if (count($errors)) {
                    //row fail
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            "Academic Period" => $examDetails["academicPeriod"]["name"],
                            "Examination" => $examDetails['full_name'],
                            "Candidate Id" => $data[0],
                            "Date Received" => Carbon::parse($data[1])->format('Y-m-d'),
                            "Method" => $data[2],
                        ],
                        'errors' => $errors,
                    ];
                } else {
                    if ($data[0]) {
                        $candidateId = ExaminationStudent::where('candidate_id', $data[0])->with(
                            'securityUser',
                            'examinationStudentsCertificate'
                        )->first();
                        $candId = $candidateId->id;
                        $certificate = ExaminationStudentsCertificate::where('examination_students_id', $candId)->first(
                        )->id;

                        $t = ExaminationStudentsCertificatesStatuse::where(
                            'examination_students_certificates_id',
                            $certificate
                        )->first();
                        if ($t) {
                            $user = [];
                            $user['examination_students_certificates_id'] = $certificate;
                            $user['type'] = config('constants.certificatetype.duplicate');
                            $user['method'] = $data[2];
                            $user['date_received'] = Carbon::parse($data[1])->format('Y-m-d');
                            $user['date_sent'] = ($data[3]) ? $data[3] : null;
                            $user['postal'] = ($data[4]) ? $data[4] : null;
                            $user['comments'] = ($data[5]) ? $data[5] : null;
                            $user['modified_user_id'] = JWTAuth::user()->id;
                            $user['modified'] = Carbon::now()->toDateTimeString();
                            $secUserRec = ExaminationStudentsCertificatesStatuse::where(
                                ['examination_students_certificates_id' => $certificate]
                            )->update($user);
                            if ($data[2] == 1) {
                                $method = "Collect";
                            } else {
                                $method = "Post";
                            }
                            $stored_data[] = [
                                'row_number' => $i,
                                'data' => [
                                    "Academic Period" => $examDetails["academicPeriod"]["name"],
                                    "Examination" => $examDetails['full_name'],
                                    "Candidate Id" => $data[0],
                                    "Candidate Name" => $candidateId['securityUser']['full_name'],
                                    "Certificate Number" => $candidateId['examinationStudentsCertificate']['certificate_id'],
                                    "Date Received" => Carbon::parse($data[1])->format('Y-m-d'),
                                    "Method" => $method,
                                    "Date Sent" => $data[3],
                                ],
                            ];
                        } else {
                            $candidateId = ExaminationStudent::where('candidate_id', $data[0])->with(
                                'securityUser',
                                'examinationStudentsCertificate'
                            )->first();
                            $candId = $candidateId->id;
                            $certificate = ExaminationStudentsCertificate::where(
                                'examination_students_id',
                                $candId
                            )->first()->id;

                            $user = new ExaminationStudentsCertificatesStatuse();
                            $user['examination_students_certificates_id'] = $certificate;
                            $user['type'] = config('constants.certificatetype.duplicate');
                            $user['method'] = $data[2];
                            $user['date_received'] = Carbon::parse($data[1])->format('Y-m-d');
                            $user['date_sent'] = ($data[3]) ? $data[3] : null;
                            $user['postal'] = ($data[4]) ? $data[4] : null;
                            $user['comments'] = ($data[5]) ? $data[5] : null;
                            $user['created_user_id'] = JWTAuth::user()->id;
                            $user['created'] = Carbon::now()->toDateTimeString();
                            $record = $user->save();
                            if ($data[2] == 1) {
                                $method = "Collect";
                            } else {
                                $method = "Post";
                            }
                            $add_record[] = [
                                'row_number' => $i,
                                'data' => [
                                    "Academic Period" => $examDetails["academicPeriod"]["name"],
                                    "Examination" => $examDetails['full_name'],
                                    "Candidate Id" => $data[0],
                                    "Candidate Name" => $candidateId['securityUser']['full_name'],
                                    "Certificate Number" => $candidateId['examinationStudentsCertificate']['certificate_id'],
                                    "Date Received" => Carbon::parse($data[1])->format('Y-m-d'),
                                    "Method" => $method,
                                    "Date Sent" => $data[3],
                                ],
                            ];
                        }
                    } else {
                        $label = $excelData[0][1][0];
                        $errors[$label] = 'Candidate ID does not exist';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                "Academic Period" => $examDetails["academicPeriod"]["name"],
                                "Examination" => $examDetails['full_name'],
                                "Candidate Id" => $data[0],
                               // "Candidate Name" => $candidateId['securityUser']['full_name'],
                               // "Certificate Number" => $candidateId['examinationStudentsCertificate']['certificate_id'],
                                "Date Received" => $data[1],
                                "Method" => $data[2],
                                "Date Sent" => $data[3]
                            ],
                            'errors' => $errors,
                        ];
                    }
                }
            }
            $response = [
                'total_count' => count($excelData[0]) - 2,
                'records_added' => [
                    'count' => count($add_record),
                    'rows' => $add_record,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];

            return $response;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Duplicate Certificate Not Imported');
        }
    }
}
