<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExaminationMarkerRequest;
use App\Models\ExaminationCentre;
use App\Models\ExaminationMarker;
use App\Models\ExaminationMarkersSubject;
use App\Models\SecurityUser;
use App\Models\ExaminationMarkersApportionment;
use App\Models\ExaminationStudentsComponentsMark;
use Carbon\Carbon;
use Illuminate\Support\Str;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class MarkerRepository
 * @package App\Repositories
 */

class MarkerRepository extends Controller
{
    /**
     * @param Request $request
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function markerList(Request $request)
    {
        try {
            $markerlist = ExaminationMarker::with(
                [
                    'securityUser' => function ($query) {
                        $query->select('id', 'first_name', 'last_name', 'openemis_no', 'gender_id')
                            ->where('is_staff', config('constants.staffIds.staff'))
                            ->where('status', config('constants.staffIds.status'));
                    },
                    'securityUser.gender' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examinationMarkersSubject.educationSubject' => function ($query) {
                    },
                ]
            )->whereHas(
                'examName.academicPeriod',
                function ($query) use ($request) {
                    $query->where('id', $request->academic_period_id);
                }
            )->whereHas(
                'examinationCentre',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_centre_id);
                }
            )->whereHas(
                'examinationMarkersSubject.educationSubject',
                function ($query) use ($request) {
                    $query->where('id', $request->education_subjects_id);
                }
            )->whereHas(
                'examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            );
            if (isset($request['keyword'])) {
                $markerSearch = $markerlist->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("marker_id", "LIKE", "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'securityUser',
                            function ($query) use ($request) {
                                $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('openemis_no', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'securityUser.gender',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $markerSearch = $markerlist->get();
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['MarkerList' => $markerSearch]]);
            return $markerSearch;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch marker list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Markers list Not Found');
        }
    }

    /**
     * @param Request $request
     * @param string $openemisno
     * @return mixed
     */
    public function markerview(string $openemisno)
    {
        try {
            $markerview = SecurityUser::with(
                'gender:id,name',
                'nationality:id,name',
                'identityType:id,name',
                'examinationMarker:staff_id,id,marker_id,examination_centre_id,examination_id',
                'examinationMarker.examName:id,name,academic_period_id',
                'examinationMarker.examinationCentre:id,name,area_id',
                'birthplaceArea:id,name',
                'nationality:id,name',
                'identityType:id,name',
                'examinationMarker.examinationMarkersSubject.educationSubject:id,name,code',
                'examinationMarker.examinationCentre.area:id,name',
                'examinationMarker.examName.academicPeriod:id,name',
                'examinationMarker.securityUserCreatedId:id,first_name'
            )->where('openemis_no', $openemisno)->where('is_staff', config('constants.staffIds.staff'))->where('status', config('constants.staffIds.status'))->get();
            return $markerview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch marker list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Markers list Not Found');
        }
    }

    /**
     * @param string $examinerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function autocompleteExaminerId(string $examinerId)
    {
        try {
            $data = ExaminationMarker::select("id as key", "marker_id as value")
                ->where("marker_id", "LIKE", "%" . $examinerId . "%")
                ->get();
            if ($data->count()) {
                $results = $data->toArray();
                return $results;
            } else {
                return array();
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Examiner id Not Found');
        }
    }

    /**
     * @param string $examinerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function detailsByExaminerId(string $examinerId)
    {
        try {
            $details = ExaminationMarker::with(
                [
                    'examName:id,name',
                    'examinationCentre:id,name',
                    'securityUser:id,first_name,middle_name,third_name,last_name,address,postal_code,openemis_no',
                    'securityUser.gender:id,name',
                    'securityUser.nationality:id,name',
                    'securityUser.identityType:id,name',
                    'securityUser.birthplaceArea:id,name',
                    'examinationCentre:id,code,name,area_id'
                ]
            )->where('marker_id', $examinerId)->get();
            return $details;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch marker from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Marker Not Found');
        }
    }

    /**
     * @param ExaminationMarkerRequest $request
     * @param string $examinerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMarkerDetails(ExaminationMarkerRequest $request)
    {
        DB::beginTransaction();
        try {
            $marker = $request->validated();
            if (!empty($request['openemis_no'])) {
                $openemis = $request['openemis_no'];
                $user = SecurityUser::where('openemis_no', $openemis)->first();
                $user['openemis_no'] = $user->openemis_no;
                $user['username'] = $user->openemis_no;
            } elseif ($request->has('marker_id')) {
                $examinerId = $request['marker_id'];
                $user = SecurityUser::whereHas(
                    'examinationMarker',
                    function ($query) use ($examinerId) {
                        $query->where('marker_id', $examinerId);
                    }
                )->first();
                $user['marker_id'] = $user->marker_id;
            } else {
                $user = new SecurityUser();
                $user['openemis_no'] = uniqid();
                $user['username'] = $user['openemis_no'];
            }
            $user['first_name'] = $request->first_name;
            $user['middle_name'] = $request->middle_name;
            $user['third_name'] = $request->third_name;
            $user['last_name'] = $request->last_name;
            $user['gender_id'] = $request->gender_id;
            $user['date_of_birth'] = $this->changeDateFormat($request['date_of_birth']['text']);
            $user['address'] = $request->address;
            $user['postal_code'] = $request->postal_code;
            $user['address_area_id'] = (isset($request['address_area_id'][0]['id'])) ? $request['address_area_id'][0]['id'] : null;
            $user['birthplace_area_id'] = (isset($request['birthplace_area_id'][0]['id'])) ? $request['birthplace_area_id'][0]['id'] : null;
            $user['nationality_id'] = ($request['nationality_id'] == "null" || $request['nationality_id'] == "") ? null : $request['nationality_id'];
            $user['identity_type_id'] = ($request['identity_type_id'] == "null" || $request['identity_type_id'] == "") ? null : $request['identity_type_id'];
            $user['identity_number'] = $request->identity_number;
            $user['is_staff'] = config('constants.staffIds.staff');
            $user['status'] = config('constants.staffIds.status');
            $user['created_user_id'] = JWTAuth::user()->id;
            $user['created'] = Carbon::now()->toDateTimeString();
            $record = $user->save();
            if ($record) {
                $marker = new ExaminationMarker();
                $marker['examination_centre_id'] = $request->examination_centre_id;
                $marker['examination_id'] = $request->examination_id;
                $marker['classification'] = $request->classification;
                $marker['marker_id'] = '';
                $marker['staff_id'] = $user->id;
                $marker['created_user_id'] = JWTAuth::user()->id;
                $marker['created'] = Carbon::now()->toDateTimeString();
                $exammarker = $marker->save();
                $markersId = $this->examinerIdGenerateId($marker);
            }
            if ($exammarker && $request->subjects && count($request->subjects)) {
                $subjectdata = [];
                foreach ($request->subjects as $subject) {
                    if (!empty($subject)) {
                        $data = [];
                        $data['id'] = Str::uuid();
                        $data['examination_markers_id'] = $marker->id;
                        $data['education_subjects_id'] = $subject['id'];
                        $data['created_user_id'] = JWTAuth::user()->id;
                        $data['created'] = Carbon::now()->toDateTimeString();
                        $subjectdata[] = $data;
                    }
                }
                ExaminationMarkersSubject::insert($subjectdata);
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to add marker ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Marker Not update');
        }
    }

    /**
     * @param ExaminationMarkerRequest $request
     * @param string $examinerId
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function updateMarker(ExaminationMarkerRequest $request, string $examinerId)
    {
        DB::beginTransaction();
        try {
            $examiner = ExaminationMarker::where('marker_id', $examinerId)->first();
            if ($examiner) {
                $marker_id = $examiner->staff_id;
                $data = SecurityUser::find($marker_id);
                $data['first_name'] = $request->first_name;
                $data['middle_name'] = $request->middle_name;
                $data['third_name'] = $request->third_name;
                $data['last_name'] = $request->last_name;
                $data['gender_id'] = $request->gender_id;
                $data['date_of_birth'] = $this->changeDateFormat($request['date_of_birth']['text']);
                $data['nationality_id'] = $request->nationality_id;
                $data['identity_type_id'] = $request->identity_type_id;
                $data['identity_number'] = $request->identity_number;
                $data['address'] = $request->address;
                $data['postal_code'] = $request->postal_code;
                $data['address_area_id'] = (isset($request['address_area_id'][0]['id'])) ? $request['address_area_id'][0]['id'] : null;
                $data['birthplace_area_id'] = (isset($request['birthplace_area_id'][0]['id'])) ? $request['birthplace_area_id'][0]['id'] : null;
                $data['address'] = $request->address;
                $data['postal_code'] = $request->postal_code;
                $data['modified'] = Carbon::now()->toDateTimeString();
                $data['modified_user_id'] = JWTAuth::user()->id;
                $userdata = $data->save();
                $examiner = $data->id;
            }
            if ($userdata) {
                $examinerIdd = ExaminationMarker::where('marker_id', $examinerId)->first();
                $examinerIdd['examination_centre_id'] = $request->examination_centre_id;
                $examinerIdd['examination_id'] = $request->examination_id;
                $examinerIdd['modified'] = Carbon::now()->toDateTimeString();
                $examinerIdd['modified_user_id'] = JWTAuth::user()->id;
                $examinerIds  = $examinerIdd->save();
            }
            $examiner_Idd = ExaminationMarker::where('marker_id', $examinerId)->first()->id;
            if ($examinerIds && $request->subjects && count($request->subjects)) {
                $examinersubject = ExaminationMarkersSubject::where('examination_markers_id', $examiner_Idd)->delete();
                if (count($request->subjects)) {
                    $subjectdata = [];
                    foreach ($request->subjects as $subject) {
                        if (!empty($subject)) {
                            $data = [];
                            $data['examination_markers_id'] = $examinerIdd->id;
                            $data['education_subjects_id'] = $subject['id'];
                            $data['created'] = Carbon::now()->toDateTimeString();
                            $data['created_user_id'] = JWTAuth::user()->id;
                            $subjectdata[] = $data;
                        }
                    }
                    ExaminationMarkersSubject::insert($subjectdata);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to fetch marker from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Marker Not update');
        }
    }

    /**
     * @param ExaminationMarker|null $examStud
     * @return string
     */
    public function examinerIdGenerateId(ExaminationMarker $marker = null)
    {
        if ($marker) {
            $current_year = (Carbon::now()->format('y'));
            $examCente = ExaminationCentre::where('id', $marker->examination_centre_id)->first();
            $ec_code = $examCente->code;
            $examinerId = str_pad($current_year . $ec_code . 'X' . mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);
            ExaminationMarker::where('id', $marker->id)->update(['marker_id' => $examinerId]);
            return $examinerId;
        } else {
            $examinationmarker  = ExaminationMarker::where('marker_id', '')->with('examinationCentre')->get();
            if ($examinationmarker->count()) {
                $examinationmarker = $this->modifyData($examinationmarker);
                DB::statement(
                    'UPDATE examination_markers SET marker_id = (' . $examinationmarker['str'] . ') WHERE id IN(' .  $examinationmarker['ids'] . ')'
                );
            }
        }
    }

    /**
     * @param int $centreId
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function checkIfSafeToDelete(int $examMarkerId)
    {
        try {
            $componentMark = ExaminationStudentsComponentsMark::where('examination_markers_id', $examMarkerId)->exists();
            if ($componentMark) {
                return false;
            }

            $examapportion = ExaminationMarkersApportionment::where('examination_markers_id', $examMarkerId)->exists();
            if ($examapportion) {
                return false;
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam marker.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete exam marker.");
        }
    }

    /**
     * @param int $examMarkerId
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function deleteMarker(int $examMarkerId)
    {
        try {
            $markerId = ExaminationMarker::find($examMarkerId);
            if ($markerId) {
                $markersubject = $markerId->examinationMarkersSubject()->delete();
                return $markerId->delete();
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete marker',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination Marker not deleted.");
        }
    }
}
