<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationStudent;

class ExamCentreStudentRepository extends Controller
{
    /**
     * Getting Exam Centre Student List
     * @param array $search
     * @param $examCentreId
     * @param $examinationId
     * @return JsonResponse
     */
    public function getExamCentreStudentList(array $search, $examCentreId, $examinationId)
    {
        try {
            $students = ExaminationStudent::where('examination_centre_id', $examCentreId)
                ->where('examination_id', $examinationId)
                ->select('id', 'candidate_id', 'student_id', 'examination_centre_id', 'examination_id', 'student_id')
                ->whereHas(
                    'securityUser',
                    function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name', 'openemis_no')
                            ->where('is_student', config('constants.students.isStudent'))
                            ->where('is_staff', config('constants.students.isStaff'))
                            ->where('is_guardian', config('constants.students.isGuardian'));
                    }
                );

            if (isset($search['keyword'])) {
                $students->where(function ($q) use ($search) {
                    $q->where('candidate_id', 'LIKE', "%" . $search['keyword'] . "%")
                    ->orWhereHas(
                        'securityUser',
                        function ($query) use ($search) {
                            $query->where('first_name', 'LIKE', "%" . $search['keyword'] . "%")
                                ->orWhere('middle_name', 'LIKE', "%" . $search['keyword'] . "%")
                                ->orWhere('third_name', 'LIKE', "%" . $search['keyword'] . "%")
                                ->orWhere('last_name', 'LIKE', "%" . $search['keyword'] . "%")
                                ->orWhere('openemis_no', 'LIKE', "%" . $search['keyword'] . "%");
                        }
                    );
                });
            }

            $students = $students->get();
            return $students;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam Centres Students Not Found");
        }
    }

    /**
     * Getting Exam Centre Student Details
     * @param string $studentId
     * @return JsonResponse
     */
    public function examCentreStudentDetails(string $studentId)
    {
        try {
            $student = ExaminationStudent::where('id', $studentId)
                    ->select(
                        'id',
                        'candidate_id',
                        'examination_centre_id',
                        'examination_id',
                        'student_id',
                        'modified_user_id',
                        'modified',
                        'created_user_id',
                        'created',
                        'examination_attendance_types_id'
                    )
                    ->with(
                        [
                            'examinationCentre' => function ($query) {
                                $query->select('id', 'code', 'name', 'area_id');
                            },
                            'examinationCentre.area' => function ($query) {
                                $query->select('id', 'name');
                            },
                            'examName' => function ($query) {
                                $query->select('id', 'code', 'name', 'academic_period_id');
                            },
                            'examName.academicPeriod' => function ($query) {
                                $query->select('id', 'start_year');
                            },
                            'examinationStudentsOption' => function ($query) {
                                $query->select('id', 'carry_forward', 'examination_students_id', 'examination_options_id')
                                    ->with(
                                        [
                                            'examinationOption' => function ($q) {
                                                $q->select('id', 'code', 'name');
                                            }
                                        ]
                                    );
                            },
                            'attendanceType' => function ($query) {
                                $query->where('visible', config('constants.visibleValue.visibleCode'))
                                    ->select('id', 'name');
                            },
                            'securityUser' => function ($query) {
                                $query->select(
                                    'id',
                                    'first_name',
                                    'middle_name',
                                    'third_name',
                                    'last_name',
                                    'openemis_no',
                                    'address',
                                    'postal_code',
                                    'gender_id',
                                    'date_of_birth',
                                    'nationality_id',
                                    'identity_type_id',
                                    'address_area_id',
                                    'identity_number'
                                );
                            },
                            'securityUser.gender' => function ($query) {
                                $query->select('id', 'name');
                            },
                            'securityUser.nationality' => function ($query) {
                                $query->where('visible', config('constants.visibleValue.visibleCode'))
                                    ->select('id', 'name');
                            },
                            'securityUser.identityType' => function ($query) {
                                $query->where('visible', config('constants.visibleValue.visibleCode'))
                                    ->select('id', 'name');
                            },
                            'securityUser.areaAdministrative' => function ($query) {
                                $query->where('visible', config('constants.visibleValue.visibleCode'))
                                    ->select('id', 'name');
                            },
                            'securityUser.birthplaceArea' => function ($query) {
                                $query->where('visible', config('constants.visibleValue.visibleCode'))
                                    ->select('id', 'name');
                            },
                            'securityUser.userSpecialNeedsAssessment' =>  function ($query) {
                                $query->select('id', 'special_need_type_id', 'special_need_difficulty_id', 'security_user_id')
                                    ->with(
                                        [
                                            'specialNeed' => function ($q) {
                                                $q->where('visible', config('constants.visibleValue.visibleCode'))
                                                    ->select('id', 'name');
                                            },
                                            'specialDifficulty' => function ($q) {
                                                $q->where('visible', config('constants.visibleValue.visibleCode'))
                                                    ->select('id', 'name');
                                            }
                                        ]
                                    );
                            },
                            'createdByUser' => function ($query) {
                                $query->where('status', config('constants.createdByUser.status'))
                                    ->select('id', 'first_name', 'middle_name', 'third_name', 'last_name');
                            },
                            'modifiedUser' => function ($query) {
                                $query->where('status', config('constants.modifiedByUser.status'))
                                    ->select('id', 'first_name', 'middle_name', 'third_name', 'last_name');
                            }
                        ]
                    )
                    ->first();
            return $student;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch student details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Student Details Not Found");
        }
    }
}
