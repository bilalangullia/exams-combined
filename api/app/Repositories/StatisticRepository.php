<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDF;
use Storage;
use App\Models\ExaminationSubject;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationCentre;
use App\Models\ExaminationOption;
use App\Models\AcademicPeriod;
use App\Models\Examination;
use App\Models\ReportProgress;
use Excel;
use App\Exports\CandidatePerSubjectExport;
use App\Exports\CandidatePerCentreExport;
use App\Exports\SubjectsPerCandidateExport;
use Illuminate\Support\Str;
use App\Helper\SlugHelper;
use Tymon\JWTAuth\Facades\JWTAuth;

class StatisticRepository extends Controller
{
    /**
     * Generating Candidate Per Subject Report.
     * @param array $data
     * @return int
     */
    public function candidatePerSubjectReport(array $data)
    {
        DB::beginTransaction();
        try {
            $requestData = $data;
            $reportData = [];

            if ($requestData['format'] == 1) {
                $format = '.xlsx';
            } else {
                $format = '.csv';
            }

            $academicPeriod = AcademicPeriod::where('id', $requestData['academic_period_id'])->select('id', 'start_year')->first()->toArray();
            
            

            $reportData['data'] = $requestData;
            $reportData['academicPeriod'] = $academicPeriod;
            $reportData['reportName'] = 'Statistics: Candidates Per Subject';
            $reportData['format'] = $format;
            $reportData['module'] = 'CandidatePerSubjectStatistics';
            
            $saveDataArr = $this->saveReportData($reportData);
            $file_name = $saveDataArr['file_name'];
            unset($saveDataArr['file_name']);
            $report = ReportProgress::create($saveDataArr);
            if ($report) {
                $id = $report->id;
                
                $processing = config('constants.reportUpdateStatus.processing');
                ReportProgress::where('id', $id)->update(['status' => $processing]);

                $timeOut = false;
                for ($i = 0; $i < $saveDataArr['total_records']; $i++) {
                    if (Carbon::now()->toDateTimeString() >= $saveDataArr['expiry_date']) {
                        $timeOut = true;
                        break;
                    }
                    ReportProgress::where('id', $id)->increment('current_records');
                }

                if ($timeOut) {
                    $updateArr['status'] = config('constants.reportUpdateStatus.notCompleted');
                    $updateArr['error_message'] = "report can not be generated due to exceed time";
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);
                    $error['msg'] = "report can not be generated due to exceed time";
                    DB::commit();
                    return 2;
                } else {
                    $storage_path = 'public/reports';
                    $filePath = $storage_path . '/' . $file_name;
                    $excel =  Excel::store(new CandidatePerSubjectExport($data), $filePath);
                    
                    
                    $updateArr['status'] = config('constants.reportUpdateStatus.completed');
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);

                    Log::info('Excel Report generated and saved in database and directory', ['method' => __METHOD__, 'data' => []]);
                    DB::commit();
                    return 1;
                }

            } else {
                DB::commit();
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to generate candidate per subject statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to generate candidate per subject statistics report");
        }
    }

    /**
     * Creating data set for Candidate per subject report.
     * @param array $data
     * @return array
     */
    public function saveReportData(array $data)
    {
        try {
            $academicPeriod = $data['academicPeriod'];
            $reportName = $data['reportName'];
            
            $reportName = $reportName . " " . $academicPeriod['start_year'];
            
            $fileName = $reportName . "_" . time();
            
            $fileName = SlugHelper::slugify($fileName);
            $params = json_encode($data['data']);
            
            $storage_path = 'public/reports';
            if (!Storage::exists($storage_path)) {
                Storage::makeDirectory($storage_path, 0755);
            }
            $url = asset('storage/reports/' . $fileName . $data['format']);
            $countCandidates = 1;
            
            $saveArr['id'] = Str::uuid();
            $saveArr['name'] = $reportName;
            $saveArr['module'] = $data['module'];
            $saveArr['params'] = $params;
            $saveArr['file_path'] = $url;
            $saveArr['total_records'] = $countCandidates;
            $saveArr['status'] = config('constants.reportUpdateStatus.notGeneratedOrNew');
            $saveArr['created'] = Carbon::now()->toDateTimeString();
            $saveArr['modified'] = Carbon::now()->toDateTimeString();
            $saveArr['created_user_id'] = JWTAuth::user()->id;
            $saveArr['expiry_date'] = Carbon::now()->addHours(6)->toDateTimeString();
            $saveArr['modified_user_id'] = JWTAuth::user()->id;
            $saveArr['file_name'] = $fileName . $data['format'];
            
            return $saveArr;
        } catch (\Exception $e) {
            log::error(
                'Failed to genrate report data',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to genrate report data");
        }
    }

    /**
     * Getting Statistics Report List
     * @param Request $request
     * @return JsonResponse
     */
    public function statisticsReportList(array $data)
    {
        try {
            $module = $data['module']??"";
            unset($data['module']);
            $reportList = ReportProgress::select(
                        'id', 
                        'name', 
                        'created_user_id',
                        'created',
                        'expiry_date',
                        'modified',
                        'file_path',
                        'status'
                    )
                    ->where('module', $module)
                    ->with(
                        [
                            'securityUser' => function ($q) {
                                $q->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                    ->where('status', config('constants.createdByUser.status'));
                            }
                        ]
                    );

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $reportList->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%")
                        ->orWhereHas(
                        'securityUser',
                        function ($query) use ($keyword) {
                            $query->where('first_name', 'LIKE', "%" . $keyword . "%")
                                ->orWhere('middle_name', 'LIKE', "%" . $keyword . "%")
                                ->orWhere('third_name', 'LIKE', "%" . $keyword . "%")
                                ->orWhere('last_name', 'LIKE', "%" . $keyword . "%");
                        }
                    );
                });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $reportCount = $reportList;
                $total = $reportCount->count();
                $reportList->skip($data['start'])
                                ->take($data['end'] - $data['start']);
            } else {
                $reportCount = $reportList;
                $total = $reportCount->count();
            }

            $list = $reportList->get();
            
            $data['reports'] = $list;
            $data['total'] = $total;
            
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get list of candidate per subject statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get list of candidate per subject statistics report");
        }
    }

    /**
     * Generating Statistics Report.
     * @param array $data
     * @return int
     */
    public function candidatePerCentreReport(array $data)
    {
        DB::beginTransaction();
        try {
            $requestData = $data;
            
            $reportType = $requestData['report_name'];
            $reportData = [];

            if ($requestData['format'] == 1) {
                $format = '.xlsx';
            } else {
                $format = '.csv';
            }

            $academicPeriod = AcademicPeriod::where('id', $requestData['academic_period_id'])->select('id', 'start_year')->first()->toArray();
            
            $reportData['data'] = $requestData;
            $reportData['academicPeriod'] = $academicPeriod;
            
            if ($reportType == 1) {
                $reportData['reportName'] = 'Statistics: Subjects Per Candidate';
            } else {
                $reportData['reportName'] = 'Statistics: Candidates Per Centre';
            }
            
            $reportData['format'] = $format;
            $reportData['module'] = 'StatisticsExam';
            
            $saveDataArr = $this->saveReportData($reportData);
            $file_name = $saveDataArr['file_name'];
            unset($saveDataArr['file_name']);
            $report = ReportProgress::create($saveDataArr);
            if ($report) {
                $id = $report->id;
                
                $processing = config('constants.reportUpdateStatus.processing');
                ReportProgress::where('id', $id)->update(['status' => $processing]);

                $timeOut = false;
                for ($i = 0; $i < $saveDataArr['total_records']; $i++) {
                    if (Carbon::now()->toDateTimeString() >= $saveDataArr['expiry_date']) {
                        $timeOut = true;
                        break;
                    }
                    ReportProgress::where('id', $id)->increment('current_records');
                }

                if ($timeOut) {
                    $updateArr['status'] = config('constants.reportUpdateStatus.notCompleted');
                    $updateArr['error_message'] = "report can not be generated due to exceed time";
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);
                    $error['msg'] = "report can not be generated due to exceed time";
                    DB::commit();
                    return 2;
                } else {
                    $storage_path = 'public/reports';
                    $filePath = $storage_path . '/' . $file_name;

                    if ($reportType == 1) {
                        $excel =  Excel::store(new SubjectsPerCandidateExport($data), $filePath);
                    } else {
                        $excel =  Excel::store(new CandidatePerCentreExport($data), $filePath);
                    }
                    
                    $updateArr['status'] = config('constants.reportUpdateStatus.completed');
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);

                    Log::info('Excel Report generated and saved in database and directory', ['method' => __METHOD__, 'data' => []]);
                    DB::commit();
                    return 1;
                }

            } else {
                DB::commit();
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to Statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to Statistics report");
        }
    }
}
