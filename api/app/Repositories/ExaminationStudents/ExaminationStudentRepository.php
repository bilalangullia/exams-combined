<?php

namespace App\Repositories\ExaminationStudents;

use App\Models\ExaminationStudent;
use DB;
use Illuminate\Support\Facades\Log;

class ExaminationStudentRepository
{
    
    public function __construct()
    {
        ini_set('memory_limit', -1);
    }
    
    public function getCandidatesByExamIdAndExamCenterId($examId, $examCenterId, $candidateId, $skip = '', $take = '')
    {
        if (empty($candidateId)) {
            try {
                $candidatesQuery = DB::table('examination_students')
                    ->join('security_users', 'examination_students.student_id', '=', 'security_users.id')
                    ->join('genders', 'security_users.gender_id', '=', 'genders.id')
                    ->select(
                        'examination_students.id',
                        'examination_students.candidate_id',
                        'examination_students.student_id',
                        'security_users.first_name',
                        'security_users.middle_name',
                        'security_users.third_name',
                        'security_users.last_name',
                        'security_users.date_of_birth',
                        'genders.name',
                        'security_users.identity_number'
                    )
                    ->where('examination_students.examination_centre_id', '=', $examCenterId)
                    ->where('examination_students.examination_id', '=', $examId);
                
                if($skip >= 0 && $take > 0){
                    $candidatesQuery->skip($skip)->take($take);
                }
                
                $candidates = $candidatesQuery->get();
                
               // Log::info('Successfully Fetched all candidates details by exam id and exam centre id', ['method' => __METHOD__, 'data' => ['allCandidatedData' => $candidates, 'examId' => $examId, 'examCentreId' => $examCenterId]]);
            } catch (\Exception $e) {
                Log::error(
                    'Failed to fetch all candidates details by exam id and exam centre id',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                return array();
            }
        } else {
            try {
                $candidates = DB::table('examination_students')
                    ->join('security_users', 'examination_students.student_id', '=', 'security_users.id')
                    ->join('genders', 'security_users.gender_id', '=', 'genders.id')
                    ->select(
                        'examination_students.id',
                        'examination_students.candidate_id',
                        'examination_students.student_id',
                        'security_users.first_name',
                        'security_users.middle_name',
                        'security_users.third_name',
                        'security_users.last_name',
                        'security_users.date_of_birth',
                        'genders.name',
                        'security_users.identity_number'
                    )
                    ->where('examination_students.examination_centre_id', '=', $examCenterId)
                    ->where('examination_students.examination_id', '=', $examId)
                    ->where('examination_students.id', '=', $candidateId)
                    ->get();
                Log::info('Successfully Fetched individual candidate by exam id ,exam centre id and examstudent id', ['method' => __METHOD__, 'data' => ['candidatedData' => $candidates, 'examId' => $examId, 'examCentreId' => $examCenterId, 'examStudentId' => $candidateId]]);
            } catch (\Exception $e) {
                Log::error(
                    'Failed to fetch individual candidate details by exam id,exam centre id and exam student id',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                return array();
            }
        }


        if (!$candidates) {
            return array();
        }
        return $candidates->toArray();

    }

}