<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamComponentUpdationRequest;
use App\Http\Requests\ExaminationSubjectImportRequest;
use App\Imports\ExaminationComponentImport;
use App\Models\ComponentType;
use App\Models\Examination;
use App\Models\ExaminationComponent;
use App\Models\ExaminationComponentsMarkType;
use App\Models\ExaminationStudentsComponentsGrade;
use App\Models\ExaminationStudentsComponentsMark;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Tymon\JWTAuth\Facades\JWTAuth;

class ComponentRepository extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationcomponetList(Request $request, string $examId)
    {
        try {
            $data = $request->all();
            $componentlist = ExaminationComponent::whereHas(
                'examinationOption.examinationSubject.examName',
                function ($query) use ($request) {
                    $query->where('examination_id', $request->examId);
                }
            )->with(
                [
                    'examinationOption:id,code,name',
                ]
            );
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $componentlist;
                $total = $userCount->count();
                $componentlist->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $componentlist;
                $total = $userCount->count();
            }
            if (isset($request->academic_period_id)) {
                $complist = $componentlist->select('id', 'code', 'name', 'examination_options_id')->where(
                    'component_types_id',
                    config('constants.componentType.type')
                )->whereHas(
                    'examinationOption.examinationSubject.examName.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('academic_period_id', $request->academic_period_id);
                    }
                );
            }
            if (isset($request['keyword'])) {
                $componentSearch = $componentlist->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("code", "LIKE", "%" . $request['keyword'] . "%");
                                $query->orwhere("name", "LIKE", "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationOption',
                            function ($query) use ($request) {
                                $query->where('code', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $componentSearch = $componentlist->get();
            return array("totalRecord" => $total, "record" => $componentSearch);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch component list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Component List Not Found');
        }
    }

    /**
     * @param Request $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function getExaminationComponentView(Request $request, string $componentId)
    {
        try {
            $componentId = ExaminationComponent::select(
                'id',
                'code',
                'name',
                'weight',
                'examination_date',
                'start_time',
                'end_time',
                'examination_options_id',
                'examination_grading_type_id',
                'max_raw_mark',
                'max_con_mark',
                'component_types_id',
                'created_user_id'
            )->where('id', $request->componentId)->with(
                'examinationOption:id,code,name,examination_subject_id',
                'examinationOption.examinationSubject:id,code,name,examination_id',
                'examinationOption.examinationSubject.examName:id,code,name,academic_period_id',
                'examinationOption.examinationSubject.examName.academicPeriod:id,code,name',
                'examinationComponentsMarkType.markType:id,name'
            )->get();
            return $componentId;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch component list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Component view not found');
        }
    }

    /**
     * @param Request $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function getExaminationComponentEdit(Request $request, string $componentId)
    {
        try {
            $componentId = ExaminationComponent::select(
                'id',
                'code',
                'name',
                'weight',
                'examination_date',
                'start_time',
                'end_time',
                'examination_options_id',
                'examination_grading_type_id',
                'max_raw_mark',
                'max_con_mark',
                'component_types_id',
                'created_user_id'
            )->with(
                'examinationOption:id,code,name,examination_subject_id',
                'examinationOption.examinationSubject:id,code,name',
                'examinationOption.examinationSubject.examName:id,code,name,academic_period_id',
                'examinationOption.examinationSubject.examName.academicPeriod:id,code,name',
                'examinationComponentsMarkType.markType:id,name'
            )->where('id', $request->componentId)->get();
            return $componentId;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch component list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(' view Not Found');
        }
    }

    /**
     * get componnet type dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComponentTypeDropdown()
    {
        DB::beginTransaction();
        try {
            $componenttypelist = ComponentType::select('id', 'name')->get();

            return $componenttypelist;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch component list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(' Component Type Not Found');
        }
    }

    /**
     * @param ExamComponentUpdationRequest $request
     * @param int $componentId
     * @return array
     */
    public function getComponentUpdate(ExamComponentUpdationRequest $request, int $componentId)
    {
        DB::beginTransaction();
        try {
            $examcomponent = ExaminationComponent::find($componentId);           
            $examcomponent['code'] = $request->code;
            $examcomponent['name'] = $request->name;
            $examcomponent['weight'] = $request->weight;
            $examcomponent['carry_forward'] = ($request->carry_forward == 'No') ? 0 : 1;
            $examcomponent['start_time'] = $request->start_time;
            $examcomponent['end_time'] = $request->end_time;
            $examcomponent['max_raw_mark'] = $request->max_raw_mark;
            $examcomponent['max_con_mark'] = $request->max_con_mark;
            $examcomponent['component_types_id'] = $request->component_types_id;            
            $examcomponent['examination_grading_type_id'] = $request->examination_grading_type_id;
            $examcomponent['examination_date'] = $request->examination_date;
            $examcomponent['modified'] = Carbon::now()->toDateTimeString();
            $examcomponent['modified_user_id'] = JWTAuth::user()->id;
            $examcomponent->save();
            $com_id = $examcomponent->id;
            $addmarktype = ExaminationComponentsMarkType::where('examination_components_id', $com_id)->first();
            if (!$addmarktype) {
                return array('message' => '');
            } else {
                $addmarktype['mark_types_id'] = $request->mark_types_id;
                $addmarktype['examination_components_id'] = $com_id;
                $addmarktype['modified'] = Carbon::now()->toDateTimeString();
                $addmarktype['modified_user_id'] = config('modifiedUser.user_id');
                $savedata = $addmarktype->save();
            }
            DB::commit();
            return array('message' => '');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to update details  into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return array('message' => 'error');
        }
    }

    /**
     * Checking component existence
     * @param string $compId
     * @return array
     */
    public function checkComponentExistence(string $compId)
    {
        try {
            $checkComponent = ExaminationStudentsComponentsMark::where('examination_components_id', $compId)->first();

            return array('data' => $checkComponent, 'compId' => $compId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to update details  into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return array('message' => 'error');
        }
    }

    /**
     * add new component
     * @param ExamComponentUpdationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addExaminationComponent(ExamComponentUpdationRequest $request)
    {
        DB::beginTransaction();
        try {
            $components = new ExaminationComponent();
            $components['code'] = $request->code;
            $components['name'] = $request->name;
            $components['component_types_id'] = $request->component_types_id;
            $components['carry_forward'] = ($request->carry_forward == 'No') ? 0 : 1;
            $components['examination_options_id'] = $request->examination_options_id;
            $components['examination_grading_type_id'] = $request->examination_grading_type_id;
            $components['weight'] = $request->weight;
            $components['start_time'] = $request->start_time;
            $components['end_time'] = $request->end_time;
            $components['max_raw_mark'] = $request->max_raw_mark;
            $components['max_con_mark'] = $request->max_con_mark;
            $components['examination_date'] = $request->examination_date;
            $components['created'] = Carbon::now()->toDateTimeString();
            $components['created_user_id'] = JWTAuth::user()->id;
            $savecomp = $components->save();
            if ($savecomp) {
                $com_marktype = new ExaminationComponentsMarkType();
                $com_marktype['mark_types_id'] = $request->mark_types_id;
                $com_marktype['examination_components_id'] = $components->id;
                $com_marktype['created'] = Carbon::now()->toDateTimeString();
                $com_marktype['created_user_id'] = config('constants.createdUser.user_id');
                $com_marktype->save();
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to add details into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination's components Not Added");
        }
    }

    /**
     * checking Component id existance
     * @param string $componentId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function checkIfSafeToDelete(int $componentId)
    {
        try {
            $compgrade = ExaminationStudentsComponentsGrade::where('examination_components_id', $componentId)->exists();
            if ($compgrade) {
                return false;
            }
            $markcomp = ExaminationStudentsComponentsMark::where('examination_components_id', $componentId)->exists();
            if ($markcomp) {
                return false;
            }
            $marktypecomp = examinationComponentsMarkType::where('examination_components_id', $componentId)->exists();
            if ($marktypecomp) {
                return false;
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam component.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed To Delete Examination component");
        }
    }

    /**
     * @param int $optionId
     * @return bool|\Illuminate\Http\JsonResponse
     */

    public function deleteExamComponent(int $componentId)
    {
        try {
            $comp = ExaminationComponent::find($componentId);
            if ($comp) {
                $dltmultichoice = $comp->examinationComponentsMultiplechoice()->delete();
                $dltmultichoice_response = $comp->examinationStudentsMultiplechoiceResponse()->delete();
                $dltmark_scaling = $comp->examinationCentresExaminationsMarksScaling()->delete();
                return $comp->delete();
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam component.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("component not deleted.");
        }
    }

    /**
     * @param ExaminationSubjectImportRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function importComponent(ExaminationSubjectImportRequest $request)
    {
        try {
            $excelData = Excel::toArray(new ExaminationComponentImport(), $request->file('file'));
            $i = -1;
            $validation = [];
            $add_record = [];
            $stored_data = [];
            $insert = [];
            $inserta = [];
            $insertb = [];
            $academic_period = $request['academic_period_id'];
            $examId = $request['examination_id'];
            $examsData = Examination::select(
                'id',
                'code',
                'name',
                'academic_period_id'
            )->where('academic_period_id', $academic_period)
                ->with(
                    [
                        'academicPeriod' => function ($query) {
                            $query->select('id', 'name');
                        },
                    ]
                )->first();
            foreach ($excelData[0] as $data) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }

                if (!$data[0]) { //option
                    $label = $excelData[0][1][0];
                    $errors[$label] = 'option Required';
                }

                if (!$data[1]) { //Component Code
                    $label = $excelData[0][1][1];
                    $errors[$label] = 'Component Code Required';
                }
                if (!$data[2]) { //Component name
                    $label = $excelData[0][1][2];
                    $errors[$label] = 'Component name Required';
                }
                if (!$data[3]) { //Component type
                    $label = $excelData[0][1][3];
                    $errors[$label] = 'Component type Required';
                }
                if (!$data[7]) { //Carry Forward
                    $label = $excelData[0][1][7];
                    $errors[$label] = 'Carry Forward Required';
                }
                if (!$data[7]) { //mark type
                    $label = $excelData[0][1][7];
                    $errors[$label] = 'mark type Required';
                }
                if (!$data[7]) { //max raw mark
                    $label = $excelData[0][1][7];
                    $errors[$label] = 'max raw mark Required ';
                }
                if (!$data[8]) { //max con mark
                    $label = $excelData[0][1][8];
                    $errors[$label] = 'max con mark Required';
                }
                if (count($errors) > 0) { //row fail
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            'Academic Period' => $examsData['academicPeriod']['name'],
                            'Examination' => $examsData['name'],
                        ],
                        'errors' => $errors,
                    ];
                } else {
                    if ($data[0]) { //Update Record
                        $componentExist = ExaminationComponent::where('code', $data[1])->first();
                        if ($componentExist) {
                            $ids = $componentExist['id'];
                            $component = [];
                            $component['examination_options_id'] = $data[0];
                            $component['code'] = $data[1];
                            $component['name'] = $data[2];
                            $component['component_types_id'] = $data[3];
                            $component['examination_grading_type_id'] = $data[5];
                            $component['carry_forward'] = $data[9];
                            $component['weight'] = $data[6];
                            $component['examination_date'] = $data[10];
                            $component['start_time'] = $data[11];
                            $component['end_time'] = $data[12];
                            $component['max_raw_mark'] = $data[7];
                            $component['max_con_mark'] = $data[8];
                            $component['modified_user_id'] = JWTAuth::user()->id;
                            $component['modified'] = Carbon::now()->toDateTimeString();
                            $examComp = ExaminationComponent::where(['code' => $data[1]])->update($component);
                            $marktype = ExaminationComponentsMarkType::where(
                                'examination_components_id',
                                $ids
                            )->first();
                            if (!empty($marktype)) {
                                $markcomponent = [];
                                $markcomponent['mark_types_id'] = $data[7];
                                $markcomponent['examination_components_id'] = $ids;
                                $markcomponent['modified'] = Carbon::now()->toDateTimeString();
                                $markcomponent['modified_user_id'] = JWTAuth::user()->id;
                                $mark_comp = ExaminationComponentsMarkType::where(
                                    'examination_components_id',
                                    $ids
                                )->update($markcomponent);
                            }
                            $stored_data[] = [
                                'row_number' => $i,
                                'data' => [
                                    'Academic Period' => $examsData['academicPeriod']['name'],
                                    'Examination' => $examsData['name'],
                                    'component code' => $data[1],
                                    'component name' => $data[2],
                                    'carry_forward' => $data[7],
                                    'weight' => $data[6],
                                    'max_raw_mark' => $data[7],
                                    'max_con_mark' => $data[8],
                                ],
                            ];
                        } else {
                            $component = new ExaminationComponent();
                            $component['examination_options_id'] = $data[0];
                            $component['code'] = $data[1];
                            $component['name'] = $data[2];
                            $component['component_types_id'] = $data[3];
                            $component['examination_grading_type_id'] = $data[5];
                            $component['carry_forward'] = $data[9];
                            $component['weight'] = $data[6];
                            $component['examination_date'] = $data[10];
                            $component['start_time'] = $data[11];
                            $component['end_time'] = $data[12];
                            $component['max_raw_mark'] = $data[7];
                            $component['max_con_mark'] = $data[8];
                            $component['created_user_id'] = JWTAuth::user()->id;
                            $component['created'] = Carbon::now()->toDateTimeString();
                            $record = $component->save();
                            if ($record) {
                                $markcomponent = new ExaminationComponentsMarkType();
                                $markcomponent['mark_types_id'] = $data[4];
                                $markcomponent['examination_components_id'] = $component->id;
                                $markcomponent['created'] = Carbon::now()->toDateTimeString();
                                $markcomponent['created_user_id'] = JWTAuth::user()->id;
                                $record = $markcomponent->save();
                            }
                            $add_record[] = [
                                'row_number' => $i,
                                'data' => [
                                    'Academic Period' => $examsData['academicPeriod']['name'],
                                    'Examination' => $examsData['name'],
                                    'component code' => $data[1],
                                    'component name' => $data[2],
                                    'carry_forward' => $data[9],
                                    'weight' => $data[6],
                                    'max_raw_mark' => $data[7],
                                    'max_con_mark' => $data[8],

                                ],
                            ];
                        }
                    } else {
                        $label = $excelData[0][1][0];
                        $errors[$label] = 'Candidate ID does not exist';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                'Academic Period' => $examsData['academicPeriod']['name'],
                                'Examination' => $examsData['name'],
                                'component code' => $data[1],
                                'component name' => $data[2],
                                'carry_forward' => $data[9],
                                'weight' => $data[6],
                                'max_raw_mark' => $data[7],
                                'max_con_mark' => $data[8],
                            ],
                            'errors' => $errors,
                        ];
                    }
                }
            }
            $response = [
                'total_count' => count($excelData[0]) - 2,
                'records_added' => [
                    'count' => count($add_record),
                    'rows' => $add_record,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];
            return $response;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Component Not Imported');
        }
    }
}
