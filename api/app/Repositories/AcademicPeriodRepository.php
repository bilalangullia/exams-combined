<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\AcademicPeriod;
use App\Models\AcademicPeriodLevel;
use App\Models\Examination;
use Carbon\Carbon;
use JWTAuth;

class AcademicPeriodRepository extends Controller
{
	public function getAcademicPeriodList(Request $request)
	{
		try{
            $heading = AcademicPeriod::select('id', 'name', 'code')->where('parent_id', '=', config('constants.allData.parent'))->first();

            if($request['parent_id']){
                $heading = AcademicPeriod::select('id', 'name', 'code')->where('id', $request['parent_id'])->first();
            }

			$listing = AcademicPeriod::select('id', 'name', 'code','current', 'editable','visible','start_date','end_date','academic_period_level_id')->with('academicPeriodLevel')->where('parent_id', $heading->id)->orderBy('id', 'desc');

            if($request['parent_id']) {
                 $listing = AcademicPeriod::select('id', 'name', 'code','current', 'editable','visible','start_date','end_date','academic_period_level_id')->with('academicPeriodLevel')->where('parent_id',$request['parent_id'])->orderBy('id', 'desc');
            }

			if (isset($request['start']) && isset($request['end'])) {
                $listCount = $listing;
                $total           = $listCount->count();
                $listing->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $listCount = $listing;
                $total = $listCount->count();
            }

            if (isset($request['keyword'])) {
                $search = $listing->where( function ($q) use ($request){
                    $q->where(
                        function ($query) use ($request) {
                            $query->where('code', 'LIKE', "%" . $request['keyword'] . "%");
                            $query->orwhere('name', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    )->orwhereHas(
                        'academicPeriodLevel',
                        function ($query) use ($request) {
                            $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    );
                }
            );   
        }
            $search = $listing->get();

            return array("all_data" => $heading, "response" => $search, "total" => $total);
		} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period List Not Found');
        }
	}

    public function getAcademicPeriodDetails(int $academicPeriodId)
    {
        try{
            $detail = AcademicPeriod::where('id', $academicPeriodId)->with('parent')->get();

            return $detail;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Detail Not Found');
        }
    }

    public function academicPeriodLevelDropdown()
    {
        try{
            $dropdown = AcademicPeriodLevel::select('id', 'name')->get();

            return $dropdown;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level Drop Down Not Found');
        }
    }

    public function addAcademicPeriod(array $rawData)
    {
        try{
            $order = $this->getLargestPeriodOrder($rawData['academic_period_level_id']);
            $add = new AcademicPeriod();
            $add['code'] = $rawData['code'];
            $add['name'] = $rawData['name'];
            $add['start_date'] = $rawData['start_date'];
            $add['start_year'] = Carbon::createFromFormat('Y-m-d', $rawData['start_date'])->year;
            $add['end_date'] = $rawData['end_date'];
            $add['end_year'] = Carbon::createFromFormat('Y-m-d', $rawData['end_date'])->year;
            $add['current'] = $rawData['current'];
            $add['academic_period_level_id'] = $rawData['academic_period_level_id'];
            $add['editable'] = $rawData['editable'];
            $add['order'] = $order;
            $add['parent_id'] = $rawData['parent'];
            $add['visible'] = config('constants.visibleValue.visibleCode');
            $add['created_user_id'] = JWTAuth::user()->id;
            $add['created'] = Carbon::now()->toDateTimeString();
            $store = $add->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Added');
        }
    }

    public function getLargestPeriodOrder(int $academicPeriodLevelId = -1)
    {
        try {
            $largestOrder = 0;
            $order = AcademicPeriod::where('academic_period_level_id', $academicPeriodLevelId)->max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest role order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch Largest Order from DB");
        }
    }

    public function updateAcademicPeriod(int $academicPeriodId, array $updateData)
    {
        try{
            $update = AcademicPeriod::find($academicPeriodId);
            $update['code'] = $updateData['code'];
            $update['name'] = $updateData['name'];
            $update['start_date'] = $updateData['start_date'];
            $update['start_year'] = Carbon::createFromFormat('Y-m-d', $updateData['start_date'])->year;
            $update['end_date'] = $updateData['end_date'];
            $update['end_year'] = Carbon::createFromFormat('Y-m-d', $updateData['end_date'])->year;
            $update['current'] = $updateData['current'];
            $update['editable'] = $updateData['editable'];
            $update['visible'] = $updateData['visible'];
            $update['modified_user_id'] = JWTAuth::user()->id;
            $update['modified'] = Carbon::now()->toDateTimeString();
            $updateData = $update->save();

            return $updateData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Updated');
        }
    }

    public function checkIfSafeToDelete(int $academicPeriodId)
    {
        try{
            $academicPeriod = AcademicPeriod::find($academicPeriodId);

            $parent = AcademicPeriod::where('parent_id', $academicPeriodId)->first();
            if($parent){
                return false;
            }

            $exam = Examination::where('academic_period_id', $academicPeriodId)->exists();
            if ($exam) {
                return false;
            }
            
            return true;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete academic period.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }

    /**
     * Deleteing Exam Centres
     * @param int $centreId
     * @return JsonResponse
     */
    public function deleteAcademicPeriod(int $academicPeriodId)
    {
        try{
            $academicPeriod = AcademicPeriod::find($academicPeriodId);
            if ($academicPeriod) {
                return $academicPeriod->delete();
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete academic period.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }
}