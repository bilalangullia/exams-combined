<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExaminationSubjectImportRequest;
use App\Http\Requests\ExamSubjectUpdationRequest;
use App\Imports\ExaminationSubjectImport;
use App\Models\EducationSubject;
use App\Models\Examination;
use App\Models\ExaminationComponent;
use App\Models\ExaminationComponentsMultiplechoice;
use App\Models\ExaminationOption;
use App\Models\ExaminationSubject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Tymon\JWTAuth\Facades\JWTAuth;

class SubjectRepository extends Controller
{
    /**
     * Get multiple-type subjects DropDown on examinationId basis
     * @return mixed
     */
    public function getSubjectDropDown(Request $request)
    {
        try {
            $record = ExaminationComponent::select('id', 'code', 'name', 'examination_options_id')->where(
                'component_types_id',
                config('constants.componentType.type')
            )->with('examinationOption:id,code,name')->whereHas(
                'examinationOption.examinationSubject.examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examId);
                }
            )->get();

            return $record;
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Subject List Not Found');
        }
    }

    /**
     * Getting component list on optionId basis
     * @param Request $request
     * @return mixed
     */
    public function getComponentList(Request $request)
    {
        try {
            $response = ExaminationComponent::select('id', 'code', 'name', 'examination_options_id')->where(
                'component_types_id',
                config('constants.componentType.type')
            )->wherehas(
                'examinationOption',
                function ($query) use ($request) {
                    $query->where('id', $request->optionId);
                }
            )->with('examinationOption:id,code,name')->get();

            return $response;
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Component List Not Found');
        }
    }

    /**
     * Getting Multiple-choice Question dropdown on componentId basis
     * @param Request $request
     * @return mixed
     */
    public function getQuestionDropDown(Request $request)
    {
        try {
            $question_list = ExaminationComponentsMultiplechoice::select('question')->whereHas(
                'examinationComponent',
                function ($query) use ($request) {
                    $query->where('id', $request->compId);
                }
            )->get();

            return $question_list;
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Question No list Not Found');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subjectList(Request $request)
    {
        try {
            $data = $request->all();
            $subjectlist = ExaminationSubject::select(
                'id',
                'code',
                'name',
                'examination_id'
            )->where('examination_id', $request->examId)->with(
                [
                    'examName' => function ($query) {
                        $query->select('id', 'code', 'name');
                    },
                ]
            );
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $subjectlist;
                $total = $userCount->count();
                $subjectlist->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $subjectlist;
                $total = $userCount->count();
            }
            if (isset($request['keyword'])) {
                $subjectSearch = $subjectlist->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("code", "LIKE", "%" . $request['keyword'] . "%");
                                $query->orwhere("name", "LIKE", "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examName',
                            function ($query) use ($request) {
                                $query->where('code', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $subjectSearch = $subjectlist->get();
            return array("totalRecord" => $total, "record" => $subjectSearch);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subject list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(' Subject list Not Found');
        }
    }

    /**
     * @param Request $request
     * @param int $examsubjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function subjectView(Request $request, int $examsubjectId)
    {
        try {
            $subjectView = ExaminationSubject::select(
                'id',
                'code',
                'name',
                'examination_id',
                'education_subject_id',
                'created_user_id'
            )->where('id', $request->examsubjectId)->with(
                [
                    'educationSubject' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examName' => function ($query) {
                        $query->select('id', 'name');
                    },
                ]
            )->get();
            Log::info('Fetched view from DB', ['method' => __METHOD__, 'data' => ['subjectView' => $subjectView]]);

            return $subjectView;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subject view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(' Subject list Not Found');
        }
    }

    /**
     * @param Request $request
     * @param int $examsubjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function subjectEdit(Request $request, int $examsubjectId)
    {
        try {
            $subjecteditdetails = ExaminationSubject::select(
                'id',
                'code',
                'name',
                'examination_id',
                'education_subject_id'
            )->where('id', $request->examsubjectId)->with(
                [
                    'educationSubject' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examName' => function ($query) {
                        $query->select('id', 'name', 'academic_period_id');
                    },
                ]
            )->get();
            Log::info(
                'Fetched details from DB',
                ['method' => __METHOD__, 'data' => ['subjecteditdetails' => $subjecteditdetails]]
            );

            return $subjecteditdetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subject details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(' Subject list Not Found');
        }
    }

    /**
     * get education subject list
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationSubjectListDropdown()
    {
        try {
            $subjectlist = EducationSubject::select('id', 'code', 'name')
                ->where('visible', config('constants.visibleValue.visibleCode'))->get();
            return $subjectlist;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subject list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(' Subject list Not Found');
        }
    }

    /**
     * Update examination >> subject tab >> subject
     * @param ExamSubjectUpdationRequest $request
     * @param string $subjectId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function examinationSubjectUpdate(ExamSubjectUpdationRequest $request, string $subjectId)
    {
        try {
            $subjectupdateid = ExaminationSubject::find($subjectId);
            $subjectupdateid['code'] = $request->code;
            $subjectupdateid['name'] = $request->name;
            $subjectupdateid['examination_id'] = $request->examination_id;
            $subjectupdateid['education_subject_id'] = $request->education_subject_id;
            $subjectupdateid['modified'] = Carbon::now()->toDateTimeString();
            $subjectupdateid['modified_user_id'] = JWTAuth::user()->id;
            $subjectupdateid->save();
            $responseData =  $subjectupdateid->save();
            $responseData = [
                'code' => $subjectupdateid['code'],
                'name' => $subjectupdateid['name'],
                'examination_id' => $subjectupdateid['examination_id'],
                'education_subject_id' => $subjectupdateid['education_subject_id'],
            ];
            DB::commit();
            return $responseData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update details  into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('subject not update ');
        }
    }

    /**
     * Checking subject dependency in examination option table
     * @param string $subjectId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function checkExaminationSubjectDependency(string $subjectId)
    {
        try {
            $checkSubject = ExaminationOption::where('examination_subject_id', $subjectId)->first();

            return array("data" => $checkSubject, "subjectId" => $subjectId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to update details  into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Examination's Subject Not Removed");
        }
    }

    /**
     * @param ExamSubjectUpdationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addExaminationSubject(ExamSubjectUpdationRequest $request)
    {
        try {
            $addsubject = new ExaminationSubject();
            $addsubject['code'] = $request->code;
            $addsubject['name'] = $request->name;
            $addsubject['examination_id'] = $request->examination_id;
            $addsubject['education_subject_id'] = $request->education_subject_id;
            $addsubject['created_user_id'] = JWTAuth::user()->id;
            $addsubject['created'] = Carbon::now()->toDateTimeString();
            $storesubject = $addsubject->save();
            return $storesubject;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update details  into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination's Subject Not Added");
        }
    }
    /**
     * checking subjectId existance
     * @param string $subjectId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function checkIfSafeToDelete(int $examSubjectId)
    {
        try {
            $examinationOption = ExaminationOption::where('examination_subject_id', $examSubjectId)->exists();
            if ($examinationOption) {
                return false;
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam subject.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed To Delete Examination Subject.");
        }
    }

    /**
     * @param int $examSubjectId
     * @return bool|\Illuminate\Http\JsonResponse
     */

    public function deleteExamSubject(int $examSubjectId)
    {
        try {
            $examsubject = ExaminationSubject::find($examSubjectId);
            if ($examsubject) {
                $dltStdSub = $examsubject->examinationOption()->delete();
               // $dltmarkerSub = $examsubject->ExaminationMarkersSubject()->delete();
                return $examsubject->delete();
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("subject not deleted.");
        }
    }

    /**
     * @param ExamSubjectUpdationRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function subjectImport(ExaminationSubjectImportRequest $request)
    {
        try {
            $excelData = Excel::toArray(new ExaminationSubjectImport(), $request->file('file'));
            $i = -1;
            $validation = [];
            $add_record = [];
            $stored_data = [];
            $insert = [];
            $inserta = [];
            $insertb = [];
            $academic_period = $request['academic_period_id'];
            $examId = $request['examination_id'];
            $examsData = Examination::select(
                'id',
                'code',
                'name',
                'academic_period_id'
            )->where('academic_period_id', $academic_period)
                ->with(
                    [
                        'academicPeriod' => function ($query) {
                            $query->select('id', 'name');
                        },
                        ]
                )->first();
            foreach ($excelData[0] as $data) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }

                if (!$data[0]) { //Subject Code
                    $label = $excelData[0][1][0];
                    $errors[$label] = 'Subject Code Required';
                }

                if (!$data[1]) { //Subject Name
                    $label = $excelData[0][1][1];
                    $errors[$label] = 'Subject Name Required  ';
                }
                if (!$data[2]) { //Education Subject
                    $label = $excelData[0][1][2];
                    $errors[$label] = 'Education Subject Required  ';
                }
                if (count($errors) > 0) { //row fail
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            'Academic Period' => $examsData['academicPeriod']['name'],
                            'Examination' => $examsData['name'],
                        ],
                        'errors' => $errors,
                    ];
                } else {
                    if ($data[0]) { //Update Record
                        $subjectexist = ExaminationSubject::where('code', $data[0])->first();
                        if ($subjectexist) {
                            $subject = [];
                            $subject['code'] = $subjectexist->code;
                            $subject['name'] = $data[1];
                            $subject['education_subject_id'] = $data[2];
                            $subject['examination_id'] = $examId;
                            $subject['modified_user_id'] = JWTAuth::user()->id;
                            $subject['modified'] = Carbon::now()->toDateTimeString();
                            $examSub = ExaminationSubject::where(['code' => $subjectexist->code])->update($subject);
                            $stored_data[] = [
                                'row_number' => $i,
                                'data' => [
                                    'Academic Period' => $examsData['academicPeriod']['name'],
                                    'Examination' => $examsData['name'],
                                    'Subject code' => $data[0],
                                    'Subject name' => $data[1],
                                    'Education subject' => $data[2],
                                ],
                            ];
                        } else { // add record
                            $subject = new ExaminationSubject();
                            $subject['code'] = $data[0];
                            $subject['name'] = $data[1];
                            $subject['education_subject_id'] = $data[2];
                            $subject['examination_id'] = $examId;
                            $subject['created_user_id'] = JWTAuth::user()->id;
                            $subject['created'] = Carbon::now()->toDateTimeString();
                            $record = $subject->save();
                            $add_record[] = [
                                'row_number' => $i,
                                'data' => [
                                    'Academic Period' => $examsData['academicPeriod']['name'],
                                    'Examination' => $examsData['name'],
                                    'Subject code' => $data[0],
                                    'Subject name' => $data[1],
                                    'Education subject' => $data[2],
                                ],
                            ];
                        }
                    } else {
                        $label = $excelData[0][1][0];
                        $errors[$label] = 'Candidate ID does not exist';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                'Academic Period' => $examsData['academicPeriod']['name'],
                                'Examination' => $examsData['name'],
                                'Subject code' => $data[0],
                                'Subject name' => $data[1],
                                'Education subject' => $data[2],
                            ],
                            'errors' => $errors,
                        ];
                    }
                }
            }

            $response = [
                'total_count' => count($excelData[0]) - 2,
                'records_added' => [
                    'count' => count($add_record),
                    'rows' => $add_record,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];
            return $response;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Subject Not Imported');
        }
    }
}
