<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\AttendanceType;
use App\Models\Classification;
use App\Models\ComponentType;
use App\Models\Countrie;
use App\Models\Examination;
use App\Models\ExaminationComponent;
use App\Models\ExaminationComponentsMarkType;
use App\Models\ExaminationItem;
use App\Models\ExaminationMarker;
use App\Models\ExaminationOption;
use App\Models\ExaminationStudent;
use App\Models\ExaminationType;
use App\Models\ExamSessionType;
use App\Models\Gender;
use App\Models\Item;
use App\Models\MarkStatus;
use App\Models\MarkType;
use App\Models\ProcessingType;
use App\Models\QualificationLevel;
use App\Models\QualificationSpecialisation;
use App\Models\QualificationTitle;
use App\Models\SpecialNeedDifficulty;
use App\Models\SpecialNeedType;
use App\Models\StaffQualification;
use App\Models\StaffQualificationsSpecialisation;
use App\Models\UserSpecialNeedsAssessment;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use JWTAuth;

class AttendanceTypeRepository extends Controller
{
    /**
     * Getting Field Option List
     * @param array $data
     * @return JsonResponse
     */
    public function getListing(array $data)
    {
        try {
            if ($data['key'] == 1) {
                $listing = AttendanceType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 2) {
                $listing = Countrie::select(
                    'id',
                    'visible',
                    'default',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 3) {
                $listing = Classification::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 4) {
                $listing = ComponentType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 5) {
                $listing = ExamSessionType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 6) {
                $listing = ExaminationType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 7) {
                $listing = Gender::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 8) {
                $listing = MarkStatus::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 9) {
                $listing = ProcessingType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 10) {
                $listing = SpecialNeedType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 11) {
                $listing = SpecialNeedDifficulty::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }
            if ($data['key'] == 12) {
                $listing = QualificationLevel::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 13) {
                $listing = QualificationTitle::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'qualification_level_id'
                )->with('qualificationLevel:id,name')->orderBy('id', 'desc');
            }

            if ($data['key'] == 14) {
                $listing = QualificationSpecialisation::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'education_field_of_study_id'
                )->with('educationFieldOfStudie:id,name')->where('education_field_of_study_id', $data['studyId'])->orderBy('id', 'desc');
            }

            if ($data['key'] == 15) {
                $listing = MarkType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if ($data['key'] == 16) {
                $listing = Item::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code'
                )->orderBy('id', 'desc');
            }

            if (isset($data['start']) && isset($data['end'])) {
                $listCount = $listing;
                $total = $listCount->count();
                $listing->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $listCount = $listing;
                $total = $listCount->count();
            }

            if (isset($data['keyword'])) {
                $search = $listing->where(
                    function ($q) use ($data) {
                        $q->where(
                            function ($query) use ($data) {
                                $query->where('name', 'LIKE', "%" . $data['keyword'] . "%");
                                $query->orwhere('international_code', 'LIKE', "%" . $data['keyword'] . "%");
                                $query->orwhere('national_code', 'LIKE', "%" . $data['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $search = $listing->get();
            $data['record'] = $search;
            $data['total'] = $total;

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('List Not Found');
        }
    }

    /**
     * Getting Field Option Detail
     * @param array $data
     * @param int $optionId
     * @return JsonResponse
     */
    public function getFieldOptionDetail(array $data, int $optionId)
    {
        try {
            $result = [];

            if ($data['key'] == 1) {
                $detail = AttendanceType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 2) {
                $detail = Countrie::select(
                    'id',
                    'visible',
                    'default',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 3) {
                $detail = Classification::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 4) {
                $detail = ComponentType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 5) {
                $detail = ExamSessionType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 6) {
                $detail = ExaminationType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }


            if ($data['key'] == 7) {
                $detail = Gender::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 8) {
                $detail = MarkStatus::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 9) {
                $detail = ProcessingType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 10) {
                $detail = SpecialNeedType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if ($data['key'] == 11) {
                $detail = SpecialNeedDifficulty::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }
            if ($data['key'] == 12) {
                $detail = QualificationLevel::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }
            if ($data['key'] == 13) {
                $detail = QualificationTitle::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'qualification_level_id',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name',
                        'qualificationLevel:id,name'
                    );
            }
            if ($data['key'] == 14) {
                $detail = QualificationSpecialisation::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'education_field_of_study_id',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name',
                        'educationFieldOfStudie:id,name'
                    );
            }
            if ($data['key'] == 15) {
                $detail = MarkType::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }
            if ($data['key'] == 16) {
                $detail = Item::select(
                    'id',
                    'visible',
                    'default',
                    'editable',
                    'name',
                    'international_code',
                    'national_code',
                    'modified_user_id',
                    'modified',
                    'created_user_id',
                    'created'
                )
                    ->with(
                        'createdByUser:id,first_name,middle_name,third_name,last_name',
                        'modifiedUser:id,first_name,middle_name,third_name,last_name'
                    );
            }

            if (isset($detail)) {
                $result = $detail->where('id', $optionId)->first();
            }

            return $result;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Detail Not Found');
        }
    }

    /**
     * get db Order column
     * @param int $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLargestFieldOptionOrder(int $key = 0)
    {
        try {
            $largestOrder = 0;
            if ($key == 1) {
                $order = AttendanceType::max('order');
            } elseif ($key == 2) {
                $order = Countrie::max('order');
            } elseif ($key == 3) {
                $order = Classification::max('order');
            } elseif ($key == 4) {
                $order = ComponentType::max('order');
            } elseif ($key == 5) {
                $order = ExamSessionType::max('order');
            } elseif ($key == 6) {
                $order = ExaminationType::max('order');
            } elseif ($key == 7) {
                $order = Gender::max('order');
            } elseif ($key == 8) {
                $order = MarkStatus::max('order');
            } elseif ($key == 9) {
                $order = ProcessingType::max('order');
            } elseif ($key == 10) {
                $order = SpecialNeedType::max('order');
            } elseif ($key == 11) {
                $order = SpecialNeedDifficulty::max('order');
            } elseif ($key == 12) {
                $order = QualificationLevel::max('order');
            } elseif ($key == 13) {
                $order = QualificationTitle::max('order');
            } elseif ($key == 14) {
                $order = QualificationSpecialisation::max('order');
            } elseif ($key == 15) {
                $order = MarkType::max('order');
            } elseif ($key == 16) {
                $order = Item::max('order');
            }

            if (isset($order)) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch largest order from DB");
        }
    }

    /**
     * Storing Field Option Detail
     * @param array $data
     * @return JsonResponse
     */
    public function storeFieldOption(array $data)
    {
        DB::beginTransaction();
        try {
            $store = 0;
            $key = $data['key'];
            $order = $this->getLargestFieldOptionOrder($key);
            $insertArr['name'] = $data['name'];
            $insertArr['default'] = $data['default'];
            $insertArr['international_code'] = $data['international_code'];
            $insertArr['national_code'] = $data['national_code'];
            $insertArr['order'] = $order;
            $insertArr['visible'] = config('constants.visibleValue.visibleCode');
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();

            if ($key == 1) {
                $store = AttendanceType::insert($insertArr);
            } elseif ($key == 2) {
                $store = Countrie::insert($insertArr);
            } elseif ($key == 3) {
                $store = Classification::insert($insertArr);
            } elseif ($key == 4) {
                $store = ComponentType::insert($insertArr);
            } elseif ($key == 5) {
                $store = ExamSessionType::insert($insertArr);
            } elseif ($key == 6) {
                $store = ExaminationType::insert($insertArr);
            } elseif ($key == 7) {
                $store = Gender::insert($insertArr);
            } elseif ($key == 8) {
                $store = MarkStatus::insert($insertArr);
            } elseif ($key == 9) {
                $store = ProcessingType::insert($insertArr);
            } elseif ($key == 10) {
                $store = SpecialNeedType::insert($insertArr);
            } elseif ($key == 11) {
                $store = SpecialNeedDifficulty::insert($insertArr);
            } elseif ($key == 12) {
                $store = QualificationLevel::insert($insertArr);
            } elseif ($key == 13) {
                $insertArr['qualification_level_id'] = $data['qualification_level'];
                $store = QualificationTitle::insert($insertArr);
            } elseif ($key == 14) {
                $insertArr['education_field_of_study_id'] = $data['education_field_of_study'];
                $store = QualificationSpecialisation::insert($insertArr);
            } elseif ($key == 15) {
                $store = MarkType::insert($insertArr);
            } elseif ($key == 16) {
                $store = Item::insert($insertArr);
            }

            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to store field option in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse('Failed to store field option in DB');
        }
    }

    /**
     * Updating Field Option Detail
     * @param array $data
     * @return JsonResponse
     */
    public function updateFieldOption(array $data)
    {
        DB::beginTransaction();
        try {
            $update = 0;
            $key = $data['key'];

            $updateArr['name'] = $data['name'];
            $updateArr['default'] = $data['default'];
            $updateArr['international_code'] = $data['international_code'];
            $updateArr['national_code'] = $data['national_code'];
            $updateArr['visible'] = $data['visible'];
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            $updateArr['modified'] = Carbon::now()->toDateTimeString();
            if ($key == 1) {
                $update = AttendanceType::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 2) {
                $update = Countrie::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 3) {
                $update = Classification::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 4) {
                $update = ComponentType::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 5) {
                $update = ExamSessionType::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 6) {
                $update = ExaminationType::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 7) {
                $update = Gender::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 8) {
                $update = MarkStatus::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 9) {
                $update = ProcessingType::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 10) {
                $update = SpecialNeedType::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 11) {
                $update = SpecialNeedDifficulty::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 12) {
                $update = QualificationLevel::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 13) {
                $updateArr['qualification_level_id'] = isset($data['qualification_level']) ? $data['qualification_level'] : null;
                $update = QualificationTitle::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 14) {
                $updateArr['education_field_of_study_id'] = $data['education_field_of_study'];
                $update = QualificationSpecialisation::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 15) {
                $update = MarkType::where('id', $data['id'])->update($updateArr);
            } elseif ($key == 16) {
                $update = Item::where('id', $data['id'])->update($updateArr);
            }

            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
            return $update;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update field option in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse('Failed to update field option in DB');
        }
    }

    /**
     * check delete
     * @param array $data
     * @return bool
     */
    public function checkIfSafeToDelete(array $data)
    {
        try {
            $key = $data['key'];
            $id = $data['id'];
            if ($key == 1 && $id) {
                $attId = AttendanceType::find($id);
                $examStudent = ExaminationStudent::where('examination_attendance_types_id', $attId->id)->exists();
                if ($examStudent) {
                    return false;
                }

                $exam = Examination::where('attendance_type_id', $attId->id)->exists();
                if ($exam) {
                    return false;
                }
                $examOption = ExaminationOption::where('attendance_type_id', $attId->id)->exists();
                if ($examOption) {
                    return false;
                }
            }
            if ($key == 2 && $id) {
                $cntryId = Countrie::find($id);
                $stffQulft = StaffQualification::where('qualification_country_id', $cntryId->id)->exists();
                if ($stffQulft) {
                    return false;
                }
            }
            if ($key == 3 && $id) {
                $clssfctnId = Classification::find($id);
                $examMarker = ExaminationMarker::where('classification', $clssfctnId->id)->exists();
                if ($examMarker) {
                    return false;
                }
            }
            if ($key == 4 && $id) {
                $componentId = ComponentType::find($id);
                $examComponent = ExaminationComponent::where('component_types_id', $componentId->id)->exists();
                if ($examComponent) {
                    return false;
                }
            }
            if ($key == 5 && $id) {
                $examSession = ExamSessionType::find($id);
                $exam = Examination::where('exam_session_types_id', $examSession->id)->exists();
                if ($exam) {
                    return false;
                }
            }
            if ($key == 6 && $id) {
                $examType = ExaminationType::find($id);
                $exams = Examination::where('examination_types_id', $examType->id)->exists();
                if ($exams) {
                    return false;
                }
            }

            if ($key == 7 && $id) {
                $gender = Gender::find($id);
                if ($gender) {
                    $securityUsers = $gender->securityUsers()->exists();
                    if ($securityUsers) {
                        return false;
                    }
                }
                return true;
            }

            if ($key == 8 && $id) {
                $markStatus = MarkStatus::find($id);
                if ($markStatus) {
                    $marks = $markStatus->examinationStudentComponentMarks()->exists();
                    if ($marks) {
                        return false;
                    }
                }
                return true;
            }

            if ($key == 9 && $id) {
                return true;
            }

            if ($key == 10 && $id) {
                $specialNeedType = SpecialNeedType::find($id);
                if ($specialNeedType) {
                    $centre = $specialNeedType->examCentre()->exists();
                    if ($centre) {
                        return false;
                    }

                    $usedSpecialNeed = UserSpecialNeedsAssessment::where('special_need_type_id', $id)->exists();
                    if ($usedSpecialNeed) {
                        return false;
                    }
                }
                return true;
            }

            if ($key == 11 && $id) {
                $specialNeedDifficulty = SpecialNeedDifficulty::find($id);
                if ($specialNeedDifficulty) {
                    $usedSpecialNeed = UserSpecialNeedsAssessment::where('special_need_difficulty_id', $id)->exists();
                    if ($usedSpecialNeed) {
                        return false;
                    }
                }
                return true;
            }
            if ($key == 12 && $id) {
                $qualificationlLevel = QualificationLevel::find($id);
                if ($qualificationlLevel) {
                    $level = QualificationTitle::where('qualification_level_id', $id)->exists();
                    if ($level) {
                        return false;
                    }
                }
                return true;
            }
            if ($key == 13 && $id) {
                $qualificationlTitle = QualificationTitle::find($id);
                if ($qualificationlTitle) {
                    $title = StaffQualification::where('qualification_title_id', $id)->exists();
                    if ($title) {
                        return false;
                    }
                }
                return true;
            }
            if ($key == 14 && $id) {
                $qualificationSpecialisation = QualificationSpecialisation::find($id);
                if ($qualificationSpecialisation) {
                    $staffQualifications = StaffQualificationsSpecialisation::where('qualification_specialisation_id', $id)->exists();
                    if ($staffQualifications) {
                        return false;
                    }
                }
                return true;
            }
            if ($key == 15 && $id) {
                $markType = MarkType::find($id);
                if ($markType) {
                    $type = ExaminationComponentsMarkType::where('mark_types_id', $id)->exists();
                    if ($type) {
                        return false;
                    }
                }
                return true;
            }
            if ($key == 16 && $id) {
                $item = Item::find($id);
                if ($item) {
                    $examItems = ExaminationItem::where('items_id', $id)->exists();
                    if ($examItems) {
                        return false;
                    }
                }
                return true;
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }

    /**
     * @param array $data
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function deleteFieldOption(array $data)
    {
        try {
            $key = $data['key'];
            $id = $data['id'];
            if ($key == 1 && $id) {
                $dltAttnd = AttendanceType::where('id', $id)->delete();
                return $dltAttnd;
            }
            if ($key == 2 && $id) {
                $dltCountry = Countrie::where('id', $id)->delete();
                return $dltCountry;
            }
            if ($key == 3 && $id) {
                $dltClssfctn = Classification::where('id', $id)->delete();
                return $dltClssfctn;
            }
            if ($key == 4 && $id) {
                $dltCmptype = ComponentType::where('id', $id)->delete();
                return $dltCmptype;
            }
            if ($key == 5 && $id) {
                $dltExamSession = ExamSessionType::where('id', $id)->delete();
                return $dltExamSession;
            }
            if ($key == 6 && $id) {
                $dltExamType = ExaminationType::where('id', $id)->delete();
                return $dltExamType;
            }
            if ($key == 7 && $id) {
                return Gender::where('id', $id)->delete();
            }
            if ($key == 8 && $id) {
                return MarkStatus::where('id', $id)->delete();
            }
            if ($key == 9 && $id) {
                return ProcessingType::where('id', $id)->delete();
            }
            if ($key == 10 && $id) {
                return SpecialNeedType::where('id', $id)->delete();
            }
            if ($key == 11 && $id) {
                return SpecialNeedDifficulty::where('id', $id)->delete();
            }
            if ($key == 12 && $id) {
                return QualificationLevel::where('id', $id)->delete();
            }
            if ($key == 13 && $id) {
                return QualificationTitle::where('id', $id)->delete();
            }
            if ($key == 14 && $id) {
                return QualificationSpecialisation::where('id', $id)->delete();
            }
            if ($key == 15 && $id) {
                return MarkType::where('id', $id)->delete();
            }
            if ($key == 16 && $id) {
                return Item::where('id', $id)->delete();
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Record not deleted.");
        }
    }
}
