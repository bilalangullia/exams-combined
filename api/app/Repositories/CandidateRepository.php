<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\ExaminationComponentsMultiplechoice;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsForecastGrade;
use App\Models\ExaminationStudentsMultiplechoiceResponse;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationStudentsComponentsMark;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;

class CandidateRepository extends Controller
{
    /**
     * Multiple-choice Candidate Listing
     * @param Request $request
     * @return mixed
     */
    public function multiChoicesCandidateList(Request $request)
    {
        try {
            $optionId = $request->optId;
            $data = ExaminationStudent::with(
                'securityUser:id,first_name,middle_name,third_name,last_name',
                'examinationStudentsOption:examination_students_id,examination_options_id',
                'examinationStudentsOption.examinationOption.examinationStudentsForecastGrade.examinationGradingOption')
                ->whereHas(
                    'examinationCentre',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_centre_id);
                    }
                )->whereHas(
                    'examName',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_id);
                    }
                )->whereHas(
                    'examName.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('id', $request->academic_period_id);
                    }
                );

             if (isset($request['options_id'])) {
                $forecast_grade = $data->whereHas(
                    'examinationStudentsOption.examinationOption',
                    function ($query) use ($request) {
                        $query->where('id', $request->options_id);
                    }
                );
            }

            if (isset($request['compId'])) {
                        $componentBasedCandidate = $data->whereHas(
                            'examinationStudentsOption.examinationOption.examinationComponent',
                            function ($query) use ($request) {
                                $query->where(
                                    'component_types_id',
                                    config('constants.componentType.type')
                                )->where('id', $request->compId);
                            }
                        );
                    }

            if (isset($request['keyword'])) {
                $candidateSearch = $data->where(function ($q) use ($request){
                    $q->where(
                        function ($query) use ($request) {
                            $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    )->orWhereHas(
                        'securityUser',
                        function ($query) use ($request) {
                            $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('middle_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('third_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $candidateSearch = $data->get();

            return $candidateSearch;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }

    /**
     * Multiple-choice-Candidate-view
     * @param string $candidId
     * @return mixed
     */
    public function getMultiChoiceCandidateDetails(Request $request, string $candidId)
    {
        try {
            $multiChoiceView = ExaminationStudent::select(
                'id',
                'candidate_id',
                'examination_centre_id',
                'examination_id',
                'student_id',
                'modified_user_id',
                'modified',
                'created_user_id',
                'created'
            )
                ->with(
                    'examName:id,code,name,academic_period_id',
                    'examinationCentre:id,code,name',
                    'examName.academicPeriod:id,name',
                    'securityUser:id,first_name,middle_name,third_name,last_name',
                    'createdByUser:id,first_name,middle_name,third_name,last_name',
                    'examinationStudentsMultiplechoiceResponse:id,response,question,examination_students_id',
                    'examinationStudentsForecastGrade:examination_students_id,grade,examination_options_id',
                    'examinationStudentsForecastGrade.examinationGradingOption:id,name'
                )->where('candidate_id', $candidId)->get();

             return $multiChoiceView;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate View Not Found');
        }
    }

    /**
     * Getting candidate forecast grade
     * @param Request $request
     * @param string $candidId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCandidateForecastGrade(Request $request, string $candidId)
    {
        try {
            $grade = ExaminationStudentsForecastGrade::select('grade')->
            whereHas(
                'examinationStudent',
                function ($query) use ($candidId) {
                    $query->where('candidate_id', $candidId);
                }
            )->whereHas(
                'examinationOption',
                function ($query) use ($request) {
                    $query->where('id', $request->options_id);
                }
            )->with('examinationGradingOption:id,name')->get();

            return $grade;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate Forecast Grade Not Found');
        }
    }

    /**
     * Getting candidate response
     * @param string $candidId
     * @return mixed
     */
    public function getCandidateResponse(Request $request, string $candidId)
    {
        try {
            $responseList = ExaminationStudentsMultiplechoiceResponse::select(
                'id',
                'response',
                'question',
                'examination_students_id'
            )->whereHas(
                'examinationStudent',
                function ($query) use ($candidId) {
                    $query->where('candidate_id', $candidId);
                }
            )->whereHas(
                'examinationComponent',
                function ($query) use ($request) {
                    $query->where('id', $request->compId);
                }
            )->get();

            return $responseList;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return array();
        }
    }

    /**
     * Getting Candidate's existing response
     * @param Request $request
     * @param string $candidId
     */
    public function getCandidateExistResponse(Request $request, string $candidId)
    {
        try {
            $candId = ExaminationStudent::where('candidate_id', $candidId)->first();
            $cand_id = $candId->id;

            return $cand_id;
        } catch (Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Record Not Saved');
        }
    }

    /**
     * Getting one candidate multiple-choice Response
     * @param Request $request
     * @param string $candId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getCandidateQuestionResponse(Request $request, string $candId)
    {
        try {
            $candidateId = ExaminationStudent::where('candidate_id', $candId)->first();
            $questionList = ExaminationComponentsMultiplechoice::select(
                'answer',
                'question',
                'examination_components_id'
            )->where('examination_components_id', $request->compID)->get();

            $answerList = ExaminationStudentsMultiplechoiceResponse::select(
                'response',
                'question',
                'examination_students_id',
                'examination_components_id'
            )->where('examination_students_id', $candidateId->id)->where(
                'examination_components_id',
                $request->compID
            )->get();

            return array("questionList" => $questionList, "candAns" => $answerList, "candidateId" => $candidateId);
        } catch (Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate Record Not Found');
        }
    }


     public function getAllCandidateQuestionResponse(Request $request)
    {
        try {
            $generate_btn = $this->multiChoicesList($request);
            if ($generate_btn) {
              $candId = $generate_btn->pluck('id');
            }
            $quesList = ExaminationComponentsMultiplechoice::select(
                'answer',
                'question',
                'examination_components_id'
            )->where('examination_components_id', $request->compId)->get();

            $ansList = ExaminationStudentsMultiplechoiceResponse::select(
                'response',
                'question',
                'examination_students_id',
                'examination_components_id'
            )->whereIn('examination_students_id', $candId)->where(
                'examination_components_id',
                $request->compId
            )->get();

            return array("questionList" => $quesList, "candAns" => $ansList);
        } catch (Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return array();
        }
    }

    /**
     * getting candidate final grades
     * @param string $candidId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCandidateFinalGrade(string $candidId)
    {
        try {
            $finalGrade = ExaminationStudentsOption::select(
                'examination_students_id',
                'examination_options_id'
            )->whereHas(
                'examinationStudent',
                function ($query) use ($candidId) {
                    $query->where('candidate_id', $candidId)->with('examinationStudentsComponentsMark');
                }
            )->with(
                'examinationOption:id,code,name',
                'examinationOption.examinationComponent.examinationStudentsComponentsMark',
                'examinationOption.examinationComponent.examinationGradingType.examinationGradingOption'
            )->get();

            return $finalGrade;
        } catch (Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate Record Not Found');
        }
    }

    public function calculateCandidateAggregateScore(string $candidId)
    {
        try{
            $getScore = ExaminationStudent::where('candidate_id', $candidId)->with('examinationStudentsOption.examinationOption.examinationGradingType.examinationGradingOption')->get();

            return $getScore;
        } catch (Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate Record Not Found');
        }
    }

    public function multiChoicesList(Request $request)
    {
        try {
            $dataList = ExaminationStudent::with(
                'securityUser:id,first_name,middle_name,third_name,last_name',
                'examinationStudentsOption:examination_students_id,examination_options_id',
                'examinationStudentsOption.examinationOption.examinationStudentsForecastGrade.examinationGradingOption')
                ->whereHas(
                    'examinationCentre',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_centre_id);
                    }
                )->whereHas(
                    'examName',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_id);
                    }
                )->whereHas(
                    'examName.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('id', $request->academic_period_id);
                    }
                )->whereHas(
                    'examinationStudentsOption.examinationOption',
                    function ($query) use ($request) {
                        $query->where('id', $request->options_id);
                    }
                )->whereHas(
                            'examinationStudentsOption.examinationOption.examinationComponent',
                            function ($query) use ($request) {
                                $query->where(
                                    'component_types_id',
                                    config('constants.componentType.type')
                                )->where('id', $request->compId);
                            }
                        );

               if (isset($request['keyword'])) {
                $candidateSearch = $dataList->where(function ($q) use ($request){
                    $q->where(
                        function ($query) use ($request) {
                            $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    )->orWhereHas(
                        'securityUser',
                        function ($query) use ($request) {
                            $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('middle_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('third_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $candidateSearch = $dataList->get();

            return $candidateSearch;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }
}
