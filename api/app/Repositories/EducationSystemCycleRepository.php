<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\EducationStructurAddCycleRequest;
use App\Models\EducationCycle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class EducationSystemCycleRepository extends Controller
{
    /**
     * @param Request $request
     * @param string $educationlevelId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function cycleList(Request $request, string $educationlevelId)
    {
        try {
            $list = EducationCycle::with('educationLevel:id,name')
                ->where('education_level_id', $educationlevelId);
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $list;
                $total = $userCount->count();
                $list->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $list;
                $total = $userCount->count();
            }

            if (isset($request['keyword'])) {
                $Search = $list->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('admission_age', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'educationLevel',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $filter = $list->get();
            return array("totalRecord" => $total, "record" => $filter);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle List Not Found");
        }
    }

    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cyclesView(string $eduCycleId)
    {
        try {
            $cycleview = EducationCycle::where('id', $eduCycleId)->with('educationLevel:id,name')->get();
            return $cycleview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle View Not Found");
        }
    }

    /**
     * add new cycle
     * @param EducationStructurAddCycleRequest $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function educationCycleAdd(EducationStructurAddCycleRequest $request)
    {
        try {
            $cycleAdd = new EducationCycle();
            $cycleAdd['name'] = $request->name;
            $cycleAdd['admission_age'] = $request->admission_age;
            $cycleAdd['education_level_id'] = $request->education_level_id;
            $cycleAdd['visible'] = config('constants.visibleValue.visibleCode');
            $cycleAdd['order'] = config('constants.orderValue.value');
            $cycleAdd['created_user_id'] = JWTAuth::user()->id;
            $cycleAdd['created'] = Carbon::now()->toDateTimeString();
            $recordcycle = $cycleAdd->save();
            return $recordcycle;
        } catch (\Exception $e) {
            Log::error(
                'Failed to Add cycle into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Cycle Not Added");
        }
    }

    /**
     * @param EducationStructurAddCycleRequest $request
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationCycleUpdate(EducationStructurAddCycleRequest $request, string $eduCycleId)
    {
        try {
            $valueUpdate = EducationCycle::find($eduCycleId);
            $valueUpdate['name'] = $request->name;
            $valueUpdate['admission_age'] = $request->admission_age;
            $valueUpdate['education_level_id'] = $request->education_level_id;
            $valueUpdate['visible'] = $request->visible;
            $valueUpdate['order'] = config('constants.orderValue.value');
            $valueUpdate['modified'] = Carbon::now()->toDateTimeString();
            $valueUpdate['modified_user_id'] = JWTAuth::user()->id;
            $updatecycle = $valueUpdate->save();
            return $updatecycle;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update cycle into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Cycle Not Updated");
        }
    }
}
