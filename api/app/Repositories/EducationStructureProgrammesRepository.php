<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\EducationStruProgrammeAddRequest;
use App\Http\Requests\EducationStruProgrammeUpdateRequest;
use App\Models\EducationProgramme;
use App\Models\EducationProgrammesNextProgramme;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class EducationStructureProgrammesRepository extends Controller
{
    /**
     * @param Request $request
     * @param string $educationlevelId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function programmesList(Request $request, string $educationCycleId)
    {
        try {
            $list = EducationProgramme::where('education_cycle_id', $educationCycleId)->with(
                'educationFieldOfStudie:id,name',
                'educationCycle:id,name',
                'educationCertification:id,name'
            );
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $list;
                $total = $userCount->count();
                $list->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $list;
                $total = $userCount->count();
            }

            if (isset($request['keyword'])) {
                $Search = $list->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where('code', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orWhere('duration', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'educationFieldOfStudie',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'educationCycle',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'educationCertification',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $filter = $list->get();
            return array("totalRecord" => $total, "record" => $filter);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle List Not Found");
        }
    }

    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function programmesView(string $programmeId)
    {
        try {
            $cycleview = EducationProgramme::where('id', $programmeId)->with(
                'educationFieldOfStudie:id,name',
                'educationCycle:id,name',
                'educationCertification:id,name'
            )->get();
            return $cycleview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Programme View Not Found");
        }
    }

    /**
     * add new programme
     * @param EducationStruProgrammeAddRequest $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function educationProgrammesAdd(EducationStruProgrammeAddRequest $request)
    {
        try {
            $progAdd = new EducationProgramme();
            $progAdd['name'] = $request->name;
            $progAdd['code'] = $request->code;
            $progAdd['duration'] = $request->duration;
            $progAdd['visible'] = config('constants.visibleValue.visibleCode');
            $progAdd['order'] = config('constants.orderValue.value');
            $progAdd['education_field_of_study_id'] = $request->education_field_of_study_id;
            $progAdd['education_cycle_id'] = $request->education_cycle_id;
            $progAdd['education_certification_id'] = $request->education_certification_id;
            $progAdd['created_user_id'] = JWTAuth::user()->id;
            $progAdd['created'] = Carbon::now()->toDateTimeString();
            $recordprog = $progAdd->save();
            return $recordprog;
        } catch (\Exception $e) {
            Log::error(
                'Failed to Add cycle into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Programme Not Added");
        }
    }

    /**
     * @param EducationStructurAddCycleRequest $request
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationCycleUpdate(EducationStruProgrammeUpdateRequest $request, string $programmeId)
    {
        DB::beginTransaction();
        try {
            $valueUpdate = EducationProgramme::find($programmeId);
            $valueUpdate['name'] = $request->name;
            $valueUpdate['code'] = $request->code;
            $valueUpdate['visible'] = $request->visible;
            $valueUpdate['order'] = config('constants.orderValue.value');
            $valueUpdate['education_field_of_study_id'] = $request->education_field_of_study_id;
            $valueUpdate['education_cycle_id'] = $request->education_cycle_id;
            $valueUpdate['education_certification_id'] = $request->education_certification_id;
            $valueUpdate['modified'] = Carbon::now()->toDateTimeString();
            $valueUpdate['modified_user_id'] = JWTAuth::user()->id;
            $updateprogramme = $valueUpdate->save();
            $cycleprogId = $valueUpdate->id;
            if ($updateprogramme && $request->next_programme && count($request->next_programme)) {
                $dataprog = EducationProgrammesNextProgramme::where('education_programme_id', $cycleprogId)->delete();
                if (count($request->next_programme)) {
                    $nextProgdata = [];
                    foreach ($request->next_programme as $nextProgramme) {
                        if (!empty($nextProgramme)) {
                            $nextProg = [];
                            $nextProg['id'] = Str::uuid();
                            $nextProg['education_programme_id'] = $cycleprogId;
                            $nextProg['next_programme_id'] = $nextProgramme['id'];
                            $nextProg['modified'] = Carbon::now()->toDateTimeString();
                            $valueUpdate['modified_user_id'] = JWTAuth::user()->id;
                            $nextProgdata[] = $nextProg;
                        }
                    }
                    EducationProgrammesNextProgramme::insert($nextProgdata);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to update Qualification  from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Education Programme Not Updated successfully');
        }
    }

    /**
     * get next programe dropdown exclude current one.
     * @return \Illuminate\Http\JsonResponse
     */
    public function nextProgrammeDropdowm(string $programmeId)
    {
        try {
            $nextProg = EducationProgramme::select('id', 'name')->where('id', '!=', $programmeId)->get();
            return $nextProg;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch dropdown from DB");
        }
    }

    /**
     * get next programme dropdown exclude current one.
     * @return \Illuminate\Http\JsonResponse
     */
    public function nextCycleProgramme(string $nextProgId)
    {
        try {
            $nextProgField = EducationProgramme::select('id', 'name', 'education_cycle_id')->where('id', $nextProgId)
                ->with('educationCycle:id,name')->get()->map(
                    function ($item, $key) {
                        return [
                            "id" => $item['id'],
                            "cycle_programme" => ($item['educationCycle']['name'] . '-' . $item['name']),
                        ];
                    }
                );
            return $nextProgField;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch Next Cycle Programme  from DB");
        }
    }
}
