<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\CollectionImportRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationStudentsCertificate;
use App\Models\ExaminationStudentsCertificatesStatuse;
use App\Models\ExaminationStudent;
use App\Models\Examination;
use Carbon\Carbon;
use App\Imports\CollectionImport;
use JWTAuth;
use Excel;
use File;
use DB;

class CollectionRepository extends Controller
{

    public function getCollectionList(Request $request)
    {
        try{
            $collection = ExaminationStudentsCertificatesStatuse::select('id', 'examination_students_certificates_id', 'method', 'date_received', 'date_sent')
                ->where('type', config('constants.certificatetype.initial'))
                ->whereHas(
                    'examinationStudentsCertificate.examinationStudent.examName',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_id);
                    }
                )
                ->whereHas(
                    'examinationStudentsCertificate.examinationStudent.examName.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('id', $request->academic_period_id);
                    }
                )
                ->with([
                           'examinationStudentsCertificate' =>
                               function ($query) {
                                   $query->select('id', 'certificate_id','examination_students_id');
                               },
                           'examinationStudentsCertificate.examinationStudent' =>
                               function ($query) {
                                   $query->select('id', 'candidate_id','examination_id','student_id');
                               },
                           'examinationStudentsCertificate.examinationStudent.securityUser' =>
                               function ($query) {
                                   $query->select('id', 'first_name','middle_name','third_name', 'last_name');
                               },
                           'examinationStudentsCertificate.examinationStudent.examName' =>
                               function ($query)  {
                                   $query->select('id', 'code','name');
                               }
                       ]

                )->orderBy('id', 'desc');


            if (isset($request['start']) && isset($request['end'])) {
                $collectionCount = $collection;
                $total           = $collectionCount->count();
                $collection->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $collectionCount = $collection;
                $total = $collectionCount->count();
            }

            if (isset($request['keyword'])) {
                $search = $collection->where(function ($q) use ($request){
                    $q->whereHas(
                        'examinationStudentsCertificate.examinationStudent',
                        function ($query) use ($request) {
                            $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    )->orWhereHas(
                        'examinationStudentsCertificate.examinationStudent.examName',
                        function ($query) use ($request) {
                            $query->where('code', 'LIKE', "%" . $request['keyword'] . "%")
                                ->orwhere('name', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    )->orWhereHas(
                        'examinationStudentsCertificate.examinationStudent.securityUser',
                        function ($query) use ($request) {
                            $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%")
                                ->orWhere('middle_name', 'LIKE', "%" . $request['keyword'] . "%")
                                ->orWhere('third_name', 'LIKE', "%" . $request['keyword'] . "%")
                                ->orWhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    );
                }
                );
            }
            $search = $collection->get();

            return array("record" => $search, "totalRecord" => $total);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection List Not Found');
        }
    }


    public function getCollectionDetail(int $collectionId)
    {
        try{
            $detail = ExaminationStudentsCertificatesStatuse::where('id', $collectionId)
                ->select('id', 'examination_students_certificates_id', 'method', 'date_received', 'date_sent', 'postal', 'comments', 'modified_user_id', 'modified', 'created_user_id', 'created')
                ->with([
                           'examinationStudentsCertificate.examinationStudent' =>
                               function ($query) {
                                   $query->select('id', 'candidate_id','examination_id','student_id');
                               },
                           'examinationStudentsCertificate.examinationStudent.securityUser' =>
                               function ($query) {
                                   $query->select('id', 'first_name', 'last_name', 'gender_id', 'date_of_birth');
                               },
                           'examinationStudentsCertificate.examinationStudent.securityUser.gender' =>
                               function ($query) {
                                   $query->select('id', 'name');
                               },
                           'examinationStudentsCertificate.examinationStudent.examName' =>
                               function ($query)  {
                                   $query->select('id', 'code','name');
                               }
                       ]
                )->get();

            return $detail;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection Detail Not Found');
        }
    }

    public function addCollection(array $data)
    {
        try{
            $candidateId = ExaminationStudent::where('candidate_id', $data['candidate_id'])->first()->id;
            $certificate = ExaminationStudentsCertificate::where('examination_students_id', $candidateId)->first()->id;
            $add = new ExaminationStudentsCertificatesStatuse();
            $exist = ExaminationStudentsCertificatesStatuse::where('examination_students_certificates_id', $data['examination_students_certificates_id'])->where('type', config('constants.certificatetype.initial'))->first();
            if($exist){
                return 1;
            } else {
                $add['type'] = config('constants.certificatetype.initial');
                $add['examination_students_certificates_id'] = $data['examination_students_certificates_id'];
                $add['method'] = $data['method'];
                $add['date_received'] = $data['date_received'];
                $add['date_sent'] = isset($data['date_sent']) ? $data['date_sent'] : null;
                $add['postal'] = isset($data['postal']) ? $data['postal'] : null;
                $add['comments'] = isset($data['comments']) ? $data['comments'] : null;
                $add['created_user_id'] = JWTAuth::user()->id;
                $add['created'] = Carbon::now()->toDateTimeString();
                $store = $add->save();
                return 2;
            }
        }  catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection Not Added');
        }
    }

    public function updateCollection(int $collectionId, array $updateData)
    {
        try{
            $candId = ExaminationStudent::where('candidate_id', $updateData['candidate_id'])->first()->id;
            $cert_id = ExaminationStudentsCertificate::where('examination_students_id', $candId)->first()->id;
            $update = ExaminationStudentsCertificatesStatuse::find($collectionId);
            $update['examination_students_certificates_id'] = $cert_id;
            $update['type'] = config('constants.certificatetype.initial');
            $update['method'] = $updateData['method'];
            $update['date_received'] = $updateData['date_received'];
            $update['date_sent'] = isset($updateData['date_sent']) ? $updateData['date_sent'] : null;
            $update['postal'] = isset($updateData['postal']) ? $updateData['postal'] : null;
            $update['comments'] = isset($updateData['comments']) ? $updateData['comments'] : null;
            $update['modified_user_id'] = JWTAuth::user()->id;
            $update['modified'] = Carbon::now()->toDateTimeString();
            $updCllt = $update->save();

            return $updCllt;
        }  catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection Not Added');
        }
    }

    public function collectionImport(CollectionImportRequest $request)
    {
        try{
            $excelData = Excel::toArray(new CollectionImport(), $request->file('file'));

            $i = -1;
            $validation = [];
            $add_record = [];
            $stored_data = [];
            $insert = [];
            $inserta = [];
            $insertb = [];
            $examId = $request['examination_id'];
            $examDetails = Examination::where('id', $request['examination_id'])->with('academicPeriod')->first();

            foreach ($excelData[0] as $data) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }

                if (!$data[0]) { //Candidate Id
                    $label = $excelData[0][1][0];
                    $errors[$label] = 'Candidate Id Required';
                }

                if (!$data[1]) { //Date Received
                    $label = $excelData[0][1][1];
                    $errors[$label] = 'Date Received Required';
                }
                if (!$data[2]) { //Method
                    $label = $excelData[0][1][2];
                    $errors[$label] = 'Method Required';
                }
                if (count($errors)) {
                    //row fail
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            "Academic Period" => $examDetails["academicPeriod"]["name"],
                            "Examination" => $examDetails['full_name'],
                            "Candidate Id" => $data[0],
                            "Date Received" => Carbon::parse($data[1])->format('Y-m-d'),
                            "Method" => $data[2],
                        ],
                        'errors' => $errors,
                    ];
                } else {
                    if ($data[0]) {
                        $candidateId = ExaminationStudent::where('candidate_id', $data[0])->with('securityUser', 'examinationStudentsCertificate')->first();
                        $candId = $candidateId->id;
                        $certificate = ExaminationStudentsCertificate::where('examination_students_id', $candId)->first()->id;

                        $t = ExaminationStudentsCertificatesStatuse::where('examination_students_certificates_id', $certificate)->first();
                        if ($t) {
                            $user = [];
                            $user['examination_students_certificates_id'] = $certificate;
                            $user['type'] = config('constants.certificatetype.initial');
                            $user['method'] = $data[2];
                            $user['date_received'] = Carbon::parse($data[1])->format('Y-m-d');
                            $user['date_sent'] = ($data[3]) ? $data[3] : null;
                            $user['postal'] = ($data[4]) ? $data[4] : null;
                            $user['comments'] = ($data[5]) ? $data[5] : null;
                            $user['modified_user_id'] = JWTAuth::user()->id;
                            $user['modified'] = Carbon::now()->toDateTimeString();
                            $secUserRec = ExaminationStudentsCertificatesStatuse::where(['examination_students_certificates_id' => $certificate])->update($user);
                            if($data[2] == 1) {
                                $method = "Collect";
                            } else {
                                $method = "Post";
                            }
                            $stored_data[] = [
                                'row_number' => $i,
                                'data' => [
                                    "Academic Period" => $examDetails["academicPeriod"]["name"],
                                    "Examination" => $examDetails['full_name'],
                                    "Candidate Id" => $data[0],
                                    "Candidate Name" => $candidateId['securityUser']['full_name'],
                                    "Certificate Number" => $candidateId['examinationStudentsCertificate']['certificate_id'],
                                    "Date Received" => Carbon::parse($data[1])->format('Y-m-d'),
                                    "Method" => $method,
                                    "Date Sent" => $data[3],
                                ],
                            ];
                        } else {
                            $candidateId = ExaminationStudent::where('candidate_id', $data[0])->with('securityUser', 'examinationStudentsCertificate')->first();
                            $candId = $candidateId->id;
                            $certificate = ExaminationStudentsCertificate::where('examination_students_id', $candId)->first()->id;

                            $user = new ExaminationStudentsCertificatesStatuse();
                            $user['examination_students_certificates_id'] = $certificate;
                            $user['type'] = config('constants.certificatetype.initial');
                            $user['method'] = $data[2];
                            $user['date_received'] = Carbon::parse($data[1])->format('Y-m-d');
                            $user['date_sent'] = ($data[3]) ? $data[3] : null;
                            $user['postal'] = ($data[4]) ? $data[4] : null;
                            $user['comments'] = ($data[5]) ? $data[5] : null;
                            $user['created_user_id'] = JWTAuth::user()->id;
                            $user['created'] = Carbon::now()->toDateTimeString();
                            $record = $user->save();
                            if($data[2] == 1) {
                                $method = "Collect";
                            } else {
                                $method = "Post";
                            }
                            $add_record[] = [
                                'row_number' => $i,
                                'data' => [
                                    "Academic Period" => $examDetails["academicPeriod"]["name"],
                                    "Examination" => $examDetails['full_name'],
                                    "Candidate Id" => $data[0],
                                    "Candidate Name" => $candidateId['securityUser']['full_name'],
                                    "Certificate Number" => $candidateId['examinationStudentsCertificate']['certificate_id'],
                                    "Date Received" => Carbon::parse($data[1])->format('Y-m-d'),
                                    "Method" => $method,
                                    "Date Sent" => $data[3],
                                ],
                            ];
                        }
                    } else {
                        $label = $excelData[0][1][0];
                        $errors[$label] = 'Candidate ID does not exist';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                "Academic Period" => $examDetails["academicPeriod"]["name"],
                                "Examination" => $examDetails['full_name'],
                                "Candidate Id" => $data[0],
                                "Candidate Name" => $candidateId['securityUser']['full_name'],
                                "Certificate Number" => $candidateId['examinationStudentsCertificate']['certificate_id'],
                                "Date Received" => $data[1],
                                "Method" => $data[2],
                                "Date Sent" => $data[3]
                            ],
                            'errors' => $errors,
                        ];
                    }
                }
            }
            $response = [
                'total_count' => count($excelData[0]) - 2,
                'records_added' => [
                    'count' => count($add_record),
                    'rows' => $add_record,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];

            return $response;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection Not Imported');
        }
    }
}
