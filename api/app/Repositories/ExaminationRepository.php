<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddExaminationRequest;
use App\Http\Requests\AdministratorExaminationUpdateRequest;
use App\Models\EducationGrade;
use App\Models\Examination;
use App\Models\ExaminationStudent;
use App\Models\ExaminationMarker;
use App\Models\ExaminationCentre;
use App\Models\ExaminationSubject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use JWTAuth;

class ExaminationRepository extends Controller
{
    /**
     * Getting Examination exam tab list
     * @param Request $request
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function getExaminationExamList(Request $request)
    {
        try {
            $examList = Examination::select(
                'id',
                'code',
                'name',
                'academic_period_id',
                'registration_start_date',
                'registration_end_date',
                'education_grade_id',
                'exam_session_types_id',
                'examination_types_id'
            )->with(
                'academicPeriod:id,name',
                'educationGrade:id,code,name',
                'examSessionType:id,name',
                'examinationType:id,name'
            )->orderBy(
                'academic_period_id',
                'desc'
            );

            if (isset($request['start']) && isset($request['end'])) {
                $listCount = $examList;
                $total = $listCount->count();
                $examList->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $listCount = $examList;
                $total = $listCount->count();
            }

            if (isset($request['keyword'])) {
                $examSearch = $examList->where("code", "LIKE", "%" . $request['keyword'] . "%")->orWhere(
                    "name",
                    "LIKE",
                    "%" . $request['keyword'] . "%"
                )->orWhereHas(
                    'academicPeriod',
                    function ($query) use ($request) {
                        $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                    }
                )->orWhereHas(
                    'educationGrade',
                    function ($query) use ($request) {
                        $query->where('code', 'LIKE', "%" . $request['keyword'] . "%");
                    }
                );
            }
            $examSearch = $examList->get();
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['examList' => $examSearch]]);

            $data['record'] = $examSearch;
            $data['totalRecord'] = $total;
            
            return $data;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting examination >> exam tab >> view
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationExamDetails(Request $request, string $examId)
    {
        try {
            $examView = Examination::where('id', $request->examId)->with(
                'academicPeriod',
                'educationGrade',
                'educationGrade.educationProgramme',
                'securityUser',
                'attendanceType',
                'examSessionType',
                'examinationType'
            )->get();
            Log::info('Fetched exam details  from DB', ['method' => __METHOD__, 'data' => ['examView' => $examView]]);
            
            return $examView;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Update examination >> exam tab >> exam
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateExaminationRecord(AdministratorExaminationUpdateRequest $request, string $examId)
    {
        try {
            $data = Examination::find($examId);
            $data->registration_start_date = $request->registration_start_date;
            $data->registration_end_date = $request->registration_end_date;
            $data->modified_user_id =  JWTAuth::user()->id;
            $data->modified = Carbon::now()->toDateTimeString();
            $store = $data->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * adding examination
     * @param AddExaminationRequest $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function addExamination(AddExaminationRequest $request)
    {
        //DB::beginTransaction();
        try {
            $insertExamination = new Examination();
            $insertExamination->code  = $request->code;
            $insertExamination->name  = $request->name;
            $insertExamination->academic_period_id  = $request->academic_period_id;
            $insertExamination->examination_types_id  = $request->examination_types_id;
            $insertExamination->attendance_type_id  = $request->attendance_type_id;
            $insertExamination->exam_session_types_id  = $request->exam_session_types_id;
            $insertExamination->education_grade_id  = $request->education_grade_id;
            $insertExamination->registration_start_date  = $request->registration_start_date;
            $insertExamination->registration_end_date  = $request->registration_end_date;
            $insertExamination->created_user_id = JWTAuth::user()->id;
            $insertExamination->created = Carbon::now()->toDateTimeString();
            $store = $insertExamination->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Not Added");
        }
    }

    /**
     * Checking if safe to delete
     * @param int $examId
     * @return JsonResponse
     */
    public function checkIfSafeToDelete(int $examId)
    {
        try{
            $exam = Examination::find($examId);

            $examStudent = ExaminationStudent::where('examination_id', $examId)->exists();
            if ($examStudent) {
                return false;
            }

            $examMarker = ExaminationMarker::where('examination_id', $examId)->exists();
            if ($examMarker) {
                return false;
            }

            $examCentre = ExaminationCentre::where('examination_id', $examId)->exists();
            if ($examCentre) {
                return false;
            }
            
            return true;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }

    /**
     * Deleteing Exam Centres
     * @param int $centreId
     * @return JsonResponse
     */
    public function deleteExam(int $examId)
    {
        try{
            $exam = Examination::find($examId);
            if ($exam) {
                $dltExamStudent = $exam->examinationStudent()->delete();
                $dltExamMarker  = $exam->examinationMarker()->delete();
                $dltExamCentre  = $exam->examinationCentre()->delete();
                return $exam->delete();
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }

    public function examCopy(array $rawData)
    {
        DB::beginTransaction();
        try{
            $examId = $rawData['from_examination'];
            $copy = new Examination();
            $copy['code']  = $rawData['code'];
            $copy['name']  = $rawData['name'];
            $copy['academic_period_id']  = $rawData['academic_period_id'];
            $copy['examination_types_id']  = $rawData['examination_types_id'];
            $copy['attendance_type_id']  = $rawData['attendance_type_id'];
            $copy['exam_session_types_id']  = $rawData['exam_session_types_id'];
            $copy['education_grade_id']  = $rawData['education_grade_id'];
            $copy['registration_start_date']  = $rawData['registration_start_date'];
            $copy['registration_end_date']  = $rawData['registration_end_date'];
            $copy['created_user_id'] = JWTAuth::user()->id;
            $copy['created'] = Carbon::now()->toDateTimeString();
            $storeCopy = $copy->save();
            if ($storeCopy) {
                $updtSubject = ExaminationSubject::where('examination_id', $examId)->get();
                foreach ($updtSubject as $subject) {
                     ExaminationSubject::where('examination_id', $examId )->update(array
                                            (
                                                'examination_id'=> $copy->id,
                                                'modified_user_id' => JWTAuth::user()->id,
                                                'modified' => Carbon::now()->toDateTimeString()
                                            )
                                        );
                        } 
               }
                DB::commit();
            
                return $storeCopy;
            } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to copy exam.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }
}