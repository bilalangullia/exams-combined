<?php

namespace App\Repositories;

use App\Exports\UsersExport;
use App\Helper\SlugHelper;
use App\Http\Controllers\Controller;
use App\Models\AcademicPeriod;
use App\Models\Examination;
use App\Models\ExaminationCentre;
use App\Models\ExaminationComponent;
use App\Models\ExaminationStudent;
use App\Models\ReportProgress;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class MarksReportRepository extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function candidateDropdownList(Request $request)
    {
        try {
            $list = ExaminationStudent::select('id', 'candidate_id')->with(
                'examinationStudentsOption:examination_students_id,examination_options_id',
                'examinationStudentsOption.examinationOption.examinationComponent:examination_options_id,id'
            )/*->whereHas(
                'examinationStudentsOption.examinationOption.examinationComponent',
                function ($query) use ($request) {
                    $query->where(
                        'component_types_id',
                        config('constants.courseComponentType.type')
                    );
                }
            )*/ ->whereHas(
                'examinationCentre',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_centre_id);
                }
            )->whereHas(
                'examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            )->whereHas(
                'examName.academicPeriod',
                function ($query) use ($request) {
                    $query->where('id', $request->academic_period_id);
                }
            )->get()->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "candidate_id" => $item['candidate_id'],
                    ];
                }
            );

            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }

    /**
     * @param string $candidateId
     * @return \Illuminate\Http\JsonResponse`
     */
    public function getComponentDropdownList(int $optionId)
    {
        try {
            $component = ExaminationComponent::select('id', 'name')->where('examination_options_id', $optionId)->get();
            return $component;
        } catch (\Exception $e) {
            Log::error(
                'Failed to add review into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' component list not found');
        }
    }

    /**
     * @param array $reportData
     * @return \Illuminate\Http\JsonResponse|int
     */
    public function generate(array $data)
    {
        try {
            $requestData = $data;
            $reportData = [];
            if ($requestData['format'] == 1) {
                $format = '.xlsx';
            } else {
                $format = '.csv';
            }
            $academicPeriod = AcademicPeriod::where('id', $requestData['academic_period_id'])->select(
                'id',
                'start_year'
            )->first()->toArray();
            $exam = Examination::where('id', $requestData['examination_id'])->select('id', 'code', 'name')->first(
            )->toArray();
            $examCentre = ExaminationCentre::select('id', 'name', 'code')->where(
                'id',
                $requestData['examination_centre_id']
            )->first()->toArray();

            $examComponent = ExaminationComponent::where('examination_options_id', $requestData['component_id'])
                ->select('id', 'code', 'name')
                ->first()
                ->toArray();
            $reportData['data'] = $requestData;
            $reportData['academicPeriod'] = $academicPeriod;
            $reportData['exam'] = $exam;
            $reportData['examCentre'] = $examCentre;
            $reportData['examComponent'] = $examComponent;
            $reportData['reportName'] = 'Coursework Marks';
            $reportData['format'] = $format;
            $storage_path = 'public/reports';
            $saveDataArr = $this->saveReportData($reportData);
            $file_name = $saveDataArr['file_name'];
            unset($saveDataArr['file_name']);
            $report = ReportProgress::create($saveDataArr);
            if ($report) {
                $id = $report->id;

                $processing = config('constants.reportUpdateStatus.processing');
                ReportProgress::where('id', $id)->update(['status' => $processing]);

                $timeOut = false;
                for ($i = 0; $i < $saveDataArr['total_records']; $i++) {
                    if (Carbon::now()->toDateTimeString() >= $saveDataArr['expiry_date']) {
                        $timeOut = true;
                        break;
                    }
                    ReportProgress::where('id', $id)->increment('current_records');
                }

                if ($timeOut) {
                    $updateArr['status'] = config('constants.reportUpdateStatus.notCompleted');
                    $updateArr['error_message'] = "report can not be generated due to exceed time";
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);
                    $error['msg'] = "report can not be generated due to exceed time";
                    return false;
                } else {
                    $reportPath = $storage_path . '/' . $file_name . '' . $reportData['format'];
                    $generateFile = Excel::store(new UsersExport($data), $reportPath);
                    $updateArr['status'] = config('constants.reportUpdateStatus.completed');
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();
                    ReportProgress::where('id', $id)->update($updateArr);
                    Log::info(
                        'Pdf Report generated and saved in database and directory',
                        ['method' => __METHOD__, 'data' => []]
                    );
                    return true;
                }
            }
        } catch (\Exception $e) {
            log::error(
                'Failed to generate report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate report");
        }
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveReportData(array $data)
    {
        try {
            $examCentre = $data['examCentre'];
            $exam = $data['exam'];
            $academicPeriod = $data['academicPeriod'];
            $examCentreName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $examCentre['name']);
            $examCentreName = str_replace(" ", "_", $examCentreName);
            $examCentreCode = $examCentre['code'];
            $reportName = $data['reportName'];
            $reportName = $reportName . ": " . $exam['code'] . " " . $academicPeriod['start_year'] . $examCentreCode;
            $fileName = $reportName . '_' . $exam['code'] . '_' . $academicPeriod['start_year'] . '_' . $examCentreCode . "_" . time(
                );
            $fileName = SlugHelper::slugify($fileName);
            $params = json_encode($data['data']);

            $storage_path = 'public/reports';
            if (!Storage::exists($storage_path)) {
                Storage::makeDirectory($storage_path, 0755);
            }
            $url = asset('storage/reports/' . $fileName);
            // $countCandidates = count($data['candidates']);
            $saveArr['id'] = Str::uuid();
            $saveArr['name'] = $reportName;
            $saveArr['module'] = 'Coursework-Marks';
            $saveArr['params'] = $params;
            $saveArr['file_path'] = $url;
            $saveArr['total_records'] = 1;
            $saveArr['status'] = config('constants.reportUpdateStatus.notGeneratedOrNew');
            $saveArr['created'] = Carbon::now()->toDateTimeString();
            $saveArr['modified'] = Carbon::now()->toDateTimeString();
            $saveArr['created_user_id'] = JWTAuth::user()->id;
            $saveArr['expiry_date'] = Carbon::now()->addHours(6)->toDateTimeString();
            $saveArr['modified_user_id'] = JWTAuth::user()->id;
            $saveArr['file_name'] = $fileName;
            return $saveArr;
        } catch (\Exception $e) {
            echo $e;
            log::error(
                'Failed to genrate report data',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate report data");
        }
    }

    /**
     * @param $examId
     * @param $examCenterId
     * @param $candidateId
     * @return array
     */
    public function candidateList($examId, $examCenterId)
    {
        try {
            $list = ExaminationStudent::select('id', 'candidate_id')->with(
                'examinationStudentsOption:examination_students_id,examination_options_id',
                'examinationStudentsOption.examinationOption.examinationComponent:examination_options_id,id'
            )/*->whereHas(
                'examinationStudentsOption.examinationOption.examinationComponent',
                function ($query) {
                    $query->where(
                        'component_types_id',
                        config('constants.courseComponentType.type')
                    );
                }
            )*/->where('examination_centre_id', $examCenterId)
                ->where('examination_id', $examId)
                ->get()
                ->toArray();
            return $list;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }
}
