<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\EducationSystem;
use App\Models\EducationLevel;
use JWTAuth;

class EducationSystemRepository extends Controller
{
	public function getEducationList(Request $request)
	{
		try{
			$list = EducationSystem::select('id', 'name', 'visible');

			if (isset($request['start']) && isset($request['end'])) {
				$listCount = $list;
				$total     = $listCount->count();
				$list->skip($request['start'])
				->take($request['end'] - $request['start']);
			} else {
				$listCount = $list;
				$total = $listCount->count();
			}

			if (isset($request['keyword'])) {
                $listSearch = $list->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where("name", "LIKE", "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            
            $listSearch = $list->get();

            return array("record" => $listSearch, "totalRecord" => $total);
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System List Not Found');
		}
	}

	public function getSystemDetail(int $eduSysId)
	{
		try{
			$view  = EducationSystem::where('id', $eduSysId)->get();

			return $view;
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System Detail Not Found');
		}
	}

	public function getLargestEducationSystemOrder()
    {
        try {
            $largestOrder = 0;
            $order = EducationSystem::max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch largest Education System order from DB");
        }
    }

	public function addEducationSystem(array $data)
	{
		try{
			$order = $this->getLargestEducationSystemOrder();
			$add  = new EducationSystem();
			$add['name'] = $data['name'];
			$add['order'] = $order;
			$add['visible'] = config('constants.visibleValue.visibleCode');
			$add['created_user_id'] = JWTAuth::user()->id;
			$add['created'] = Carbon::now()->toDateTimeString();
			$store  = $add->save();

			return $store;
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System  Not Added');
		}
	}

	public function updateEducationSystem(int $eduSysId, array $data)
	{
		try{
			$update  = EducationSystem::find($eduSysId);
			$update['name'] = $data['name'];
			$update['visible'] = $data['visible'];
			$update['modified_user_id'] = JWTAuth::user()->id;
			$update['modified'] = Carbon::now()->toDateTimeString();
			$updateStore  = $update->save();

			return $updateStore;
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System  Not Updated');
		}
	}

	public function checkIfSafeToDelete(int $eduSysId)
	{
		try{
			$educationSystem = EducationLevel::where('education_system_id', $eduSysId)->exists();
            if ($educationSystem) {
                return false;
            }

            return true;
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System  Can Not Deleted');
		}

	}

	public function deleteSystem(int $eduSysId)
	{
		try{
			$eduSystem = EducationSystem::find($eduSysId);
            if ($eduSystem) {
                $dltEduLevel = $eduSystem->educationLevel()->delete();
                return $eduSystem->delete();
            }
            return false;

		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System  Can Not Deleted');
		}
	}

}