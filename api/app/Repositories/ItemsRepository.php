<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamItemsUpdationRequest;
use App\Models\ExaminationItem;
use App\Models\Examination;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\AddExaminationItemRequest;
use App\Models\ExaminationComponent;
use JWTAuth;
use DB;

class ItemsRepository extends Controller
{
    /**
     * @param Request $request
     * @param string $subjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationItemListing(Request $request, string $examId)
    {
        try {
            $itemList = ExaminationItem::whereHas(
                'examinationComponent.examinationOption.examinationSubject.examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examId);
                }
            )->with('examinationComponent', 'item');

            if (isset($request['start']) && isset($request['end'])) {
                $listCount = $itemList;
                $total = $listCount->count();
                $itemList->skip($request['start'])
                    ->take($request['end'] - $request['start']);
            } else {
                $listCount = $itemList;
                $total = $listCount->count();
            }

            if (isset($request['keyword'])) {
                $itemsSearch = $itemList->where(
                    function ($query) use ($request) {
                        $query->whereHas(
                            'item',
                            function ($query) use ($request) {
                                $query->where('code', 'LIKE', "%".$request['keyword']."%");
                                $query->orwhere('name', 'LIKE', "%".$request['keyword']."%");
                            }
                        )->orWhereHas(
                            'examinationComponent',
                            function ($query) use ($request) {
                                $query->where('code', 'LIKE', "%".$request['keyword']."%");
                                $query->orwhere('name', 'LIKE', "%".$request['keyword']."%");
                            }
                        );
                    }
                );
            }

            $itemsSearch = $itemList->get();
            //dd($itemsSearch);
            $data['record'] = $itemsSearch;
            $data['totalRecord'] = $total;

            Log::info('Fetched Item List from DB', ['method' => __METHOD__, 'data' => ['itemList' => $itemsSearch]]);

            return $data;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Items List Not Found");
        }
    }

    /**
     * Getting admin >> examination >> item tab details
     * @param Request $request
     * @param string $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationItemsDetails(Request $request, string $itemId)
    {
        try {
            $itemDetails = ExaminationItem::where('id', $request->itemId)->with(
                'examinationComponent',
                'examinationComponent.examinationOption',
                'examinationComponent.examinationOption.examinationSubject'
            )->get();
            Log::info(
                'Fetched Item table from DB',
                ['method' => __METHOD__, 'data' => ['itemDetails' => $itemDetails]]
            );

            return $itemDetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch item details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Items Detail Not Found");
        }
    }


    /**
     * updating Getting admin >> examination >> item tab
     * @param Request $request
     * @param string $itemId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function updateExaminationItems(ExamItemsUpdationRequest $request, string $itemId)
    {
        try {
            $updateItem = ExaminationItem::find($itemId);
            $updateItem['items_id'] = $request->items_id;
            $updateItem['examination_components_id'] = $request->examination_components_id;
            $updateItem['modified_user_id'] = JWTAuth::user()->id;
            $updateItem['modified'] = Carbon::now()->toDateTimeString();
            $store = $updateItem->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch item details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return array();
        }
    }

    /**
     * Admin >> examination >> item tab >> delete
     * @param string $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteEXaminationItem(int $itemId)
    {
        try {
            $examItem = ExaminationItem::where('id', $itemId);
            if ($examItem) {
                return $examItem->delete();
            }
            return false;

        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch item details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examinatio Item's Not Removed");
        }
    }

    /**
     * Add examination item
     * @param AddExaminationItemRequest $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function addExaminationItem(AddExaminationItemRequest $request)
    {
        try {
            $record = new ExaminationItem();
            $record->items_id = $request->items_id;
            $record->examination_components_id = $request->examination_components_id;
            $record->created_user_id =  JWTAuth::user()->id;
            $record->created = Carbon::now()->toDateTimeString();
            $store = $record->save();

            return $store;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch item details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Item's Not Added");
        }
    }

    public function itemImport(array $excel, $requestParameter)
    {
        DB::beginTransaction();
        try {
            $i = -1;
            $validation = [];
            $stored_data = [];
            $add_data = [];
            $importResponse = [];
            $academicPeriodId = $requestParameter['academic_period_id'];
            $exam = $requestParameter['examination_id'];
            $examination = Examination::select(
                'id',
                'code',
                'name',
                'academic_period_id'
            )->where('id', $exam)
                ->with(
                    [
                        'academicPeriod' => function ($query) use ($academicPeriodId) {
                            $query->where('id', $academicPeriodId)->select('id', 'name');
                        }
                    ]
                )->first();

            foreach ($excel[0] as  $row) 
            { 
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }
                if (!$row[0]) {//Subject Id
                    $label = $excel[0][1][0];
                    $errors[$label] = 'Component required';
                }

                if (!$row[1]) {//Response
                    $label = $excel[0][1][1];
                    $errors[$label] = 'Item code required';
                }

                if (count($errors) > 0) {
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            "Academic Period" => $examination["academicPeriod"]["name"],
                            "Examination" => $examination['code'],
                            "Component" => "",
                            "Item" => ""
                        ],
                        'errors' => $errors
                    ];
                } else {
                    if ($row[0]) {
                        $stored = ExaminationItem::updateOrCreate(
                                [
                                    'items_id' => $row[1],
                                    'examination_components_id' => $row[0],
                                    'created_user_id' => JWTAuth::user()->id,
                                    'created' => Carbon::now()->toDateTimeString()
                                ]
                            );
                            if ($stored->wasRecentlyCreated) {
                                $add_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        "Academic Period" => $examination["academicPeriod"]["name"],
                                        "Examination" => $examination['code'],
                                        "Items" => $stored['item']['full_name'],
                                        "Components" => $stored['examinationComponent']['full_name']
                                    ],
                                    'errors' => $errors
                                ];
                            } else {
                                $stored_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        "Academic Period" => $examination["academicPeriod"]["name"],
                                        "Examination" => $examination['code'],
                                        "Items" => $stored['item']['full_name'],
                                        "Components" => $stored['examinationComponent']['full_name']
                                    ],
                                    'errors' => $errors
                                ];
                            }
                        } else {
                            $label = $excel[0][1][0];
                            $errors[$label] = 'Subject ID does not exists.';
                            $validation[] = [
                                'row_number' => $i,
                                'data' => [
                                    "Academic Period" => $examination["academicPeriod"]["name"],
                                    "Examination" => $examination['code'],
                                    "Items" => $stored['item']['full_name'],
                                    "Components" => $stored['examinationComponent']['full_name']
                                ],
                                'errors' => $errors
                            ];
                        } 
                    }  
            }
                $importResponse = [
                            'total_count' => count($excel[0]) - 2,
                            'records_added' => [
                                'count' => count($add_data),
                                'rows' => $add_data,
                            ],
                            'records_updated' => [
                                'count' => count($stored_data),
                                'rows' => $stored_data,
                            ],
                            'records_failed' => [
                                'count' => count($validation),
                                'rows' => $validation,
                            ],
                        ];
                
                    DB::commit();
                    return $importResponse; 
        } catch (\Exception $e) {
                echo $e;
                DB::rollback();
                Log::error(
                    'Failed to import item in DB',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                
                return array();
            }
        }

}