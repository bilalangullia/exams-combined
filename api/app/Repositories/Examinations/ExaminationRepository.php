<?php

namespace App\Repositories\Examinations;

use App\Models\Examination;
use Illuminate\Support\Facades\Log;

class ExaminationRepository
{

    public function getExamName($examId)
    {
        try {
            $examName = Examination::where('id', $examId)->select('id','code', 'name', 'academic_period_id')->with('academicPeriod:id,name')->first();
            Log::info('Fetched exam name by exam id', ['method' => __METHOD__, 'data' => ['examId' => $examId, 'examName' => $examName]]);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam name by exam id',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return array();
        }
        if (!$examId) {
            return array();
        }
        return $examName;
    }

}