<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\SystemConfigurationUpdateRequest;
use App\Models\ConfigItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class SystemConfigurationRepository extends Controller
{
    /**
     * @param Request $request
     * @param int $systemconfigId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function systemConfigurationList(Request $request, int $systemconfigId)
    {
        try {
            $data = $request->all();
            $total = 0;
            $list = ConfigItem::select('id', 'type', 'label', 'value', 'visible')->where('id', $systemconfigId);
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $list;
                $total = $userCount->count();
                $list->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $list;
                $total = $userCount->count();
            }
            if (isset($request['keyword'])) {
                $Search = $list->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where('type', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('label', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('value', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('visible', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $Search = $list->get();
            return array("totalRecord" => $total, "record" => $Search);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("System Configuration list not found");
        }
    }

    /**
     * @param string $systemconfigId
     * @return \Illuminate\Http\JsonResponse
     */
    public function systemConfigurationView(int $systemconfigId)
    {
        try {
            $viewId  = ConfigItem::where('id', $systemconfigId)->get();
            return $viewId;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("System Configuration View not found");
        }
    }

    /**
     * @param SystemConfigurationUpdateRequest $request
     * @param string $systemconfigId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSystemConfiguration(SystemConfigurationUpdateRequest $request, int $systemconfigId)
    {
        try {
            $string = $request->prefix_value;
            preg_match_all("/[a-zA-Z-0-9]+/", $string, $matches);
            $prefix_Value = strtolower(implode('_', $matches[0]));
            $updateconfig = ConfigItem::find($systemconfigId);
            $updateconfig['type'] = $updateconfig->type;
            $updateconfig['label'] = $updateconfig->label;
            $updateconfig['code'] = $prefix_Value;
            $updateconfig['value'] = $request->value;
            $updateconfig['modified'] = Carbon::now()->toDateTimeString();
            $updateconfig['modified_user_id'] = JWTAuth::user()->id;
            $record = $updateconfig->save();
            return $record;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("System Configuration not updated");
        }
    }
}
