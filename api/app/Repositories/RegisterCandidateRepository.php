<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationStudent;
use App\Models\ExaminationGradeReviewStudent;
use App\Models\ExaminationStudentsCertificate;
use App\Models\ExaminationStudentsComponentsGrade;
use App\Models\ExaminationStudentsComponentsMark;
use App\Models\ExaminationStudentsFee;
use App\Models\Examination;
use App\Models\ExaminationStudentsForecastGrade;
use App\Models\ExaminationStudentsMultiplechoiceResponse;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationStudentsOptionsGrade;
use App\Models\SecurityGroupUser;
use App\Models\SecurityUser;
use App\Models\SecurityUserLogin;
use App\Models\SecurityUserPasswordRequest;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use JWTAuth;
use DB;
use Carbon\Carbon;
use App\Models\UserSpecialNeedsAssessment;
use App\Models\ExaminationCentre;
use App\Models\ExaminationStudentsFeesPayment;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;

class RegisterCandidateRepository extends Controller
{

    /**
     * Candidate ID generation
     * @param $exam
     * @return string
     */
    public function candidateId(ExaminationStudent $examStud = null)
    {
        if ($examStud) {
            $current_year = (Carbon::now()->format('y'));
            $examCente = ExaminationCentre::where('id', $examStud->examination_centre_id)->first();
            $examLevel = Examination::where('id', $examStud->examination_id)->with('examinationType')->first();
            $word = $examLevel['examinationType']['name'];
            $letter = substr($word, 0, 1);
            $ec_code = $examCente->code;
            $candId = str_pad($current_year . $letter . $ec_code . mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);
            ExaminationStudent::where('id', $examStud->id)->update(['candidate_id' => $candId]);

            return $candId;
        } else {
            $examinationStudents = ExaminationStudent::where('candidate_id', '')->with('examinationCentre','examName.examinationType')->get();
            if ($examinationStudents->count()) {
                $examinationStudents = $this->modifyData($examinationStudents);
                DB::statement(
                    'UPDATE examination_students SET candidate_id = (' . $examinationStudents['str'] . ') WHERE id IN(' . $examinationStudents['ids'] . ')'
                );
            }
        }
    }

    /**
     * @param $examinationStudents
     * @return array
     */
    protected function modifyData($examinationStudents)
    {
        $ids = [];
        $str = 'CASE id';
        $data = [];
        for ($i = 0; $i < ($examinationStudents->count()); $i++) {
            $examType = $examinationStudents[$i]['examName']['examinationType']['name'];
            $firstLtr = substr($examType, 0, 1);
            $year = Carbon::now()->format('y');
            $candidate_id = str_pad(
                $year . $firstLtr .  $examinationStudents[$i]['examinationCentre']['code'] . mt_rand(0, 9999),
                4,
                '0',
                STR_PAD_LEFT
            );
            $str .= ' WHEN ' . $examinationStudents[$i]['id'] . ' THEN ' . "'$candidate_id'";
            $ids[] = $examinationStudents[$i]['id'];
        }

        $str .= ' END';
        $ids = implode(",", $ids);
        $data['str'] = $str;
        $data['ids'] = $ids;
        return $data;
    }

	public function checkIfSafeToDelete(string $candId)
    {
        try{
            $candidateId = ExaminationStudent::where('candidate_id', $candId)->first();
            $candidId = $candidateId->id;

            $examGrdRvwStd = ExaminationGradeReviewStudent::where('examination_students_id', $candidId)->exists();
            if ($examGrdRvwStd) {
                $error  = "Candidate Grade Review Exist";
                return array('msg' => $error);
            }

            $examStdCert = ExaminationStudentsCertificate::where('examination_students_id', $candidId)->exists();
            if ($examStdCert) {
                $error  = "Candidate Certificate Number Exist";
                return array('msg' => $error);
            }

            $examStdCompGrd = ExaminationStudentsComponentsGrade::where('examination_students_id', $candidId)->exists();
            if ($examStdCompGrd) {
                $error  = "Candidate Component Grade Exist";
                return array('msg' => $error);
            }

            $examStdCompMrk = ExaminationStudentsComponentsMark::where('examination_students_id', $candidId)->exists();
            if ($examStdCompMrk) {
                $error  = "Candidate Component Mark Exist";
                return array('msg' => $error);
            }

            $examStdFee = ExaminationStudentsFee::where('examination_students_id', $candidId)->exists();
            if ($examStdFee) {
                $error  = "Candidate Fee Exist";
                return array('msg' => $error);
            }

            $examStdFrcstGrd = ExaminationStudentsForecastGrade::where('examination_students_id', $candidId)->exists();
            if ($examStdFrcstGrd) {
                $error  = "Candidate Forecast Grade Exist";
                return array('msg' => $error);
            }

            $examStdMultRes = ExaminationStudentsMultiplechoiceResponse::where('examination_students_id', $candidId)->exists();
            if ($examStdMultRes) {
                $error  = "Candidate Multiple Choice Response Exist";
                return array('msg' => $error);
            }

            $examStdOpt = ExaminationStudentsOption::where('examination_students_id', $candidId)->exists();
            if ($examStdOpt) {
                $error  = "Candidate Option Exist";
                return array('msg' => $error);
            }

             $examStdOptGrd = ExaminationStudentsOptionsGrade::where('examination_students_id', $candidId)->exists();
            if ($examStdOptGrd) {
                $error  = "Candidate Option Grade Exist";
                return array('msg' => $error);
            }
            
            return true;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete candidate.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }

    /**
     * Deleteing Exam Centres
     * @param int $centreId
     * @return JsonResponse
     */
    public function deleteCandidate(string $candId)
    {
        try{
            $candidateId = ExaminationStudent::where('candidate_id', $candId)->first();
            $candidate = $candidateId->id;
            if ($candidate) {
                $dltScrtStd = SecurityUser::where('id', $candidateId->student_id)->delete();
                $dltGrpUser = SecurityGroupUser::where('security_user_id', $candidateId->student_id)->delete();
                $dltLoginUser = SecurityUserLogin::where('security_user_id', $candidateId->student_id)->delete();
                $dltUsrPasswdRqst =SecurityUserPasswordRequest::where('user_id', $candidateId->student_id)->delete();
                $dlt = ExaminationStudent::where('id', $candidate)->delete();
                return $dlt;
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }	

    /**
     * Getting Candidate List Using Advance Search
     * @param array $data
     * @return JsonResponse
     */
    public function advanceSearch(array $data)
    {
        try {
            $candidateList1 = SecurityUser::select(
                'id',
                'openemis_no',
                'first_name',
                'middle_name',
                'third_name',
                'last_name',
                'date_of_birth',
                'gender_id'
            )
            ->with(
                [
                    'gender' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examinationStudent' => function ($query) {
                        $query->select('id', 'student_id', 'candidate_id', 'examination_id', 'modified', 'examination_centre_id');
                    },
                    'examinationStudent.examName' => function ($query) {
                        $query->select('id', 'code', 'name');
                    },
                ]
            )
            ->where('status', config('constants.students.status'))
            ->where('is_student', config('constants.students.isStudent'))
            ->orderby('id', 'DESC');


            if (count($data) > 0) {
                $candidateList1->where(function ($q) use ($data) {
                    if (isset($data['openemis_no'])) {
                        $q->where('openemis_no', 'LIKE', "%" . $data['openemis_no'] . "%");
                    }

                    if (isset($data['first_name'])) {
                        $q->orWhere('first_name', 'LIKE', "%" . $data['first_name'] . "%");
                    }

                    if (isset($data['middle_name'])) {
                        $q->orWhere('middle_name', 'LIKE', "%" . $data['middle_name'] . "%");
                    }

                    if (isset($data['third_name'])) {
                        $q->orWhere('third_name', 'LIKE', "%" . $data['third_name'] . "%");
                    }

                    if (isset($data['last_name'])) {
                        $q->orWhere('last_name', 'LIKE', "%" . $data['last_name'] . "%");
                    }

                    if (isset($data['gender_id'])) {
                        $q->orWhere('gender_id', 'LIKE', "%" . $data['gender_id'] . "%");
                    }

                    if (isset($data['nationality_id'])) {
                        $q->orWhere('nationality_id', 'LIKE', "%" . $data['nationality_id'] . "%");
                    }

                    if (isset($data['identity_type_id'])) {
                        $q->orWhere('identity_type_id', 'LIKE', "%" . $data['identity_type_id'] . "%");
                    }

                    if (isset($data['identity_number'])) {
                        $q->orWhere('identity_number', 'LIKE', "%" . $data['identity_number'] . "%");
                    }

                    if (isset($data['examination_id'])) {
                        $q->orWhereHas(
                            'examinationStudent',
                            function ($query) use ($data) {
                                $query->where('examination_id', 'LIKE', "%" . $data['examination_id'] . "%");
                            }
                        );
                    }

                    if (isset($data['examination_centre_id'])) {
                        $q->orWhereHas(
                            'examinationStudent',
                            function ($query) use ($data) {
                                $query->where('examination_centre_id', 'LIKE', "%" . $data['examination_centre_id'] . "%");
                            }
                        );
                    }
                });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $candidateCount = $candidateList1;
                $total = $candidateCount->count();
                $candidateList1->skip($data['start'])
                                ->take($data['end'] - $data['start']);
            } else {
                $candidateCount = $candidateList1;
                $total = $candidateCount->count();
            }

            $candidateList = $candidateList1->get();
            
            $result['list'] = $candidateList;
            $result['total'] = $total;
            $result['start'] = $data['start'];
            $result['end'] = $data['end'];
            return $result;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get Candidate',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get Candidate");
        }
    }

    public function candidateImport(array $excelData, $requestParameter)
    {
        DB::beginTransaction();
        try {
            $i = -1;
            $validation = [];
            $stored_data = [];
            $add_record = [];
            $response = [];
            $insert = [];
            $inserta = [];
            $insertb = [];
            $insertc = [];
            $insertOpt = [];
            $academicPeriodId = $requestParameter['academic_period_id'];
            $exam = $requestParameter['examination_id'];
            foreach ($excelData[0] as  $data) {
                $errors = [];
                $i++;

                if ($i < 1) {
                    continue;
                }
                if (!$data[1]) {
                    $label = $excelData[0][1][1];
                    $errors[$label] = 'First Name Required';
                }
                if (!$data[4]) {
                    $label = $excelData[0][1][4];
                    $errors[$label] = 'Last Name Required';
                }
                if (!($data[6])) {
                    $label = $excelData[0][1][6];
                    $errors[$label] = 'Gender Required';
                }
                if (!$data[7]) {
                    $label = $excelData[0][1][7];
                    $errors[$label] = 'Date of Birth Required';
                }
                if (!$data[21]) { 
                    $label = $excelData[0][1][21];
                    $errors[$label] = 'Exam Centre Required';
                } 
                if (!$data[22]) { 
                    $label = $excelData[0][1][22];
                    $errors[$label] = 'Attendance Type Required';
                } 
                if(is_numeric($data[7])) {
                    $dateofbirth = Date::excelToDateTimeObject($data[7])->format('Y/m/d');
                } else {
                    $dateofbirth = Carbon::createFromFormat('d/m/Y', $data[7]);
                }

            if (count($errors)) { //row fail
                $validation[] = [
                    'row_number' => $i,
                    'data' => [
                    $excelData[0][0][0] => $data[0],
                                $excelData[0][0][1] => $data[1],
                                $excelData[0][0][2] => $data[2],
                                $excelData[0][0][3] => $data[3],
                                $excelData[0][0][4] => $data[4],
                                $excelData[0][0][5] => $data[5],
                                $excelData[0][0][6] => $data[6],
                                $excelData[0][0][7] => $dateofbirth,
                                $excelData[0][0][8] => $data[8],
                                $excelData[0][0][9] => $data[9],
                                $excelData[0][0][10] => $data[10],
                                $excelData[0][0][11] => $data[11],
                                $excelData[0][0][12] => $data[12],
                                $excelData[0][0][13] => $data[13],
                                $excelData[0][0][14] => $data[14],
                                $excelData[0][0][15] => $data[15],
                                $excelData[0][0][16] => $data[16],
                                $excelData[0][0][17] => $data[17],
                                $excelData[0][0][18] => $data[18],
                                $excelData[0][0][19] => $data[19],
                                $excelData[0][0][20] => $data[20],
                                $excelData[0][0][21] => $data[21],
                                $excelData[0][0][22] => $data[22],
                                $excelData[0][0][23] => $data[23],
                                $excelData[0][0][24] => $data[24],
                                $excelData[0][0][25] => $data[25],
                                $excelData[0][0][26] => $data[26],
                                $excelData[0][0][27] => $data[27],
                                $excelData[0][0][28] => $data[28],
                                $excelData[0][0][29] => $data[29],
                                $excelData[0][0][30] => $data[30],
                                $excelData[0][0][31] => $data[31],
                                $excelData[0][0][32] => $data[32],
                                $excelData[0][0][33] => $data[33],
                                $excelData[0][0][34] => $data[34],
                                $excelData[0][0][35] => $data[35],
                                $excelData[0][0][36] => $data[36],
                                $excelData[0][0][37] => $data[37],
                                $excelData[0][0][38] => $data[38]],
                    'errors' => $errors,
                ];
            } else {
                if ($data[0]) {
                    $examStudRec = ExaminationStudent::where(['candidate_id' => $data[0]])->first();
                    if ($examStudRec) {
                        $examStudRec->examination_centre_id = $data[21];
                        $examStudRec->examination_id = $requestParameter['examination_id'];
                        $store = $examStudRec->save();
                        ExaminationStudentsOption::where('examination_students_id', $examStudRec->id)->delete();
                        if($store && $data[23]) {
                                $examStdOpt1 = new ExaminationStudentsOption();
                                $examStdOpt1['id'] = Str::uuid();
                                $examStdOpt1['carry_forward'] = $data[24];
                                $examStdOpt1['examination_students_id'] = $examStudRec->id;
                                $examStdOpt1['examination_options_id'] = $data[23];
                                $examStdOpt1['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt1['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption1 = $examStdOpt1->save();
                            }
                            if($examStudentOption1 && $data[25]) {
                                $examStdOpt2 = new ExaminationStudentsOption();
                                $examStdOpt2['id'] = Str::uuid();
                                $examStdOpt2['carry_forward'] = $data[26];
                                $examStdOpt2['examination_students_id'] = $examStudRec->id;
                                $examStdOpt2['examination_options_id'] = $data[25];
                                $examStdOpt2['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt2['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption2 = $examStdOpt2->save();
                            }
                            if($examStudentOption2 && $data[27]) {
                                $examStdOpt3 = new ExaminationStudentsOption();
                                $examStdOpt3['id'] = Str::uuid();
                                $examStdOpt3['carry_forward'] = $data[28];
                                $examStdOpt3['examination_students_id'] = $examStudRec->id;
                                $examStdOpt3['examination_options_id'] = $data[27];
                                $examStdOpt3['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt3['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption3 = $examStdOpt3->save();
                            }
                            if($examStudentOption3 && $data[29]) {
                                $examStdOpt4 = new ExaminationStudentsOption();
                                $examStdOpt4['id'] = Str::uuid();
                                $examStdOpt4['carry_forward'] = $data[30];
                                $examStdOpt4['examination_students_id'] = $examStudRec->id;
                                $examStdOpt4['examination_options_id'] = $data[29];
                                $examStdOpt4['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt4['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption4 = $examStdOpt4->save();
                            }
                            if($examStudentOption4 && $data[31]) {
                                $examStdOpt5 = new ExaminationStudentsOption();
                                $examStdOpt5['id'] = Str::uuid();
                                $examStdOpt5['carry_forward'] = $data[32];
                                $examStdOpt5['examination_students_id'] = $examStudRec->id;
                                $examStdOpt5['examination_options_id'] = $data[31];
                                $examStdOpt5['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt5['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption5 = $examStdOpt5->save();
                            }
                            if($examStudentOption5 && $data[33]) {
                                $examStdOpt6 = new ExaminationStudentsOption();
                                $examStdOpt6['id'] = Str::uuid();
                                $examStdOpt6['carry_forward'] = $data[34];
                                $examStdOpt6['examination_students_id'] = $examStudRec->id;
                                $examStdOpt6['examination_options_id'] = $data[33];
                                $examStdOpt6['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt6['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption6 = $examStdOpt6->save();
                            }
                        $user = array();
                        $user['first_name'] = $data[1];
                        $user['middle_name'] = $data[2];
                        $user['third_name'] = $data[3];
                        $user['last_name'] = $data[4];
                        $user['preferred_name'] = $data[5];
                        $user['gender_id'] = ($data[6] == 'M') ? 1 : 2;
                        $user['date_of_birth'] = $dateofbirth;
                        $user['address'] = $data[8];
                        $user['postal_code'] = $data[9];
                        $user['nationality_id'] = $data[13];
                        $user['identity_type_id'] = $data[14];
                        $user['identity_number'] = $data[15];
                        $secUserRec = SecurityUser::where(['id' => $examStudRec->student_id])->update($user);
                        $specialNeeds = [
                            'date' => ($data[18]) ? $data[18] : Carbon::now(),
                            'special_need_type_id' => ($data[19]) ? $data[19] : 0,
                            'special_need_difficulty_id' => ($data[20]) ? $data[20] : 0,
                        ];
                        UserSpecialNeedsAssessment::where(['security_user_id' => $examStudRec->student_id])->update(
                            $specialNeeds
                        );

                        $feeData = ExaminationStudentsFee::where('examination_students_id', $examStudRec->id)->first();
                        
                        $fees = [
                            'examination_student_fees_id' => $feeData['id'], 
                            'date' => Carbon::now()->toDateTimeString(),
                            'amount' => ($data[37]) ? $data[37] : 0, 
                            'receipt' => $data[38],
                        ];
                        ExaminationStudentsFeesPayment::where('examination_student_fees_id', $feeData['id'])->update(
                            $fees
                        );
                        
                        $stored_data[] = [
                            'row_number' => $i,
                            'data' => [
                            $excelData[0][0][0] => $data[0],
                                $excelData[0][0][1] => $data[1],
                                $excelData[0][0][2] => $data[2],
                                $excelData[0][0][3] => $data[3],
                                $excelData[0][0][4] => $data[4],
                                $excelData[0][0][5] => $data[5],
                                $excelData[0][0][6] => $data[6],
                                $excelData[0][0][7] => $dateofbirth,
                                $excelData[0][0][8] => $data[8],
                                $excelData[0][0][9] => $data[9],
                                $excelData[0][0][10] => $data[10],
                                $excelData[0][0][11] => $data[11],
                                $excelData[0][0][12] => $data[12],
                                $excelData[0][0][13] => $data[13],
                                $excelData[0][0][14] => $data[14],
                                $excelData[0][0][15] => $data[15],
                                $excelData[0][0][16] => $data[16],
                                $excelData[0][0][17] => $data[17],
                                $excelData[0][0][18] => $data[18],
                                $excelData[0][0][19] => $data[19],
                                $excelData[0][0][20] => $data[20],
                                $excelData[0][0][21] => $data[21],
                                $excelData[0][0][22] => $data[22],
                                $excelData[0][0][23] => $data[23],
                                $excelData[0][0][24] => $data[24],
                                $excelData[0][0][25] => $data[25],
                                $excelData[0][0][26] => $data[26],
                                $excelData[0][0][27] => $data[27],
                                $excelData[0][0][28] => $data[28],
                                $excelData[0][0][29] => $data[29],
                                $excelData[0][0][30] => $data[30],
                                $excelData[0][0][31] => $data[31],
                                $excelData[0][0][32] => $data[32],
                                $excelData[0][0][33] => $data[33],
                                $excelData[0][0][34] => $data[34],
                                $excelData[0][0][35] => $data[35],
                                $excelData[0][0][36] => $data[36],
                                $excelData[0][0][37] => $data[37],
                                $excelData[0][0][38] => $data[38]
                            ],
                        ];
                    } else {
                        $label = $excelData[0][0][0];
                        $errors[$label] = 'Candidate ID does not exist';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                $excelData[0][0][0] => $data[0],
                                $excelData[0][0][1] => $data[1],
                                $excelData[0][0][2] => $data[2],
                                $excelData[0][0][3] => $data[3],
                                $excelData[0][0][4] => $data[4],
                                $excelData[0][0][5] => $data[5],
                                $excelData[0][0][6] => $data[6],
                                $excelData[0][0][7] => $dateofbirth,
                                $excelData[0][0][8] => $data[8],
                                $excelData[0][0][9] => $data[9],
                                $excelData[0][0][10] => $data[10],
                                $excelData[0][0][11] => $data[11],
                                $excelData[0][0][12] => $data[12],
                                $excelData[0][0][13] => $data[13],
                                $excelData[0][0][14] => $data[14],
                                $excelData[0][0][15] => $data[15],
                                $excelData[0][0][16] => $data[16],
                                $excelData[0][0][17] => $data[17],
                                $excelData[0][0][18] => $data[18],
                                $excelData[0][0][19] => $data[19],
                                $excelData[0][0][20] => $data[20],
                                $excelData[0][0][21] => $data[21],
                                $excelData[0][0][22] => $data[22],
                                $excelData[0][0][23] => $data[23],
                                $excelData[0][0][24] => $data[24],
                                $excelData[0][0][25] => $data[25],
                                $excelData[0][0][26] => $data[26],
                                $excelData[0][0][27] => $data[27],
                                $excelData[0][0][28] => $data[28],
                                $excelData[0][0][29] => $data[29],
                                $excelData[0][0][30] => $data[30],
                                $excelData[0][0][31] => $data[31],
                                $excelData[0][0][32] => $data[32],
                                $excelData[0][0][33] => $data[33],
                                $excelData[0][0][34] => $data[34],
                                $excelData[0][0][35] => $data[35],
                                $excelData[0][0][36] => $data[36],
                                $excelData[0][0][37] => $data[37],
                                $excelData[0][0][38] => $data[38]
                            ],
                            'errors' => $errors,
                        ];
                } 
            } else { 
                        $lastId = SecurityUser::orderBy('id','DESC')->first();
                        $user = new SecurityUser();
                        //$user['openemis_no'] = $lastId->openemis_no + 1;
                        if(is_numeric($lastId)) {
                            $user['openemis_no'] = $lastId->openemis_no + 1;
                        } else {
                        $string = $lastId->openemis_no;
                        $newStr = ++$string;
                        $user['openemis_no'] = $newStr;
                        }
                        $user['username'] = $user['openemis_no'];
                        $user['first_name'] = $data[1];
                        $user['middle_name'] = $data[2];
                        $user['third_name'] = $data[3];
                        $user['last_name'] = $data[4];
                        $user['gender_id'] = ($data[6] == 'M') ? 1 : 2;
                        $user['date_of_birth'] = $dateofbirth;
                        $user['address'] = $data[8];
                        $user['postal_code'] = $data[9];
                        $user['nationality_id'] = $data[13];
                        $user['identity_type_id'] = $data[14];
                        $user['identity_number'] = $data[15];
                        $user['is_student'] = config('constants.students.isStudent');
                        $user['created_user_id'] = JWTAuth::user()->id;
                        $user['status'] = config('constants.students.status');
                        $user['created'] = Carbon::now()->toDateTimeString();
                        $record = $user->save();
                            if ($record) {
                            $exam = new ExaminationStudent();
                            $exam['candidate_id'] = '';
                            $exam['examination_centre_id'] = $data[21];
                            $exam['examination_id'] = $requestParameter['examination_id'];
                            $exam['student_id'] = $user->id;
                            $exam['examination_attendance_types_id'] = $data[22];
                            $exam['created_user_id'] = JWTAuth::user()->id;
                            $exam['created'] = Carbon::now()->toDateTimeString();
                            $examStudent = $exam->save();
                            if($examStudent && $data[23]) {
                                $examStdOpt1 = new ExaminationStudentsOption();
                                $examStdOpt1['id'] = Str::uuid();
                                $examStdOpt1['carry_forward'] = $data[24];
                                $examStdOpt1['examination_students_id'] = $exam->id;
                                $examStdOpt1['examination_options_id'] = $data[23];
                                $examStdOpt1['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt1['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption1 = $examStdOpt1->save();
                            }
                            if($examStudent && $data[25]) {
                                $examStdOpt2 = new ExaminationStudentsOption();
                                $examStdOpt2['id'] = Str::uuid();
                                $examStdOpt2['carry_forward'] = $data[26];
                                $examStdOpt2['examination_students_id'] = $exam->id;
                                $examStdOpt2['examination_options_id'] = $data[25];
                                $examStdOpt2['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt2['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption2 = $examStdOpt2->save();
                            }
                            if($examStudent && $data[27]) {
                                $examStdOpt3 = new ExaminationStudentsOption();
                                $examStdOpt3['id'] = Str::uuid();
                                $examStdOpt3['carry_forward'] = $data[28];
                                $examStdOpt3['examination_students_id'] = $exam->id;
                                $examStdOpt3['examination_options_id'] = $data[27];
                                $examStdOpt3['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt3['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption3 = $examStdOpt3->save();
                            }
                            if($examStudent && $data[29]) {
                                $examStdOpt4 = new ExaminationStudentsOption();
                                $examStdOpt4['id'] = Str::uuid();
                                $examStdOpt4['carry_forward'] = $data[30];
                                $examStdOpt4['examination_students_id'] = $exam->id;
                                $examStdOpt4['examination_options_id'] = $data[29];
                                $examStdOpt4['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt4['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption4 = $examStdOpt4->save();
                            }
                            if($examStudent && $data[31]) {
                                $examStdOpt5 = new ExaminationStudentsOption();
                                $examStdOpt5['id'] = Str::uuid();
                                $examStdOpt5['carry_forward'] = $data[32];
                                $examStdOpt5['examination_students_id'] = $exam->id;
                                $examStdOpt5['examination_options_id'] = $data[31];
                                $examStdOpt5['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt5['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption5 = $examStdOpt5->save();
                            }
                            if($examStudent && $data[33]) {
                                $examStdOpt6 = new ExaminationStudentsOption();
                                $examStdOpt6['id'] = Str::uuid();
                                $examStdOpt6['carry_forward'] = $data[34];
                                $examStdOpt6['examination_students_id'] = $exam->id;
                                $examStdOpt6['examination_options_id'] = $data[33];
                                $examStdOpt6['created_user_id'] = JWTAuth::user()->id;
                                $examStdOpt6['created'] = Carbon::now()->toDateTimeString();
                                $examStudentOption6 = $examStdOpt6->save();
                            }
                            $inserta[] = [
                                'date' => Carbon::now()->toDateTimeString(),
                                'special_need_type_id' => ($data[18]) ? $data[18] : 0,
                                'special_need_difficulty_id' => ($data[19]) ? $data[19] : 0,
                                'created_user_id' => JWTAuth::user()->id,
                                'created' => Carbon::now()->toDateTimeString(),
                                'security_user_id' => $user->id,
                            ];
                            if ($record && $data[35]) {
                                $fee = new ExaminationStudentsFee();
                                $fee['quantity'] = 6;
                                $fee['examination_fees_id'] = $data[35];
                                $fee['examination_students_id'] = $exam->id;
                                $fee['created_user_id'] = JWTAuth::user()->id;
                                $fee['created'] = Carbon::now()->toDateTimeString();
                                $feeSave = $fee->save();
                                
                                $insertc[] = [
                                    'examination_student_fees_id' => $fee->id,
                                    'date' => Carbon::now()->toDateTimeString(),
                                    'amount' => ($data[37]) ? $data[37] : 0,
                                    'receipt' => $data[38],
                                    'created_user_id' => JWTAuth::user()->id,
                                    'created' => Carbon::now()->toDateTimeString(),
                                ];
                            }
                            $this->candidateId();
                        }
                        if (count($inserta)) {
                            UserSpecialNeedsAssessment::insert($inserta);
                        }
                        if (count($insertc)) {
                            ExaminationStudentsFeesPayment::insert($insertc);
                        }
                            $add_record[] = [
                                'row_number' => $i,
                                'data' => [
                                    $excelData[0][0][0] => $data[0],
                                    $excelData[0][0][1] => $data[1],
                                    $excelData[0][0][2] => $data[2],
                                    $excelData[0][0][3] => $data[3],
                                    $excelData[0][0][4] => $data[4],
                                    $excelData[0][0][5] => $data[5],
                                    $excelData[0][0][6] => $data[6],
                                    $excelData[0][0][7] => $dateofbirth,
                                    $excelData[0][0][8] => $data[8],
                                    $excelData[0][0][9] => $data[9],
                                    $excelData[0][0][10] => $data[10],
                                    $excelData[0][0][11] => $data[11],
                                    $excelData[0][0][12] => $data[12],
                                    $excelData[0][0][13] => $data[13],
                                    $excelData[0][0][14] => $data[14],
                                    $excelData[0][0][15] => $data[15],
                                    $excelData[0][0][16] => $data[16],
                                    $excelData[0][0][17] => $data[17],
                                    $excelData[0][0][18] => $data[18],
                                    $excelData[0][0][19] => $data[19],
                                    $excelData[0][0][20] => $data[20],
                                    $excelData[0][0][21] => $data[21],
                                    $excelData[0][0][22] => $data[22],
                                    $excelData[0][0][23] => $data[23],
                                    $excelData[0][0][24] => $data[24],
                                    $excelData[0][0][25] => $data[25],
                                    $excelData[0][0][26] => $data[26],
                                    $excelData[0][0][27] => $data[27],
                                    $excelData[0][0][28] => $data[28],
                                    $excelData[0][0][29] => $data[29],
                                    $excelData[0][0][30] => $data[30],
                                    $excelData[0][0][31] => $data[31],
                                    $excelData[0][0][32] => $data[32],
                                    $excelData[0][0][33] => $data[33],
                                    $excelData[0][0][34] => $data[34],
                                    $excelData[0][0][35] => $data[35],
                                    $excelData[0][0][36] => $data[36],
                                    $excelData[0][0][37] => $data[37],
                                    $excelData[0][0][38] => $data[38]
                                ],
                            ];
                        }  
                    } 
                }
            $response = [
            'total_count' => count($excelData[0]) - 1,
            'records_added' => [
                'count' => count($add_record),
                'rows' => $add_record
            ],
            'records_updated' => [
                'count' => count($stored_data),
                'rows' => $stored_data
            ],
            'records_failed' => [
                'count' => count($validation),
                'rows' => $validation
            ],
        ];
            DB::commit();
            return  $response;  
        } catch (\Exception $e) {
            echo $e;
                DB::rollback();
                Log::error(
                    'Failed to import option in DB',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                
                return array();
        }
    }

}