<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Models\ConfigItem;
use App\Models\EducationCertification;
use App\Models\EducationCycle;
use App\Models\EducationGrade;
use App\Models\EducationLevel;
use App\Models\EducationProgramme;
use App\Models\EducationStage;
use App\Models\ExaminationCentre;
use App\Models\ExaminationCentreOwnership;
use App\Models\ExaminationComponent;
use App\Models\ExaminationFee;
use App\Models\ExaminationGradingType;
use App\Models\ExaminationOption;
use App\Models\ExaminationSubject;
use App\Models\ExaminationType;
use App\Models\ExamSessionType;
use App\Models\Item;
use App\Models\SecurityGroup;
use App\Models\EducationSubject;
use App\Models\AreaAdministrative;
use App\Models\Countrie;
use Illuminate\Support\Facades\Log;

class MiscRepository extends Controller
{
    /**
     * Education program dropdown api
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEducationProgrammeDropDown()
    {
        try {
            $educationProgramme = EducationProgramme::select('id', 'code', 'name')->get();
            Log::info(
                'Fetched  data from DB',
                ['method' => __METHOD__, 'data' => ['educationProgrammeDropDown' => $educationProgramme]]
            );

            return $educationProgramme;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting Education grade on education Programme ID basis
     * @param string $educationProgrammeID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEducationGradeDropDown(string $educationProgrammeID)
    {
        try {
            $educationGrade = EducationGrade::select('id', 'code', 'name')->where(
                'education_programme_id',
                $educationProgrammeID
            )->get();
            Log::info(
                'Fetched  data from DB',
                ['method' => __METHOD__, 'data' => ['educationGradeDropDown' => $educationGrade]]
            );

            return $educationGrade;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting examination type dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationTypeDropDown()
    {
        try {
            $examinationType = ExaminationType::select('id', 'name')->get();

            return $examinationType;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting examination session dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationSessionDropDown()
    {
        try {
            $examSession = ExamSessionType::select('id', 'name')->get();

            return $examSession;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam Session Not Found");
        }
    }

    /**
     * Getting examination grading type dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationGradingTypeDropDown()
    {
        try {
            $examGradingType = ExaminationGradingType::select('id', 'name')->get();

            return $examGradingType;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Grading Type Not Found");
        }
    }


    /**
     * Getting examination >> Item tab >> Items dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function itemsDropDown()
    {
        try {
            $itemDropdown = Item::select('id', 'code', 'name')->get();

            return $itemDropdown;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch item dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Item's Dropdown Not Found");
        }
    }

    /**
     * Getting examination fee dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function examinationFeeDropdowp()
    {
        try {
            $feeDropdown = ExaminationFee::select('id', 'name')->get();

            return $feeDropdown;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("EXamination Fee Dropdown Not Found");
        }
    }

    /**
     * Getting examination fee amount dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function examinationFeeAmountDropdown()
    {
        try {
            $feeDropdown = ExaminationFee::select('id', 'amount')->get();

            return $feeDropdown;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee amount dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Amount Dropdown Not Found");
        }
    }

    /**
     * Fetching examination option dropdown
     * @param string $subjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function optionDropdown(string $subjectId)
    {
        try {
            $optionDropdown = ExaminationOption::select('id', 'code', 'name')->where(
                'examination_subject_id',
                $subjectId
            )->get();

            return $optionDropdown;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Option Dropdown Not Found");
        }
    }

    /**
     * Fetching examination component dropdown
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function componentDropdown(string $optionId)
    {
        try {
            $componentDropdown = ExaminationComponent::select('id', 'code', 'name')->where(
                'examination_options_id',
                $optionId
            )->get();

            return $componentDropdown;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Component Dropdown Not Found");
        }
    }

    /**
     * Fetching ownership dropdown.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOwnershipList()
    {
        try {
            $ownerships = ExaminationCentreOwnership::where('visible', config('constants.visibleValue.visibleCode'))
                ->select('id as key', 'name as value')
                ->get()
                ->toArray();
            return $ownerships;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch ownership dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(" ownership  Not Found");
        }
    }

    /**
     * Getting Exam Centre List
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExamCentreList(string $examId)
    {
        try {
            $examCentre = ExaminationCentre::where('examination_id', $examId)->select('id', 'code', 'name')->get();
            return $examCentre;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Dropdown Not Found");
        }
    }

    /**
     * Getting User Group Dropdown
     * @return JsonResponse
     */
    public function getGroupDropdown()
    {
        try {
            $group = SecurityGroup::select('id', 'name')
                        ->whereHas('institutions')
                        ->get();

            return $group;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch group dropdown from DB");
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEducationStructureCycledropdown()
    {
        try {
            $listcycle = EducationCycle::select('id', 'name')->where('visible', config('constants.visibleValue.visibleCode'))->get();
            return $listcycle;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch cycle dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch cycle dropdown from DB");
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEducationStructureCycle(string $levelId)
    {
        try {
            $listcycle = EducationCycle::select('id', 'name')->where('education_level_id', $levelId)->where('visible', config('constants.visibleValue.visibleCode'))->get();
            return $listcycle;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch cycle dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch cycle dropdown from DB");
        }
    }


    /**
     * Getting Education Level Dropdown
     * @return JsonResponse
     */
    public function getEducationLevelDropdown()
    {
        try {
            $levels = EducationLevel::select('id', 'name', 'education_system_id')
                ->with(
                    [
                        'educationSystem' => function ($q) {
                            $q->select('id', 'name');
                        }
                    ]
                )
                ->get();
            return $levels;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }

    /**
     * Getting Education Program Dropdown Via Level Id
     * @param int $levelId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEducationProgramDropdownViaLevelId(int $levelId)
    {
        try {
            $programs = EducationCycle::select('id', 'name', 'education_level_id')
                ->with(
                    [
                        'educationProgram' => function ($q) {
                            $q->select('id', 'name', 'education_cycle_id');
                        }
                    ]
                )
                ->where('education_level_id', $levelId)
                ->get()
                ->toArray();

            $data = [];
            foreach ($programs as $key => $program) {
                $educationProgram = $program['education_program'];
                foreach ($educationProgram as $key => $value) {
                    $data[] = [
                        'key' => $value['id'],
                        'value' => $program['name'] . " - " . $value['name'],
                    ];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOption()
    {
        try {
            $option = ExaminationOption::select('id', 'name', 'code')->get();
            return $option;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Option not found");
        }
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEducationCertification()
    {
        try {
            $certification = EducationCertification::select('id', 'name')->get();
            return $certification;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Option not found");
        }
    }

    /**
     * Getting Education Stages Dropdown
     * @return JsonResponse
     */
    public function getEducationStageDropdown()
    {
        try {
            $stages = EducationStage::select('id', 'name', 'code')
                        ->where('visible', config('constants.visibleValue.visibleCode'))
                        ->orderBy('order', 'desc')
                        ->get();
            return $stages;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education stage dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education stage dropdown from DB");
        }
    }

    /**
     * Getting Education Level Dropdown
     * @return JsonResponse
     */
    public function educationLevelForProgrTab()
    {
        try {
            $levels = EducationLevel::select('id', 'name')->where('visible', config('constants.visibleValue.visibleCode'))->get();
            return $levels;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education Level dropdown from DB");
        }
    }

    /**
     * Getting Education Grades Dropdown
     * @param int $programId
     * @return JsonResponse
     */
    public function getEducationGradeDropdownViaProgramId(int $programId)
    {
        try {
            $grades = EducationGrade::select('id', 'name', 'code')
                        ->where('education_programme_id', $programId)
                        ->where('visible', config('constants.visibleValue.visibleCode'))
                        ->orderBy('order', 'desc')
                        ->get();
            $data = [];
            foreach ($grades as $key => $grade) {
                $data[] = [
                    'key' => $grade['id'],
                    'value' => $grade['code'] . " - " . $grade['name'],
                ];
            }

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education grade details in DB");
        }
    }

    /**
     * Getting Education Subjects Dropdown
     * @return JsonResponse
     */
    public function getEducationSubjectDropdown()
    {
        try {
            $subjects = EducationSubject::select('id', 'code', 'name')
                            ->where('visible', config('constants.visibleValue.visibleCode'))
                            ->get();
            return $subjects;
        } catch (\Exception $e) {
            Log::error(
                'Failed to store education grade subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store education grade subject details");
        }
    }

    /**
     * Getting Area administrative Dropdown
     * @return JsonResponse
     */
    public function getAreaAdministrativeDropdown()
    {
        try {
            $areas = AreaAdministrative::select('id as key', 'name as value')
                        ->where('is_main_country', 1)
                        ->where('visible', config('constants.visibleValue.visibleCode'))
                        ->get()
                        ->toArray();
            return $areas;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative dropdown");
        }
    }

    /**
     * Getting Country Dropdown
     * @return JsonResponse
     */
    public function getCountryDropdown()
    {
        try {
            $country = Countrie::select('id as key', 'name as value')
                        ->where('visible', config('constants.visibleValue.visibleCode'))
                        ->orderBy('order')
                        ->get();
            return $country;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get country dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get country dropdown");
        }
    }

    /**
     * Getting System Configuration Dropdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function systemConfigurationDropdown()
    {
        try {
            $item = ConfigItem::select('id', 'type')->get();
            return $item;
        } catch (\Exception $e) {
            Log::error(
                'Failed to System Configuration Dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get System Configuration Dropdown");
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function examinationSubject()
    {
        try {
            $item = ExaminationSubject::select('id', 'name', 'code')->get();
            return $item;
        } catch (\Exception $e) {
            Log::error(
                'Failed to System Configuration Dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get  Dropdown");
        }
    }
}
