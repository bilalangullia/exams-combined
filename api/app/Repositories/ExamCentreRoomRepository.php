<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationCentreRoom;
use App\Models\ExaminationCentreRoomExaminations;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class ExamCentreRoomRepository extends Controller
{
    /**
     * Getting Exam Centre Rooms List
     * @param array $search
     * @param string $examCentreId
     * @return mixed
     */
    public function getExamCentreRoomList(array $search, string $examCentreId)
    {
        try {
            $rooms = ExaminationCentreRoom::select('id', 'name', 'size', 'number_of_seats', 'examination_centre_id');

            if (isset($search['keyword'])) {
                $rooms->where('name', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('size', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('number_of_seats', 'LIKE', '%' . $search['keyword'] . '%');
            }

            $rooms = $rooms->where('examination_centre_id', $examCentreId)
                ->get();

            return $rooms;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room list not found');
        }
    }

    /**
     * Getting Exam Centre Room Details
     * @param string $roomId
     * @return mixed
     */
    public function getExamCentreRoomDetails(string $roomId)
    {
        try {
            $roomDetails = ExaminationCentreRoom::where('id', $roomId)->select(
                'id',
                'name',
                'size',
                'number_of_seats',
                'examination_centre_id',
                'created_user_id',
                'modified_user_id',
                'modified',
                'created'
            )
                ->with(
                    [
                        'examCentre' => function ($query) {
                            $query->select('id', 'name', 'examination_id');
                        },
                        'examCentre.exams' => function ($query) {
                            $query->select('id', 'name', 'code', 'academic_period_id');
                        },
                        'examCentre.exams.academicPeriod' => function ($query) {
                            $query->select('id', 'name');
                        },
                        'createdByUser' => function ($query) {
                            $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                ->where('status', config('constants.createdByUser.status'));
                        },
                        'modifiedUser' => function ($query) {
                            $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                ->where('status', config('constants.modifiedByUser.status'));
                        }
                    ]
                )
                ->where('id', $roomId)
                ->first();

            return $roomDetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room details not found');
        }
    }

    /**
     * Updating Exam Centre Room Details
     * @param array $data
     * @return int
     */
    public function examCentreRoomUpdate(array $data)
    {
        try {
            $is_exists = ExaminationCentreRoom::where('id', $data['id'])->first();
            if ($is_exists) {
                $roomData['name'] = $data['name'];
                $roomData['size'] = $data['size'];
                $roomData['number_of_seats'] = $data['number_of_seats'];
                $roomData['modified_user_id'] = JWTAuth::user()->id;
                $roomData['modified'] = Carbon::now()->toDateTimeString();
                ExaminationCentreRoom::where('id', $data['id'])->update($roomData);
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room details not updated');
        }
    }

    /**
     * Storing Exam Centre Room Details
     * @param array $data
     * @return int
     */
    public function examCentreRoomStore(array $data)
    {
        DB::beginTransaction();
        try {
            $roomData['name'] = $data['name'];
            $roomData['size'] = $data['size'];
            $roomData['number_of_seats'] = $data['number_of_seats'];
            $roomData['examination_centre_id'] = $data['examination_centre_id'];
            $roomData['created_user_id'] = JWTAuth::user()->id;
            $roomData['created'] = Carbon::now()->toDateTimeString();
            $room = ExaminationCentreRoom::insert($roomData);
            DB::commit();
            if ($room) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse('Exam centre room details not stored');
        }
    }

    /**
     * Deleting exam centre room
     * @param int $roomId
     * @param int $examCentreId
     * @return JsonResponse|int
     */
    public function deleteExamCentreRoom(int $roomId, int $examCentreId)
    {
        try {
            if ($examCentreId == 0) {
                return ExaminationCentreRoom::where('id', $roomId)->delete();
            } elseif ($roomId == 0) {
                return ExaminationCentreRoom::where('examination_centre_id', $examCentreId)->delete();
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre room.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete exam centre room from DB");
        }
    }
}
