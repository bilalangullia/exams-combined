<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationSubject;
use App\Models\ExaminationCentre;

class ExamCentreSubjectRepository extends Controller
{
    /**
     * Getting Subject List.
     * @param array $search
     * @param string $examinationId
     * @return array|JsonResponse
     */
    public function getExamCentreSubjectList(array $search, string $examinationId)
    {
        try {
            $subjects = ExaminationSubject::where('examination_id', $examinationId)
                        ->select('id', 'code', 'name');

            if (isset($search['keyword'])) {
                $subjects->where(function ($query) use ($search) {
                    $query->where('code', 'LIKE', '%' . $search['keyword'] . '%')
                            ->orWhere('name', 'LIKE', '%' . $search['keyword'] . '%');
                });
            }
            $subjects = $subjects->get()->toArray();
            return $subjects;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subjects list in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Subjects List Not Found");
        }
    }

    /**
     * Getting Subject Details
     * @param string $examCentreId
     * @param $subjectId
     * @return JsonResponse
     */
    public function examCentreSubjectView(string $examCentreId, $subjectId)
    {
        try {
            //dd($examCentreId, $subjectId);
            $subject = ExaminationCentre::where('id', $examCentreId)
                        ->select('id', 'name', 'code', 'examination_id')
                        ->with(
                            [
                                'exams' => function ($query) {
                                    $query->select('id', 'code', 'name', 'academic_period_id');
                                },
                                'exams.examinationSubject' => function ($query) use ($subjectId) {
                                    $query->where('id', $subjectId)
                                        ->select(
                                            'id',
                                            'code',
                                            'name',
                                            'examination_id',
                                            'created_user_id',
                                            'created',
                                            'modified_user_id',
                                            'modified',
                                            'education_subject_id'
                                        );
                                },
                                'exams.examinationSubject.securityUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                    ->where('status', config('constants.createdByUser.status'));
                                },
                                'exams.examinationSubject.modifiedUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.modifiedByUser.status'));
                                },'exams.examinationSubject.educationSubject' => function ($query) {
                                    $query->select('id', 'name')
                                        ->where('visible', config('constants.visibleValue.visibleCode'));
                                },
                                'exams.academicPeriod' => function ($query) {
                                    $query->select('id', 'start_year');
                                }
                            ]
                        )
                        ->first();
            
            if ($subject) {
                $subject = $subject->toArray();
            }
            return $subject;

        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subjects details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Subjects Details Not Found");
        }
    }
}
