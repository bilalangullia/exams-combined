<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExaminationFeeRequest;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsFee;
use App\Models\ExaminationStudentsFeesPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;

class RegistrationFeeRepository extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function feeList(Request $request)
    {
        try {
            $data = $request->all();
            $paidStudents = [];
            $unPaidStudents = [];
            $listing = ExaminationStudent::select(
                'id',
                'candidate_id',
                'student_id',
                'examination_centre_id',
                'examination_id'
            )
                ->with(
                    [
                        'examinationCentre' => function ($query) {
                            $query->select('id', 'name', 'code');
                        },
                        'securityUser' => function ($query) {
                            $query->select(
                                'id',
                                'first_name',
                                'middle_name',
                                'third_name',
                                'last_name',
                                'openemis_no',
                                'first_name'
                            );
                        },
                        'examinationFee' => function ($query) {
                            $query->select('quantity', 'examination_fees_id', 'examination_students_id', 'amount')
                                ->withPivot('id');
                        }
                    ]
                )
                ->where('examination_id', $data['examination_id'])
                ->where('examination_centre_id', $data['examination_centre_id']);
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $listing;
                $total = $userCount->count();
                $listing->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $listing;
                $total = $userCount->count();
            }
            if (isset($request['keyword'])) {
                $search = $listing->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'securityUser',
                            function ($query) use ($request) {
                                $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('middle_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('third_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('openemis_no', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationCentre',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->where('code', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $students = $listing->get();

            foreach ($students as $key => $student) {
                $totalFeeAmount = 0;
                $totalPaymentAmount = 0;
                if (count($student['examinationFee']) > 0) {
                    foreach ($student['examinationFee'] as $key => $fee) {
                        $amount = $fee['quantity'] * $fee['amount'];
                        $totalFeeAmount = $totalFeeAmount + $amount;

                        $feeId = $fee['pivot']['id'];
                        $payment = ExaminationStudentsFeesPayment::where('examination_student_fees_id', $feeId)->sum(
                            'amount'
                        );
                        $totalPaymentAmount = $totalPaymentAmount + $payment;
                    }

                    if ($totalPaymentAmount == $totalFeeAmount) {
                        $student['payment_status'] = 'Paid';
                    } else {
                        $student['payment_status'] = 'Unpaid';
                    }
                } else {
                    $student['payment_status'] = 'Unpaid';
                }
            }

            return array("totalRecord" => $total, "record" => $students);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Fee list not found");
        }
    }

    /**
     *  Get examination >> fee  details
     * @param int $feeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function feeDetails(string $candId, string $examId)
    {
        try {
            $viewdata = ExaminationStudent::where('candidate_id', $candId)->where('examination_id', $examId)->get();
            return $viewdata;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Fee Details not found");
        }
    }

    /**
     * update adminstration >examination >>fee
     * @param ExaminationFeeRequest $request
     * @param int $feeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function feeAdd(Request $request, string $candId)
    {
        DB::beginTransaction();
        try {
            $req = $request->all();
            $StuId = ExaminationStudent::where('candidate_id', $candId)->first()->id;

            if (count($req['fee_type']) > 0) {
                $feeTypes = $req['fee_type'];
                foreach ($feeTypes as $key => $feeType) {
                    $is_exists = ExaminationStudentsFee::where('examination_students_id', $StuId)
                                ->where('examination_fees_id', $feeType['fee_id'])
                                ->first();

                    if (isset($is_exists)) {
                        $updateArr['quantity'] = $feeType['quantity'];
                        $updateArr['modified_user_id'] = config('constants.modifiedByUser.id');
                        $updateArr['modified'] = Carbon::now()->toDateTimeString();
                        ExaminationStudentsFee::where('id', $is_exists->id)->update($updateArr);
                        $feeId = $is_exists->id;
                    } else {
                        $insertArr['examination_fees_id'] = $feeType['fee_id'];
                        $insertArr['quantity'] = $feeType['quantity'];
                        $insertArr['examination_students_id'] = $StuId;
                        $insertArr['created_user_id'] = config('constants.modifiedByUser.id');
                        $insertArr['created'] = Carbon::now()->toDateTimeString();
                        $feeId = ExaminationStudentsFee::insertGetId($insertArr);
                    }
                }

                if (count($request->payment) > 0) {
                    $payments = $request->payment;
                    foreach ($payments as $key => $payment) {
                        $pay[] = [
                            'examination_student_fees_id' => $feeId ?? 0,
                            'date' => $payment['date'],
                            'receipt' => $payment['receipt'],
                            'amount' => $payment['amount'],
                            'date' => $payment['date'],
                            'created' => Carbon::now()->toDateTimeString(),
                            'created_user_id' => config('constants.createdUser.user_id'),
                        ];
                    }
                    ExaminationStudentsFeesPayment::insert($pay);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Fee Details Not Added");
        }
    }
}
