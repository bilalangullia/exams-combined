<?php

namespace App\Repositories\Fee;

use App\Models\SecurityUser;
use Illuminate\Http\Request;

class FeeRepository
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function feeList(Request $request)
    {
        $feeList = SecurityUser::select('id', 'openemis_no', 'first_name', 'middle_name', 'third_name', 'last_name')->
        with(
            'examinationStudent:student_id,id,candidate_id,examination_centre_id,examination_id',
            'examinationStudent.examinationStudentsFee:examination_students_id,id,amount,examination_fees_id',
            'examinationStudent.examinationCentre:id,code,name,academic_period_id',
            'examinationStudent.examinationStudentsFee.examinationFee:id,amount',
            'examinationStudent.examName:id,code,name',
            'examinationStudent.examinationCentre.academicPeriod:id,name'
        )->whereHas(
            'examinationStudent.examinationCentre',
            function ($query) use ($request) {
                $query->where('id', $request->examination_centre_id);
            }
        )->whereHas(
            'examinationStudent.examName',
            function ($query) use ($request) {
                $query->where('id', $request->examination_id);
            }
        )->whereHas(
            'examinationStudent.examinationCentre.academicPeriod',
            function ($query) use ($request) {
                $query->where('id', $request->academic_period_id);
            }
        )->get()->map(
            function ($item, $key) {
                $var = $item['examinationStudent']['examinationStudentsFee']['amount'];
                $var1 = $item['examinationStudent']['examinationStudentsFee']['examinationFee']['amount'];
                $due = $var1 - $var;
                return [
                    "candidate_id" => $item['examinationStudent']['candidate_id'],
                    "openemis_no" => $item['openemis_no'],
                    "name" => $item['full_name'],
                    "exam_centre" => $item['examinationStudent']['examinationCentre']['full_name'],
                    "outstanding_fee" => $due,
                    "status" => ($due && $due != $var1) ? 'Unpaid' : ((!$var) ? 'Unpaid' : 'Paid'),
                ];
            }
        );
        return $feeList;
    }
}