<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationGradingType;
use App\Models\ExaminationGradingOption;
use Tymon\JWTAuth\Facades\JWTAuth;

class GradingTypeRepository extends Controller
{
    /**
     * Grading Type Listing
     * @param array $search
     * @return mixed
     */
    public function gradingTypeList(array $search)
    {
        try {
            $gradingType = ExaminationGradingType::select(['code', 'name', 'max', 'result_type', 'id'])
                ->where('visible', config('constants.visibleValue.visibleCode'));
            if (isset($search['keyword'])) {
                $gradingType->where(function ($query) use ($search) {
                    $query->where('code', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('name', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('pass_mark', 'LIKE', '%' . $search['keyword'] . '%')
                        ->orWhere('max', 'LIKE', '%' . $search['keyword'] . '%');
                });
            }
            $gradingType = $gradingType->get();

            return $gradingType;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }


    /**
     * Getting grading type details via grading type id
     * @param int $gradingTypeId
     * @return JsonResponse
     */
    public function getGradingTypeDetails(int $gradingTypeId)
    {
        try {
            $gradingTypeDetails = ExaminationGradingType::select(
                'id',
                'code',
                'name',
                'pass_mark',
                'max',
                'visible',
                'result_type',
                'created_user_id',
                'created'
            )
                ->with(
                    [
                        'examinationGradingOption' => function ($query) {
                            $query->select('code', 'examination_grading_type_id', 'name', 'description', 'min', 'max', 'points', 'id')
                                ->where('visible', config('constants.visibleValue.visibleCode'))
                                ->orderBy('order', 'asc');
                        },
                        'createdByUser' => function ($query) {
                            $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')->where('status', 1);
                        },
                        'modifiedUser' => function ($query) {
                            $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')->where('status', 1);
                        }
                    ]
                )
                ->where('id', $gradingTypeId)
                ->first();

            return $gradingTypeDetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch grading type details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }


    /**
     * Updating grading type details
     * @param array $request
     * @return JsonResponse
     */
    public function updateGradingType(array $request)
    {
        DB::beginTransaction();
        try {
            $optionArr = [];
            $gradingType['name'] = $request['name'];
            $gradingType['code'] = $request['code'];
            $gradingType['pass_mark'] = $request['pass_mark'];
            $gradingType['result_type'] = $request['result_type'];
            $gradingType['max'] = $request['max'];
            $gradingType['modified'] = Carbon::now()->toDateTimeString();
            $gradingType['modified_user_id'] = JWTAuth::user()->id;
            
            ExaminationGradingType::where('id', $request['id'])->update($gradingType);

            ExaminationGradingOption::where('examination_grading_type_id', $request['id'])->delete();
            if ($request['gradingOption'] && count($request['gradingOption']) > 0) {
                $gradingOption = $request['gradingOption'];
                foreach ($gradingOption as $key => $option) {
                    $option['code'] = $option['name'];
                    $option['order'] = $key + 1;
                    $option['examination_grading_type_id'] = $request['id'];
                    $option['created'] = Carbon::now()->toDateTimeString();
                    $option['created_user_id'] = JWTAuth::user()->id;
                    $option['modified'] = Carbon::now()->toDateTimeString();
                    $option['modified_user_id'] = JWTAuth::user()->id;
                    $optionArr[$key] = $option;
                }
                ExaminationGradingOption::insert($optionArr);
            }
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update grading type in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse('Failed to update grading type in DB');
        }
    }


    /**
     * Deleting grading type listing
     * @param int $gradingTypeId
     * @return JsonResponse|int
     */
    public function gradingTypeDelete(int $gradingTypeId, int $parentId)
    {
        try {
            return ExaminationGradingType::where('id', $gradingTypeId)->delete();
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete grading type from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to delete grading type from DB');
        }
    }

    /**
     * Storing Grading type details
     * @param array $data
     * @return JsonResponse
     */
    public function gradingTypeStore(array $data)
    {
        DB::beginTransaction();
        try {
            $optionArr = [];
            $gradingTypeArr['name'] = $data['name'];
            $gradingTypeArr['code'] = $data['code'];
            $gradingTypeArr['pass_mark'] = $data['pass_mark'];
            $gradingTypeArr['max'] = $data['max'];
            $gradingTypeArr['result_type'] = $data['result_type'];
            $gradingTypeArr['created'] = Carbon::now()->toDateTimeString();
            $gradingTypeArr['created_user_id'] = JWTAuth::user()->id;
            $gradingTypeId = ExaminationGradingType::insertGetId($gradingTypeArr);
            if ($data['gradingOption'] && count($data['gradingOption']) > 0) {
                $gradingOption = $data['gradingOption'];
                foreach ($gradingOption as $key => $option) {
                    $optionArr[] = [
                        'code' => $option['name'],
                        'name' => $option['name'],
                        'description' => $option['description']??null,
                        'min' => $option['min']??null,
                        'max' => $option['max']??null,
                        'order' => $key + 1,
                        'points' => $option['points']??null,
                        'examination_grading_type_id' => $gradingTypeId,
                        'created' => Carbon::now()->toDateTimeString(),
                        'created_user_id' => JWTAuth::user()->id,
                    ];
                }
                ExaminationGradingOption::insert($optionArr);
            }
            DB::commit();
            return 1;
        } catch(\Exception $e) {
            Log::error(
                'Failed to store grading type in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse('Failed to store grading type in DB');
        }
    }

    /**
     * Checking if safe to delete Grading type.
     * @param int $gradingTypeId
     * @return bool
     */
    public function checkIfSafeToDeleteGradingType(int $gradingTypeId)
    {
        try {
            $gradingType = ExaminationGradingType::find($gradingTypeId);
            
            if ($gradingType) {
                $gradingOption = $gradingType->examinationGradingOption()->exists();
                if ($gradingOption) {
                    return false;
                }
                $examinationOption = $gradingType->examinationOption()->exists();
                if ($examinationOption) {
                    return false;
                }
                $examinationComponent = $gradingType->examinationComponent()->exists();
                if ($examinationComponent) {
                    return false;
                }
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to check Grading type Dependencies',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to check Grading type Dependencies");
        }
    }

    /**
     * Importing Grading options
     * @param array $excel
     * @param array $requestParameter
     * @return JsonResponse
     */
    public function gradingTypeImport(array $excel, array $requestParameter)
    {
        DB::beginTransaction();
        try {
            $i = -1;
            $validation = [];
            $stored_data = [];
            $add_data = [];
            $importResponse = [];
            $insertArr = [];
            $order = 0;
            foreach ($excel[0] as $key => $row) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }
                
                if (!$row[0]) {//Grading Type
                    $label = $excel[0][1][0];
                    $errors[$label] = 'Grading Type id required';
                }

                if (!$row[1]) {//Name
                    $label = $excel[0][1][1];
                    $errors[$label] = 'Name required';
                }

                if (!$row[3]) {//Points
                    $label = $excel[0][1][3];
                    $errors[$label] = 'Points required';
                }

                if (!$row[4]) {//Min
                    $label = $excel[0][1][4];
                    $errors[$label] = 'Min required';
                }

                if (!$row[5]) {//Max
                    $label = $excel[0][1][5];
                    $errors[$label] = 'Max required';
                }

                if (isset($row[0])) {
                    $gradingType = $this->getGradingType($row[0]);
                }

                if (count($errors) > 0) {
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            $excel[0][1][0] => $row[0],
                            $excel[0][1][1] => $row[1],
                            $excel[0][1][2] => $row[2],
                            $excel[0][1][3] => $row[3],
                            $excel[0][1][4] => $row[4],
                            $excel[0][1][5] => $row[5],
                        ],
                        'errors' => $errors
                    ];
                } else {
                    if (isset($gradingType)) {
                            $order = $this->getLargestOptionOrder($row[0]);
                            $insertArr = [
                                'examination_grading_type_id' => $row[0],
                                'name' => $row[1],
                                'code' => $row[1],
                                'description' => $row[2],
                                'points' => $row[3],
                                'min' => $row[4],
                                'max' => $row[5],
                                'order' => $order,
                                'created_user_id' => JWTAuth::user()->id,
                                'created' => Carbon::now()->toDateTimeString()
                            ];
                            ExaminationGradingOption::insert($insertArr);
                            $add_data[] = [
                                'row_number' => $i,
                                'data' => [
                                    $excel[0][1][0] => $row[0],
                                    $excel[0][1][1] => $row[1],
                                    $excel[0][1][2] => $row[2],
                                    $excel[0][1][3] => $row[3],
                                    $excel[0][1][4] => $row[4],
                                    $excel[0][1][5] => $row[5],
                                ],
                                'errors' => $errors
                            ];
                    } else {
                        $label = $excel[0][1][0];
                        $errors[$label] = 'Grading type does not exists.';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                $excel[0][1][0] => $row[0],
                                $excel[0][1][1] => $row[1],
                                $excel[0][1][2] => $row[2],
                                $excel[0][1][3] => $row[3],
                                $excel[0][1][4] => $row[4],
                                $excel[0][1][5] => $row[5],
                            ],
                            'errors' => $errors
                        ];
                    }
                }
            }

            
            $importResponse = [
                'total_count' => count($excel[0]) - 2,
                'records_added' => [
                    'count' => count($add_data),
                    'rows' => $add_data,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];
            
            DB::commit();
            return $importResponse;
        } catch (\Exception $e) {
            Log::error(
                'Failed to import grading type option in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to import grading type option in DB');
        }
    }

    /**
     * Getting Grading Type
     * @param int $id
     */
    public function getGradingType(int $id)
    {
        try {
            $gradingType = ExaminationGradingType::where('id', $id)->select('id')->first();
            return $gradingType;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get Grading Type',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to get Grading Type');
        }
    }

   /**
     * Getting Grading Option Max order
     * @param int $id
     */
    public function getLargestOptionOrder(int $id)
    {
        try {
            $largestOrder = 0;
            $order = ExaminationGradingOption::where('examination_grading_type_id', $id)
                        ->max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get Grading Option Order',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to get Grading Option Order');
        }
    }
}
