<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Str;
use App\Models\AreaLevel;
use App\Models\Area;
use App\Models\AreaAdministrativeLevel;
use App\Models\AreaAdministrative;

class AdministrativeBoundaryRepository extends Controller
{
    /**
     * Getting Area level (education) List
     * @param array $data
     * @return JsonResponse
     */
    public function areaLevelEducation(array $data)
    {
        try {
            $areaLevels = AreaLevel::select('id', 'name', 'level');

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $areaLevels->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%")
                        ->orWhere('level', 'LIKE', "%" . $keyword . "%");
                    });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $areaLevelsCount = $areaLevels;
                $total = $areaLevelsCount->count();
                $areaLevels->skip($data['start'])
                            ->take($data['end'] - $data['start']);
            } else {
                $areaLevelsCount = $areaLevels;
                $total = $areaLevelsCount->count();
            }
            $list = $areaLevels->get()->toArray();
            
            $data['list'] = $list;
            $data['total'] = $total;
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level education list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level education list");
        }
    }

    /**
     * Getting Area level (education) Details
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelEducationView(int $levelId)
    {
        try {
            $areaLevel = AreaLevel::select('id', 
                            'name', 
                            'level', 
                            'modified_user_id', 
                            'modified', 
                            'created_user_id', 
                            'created'
                        )
                        ->with(
                            [
                                'createdByUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.createdByUser.status'));
                                },
                                'modifiedUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.modifiedByUser.status'));
                                }
                            ]
                        )
                        ->where('id', $levelId)
                        ->first();
            return $areaLevel;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level education details");
        }
    }

    /**
     * Updating Area level (education) Details
     * @param array $data
     * @return JsonResponse
     */
    public function areaLevelEducationUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $updateArr['name'] = $data['name'];
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            $updateArr['modified'] = Carbon::now()->toDateTimeString();
            $update = AreaLevel::where('id', $data['id'])->update($updateArr);
            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area level education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update area level education details");
        }
    }

    /**
     * Storing Area level (education) Details
     * @param array $data
     * @return JsonResponse
     */
    public function areaLevelEducationStore(array $data)
    {
        DB::beginTransaction();
        try {
            $areaLevel = AreaLevel::max('level')??0;
            $level = $areaLevel + 1;
            
            $insertArr['name'] = $data['name'];
            $insertArr['level'] = $level;
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();
            $store = AreaLevel::insert($insertArr);
            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store area level education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store area level education details");
        }
    }

    /**
     * Getting Area (education) List
     * @param array $data
     * @return JsonResponse
     */
    public function areaEducationList(array $data)
    {
        try {
            $parent = [];
            $areas = Area::select(
                        'id', 
                        'name', 
                        'code', 
                        'visible',
                        'area_level_id'
                    )
                    ->with(
                        [
                            'areaLevel' => function ($q) {
                                $q->select('id', 'name');
                            }
                        ]
                    );
            if (isset($data['parent_id'])) {
                $areas->where('parent_id', $data['parent_id']);
                $parent = Area::select('id as key', 'name as value')
                            ->where('id', $data['parent_id'])
                            ->first();
            }

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $areas->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%")
                    ->orWhere('code', 'LIKE', "%" . $keyword . "%")
                    ->orWhereHas(
                        'areaLevel',
                        function ($query) use ($keyword) {
                            $query->where('name', 'LIKE', "%" . $keyword . "%");
                        }
                    );
                });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $areaCount = $areas;
                $total = $areaCount->count();
                $areas->skip($data['start'])
                            ->take($data['end'] - $data['start']);
            } else {
                $areaCount = $areas;
                $total = $areaCount->count();
            }
                        
            $list = $areas->get();
            $data['list'] = $list;
            $data['total'] = $total;
            $data['parent'] = $parent;
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area education list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area education list");
        }
    }

    /**
     * Getting Area (education) Details
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaEducationView(int $areaId)
    {
        try {
            $areaData = Area::select(
                        'id', 
                        'name', 
                        'code', 
                        'visible',
                        'area_level_id',
                        'parent_id',
                        'modified_user_id',
                        'modified',
                        'created_user_id',
                        'created'
                    )
                    ->with(
                        [
                            'areaLevel' => function ($q) {
                                $q->select('id', 'name');
                            },
                            'parentArea' => function ($q) {
                                $q->select('id', 'name');
                            },
                            'createdByUser' => function ($q) {
                                $q->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                    ->where('status', config('constants.createdByUser.status'));
                            },
                            'modifiedUser' => function ($q) {
                                $q->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                    ->where('status', config('constants.modifiedByUser.status'));
                            }
                        ]
                    )
                    ->where('id', $areaId)
                    ->first();
            return $areaData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area education details");
        }
    }

    /**
     * Updating Area (education) Details
     * @param array $data
     * @return JsonResponse
     */
    public function areaEducationUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $updateArr['name'] = $data['name'];
            $updateArr['code'] = $data['code'];
            $updateArr['visible'] = $data['visible'];
            $updateArr['area_level_id'] = $data['area_level'];
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            $updateArr['modified'] = Carbon::now()->toDateTimeString();

            $update = Area::where('id', $data['id'])->update($updateArr);
            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update area education details");
        }
    }

    /**
     * Storing Area (education) Details
     * @param array $data
     * @return JsonResponse
     */
    public function areaEducationStore(array $data)
    {
        DB::beginTransaction();
        try {
            $order = $this->getLargestAreaOrder();
            $lftRghtArr = $this->getLftRghtAreaValue();
            
            $insertArr['name'] = $data['name'];
            $insertArr['code'] = $data['code'];
            $insertArr['lft'] = $lftRghtArr['lft'];
            $insertArr['rght'] = $lftRghtArr['rght'];
            $insertArr['area_level_id'] = $data['area_level'];
            $insertArr['parent_id'] = $data['parent_id'];
            $insertArr['order'] = $order;
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();
            
            $store = Area::insert($insertArr);
            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update area education details");
        }
    }

    /**
     * Get lft rght Area Value
     * @param array $array
     */
    public function getLftRghtAreaValue()
    {
        try {
            $areaRght = Area::max('rght');
            if ($areaRght) {
                $array['lft'] = $areaRght + 1;
                $array['rght'] = $array['lft'] + 1;
            } else {
                $array['lft'] = 1;
                $array['rght'] = $array['lft'] + 1;
            }
            return $array;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get lft and rght value',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get lft and rght value");
        }
    }

    /**
     * Getting largest area order
     * @return int $largestOrder
     */
    public function getLargestAreaOrder()
    {
        try {
            $largestOrder = 0;
            $order = Area::max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest grade order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch largest grade order from DB");
        }
    }

    /**
     * Getting Area Level Dropdown via parent_id
     * @param int $parentId
     * @return JsonResponse
     */
    public function areaLevelDropdown(int $parentId)
    {
        try {
            $area = Area::select('id', 'area_level_id')
                        ->with(
                        [
                            'areaLevel' => function ($q) {
                                $q->select('id', 'level');
                            }
                        ]
                    )
                    ->where('id', $parentId)
                    ->first();
            
            $areaLevel = [];
            if ($area) {
                $areaLevel = AreaLevel::select('id as key', 'name as value')
                            ->where('level', '>', $area['areaLevel']['level'])
                            ->get()
                            ->toArray();
            }
            
            return $areaLevel;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level dropdown");
        }
    }

    /**
     * Getting Area level (Administrative) Details
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelAdministrativeView(int $levelId)
    {
        try {
            $areaLevel = AreaAdministrativeLevel::select(
                            'id', 
                            'level', 
                            'name', 
                            'modified_user_id',
                            'modified',
                            'created_user_id',
                            'created'
                        )
                        ->with(
                            [
                                'createdByUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.createdByUser.status'));
                                },
                                'modifiedUser' => function ($query) {
                                    $query->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.modifiedByUser.status'));
                                }
                            ]
                        )
                        ->where('id', $levelId)
                        ->first();
            return $areaLevel;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level details");
        }
    }

    /**
     * Getting Area level (Administrative) List
     * @param array $data
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaLevelAdministrativeList(array $data, int $areaId)
    {
        try {
            $areaLevel = AreaAdministrativeLevel::select(
                                'id',
                                'name',
                                'level'
                            )
                            ->where('area_administrative_id', $areaId);
            
            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $areaLevel->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%")
                    ->orWhere('level', 'LIKE', "%" . $keyword . "%");
                });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $areaLevelCount = $areaLevel;
                $total = $areaLevelCount->count();
                $areaLevel->skip($data['start'])
                            ->take($data['end'] - $data['start']);
            } else {
                $areaLevelCount = $areaLevel;
                $total = $areaLevelCount->count();
            }
            
            $list = $areaLevel->get();
            $data['list'] = $list;
            $data['total'] = $total;
            
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level list");
        }
    }

    /**
     * Storing Area level (Administrative) details
     * @param array $data
     * @return JsonResponse
     */
    public function areaLevelAdministrativeStore(array $data)
    {
        DB::beginTransaction();
        try {
            $level = $this->getAreaAdministrativeLevel($data['area_administrative_id']);

            $insertArr['name'] = $data['name'];
            $insertArr['area_administrative_id'] = $data['area_administrative_id'];
            $insertArr['level'] = $level;
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();

            $store = AreaAdministrativeLevel::insert($insertArr);
            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store area level details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store area level details");
        }
    }

    /**
     * Getting Area Administrative Level
     * @return int $level
     */
    public function getAreaAdministrativeLevel($areaAdministrativeId = 0)
    {
        try {
            $level = AreaAdministrativeLevel::where('area_administrative_id', $areaAdministrativeId)
                        ->first()
                        ->level??0;
            return $level;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest grade order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch largest grade order from DB");
        }
    }

    /**
     * Updating Area level (Administrative) details
     * @param array $data
     * @return JsonResponse
     */
    public function areaLevelAdministrativeUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $updateArr['name'] = $data['name'];
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            $updateArr['modified'] = Carbon::now()->toDateTimeString();
            $update = AreaAdministrativeLevel::where('id', $data['id'])->update($updateArr);
            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area level details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update area level details");
        }
    }

    /**
     * Getting Area (Administrative) List
     * @param array $data
     * @return JsonResponse
     */
    public function areaAdministrativeList(array $data)
    {
        try {
            $parent = [];
            $areaList = AreaAdministrative::select(
                            'id', 
                            'code', 
                            'name', 
                            'is_main_country', 
                            'area_administrative_level_id',
                            'visible',
                            'parent_id'
                        )
                        ->with(
                            [
                                'areaAdministrativeLevel' => function ($q) {
                                    $q->select('id', 'name');
                                },
                                'parentArea' => function ($q) {
                                    $q->select('id', 'name');
                                }
                            ]
                        );
            
            if (isset($data['parent_id'])) {
                $areaList->where('parent_id', $data['parent_id']);
                $parent = AreaAdministrative::select('id as key', 'name as value')
                                ->where('id', $data['parent_id'])
                                ->first();
            }

            if (isset($data['keyword'])) {
                $keyword = $data['keyword'];
                unset($data['keyword']);
                $areaList->where(function ($q) use ($keyword) {
                    $q->where('name', 'LIKE', "%" . $keyword . "%")
                    ->orWhere('code', 'LIKE', "%" . $keyword . "%")
                    ->orWhereHas(
                        'areaAdministrativeLevel',
                        function ($query) use ($keyword) {
                            $query->where('name', 'LIKE', "%" . $keyword . "%");
                        }
                    );
                });
            }

            if (isset($data['start']) && isset($data['end'])) {
                $areaCount = $areaList;
                $total = $areaCount->count();
                $areaList->skip($data['start'])
                            ->take($data['end'] - $data['start']);
            } else {
                $areaCount = $areaList;
                $total = $areaCount->count();
            }

            $list = $areaList->get();
            $data['list'] = $list;
            $data['total'] = $total;
            $data['parent'] = $parent;

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative list");
        }
    }

    /**
     * Getting Area (Administrative) Details
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaAdministrativeView(int $areaId)
    {
        try {
            $area = AreaAdministrative::select(
                            'id', 
                            'code', 
                            'name', 
                            'is_main_country', 
                            'area_administrative_level_id',
                            'visible',
                            'modified_user_id',
                            'modified',
                            'created_user_id',
                            'created',
                            'parent_id'
                        )
                        ->with(
                            [
                                'areaAdministrativeLevel' => function ($q) {
                                    $q->select('id', 'name');
                                },
                                'createdByUser' => function ($q) {
                                    $q->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.createdByUser.status'));
                                },
                                'modifiedUser' => function ($q) {
                                    $q->select('id', 'first_name', 'middle_name', 'third_name', 'last_name')
                                        ->where('status', config('constants.modifiedByUser.status'));
                                },
                                'parentArea' => function ($q) {
                                    $q->select('id', 'name');
                                }
                            ]
                        )
                        ->where('id', $areaId)
                        ->first();
            return $area;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative details");
        }
    }

    /**
     * Getting Area Level (Administrative) Dropdown
     * @param int $parentId
     * @return JsonResponse
     */
    public function areaAdministrativeLevelDropdown(int $parentId)
    {
        try {
            $areaLevel = AreaAdministrativeLevel::select('id as key', 'name as value')
                            ->where('area_administrative_id', $parentId)
                            ->get()
                            ->toArray();
            return $areaLevel;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative level dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative level dropdown");
        }
    }

    /**
     * Storing Area (Administrative) Details
     * @param array $data
     * @return JsonResponse
     */
    public function areaAdministrativeStore(array $data)
    {
        DB::beginTransaction();
        try {
            $order = $this->getLargestAreaOrder();
            $lftRghtArr = $this->getLftRghtAreaValue();
            
            $insertArr['name'] = $data['name'];
            $insertArr['code'] = $data['code'];
            $insertArr['parent_id'] = $data['parent_id'];
            $insertArr['area_administrative_level_id'] = $data['area_administrative_level'];
            $insertArr['is_main_country'] = $data['is_main_country'];
            $insertArr['lft'] = $lftRghtArr['lft'];
            $insertArr['rght'] = $lftRghtArr['rght'];
            $insertArr['order'] = $order;
            $insertArr['created_user_id'] = JWTAuth::user()->id;
            $insertArr['created'] = Carbon::now()->toDateTimeString();
            
            $store = AreaAdministrative::insert($insertArr);
            DB::commit();
            if ($store) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store area administrative details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to store area administrative details");
        }
    }

    /**
     * Get lft rght Area Administrative Value
     * @param array $array
     */
    public function getLftRghtAreaAdministrativeValue()
    {
        try {
            $areaRght = AreaAdministrative::max('rght');
            if ($areaRght) {
                $array['lft'] = $areaRght + 1;
                $array['rght'] = $array['lft'] + 1;
            } else {
                $array['lft'] = 1;
                $array['rght'] = $array['lft'] + 1;
            }
            return $array;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get lft and rght value',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get lft and rght value");
        }
    }

    /**
     * Getting largest area administrative order
     * @return int $largestOrder
     */
    public function getLargestAreaAdministrativeOrder()
    {
        try {
            $largestOrder = 0;
            $order = AreaAdministrative::max('order');
            if ($order) {
                $largestOrder = $order + 1;
            } else {
                $largestOrder = $largestOrder + 1;
            }
            return $largestOrder;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch largest grade order from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch largest grade order from DB");
        }
    }

    /**
     * Updating Area (Administrative) Details
     * @param array $data
     * @return JsonResponse
     */
    public function areaAdministrativeUpdate(array $data)
    {
        DB::beginTransaction();
        try {
            $updateArr['parent_id'] = $data['parent_id'];
            $updateArr['code'] = $data['code'];
            $updateArr['name'] = $data['name'];
            $updateArr['visible'] = $data['visible'];
            $updateArr['area_administrative_level_id'] = $data['area_administrative_level'];
            $updateArr['is_main_country'] = $data['is_main_country'];
            $updateArr['modified_user_id'] = JWTAuth::user()->id;
            $updateArr['modified'] = Carbon::now()->toDateTimeString();
            
            $update = AreaAdministrative::where('id', $data['id'])->update($updateArr);
            DB::commit();
            if ($update) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area administrative',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to update area administrative");
        }
    }

    /**
     * Checking if safe to delete Area Level (Education).
     * @param int $levelId
     * @return bool
     */
    public function checkIfSafeToDelete(int $levelId)
    {
        try {
            $areaLevel = AreaLevel::find($levelId);
            if ($areaLevel) {
                $areas = $areaLevel->areas()->exists();
                if ($areas) {
                    return false;
                }
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to check Area Level (Education) Dependencies',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to check Area Level (Education) Dependencies");
        }
    }

    /**
     * Deleting Area Level (Education).
     * @param int $levelId
     * @param int $parentId
     * @return int
     */
    public function areaLevelEducationDelete(int $levelId, int $parentId)
    {
        try {
            return AreaLevel::where('id', $levelId)->delete();
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area level (education).',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area level (education).");
        }
    }

    /**
     * Checking if safe to delete Area (Education).
     * @param int $areaId
     * @return bool
     */
    public function checkIfSafeToDeleteArea(int $areaId)
    {
        try {
            $area = Area::find($areaId);
            
            if ($area) {
                $examCentres = $area->examCentres()->exists();
                if ($examCentres) {
                    return false;
                }

                $securityGroups = $area->securityGroups()->exists();
                if ($securityGroups) {
                    return false;
                }
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to check Area (Education) Dependencies',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to check Area (Education) Dependencies");
        }
    }

    /**
     * Deleting Area (Education).
     * @param int $areaId
     * @param int $parentId
     * @return int
     */
    public function areaEducationDelete(int $areaId, int $parentId)
    {
        try {
            return Area::where('id', $areaId)->delete();
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area (education).',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area (education).");
        }
    }

    /**
     * Checking if safe to delete Area Level (Administrative).
     * @param int $levelId
     * @return bool
     */
    public function checkIfSafeToDeleteAreaLevel(int $levelId)
    {
        try {
            $areaLevel = AreaAdministrativeLevel::find($levelId);
            
            if ($areaLevel) {
                $areas = $areaLevel->areaAdministrative()->exists();
                if ($areas) {
                    return false;
                }
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to check Area Level (Administrative) Dependencies',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to check Area Level (Administrative) Dependencies");
        }
    }

    /**
     * Deleting Area Level (Administrative).
     * @param int $levelId
     * @param int $parentId
     * @return int
     */
    public function areaLevelAdministrativeDelete(int $levelId, int $parentId)
    {
        try {
            return AreaAdministrativeLevel::where('id', $levelId)->delete();
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area level (administrative).',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area level (administrative).");
        }
    }

    /**
     * Checking if safe to delete Area (Administrative).
     * @param int $areaId
     * @return bool
     */
    public function checkIfSafeToDeleteAreaAdministrative(int $areaId)
    {
        try {
            $area = AreaAdministrative::find($areaId);
            
            if ($area) {
                $securityUserAddress = $area->securityUserAddress()->exists();
                if ($securityUserAddress) {
                    return false;
                }

                $securityUserBirthplace = $area->securityUserBirthplace()->exists();
                if ($securityUserBirthplace) {
                    return false;
                }

                $areaLevel = $area->areaLevel()->exists();
                if ($areaLevel) {
                    return false;
                }
            }
            return true;
        } catch (\Exception $e) {
            Log::error(
                'Failed to check Area (Administrative) Dependencies',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            DB::rollback();
            return $this->sendErrorResponse("Failed to check Area (Administrative) Dependencies");
        }
    }

    /**
     * Deleting Area (Administrative).
     * @param int $areaId
     * @param int $parentId
     * @return int
     */
    public function areaAdministrativeDelete(int $areaId, int $parentId)
    {
        try {
            return AreaAdministrative::where('id', $areaId)->delete();
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area (administrative).',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area (administrative).");
        }
    }
}
