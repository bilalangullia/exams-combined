<?php

namespace App\Repositories\Report;

use App\Models\ReportProgress;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;

class ReportRepository
{

    /**
     * To update the status , error message if any and completion date of report generation
     * @param $lastReportId
     * @param $status
     * @param $error_message
     * @return mixed
     */
    public function updateReportStatus($lastReportId, $status, $error_message)
    {
        $error['msg'] = "";
        try {
            ReportProgress::where('id', $lastReportId)->update(
                [
                    'status' => $status,
                    'error_message' => $error_message,
                    'modified' => Carbon::now()->toDateTimeString(),
                ]
            );
            Log::info(
                'Update status and error message of report',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'reportId' => $lastReportId,
                        'status' => $status,
                        'error_message' => $error_message,
                    ],
                ]
            );
        } catch (\Exception $e) {
            $error['msg'] = "something went wrong";
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $error;
        }
    }

    /**
     * To increment the current records till reaching total records while generating the report
     *
     * @param $lastReportId
     * @return mixed
     */

    public function updateCount($lastReportId)
    {
        $error['msg'] = "";
        try {
            ReportProgress::where('id', $lastReportId)->increment('current_records');
            Log::info(
                'Update count of the records in the report',
                ['method' => __METHOD__, 'data' => ['reportId' => $lastReportId]]
            );
        } catch (\Exception $e) {
            $error['msg'] = "something went wrong";
            Log::error(
                'Unable to update count of records in the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $error;
        }
    }
    
    public function updateReportProcessRecordCount($reportId, $updateRecord){
       ReportProgress::where('id', $reportId)->update(array('current_records' => $updateRecord));
    }

    /**
     * To create and save new report from report data
     *
     * @param $saveReportData
     * @return mixed
     */
    public function createNewReport($saveReportData)
    {
        $error['msg'] = "";
        try {
            $reportObject = ReportProgress::create($saveReportData);
            Log::info(
                'New Report created in the database',
                ['method' => __METHOD__, 'data' => ['reportId' => $reportObject->id]]
            );
        } catch (\Exception $e) {
            $error['msg'] = "something went wrong";
            Log::error(
                'Failed to create a new report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $error;
        }

        if (!$reportObject) {
            $error['msg'] = "unable to create report";

            return $error;
        }

        return $reportObject->id;
    }

    /**
     * Getting Reports List
     * @param array $data
     * @return array
     */
    public function listReports(array $data)
    {
        try {
            $total = 0;
            $reports1 = ReportProgress::select(
                'name',
                'expiry_date',
                'file_path',
                'status',
                'created_user_id',
                'created AS started',
                'modified AS completed'
            )   ->with('createdBy:id,first_name,middle_name,third_name,last_name')
                ->where('module', 'Registration')
                ->orderBy('created', 'desc');

            if (isset($data['search'])) {
                $search = $data['search'];
                $reports1->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('status', 'LIKE', '%' . config('constants.reportUpdateStatus.' . $search) . '%')
                    ->orWhere('modified', 'LIKE', '%' . $search . '%');
            }

            if (isset($data['start']) && isset($data['end'])) {
                $reportsCount = $reports1;
                $total = $reportsCount->count();
                $reports1->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $reportsCount = $reports1;
                $total = $reportsCount->count();
            }

            $reports = $reports1->get();
            $data['reports'] = $reports;
            $data['total'] = $total;

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list of reports from Database',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
        }
    }
}
