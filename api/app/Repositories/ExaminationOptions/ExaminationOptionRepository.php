<?php

namespace App\Repositories\ExaminationOptions;

use App\Models\ExaminationOption;
use App\Models\ExaminationStudent;
use DB;
use Illuminate\Support\Facades\Log;

class ExaminationOptionRepository
{
    public function getOptionsByExamStudentId($examStudentId)
    {

        try {
            $options = DB::table('examination_students')
                ->join('examination_students_options', 'examination_students.id', '=', 'examination_students_options.examination_students_id')
                ->join('examination_options', 'examination_options.id', '=', 'examination_students_options.examination_options_id')
                ->select('examination_options.id', 'examination_options.code', 'examination_options.name')
                ->where('examination_students.id', '=', $examStudentId)
                ->get();

            Log::info('Fetched Options details by exam student id', ['method' => __METHOD__, 'data' => ['examStudentId' => $examStudentId, 'optionData' => $options]]);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch option details by exam student id',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return array();
        }
        if (!$options) {
            return array();
        }
        return $options->toArray();
    }
}