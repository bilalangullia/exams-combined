<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarkComponentAddRequest;
use App\Http\Requests\MarksComponentImportRequest;
use App\Imports\MarksComponentImport;
use App\Models\ExaminationCentre;
use App\Models\ExaminationComponent;
use App\Models\ExaminationGradingOption;
use App\Models\ExaminationMarkersApportionment;
use App\Models\ExaminationOption;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsComponentsMark;
use App\Models\MarkStatus;
use App\Models\MarkType;
use App\Models\Statu;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class MarkRepository.
 */
class MarkRepository extends Controller
{
    /**
     * getting option dropdown on the basis on Examination Id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function optionListDropdown(Request $request)
    {
        try {
            $record = ExaminationOption::select('id', 'code', 'name', 'examination_subject_id')
                ->whereHas(
                    'examinationSubject.examName',
                    function ($query) use ($request) {
                        $query->where('id', $request->examId);
                    }
                )->get();
            return $record;
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Option List Not Found');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function componentTab(Request $request)
    {
        try {
            $component = ExaminationComponent::select('id', 'code', 'name')->whereHas(
                'examinationOption',
                function ($query) use ($request) {
                    $query->where('id', $request->optionId);
                }
            )->get();
            return $component;
        } catch (\Exception $e) {
            return $this->sendErrorResponse('component tab  Not Found');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusDropdownVals()
    {
        try {
            $status = MarkStatus::select('id', 'name')->orderBy('order')->where(
                'visible',
                config('constants.visibleValue.visibleCode')
            )->get();
            return $status;
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Status Dropdown Values Not Found');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function markTypedropdownVals()
    {
        try {
            $type = MarkType::select('id', 'name')->orderBy('order')->where(
                'visible',
                config('constants.visibleValue.visibleCode')
            )->get();
            return $type;
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Mark Type Dropdown Values Not Found');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function componentList(Request $request)
    {
        try {
            $candidatelist = ExaminationStudentsComponentsMark::select(
                'id',
                'mark',
                'mark_status_id',
                'examination_students_id',
                'examination_components_mark_types_id',
                'examination_components_id',
                'created_user_id',
                'created'
            )->with(
                [
                    'markStatus' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examinationStudent' => function ($query) {
                        $query->select('id', 'candidate_id', 'student_id');
                    },
                    'examinationStudent.securityUser' => function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'last_name', 'third_name');
                    },

                    'examinationComponentsMarkTypes.markType' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examinationComponent' => function ($query) {
                        $query->select('id', 'code', 'name', 'examination_options_id');
                    },
                    'examinationComponent.examinationOption' => function ($query) {
                        $query->select('id', 'code');
                    },
                    'createdBy' => function ($query) {
                        $query->select('id', 'first_name', 'last_name');
                    },
                ]
            )->whereHas(
                'examinationComponent.examinationOption.examinationSubject.examName.academicPeriod',
                function ($query) use ($request) {
                    $query->where('id', $request->academic_period_id);
                }
            )->whereHas(
                'examinationComponent.examinationOption.examinationSubject.examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            )->whereHas(
                'examinationStudent.examinationCentre',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_centre_id);
                }
            )->whereHas(
                'examinationComponent.examinationOption',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_options_id);
                }
            )->distinct('examination_students_id')->orderby('created', 'desc')->get();
            $listdata = ExaminationComponent::select('id', 'code', 'name')
                ->where('examination_options_id', $request->examination_options_id)->get();
            return array("compList" => $listdata , "candidatelist" => $candidatelist);
        } catch (\Exception $e) {
            return $this->sendErrorResponse(' List Data Not Found');
        }
    }

    /**
     * @param Request $request
     * @param string $candidateId
     * @return string
     */
    public function addMark(MarkComponentAddRequest $request)
    {
        DB::beginTransaction();
        try {
            $candId = ExaminationStudent::where('candidate_id', $request->candidate_id)->whereHas(
                'examName.academicPeriod',
                function ($query) use ($request) {
                    $query->where('id', $request->academic_period_id);
                }
            )->whereHas(
                'examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            )->whereHas(
                'examinationCentre',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_centre_id);
                }
            )->whereHas(
                'examinationStudentsOption.examinationOption',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_options_id);
                }
            )->whereHas(
                'examinationStudentsOption.examinationOption.examinationComponent',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_components_id);
                }
            )->first();
            if (isset($candId)) {
                $canId = $candId->id ;
                $input = new ExaminationStudentsComponentsMark();
                $input['id'] = Str::uuid();
                $input['examination_students_id'] = $canId;
                $input['mark'] = $request->mark;
                $input['examination_components_id'] = $request->examination_components_id;
                $input['mark_status_id'] = $request->status_id;
                $input['examination_components_mark_types_id'] = $request->examination_component_mark_types_id;
                $input['examination_markers_id'] = $request->examination_markers_id;
                $input['created_user_id'] = JWTAuth::user()->id;
                $input['created'] = Carbon::now()->toDateTimeString();
                $responseData = $input->save();
                DB::commit();
                return $responseData;

            } else {
                return $this->sendErrorResponse(' candidate id does not belongs to corresponding input parameter');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendErrorResponse(' Marks not added');
        }
    }

    /**
     * @param string $candidateId
     * @return \Illuminate\Http\JsonResponse
     */
    public function detailsByCandidate(Request $request)
    {
        try {
            $canId = $request->candidate_id;
            $cid = ExaminationStudent::select('id')->where('candidate_id', $canId)->first();
            if ($cid) {
                $candidatemark = ExaminationStudent::select(
                    'id',
                    'candidate_id',
                    'student_id'
                )->where('candidate_id', $request->candidate_id)->with(
                    [
                        'securityUser' => function ($query) {
                            $query->select('id', 'first_name', 'middle_name', 'last_name', 'third_name', 'is_staff');
                        },
                    ]
                )->whereHas(
                    'examName.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('id', $request->academic_period_id);
                    }
                )->whereHas(
                    'examName',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_id);
                    }
                )->whereHas(
                    'examinationCentre',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_centre_id);
                    }
                )->whereHas(
                    'examinationStudentsOption.examinationOption',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_options_id);
                    }
                )->whereHas(
                    'examinationStudentsOption.examinationOption.examinationComponent',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_components_id);
                    }
                )->get()->map(
                    function ($item, $key) {
                        return [
                            "id" => $item['id'],
                            "name" => $item['securityUser']['full_name'],
                            "candidate_id" => $item['candidate_id'],
                        ];
                    }
                );
                $makrsdetails = $candidatemark->toArray();
                if (!empty($makrsdetails)) {
                    $grade = ExaminationComponent::select('examination_grading_type_id')
                        ->where('id', $request->examination_components_id)
                        ->first();
                    $gradeid = $grade->examination_grading_type_id;
                    $mark = examinationGradingOption::select('id', 'code', 'min', 'max')
                        ->where('examination_grading_type_id', $gradeid)->get();
                    $candidateChar = substr($canId, -4);
                    $markersdata = ExaminationMarkersApportionment::select(
                        'from_candidate',
                        'to_candidate',
                        'examination_markers_id'
                    )->where('examination_centres_id', $request->examination_centre_id)
                        ->whereRaw('"' . $candidateChar . '" between `from_candidate` and `to_candidate`')->with(
                            'examinationMarker:id,marker_id'
                        )->get()->map(
                            function ($item, $key) {
                                return [
                                    "id" => $item['examinationMarker']['id'],
                                    "name" => $item['examinationMarker']['marker_id'],
                                ];
                            }
                        );
                    return array("grade" => $mark, "candidatemarks" => $candidatemark, "markers" => $markersdata);
                } else {
                    return $this->sendErrorResponse(' Data Not Found');
                }
            } else {
                return $this->sendErrorResponse('Candidate details not found based on the parameter');
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse(' Data Not Found');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchMarksComponent(Request $request)
    {
        try {
            $data = $request->all();
            $total = 0;
            $listuser = ExaminationStudentsComponentsMark::select(
                'id',
                'mark',
                'mark_status_id',
                'examination_students_id',
                'examination_component_mark_types_id',
                'examination_components_id'
            )->where('examination_components_id', $request->examination_components_id)->with(
                [
                    'status' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examinationStudent' => function ($query) {
                        $query->select('id', 'candidate_id', 'student_id');
                    },
                    'examinationStudent.securityUser' => function ($query) {
                        $query->select('id', 'first_name', 'middle_name', 'last_name', 'third_name');
                    },

                    'examinationComponentsMarkType.markType' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'examinationComponent' => function ($query) {
                        $query->select('id', 'code', 'name', 'examination_options_id');
                    },
                    'examinationComponent.examinationOption' => function ($query) {
                        $query->select('id', 'code');
                    },
                ]
            )->whereHas(
                'examinationComponent.examinationOption.examinationSubject.examName.academicPeriod',
                function ($query) use ($request) {
                    $query->where('id', $request->academic_period_id);
                }
            )->whereHas(
                'examinationComponent.examinationOption.examinationSubject.examName',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_id);
                }
            )->whereHas(
                'examinationStudent.examinationCentre',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_centre_id);
                }
            )->whereHas(
                'examinationComponent.examinationOption',
                function ($query) use ($request) {
                    $query->where('id', $request->examination_options_id);
                }
            );
            if (isset($data['start']) && isset($data['end'])) {
                $userCount = $listuser;
                $total = $userCount->count();
                $listuser->skip($data['start'])
                    ->take($data['end'] - $data['start']);
            } else {
                $userCount = $listuser;
                $total = $userCount->count();
            }
            if (isset($request['keyword'])) {
                $userSearch = $listuser->where(
                    function ($query) use ($request) {
                        $query->where(
                            function ($query) use ($request) {
                                $query->where('mark', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'status',
                            function ($query) use ($request) {
                                $query->where('name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationStudent',
                            function ($query) use ($request) {
                                $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        )->orWhereHas(
                            'examinationStudent.securityUser',
                            function ($query) use ($request) {
                                $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%");
                                $query->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                            }
                        );
                    }
                );
            }
            $userSearch = $listuser->get();
            return array("totalRecord" => $total, "record" => $userSearch);
        } catch (\Exception $e) {
            return $this->sendErrorResponse(' List Data Not Found');
        }
    }

    /**
     * @param MarksComponentImportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importmMarksComponent(MarksComponentImportRequest $request)
    {
        try {
            $excelData = Excel::toArray(new MarksComponentImport(), $request->file('file'));
            $i = -1;
            $validation = [];
            $add_record = [];
            $stored_data = [];
            $insert = [];
            $inserta = [];
            $insertb = [];
            $componentID = $request['examination_components_id'];
            $centerID = $request['examination_centre_id'];
            $academicID = $request['academic_period_id'];
            $examinationId = $request['examination_id'];
            $optionId = $request['examination_options_id'];

            $examCentreDetails = ExaminationCentre::select(
                'id',
                'code',
                'examination_id'
            )
                ->with(
                    [
                        'exams' => function ($query) {
                            $query->select('id', 'code', 'academic_period_id');
                        },
                        'exams.academicPeriod' => function ($query) {
                            $query->select('id', 'name', 'start_year')
                                ->where('visible', config('constants.visibleValue.visibleCode'));
                        }
                    ]
                )
                ->where('id', $request['examination_centre_id'])
                ->first();
            foreach ($excelData[0] as $data) {
                $errors = [];
                $i++;

                if ($i < 2) {
                    continue;
                }

                if (!$data[0]) { //Candidate Id
                    $label = $excelData[0][1][0];
                    $errors[$label] = 'Candidate Id Required';
                }

                if (!$data[1]) { //Mark
                    $label = $excelData[0][1][1];
                    $errors[$label] = 'Mark Required  ';
                }
                /*if (!$data[2]) { //Marks Status
                    $label = $excelData[0][1][2];
                    $errors[$label] = 'Marks Status Required';
                }*/
                if (!$data[3]) { //Marks Type
                    $label = $excelData[0][1][3];
                    $errors[$label] = 'Marks Type Required';
                }
                if (count($errors) > 0) { //row fail
                    $validation[] = [
                        'row_number' => $i,
                        'data' => [
                            'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                            'Exam Code' => $examCentreDetails['exams']['code'],
                            'Centre Code' => $examCentreDetails['code'],
                            "Candidate ID" => $data[0],
                            "Marks" => $data[1],
                            "Marks Status" => $data[2],
                            "Marks Types" => $data[3],
                        ],
                        'errors' => $errors,
                    ];
                } else {
                    if ($data[0]) { //Update Record
                        $candidateId = ExaminationStudent::where('candidate_id', $data[0])->with('securityUser')->first(
                        );
                        if ($candidateId) {
                            $candId = $candidateId['id'];
                            $userexist = ExaminationStudentsComponentsMark::where('examination_students_id', $candId)
                                ->first();
                            if ($userexist) {
                                $user = [];
                                $user['examination_students_id'] = $candId;
                                $user['mark'] = $data[1];
                                $user['examination_components_id'] = $componentID;
                                $user['mark_status_id'] = $data[2];
                                $user['examination_components_mark_types_id'] = $data[3];
                                $user['modified_user_id'] = JWTAuth::user()->id;
                                $user['modified'] = Carbon::now()->toDateTimeString();
                                $secUserRec = ExaminationStudentsComponentsMark::where(
                                    ['examination_students_id' => $candId]
                                )->update($user);

                                $stored_data[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                        'Exam Code' => $examCentreDetails['exams']['code'],
                                        'Centre Code' => $examCentreDetails['code'],
                                        "Candidate Name" => $candidateId['securityUser']['full_name'],
                                        "Candidate ID" => $data[0],
                                        "Marks" => $data[1],
                                        "Mark Status" => $data[2],
                                        "Mark Types" => $data[3],
                                    ],
                                ];
                            } else {
                                $candidateId = ExaminationStudent::where('candidate_id', $data[0])
                                    ->with('securityUser')->first();
                                $candId = $candidateId['id'];
                                $user = new ExaminationStudentsComponentsMark();
                                $user['id'] = Str::uuid();
                                $user['examination_students_id'] = $candId;
                                $user['mark'] = $data[1];
                                $user['examination_components_id'] = $componentID;
                                $user['mark_status_id'] = $data[2];
                                $user['examination_components_mark_types_id'] = $data[3];
                                $user['created_user_id'] = JWTAuth::user()->id;
                                $user['created'] = Carbon::now()->toDateTimeString();
                                $record = $user->save();
                                $add_record[] = [
                                    'row_number' => $i,
                                    'data' => [
                                        'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                        'Exam Code' => $examCentreDetails['exams']['code'],
                                        'Centre Code' => $examCentreDetails['code'],
                                        "Candidate Name" => $candidateId['securityUser']['full_name'],
                                        "Candidate ID" => $data[0],
                                        "Marks" => $data[1],
                                        "Mark Status" => $data[2],
                                        "Mark Types" => $data[3],
                                    ],
                                ];
                            }
                        } else {
                            $label = $excelData[0][1][0];
                            $errors[$label] = 'Candidate ID does not exist';
                            $validation[] = [
                                'row_number' => $i,
                                'data' => [
                                    'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                    'Exam Code' => $examCentreDetails['exams']['code'],
                                    'Centre Code' => $examCentreDetails['code'],
                                    // "Candidate Name" => $candidateId['securityUser']['full_name'],
                                    "Candidate ID" => $data[0],
                                    "Marks" => $data[1],
                                    "Mark Status" => $data[2],
                                    "Mark Types" => $data[3],
                                ],
                                'errors' => $errors,
                            ];

                        }
                    } else {
                        $label = $excelData[0][1][0];
                        $errors[$label] = 'Candidate ID does not exist';
                        $validation[] = [
                            'row_number' => $i,
                            'data' => [
                                'Academic Period' => $examCentreDetails['exams']['academicPeriod']['start_year'],
                                'Exam Code' => $examCentreDetails['exams']['code'],
                                'Centre Code' => $examCentreDetails['code'],
                               // "Candidate Name" => $candidateId['securityUser']['full_name'],
                                "Candidate ID" => $data[0],
                                "Marks" => $data[1],
                                "Mark Status" => $data[2],
                                "Mark Types" => $data[3],
                            ],
                            'errors' => $errors,
                        ];
                    }
                }
            }
            $response = [
                'total_count' => count($excelData[0]) - 2,
                'records_added' => [
                    'count' => count($add_record),
                    'rows' => $add_record,
                ],
                'records_updated' => [
                    'count' => count($stored_data),
                    'rows' => $stored_data,
                ],
                'records_failed' => [
                    'count' => count($validation),
                    'rows' => $validation,
                ],
            ];
            return $response;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Marks Not Imported');
        }
    }
}
