<?php

namespace App\Repositories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ExaminationGradingType;
use App\Models\ExaminationOption;
use App\Models\ExaminationStudent;
use Illuminate\Support\Facades\Log;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationStudentsForecastGrade;
use Carbon\Carbon;
use DB;
use JWTAuth;

class ForecastGradeRepository extends Controller
{
    /**
     * Getting forecast grade dropdown
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function forecastGradeDropdown(string $optionId)
    {
        try {
            $forecastGradeDropdown = ExaminationOption::where('id', $optionId)->with('examinationGradingType', 'examinationGradingType.examinationGradingOption')
                            ->whereHas('examinationGradingType', function ($query) {
                                $query->where('visible', config('constants.visibleValue.visibleCode'));
                            })->get();

            return $forecastGradeDropdown;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Forecast Grade Dropdown Not Found");
        }
    }

    /**
     * Updating candidate grade
     * @param ForecastGradeRequest $request
     * @param string $candidateId
     * @return bool|\Exception|\Illuminate\Http\JsonResponse
     */
    public function updateCandidateForecastGrade(Request $request, string $candidateId)
    {
        try{
            $std_id = ExaminationStudent::where('candidate_id', $candidateId)->first()->id;
            $data = ExaminationStudentsForecastGrade::where('examination_students_id', $std_id)->where('examination_options_id', $request->options_id)->first();

            if(!empty($data))
            {
                $data->id = uniqid();
                $data->grade = $request->grade_id;
                $data->examination_students_id = $std_id;
                $data->examination_options_id = $request->options_id;
                $data->modified_user_id = JWTAuth::user()->id;
                $data->modified = Carbon::now()->toDateTimeString();
                $update = $data->save();
                return $update;
            } else {
                $newData = new ExaminationStudentsForecastGrade();
                $newData->id = uniqid();
                $newData->grade = $request->grade_id;
                $newData->examination_students_id = $std_id;
                $newData->examination_options_id = $request->options_id;
                $newData->created_user_id = config('constants.createdUser.user_id');
                $newData->created = Carbon::now()->toDateTimeString();
                $newData->modified_user_id = JWTAuth::user()->id;
                $newData->modified = Carbon::now()->toDateTimeString();
                $store = $newData->save();
                return $store;
            }
    } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Candidate Forecast Grade Not Updated");
        }
    }

    //getting forecast list
    public function forecastCandidateList(Request $request)
    {
        try {
            $optionId = $request->optId;
            $data = ExaminationStudentsOption::select('id', 'examination_students_id','examination_options_id')
                    ->where('examination_options_id', $optionId)->with([
                         'examinationStudent' =>
                            function ($query) use ($request) {
                                $query->select('id','candidate_id','student_id', 'examination_centre_id', 'examination_id');
                            },
                            'examinationStudent.examinationCentre' =>
                            function ($query) use ($request) {
                                $query->select('id')->where('id', $request->examination_centre_id);
                            },
                            'examinationStudent.examName' => 
                            function ($query) use ($request) {
                                $query->select('id', 'academic_period_id')->where('id', $request->examination_id);
                            }, 
                            'examinationStudent.examName.academicPeriod' =>
                            function ($query) use ($request) {
                                $query->select('id')->where('id', $request->academic_period_id);
                            },
                            'examinationStudent.securityUser' =>
                            function ($query)  {
                                $query->select('id','first_name','middle_name','third_name','last_name');
                            },
                             'examinationStudent.examinationStudentsForecastGrade' => 
                              function ($query) use($optionId) {
                                $query->where('examination_options_id', $optionId);
                            },
                            'examinationOption' =>
                            function ($query) use ($optionId) {
                                $query->select('id')->where('id', $optionId);
                            }]);
                        
            if (isset($request['keyword'])) {
                $candidateSearch = $data->where(function ($q) use ($request){
                    $q->whereHas(
                        'examinationStudent',
                        function ($query) use ($request) {
                            $query->where('candidate_id', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    )->orWhereHas(
                        'examinationStudent.securityUser',
                        function ($query) use ($request) {
                            $query->where('first_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('middle_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('third_name', 'LIKE', "%" . $request['keyword'] . "%")
                                  ->orwhere('last_name', 'LIKE', "%" . $request['keyword'] . "%");
                        }
                    );
                    }
                );
            }

            $candidateSearch = $data->get();

            return $candidateSearch;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate Forecast Grade List Not Found');
        }
    }

}

