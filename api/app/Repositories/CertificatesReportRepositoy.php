<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ReportProgress;
use App\Http\Controllers\Controller;
use App\Helper\SlugHelper;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use App\Http\Requests\ForecastGradeFormRequest;
use App\Models\ExaminationSubject;
use App\Models\ExaminationStudent;
use App\Models\ExaminationCentre;
use DB;
use Illuminate\Support\Facades\Log;
use Storage;
use Illuminate\Support\Str;
use PDF;
use JWTAuth;

class CertificatesReportRepositoy extends Controller
{

	protected $examinationCentreRepository;
    protected $examinationRepository;

    public function __construct(
        ExaminationCentreRepository $examinationCentreRepository,
        ExaminationRepository $examinationRepository
    ) {
        $this->examinationCentreRepository = $examinationCentreRepository;
        $this->examinationRepository = $examinationRepository;
    }

    public function getExamCentreDropDown(Request $request)
    {
        try{

            $centre = ExaminationCentre::select('id','code','name')->whereHas(
                    'exams',
                    function ($query) use ($request) {
                        $query->where('id', $request->examination_id);
                    }
                )->whereHas(
                    'exams.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('id', $request->academic_period_id);
                    }
                )->get();

                return $centre;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Exam Centre Drop down not Found');
        }
    }

	public function createNewReport($saveReportData)
    {
        try {
            $reportObject = ReportProgress::create($saveReportData);
            
            return $reportObject;
        } catch (\Exception $e) {
            Log::error(
                'Failed to create a new report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Certificates Report Not Generated');
        }
    }

	/**
     * To create and save new report from report data
     *
     * @param $saveReportData
     * @return mixed
     */
    public function saveReportData(array $repotData)
    {
        
        try {
                $examName = $this->examinationRepository->getExamName($repotData['examination_id']);
                $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
                    $repotData['examination_centre_id']
                );
                $candidate = ExaminationStudent::where('id',  $repotData['candidate_id'])->first();

                if($repotData['candidate_id'] == 0 && $repotData['examination_centre_id'] == 0) {
                	$name = 'Certificate File' . ':' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .'All Exam Centre'. ' ' .'All Candidates';
                	$reportName = 'Certificate_File' . ' : ' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' . 'all exam centre' . ' ' .'all candidates' . time();

                    $candidates = ExaminationStudent::where('examination_id', $repotData['examination_id'])->get(); 
                } elseif ($repotData['candidate_id'] == 0) {
                   $name = 'Certificate File' . ':' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .$examCenterNameCode['code']. ' ' .'All Candidates';

                    $reportName = 'Certificate_File' . ' : ' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' . $examCenterNameCode['code'] . ' '. 'all candidates' . time();

                    $candidates = ExaminationStudent::where('examination_id', $repotData['examination_id'])->
                                where('examination_centre_id', $repotData['examination_centre_id'])->get(); 
                } else {
                	$name = 'Certificate File' . ':' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .$examCenterNameCode['code'] . ' ' .$candidate['candidate_id'];

                	$reportName = 'Certificate_File' . ' : ' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .$examCenterNameCode['code'] . ' ' .$candidate['candidate_id'] . time();

                    $candidates = ExaminationStudent::where('id', $repotData['candidate_id'] )->where('examination_id', $repotData['examination_id'])->
                                where('examination_centre_id', $repotData['examination_centre_id'])->get();
                }
                
                $reportName = SlugHelper::slugify($reportName);
                $storage_path = 'public/reports';
                if (!Storage::exists($storage_path)) {
                    Storage::makeDirectory($storage_path, 0755);
                }

                $file_path = $reportName . '.xlsx';
                $url = asset('storage/reports/' . $file_path);
                $countCandidates = count($candidates);
                $saveReport = [
                    'id' => Str::uuid(),
                    'name' => $name,
                    'module' => 'Certificate',
                    'params' => json_encode($repotData),
                    'file_path' => $url,
                    'total_records' => $countCandidates ,
                    'status' => config('constants.reportUpdateStatus.notGeneratedOrNew'),
                    'created' => Carbon::now()->toDateTimeString(),
                    'modified' => Carbon::now()->toDateTimeString(),
                    'created_user_id' => JWTAuth::user()->id,
                    'expiry_date' => Carbon::now()->addHours(6)->toDateTimeString(),
                    'file_name' => $reportName.'.xlsx',
                    'exam_details' => $examName,
                    'candidate_details' => $candidate,
                    'exam_centre_detail' =>   $examCenterNameCode,
                    'current_date' => Carbon::now()
                ];
                
                return $saveReport;
            }  catch (\Exception $e) {
                echo $e;
                    Log::error(
                        'failed to fetch data from db',
                        ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                    );

                    return $this->sendErrorResponse('Certificates Report Not Generated'); 
                }
        }


    public function saveUpdateReport(array $repotData)
    {
        try{
            $saveReportData = $this->saveReportData($repotData);
            $lastReportId = $this->createNewReport($saveReportData);
            return array("res" => $lastReportId, "reportData" => $saveReportData);
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Certificates Report Not Generated'); 
        }
    }

    public function getTotalCandidates(array $repotData)
    {
        try{
            $candidate_data = ExaminationStudent::with('securityUser','securityUser.gender')
            ->whereHas(
                    'examinationCentre',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['examination_centre_id']);
                    }
                )->whereHas(
                    'examName',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['examination_id']);
                    }
                )->whereHas(
                    'examName.academicPeriod',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['academic_period']);
                    }
                )->whereHas(
                    'examName.examinationSubject',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['subject_id']);
                    }
                )->get();

            return $candidate_data;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse("Candidate's Record Not Found"); 
        }
    }
}