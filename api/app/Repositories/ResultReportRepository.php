<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\ReportProgress;
use App\Http\Controllers\Controller;
use App\Helper\SlugHelper;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use App\Http\Requests\ForecastGradeFormRequest;
use App\Models\ExaminationSubject;
use App\Models\ExaminationStudent;
use DB;
use Illuminate\Support\Facades\Log;
use Storage;
use Illuminate\Support\Str;
use PDF;
use JWTAuth;

class ResultReportRepository extends Controller
{

	protected $examinationCentreRepository;
    protected $examinationRepository;

    public function __construct(
        ExaminationCentreRepository $examinationCentreRepository,
        ExaminationRepository $examinationRepository
    ) {
        $this->examinationCentreRepository = $examinationCentreRepository;
        $this->examinationRepository = $examinationRepository;
    }

	public function createNewReport($saveReportData)
    {
        try {
            $reportObject = ReportProgress::create($saveReportData);
            
            return $reportObject;
        } catch (\Exception $e) {
            Log::error(
                'Failed to create a new report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Result Report Not Generated');
        }
    }

	/**
     * To create and save new report from report data
     *
     * @param $saveReportData
     * @return mixed
     */
    public function saveReportData(array $repotData)
    {
        
        try {
                $examName = $this->examinationRepository->getExamName($repotData['examination_id']);
                $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
                    $repotData['examination_centre_id']
                );
                $candidate = ExaminationStudent::where('id',  $repotData['candidate_id'])->first();
                
                if($repotData['candidate_id'] == 0){
                	$name = 'Statement Of Results' . ':' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .$examCenterNameCode['code'] . ' ' .'All Candidates';
                	$reportName = 'Statement_Of_Results' . ' : ' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .$examCenterNameCode['code'] . ' ' . 'all';
                } else {
                	$name = 'Statement Of Results' . ':' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .$examCenterNameCode['code'] . ' ' .$candidate['candidate_id'];

                	$reportName = 'Statement_Of_Results' . ' : ' . ' ' . $examName['code'] . ' ' . $examName['academicPeriod']['name'] . ' ' .$examCenterNameCode['code'] . ' ' .$candidate['candidate_id'];
                }
                
                $reportName = SlugHelper::slugify($reportName);
                $storage_path = 'public/reports';
                if (!Storage::exists($storage_path)) {
                    Storage::makeDirectory($storage_path, 0755);
                }

                $file_path = $reportName . '.pdf';
                $url = asset('storage/reports/' . $file_path);
                //$countCandidates = count($repotData['candidate']);
                $saveReport = [
                    'id' => Str::uuid(),
                    'name' => $name,
                    'module' => 'Results',
                    'params' => json_encode($repotData),
                    'file_path' => $url,
                    'total_records' => 1,
                    'status' => config('constants.reportUpdateStatus.notGeneratedOrNew'),
                    'created' => Carbon::now()->toDateTimeString(),
                    'modified' => Carbon::now()->toDateTimeString(),
                    'created_user_id' => JWTAuth::user()->id,
                    'expiry_date' => Carbon::now()->addHours(6)->toDateTimeString(),
                    'file_name' => $reportName. '.pdf',
                    'exam_details' => $examName,
                    'exam_centre_detail' =>   $examCenterNameCode,
                    'current_date' => Carbon::now()
                ];
                
                return $saveReport;
            }  catch (\Exception $e) {
                echo $e;
                    Log::error(
                        'failed to fetch data from db',
                        ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                    );

                    return $this->sendErrorResponse('Result Report Not Generated'); 
                }
        }


    public function saveUpdateReport(array $repotData)
    {
        try{
            $saveReportData = $this->saveReportData($repotData);
            $blade = 'results';
            $lastReportId = $this->createNewReport($saveReportData);
            
            return array("res" => $lastReportId, "reportData" => $saveReportData, "blade" => $blade);
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Result Report Not Generated'); 
        }
    }

    public function getTotalCandidates(array $repotData)
    {
        try{
            $candidate_data = ExaminationStudent::with('securityUser','securityUser.gender')
            ->whereHas(
                    'examinationCentre',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['examination_centre_id']);
                    }
                )->whereHas(
                    'examName',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['examination_id']);
                    }
                )->whereHas(
                    'examName.academicPeriod',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['academic_period']);
                    }
                )->whereHas(
                    'examName.examinationSubject',
                    function ($query) use ($repotData) {
                        $query->where('id', $repotData['subject_id']);
                    }
                )->get();

            return $candidate_data;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse("Candidate's Record Not Found"); 
        }
    }
}