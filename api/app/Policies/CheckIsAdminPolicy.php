<?php

namespace App\Policies;

use App\Models\SecurityUser;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Traits\isAdminTraits;

class CheckIsAdminPolicy
{
    use HandlesAuthorization, isAdminTraits;

    /**
     * Execute before checking policies. Return true if user is HP admin
     *
     * @param  \App\BusinessAccountUser  $user
     *
     * @return boolean
     */
    public function before(SecurityUser $user)
    {
        if($this->isSuperAdmin())
            return true;
    }

    /**
     * Determine whether the user can view any models security users.
     *
     * @param  \App\Models\SecurityUser  $user
     * @return mixed
     */
    public function viewAny(SecurityUser $user)
    {
        //
    }

    /**
     * Determine whether the user can view the models security user.
     *
     * @param  \App\Models\SecurityUser  $user
     * @param  \App\ModelsSecurityUser  $modelsSecurityUser
     * @return mixed
     */
    public function view(SecurityUser $user, ModelsSecurityUser $modelsSecurityUser)
    {
        //
    }

    /**
     * Determine whether the user can create models security users.
     *
     * @param  \App\Models\SecurityUser  $user
     * @return mixed
     */
    public function create(SecurityUser $user)
    {
        //
    }

    /**
     * Determine whether the user can update the models security user.
     *
     * @param  \App\Models\SecurityUser  $user
     * @param  \App\ModelsSecurityUser  $modelsSecurityUser
     * @return mixed
     */
    public function update(SecurityUser $user, ModelsSecurityUser $modelsSecurityUser)
    {
        //
    }

    /**
     * Determine whether the user can delete the models security user.
     *
     * @param  \App\Models\SecurityUser  $user
     * @param  \App\ModelsSecurityUser  $modelsSecurityUser
     * @return mixed
     */
    public function delete(SecurityUser $user, ModelsSecurityUser $modelsSecurityUser)
    {
         //$currentUser->created_user_id == JWTAuth::user()->id
    }

    /**
     * Determine whether the user can restore the models security user.
     *
     * @param  \App\Models\SecurityUser  $user
     * @param  \App\ModelsSecurityUser  $modelsSecurityUser
     * @return mixed
     */
    public function restore(SecurityUser $user, ModelsSecurityUser $modelsSecurityUser)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the models security user.
     *
     * @param  \App\Models\SecurityUser  $user
     * @param  \App\ModelsSecurityUser  $modelsSecurityUser
     * @return mixed
     */
    public function forceDelete(SecurityUser $user, ModelsSecurityUser $modelsSecurityUser)
    {
        //
    }
}
