<?php

namespace App\Services\Fee;

use App\Repositories\Fee\FeeRepository;
use Illuminate\Http\Request;

class FeeService
{
    /**
     * fee Repository instance
     * @var FeeRepository
     */
    protected $feeRepository;

    /**
     * FeeService constructor.
     * @param FeeRepository $feeRepository
     */
    public function __construct(
        FeeRepository $feeRepository
    ) {
        $this->feeRepository = $feeRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function feeList(Request $request)
    {
        $data = $this->feeRepository->feeList($request);
        return $data;
    }

}