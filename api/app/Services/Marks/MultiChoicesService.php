<?php

namespace App\Services\Marks;

use App\Http\Controllers\Controller;
use App\Models\ExaminationStudentsComponentsMark;
use App\Models\ExaminationStudentsMultiplechoiceResponse;
use App\Models\Statu;
use App\Models\ExaminationGradingOption;
use App\Repositories\CandidateRepository;
use App\Repositories\MultipleChoiceImportRepository;
use App\Repositories\SubjectRepository;
use App\Models\ExaminationStudent;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\ExaminationComponent;
use App\Models\ExaminationStudentsOptionsGrade;
use Illuminate\Support\Str;
use JWTAuth;

class MultiChoicesService extends Controller

{
    /**
     * CandidateRepository instance
     * @var CandidateRepository
     */
    protected $candidateRepository;

    /**
     * SubjectRepository instance
     * @var SubjectRepository
     */
    protected $subjectRepository;

    /**
     * MultipleChoiceImportRepository instance
     * @var MultipleChoiceImportRepository
     */
    protected $multipleChoiceImportRepository;

    /**
     * MultiChoicesService constructor.
     * @param CandidateRepository $candidateRepository
     * @param SubjectRepository $subjectRepository
     * @param MultipleChoiceImportRepository $multipleChoiceImportRepository
     */
    public function __construct(
        CandidateRepository $candidateRepository,
        SubjectRepository $subjectRepository,
        MultipleChoiceImportRepository $multipleChoiceImportRepository
    )
    {
        $this->candidateRepository = $candidateRepository;
        $this->subjectRepository = $subjectRepository;
        $this->multipleChoiceImportRepository = $multipleChoiceImportRepository;
    }

    /**
     * Multiple-choice Candidate Listing examination_components_multiple-choice
     * @param Request $request
     * @return JsonResponse
     */
    public function getCandidateList(Request $request)
    {
        try {
            $record = $this->candidateRepository->multiChoicesList($request)->map(
                function ($item, $key) {
                    return [
                        "candidate_id" => $item['candidate_id'],
                        "candidate_name" => $item['securityUser']['full_name'],
                        "status" => "New",
                        "created_on" => $item['created'],
                    ];
                }
            );

            return $record;
        } catch (\Exception $e) {
            Log::error($e);

            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }
    
    //Getting candidate final grades
    protected function setFinalGradeResponse(string $candidId)
    {
        try{
            $optionlist = [];
            $count = 0;
            $data = $this->candidateRepository->getCandidateFinalGrade($candidId);

        foreach ($data as $key => $item) {
            $option_list =[];
            $counter = 0;
                foreach ($item['examinationOption']['examinationComponent'] as $keys => $record) {
                            $option_list[$counter]['weight'] = ($record['weight']) * 100;
                            $counter++;
                        }
                $option_weight = array_sum(array_column($option_list, 'weight'));
                $optionlist[$count]['option_id'] = $item['examinationOption']['id'];
                $optionlist[$count]['option_code'] = $item['examinationOption']['code'];
                $optionlist[$count]['option_name'] = $item['examinationOption']['name'];
                $optionlist[$count]['option_raw_marks'] = '';
                $optionlist[$count]['option_weighting'] = $option_weight;
                $optionlist[$count]['option_final_marks'] = $item['examinationOption']['examinationStudentsOptionsGrade']['mark'];
                $optionlist[$count]['option_final_grade'] = $item['examinationOption']['examinationStudentsOptionsGrade']['examinationGradingOption']['name'];
                $optionlist[$count]['option_points'] = $item['examinationOption']['examinationStudentsOptionsGrade']['examinationGradingOption']['points'];
           foreach ($item['examinationOption']['examinationComponent'] as $k => $value) {
            
                    $obtainedMark = $value['examinationStudentsComponentsMark']['mark'];
                    $totalMark = floatval($value['examinationGradingType']['max']);
                    $weightVal =  ($value['weight'])*100 ;
                    $finalMarks = ($obtainedMark / $totalMark) * $weightVal;
                    $getGrade = ExaminationGradingOption::select('name')->whereRaw('"'.$finalMarks.'" between `min` and `max`')->first();
                    $optionlist[$count]['component'][$k]['component_id'] = $value['id'];
                    $optionlist[$count]['component'][$k]['component_code'] = $value['code'];
                    $optionlist[$count]['component'][$k]['component_name'] = $value['name'];
                    $optionlist[$count]['component'][$k]['component_weighting'] = $weightVal;
                    $optionlist[$count]['component'][$k]['component_raw_marks'] = isset($obtainedMark) ? $obtainedMark . ' / ' . $totalMark : '';
                    $optionlist[$count]['component'][$k]['component_final_marks'] = isset($obtainedMark) ? $finalMarks : '';
                    $optionlist[$count]['component'][$k]['component_final_grade'] = isset($getGrade) ? $getGrade : '';
                    $optionlist[$count]['component'][$k]['component_points'] = '';
                } 
            $count ++;
        }
            
            return $optionlist;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate Final Grades Not Found');
        }
    }

    /**
     * Multiple-choice Candidate Details
     * @param $candidId
     */
    public function multiChoiceView(Request $request, string $candidId)
    {
        try {
            $data = $this->candidateRepository->getMultiChoiceCandidateDetails($request, $candidId)->map(
                function ($item, $key) use ($candidId, $request) {
                    return [
                        "academic_period_id" => [
                            "key" => $item['examName']['academicPeriod']['id'],
                            "value" => $item['examName']['academicPeriod']['name'],
                        ],
                        "examination_id" => [
                            "key" => $item['examName']['id'],
                            "value" => $item['examName']->full_name,
                        ],
                        "examination_centre_id" => [
                            "key" => $item['examinationCentre']['id'],
                            "value" => $item['examinationCentre']->full_name,
                        ],
                        "candidate_id" => [
                            "key" => $item['id'],
                            "value" => $item['candidate_id'],
                        ],
                        "candidate_name" => [
                            "key" => $item['securityUser']['id'],
                            "value" => $item['securityUser']['full_name'],
                        ],
                        "forecast_grade" => $this->candidateRepository->getCandidateForecastGrade($request, $candidId),
                        "response" => $this->candidateRepository->getCandidateResponse($request, $candidId),
                        "aggregate_score" => $this->setCandidateAggregateMarks($candidId),
                        "final_grade" => $this->setFinalGradeResponse($candidId),
                        "modified_by" => $item['modified_user_id'],
                        "modified_on" => Carbon::parse($item['modified'])->format('F d,Y - h:i:s'),
                        "created_by" => $item['createdByUser']['full_name'],
                        "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );

            return $data;
        } catch (\Exception $e) {
            return $e;
            Log::error($e);

            return $this->sendErrorResponse('Candidate View Not Found');
        }
    }

    /**
     * Getting multiple-choice subject dropdown
     * @param Request $request
     * @return JsonResponse
     */
    public function getSubjectList(Request $request)
    {
        try {
            $subjectList = $this->subjectRepository->getSubjectDropDown($request)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['examinationOption']['id'],
                        "code" => $item['examinationOption']['code'],
                        "name" => $item['examinationOption']['name'],
                    ];
                }
            );

            return $subjectList;
        } catch (\Exception $e) {
            Log::error($e);

            return $this->sendErrorResponse('Subject List Not Found');
        }
    }

    /**
     * Getting multiple-choice component dropdown
     * @param Request $request
     * @return JsonResponse
     */
    public function getComponentList(Request $request)
    {
        try {
            $componetList = $this->subjectRepository->getComponentList($request)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                    ];
                }
            );

            return $componetList;
        } catch (\Exception $e) {
            Log::error($e);

            return $this->sendErrorResponse('Subject List Not Found');
        }
    }

    /**
     * Add/update candidate record
     * @param $data
     * @param $request
     * @return array|JsonResponse
     */
    public function updateCandidateResponse(Request $request, $cand_id, $compId)
    {
        try {
            $std_id = ExaminationStudent::where('candidate_id', $cand_id)->first()->id;
            if ($request['response'] && count($request['response'])) {
                $delete = ExaminationStudentsMultiplechoiceResponse::where('examination_students_id', $std_id)->where(
                'examination_components_id',
                $compId
               )->delete();
                $answerList = [];
                foreach ($request->response as $res) {
                    $record_data = [];
                    $record_data['response'] = $res['response'];
                    $record_data['question'] = $res['question'];
                    $record_data['examination_students_id'] = $std_id;
                    $record_data['examination_components_id'] = $res['examination_components_id'];
                    $record_data['created_user_id'] = JWTAuth::user()->id;
                    $record_data['created'] = Carbon::now()->toDateTimeString();
                    $answerList[] = $record_data;
                }
                $data = ExaminationStudentsMultiplechoiceResponse::insert($answerList);

                return $data;
            } else {
               $dlt = ExaminationStudentsMultiplechoiceResponse::where('examination_students_id', $std_id)->where(
                'examination_components_id',
                $compId
               )->delete(); 
            }
        } catch (\Exception $e) {
            echo $e;
            Log::error($e);

            return $this->sendErrorResponse('Record Not updated');
        }
    }

    /**
     * Storing candidate's marks
     * @param $data
     * @return bool|JsonResponse
     */
    public function getCandidateResponse(Request $request, string $candId)
    {
        try {
           $data = $this->candidateRepository->getCandidateQuestionResponse($request, $candId);
            $setData = [];
            foreach ($data['questionList'] as $item) {
                $key = $item['examination_components_id'] . "_" . $item['question'];
                $setData[$key] = $item['answer'];
            }
            $candData = [];
            $result = 0;
            foreach ($data['candAns'] as $key => $val) {
                $key = $val['examination_components_id'] . "_" . $val['question'];
                if ($setData[$key] == $val['response']) {
                    $result++;
                }
                $candData[$key] = $item['response'];
            }
            $std_id  = ExaminationStudent::where('candidate_id', $candId)->first()->id;
            $data = new ExaminationStudentsComponentsMark(); 
            $data['id'] = Str::uuid();
            $data['mark'] = $result;
            $data['examination_students_id'] = $std_id;
            $data['examination_components_id'] = $request->compID;
            $data['examination_components_mark_types_id'] = config('constants.markType.id');
            $data['created_user_id'] =  JWTAuth::user()->id;
            $data['created'] = Carbon::now()->toDateTimeString();
            $store = $data->save();

            return $store;
        } catch (\Exception $e) {
            echo $e;
            Log::error($e);

            return $this->sendErrorResponse('Candidate Record Not Found');
        }
    }

    public function setCandidateAggregateMarks(string $candidId){
        try{
            $list = [];
            $count = 0;
            $i = 0;
            $test = $this->candidateRepository->calculateCandidateAggregateScore($candidId);
            foreach ($test as $key => $value) {
                   foreach ($value['examinationStudentsOption'] as $key => $val) {
                        foreach ($val['examinationOption']['examinationGradingType']['examinationGradingOption'] as $key => $record) {
                            $list[$count]['points'] = $record['points'];
                            $i++;
                            $count++;

                        }
                    } 
                }

            $aggregate_points = array_sum(array_column($list, 'points'));
            
            return $aggregate_points;
        } catch (Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Candidate Record Not Found');
        }
    }

    public function finalGradeGenerateButton(Request $request)
    {
        try {
            $student_marks = [];
            $generate = $this->candidateRepository->multiChoicesCandidateList($request);
            if ($generate) {
              $candIds = $generate->pluck('id');

              $data = ExaminationStudentsComponentsMark::whereIn('examination_students_id', $candIds)->with('examinationComponent', 'examinationComponent.examinationGradingType')->get();
              foreach ($data as $key => $value) {
                $marks  = (($value['mark'] / $value['examinationComponent']['examinationGradingType']['max']) * $value['examinationComponent']['weight']) * 100;
                $grade = ExaminationGradingOption::select('id')->whereRaw('"'.$marks.'" between `min` and `max`')->first();
                    $student_marks[] = [
                        'id' => uniqid(),
                        'mark' => $marks,
                        'grade' => $grade->id,
                        'examination_students_id' => $value['examination_students_id'],
                        'examination_options_id' => $value['examinationComponent']['examination_options_id'],
                        'created_user_id' => config('constants.createdByUser.id'),
                        'created' => Carbon::now()->toDateTimeString(),
                        'modified_user_id' => config('constants.modifiedUser.user_id'),
                        'modified' => Carbon::now()->toDateTimeString(),
                    ];
               }
                 $result = ExaminationStudentsOptionsGrade::insert($student_marks);
            }
            
            return $result;
        } catch (Exception $e) {
             Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Candidate's option grade not generated");
        }
    }

    public function multipleChoiceGenerateButton(Request $request)
    {
        try {
            $student_comp_marks = [];
            $compId = $request['compId'];
            $dataa = $this->candidateRepository->getAllCandidateQuestionResponse($request);
             $setData = [];
                foreach ($dataa['questionList'] as $item) {
                    $key = $item['examination_components_id'] . "_" . $item['question'];
                    $setData[$key] = $item['answer'];
                }
                $candData = [];
                $result = 0;
                foreach ($dataa['candAns'] as $key => $val) {
                    $key = $val['examination_components_id'] . "_" . $val['question'];
                    if ($setData[$key] == $val['response']) {
                        $result++;
                    }
                    $candData[$key] = $item['response'];
                /*}*/
              foreach ($dataa as $key => $value) {
                    $student_comp_marks[] = [
                        'id' => uniqid(),
                        'mark' => $result,
                        'examination_students_id' => $val['examination_students_id'],
                        'examination_components_id' => $compId,
                        'examination_components_mark_types_id' => config('constants.markType.id'),
                        'created_user_id' => JWTAuth::user()->id,
                        'created' => Carbon::now()->toDateTimeString(),
                        'modified_user_id' => JWTAuth::user()->id,
                        'modified' => Carbon::now()->toDateTimeString()
                    ];
               }
           }
                 $result = ExaminationStudentsComponentsMark::insert($student_comp_marks);
            
            return $result;
        } catch (Exception $e) {
             Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Candidate's option grade not generated");
        }
    }
}
