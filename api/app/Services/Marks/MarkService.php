<?php

namespace App\Services\Marks;

use App\Http\Controllers\Controller;
use App\Repositories\MarkRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MarkService extends Controller
{
    /**
     * Mark repository Instance
     * @var MarkRepository
     */
    protected $markRepository;

    /**
     * MarkService constructor.
     * @param MarkRepository $markRepository
     */
    public function __construct(
        MarkRepository $markRepository
    ) {
        $this->markRepository = $markRepository;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function optionDropdown(Request $request)
    {
        try {
            $details = $this->markRepository->optionListDropdown($request)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                    ];
                }
            );
            return $details;
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function markComponentTab(Request $request)
    {
        try {
            $tab = $this->markRepository->componentTab($request)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "component-tab" => $item['code'] . '-' . $item['name'],
                    ];
                }
            );
            return $tab;
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * @param Request $request
     * @return array|\Exception
     */
    public function markComponentListing(Request $request)
    {
        try {
            $tabArray = [];
            $list = [];
            $list = $this->markRepository->componentList($request);
            if (count($list['compList']) > 0 || count($list['candidatelist']) > 0) {
                $comp_a = $list['compList'];
                $com_b = $list['candidatelist'];
                $c = $comp_a->toArray();
                $c1 = $com_b->toArray();

                $d = array_merge($c, $c1);
                foreach ($c as $item) {
                    $compId = $item['id'];
                    $code = strtolower($item['code']);
                    $name = strtolower($item['name']);
                    $name_comp = preg_replace('/\s+/', '', $name);
                    $tab = trim($code . '-' . $name_comp);
                    $tabArraya[$tab][] = [
                        'components_id' => $item['id']
                    ];
                }
                $tabvala = $tabArraya;
                if (!empty($c1)) {
                    foreach ($c1 as $value) {
                        $compId = $value['examination_component']['id'];
                        $code = strtolower($value['examination_component']['code']);
                        $name = strtolower($value['examination_component']['name']);
                        $name_comp = preg_replace('/\s+/', '', $name);
                        $tab = trim($code . '-' . $name_comp);
                        $tabArrayb[$tab][] = [
                            'components_id' => $value['examination_component']['id'],
                            'mark' => $value['mark'],
                            'candidate_id' => $value['examination_student']['candidate_id'],
                            'mark_type' => $value['examination_components_mark_types']['mark_type']['name'],
                            'status' => $value['mark_status']['name'],
                            'name' => $value['examination_student']['security_user']['first_name'] . ' ' . $value['examination_student']['security_user']['last_name'],
                            'created_on' => $value['created'],
                            'Created_by' => $value['created_by']['first_name'] . ' ' . $value['created_by']['last_name'],
                        ];
                    }

                    $tabvalb = $tabArrayb;
                    $data = array_merge($tabvala, $tabvalb);
                    return $data;
                } else {
                    return $tabvala;
                }
            } else {
                return false;
            }
        } catch (\Exception $e) {
            echo $e;
            Log::error($e);
            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function marksSearch(Request $request)
    {
        try {
            $response = $this->markRepository->searchMarksComponent($request);
            $list = $response['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        'mark' => $item['mark'],
                        "candidate_id" => $item['examinationStudent']['candidate_id'],
                        "name" => $item['examinationStudent']['securityUser']['full_name'],
                        "mark_type" => $item['examinationComponentsMarkType']['markType']['name'],
                        "status" => $item['status']['name'],
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $response['totalRecord'];
                $data['userRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error($e);

            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }
}
