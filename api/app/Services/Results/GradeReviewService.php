<?php

namespace App\Services\Results;

use App\Http\Controllers\Controller;
use App\Models\ExaminationGradeReviewCriteria;
use App\Models\ExaminationGradeReviewStudent;
use App\Repositories\GradeReviewRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GradeReviewService extends Controller
{
    protected $gradeReviewRepository;

    public function __construct(GradeReviewRepository $gradeReviewRepository)
    {
        $this->gradeReviewRepository = $gradeReviewRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGradeReviewList(Request $request)
    {
        try {
            $grade = $this->gradeReviewRepository->gradeReviewList($request);
            $exist = [];
            $new = [];
            $count = 0;
            foreach ($grade as $gradetype) {
                if (array_key_exists($gradetype['examinationStudent']['candidate_id'], $exist)) {
                    $value = $exist[$gradetype['examinationStudent']['candidate_id']];
                    $new[$value]['review_criteria'] = $new[$value]['review_criteria'] . ',' . $gradetype['examinationGradeReviewCriteria']['gradeReviewCriteriaType']['name'];
                } else {
                    $new[$count]['candidate_id'] = $gradetype['examinationStudent']['candidate_id'];
                    $new[$count]['candidate_name'] = $gradetype['examinationStudent']['securityUser']['full_name'];
                    $new[$count]['review_criteria'] = $gradetype['examinationGradeReviewCriteria']['gradeReviewCriteriaType']['name'];
                    $new[$count]['created_on'] = Carbon::now()->toDateTimeString();
                    $new[$count]['modified_on'] = Carbon::now()->toDateTimeString();
                    $exist[$gradetype['examinationStudent']['candidate_id']] = $count;
                    $count++;
                }
            }
            return $new;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Grade Review Not Found");
        }
    }
}
