<?php

namespace App\Services\Results;

use App\Http\Controllers\Controller;
use App\Repositories\ForecastGradeRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ForecastGradeService extends Controller
{
    protected $forecastGradeRepository;

    public function __construct(ForecastGradeRepository $forecastGradeRepository)
    {
        $this->forecastGradeRepository = $forecastGradeRepository;
    }

    /**
     * Getting forecast grade dropdown
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function forecastGradeDropdown(string $optionId)
    {
        try {
            $list = [];
            $count = 0;
            $forecastGrade = $this->forecastGradeRepository->forecastGradeDropdown($optionId)->map(
                    function ($item, $key) use (&$list, &$count) {
                        foreach ($item['examinationGradingType']['examinationGradingOption'] as $key => $value) {
                            $list[$count]['id'] = $value['id'];
                            $list[$count]['option_id'] = $value['name'];
                            $count++;
                        }
                    }
                );

            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Forecast Grade Dropdown Not Found");
        }
    }

    public function getForecastGradeCandidateList(Request $request)
    {
        try {
            $list = [] ;
            $record = $this->forecastGradeRepository->forecastCandidateList($request)->map(
                function ($item, $key) {
                    return [
                        "candidate_id" => $item['examinationStudent']['candidate_id'],
                        "candidate_name" => $item['examinationStudent']['securityUser']['full_name'],
                        "forecast_grade" => isset($item['examinationStudent']['examinationStudentsForecastGrade']['examinationGradingOption']['name']) ? $item['examinationStudent']['examinationStudentsForecastGrade']['examinationGradingOption']['name'] : '',
                        "modified_on" => isset($item['examinationStudent']['examinationStudentsForecastGrade']['modified']) ? $item['examinationStudent']['examinationStudentsForecastGrade']['modified'] : '',
                        "modified_by" => isset($item['examinationStudent']['examinationStudentsForecastGrade']['modified_user_id']) ? $item['examinationStudent']['examinationStudentsForecastGrade']['modifiedUser']['full_name'] : ''
                    ];
                }
            );

            return $record;
        } catch (\Exception $e) {
            echo $e;
            Log::error($e);

            return $this->sendErrorResponse('Candidate Forecast Grade List Not Found');
        }
    }

}