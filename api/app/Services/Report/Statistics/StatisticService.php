<?php

namespace App\Services\Report\Statistics;

use App\Http\Controllers\Controller;
use App\Repositories\FormReportRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;
use App\Repositories\StatisticRepository;

class StatisticService extends Controller
{
    /**
     * StatisticRepository instance.
     * @var $statisticRepository
     */
    protected $statisticRepository;

    /**
     * StatisticService Constructor.
     * @param StatisticRepository $statisticRepository
     */
    public function __construct(
        StatisticRepository $statisticRepository
    )
    {
        $this->statisticRepository = $statisticRepository;
    }

    /**
     * Generating Candidate Per Subject Report.
     * @param array $data
     * @return int
     */
    public function candidatePerSubjectReport(array $data)
    {
        try {
            $generate = $this->statisticRepository->candidatePerSubjectReport($data);
            return $generate;
        } catch (\Exception $e) {
            Log::error(
                'Failed to generate candidate per subject statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate candidate per subject statistics report");
        }
    }

    /**
     * Getting Statistics Report List
     * @param Request $request
     * @return JsonResponse
     */
    public function statisticsReportList(array $data)
    {
        try {
            $data = $this->statisticRepository->statisticsReportList($data);
            $data['reports'] = $data['reports']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "file_path" => $item['file_path'],
                        "status" => config('constants.reportProgressStatus.' . $item['status']),
                        "started" => isset($item['created']) ? Carbon::parse($item['created'])->format('F d,Y - h:i:s') : "",
                        "completed" => isset($item['modified']) ? Carbon::parse($item['modified'])->format('F d,Y - h:i:s') : "",
                        "expiry_date" => isset($item['expiry_date']) ? Carbon::parse($item['expiry_date'])->format('F d,Y - h:i:s') : "",
                        "created_user_id" => $item['securityUser']['first_name'] . " " . $item['securityUser']['middle_name'] . " " . $item['securityUser']['third_name'] . " " . $item['securityUser']['last_name'],
                    ];
                }
            );
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get list of candidate per subject statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get list of candidate per subject statistics report");
        }
    }

    /**
     * Generating Statistics Report.
     * @param array $data
     * @return int
     */
    public function candidatePerCentreReport(array $data)
    {
        try {
            $generate = $this->statisticRepository->candidatePerCentreReport($data);
            return $generate;
        } catch (\Exception $e) {
            Log::error(
                'Failed to generate candidate per centre statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate candidate per centre statistics report");
        }
    }
}
