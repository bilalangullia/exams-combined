<?php

namespace App\Services\Report\Forms;

use App\Http\Controllers\Controller;
use App\Repositories\FormReportRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;
use App\Repositories\MsExaminerReportRepository;

class MsExaminerReportService extends Controller
{
    
	/**
     * The report service instance.
     * @var MsExaminerReportRepository
     */
    protected $msExaminerReportRepository;

    /**
     * Create a new controller instance.
     * @param MsExaminerReportRepository $msExaminerReportRepository
     * @return void
     */
    public function __construct(
        MsExaminerReportRepository $msExaminerReportRepository
    )
    {
        $this->msExaminerReportRepository = $msExaminerReportRepository;
    }

    /**
     * Generating Report
     * @param array $data
     * @return JsonResponse
     */
    public function generate(array $data)
    {
    	try {
            $generate = $this->msExaminerReportRepository->generate($data);
            return $generate;
    	} catch (\Exception $e) {
    		log::error(
                'Failed to genrate report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to genrate report");
    	}
    }
}
