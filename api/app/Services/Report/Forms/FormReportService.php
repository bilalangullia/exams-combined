<?php

namespace App\Services\Report\Forms;

use App\Helper\SlugHelper;
use App\Http\Controllers\Controller;
use App\Models\AcademicPeriod;
use App\Models\Examination;
use App\Models\ExaminationCentre;
use App\Models\ExaminationSubject;
use App\Repositories\ExaminationOptions\ExaminationOptionRepository;
use App\Repositories\FormReportRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;

class FormReportService extends Controller
{
    protected $formReportRepository;
    protected $examinationOptionRepository;

    public function __construct(
        FormReportRepository $formReportRepository,
        ExaminationOptionRepository $examinationOptionRepository
    ) {
        $this->formReportRepository = $formReportRepository;
        $this->examinationOptionRepository = $examinationOptionRepository;
    }

    public function saveReportData($reportData, $requestData)
    {

        try {
            $academicPeriod = AcademicPeriod::select('code', 'name', 'start_year')->where('id', $requestData['academic_period_id'])->first();
            $examName = Examination::select('code', 'attendance_type_id', 'name')->where('id', $requestData['examination_id'])->first();
            $examCenterNameCode = ExaminationCentre::select('code', 'name')
                                  ->where('id', $requestData['examination_centre_id'])->first();
            $subject = ExaminationSubject::select('name')->where('id', $requestData['examination_subject_id'])->first();
            $reportName = 'Multiple Choice:' . '_' . $examName->code . '_' . $academicPeriod->name . '_' . $examCenterNameCode->code;
            if (empty($requestData['candidate'])) {
                $reportName .= '_' . 'All_candidates' . '_' . time() . '.pdf';
            } else {
                $reportName .= '_' . $reportData['candidates'][0]['id'] . '_' . time() . '.pdf';
            }
            $reportName = SlugHelper::slugify($reportName);
            $storage_path = 'public/reports';
            if (!Storage::exists($storage_path)) {
                Storage::makeDirectory($storage_path, 0755);
            }
            $url = asset('storage/reports/' . $reportName);
            $countCandidates = count($reportData['candidates']);
            $saveReport = [
                'id' => Str::uuid(),
                'name' => $reportName,
                'module' => 'Multiple-Choice',
                'params' => json_encode($requestData),
                'file_path' => $url,
                'total_records' => $countCandidates,
                'status' => config('constants.reportUpdateStatus.notGeneratedOrNew'),
                'created' => Carbon::now()->toDateTimeString(),
                'modified' => Carbon::now()->toDateTimeString(),
                'created_user_id' => config('constants.createdUser.user_id'),
                'expiry_date' => Carbon::now()->addHours(6)->toDateTimeString()
            ];
            return $saveReport;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Multiplechoice Save Report Data Not Generated');
        }
    }

    /**
     * @param $reportData
     * @param $requestData
     * @return mixed
     */
    public function saveUpdateReport($reportData, $requestData)
    {
        try {
            $error['msg'] = "";
            $storage_path = 'public/reports';
            $saveReportData = $this->saveReportData($reportData, $requestData);
            $blade = 'multiple-choice';
            $lastReportId = $this->formReportRepository->createNewReport($saveReportData);
            if (!$lastReportId) {
                $this->formReportRepository->updateReportStatus(
                    $lastReportId,
                    $status = config(
                        'constants.reportUpdateStatus.notCompleted'
                    ),
                    $error_message = "report can not be saved"
                );
                $error['msg'] = "report can not be generated";
                return $error;
            }
            $this->formReportRepository->updateReportStatus(
                $lastReportId,
                $status = config('constants.reportUpdateStatus.processing'),
                $error_message = null
            );
            $timeOut = false;
            for ($i = 0; $i < $saveReportData['total_records']; $i++) {
                if (Carbon::now()->toDateTimeString() >= $saveReportData['expiry_date']) {
                    $timeOut = true;
                    break;
                }

                $this->formReportRepository->updateCount($lastReportId);
            }
            if ($timeOut) {
                $this->formReportRepository->updateReportStatus(
                    $lastReportId,
                    $status = config(
                        'constants.reportUpdateStatus.notCompleted'
                    ),
                    $error_message = "report can not be generated due to exceed time"
                );
                $error['msg'] = "report can not be generated due to exceed time";
                return $error;
            } else {
                try {
                    $pdf = PDF::loadView($blade, $reportData);
                    $this->formReportRepository->updateReportStatus(
                        $lastReportId,
                        $status = config(
                            'constants.reportUpdateStatus.completed'
                        ),
                        $error_message = null
                    );
                    Storage::put($storage_path . '/' . $saveReportData['name'], $pdf->output());
                    Log::info(
                        'Pdf Report generated and saved in database and directory',
                        ['method' => __METHOD__, 'data' => []]
                    );
                } catch (\Exception $e) {
                    $this->formReportRepository->updateReportStatus(
                        $lastReportId,
                        $status = config(
                            'constants.reportUpdateStatus.notCompleted'
                        ),
                        $error_message = "Failed to generate the pdf report"
                    );
                    $error['msg'] = "Failed to generate the pdf report";
                    Log::error(
                        'Failed to generate the pdf report',
                        ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                    );
                    return $error;
                }
            }
        } catch (\Exception $e) {
                Log::error(
                    'Failed to fetch list',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );
                return $this->sendErrorResponse('Multiplechoice Update Report Issue');
        }
    }

    /**
     * @param $examName
     * @param $examCenterNameCode
     * @param $candidates
     * @return mixed
     */
    public function formatReportData($academicPeriod, $examName, $examCenterNameCode, $subject, $candidates)
    {
        try {
            foreach ($candidates as $value) {
                $formatArray ['candidates'][] = [
                    'academicPeriodcode' => $academicPeriod->code,
                    'academicPeriodname' => $academicPeriod->name,
                    'academicYr' => $academicPeriod->start_year,
                    'examName' => $examName->name,
                    'examNameatt' => $examName->attendance_type_id,
                    'examCenterCode' => $examCenterNameCode->code,
                    'examCenterName' => $examCenterNameCode->name,
                    'id' => $value->candidate_id,
                    'name' => $value->first_name . ' ' . $value->third_name . ' ' . $value->last_name,
                    'subjectname' => $subject->name,
                    'subjectcode' => $subject->code,
                    'component' => 'MULTIPLE CHOICE',
                    'date' => date("d-M-y"),

                ];
            }
            return $formatArray;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('formate report data issue');
        }
    }
    /**
     * Getting Reports List
     * @param array $data
     * @return array
     */
    public function listingReports(Request $request)
    {
        try {
            $reports = $this->formReportRepository->listReportsData($request);

            foreach ($reports['reports'] as $report) {
                $report->started = Carbon::parse($report->started)->format("M d,Y H:i:s");
                $report->completed = Carbon::parse($report->completed)->format("M d,Y H:i:s");
                $report->status = array_search($report->status, config('constants.reportUpdateStatus'));
                if ($report->created_user_id == 2) {
                    $report->created_user_id = 'Admin - System Administrator';
                }
            }
            return $reports;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

}
