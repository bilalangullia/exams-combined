<?php

namespace App\Services\Report\Forms;

use App\Helper\SlugHelper;
use App\Http\Controllers\Controller;
use App\Repositories\ForecastGradeFormRepository;
use App\Http\Requests\ForecastGradeFormRequest;
use Carbon\Carbon;
use App\Models\ReportProgress;
use App\Models\Examination;
use App\Models\ExaminationStudent;
use App\Models\ExaminationCentre;
use App\Models\ExaminationSubject;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;

class ForecastGradeService extends Controller
{

    protected $forecastGradeFormRepository;

    public function __construct(
        ForecastGradeFormRepository $forecastGradeFormRepository
    ) {
        $this->forecastGradeFormRepository = $forecastGradeFormRepository;
    }

    public function generateReport(array $repoData)
    {
        try{
            $data = $this->forecastGradeFormRepository->generate($repoData);

            return $data;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Forecast Grade Report Not Generated'); 
        }
    }
}

