<?php

namespace App\Services\Report\Marks;

use App\Helper\SlugHelper;
use App\Http\Controllers\Controller;
use App\Repositories\MarksReportRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;

class MarksReportService extends Controller
{
    /**
     * @var MarksReportRepository
     */
    protected $marksReportRepository;

    /**
     * MarksReportService constructor.
     * @param MarksReportRepository $marksReportRepository
     */
    public function __construct(
        MarksReportRepository $marksReportRepository
    ) {
        $this->marksReportRepository = $marksReportRepository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function generate(array $data)
    {
        try {
            $generate = $this->marksReportRepository->generate($data);
            return $generate;
        } catch (\Exception $e) {
            echo $e;
            log::error(
                'Failed to genrate report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to genrate report");
        }
    }
}
