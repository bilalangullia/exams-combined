<?php

namespace App\Services\Report;

use App\Helper\SlugHelper;
use App\Repositories\Areas\AreaRepository;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\ExaminationOptions\ExaminationOptionRepository;
use App\Repositories\Examinations\ExaminationRepository;
use App\Repositories\ExaminationSubjects\ExaminationSubjectRepository;
use App\Repositories\Report\ReportRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;
use JWTAuth;

class ReportService
{

    protected $reportRepository;
    protected $examinationSubjectRepository;
    protected $examinationOptionRepository;
    protected $areaRepository;
    protected $examinationCentreRepository;
    protected $examinationRepository;

    public function __construct(
        ReportRepository $reportRepository,
        ExaminationSubjectRepository $examinationSubjectRepository,
        ExaminationOptionRepository $examinationOptionRepository,
        AreaRepository $areaRepository,
        ExaminationCentreRepository $examinationCentreRepository,
        ExaminationRepository $examinationRepository
    ) {
        $this->reportRepository = $reportRepository;
        $this->examinationSubjectRepository = $examinationSubjectRepository;
        $this->examinationOptionRepository = $examinationOptionRepository;
        $this->areaRepository = $areaRepository;
        $this->examinationCentreRepository = $examinationCentreRepository;
        $this->examinationRepository = $examinationRepository;
    }

    public function saveReportData($reportData, $requestData)
    {
        $areaNameCode = $this->areaRepository->getAreaNameCodeByAreaId($requestData['area_id']);
        $examName = $this->examinationRepository->getExamName($requestData['examination_id']);

        $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
            $requestData['examination_centre_id']
        );
        $reportName = 'Candidate_Registration' . '_' . $areaNameCode['name'] . '_' . $areaNameCode['code'] . '_' . $examCenterNameCode['name'] . '_' . $examCenterNameCode['code'];
        if (empty($requestData['candidate'])) {
            $reportName .= '_' . 'All_candidates' . '_' . time() . '.pdf';
        } else {
            $reportName .= '_' . $reportData['candidates'][0]['id'] . '_' . time() . '.pdf';
        }
        $reportName = SlugHelper::slugify($reportName);
        $storage_path = 'public/reports';
        if (!Storage::exists($storage_path)) {
            Storage::makeDirectory($storage_path, 0755);
        }
        $url = asset('storage/reports/' . $reportName);
        $countCandidates = count($reportData['candidates']);
        $saveReport = [
            'id' => Str::uuid(),
            'name' => $reportName,
            'module' => 'Registration',
            'params' => json_encode($requestData),
            'file_path' => $url,
            'total_records' => $countCandidates,
            'status' => config('constants.reportUpdateStatus.notGeneratedOrNew'),
            'created' => Carbon::now()->toDateTimeString(),
            'modified' => Carbon::now()->toDateTimeString(),
            'created_user_id' => JWTAuth::user()->id,
            'expiry_date' => Carbon::now()->addHours(6)->toDateTimeString()
        ];
        return $saveReport;
    }

    public function saveUpdateReport($reportData, $requestData)
    {
        $error['msg'] = "";        
        $saveReportData = $this->saveReportData($reportData, $requestData);
        
        DB::beginTransaction();
        $lastReportId = $this->reportRepository->createNewReport($saveReportData);
        if (!$lastReportId) {
            $this->reportRepository->updateReportStatus($lastReportId, $status = config('constants.reportUpdateStatus.notCompleted'), $error_message = "report can not be saved");
            $error['msg'] = "report can not be generated";
            return $error;
        }
        
        $this->reportRepository->updateReportStatus($lastReportId, $status = config('constants.reportUpdateStatus.processing'), $error_message = null);
        DB::commit();
        
        Log::info('Shell script running');
        
        //$cmd = sprintf('nohup ' . $consoleDir . ' jsonstat run_job %d > /dev/null 2>/dev/null &', $id);
        $cmd = 'php ../artisan report:registration_candidate_verification '. $lastReportId;
        //echo $cmd;
        $output = shell_exec($cmd);
        
        return true;
    }
    
    public function saveUpdateReportCommand($reportData, $templateName, $reportId, $recordCount)
    {
        $error['msg'] = "";
        $storage_path = 'public/reports';
        
        $blade = 'pdf-report-registration';
        DB::beginTransaction();
        try {
            
            $htmldata = view($blade, ['candidatesdata' => $reportData]);
            Storage::disk('local')->append($storage_path .'/' .$templateName . '.html', $htmldata, null);
            
            $reportProgress = DB::table('report_progress')
                        ->where('id', '=', $reportId)
                        ->first();
            
            $currentRecords = $reportProgress->current_records;            
            
            $updateRecord = $recordCount + $currentRecords;
            $this->reportRepository->updateReportProcessRecordCount($reportId, $updateRecord);
            Log::info('Pdf Report generated and saved in database and directory', ['method' => __METHOD__, 'data' => []]);
        } catch (\Exception $e) {
            //$this->reportRepository->updateReportStatus($lastReportId, $status = config('constants.reportUpdateStatus.notCompleted'), $error_message = "Failed to generate the pdf report");
            $error['msg'] = "Failed to generate the pdf report";
            Log::error(
                'Failed to generate the pdf report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $error;

        }
        DB::commit();
    }


    /**
     * Getting Reports List
     * @param array $data
     * @return array
     */
    public function listReports(array $data)
    {
        try {
            $reports = $this->reportRepository->listReports($data);
            foreach ($reports['reports'] as $report) {
                $report->started = Carbon::parse($report->started)->format("M d,Y H:i:s");
                $report->completed = Carbon::parse($report->completed)->format("M d,Y H:i:s");
                $report->status = array_search($report->status, config('constants.reportUpdateStatus'));
                $report->created_user_id = $report['createdBy']['full_name'];
            }
            return $reports;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    public function formatReportData($areaNameCode, $examName, $examCenterNameCode, $candidates)
    {

        foreach ($candidates as $value) {
            $options = $this->examinationOptionRepository->getOptionsByExamStudentId($value->id);
            $optionArray = [];
            foreach ($options as $option) {
                $optionArray[] = ['code' => $option->code, 'name' => $option->name];
            }
            $formatArray['candidates'][] = [
                'areaName' => $areaNameCode['name'],
                'areaCode' => $areaNameCode['code'],
                'examName' => $examName['name'],
                'examCode' => $examName['code'],
                'examCenterName' => $examCenterNameCode['name'],
                'examCenterCode' => $examCenterNameCode['code'],
                'id' => $value->candidate_id,
                'name' => $value->first_name . ' ' . $value->middle_name . ' ' . $value->third_name . ' ' . $value->last_name,
                'date_of_birth' => Carbon::parse($value->date_of_birth)->format('Y-M-d'),
                'gender' => $value->name,
                'identification' => $value->identity_number,
                'options' => $optionArray
            ];

        }

        return $formatArray;
    }
    
    public function getPercentageReportService($id){
        $reportProgress = DB::table('report_progress')
                        ->where('id', '=', $id)
                        ->where('expiry_date', '>=', Carbon::now()->toDateTimeString())
                        ->first();
        
        return $reportProgress;
    }
}
