<?php

namespace App\Services\Report\Certificates;

use App\Helper\SlugHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use App\Repositories\CertificatesReportRepositoy;
use App\Http\Requests\ForecastGradeFormRequest;
use Carbon\Carbon;
use App\Models\ReportProgress;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;
use App\Exports\CertificateExport;
use Maatwebsite\Excel\Facades\Excel;

class CertificatesReportService extends Controller
{
    protected $certificatesReportRepositoy;
    protected $examinationCentreRepository;
    protected $examinationRepository;

    public function __construct(
        CertificatesReportRepositoy $certificatesReportRepositoy,
        ExaminationCentreRepository $examinationCentreRepository,
        ExaminationRepository $examinationRepository
    ) {
        $this->certificatesReportRepositoy = $certificatesReportRepositoy;
        $this->examinationCentreRepository = $examinationCentreRepository;
        $this->examinationRepository = $examinationRepository;
    }
    
    public function getExamCentreDropDown(Request $request)
    {
        try{
            $centre = $this->certificatesReportRepositoy->getExamCentreDropDown($request);
            if ($centre->count() > 0) {
                $centre->prepend(['id' => '0', 'code' => 'All', 'name' => 'Exam Centre']);
                return $centre;
            } 

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Exam Centre Drop down not Found');
        }
    }

    public function saveUpdateReport(array $repoData)
    {
        try{
            $data = $this->certificatesReportRepositoy->saveUpdateReport($repoData);
            if ($data['res']) {
                $id = $data['res']->id;
                $processing = config('constants.reportUpdateStatus.processing');
                ReportProgress::where('id', $id)->update(['status' => $processing]);

                $timeOut = false;
                for ($i = 0; $i < $data['reportData']['total_records']; $i++) {
                    if (Carbon::now()->toDateTimeString() >= $data['reportData']['expiry_date']) {
                        $timeOut = true;
                        break;
                    }
                    ReportProgress::where('id', $id)->increment('current_records');
                }

                if ($timeOut) {
                    $updateArr['status'] = config('constants.reportUpdateStatus.notCompleted');
                    $updateArr['error_message'] = "report can not be generated due to exceed time";
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);
                    $error['msg'] = "report can not be generated due to exceed time";
                    return 2;
                } else {
                    $storage_path = 'public/reports';
                    $file = $storage_path . '/' . $data['reportData']['file_name'];
                    $excel =  Excel::store(new CertificateExport($data), $file);
                    $updateArr['status'] = config('constants.reportUpdateStatus.completed');
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);

                    Log::info('Pdf Report generated and saved in database and directory', ['method' => __METHOD__, 'data' => []]); 
                    return 1;
                }
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Certificate Report Not Generated'); 
        }
    }
}