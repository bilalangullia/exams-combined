<?php

namespace App\Services\Report\Results;

use App\Helper\SlugHelper;
use App\Http\Controllers\Controller;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use App\Repositories\ResultReportRepository;
use App\Http\Requests\ForecastGradeFormRequest;
use Carbon\Carbon;
use App\Models\ReportProgress;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PDF;
use Storage;

class ResultService extends Controller
{
    protected $resultReportRepository;
    protected $examinationCentreRepository;
    protected $examinationRepository;

    public function __construct(
        ResultReportRepository $resultReportRepository,
        ExaminationCentreRepository $examinationCentreRepository,
        ExaminationRepository $examinationRepository
    ) {
        $this->resultReportRepository = $resultReportRepository;
        $this->examinationCentreRepository = $examinationCentreRepository;
        $this->examinationRepository = $examinationRepository;
    }

    public function saveUpdateReport(array $repoData)
    {
        try{
            $data = $this->resultReportRepository->saveUpdateReport($repoData);
            if ($data['res']) {
                $id = $data['res']->id;
                $processing = config('constants.reportUpdateStatus.processing');
                ReportProgress::where('id', $id)->update(['status' => $processing]);

                $timeOut = false;
                for ($i = 0; $i < $data['reportData']['total_records']; $i++) {
                    if (Carbon::now()->toDateTimeString() >= $data['reportData']['expiry_date']) {
                        $timeOut = true;
                        break;
                    }
                    ReportProgress::where('id', $id)->increment('current_records');
                }

                if ($timeOut) {
                    $updateArr['status'] = config('constants.reportUpdateStatus.notCompleted');
                    $updateArr['error_message'] = "report can not be generated due to exceed time";
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);
                    $error['msg'] = "report can not be generated due to exceed time";
                    return 2;
                } else {
                    $pdf = PDF::loadView($data['blade'], $data['reportData']);
                    $storage_path = 'public/reports';
                    Storage::put($storage_path . '/' . $data['reportData']['file_name'], $pdf->output());
                    
                    $updateArr['status'] = config('constants.reportUpdateStatus.completed');
                    $updateArr['modified'] = Carbon::now()->toDateTimeString();

                    ReportProgress::where('id', $id)->update($updateArr);

                    Log::info('Pdf Report generated and saved in database and directory', ['method' => __METHOD__, 'data' => []]); 
                    return 1;
                }
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Result Report Not Generated'); 
        }
    }
}