<?php

namespace App\Services\Register;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Repositories\MiscRepository;

class MiscService extends Controller

{
    /**
     * MiscRepository instance
     * @var MiscRepository
     */
    protected $miscRepository;

    /**
     * MiscService constructor.
     * @param MiscRepository $miscRepository
     */
    public function __construct(
        MiscRepository $miscRepository
    ) {
        $this->miscRepository = $miscRepository;
    }

    /**
     * Getting Exam Centre List
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExamCentreList(string $examId)
    {
        try {
            $examCentre = $this->miscRepository->getExamCentreList($examId)->map(
                function ($item, $key) {
                    return [
                        'key' => $item['id'],
                        'value' => $item['code'] . " - " . $item['name'],
                    ];
                }
            );
            return $examCentre;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Dropdown Not Found");
        }
    }

    /**
     * Getting Education Level Dropdown
     * @return JsonResponse
     */
    public function getEducationLevelDropdown()
    {
        try {
            $levels = $this->miscRepository->getEducationLevelDropdown()->map(
                function ($item, $key) {
                    return [
                        "key" => $item['id'],
                        "value" => $item['educationSystem']['name'] . " - " . $item['name'],
                    ];
                }
            );
            return $levels;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }

    /**
     * Getting Education Program Dropdown Via Level Id
     * @return JsonResponse
     */
    public function getEducationProgramDropdownViaLevelId(int $levelId)
    {
        try {
            $programs = $this->miscRepository->getEducationProgramDropdownViaLevelId($levelId);
            return $programs;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }

    /**
     * Getting Education Stages Dropdown
     * @return JsonResponse
     */
    public function getEducationStageDropdown()
    {
        try {
            $stages = $this->miscRepository->getEducationStageDropdown()->map(
                function ($item, $key) {
                    return [
                        "key" => $item['id'],
                        "value" => $item['name'],
                    ];
                }
            );
            return $stages;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education stage dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education stage dropdown from DB");
        }
    }

    /**
     * Getting Education Grades Dropdown
     * @param int $programId
     * @return JsonResponse
     */
    public function getEducationGradeDropdownViaProgramId(int $programId)
    {
        try {
            $grades = $this->miscRepository->getEducationGradeDropdownViaProgramId($programId);
            return $grades;
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education grade details in DB");
        }
    }

    /**
     * Getting Education Subjects Dropdown
     * @return JsonResponse
     */
    public function getEducationSubjectDropdown()
    {
        try {
            $subjects = $this->miscRepository->getEducationSubjectDropdown()->map(
                function ($item, $key) {
                    return [
                        "key" => $item['id'],
                        "value" => $item['name'],
                    ];
                }
            );
            return $subjects;
        } catch (\Exception $e) {
            Log::error(
                'Failed to store education grade subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store education grade subject details");
        }
    }

    /**
     * Getting Area administrative Dropdown
     * @return JsonResponse
     */
    public function getAreaAdministrativeDropdown()
    {
        try {
            $areas = $this->miscRepository->getAreaAdministrativeDropdown();
            return $areas; 
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative dropdown");
        }
    }

    /**
     * Getting Country Dropdown
     * @return JsonResponse
     */
    public function getCountryDropdown()
    {
        try {
            $country = $this->miscRepository->getCountryDropdown();
            return $country;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get country dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get country dropdown");
        }
    }
}
