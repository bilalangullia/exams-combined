<?php

namespace App\Services\Register\Fee;

use App\Http\Controllers\Controller;
use App\Models\ExaminationFee;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsFee;
use App\Repositories\RegistrationFeeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RegistrationFeeService extends Controller
{
    /**
     * @var RegistrationfeeRepository
     */
    protected $registrationFeeRepository;

    /**
     * RegistrationFeeService constructor.
     * @param RegistrationFeeRepository $registrationFeeRepository
     */
    public function __construct(
        RegistrationFeeRepository $registrationFeeRepository
    ) {
        $this->registrationFeeRepository = $registrationFeeRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getFeeList(Request $request)
    {
        try {
            $response = $this->registrationFeeRepository->feeList($request);
            $list = $response['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "candidate_id" => $item['candidate_id'],
                        "name" => $item['securityUser']['full_name'],
                        "openemis_no" => $item['securityUser']['openemis_no'],
                        "center_name" => $item['examinationCentre']['name'],
                        "center_code" => $item['examinationCentre']['code'],
                        "status" => $item['payment_status'],
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $response['totalRecord'];
                $data['userRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
        }
    }

    /**
     * @param int $feeId
     */
    public function getFeeDetails(string $candId, string $examId)
    {
        try {
            $details = $this->registrationFeeRepository->feeDetails($candId, $examId)->map(
                function ($item, $key) use ($candId, $examId) {
                    return [
                        "cid" => $item['id'],
                        "candidate_id" => $item['candidate_id'],
                        "fee_type" => $this->getFeeType($examId, $candId),
                        "payment" => $this->getPayment($candId),
                        "modified_by" => $item['modifiedUser']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['createdByUser']['full_name'],
                        "created_on" => $item['created'],
                    ];
                }
            );
            return $details;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
        }
    }

    /**
     * @param string $examId
     * @param string $candId
     * @return mixed
     */
    protected function getFeeType(string $examId, string $candId)
    {
        try {
            $candid = ExaminationStudent::where('candidate_id', $candId)->first()->id;
            $fee = ExaminationFee::select('id', 'name', 'amount')
                ->with(
                    [
                        'examinationStudentsFee' => function ($query) use ($candid) {
                            $query->select('id', 'examination_fees_id', 'quantity')->where(
                                'examination_students_id',
                                $candid
                            );
                        }
                    ]
                )->where('examinations_id', $examId)->get()->map(
                    function ($item, $key) {
                        return [
                            "id" => $item['id'],
                            "name" => $item['name'],
                            "amount" => $item['amount'],
                            "qty" => isset($item['examinationStudentsFee'][0]['quantity']) ? $item['examinationStudentsFee'][0]['quantity'] : 0,
                            "totalamount" => (isset($item['examinationStudentsFee'][0]['quantity']) ? $item['examinationStudentsFee'][0]['quantity'] : 0) * $item['amount'],
                        ];
                    }
                );
            return $fee;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
        }
    }

    /**
     * @param string $candId
     * @return mixed
     */
    protected function getPayment(string $candId)
    {
        try {
            $candid = ExaminationStudent::where('candidate_id', $candId)->first()->id;
            $payment = ExaminationStudentsFee::select('id')->where('examination_students_id', $candid)
                ->with(
                    'examinationStudentsFeesPayment:id,examination_student_fees_id,date,amount,receipt,created_user_id',
                    'examinationStudentsFeesPayment.securityUser'
                )
                ->get()->map(
                    function ($item, $key) {
                        return [
                            "id" => $item['examinationStudentsFeesPayment'][0]['id'],
                            "fee_id" => $item['examinationStudentsFeesPayment'][0]['examination_student_fees_id'],
                            "date" => $item['examinationStudentsFeesPayment'][0]['date'],
                            "amount" => $item['examinationStudentsFeesPayment'][0]['amount'],
                            "receipt" => $item['examinationStudentsFeesPayment'][0]['receipt'],
                            "comment" => $item['examinationStudentsFeesPayment'][0]['comment'],
                            "created_by" => $item['examinationStudentsFeesPayment'][0]['securityUser']['full_name'],

                        ];
                    }
                );
            return $payment;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
        }
    }
}
