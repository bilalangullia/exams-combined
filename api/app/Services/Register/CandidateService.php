<?php

namespace App\Services\Register;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Repositories\RegisterCandidateRepository;
use Carbon\Carbon;

class CandidateService extends Controller
{
    protected $registerCandidateRepository;

    public function __construct(
        RegisterCandidateRepository $registerCandidateRepository
    ) {
        $this->registerCandidateRepository = $registerCandidateRepository;
    }

	public function deleteCandidate(string $candId, $check)
    {
        try{
            $canDelete = true;
            if ($check) {
                $canDelete = $this->registerCandidateRepository->checkIfSafeToDelete($candId);
            }

            if (is_array($canDelete)) {
                return $canDelete;
            } else {
               return $this->registerCandidateRepository->deleteCandidate($candId); 
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }

    }

    /**
     * Getting Candidates List Using Advance Search
     * @param array $data
     * @return JsonResponse
     */
    public function advanceSearch(array $data)
    {
        try {
            $list = $this->registerCandidateRepository->advanceSearch($data);
            $list['list'] = $list['list']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "openemis_no" => $item['openemis_no'],
                        "first_name" => $item['first_name'],
                        "last_name" => $item['last_name'],
                        "date_of_birth" => Carbon::parse($item['date_of_birth'])->format('d-m-y'),
                        "gender_id" => $item['gender']['name'],
                        "candidate_id" => $item['examinationStudent']['candidate_id'],
                        "examination" => $item['examinationStudent']['examName']['full_name'],
                    ];
                }
            );
            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get Candidate',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get Candidate");
        }
    }
}