<?php

namespace App\Services\Certificates;

use App\Http\Controllers\Controller;
use App\Models\ExaminationStudentsCertificate;
use App\Models\ExaminationStudentsCertificatesStatuse;
use App\Repositories\DuplicatesCertificateRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DuplicatesCertificateService extends Controller
{
    protected $duplicatesCertificateRepository;

    public function __construct(DuplicatesCertificateRepository $duplicatesCertificateRepository)
    {
        $this->duplicatesCertificateRepository = $duplicatesCertificateRepository;
    }

    public function dulicatesListData(Request $request)
    {
        try {
            $response = $this->duplicatesCertificateRepository->duplicatesList($request);
            $list = $response['record']->map(
                function ($item, $key) {
                    $noOfUser = ExaminationStudentsCertificatesStatuse::where(
                        'type',
                        config('constants.certificatetype.duplicate')
                    )
                        ->get();
                    $count = $noOfUser->count();
                    return [
                        "id" => $item['id'],
                        "method" => ($item['method'] == '2') ? 'Post' : 'Collect',
                        "date_received" => $item['date_received'],
                        "date_sent" => $item['date_sent'],
                        'certificate_no' => $item['examinationStudentsCertificate']['certificate_id'],
                        "candidate_id" => $item['examinationStudentsCertificate']['examinationStudent']['candidate_id'],
                        "examination" => $item['examinationStudentsCertificate']['examinationStudent']['examName']['name'],
                        "candidate_name" => $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['full_name'],
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $response['totalRecord'];
                $data['userRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User list not found");
        }
    }

    /**
     * @param string $certificateId
     */
    public function duplicatesCertificateViewData(string $certificateId)
    {
        try {
            $data = $this->duplicatesCertificateRepository->certificateView($certificateId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "method" => ($item['method'] == '2') ? 'Post' : 'Collect',
                        "date_received" => $item['date_received'],
                        "date_sent" => $item['date_sent'],
                        "postal" => $item['postal'],
                        "candidate_id" => $item['examinationStudentsCertificate']['examinationStudent']['candidate_id'],
                        'comments' => $item['comments'],
                        'certificate_no' => $item['examinationStudentsCertificate']['certificate_id'],
                        "examination" => $item['examinationStudentsCertificate']['examinationStudent']['examName']['name'],
                        "first_name" => $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['first_name'],
                        "last_name" => $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['last_name'],
                        "date_of_birth" => $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['date_of_birth'],
                        "gender" => $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['gender']['name'],
                        "modified_by" => $item['modifiedBy']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['securityUserBy']['full_name'],
                        "created_on" => $item['created'],
                    ];
                }
            );
            return $data;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User list not found");
        }
    }

}
