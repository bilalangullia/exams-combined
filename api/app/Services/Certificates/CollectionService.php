<?php

namespace App\Services\Certificates;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\CollectionRepository;

class CollectionService extends Controller
{
	protected $collectionRepository;

    public function __construct(
    	CollectionRepository $collectionRepository
    ) {
    	$this->collectionRepository = $collectionRepository;
    }

    public function getCollectionList(Request $request)
    {
    	try{
    		$list = $this->collectionRepository->getCollectionList($request);
    		$listData = $list['record']->map(
                function ($item, $key) {
                	if($item['method'] == 1){
                		$methodValue = "Collect";
                	} else {
                		$methodValue = "Post";
                	}
                    return [
                        "collection_id"  =>  $item['id'],
                        "candidate_id"   =>  $item['examinationStudentsCertificate']['examinationStudent']['candidate_id'],
                        "candidate_name" =>  $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['full_name'],
                        "examination"    =>  $item['examinationStudentsCertificate']['examinationStudent']['examName']['full_name'],
                        "certificate_number" => $item['examinationStudentsCertificate']['certificate_id'],
                        "date_received"  =>  $item['date_received'] ,
                        "method" 		 =>  $methodValue,
                        "date_sent"		 =>  $item['date_sent']	
                    ];
                }
            );

            if ($listData->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['collectData'] = $listData;
                $data['total'] = $list['totalRecord'];
                return $data;
            }
    	} catch (\Exception $e) {
			Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection List Not Found');
        }
    }

    public function getCollectionDetail(int $collectionId)
    {
        try{
            $detail = $this->collectionRepository->getCollectionDetail($collectionId)->map(
                function ($item, $key) {
                    if($item['method'] == 1){
                        $methodValue = [
                            "key" => 1,
                            "value" => "Collect"
                        ];
                    } else {
                        $methodValue = [
                            "key" => 2,
                            "value" => "Post"
                        ];
                    }
                    return [
                        "collection_id"  =>  $item['id'],
                        "candidate_id"   =>  $item['examinationStudentsCertificate']['examinationStudent']['candidate_id'],
                        "examination"    =>  $item['examinationStudentsCertificate']['examinationStudent']['examName']['full_name'],
                        "certificate_number" => [
                            "key" => $item['examinationStudentsCertificate']['id'],
                            "value" => $item['examinationStudentsCertificate']['certificate_id']
                        ],
                        "first_name"     =>  $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['first_name'],
                        "last_name"      =>  $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['last_name'],
                        "gender"         =>  $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['gender']['name'],
                        "date_of_birth"  =>  $item['examinationStudentsCertificate']['examinationStudent']['securityUser']['date_of_birth'],
                        "date_received"  =>  $item['date_received'] ,
                        "method"         =>  $methodValue,
                        "date_sent"      =>  $item['date_sent'],
                        "postal_address" =>  $item['postal'],
                        "comments"       =>  $item['comments'],
                        "modified_by"    =>  $item['modifiedBy']['full_name'],
                        "modified_on"    =>  $item['modified'],
                        "created_by"     =>  $item['securityUserBy']['full_name'],
                        "created_on"     =>  $item['created']
                    ];
                }
            );;

            return $detail;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection Detail Not Found');
        }
    }
}
