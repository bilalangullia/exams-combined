<?php

namespace App\Services\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\ExamCentreStudentRepository;

class ExamCentreStudentService extends Controller
{

    /**
     * ExamCentreStudentRepository
     * @var ExamCentreStudentRepository
     */
    protected $examCentreStudentRepository;

    /**
     * ExamCentreStudentService constructor.
     * @param $examCentreStudentRepository $examCentreStudentRepository
     */
    public function __construct(ExamCentreStudentRepository $examCentreStudentRepository)
    {
        $this->examCentreStudentRepository = $examCentreStudentRepository;
    }

    /**
     * Getting Exam Centre Student List
     * @param array $search
     * @param string $examCentreId
     * @param $examinationId
     * @return JsonResponse
     */
    public function getExamCentreStudentList(array $search, string $examCentreId, $examinationId)
    {
        try {
            $students = $this->examCentreStudentRepository->getExamCentreStudentList($search, $examCentreId, $examinationId)->map(
                function ($item, $key) {
                    return [
                        'id' => $item['id'],
                        'examination_centre_id' => $item['examination_centre_id'],
                        'student_id' => $item['student_id'],
                        'open_emis_id' => $item['securityUser']['openemis_no'],
                        'name' => $item['securityUser']['first_name'] . " " . $item['securityUser']['middle_name'] . " " . $item['securityUser']['third_name'] . " " . $item['securityUser']['last_name'],
                        'candidate_id' => $item['candidate_id'],
                    ];
                }
            );

            return $students;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch student list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Students Not Found");
        }
    }

    /**
     * Getting Exam Centre Student Details
     * @param string $studentId
     * @return array|JsonResponse
     */
    public function examCentreStudentDetails(string $studentId)
    {
        try {
            $student = $this->examCentreStudentRepository->examCentreStudentDetails($studentId);
            $data = [];
            if ($student) {
                $data['candidate_id'] = $student['candidate_id'];
                $data['examination_centre_id'] = $student['examination_centre_id'];
                $data['examination_id'] = $student['examination_id'];
                $data['student_id'] = $student['student_id'];
                $data['attendance_type'] = $student['attendanceType']['name'];
                $data['exam_centre'] = $student['examinationCentre']['code'] . " - " . $student['examinationCentre']['name'];
                $data['examination'] = $student['examName']['code'] . " - " . $student['examName']['name'];
                $data['academic_year'] = $student['examName']['academicPeriod']['start_year'];
                $data['area'] = $student['examinationCentre']['area']['name'];
                $data['open_emis_id'] = $student['securityUser']['openemis_no'];
                $data['first_name'] = $student['securityUser']['first_name'];
                $data['middle_name'] = $student['securityUser']['middle_name'];
                $data['third_name'] = $student['securityUser']['third_name'];
                $data['last_name'] = $student['securityUser']['last_name'];
                $data['address'] = $student['securityUser']['address'];
                $data['postal_code'] = $student['securityUser']['postal_code'];
                $data['date_of_birth'] = Carbon::parse($student['securityUser']['date_of_birth'])->format('d-m-Y');
                $data['gender'] = $student['securityUser']['gender']['name'];
                $data['nationality'] = $student['securityUser']['nationality']['name'];
                $data['identityType'] = $student['securityUser']['identityType']['name'];
                $data['identity_number'] = $student['securityUser']['identity_number'];
                $data['address_area'] = $student['securityUser']['areaAdministrative']['name'];
                $data['birthplace_area'] = $student['securityUser']['birthplaceArea']['name'];
                $data['options'] = [];
                if (count($student['examinationStudentsOption']) > 0) {
                    $options = $student['examinationStudentsOption'];
                    foreach ($options as $option) {
                        $data['options'][] = [
                            'carry_forward' => $option['carry_forward'],
                            'option_name' => $option['examinationOption']['name'],
                            'option_code' => $option['examinationOption']['code'],
                        ];
                    }
                }
                $data['created_by'] = "";
                if ($student['createdByUser']) {
                    $createdBy = $student['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }
                $data['modified_by'] = "";
                if ($student['modifiedUser']) {
                    $modifiedBy = $student['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
                $data['created'] = isset($student['created']) ? Carbon::parse($student['created'])->format('F d,Y - h:i:s') : "";
                $data['modified'] = isset($student['modified']) ? Carbon::parse($student['modified'])->format('F d,Y - h:i:s') : "";
                $data['special_needs_assessment_type'] = $student['securityUser']['userSpecialNeedsAssessment']['specialNeed']['name'];
                $data['difficulties'] = $student['securityUser']['userSpecialNeedsAssessment']['specialDifficulty']['name'];
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch student details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Student Details Not Found");
        }
    }
}
