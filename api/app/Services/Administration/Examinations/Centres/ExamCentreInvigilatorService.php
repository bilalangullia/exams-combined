<?php

namespace App\Services\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\ExamCentreInvigilatorRepository;

class ExamCentreInvigilatorService extends Controller
{

    /**
     * ExamCentreInvigilatorRepository
     * @var ExamCentreInvigilatorRepository
     */
    protected $examCentreInvigilatorRepository;

    /**
     * ExamCentreInvigilatorService constructor.
     * @param ExamCentreInvigilatorRepository $examCentreInvigilatorRepository
     */
    public function __construct(ExamCentreInvigilatorRepository $examCentreInvigilatorRepository)
    {
        $this->examCentreInvigilatorRepository = $examCentreInvigilatorRepository;
    }

    /**
     * Getting Exam Centre Invigilator List.
     * @param array $search
     * @param string $examCentreId
     * @return array|JsonResponse
     */
    public function examCentreInvigilatorList(array $search, string $examCentreId)
    {
        try {
            $invigilators = $this->examCentreInvigilatorRepository->examCentreInvigilatorList($search, $examCentreId);

            $data = [];
            if ($invigilators) {
                $data['exam_centre_id'] = $invigilators['id'];
                $data['exam_centre_name'] = $invigilators['name'];
                $data['examination_id'] = $invigilators['examination_id'];
                $data['invigilators'] = [];
                if (count($invigilators['invigilators']) > 0) {
                    foreach ($invigilators['invigilators'] as $invigilator) {
                        $data['invigilators'][] = [
                            'invigilator_id' => $invigilator['invigilator_id'],
                            'openemis_no' => $invigilator['openemis_no'],
                            'first_name' => $invigilator['first_name'],
                            'middle_name' => $invigilator['middle_name'],
                            'third_name' => $invigilator['third_name'],
                            'last_name' => $invigilator['last_name'],
                            'full_name' => $invigilator['first_name'] . " " . $invigilator['middle_name'] . " " . $invigilator['middle_name'] . " " . $invigilator['third_name'] . " " . $invigilator['last_name'],
                        ];
                    }
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam Centres Invigilators Not Found");
        }
    }

    /**
     * Getting Exam Centre Invigilator List.
     * @param string $examCentreId
     * @param $invigilatorId
     * @return array|JsonResponse
     */
    public function getExamCentreInvigilatorDetails(string $examCentreId, $invigilatorId)
    {
        try {
            $invigilator = $this->examCentreInvigilatorRepository
                                ->getExamCentreInvigilatorDetails($examCentreId, $invigilatorId);

            $data = [];

            if ($invigilator) {
                $data['exam_centre_name'] = $invigilator['name'];
                $data['exam_centre_id'] = $invigilator['id'];
                $data['invigilator_id'] = $invigilator['invigilators'][0]['invigilator_id'];
                $data['first_name'] = $invigilator['invigilators'][0]['first_name'];
                $data['middle_name'] = $invigilator['invigilators'][0]['middle_name'];
                $data['third_name'] = $invigilator['invigilators'][0]['third_name'];
                $data['last_name'] = $invigilator['invigilators'][0]['last_name'];
                $data['openemis_no'] = $invigilator['invigilators'][0]['openemis_no'];
                $data['academic_year'] = $invigilator['exams']['academicPeriod']['start_year'];
                $data['examination'] = $invigilator['exams']['code'] . " - " . $invigilator['exams']['name'];
                $data['examination_id'] = $invigilator['exams']['id'];
                $data['created_on'] = isset($invigilator['created']) ? Carbon::parse($invigilator['created'])->format('F d,Y - h:i:s') : "";
                $data['created_by'] = '';
                if ($invigilator['createdByUser']) {
                    $createdBy = $invigilator['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam centre invigilator details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Found");
        }
    }

    /**
     * Getting Exam Centre Invigilator Dropdown
     * @param array $search
     * @return array|JsonResponse
     */
    public function getInvigilatorDropdown(array $search)
    {
        try {
            $invigilators = $this->examCentreInvigilatorRepository->getInvigilatorDropdown($search);
            $data = [];
            if (count($invigilators) > 0) {
                foreach ($invigilators as $invigilator) {
                    $data[] = [
                        'key' => $invigilator['id'],
                        'value' => $invigilator['openemis_no'] . " - " . $invigilator['first_name'] . " " . $invigilator['middle_name'] . " " . $invigilator['third_name'] . " " . $invigilator['last_name'],
                    ];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch invigilator dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Found");
        }
    }

    /**
     * Deleting Invigilators
     * @param $invigilatorId
     * @param $examCentreId
     * @param $checkSafeToDelete
     * @return JsonResponse|string
     */
    public function deleteInvigilator(int $invigilatorId, int $examCentreId, $checkSafeToDelete)
    {
        try {
            return $this->examCentreInvigilatorRepository->deleteInvigilator($invigilatorId, $examCentreId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre invigilator.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete invigilator in DB");
        }
    }
}
