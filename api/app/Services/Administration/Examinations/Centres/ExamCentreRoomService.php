<?php

namespace App\Services\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\ExamCentreRoomRepository;

class ExamCentreRoomService extends Controller
{
    /**
     * ExamCentreRoomRepository
     * @var ExamCentreRoomRepository
     */
    protected $examCentreRoomRepository;

    /**
     * ExamCentreRoomService constructor.
     * @param ExamCentreRoomRepository $examCentreRoomRepository
     */
    public function __construct(ExamCentreRoomRepository $examCentreRoomRepository)
    {
        $this->examCentreRoomRepository = $examCentreRoomRepository;
    }

    /**
     * Getting Exam Centre Rooms List
     * @param array $search
     * @param string $examCentreId
     * @return mixed
     */
    public function getExamCentreRoomList(array $search, string $examCentreId)
    {
        try {
            $rooms = $this->examCentreRoomRepository->getExamCentreRoomList($search, $examCentreId)->map(
                function ($item, $key) {
                    return [
                        'id' => $item['id'],
                        'name' => $item['name'],
                        'size' => $item['size'],
                        'number_of_seats' => $item['number_of_seats'],
                        'examination_centre_id' => $item['examination_centre_id'],
                    ];
                }
            );
            return $rooms;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room list not found');
        }
    }

    /**
     * Getting Exam Centre Room Details
     * @param string $roomId
     * @return array
     */
    public function getExamCentreRoomDetails(string $roomId)
    {
        try {
            $roomDetails = $this->examCentreRoomRepository->getExamCentreRoomDetails($roomId);
            $data = [];
            if ($roomDetails) {
                $data['id'] = $roomDetails['id'];
                $data['name'] = $roomDetails['name'];
                $data['size'] = $roomDetails['size'];
                $data['number_of_seats'] = $roomDetails['number_of_seats'];
                $data['examination_centre_id'] = $roomDetails['examination_centre_id'];
                $data['modified_on'] = isset($roomDetails['modified']) ? Carbon::parse($roomDetails['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($roomDetails['created']) ? Carbon::parse($roomDetails['created'])->format('F d,Y - h:i:s') : "";

                $data['created_by'] = "";
                if ($roomDetails['createdByUser']) {
                    $createdBy = $roomDetails['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($roomDetails['modifiedUser']) {
                    $modifiedBy = $roomDetails['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }

                $data['examination_centre'] = $roomDetails['examCentre']['name'];
                $data['examination'] = $roomDetails['examCentre']['exams']['name'];
                $data['examination_code'] = $roomDetails['examCentre']['exams']['code'];
                $data['academic_period_id'] = $roomDetails['examCentre']['exams']['academic_period_id'];
                $data['academic_period'] = $roomDetails['examCentre']['exams']['academicPeriod']['name'];
            }

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room details not found');
        }
    }

    /**
     * Deleting exam centre room
     * @param int $roomId
     * @param int $examCentreId
     * @param $check
     * @return JsonResponse
     */
    public function deleteExamCentreRoom(int $roomId, int $examCentreId, $check)
    {
        try {
            return $this->examCentreRoomRepository->deleteExamCentreRoom($roomId, $examCentreId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre room.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete exam centre room from DB");
        }
    }
}
