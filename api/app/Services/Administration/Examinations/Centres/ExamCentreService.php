<?php

namespace App\Services\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteExamCentreRoom;
use App\Jobs\DeleteExamCentreInvigilator;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\ExamCentreRepository;

class ExamCentreService extends Controller
{
    /**
     * ExamCentreRepository instance
     * @var ExamCentreRepository
     */
    protected $examCentreRepository;

    /**
     * ExamCentreService constructor.
     * @param ExamCentreRepository $examCentreRepository
     */
    public function __construct(ExamCentreRepository $examCentreRepository)
    {
        $this->examCentreRepository = $examCentreRepository;
    }

    /**
     * Getting Exam Center List
     * @param array $search
     * @return JsonResponse
     */
    public function getExamCentreList(array $search)
    {
        try {
            $examCentres = $this->examCentreRepository->getExamCentreList($search)->map(
                function ($item, $key) use ($search) {
                    return [
                        "id" => $item['id'],
                        "centre_code" => $item['code'],
                        "centre_name" => $item['name'],
                        "examination_id" => $item['examination_id'],
                        "area_code" => $item['area']['code'],
                        "area_name" => $item['area']['name'],
                    ];
                }
            );
            return $examCentres;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam Centres Not Found");
        }
    }

    /**
     * Getting exam centre overview
     * @param int $examCentreId
     * @return array|JsonResponse
     */
    public function examCentreOverview(int $examCentreId)
    {
        try {
            $examCentreOverview = $this->examCentreRepository->examCentreOverview($examCentreId);
            
            $data = [];
            if ($examCentreOverview) {
                $data['id'] = $examCentreOverview['id'];
                $data['centre_code'] = $examCentreOverview['code'];
                $data['centre_name'] = $examCentreOverview['name'];
                $data['address'] = $examCentreOverview['address'];
                $data['emis_code'] = $examCentreOverview['emis_code'];
                $data['telephone'] = $examCentreOverview['telephone'];
                $data['postal_code'] = $examCentreOverview['postal_code'];
                $data['contact_person'] = $examCentreOverview['contact_person'];
                $data['modified_on'] = isset($examCentreOverview['modified']) ? Carbon::parse($examCentreOverview['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($examCentreOverview['created']) ? Carbon::parse($examCentreOverview['created'])->format('F d,Y - h:i:s') : "";

                $data['area_code'] = $examCentreOverview['area']['code'];
                $data['area_name'] = $examCentreOverview['area']['name'];

                $data['created_by'] = "";
                if ($examCentreOverview['createdByUser']) {
                    $createdBy = $examCentreOverview['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($examCentreOverview['modifiedUser']) {
                    $modifiedBy = $examCentreOverview['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }

                
                $data['ownership'] = $examCentreOverview['ownership']['name'];
                $data['ownership_id'] = $examCentreOverview['ownership']['id'];

                $data['examination'] = $examCentreOverview['exams']['code'] . " - " . $examCentreOverview['exams']['name'];
                $data['examination_id'] = $examCentreOverview['exams']['id'];
                $data['academic_period_id'] = $examCentreOverview['exams']['academic_period_id'];

                $data['academic_period'] = $examCentreOverview['exams']['academicPeriod']['start_year'];
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam center overview from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centres Overview Not Found");
        }
    }

    /**
     * Deleteing Exam Centres
     * @param int $centreId
     * @param int $parentId
     * @param bool $check
     * @return JsonResponse
     */
    public function deleteExamCentre(int $centreId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->examCentreRepository->checkIfSafeToDelete($centreId);
            }
            if ($canDelete) {
                $invigilatorDeleteJob = new DeleteExamCentreInvigilator(0, $centreId);
                $this->dispatchNow($invigilatorDeleteJob);

                $roomDeleteJob = new DeleteExamCentreRoom(0, $centreId);
                $this->dispatchNow($roomDeleteJob);

                return $this->examCentreRepository->deleteExamCentres($centreId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam centre not deleted.");
        }
    }
}
