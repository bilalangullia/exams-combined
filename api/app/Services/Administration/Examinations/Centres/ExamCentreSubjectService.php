<?php

namespace App\Services\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\ExamCentreSubjectRepository;

class ExamCentreSubjectService extends Controller
{

    /**
     * ExamCentreSubjectRepository
     * @var ExamCentreSubjectRepository
     */
    protected $examCentreSubjectRepository;

    /**
     * ExamCentreSubjectService constructor.
     * @param ExamCentreSubjectRepository $examCentreSubjectRepository
     */
    public function __construct(ExamCentreSubjectRepository $examCentreSubjectRepository)
    {
        $this->examCentreSubjectRepository = $examCentreSubjectRepository;
    }

    /**
     * Getting Subject List.
     * @param array $search
     * @param string $examinationId
     * @return JsonResponse
     */
    public function getExamCentreSubjectList(array $search, string $examinationId)
    {
        try {
            $students = $this->examCentreSubjectRepository->getExamCentreSubjectList($search, $examinationId);

            return $students;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subjects list in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Subjects List Not Found");
        }
    }

    /**
     * Getting Subject Details
     * @param string $examCentreId
     * @param $subjectId
     * @return array|JsonResponse
     */
    public function examCentreSubjectView(string $examCentreId, $subjectId)
    {
        try {
            $subject = $this->examCentreSubjectRepository->examCentreSubjectView($examCentreId, $subjectId);
            $data = [];
            if (isset($subject)) {
                $data['academic_period'] = $subject['exams']['academic_period']['start_year'];
                $data['examination'] = $subject['exams']['code'] . " - " . $subject['exams']['name'];
                $data['examination_id'] = $subject['exams']['id'];
                $data['exam_centre'] = $subject['code'] . " - " . $subject['name'];
                $data['exam_centre_id'] = $subject['id'];
                $data['subject_code'] = "";
                $data['subject_name'] = "";
                $data['subject_id'] = "";
                $data['created_by'] = "";
                $data['created'] = "";
                $data['modified_by'] = "";
                $data['modified'] = "";
                $data['education_subject'] = "";
                if (isset($subject['exams']['examination_subject'])) {
                    $data['subject_code'] = $subject['exams']['examination_subject'][0]['code'];
                    $data['subject_name'] = $subject['exams']['examination_subject'][0]['name'];
                    $data['subject_id'] = $subject['exams']['examination_subject'][0]['id'];
                    if (isset($subject['exams']['examination_subject'][0]['security_user'])) {
                        $createdBy = $subject['exams']['examination_subject'][0]['security_user'];
                        $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                        $data['created'] = isset($subject['exams']['examination_subject'][0]['created']) ? Carbon::parse($subject['exams']['examination_subject'][0]['created'])->format('F d,Y - h:i:s') : "";
                    }

                    if (isset($subject['exams']['examination_subject'][0]['modified_user'])) {
                        $modifiedBy = $subject['exams']['examination_subject'][0]['modified_user'];
                        $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                        $data['modified'] = isset($subject['exams']['examination_subject'][0]['modified']) ? Carbon::parse($subject['exams']['examination_subject'][0]['modified'])->format('F d,Y - h:i:s') : "";
                    }
                    if (isset($subject['exams']['examination_subject'][0]['education_subject'])) {
                        $data['education_subject'] = $subject['exams']['examination_subject'][0]['education_subject']['name'];
                    }

                    $data['subject_id'] = $subject['exams']['examination_subject'][0]['id'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subjects details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Subjects Details Not Found");
        }
    }
}
