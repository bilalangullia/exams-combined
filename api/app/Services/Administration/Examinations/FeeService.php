<?php

namespace App\Services\Administration\Examinations;

use App\Http\Controllers\Controller;
use App\Models\ExaminationFee;
use App\Models\ExaminationStudentsFee;
use App\Repositories\FeeRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FeeService extends Controller
{
    /**
     * FeeRepository Instance
     * @var FeeRepository
     */
    protected $feeRepository;

    /**
     * FeeController constructor.
     * @param FeeRepository $feeRepository
     */
    public function __construct(FeeRepository $feeRepository)
    {
        $this->feeRepository = $feeRepository;
    }

    /**
     * Getting Examination Fee List
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationsFeeList(Request $request)
    {
        try {
            $feeList = $this->feeRepository->getExaminationsFeeList($request)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "amount" => number_format($item['amount'], 2, '.', ''),
                    ];
                }
            );

            return $feeList;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee List Not found");
        }
    }

    /**
     *  Getting Examination Fee View
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationsFeeDetails(Request $request, string $examFeeId)
    {
        try {
            $feeList = $this->feeRepository->getExaminationFeeDetails($request, $examFeeId)->map(
                function ($item, $key) {
                    return [
                        "academic_period" => $item['examination']['academicPeriod']['name'],
                        "examination" => $item['examination']['name'],
                        "name" => $item['name'],
                        "amount" => number_format($item['amount'], 2, '.', ''),
                        "modified_by" => ($item['modified_user_id']) ? $item['modified_user_id'] : '',
                        "modified_on" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : '',
                        "created_by" => $item['securityUser']['full_name'],
                        "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );

            return $feeList;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee details Not found");
        }
    }

    /**
     * Deleting examination fee
     * @param $response
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function deleteExaminationFee($response)
    {
        try {
            $data = $this->feeRepository->checkingExaminationFee($response);
            if ($data['data']) {
                return "Delete Operation is not allowed as there are other information linked to this record";
            } else {
                $del = ExaminationStudentsFee::where('examination_fees_id', $data['examFeeId'])->delete();
                $deleteParent = ExaminationFee::where('id', $data['examFeeId'])->delete();

                return "Examination's Fee Removed successfully";
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Not Removed");
        }
    }
}