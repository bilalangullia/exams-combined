<?php

namespace App\Services\Administration\Examinations;

use App\Http\Controllers\Controller;
use App\Repositories\ComponentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MultipleChoiceService extends Controller
{
    /**
     * ComponentRepository instance
     * @var ComponentRepository
     */
    protected $componentRepository;

    /**
     * MultipleChoiceService constructor.
     * @param ComponentRepository $componentRepository
     */
    public function __construct(ComponentRepository $componentRepository)
    {
        $this->componentRepository = $componentRepository;
    }

    /**
     * Examination Multiple Choice Listing
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationMultipleChoiceList(Request $request, string $examId)
    {
        try {
            $multipleChoiceList = $this->componentRepository->getExaminationcomponetList($request, $examId);

            return $multipleChoiceList;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Examination Multiple Choice List Not Found');
        }
    }

}