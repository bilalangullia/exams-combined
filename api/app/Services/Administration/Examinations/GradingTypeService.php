<?php

namespace App\Services\Administration\Examinations;

use App\Http\Controllers\Controller;
use App\Repositories\GradingTypeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class GradingTypeService extends Controller
{
    /**
     * GradingTypeRepository instance
     * @var GradingTypeRepository
     */
    protected $gradingTypeRepository;

    public function __construct(GradingTypeRepository $gradingTypeRepository)
    {
        $this->gradingTypeRepository = $gradingTypeRepository;
    }

    /**
     * Grading type listing
     * @param array $search
     * @return JsonResponse
     */
    public function getGradingTypeList(array $search)
    {
        try {
            $gradingType = $this->gradingTypeRepository->gradingTypeList($search)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "max" => $item['max'],
                        "result_type" => $item['result_type'],
                    ];
                }
            );
            return $gradingType;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }

    /**
     * Grading Type Details
     * @param int $gradingTypeId
     * @return array|JsonResponse
     */
    public function getGradingTypeDetails(int $gradingTypeId)
    {
        try {
            $gradingTypeDetails = $this->gradingTypeRepository->getGradingTypeDetails($gradingTypeId);

            $data = [];
            if ($gradingTypeDetails) {
                $createdUser = $gradingTypeDetails->createdByUser;
                $modifiedUser = $gradingTypeDetails->modifiedUser;
                $data['id'] = $gradingTypeDetails->id;
                $data['code'] = $gradingTypeDetails->code;
                $data['name'] = $gradingTypeDetails->name;
                $data['pass_mark'] = $gradingTypeDetails->pass_mark;
                $data['max'] = $gradingTypeDetails->max;
                $data['created'] = Carbon::parse($gradingTypeDetails->created)->format('F d,Y - h:i:s');
                if ($createdUser) {
                    $data['created_by'] = $createdUser->first_name . " " . $createdUser->middle_name . " " . $createdUser->third_name . " " . $createdUser->last_name;
                } else {
                    $data['created_by'] = "";
                }

                if ($modifiedUser) {
                    $data['modified_by'] = $modifiedUser->first_name . " " . $modifiedUser->middle_name . " " . $modifiedUser->third_name . " " . $modifiedUser->last_name;
                } else {
                    $data['modified_by'] = "";
                }
                 $data['result_type'] = $gradingTypeDetails->result_type;

                $data['gradingOptions'] = [];
                foreach ($gradingTypeDetails->examinationGradingOption as $o) {
                    $data['gradingOptions'][] = [
                        'id' => $o->id,
                        'name' => $o->name,
                        'description' => $o->description,
                        'min' => $o->min,
                        'max' => $o->max,
                        'points' => $o->points,
                    ];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch grading type details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }

    /**
     * Grading type details update
     * @param array $request
     * @return JsonResponse
     */
    public function updateGradingType(array $request)
    {
        try {
            $update = $this->gradingTypeRepository->updateGradingType($request);
            return $update;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch grading type update in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }

    /**
     * Deleteing Grading type
     * @param int $gradingTypeId
     * @param int $parentId
     * @param bool $check
     * @return JsonResponse
     */
    public function gradingTypeDelete(int $gradingTypeId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->gradingTypeRepository->checkIfSafeToDeleteGradingType($gradingTypeId);
            }

            if ($canDelete) {
                return $this->gradingTypeRepository->gradingTypeDelete($gradingTypeId, $parentId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete Grading type',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete Grading type");
        }
    }
}
