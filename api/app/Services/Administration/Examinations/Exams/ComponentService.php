<?php

namespace App\Services\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteItem;
use App\Models\ExaminationComponentsMultiplechoice;
use App\Repositories\ComponentRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ComponentService extends Controller
{
    /**
     * @var ComponentRepository
     */
    protected $componentRepository;

    /**
     * ComponentService constructor.
     * @param ComponentRepository $componentRepository
     */

    public function __construct(
        ComponentRepository $componentRepository
    ) {
        $this->componentRepository = $componentRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComponentList(Request $request, string $examId)
    {
        try {
            $listcompoent = $this->componentRepository->getExaminationcomponetList($request, $examId);
            $list = $listcompoent['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "component_code" => $item['code'],
                        "component_name" => $item['name'],
                        "option" => $item['examinationOption']['code'] . '-' . $item['examinationOption']['name'],
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $listcompoent['totalRecord'];
                $data['userRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('List Not Found');
        }
    }

    /**
     * Getting component answer list
     * @param string $compId
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getComponentAnswersList(string $componentId)
    {
        try {
            $answerList = ExaminationComponentsMultiplechoice::select(
                'id',
                'answer',
                'question',
                'examination_components_id'
            )->where('examination_components_id', $componentId)->get();

            return $answerList;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Component Answer List Not Found');
        }
    }

    /**
     * @param Request $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getcomponentView(Request $request, string $componentId)
    {
        try {
            $componentview = $this->componentRepository->getExaminationComponentView($request, $componentId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "component_code" => $item['code'],
                        "component_name" => $item['name'],
                        "weighting" => $item['weight'],
                        "carry_forward" => ($item['carry_forward'] == '0') ? 'No' : 'Yes',
                        "date" => $item['examination_date'],
                        "start_time" => $item['start_time'],
                        "end_time" => $item['end_time'],
                        "max_raw_mark" => $item['max_raw_mark'],
                        "max_con_mark" => $item['max_con_mark'],
                        "component_type" => $item['componentType']['name'],
                        "academic_period" => $item['examinationOption']['examinationSubject']['examName']['academicPeriod']['name'],
                        "answers" => $this->getComponentAnswersList($item['id']),
                        "examination" => $item['examinationOption']['examinationSubject']['examName']['full_name'],
                        "option_code" => $item['examinationOption']['code'],
                        "option_name" => $item['examinationOption']['name'],
                        "subject_code" => $item['examinationOption']['examinationSubject']['code'],
                        "subject_name" => $item['examinationOption']['examinationSubject']['name'],
                        "mark_type" => $item['examinationComponentsMarkType']['markType']['name'],
                        "examination_grading_type" => $item['examinationGradingType']['name'],
                        "modified_user_id" => ($item['modified_user_id']) ? $item['modified_user_id'] : "",
                        "modified" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : "",
                        "created_user_id" => $item['securityUser']['full_name'],
                        "created" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['componentview' => $componentview]]);

            return $componentview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('List Not Found');
        }
    }

    /**
     * @param Request $request
     * @param string $componentId
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function getcomponentEdit(Request $request, string $componentId)
    {
        try {
            $componentview = $this->componentRepository->getExaminationComponentEdit($request, $componentId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "component_code" => $item['code'],
                        "component_name" => $item['name'],
                        "weighting" => $item['weight'],
                        //"carry_forward" => $item['carry_forward'],
                        "date" => $item['examination_date'],
                        "start_time" => $item['start_time'],
                        "end_time" => $item['end_time'],
                        "max_raw_mark" => $item['max_raw_mark'],
                        "max_con_mark" => $item['max_con_mark'],
                        "carry_forward" => [
                            "key" => ($item['carry_forward'] == '1') ? '1' : '0',
                            "value" => ($item['carry_forward'] == '1') ? 'Yes' : 'No',
                        ],
                        "component_type" => [
                            "key" => $item['componentType']["id"],
                            "value" => $item['componentType']["name"],
                        ],
                        "mark_type" => [
                            "key" => $item['examinationComponentsMarkType']['markType']["id"],
                            "value" => $item['examinationComponentsMarkType']['markType']["name"],
                        ],
                        "examination_grading_type" => [
                            "key" => $item['examinationGradingType']["id"],
                            "value" => $item['examinationGradingType']["name"],
                        ],
                        "academic_period" => $item['examinationOption']['examinationSubject']['examName']['academicPeriod']['name'],
                        "answers" => $this->getComponentAnswersList($item['id']),
                        "examination" => $item['examinationOption']['examinationSubject']['examName']['full_name'],
                        "option_code" => $item['examinationOption']['code'],
                        "option_name" => $item['examinationOption']['name'],
                        "subject_code" => $item['examinationOption']['examinationSubject']['code'],
                        "subject_name" => $item['examinationOption']['examinationSubject']['name'],
                        "modified_user_id" => ($item['modified_user_id']) ? $item['modified_user_id'] : "",
                        "modified" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : "",
                        "created_user_id" => $item['securityUser']['full_name'],
                        "created" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['componentview' => $componentview]]);

            return $componentview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('List Not Found');
        }
    }

    /**
     * deleting examination component and related tables
     */
    public function deleteExaminationComponent(int $componentId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->componentRepository->checkIfSafeToDelete($componentId);
            }
            if ($canDelete) {
                $delcomp = new DeleteItem(0, $componentId);
                $this->dispatchNow($delcomp);
                return $this->componentRepository->deleteExamComponent($componentId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete examination component.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination component not deleted.");
        }
    }
}
