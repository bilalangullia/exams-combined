<?php

namespace App\Services\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteOption;
use App\Repositories\SubjectRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SubjectService extends Controller
{
    /**
     * Subject repository Instance
     * @var SubjectRepository
     */
    protected $subjectRepository;

    /**
     * SubjectService constructor.
     * @param SubjectRepository $SubjectRepository
     */
    public function __construct(
        SubjectRepository $subjectRepository
    ) {
        $this->subjectRepository = $subjectRepository;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function subjectListing(Request $request)
    {
        try {
            $listsubject = $this->subjectRepository->subjectList($request);
            $list = $listsubject['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "examination" => $item['examName']['code'] . "-" . $item['examName']['name'],
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $listsubject['totalRecord'];
                $data['userRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('List Not Found');
        }
    }

    /**
     * @param Request $request
     * @param int $examsubjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function examSubjectView(Request $request, int $examsubjectId)
    {
        try {
            $examsubjectView = $this->subjectRepository->subjectView($request, $examsubjectId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "education_subject" => $item['educationSubject']['name'],
                        "examination" => $item['examName']['name'],
                        "modified_user_id" => ($item['modified_user_id']) ? $item['modified_user_id'] : "",
                        "modified" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : "",
                        "created_user_id" => $item['securityUser']['full_name'],
                        "created" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),

                    ];
                }
            );
            Log::info('Fetched view from DB', ['method' => __METHOD__, 'data' => ['listsubject' => $examsubjectView]]);

            return $examsubjectView;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('subject view Not Found');
        }
    }

    /**
     * @param Request $request
     * @param int $examsubjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function examSubjectEdit(Request $request, int $examsubjectId)
    {
        try {
            $examsubjectView = $this->subjectRepository->subjectView($request, $examsubjectId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "education_subject" => [
                            "key" => $item['educationSubject']["id"],
                            "value" => $item['educationSubject']["name"],
                        ],
                        "examination" => $item['examName']['name'],
                        "modified_user_id" => ($item['modified_user_id']) ? $item['modified_user_id'] : "",
                        "modified" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : "",
                        "created_user_id" => $item['created_user_id']['name'],
                        "created" => $item['created']['name'],

                    ];
                }
            );
            Log::info(
                'Fetched view from DB',
                ['method' => __METHOD__, 'data' => ['examsubjectView' => $examsubjectView]]
            );

            return $examsubjectView;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('subject view Not Found');
        }
    }

    /**
     * deleting examination subject
     * @param $data
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function deleteExaminationSubject(int $examSubjectId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->subjectRepository->checkIfSafeToDelete($examSubjectId);
            }
            if ($canDelete) {
                $deloption = new DeleteOption(0, $examSubjectId);
                $this->dispatchNow($deloption);
                return $this->subjectRepository->deleteExamSubject($examSubjectId);
            }
            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete examination marker.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination subject not deleted.");
        }
    }
}
