<?php

namespace App\Services\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Repositories\ItemsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ItemsService extends Controller
{
    /**
     * ItemsRepository instance
     * @var ItemsRepository
     */
    protected $itemsRepository;

    /**
     * ItemsService constructor.
     * @param ItemsRepository $itemsRepository
     */
    public function __construct(
        ItemsRepository $itemsRepository
    ) {
        $this->itemsRepository = $itemsRepository;
    }

    /**
     * Getting admin >> examination >> item tab listing
     * @param Request $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationItemsList(Request $request, string $examId)
    {
        try {
            $data = $this->itemsRepository->getExaminationItemListing($request, $examId);
            $itemList = $data['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "component_id" => $item['examinationComponent']['id'],
                        "component_name" => $item['examinationComponent']['full_name'],
                        "item_id" => $item['item']['id'],
                        "item_code" => $item['item']['code'],
                        "item_name" => $item['item']['name']
                    ];
                }
            );

            $response = [
                "start" => $request['start'],
                "end" => $request['end'],
                "itemList" => $itemList,
                "totalRecords" => $data['totalRecord']
            ];

            return $response;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Items List Not Found");
        }
    }

    /**
     * Getting admin >> examination >> item tab details
     * @param Request $request
     * @param string $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationItemsDetails(Request $request, string $itemId)
    {
        try {
            $itemList = $this->itemsRepository->getExaminationItemsDetails($request, $itemId)->map(
                function ($item, $key) {
                    return [
                        "subject" => [
                            "key" => $item['examinationComponent']['examinationOption']['examinationSubject']['id'],
                            "value" => $item['examinationComponent']['examinationOption']['examinationSubject']['full_name'],
                        ],
                        "option" => [
                            "key" => $item['examinationComponent']['examinationOption']['id'],
                            "value" => $item['examinationComponent']['examinationOption']['full_name'],
                        ],
                        "component" => [
                            "key" => $item['examinationComponent']['id'],
                            "value" => $item['examinationComponent']['full_name'],
                        ],
                        "item_code" => $item['item']['code'],
                        "item_name" => $item['item']['name'],
                        "item" => [
                            "key" => $item['item']['id'],
                            "value" => $item['item']['full_name']
                        ],
                        "modified_by" => ($item['modified_user_id']) ? $item['modified_user_id'] : '',
                        "modified_on" => ($item['modified']) ? $item['modified'] : '',
                        "created_by" => $item['securityUser']['full_name'],
                        "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );

            return $itemList;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Items List Not Found");
        }
    }


}
