<?php

namespace App\Services\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteComponent;
use App\Models\ExaminationGradeReviewCriteria;
use App\Models\ExaminationOption;
use App\Models\ExaminationStudentsForecastGrade;
use App\Models\ExaminationStudentsOption;
use App\Models\ExaminationStudentsOptionsGrade;
use App\Repositories\OptionRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OptionService extends Controller
{
    /**
     * OptionRepository instance
     * @var OptionRepository
     */
    protected $optionRepository;

    /**
     * OptionController constructor.
     * @param OptionRepository $optionRepository
     */
    public function __construct(
        OptionRepository $optionRepository
    ) {
        $this->optionRepository = $optionRepository;
    }

    /**
     * Getting examination option list
     * @param Request $request
     * @param string $subjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationOptionList(Request $request, string $examId)
    {
        try {
            $data = $this->optionRepository->getExaminationOptionList($request, $examId);
            $optionList = $data['record']->map(
                function ($item, $key) {
                    return [
                        "subject" => $item['examinationSubject']['full_name'],
                        "option_id" => $item['id'],
                        "option_code" => $item['code'],
                        "option_name" => $item['name'],
                    ];
                }
            );

            $response = [
                "start" => $request['start'],
                "end" => $request['end'],
                "optionList" => $optionList,
                "totalRecords" => $data['totalRecord']
            ];

            return $response;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Options List Not Found");
        }
    }

    /**
     * Getting examination option details
     * @param Request $request
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationOptionDetails(Request $request, string $optionId)
    {
        try {
            $optionView = $this->optionRepository->getExaminationOptionDetails($request, $optionId)->map(
                function ($item, $key) {
                    return [
                        "subject" => [
                            "key" => $item['examinationSubject']['id'],
                            "value" => $item['examinationSubject']['full_name'],
                        ],
                        "option_code" => $item['code'],
                        "option_name" => $item['name'],
                        "grading_type" => [
                            "key" => $item['examinationGradingType']['id'],
                            "value" => $item['examinationGradingType']['name'],
                        ],
                        "attendance_type" => [
                            "key" => $item['attendanceType']['id'],
                            "value" => $item['attendanceType']['name'],
                        ],
                        "modified_by" => ($item['modified_user_id']) ? $item['modified_user_id'] : '',
                        "modified_on" => ($item['modified']) ? $item['modified'] : '',
                        "created_by" => $item['securityUser']['full_name'],
                        "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),

                    ];
                }
            );

            return $optionView;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Options Details Not Found");
        }
    }

    /**
     * @param $response
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function deleteExamOption(int $optionId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->optionRepository->checkIfSafeToDelete($optionId);
            }
            if ($canDelete) {
                $componentDeleteJob = new DeleteComponent(0, $optionId);
                $this->dispatchNow($componentDeleteJob);

                return $this->optionRepository->deleteExamOption($optionId);
            }
            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam option.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed To Delete Examination Option");
        }
    }
}
