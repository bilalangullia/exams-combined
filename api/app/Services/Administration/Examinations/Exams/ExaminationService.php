<?php

namespace App\Services\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Repositories\ExaminationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Jobs\DeleteExamCentre;
use App\Jobs\DeleteExamSubject;

class ExaminationService extends Controller
{
    /**
     * ExaminationRepository Instance
     * @var ExaminationRepository
     */
    protected $examinationRepository;

    /**
     * ExaminationController constructor.
     * @param ExaminationRepository $examinationRepository
     * @param ExaminationService $feeService
     */
    public function __construct(
        ExaminationRepository $examinationRepository
    ) {
        $this->examinationRepository = $examinationRepository;
    }

    /**
     * Getting examination >> exam tab >> listing
     * @param Request $request
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function getExaminationExamList(Request $request)
    {
        try {
            $data = $this->examinationRepository->getExaminationExamList($request);
            $examList  = $data['record']->map(
                function ($item, $key) {
                    return [
                        "exam_id" => $item['id'],
                        "exam_code" => $item['code'],
                        "exam_name" => $item['name'],
                        "exam_type" => $item['examinationType']['name'],
                        "exam_session" => ($item['examSessionType']['name']) ? $item['examSessionType']['name'] : "",
                        "academic_period_id" => $item['academicPeriod']['id'],
                        "academic_period" => $item['academicPeriod']['name'],
                        "education_grade" => $item['educationGrade']['code'],

                    ];
                }
            );
            
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['examList' => $examList]]);
            
            $response = [
                "start" => $request['start'],
                "end" => $request['end'],
                "examList" => $examList,
                "totalRecords" => $data['totalRecord']
             ];

            return $response;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting examination >> exam tab >> view
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationExamDetail(Request $request, string $examId)
    {
        try {
            $examView = $this->examinationRepository->getExaminationExamDetails($request, $examId)->map(
                function ($item, $key) {
                    return [
                        "exam_id" => $item['id'],
                        "exam_code" => $item['code'],
                        "exam_name" => $item['name'],
                        "exam_type" => [
                            "key" => $item['examinationType']['id'],
                            "value" => $item['examinationType']['name'],
                        ],
                        "exam_session" => [
                            "key" => $item['examSessionType']['id'],
                            "value" => $item['examSessionType']['name'],
                        ],
                        "description" => $item['description'],
                        "attendance_name" => [
                            "key" => $item['attendanceType']['id'],
                            "value" => $item['attendanceType']['name'],
                        ],
                        "academic_period" => [
                            "key" => $item['academicPeriod']['id'],
                            "value" => $item['academicPeriod']['name'],
                        ],
                        "education_programme" => [
                            "key" => $item['educationGrade']['educationProgramme']['id'],
                            "value" => $item['educationGrade']['educationProgramme']['name'],
                        ],
                        "education_grade" => [
                            "key" => $item['educationGrade']['id'],
                            "value" => $item['educationGrade']['name'],
                        ],
                        "registration_start_date" => Carbon::parse($item['registration_start_date'])->format('Y-m-d'),
                        "registration_end_date" => Carbon::parse($item['registration_end_date'])->format('Y-m-d'),
                        "modified_by" => ($item['modified_user_id']) ? $item['modified_user_id'] : '',
                        "modified_on" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : '',
                        "created_by" => $item['securityUser']['full_name'],
                        "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['examView' => $examView]]);

            return $examView;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam List Not Found");
        }
    }
    public function deleteExamination(int $examId, int $parentId, $check)
    {
        try{
            $canDelete = true;
            if ($check) {
                $canDelete = $this->examinationRepository->checkIfSafeToDelete($examId);
            }
            if ($canDelete) {
                $subjectDeleteJob = new DeleteExamSubject(0, $examId);
                $this->dispatchNow($subjectDeleteJob);

                $dltExamCentreJob = new DeleteExamCentre(0, $examId);
                $this->dispatchNow($dltExamCentreJob);

                return $this->examinationRepository->deleteExam($examId);
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete exam.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }

    }
}