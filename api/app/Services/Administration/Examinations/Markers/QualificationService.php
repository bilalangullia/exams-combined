<?php

namespace App\Services\Administration\Examinations\Markers;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteMarkerQualification;
use App\Models\StaffQualificationsSpecialisation;
use App\Models\StaffQualificationsSubject;
use App\Repositories\QualificationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class QualificationService extends Controller
{
    /**
     * @var QualificationRepository
     */
    protected $qualificationRepository;

    /**
     * QualificationService constructor.
     * @param QualificationRepository $qualificationRepository
     */
    public function __construct(
        QualificationRepository $qualificationRepository
    ) {
        $this->qualificationRepository = $qualificationRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQualificationlist(Request $request, string $openEmisno)
    {
        try {
            $details = $this->qualificationRepository->qualificationListing($request, $openEmisno)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "title" => $item['qualificationTitle']['name'],
                        "level" => $item['qualificationTitle']['qualificationLevel']['name'],
                        "graduate_year" => $item['graduate_year'],
                        "document_no" => $item['document_no'],
                        "institution" => $item['qualification_institution'],
                        "file_type" => $item['file_name'],
                        "field_of_study" => $item['educationFieldOfStudie']['names'],
                    ];
                }
            );
            return $details;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification list Not Found');
        }
    }

    /**
     * @param string $titleid
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQualificationView(string $staffqualificationId)
    {
        try {
            $qualificationview = $this->qualificationRepository->qualificationView($staffqualificationId)->map(
                function ($item, $key) use ($staffqualificationId) {
                    return [
                        "id" => $item['id'],
                        "title" => $item['qualificationTitle']['name'],
                        "level" => $item['qualificationTitle']['qualificationLevel']['name'],
                        "graduate_year" => $item['graduate_year'],
                        "document_no" => $item['document_no'],
                        "institution" => $item['qualification_institution'],
                        "file_type" => $item['file_name'],
                        "field_of_study" => $item['educationFieldOfStudie']['names'],
                        "country" => $item['countrie']['name'],
                        "subjects" => $this->getQualificationSubject($staffqualificationId),
                        "grade_score" => $item['gpa'],
                        "specialisation" => $this->getQualificationSpecialisation($staffqualificationId),
                        "modified_user_id" => ($item['modified_user_id']) ? $item['modified_user_id'] : "",
                        "modified" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : "",
                        "created_user_id" => $item['securityUser']['full_name'],
                        "created" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            return $qualificationview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification view Not Found');
        }
    }
    /**
     * @param string $titleid
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQualificationedit(string $staffqualificationId)
    {
        try {
            $qualificationview = $this->qualificationRepository->qualificationView($staffqualificationId)->map(
                function ($item, $key) use ($staffqualificationId) {
                    return [
                        "id" => $item['id'],
                        "title" => [
                            "key" => $item['qualificationTitle']["id"],
                            "value" => $item['qualificationTitle']["name"],
                        ],
                        "level" => $item['qualificationTitle']['qualificationLevel']['name'],
                        "graduate_year" => [
                            "key" => $item["id"],
                            "value" => $item["graduate_year"],
                        ],
                        "document_no" => $item['document_no'],
                        "institution" => $item['qualification_institution'],
                        "specialisation" => $this->getQualificationSpecialisation($staffqualificationId),
                        "subjects" => $this->getQualificationSubject($staffqualificationId),
                        "country" => [
                            "key" => $item['countrie']['id'],
                            "value" => $item['countrie']['name'],
                        ],
                        "institution" => $item['qualification_institution'],
                        "field_of_study" => [
                            "key" => $item['educationFieldOfStudie']['id'],
                            "value" => $item['educationFieldOfStudie']['names'],
                        ],
                        "grade_score" => $item['gpa'],
                        "modified_by" => ($item['modified_user_id']) ? $item['modified_user_id'] : '',
                        "modified_on" => ($item['modified']) ? $item['modified'] : '',
                        "created_by" => $item['securityUser']['full_name'],
                        "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            return  $qualificationview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification edit from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification edit Not Found');
        }
    }

    /**
     * @param string $staffqualificationId
     * @return array|\Illuminate\Http\JsonResponse
     */
    protected function getQualificationSubject(string $staffqualificationId)
    {
        try {
            $list = [];
            $count = 0;
            $subject = staffQualificationsSubject::with(
                'educationSubject:id,name,code'
            )->where('staff_qualification_id', $staffqualificationId)->get()->map(
                function ($item, $key) use (&$list, &$count) {
                    foreach ($item['educationSubject'] as $key => $value) {
                        $list[$count]['id'] = $value['id'];
                        $list[$count]['code'] = $value['code'];
                        $list[$count]['name'] = $value['name'];
                        $count++;
                    }
                }
            );
            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification edit from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification subject Not Found');
        }
    }

    /**
     * @param string $staffqualificationId
     * @return array|\Illuminate\Http\JsonResponse
     */
    protected function getQualificationSpecialisation(string $staffqualificationId)
    {
        try {
            $listspecialisation = [];
            $count = 0;
            $specialisation = StaffQualificationsSpecialisation::with(
                'qualificationSpecialisation:id,name'
            )->where('staff_qualification_id', $staffqualificationId)->get()->map(
                function ($item, $key) use (&$listspecialisation, &$count) {
                    foreach ($item['qualificationSpecialisation'] as $key => $value) {
                        $listspecialisation[$count]['id'] = $value['id'];
                        $listspecialisation[$count]['name'] = $value['name'];
                        $count++;
                    }
                }
            );
            return $listspecialisation;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification edit from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification specialisation Not Found');
        }
    }

    /**
     * @param int $staffqualificationId
     * @param $check
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function deleteQualification(int $staffqualificationId, $check)
    {
        try {
            return $this->qualificationRepository->deleteStaffQualification($staffqualificationId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete Qualification.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification not deleted..");
        }
    }
}
