<?php

namespace App\Services\Administration\Examinations\Markers;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteExamMarker;
use App\Jobs\DeleteMarkerQualification;
use App\Models\ExaminationMarker;
use App\Models\ExaminationMarkersSubject;
use App\Models\SecurityUser;
use App\Repositories\MarkerRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MarkerService extends Controller
{
    /**
     * @var MarkerRepository
     */
    protected $markerRepository;

    /**
     * MarkerService constructor.
     * @param MarkerRepository $markerRepository
     */
    public function __construct(
        MarkerRepository $markerRepository
    ) {
        $this->markerRepository = $markerRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listingMarker(Request $request)
    {
        try {
            $details = $this->markerRepository->markerList($request)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "openemis_no" => $item['securityUser']['openemis_no'],
                        "marker_id" => $item['marker_id'],
                        "first_name" => $item['securityUser']['first_name'],
                        "last_name" => $item['securityUser']['last_name'],
                        "gender_id" => $item['securityUser']['gender']['name'],
                    ];
                }
            );
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['MarkerList' => $details]]);
            return $details;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch marker list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Markers List Not Found');
        }
    }

    /**
     * @param Request $request
     * @param string $openemisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMarkerView(string $openemisno)
    {
        try {
            $viewdata = $this->markerRepository->markerview($openemisno)->map(
                function ($item, $key) use ($openemisno) {
                    return [
                        "id" => $item['id'],
                        "first_name" => $item['first_name'],
                        "middle_name" => $item['middle_name'],
                        "third_name" => $item['third_name'],
                        "last_name" => $item['last_name'],
                        "openemis_no" => $item['openemis_no'],
                        "address" => $item['address'],
                        "identity_number" => $item['identity_number'],
                        "gender_id" => $item['gender']['name'],
                        "marker_id" => $item['examinationMarker']['marker_id'],
                        "birthplace_area" => $item['birthplaceArea']['name'],
                        "address_area" => $item['areaAdministrative']['name'],
                        "nationality" => $item['nationality']['name'],
                        "identityType" => $item['identityType']['name'],
                        "classification" => $item['name'],
                       "subjects" => $this->getMarkerSubject($openemisno),
                        "postal_code" => $item['postal_code'],
                        "date_of_birth" => $item['date_of_birth'],
                        "examination_id" => $item['examinationMarker']['examName']['name'],
                       "examination_centre_id" => $item['examinationMarker']['examinationCentre']['name'],
                        "academic_period" => $item['examinationMarker']['examName']['academicPeriod']['name'],
                        "area" => $item['examinationMarker']['examinationCentre']['area']['name'],
                        "modified_user_id" => ($item['modified_user_id']) ? $item['modified_user_id'] : "",
                        "modified" => ($item['modified']) ? Carbon::parse($item['modified'])->format(
                            'F d,Y - h:i:s'
                        ) : "",
                        "created_user_id" => $item['examinationMarker']['securityUserCreatedId']['full_name'],
                        "created" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            return $viewdata;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch marker view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Marker View Not Found');
        }
    }

    /**
     * @param string $openemisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMarkerEdit(string $openemisno)
    {
        try {
            $editdata = $this->markerRepository->markerview($openemisno)->map(
                function ($item, $key) use ($openemisno) {
                    return [
                        "id" => $item['id'],
                        "first_name" => $item['first_name'],
                        "middle_name" => $item['middle_name'],
                        "third_name" => $item['third_name'],
                        "last_name" => $item['last_name'],
                        "openemis_no" => $item['openemis_no'],
                        "address" => $item['address'],
                        "marker_id" => $item['examinationMarker']['marker_id'],
                        "postal_code" => $item['postal_code'],
                        "gender_id" => [
                            "key" => $item["gender"]["id"],
                            "value" => $item["gender"]["name"],
                        ],
                        "area_id" => [
                            "key" => $item['examinationMarker']["examinationCentre"]['area']["id"],
                            "value" => $item['examinationMarker']["examinationCentre"]['area']["name"],
                        ],
                        "address_area_id" => [
                            "key" => $item["areaAdministrative"]["id"],
                            "value" => $item["areaAdministrative"]["name"],
                        ],
                        "birthplace_area_id" => [
                            "key" => $item["birthplacearea"]["id"],
                            "value" => $item["birthplacearea"]["name"],
                        ],
                        "date_of_birth" => $item['date_of_birth'],
                        "nationality_id" => [
                            "key" => $item["nationality"]["id"],
                            "value" => $item["nationality"]["name"],
                        ],
                        "identity_type_id" => [
                            "key" => $item["identityType"]["id"],
                            "value" => $item["identityType"]["name"],
                        ],
                        "classification" => [
                            "key" => $item["id"],
                            "value" => $item["name"],
                        ],
                        "identity_number" => $item['identity_number'],
                        "examination_id" => $item['examinationMarker']['examName']['name'],
                        "academic_period" => $item['examinationMarker']['examName']['academicPeriod']['name'],
                        "examination_centre_id" => [
                            "key" => $item["examinationMarker"]["examinationCentre"]["id"],
                            "value" => $item["examinationMarker"]["examinationCentre"]["name"]
                        ],
                        "subjects" => $this->getMarkerSubject($openemisno),
                        "modified_user_id" => config('modifiedUser.user_id'),
                        "modified" =>  Carbon::now()->toDateTimeString(),
                        "created_user_id" => $item['createdByUser']['full_name'],
                        "created" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                    ];
                }
            );
            return $editdata;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch marker view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Marker Not Found');
        }
    }

    /**
     * @param string $examinerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetailsByExaminerId(string $examinerId)
    {
        try {
            $detailsId = $this->markerRepository->detailsByExaminerId($examinerId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "marker_id" => $item['marker_id'],
                        "examination_centre" => [
                            "key" => $item["examinationCentre"]["id"],
                            "value" => $item["examinationCentre"]["name"]
                        ],
                        "first_name" => $item['securityUser']['first_name'],
                        "middle_name" => $item['securityUser']['middle_name'],
                        "third_name" => $item['securityUser']['third_name'],
                        "last_name" => $item['securityUser']['last_name'],
                        "openemis_no" => $item['securityUser']['openemis_no'],
                        "address" => $item['securityUser']['address'],
                        "gender_id" => [
                            "key" => $item['securityUser']["gender"]["id"],
                            "value" => $item['securityUser']["gender"]["name"],
                        ],
                        "address_area_id" => [
                            "key" => $item['securityUser']["areaAdministrative"]["id"],
                            "value" => $item['securityUser']["areaAdministrative"]["name"],
                        ],
                        "birthplace_area_id" => [
                            "key" => $item['securityUser']["birthplacearea"]["id"],
                            "value" => $item['securityUser']["birthplacearea"]["name"],
                        ],
                        "date_of_birth" => $item['securityUser']['date_of_birth'],
                        "nationality_id" => [
                            "key" => $item['securityUser']["nationality"]["id"],
                            "value" => $item['securityUser']["nationality"]["name"],
                        ],
                        "identity_type_id" => [
                            "key" => $item['securityUser']["identityType"]["id"],
                            "value" => $item['securityUser']["identityType"]["name"],
                        ],
                        "identity_number" => $item['securityUser']['identity_number'],

                    ];
                }
            );
            return $detailsId;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch marker view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Marker Not Found');
        }
    }

    /**
     * get marker subject on the basis of marker
     * @param string $markerId
     * @return array|\Illuminate\Http\JsonResponse
     */
    protected function getMarkerSubject(string $openemisno)
    {
        try {
            $listsubject = [];
            $count = 0;
            $markerdid  = SecurityUser::where('openemis_no', $openemisno)->first()->id;
            $no  = ExaminationMarker::where('staff_id', $markerdid)->first()->id;
            $subejct = ExaminationMarkersSubject::with(
                'educationSubject:id,name,code'
            )->where('examination_markers_id', $no)->get()->map(
                function ($item, $key) use (&$listsubject, &$count) {
                    $listsubject[$count]['id'] = $item['educationSubject']['id'];
                    $listsubject[$count]['name'] = $item['educationSubject']['name'];
                    $listsubject[$count]['code'] = $item['educationSubject']['code'];
                    $count++;
                }
            );
            return  $listsubject;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Qualification specialisation from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Qualification specialisation Not Found');
        }
    }

    /**
     * @param int $examMarkerId
     * @param int $parentId
     * @param $check
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function deleteExaminationMarker(int $examMarkerId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->markerRepository->checkIfSafeToDelete($examMarkerId);
            }
            if ($canDelete) {
                $staffqulai = new DeleteMarkerQualification(0, $examMarkerId);
                $this->dispatchNow($staffqulai);
                return $this->markerRepository->deleteMarker($examMarkerId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete examination marker.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination marker not deleted.");
        }
    }
}
