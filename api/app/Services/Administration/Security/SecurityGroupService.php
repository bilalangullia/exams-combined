<?php

namespace App\Services\Administration\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\SecurityGroupRepository;
use App\Models\SecurityGroupUser;
use App\Models\SecurityGroup;
use App\Http\Requests\SecurityUserGroupAddRequest;
use JWTAuth;

class SecurityGroupService extends Controller
{
    protected $securityGroupRepository;

    public function __construct(
        SecurityGroupRepository $securityGroupRepository
    ) {
        $this->securityGroupRepository = $securityGroupRepository;
    }

    public function getGroupList(Request $request)
    {
        try{
            $response = $this->securityGroupRepository->securityUserGroupList($request);
            $list = $response['record']->map(
                function ($item, $key) {
                    $noOfUser = SecurityGroupUser::where('security_group_id', $item['id'])->get();
                    $count = $noOfUser->count();
                    return [
                        "user_group_id" =>  $item['id'],
                        "name" => $item['name'],
                        "noOfUser" => ($count) ? $count : 0,
                    ];
                }
            );

            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['candidateRecords'] = $list;
                $data['total'] = $response['totalRecord'];
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group List Not Found");
        }
    }

    public function autocompleteSecurityUser(string $openemisId)
    {
        try{
            $rec = [];
            $userData = $this->securityGroupRepository->autocompleteSecurityUser($openemisId);
            foreach ($userData as $key => $value) {
                 $rec[$key]['user_id'] = $value['id'];
                 $rec[$key]['openemis_id'] = $value['openemis_no'];
                 $rec[$key]['name'] = $value['full_name'];
                 $rec[$key]['role'] = isset($value['securityGroupUser'][$key]['securityRole']['name']) ? $value['securityGroupUser'][$key]['securityRole']['name'] : '';
             }

            return $rec;
        }  catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Details Not Found");
        }
    }

    public function autocompleteArea(string $areaName)
    {
        try{
            $areaData = $this->securityGroupRepository->autocompleteArea($areaName)->map(
                function($item, $key){
                    return[
                        "area_id" => $item['id'],
                        "level" => $item['areaLevel']['name'], 
                        "code" => $item['code'],
                        "area" =>  $item['name'],
                    ];
                }
            );
         return $areaData;
        }  catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Area Details Not Found");
        }
    }

    public function autocompleteInstitutions(string $institutionsName)
    {
        try{
            $institutionData = $this->securityGroupRepository->autocompleteInstitutions($institutionsName)->map(
                function($item, $key){
                    return[
                        "institution_id" => $item['id'],
                        "code" => $item['code'],
                        "institution" =>  $item['name'],
                    ];
                }
            );

            return $institutionData;
        }  catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Institution Details Not Found");
        }
    }

    public function getSecurityUserGroupDetails(string $groupId)
    {
        try{
            $user_group_details = $this->securityGroupRepository->getSecurityUserGroupDetails($groupId);
            $userDetails = [];
            $count = 0;
            foreach ($user_group_details as $key => $value) {
                $userDetails[$count]['user_group_id'] = $value['id'];
                $userDetails[$count]['user_group_name'] = $value['name'];
                foreach ($value['securityGroupArea'] as $k => $val) {
                    $userDetails[$count]['areas'][$k]['area_id'] =  $val['area']['id'];
                    $userDetails[$count]['areas'][$k]['area_level'] =  $val['area']['areaLevel']['name'];
                    $userDetails[$count]['areas'][$k]['area_code'] =  $val['area']['code'];
                    $userDetails[$count]['areas'][$k]['area_name'] =  $val['area']['name'];
                }
                foreach ($value['securityGroupInstitution'] as $ke => $values) {
                     $userDetails[$count]['institutions'][$ke]['institution_id'] =  $values['examinationCentre']['id'];
                    $userDetails[$count]['institutions'][$ke]['institution_code'] =  $values['examinationCentre']['code'];
                    $userDetails[$count]['institutions'][$ke]['institution_name'] =  $values['examinationCentre']['name'];
                }
                foreach ($value['securityGroupUser'] as $kee => $vals) {
                    $userDetails[$count]['users'][$kee]['user_id'] =  $vals['securityUser']['id'];
                    $userDetails[$count]['users'][$kee]['openemis_id'] =  $vals['securityUser']['openemis_no'];
                    $userDetails[$count]['users'][$kee]['user_name'] =  $vals['securityUser']['full_name'];
                    $userDetails[$count]['users'][$kee]['role'] =  $vals['securityRole']['name'];
                }
                $userDetails[$count]['modified_by'] = $value['modifiedUser']['full_name'];
                $userDetails[$count]['modified_on'] = $value['modified'];
                $userDetails[$count]['created_by'] = $value['createdByUser']['full_name'];
                $userDetails[$count]['created_on'] = $value['created'];
                $count++;
            }

            return  $userDetails;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

        }
    }

    public function getSystemGroupList(Request $request)
    {
        try{
            $response = $this->securityGroupRepository->getSecuritySystemGroupList($request);
            $list = $response['record']->map(
                function ($item, $key) {
                    $noOfUser = SecurityGroupUser::where('security_group_id', $item['id'])->get();
                    $count = $noOfUser->count();
                    return [
                        "system_group_id" =>  $item['id'],
                        "name" => $item['name'],
                        "noOfUser" => $count,
                    ];
                }
            );

            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['candidateRecords'] = $list;
                $data['total'] = $response['totalRecord'];
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

        }
    }

    public function getSecuritySystemGroupDetails(string $sysGroupId)
    {
        try{
            $system_group_details = $this->securityGroupRepository->getSystemGroupDetails($sysGroupId);
            $data = [];
            $count = 0;
            foreach ($system_group_details as $key => $value) {
                $data[$count]['system_group_id'] = $value['id'];
                $data[$count]['name'] = $value['name'];
                $data[$count]['modified_by'] = $value['modifiedUser']['full_name'];
                $data[$count]['modified_on'] = $value['modified'];
                $data[$count]['created_by'] = $value['createdByUser']['full_name'];
                $data[$count]['created_on'] = $value['created'];
                foreach ($value['securityGroupUser'] as $k => $val) {
                    $data[$count]['users'][$k]['openemis_no'] =  $val['securityUser']['openemis_no'];
                    $data[$count]['users'][$k]['user_name'] =  $val['securityUser']['full_name'];
                    $data[$count]['users'][$k]['role'] =  ($val['securityRole']['name']) ? $val['securityRole']['name'] : '';
                }
                $count++;
            }

            return  $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

        }
    }

     public function deleteUserGroup(int $userGroupId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->securityGroupRepository->checkIfSafeTodelete($userGroupId);
            }
            if ($canDelete) {
                    return $this->securityGroupRepository->deleteUserGroup($userGroupId);
            } 
            
            return $this->securityGroupRepository->cannotDeleteView($userGroupId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Group Can Not Delete.");
        }
    }

}
