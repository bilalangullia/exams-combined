<?php

namespace App\Services\Administration\Security;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\RoleRepository;

class RoleService extends Controller
{
    /**
     * RoleRepository instance
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * RoleService constructor.
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Getting User Role List
     * @param array $data
     * @param int $groupId
     * @return JsonResponse
     */
    public function userRolesList(array $data, int $groupId)
    {
        try {
            $data = $this->roleRepository->userRolesList($data, $groupId);
            $data['roles'] = $data['roles']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "visible" => $item['visible'],
                        "order" => $item['order'],
                    ];
                }
            );
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    /**
     * Getting User Role Details
     * @param int $roleId
     * @return JsonResponse
     */
    public function userRoleView(int $roleId)
    {
        try {
            $roleDetails = $this->roleRepository->userRoleView($roleId);
            $data = [];
            if ($roleDetails) {
                $data['id'] = $roleDetails['id'];
                $data['name'] = $roleDetails['name'];
                $data['visible'] = $roleDetails['visible'];
                $data['security_group'] = $roleDetails['securityGroup']['name'];
                $data['security_group_id'] = $roleDetails['securityGroup']['id'];
                $data['modified_on'] = isset($roleDetails['modified']) ? Carbon::parse($roleDetails['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($roleDetails['created']) ? Carbon::parse($roleDetails['created'])->format('F d,Y - h:i:s') : "";
                $data['created_by'] = "";
                if ($roleDetails['createdByUser']) {
                    $createdBy = $roleDetails['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($roleDetails['modifiedUser']) {
                    $modifiedBy = $roleDetails['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
            }

            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch role details from DB");
        }
    }

    /**
     * Getting System Role List
     * @param array $data
     * @return JsonResponse
     */
    public function systemRolesList(array $data)
    {
        try {
            $data = $this->roleRepository->systemRolesList($data);
            $data['roles'] = $data['roles']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "visible" => $item['visible'],
                        "order" => $item['order'],
                    ];
                }
            );
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    /**
     * Getting System Role Details
     * @param int $roleId
     * @return JsonResponse
     */
    public function systemRoleView(int $roleId)
    {
        try {
            $roleDetails = $this->roleRepository->systemRoleView($roleId);
            $data = [];
            if ($roleDetails) {
                $data['id'] = $roleDetails['id'];
                $data['name'] = $roleDetails['name'];
                $data['visible'] = $roleDetails['visible'];
                $data['modified_on'] = isset($roleDetails['modified']) ? Carbon::parse($roleDetails['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($roleDetails['created']) ? Carbon::parse($roleDetails['created'])->format('F d,Y - h:i:s') : "";
                $data['created_by'] = "";
                if ($roleDetails['createdByUser']) {
                    $createdBy = $roleDetails['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($roleDetails['modifiedUser']) {
                    $modifiedBy = $roleDetails['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch role details from DB");
        }
    }

    /**
     * Getting Role Permission List
     * @param array $data
     * @param int $roleId
     * @return JsonResponse
     */
    public function permissionList(array $request, int $roleId)
    {
        try {
            $function = $this->roleRepository->permissionList($request, $roleId);
            $data = [];
            if ($function) {
                foreach ($function['securityFunction'] as $key => $value) {
                    $data[$value['category']][] = [
                        'id' => $value['security_function_id'],
                        'name' => $value['name'],
                        'view' => $value['pivot']['_view'],
                        'edit' => $value['pivot']['_edit'],
                        'add' => $value['pivot']['_add'],
                        'delete' => $value['pivot']['_delete'],
                        'execute' => $value['pivot']['_execute'],
                    ];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role permission list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch role permission list from DB");
        }
    }
}
