<?php

namespace App\Services\Administration\Security;

use App\Http\Controllers\Controller;
use App\Models\SecurityGroupUser;
use App\Models\SecurityUser;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserService extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listingUser(Request $request)
    {
        try {
            $response = $this->userRepository->userList($request);
            $list = $response['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "openemis_no" => $item['openemis_no'],
                        "username" => $item['username'],
                        "name" => $item['full_name'],
                        "email" => $item['email'],
                        "identity_number" => $item['identity_number'],
                        "status" => ($item['status'] == '1') ? 'Active' : 'Inactive',
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $response['totalRecord'];
                $data['userRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User list not found");
        }
    }

    /**
     * @param string $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function userDetailView(string $openEmisno)
    {
        try {
            $userview = $this->userRepository->userDetails($openEmisno)->map(
                function ($item, $key) use ($openEmisno) {
                    return [
                        "id" => $item['id'],
                        "openemis_no" => $item['openemis_no'],
                        "username" => $item['username'],
                        "first_name" => $item['first_name'],
                        "middle_name" => $item['middle_name'],
                        "third_name" => $item['third_name'],
                        "last_name" => $item['last_name'],
                        "email" => $item['email'],
                        "nationality" => $item['nationality']['name'],
                        "identity_type" => $item['identityType']['name'],
                        "date_of_birth" => $item['date_of_birth'],
                        "identity_number" => $item['identity_number'],
                        "gender" => $item['gender']['name'],
                        "role" => $this->getRoleGroup($openEmisno),
                        "modified_by" => $item['modifiedUser']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['createdByUser']['full_name'],
                        "created_on" => $item['created'],
                    ];
                }
            );
            return $userview;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Details not found");
        }
    }

    /**
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function userDetailEdit(string $openEmisno)
    {
        try {
            $userview = $this->userRepository->userDetails($openEmisno)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "openemis_no" => $item['openemis_no'],
                        "username" => $item['username'],
                        "first_name" => $item['first_name'],
                        "middle_name" => $item['middle_name'],
                        "third_name" => $item['third_name'],
                        "last_name" => $item['last_name'],
                        "email" => $item['email'],
                        "gender" => [
                            "key" => $item['gender']['id'],
                            "value" => $item['gender']['name'],
                        ],
                        "status" => [
                            "key" => ($item['status'] == '1') ? '1' : '0',
                            "value" => ($item['status'] == '1') ? 'Active' : 'Inactive',
                        ],
                        "nationality" => $item['nationality']['name'],
                        "identity_type" => $item['identityType']['name'],
                        "date_of_birth" => $item['date_of_birth'],
                        "identity_number" => $item['identity_number'],
                    ];
                }
            );
            return $userview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Details not found");
        }
    }

    /**
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function accountView(string $openEmisno)
    {
        try {
            $userview = $this->userRepository->userDetails($openEmisno)->map(
                function ($item, $key) use ($openEmisno) {
                    return [
                        "username" => $item['username'],
                         "last_login" => $item['securityUserLogin']['login_date_time'],
                        "role" => $this->getRoleGroup($openEmisno),
                    ];
                }
            );
            return $userview;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch account view  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User account view not found");
        }
    }

    /**
     * @param string $openEmisno
     * @return array
     */
    public function getRoleGroup(string $openEmisno)
    {
        $userid = securityUser::where('openemis_no', $openEmisno)->first()->id;
        $roledata = SecurityGroupUser::select('security_user_id', 'security_group_id', 'security_role_id')
            ->where('security_user_id', $userid)->with(
                'securityRole:id,name',
                'securityGroup:id,name'
            )->groupBy('security_group_id', 'security_role_id')->get();
        $newrole = [];
        $count = 0;
        foreach ($roledata as $key => $value) {
            $newrole[$count]['security_group_id'] =  $value['securityGroup']['id'];
            $newrole[$count]['security_group_name'] =  $value['securityGroup']['name'];
            $newrole[$count]['security_role']['security_role_id'] =  $value['securityRole']['id'];
            $newrole[$count]['security_role']['security_role_name'] =  $value['securityRole']['name'];
            $count++;
        }
        return  $newrole;
    }
}
