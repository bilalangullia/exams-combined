<?php

namespace App\Services\Administration\SystemSetup\AcademicPeriod;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\AcademicPeriodRepository;

class AcademicPeriodService extends Controller
{
	protected $academicPeriodRepository;

    public function __construct(
        AcademicPeriodRepository $academicPeriodRepository
    )
    {
        $this->academicPeriodRepository = $academicPeriodRepository;
    }

    public function getAcademicPeriodList(Request $request)
    {
    	try{
    		$list = $this->academicPeriodRepository->getAcademicPeriodList($request);
    		$listData = $list['response']->map(
                function ($item, $key) {
                    return [
                        "id"         =>  $item['id'] ,
                        "visible"    =>  $item['visible'],
                        "current"	 =>  $item['current'],
                        "editable"   =>  $item['editable'],
                        "code"       =>  $item['code'],
                        "name"       =>  $item['name'],
                        "start_date" =>  $item['start_date'],
                        "end_date"   =>  $item['end_date'],
                        "academic_period_level" => $item['academicPeriodLevel']['name']
                    ];
                }
            );

            if ($listData->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['all_data'] = $list['all_data']; 
                $data['response'] = $listData;
                $data['total'] = $list['total'];
                return $data;
            }

    		return $list;
    	} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level List Not Found');
        }
    }

    public function getAcademicPeriodDetail(int $academicPeriodId)
    {
        try{
            $result = $this->academicPeriodRepository->getAcademicPeriodDetails($academicPeriodId)->map(
                function ($item, $key) {
                    if($item['visible'] == 1){
                         $visible = [
                            "key" => $item['visible'],
                            "value" => "Yes"
                        ];
                    } else {
                        $visible = [
                            "key" => $item['visible'],
                            "value" => "No"
                        ];
                    }
                    if($item['current'] == 1){
                        $current = [
                            "key" => $item['current'],
                            "value" => "Yes"
                        ];
                    } else {
                        $current = [
                            "key" => $item['current'],
                            "value" => "No"
                        ];
                    }
                    if($item['editable'] == 1){
                        $editable = [
                            "key" => $item['editable'],
                            "value" => "Yes"
                        ];
                    } else {
                         $editable = [
                            "key" => $item['editable'],
                            "value" => "No"
                        ];
                    }
                    return [
                        "id"         =>  $item['id'] ,
                        "visible"    =>  $visible,
                        "current"    =>  $current,
                        "editable"   =>  $editable,
                        "code"       =>  $item['code'],
                        "name"       =>  $item['name'],
                        "start_date" =>  $item['start_date'],
                        "end_date"   =>  $item['end_date'],
                        "academic_period_level" => $item['academicPeriodLevel']['name'],
                        "parent"     => $item['parent']['name'],
                        "modified_by" => $item['modifiedBy']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['createdBy']['full_name'],
                        "created_on" => $item['created']
                    ];
                }
            );

            return  $result;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Detail Not Found');
        }
    }

    public function deleteAcademicPeriod(int $academicPeriodId, $check)
    {
        try{
            $canDelete = true;
            if ($check) {
                $canDelete = $this->academicPeriodRepository->checkIfSafeToDelete($academicPeriodId);
            }
            if ($canDelete) {
                return $this->academicPeriodRepository->deleteAcademicPeriod($academicPeriodId);
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete academic level',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }
}
