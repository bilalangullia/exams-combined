<?php

namespace App\Services\Administration\SystemSetup\AcademicPeriod;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\AcademicPeriodLevelRepository;

class AcademicPeriodLevelService extends Controller
{
	protected $academicPeriodLevelRepository;

    public function __construct(
        AcademicPeriodLevelRepository $academicPeriodLevelRepository
    )
    {
        $this->academicPeriodLevelRepository = $academicPeriodLevelRepository;
    }

    public function getAcademicPeriodLevelList(Request $request)
    {
    	try{
    		$list = $this->academicPeriodLevelRepository->getAcademicPeriodLevelList($request);
    		$listData = $list['record']->map(
                function ($item, $key) {
                    return [
                        "id"    =>  $item['id'] ,
                        "level" =>  $item['level'],
                        "name"	=>  $item['name']	
                    ];
                }
            );

            if ($listData->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['response'] = $listData;
                $data['total'] = $list['totalRecord'];
                return $data;
            }

    		return $list;
    	} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level List Not Found');
        }
    }

    public function getAcademicPeriodLevelDetail(int $academicLevel)
    {
    	 try{
    	 	$detail = $this->academicPeriodLevelRepository->getAcademicPeriodLevelDetail($academicLevel)
    	 			->map( function ($item, $key) {
                    return [
                        "id"    =>  $item['id'] ,
                        "level" =>  $item['level'],
                        "name"	=>  $item['name'],
                        "modified_by" => $item['modifiedBy']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['createdBy']['full_name'],
                        "created_on" => $item['created']	
                    ];
                }
            );

    	 	return $detail;
    	 } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level Detail Not Found');
        }
    }

    public function deleteAcademicLevel(int $academicLevel, $check)
    {
        try{
            $canDelete = true;
            if ($check) {
                $canDelete = $this->academicPeriodLevelRepository->checkIfSafeToDelete($academicLevel);
            }
            if ($canDelete) {
                return $this->academicPeriodLevelRepository->deleteAcademicLevel($academicLevel);
            }

            return false;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete academic level',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return false;
        }
    }
}