<?php

namespace App\Services\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\EducationGradeRepository;

class EducationGradeService extends Controller
{
	/**
     * EducationGradeRepository instance
     * @var $examCentreRoomRepository
     */
    protected $educationGradeRepository;

    /**
     * EducationGradeService constructor.
     * @param EducationGradeRepository $educationGradeRepository
     */
    public function __construct(
        EducationGradeRepository $educationGradeRepository
    )
    {
        $this->educationGradeRepository = $educationGradeRepository;
    }

	/**
     * Getting Education Grades List
     * @param array $data
     * @param int $programId
     * @return JsonResponse
     */
	public function educationGrades(array $data, int $programId)
	{
		try {
			$gradeList = $this->educationGradeRepository->educationGrades($data, $programId);
			$gradeList['list'] = $gradeList['list']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "code" => $item['code'],
                        "visible" => $item['visible'],
                        "education_program" => $item['educationProgramme']['name'],
                        "education_stage" => $item['educationStage']['name'],
                        "subjects" => $item['education_grade_subject_count']
                    ];
                }
            );
			return $gradeList;
		} catch (\Exception $e) {
			Log::error(
                'Failed to get education grade list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade list");
		}
	}

    /**
     * Getting Education Grades Details
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeView(int $gradeId)
    {
        try {
            $gradeData = $this->educationGradeRepository->educationGradeView($gradeId);
            
            $data = [];
            if ($gradeData) {
                $data['id'] = $gradeData['id'];
                $data['name'] = $gradeData['name'];
                $data['code'] = $gradeData['code'];
                $data['visible'] = $gradeData['visible'];
                $data['admission_age'] = $gradeData['admission_age'];
                $data['education_stage'] = $gradeData['educationStage']['name'];
                $data['education_program'] = $gradeData['educationProgramme']['name'];
                $data['modified'] = isset($gradeData['modified']) ? Carbon::parse($gradeData['modified'])->format('F d,Y - h:i:s') : "";
                $data['created'] = isset($gradeData['created']) ? Carbon::parse($gradeData['created'])->format('F d,Y - h:i:s') : "";
                $data['created_by'] = "";
                if ($gradeData['createdByUser']) {
                    $createdBy = $gradeData['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($gradeData['modifiedUser']) {
                    $modifiedBy = $gradeData['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }

                $data['subjects'] = [];
                foreach ($gradeData['GradeSubjects'] as $key => $value) {
                    $data['subjects'][] = [
                        'hours_required' => $value['hours_required'],
                        'name' => $value['name'],
                        'code' => $value['code'],
                    ];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade list");
        }
    }

    /**
     * Getting Education Grade Subjects
     * @param array $data
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeSubjects(array $data, int $gradeId)
    {
        try {
            $subjects = $this->educationGradeRepository->educationGradeSubjects($data, $gradeId);
            $list = $subjects['list']['GradeSubjects'];
            $array = [];
            if ($list) {
                foreach ($list as $key => $value) {
                    $array[] = [
                        'name' => $value['name'],
                        'code' => $value['code'],
                        'hours_required' => $value['hours_required'],
                        'auto_allocation' => $value['auto_allocation'],
                        'auto_allocation' => $value['auto_allocation'],
                        'education_grade_id' => $value['education_grade_id'],
                        'education_subject_id' => $value['education_subject_id']
                    ];
                }
            }
            $subjects['list'] = $array;
            return $subjects;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects list");
        }
    }

    /**
     * Getting Education Grade Subject Details
     * @param int $gradeId
     * @param int $subjectId
     * @return JsonResponse
     */
    public function educationGradeSubjectView(int $gradeId, int $subjectId)
    {
        try {
            $gradeData = $this->educationGradeRepository->educationGradeSubjectView($gradeId, $subjectId);
            
            $data = [];
            if ($gradeData && count($gradeData['GradeSubjects']) > 0) {
                $data['code'] = $gradeData['GradeSubjects'][0]['code'];
                $data['education_subject'] = $gradeData['GradeSubjects'][0]['name'];
                $data['hours_required'] = $gradeData['GradeSubjects'][0]['hours_required'];
                $data['auto_allocation'] = $gradeData['GradeSubjects'][0]['auto_allocation'];
                $data['subjectId'] = $gradeData['GradeSubjects'][0]['education_subject_id'];
                $data['gradeId'] = $gradeData['id'];
                $data['education_grade'] = $gradeData['code'] . " - " . $gradeData['name'];
                $data['education_programme'] = $gradeData['educationProgramme']['code'] . " - " . $gradeData['educationProgramme']['name'];
                $data['education_level'] = $gradeData['educationProgramme']['educationCycle']['educationLevel']['name'];

                if ($gradeData['createdByUser']) {
                    $createdBy = $gradeData['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($gradeData['modifiedUser']) {
                    $modifiedBy = $gradeData['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }

                $modified = $gradeData['GradeSubjects'][0]['pivot']['modified'];
                $created = $gradeData['GradeSubjects'][0]['pivot']['created'];
                
                $data['modified'] = isset($modified) ? Carbon::parse($modified)->format('F d,Y - h:i:s') : "";
                $data['created'] = isset($created) ? Carbon::parse($created)->format('F d,Y - h:i:s') : "";

            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects details");
        }
    }

    /**
     * Getting Education Grade Details
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeSubjectAdd(int $gradeId)
    {
        try {
            $data = $this->educationGradeRepository->educationGradeSubjectAdd($gradeId);
            $gradeData = [];
            if ($data) {
                $gradeData['education_grade_id'] = $data['id'];
                $gradeData['education_grade'] = $data['code'] . " - " . $data['name'];
                $gradeData['education_programme'] = $data['educationProgramme']['code'] . " - " . $data['educationProgramme']['name'];
                $gradeData['education_level'] = $data['educationProgramme']['educationCycle']['educationLevel']['name'];
            }
            return $gradeData;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects details");
        }
    }

    /**
     * Getting Setup list.
     * @param array $data
     * @param int $setupId
     * @return JsonResponse
     */
    public function educationGradeSetUp(array $data, int $setupId)
    {
        try {
            $list = $this->educationGradeRepository->educationGradeSetUp($data, $setupId);
            return $list; 
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education setup list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education setup list");
        }
    }

    /**
     * Getting Setup subject details.
     * @param int $subjectId
     * @return JsonResponse
     */
    public function educationSetupSubjectView(int $subjectId)
    {
        try {
            $subjectData = $this->educationGradeRepository->educationSetupSubjectView($subjectId);
            $data = [];
            if ($subjectData) {
                $data['id'] = $subjectData['id'];
                $data['name'] = $subjectData['name'];
                $data['code'] = $subjectData['code'];
                $data['visible'] = $subjectData['visible'];
                $data['modified'] = isset($subjectData['modified']) ? Carbon::parse($subjectData['modified'])->format('F d,Y - h:i:s') : "";
                $data['created'] = isset($subjectData['created']) ? Carbon::parse($subjectData['created'])->format('F d,Y - h:i:s') : "";
                $data['created_by'] = "";
                if ($subjectData['createdByUser']) {
                    $createdBy = $subjectData['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($subjectData['modifiedUser']) {
                    $modifiedBy = $subjectData['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }

                $data['fieldOfStudies'] = [];
                foreach ($subjectData['fieldOfStudy'] as $key => $value) {
                    $data['fieldOfStudies'][] = [
                        'name' => $value['name'],
                        'id' => $value['education_field_of_study_id']
                    ];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get subject details");
        }
    }

    /**
     * Deleting Education grade
     * @param int $gradeId
     * @param int $parentId
     * @param int $check
     * @return JsonResponse
     */
    public function educationGradeDelete(int $gradeId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->educationGradeRepository->checkIfSafeToDelete($gradeId);
            }
            if ($canDelete) {
                return $this->educationGradeRepository->deleteEducationGrade($gradeId, $parentId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete education grade',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete education grade");
        }
    }

    /**
     * Deleting Education grade subject
     * @param int $gradeId
     * @param int $subjectId
     * @param int $check
     * @return JsonResponse
     */
    public function educationGradeSubjectDelete(int $gradeId, int $subjectId, $check)
    {
        try {
            return $this->educationGradeRepository->educationGradeSubjectDelete($gradeId, $subjectId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete education grade',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete education grade");
        }
    }
}
