<?php

namespace App\Services\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use App\Models\EducationProgramme;
use App\Models\EducationProgrammesNextProgramme;
use App\Repositories\EducationStructureProgrammesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EducationStructureProgrammesService extends Controller
{
    protected $educationStructureProgrammesRepository;

    public function __construct(EducationStructureProgrammesRepository $educationStructureProgrammesRepository)
    {
        $this->educationStructureProgrammesRepository = $educationStructureProgrammesRepository;
    }

    /**
     * @param Request $request
     * @param string $educationlevelId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProgrammesListing(Request $request, string $educationCycleId)
    {
        try {
            $response = $this->educationStructureProgrammesRepository->programmesList($request, $educationCycleId);
            $list = $response['record']->map(
                function ($item, $key) use ($educationCycleId) {
                    $noOfUser = EducationProgramme::where('education_cycle_id', $educationCycleId)
                        ->get();
                    $count = $noOfUser->count();
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "duration" => $item['duration'],
                        "visible" => $item['visible'],
                        "education_field_of_study" => $item['educationFieldOfStudie']['name'],
                        "education_cycle_id" => $item['educationCycle']['name'],
                        "education_certification" => $item['educationCertification']['name'],
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $response['totalRecord'];
                $data['programmeRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Programme List Not Found");
        }
    }

    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProgrammeEdit(string $programmeId)
    {
        try {
            $view = $this->educationStructureProgrammesRepository->programmesView($programmeId)->map(
                function ($item, $key) use ($programmeId) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "duration" => $item['duration'],
                        "visible" => [
                            "key" => ($item['visible'] == '1') ? '1' : '0',
                            "value" => ($item['visible'] == '1') ? 'Yes' : 'No',
                        ],
                        "education_field_of_study" => [
                            "key" => $item['educationFieldOfStudie']['id'],
                            "value" => $item['educationFieldOfStudie']['name'],
                        ],
                        "education_cycle_id" => [
                            "key" => $item['educationCycle']['id'],
                            "value" => $item['educationCycle']['name'],
                        ],
                        "education_certification" => [
                            "key" => $item['educationCertification']['id'],
                            "value" => $item['educationCertification']['name'],
                        ],
                        "next_programme" => $this->getNextProgramme($programmeId),
                    ];
                }
            );
            return $view;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle view Not Found");
        }
    }

    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProgrammeView(string $programmeId)
    {
        try {
            $view = $this->educationStructureProgrammesRepository->programmesView($programmeId)->map(
                function ($item, $key) use ($programmeId) {
                    return [
                        "id" => $item['id'],
                        "code" => $item['code'],
                        "name" => $item['name'],
                        "duration" => $item['duration'],
                        "visible" => ($item['visible'] == '1') ? 'Yes' : 'No',
                        "education_field_of_study" => $item['educationFieldOfStudie']['name'],
                        "education_cycle_id" => $item['educationCycle']['name'],
                        "education_certification" => $item['educationCertification']['name'],
                        "next_programme" => $this->getNextProgramme($programmeId),
                        "modified_by"    =>  $item['modifiedBy']['full_name'],
                        "modified_on"    =>  $item['modified'],
                        "created_by"     =>  $item['securityUser']['full_name'],
                        "created_on"     =>  $item['created']
                    ];
                }
            );
            return $view;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle view Not Found");
        }
    }

    /**
     * @param $programmeId
     * @return mixed
     */
    protected function getNextProgramme(string $programmeId)
    {
        $next = EducationProgrammesNextProgramme::select('next_programme_id')
            ->where('education_programme_id', $programmeId)->get();
        $nextProg = EducationProgramme::select('id', 'name', 'education_cycle_id')->whereIn('id', $next)
            ->with('educationCycle:id,name')->get()->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "cycle_programme" => trim($item['educationCycle']['name'] . '-' . $item['name']),
                    ];
                }
            );
        return $nextProg;
    }
}
