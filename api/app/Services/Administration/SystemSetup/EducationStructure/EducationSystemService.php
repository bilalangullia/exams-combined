<?php

namespace App\Services\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\EducationSystemRepository;

class EducationSystemService extends Controller
{
	protected $educationSystemRepository;

    public function __construct(
    	EducationSystemRepository $educationSystemRepository
    ) {
    	$this->educationSystemRepository = $educationSystemRepository;
    }

    public function getSystemList(Request $request)
    {
    	try{
    		$list = $this->educationSystemRepository->getEducationList($request);
    		$listData = $list['record']->map(
                function ($item, $key) {
                    return [
                        "id"  =>  $item['id'],
                        "visible"   =>  $item['visible'],
                        "name" =>  $item['name']
                    ];
                }
            );

            if ($listData->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['systemData'] = $listData;
                $data['total'] = $list['totalRecord'];
                return $data;
            }
    	} catch (\Exception $e) {
			Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection List Not Found');
        }
    }

    public function getSystemDetail(int $eduSysId)
    {
        try{
            $view = $this->educationSystemRepository->getSystemDetail($eduSysId)->map(
                function ($item, $key) {
                    if($item['visible'] == 1){
                        $visible = [
                            "key" => $item['visible'],
                            "value" => "Yes"
                        ];
                    } else {
                        $visible = [
                        "key" => $item['visible'],
                        "value" => "No"
                    ];
                    }
                    return [
                        "id"  =>  $item['id'],
                        "name" =>  $item['name'],
                        "visible"   =>  $visible,
                        "modified_by" => $item['modifiedUser']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['createdByUser']['full_name'],
                        "created_on" => $item['created']                        
                    ];
                }
            );

            return $view;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Education System Detail Not Found');
        }
    }

    public function deleteEducationSystem(int $eduSysId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->educationSystemRepository->checkIfSafeToDelete($eduSysId);
            }
            if ($canDelete) {
                /*$componentDeleteJob = new DeleteComponent(0, $optionId);
                $this->dispatchNow($componentDeleteJob);*/

                return $this->educationSystemRepository->deleteSystem($eduSysId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Education System  Can Not Deleted');
        }
    }
}