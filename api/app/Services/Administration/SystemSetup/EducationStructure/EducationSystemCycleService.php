<?php

namespace App\Services\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use App\Models\EducationCycle;
use App\Repositories\EducationSystemCycleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EducationSystemCycleService extends Controller
{
    protected $educationSystemCycleRepository;

    public function __construct(EducationSystemCycleRepository $educationSystemCycleRepository)
    {
        $this->educationSystemCycleRepository = $educationSystemCycleRepository;
    }

    /**
     * @param Request $request
     * @param string $educationlevelId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCycleListing(Request $request, string $educationlevelId)
    {
        try {
            $response = $this->educationSystemCycleRepository->cycleList($request, $educationlevelId);
            $list = $response['record']->map(
                function ($item, $key) use ($educationlevelId) {
                    $noOfUser = EducationCycle::where('education_level_id', $educationlevelId)
                        ->get();
                    $count = $noOfUser->count();
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "visible" => $item['visible'],
                        "admission_age" => $item['admission_age'],
                        "education_level" => $item['educationLevel']['name'],
                    ];
                }
            );
            if ($list->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['total'] = $response['totalRecord'];
                $data['cycleRecords'] = $list;
                return $data;
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle List Not Found");
        }
    }

    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCycleView(string $eduCycleId)
    {
        try {
            $view = $this->educationSystemCycleRepository->cyclesView($eduCycleId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "visible" => ($item['visible'] == '1') ? 'Yes' : 'No',
                        "admission_age" => $item['admission_age'],
                        "education_level" => $item['educationLevel']['name'],
                        "modified_by"    =>  $item['modifiedBy']['full_name'],
                        "modified_on"    =>  $item['modified'],
                        "created_by"     =>  $item['securityUser']['full_name'],
                        "created_on"     =>  $item['created']
                    ];
                }
            );
            return $view;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle view Not Found");
        }
    }

    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCycleEdit(string $eduCycleId)
    {
        try {
            $data = $this->educationSystemCycleRepository->cyclesView($eduCycleId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "visible" => [
                            "key" => ($item['visible'] == '1') ? '1' : '0',
                            "value" => ($item['visible'] == '1') ? 'Yes' : 'No',
                        ],
                        "admission_age" => $item['admission_age'],
                        "education_level" => [
                            "key" => $item['educationLevel']['id'],
                            "value" => $item['educationLevel']['name'],
                        ],
                        "modified_by"    =>  $item['modifiedBy']['full_name'],
                        "modified_on"    =>  $item['modified'],
                        "created_by"     =>  $item['securityUser']['full_name'],
                        "created_on"     =>  $item['created']
                    ];
                }
            );
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle view Not Found");
        }
    }
}
