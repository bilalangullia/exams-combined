<?php

namespace App\Services\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\EducationLevelRepository;

class EducationLevelService extends Controller
{
	protected $educationLevelRepository;
 	
 	public function __construct(
    	EducationLevelRepository $educationLevelRepository
    ) {
    	$this->educationLevelRepository = $educationLevelRepository;
    }

    public function getEducationLevelList(Request $request)
    {
    	try{
    		$list = $this->educationLevelRepository->getEducationLevelList($request);
    		$listData = $list['record']->map(
                function ($item, $key) {
                    return [
                        "id"        =>  $item['id'],
                        "visible"   =>  $item['visible'],
                        "name"      =>  $item['name'],
                        "education_system" =>  $item['educationSystem']['name'],
                        "education_level_isced" => $item['educationLevelIsced']['name']
                    ];
                }
            );

            if ($listData->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['educationLevelData'] = $listData;
                $data['total'] = $list['totalRecord'];
                return $data;
            }
    	} catch (\Exception $e) {
			Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Education Level List Not Found');
        }
    }

    public function getEducationLevelDetails(int $eduLvlId)
    {
    	try{
    		$view  = $this->educationLevelRepository->getEducationLevelDetails($eduLvlId)->map(
                function ($item, $key) {
                	if($item['visible'] == 1){
                		$visible = [
                			"key" => $item['visible'],
                			"value" => "Yes"
                		];
                 	} else{
                 		$visible = [
                			"key" => $item['visible'],
                			"value" => "No"
                		];
                	}
                    return [
                        "id"        =>  $item['id'],
                        "visible"   =>  $visible,
                        "name"      =>  $item['name'],
                        "education_system" =>  [
                            "key" => $item['educationSystem']['id'],
                            "value" => $item['educationSystem']['name']
                        ],
                        "education_level_isced" => [
                            "key" => $item['educationLevelIsced']['id'],
                            "value" => $item['educationLevelIsced']['name']
                        ],
                        "modified_by" => $item['modifiedUser']['full_name'],
                        "modified_on" => $item['modified'],
                        "created_by" => $item['createdByUser']['full_name'],
                        "created_on"  => $item['created']
                    ];
                }
            );;

    		return $view;
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level Detail Not Found');
		}
    }

    public function deleteEducationLevel(int $eduLevelId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->educationLevelRepository->checkIfSafeToDelete($eduLevelId);
            }
            if ($canDelete) {
                /*$componentDeleteJob = new DeleteComponent(0, $optionId);
                $this->dispatchNow($componentDeleteJob);*/

                return $this->educationLevelRepository->deleteLevel($eduLevelId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Education Level  Can Not Deleted');
        }
    }
}