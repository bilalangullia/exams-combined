<?php

namespace App\Services\Administration\SystemSetup\AdministrativeBoundaries;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Repositories\AdministrativeBoundaryRepository;

class AdministrativeBoundaryService extends Controller
{
	/**
     * AdministrativeBoundaryRepository instance
     * @var $administrativeBoundaryRepository
     */
    protected $administrativeBoundaryRepository;

    /**
     * AdministrativeBoundaryService constructor.
     * @param AdministrativeBoundaryRepository $administrativeBoundaryRepository
     */
    public function __construct(
        AdministrativeBoundaryRepository $administrativeBoundaryRepository
    )
    {
        $this->administrativeBoundaryRepository = $administrativeBoundaryRepository;
    }

    /**
     * Getting Area level (education) List
     * @param array $data
     * @return JsonResponse
     */
    public function areaLevelEducation(array $data)
    {
        try {
            $list = $this->administrativeBoundaryRepository->areaLevelEducation($data);
            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level education list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level education list");
        }
    }

    /**
     * Getting Area level (education) Details
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelEducationView(int $levelId)
    {
        try {
            $areaLevel = $this->administrativeBoundaryRepository->areaLevelEducationView($levelId);
            $data = [];
            if ($areaLevel) {
                $data['id'] = $areaLevel['id'];
                $data['name'] = $areaLevel['name'];
                $data['level'] = $areaLevel['level'];
                $data['modified_on'] = isset($areaLevel['modified']) ? Carbon::parse($areaLevel['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($areaLevel['created']) ? Carbon::parse($areaLevel['created'])->format('F d,Y - h:i:s') : "";

                if ($areaLevel['createdByUser']) {
                    $createdBy = $areaLevel['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($areaLevel['modifiedUser']) {
                    $modifiedBy = $areaLevel['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level education details");
        }
    }

    /**
     * Getting Area (education) List
     * @param array $data
     * @return JsonResponse
     */
    public function areaEducationList(array $data)
    {
        try {
            $result = $this->administrativeBoundaryRepository->areaEducationList($data);
            $result['list'] = $result['list']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "code" => $item['code'],
                        "visible" => $item['visible'],
                        "area_level" => $item['areaLevel']['name']
                    ];
                }
            );
            return $result;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area education list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area education list");
        }
    }

    /**
     * Getting Area (education) Details
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaEducationView(int $areaId)
    {
        try {
            $area = $this->administrativeBoundaryRepository->areaEducationView($areaId);
            $data = [];
            if ($area) {
                $data['id'] = $area['id'];
                $data['name'] = $area['name'];
                $data['code'] = $area['code'];
                if ($area['visible'] == 1) {
                    $data['visible'] = ["key" => $area['visible'], "value" => "Yes"];
                } else {
                    $data['visible'] = ["key" => $area['visible'], "value" => "No"];
                }
                
                $data['area_level'] = [
                    "key" => $area['areaLevel']['id'],
                    "value" => $area['areaLevel']['name']
                ];
                $data['parent'] = $area['parentArea']['name'];
                $data['parent_id'] = $area['parentArea']['id'];
                
                $data['modified_on'] = isset($area['modified']) ? Carbon::parse($area['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($area['created']) ? Carbon::parse($area['created'])->format('F d,Y - h:i:s') : "";

                if ($area['createdByUser']) {
                    $createdBy = $area['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($area['modifiedUser']) {
                    $modifiedBy = $area['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area education details");
        }
    }

    /**
     * Getting Area Level Dropdown via parent_id
     * @param int $parentId
     * @return JsonResponse
     */
    public function areaLevelDropdown(int $parentId)
    {
        try {
            $list = $this->administrativeBoundaryRepository->areaLevelDropdown($parentId);
            return $list; 
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level dropdown");
        }
    }

    /**
     * Getting Area level (Administrative) Details
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelAdministrativeView(int $levelId)
    {
        try {
            $areaLevel = $this->administrativeBoundaryRepository->areaLevelAdministrativeView($levelId);
            $data = [];
            if ($areaLevel) {
                $data['id'] = $areaLevel['id'];
                $data['name'] = $areaLevel['name'];
                $data['level'] = $areaLevel['level'];
                $data['modified_on'] = isset($areaLevel['modified']) ? Carbon::parse($areaLevel['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($areaLevel['created']) ? Carbon::parse($areaLevel['created'])->format('F d,Y - h:i:s') : "";

                if ($areaLevel['createdByUser']) {
                    $createdBy = $areaLevel['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($areaLevel['modifiedUser']) {
                    $modifiedBy = $areaLevel['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level details");
        }
    }

    /**
     * Getting Area level (Administrative) List
     * @param array $data
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaLevelAdministrativeList(array $data, int $areaId)
    {
        try {
            $result = $this->administrativeBoundaryRepository->areaLevelAdministrativeList($data, $areaId);
            $result['list'] = $result['list']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "level" => $item['level']
                    ];
                }
            );
            return $result;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level list");
        }
    }

    /**
     * Getting Area (Administrative) List
     * @param Request $request
     * @return JsonResponse
     */
    public function areaAdministrativeList(array $data)
    {
        try {
            $result = $this->administrativeBoundaryRepository->areaAdministrativeList($data);
            
            $result['list'] = $result['list']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "name" => $item['name'],
                        "code" => $item['code'],
                        "visible" => $item['visible'],
                        "is_main_country" => $item['is_main_country'],
                        "area_administrative_level" => $item['areaAdministrativeLevel']['name'],
                    ];
                }
            );
            return $result;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative list");
        }
    }

    /**
     * Getting Area (Administrative) Details
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaAdministrativeView(int $areaId)
    {
        try {
            $area = $this->administrativeBoundaryRepository->areaAdministrativeView($areaId);
            $data = [];
            if ($area) {
                $data['id'] = $area['id'];
                $data['name'] = $area['name'];
                $data['code'] = $area['code'];
                $data['is_main_country'] = $area['is_main_country'];
                if ($area['is_main_country'] == 1) {
                    $data['is_main_country'] = ["key" => $area['is_main_country'], "value" => "Yes"];
                } else {
                    $data['is_main_country'] = ["key" => $area['is_main_country'], "value" => "No"];
                }

                if ($area['visible'] == 1) {
                    $data['visible'] = ["key" => $area['visible'], "value" => "Yes"];
                } else {
                    $data['visible'] = ["key" => $area['visible'], "value" => "No"];
                }
                
                $data['area_level'] = [
                    "key" => $area['areaAdministrativeLevel']['id'],
                    "value" => $area['areaAdministrativeLevel']['name']
                ];

                $data['parent'] = [
                    "key" => $area['parentArea']['id'],
                    "value" => $area['parentArea']['name']
                ];
                
                $data['modified_on'] = isset($area['modified']) ? Carbon::parse($area['modified'])->format('F d,Y - h:i:s') : "";
                $data['created_on'] = isset($area['created']) ? Carbon::parse($area['created'])->format('F d,Y - h:i:s') : "";

                if ($area['createdByUser']) {
                    $createdBy = $area['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($area['modifiedUser']) {
                    $modifiedBy = $area['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative details");
        }
    }

    /**
     * Getting Area Level (Administrative) Dropdown
     * @param int $parentId
     * @return JsonResponse
     */
    public function areaAdministrativeLevelDropdown(int $parentId)
    {
        try {
            $areaLevel = $this->administrativeBoundaryRepository->areaAdministrativeLevelDropdown($parentId);
            return $areaLevel;
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative level dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative level dropdown");
        }
    }

    /**
     * Deleteing Area Level (Education)
     * @param int $levelId
     * @param int $parentId
     * @param bool $check
     * @return JsonResponse
     */
    public function areaLevelEducationDelete(int $levelId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->administrativeBoundaryRepository->checkIfSafeToDelete($levelId);
            }
            if ($canDelete) {
                return $this->administrativeBoundaryRepository->areaLevelEducationDelete($levelId, $parentId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area level (education)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area level (education)");
        }
    }

    /**
     * Deleteing Area (Education)
     * @param int $areaId
     * @param int $parentId
     * @param bool $check
     * @return JsonResponse
     */
    public function areaEducationDelete(int $areaId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->administrativeBoundaryRepository->checkIfSafeToDeleteArea($areaId);
            }
            
            if ($canDelete) {
                return $this->administrativeBoundaryRepository->areaEducationDelete($areaId, $parentId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area (education)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area (education)");
        }
    }

    /**
     * Deleteing Area Level (Administrative)
     * @param int $levelId
     * @param int $parentId
     * @param bool $check
     * @return JsonResponse
     */
    public function areaLevelAdministrativeDelete(int $levelId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->administrativeBoundaryRepository->checkIfSafeToDeleteAreaLevel($levelId);
            }

            if ($canDelete) {
                return $this->administrativeBoundaryRepository->areaLevelAdministrativeDelete($levelId, $parentId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area level (administrative)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area level (administrative)");
        }
    }

    /**
     * Deleteing Area (Administrative)
     * @param int $areaId
     * @param int $parentId
     * @param bool $check
     * @return JsonResponse
     */
    public function areaAdministrativeDelete(int $areaId, int $parentId, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->administrativeBoundaryRepository->checkIfSafeToDeleteAreaAdministrative($areaId);
            }
            if ($canDelete) {
                return $this->administrativeBoundaryRepository->areaAdministrativeDelete($areaId, $parentId);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area (administrative)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area (administrative)");
        }
    }
}
