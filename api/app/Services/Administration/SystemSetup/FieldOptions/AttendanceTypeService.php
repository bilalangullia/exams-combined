<?php

namespace App\Services\Administration\SystemSetup\FieldOptions;

use App\Http\Controllers\Controller;
use App\Repositories\AttendanceTypeRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class AttendanceTypeService extends Controller
{
    protected $attendanceTypeRepository;

    public function __construct(
        AttendanceTypeRepository $attendanceTypeRepository
    ) {
        $this->attendanceTypeRepository = $attendanceTypeRepository;
    }

    /**
     * Getting Field Option List
     * @param array $data
     * @return JsonResponse
     */
    public function getListing(array $data)
    {
        try {
            $list = $this->attendanceTypeRepository->getListing($data);
            $list['record'] = $list['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "visible" => $item['visible'],
                        "default" => $item['default'],
                        "editable" => $item['editable'],
                        "name" => $item['name'],
                        "international_code" => $item['international_code'],
                        "national_code" => $item['national_code'],
                        "qualification_level" => $item['qualificationLevel']['name'],
                        "education_field_of_studie" => $item['educationFieldOfStudie']['name']
                    ];
                }
            );
            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('List Not Found');
        }
    }

    /**
     * Getting Field Option Detail
     * @param array $data
     * @param int $optionId
     * @return JsonResponse
     */
    public function getFieldOptionDetail(array $data, int $optionId)
    {
        try {
            $detail = $this->attendanceTypeRepository->getFieldOptionDetail($data, $optionId);

            $data = [];
            if ($detail) {
                $data['id'] = $detail['id'];
                $data['name'] = $detail['name'];
                $data['international_code'] = $detail['international_code'];
                $data['national_code'] = $detail['national_code'];
                $data['qualification_level'] = $detail['qualificationLevel']['name'];
                $data['education_field'] = $detail['educationFieldOfStudie']['name'];
                if ($detail['visible'] == 1) {
                    $data['visible'] = ["key" => $detail['visible'], "value" => "Yes"];
                } else {
                    $data['visible'] = ["key" => $detail['visible'], "value" => "No"];
                }

                if ($detail['default'] == 1) {
                    $data['default'] = ["key" => $detail['default'], "value" => "Yes"];
                } else {
                    $data['default'] = ["key" => $detail['default'], "value" => "No"];
                }

                $data['modified_on'] = isset($detail['modified']) ? Carbon::parse($detail['modified'])->format(
                    'F d,Y - h:i:s'
                ) : "";
                $data['created_on'] = isset($detail['created']) ? Carbon::parse($detail['created'])->format(
                    'F d,Y - h:i:s'
                ) : "";

                if ($detail['createdByUser']) {
                    $createdBy = $detail['createdByUser'];
                    $data['created_by'] = $createdBy['first_name'] . " " . $createdBy['middle_name'] . " " . $createdBy['third_name'] . " " . $createdBy['last_name'];
                }

                $data['modified_by'] = "";
                if ($detail['modifiedUser']) {
                    $modifiedBy = $detail['modifiedUser'];
                    $data['modified_by'] = $modifiedBy['first_name'] . " " . $modifiedBy['middle_name'] . " " . $modifiedBy['third_name'] . " " . $modifiedBy['last_name'];
                }
            }
            return $data;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to fetch details from DB');
        }
    }

    /**
     * @param array $data
     * @param $check
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function deleteFieldOption(array $data, $check)
    {
        try {
            $canDelete = true;
            if ($check) {
                $canDelete = $this->attendanceTypeRepository->checkIfSafeToDelete($data);
            }
            if ($canDelete) {
                return $this->attendanceTypeRepository->deleteFieldOption($data);
            }
            return false;
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Data not deleted.");
        }
    }
}
