<?php

namespace App\Services\Administration\SystemSetup\SystemConfiguration;

use App\Http\Controllers\Controller;
use App\Models\EducationCycle;
use App\Repositories\SystemConfigurationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SystemConfigurationService extends Controller
{
    protected $systemConfigurationRepository;

    public function __construct(SystemConfigurationRepository $systemConfigurationRepository)
    {
        $this->systemConfigurationRepository = $systemConfigurationRepository;
    }

    /**
     * @param Request $request
     * @param int $systemId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getConfigurationList(Request $request, int $systemconfigId)
    {
        try {
            $list = $this->systemConfigurationRepository->systemConfigurationList($request, $systemconfigId);
            $listData = $list['record']->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "label" => $item['label'],
                        "type" => $item['type'],
                        "value" => ($item['value'] == '1') ? 'Enable' : 'Disable',
                        "visible" => $item['visible'],
                    ];
                }
            );

            if ($listData->count()) {
                $data['start'] = $request->start;
                $data['end'] = $request->end;
                $data['response'] = $listData;
                $data['total'] = $list['totalRecord'];
                return $data;
            }

            return $list;
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("System Configuration list not found");
        }
    }

    /**
     * @param int $systemconfigId
     * @return \Illuminate\Http\JsonResponse
     */

    public function getConfigurationView(int $systemconfigId)
    {
        try {
            $configdata = $this->systemConfigurationRepository->systemConfigurationView($systemconfigId)->map(
                function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "type" => $item['type'],
                        "label" => $item['label'],
                        "code" => $item['code'],
                        "value" => ($item['value'] == '1') ? 'Enable' : 'Disable',
                        "default_value" => $item['default_value'],
                        "modified_by"    =>  $item['modifiedBy']['full_name'],
                        "modified_on"    =>  $item['modified'],
                        "created_by"     =>  $item['securityUser']['full_name'],
                        "created_on"     =>  $item['created']

                    ];
                }
            );
            return $configdata;
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to get education grade subjects details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get System Configuration details");
        }
    }
}
