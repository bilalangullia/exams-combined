<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Areas\AreaRepository;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use App\Repositories\ExaminationStudents\ExaminationStudentRepository;
use Carbon\Carbon;
use App\Repositories\Report\ReportRepository;
use App\Services\Report\ReportService;
use DB;
use Illuminate\Support\Facades\Log;
use Storage;
use PDF;

class ReportRegistrationCandidateVerification extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:registration_candidate_verification {id*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'execute command for the process of the registration report';
    
    protected $reportService;
    protected $reportRepository;
    protected $areaRepository;
    protected $examinationRepository;
    protected $examinationCentreRepository;
    protected $examinationStudentRepository;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ReportService $reportService,
        ReportRepository $reportRepository,
        AreaRepository $areaRepository,
        ExaminationRepository $examinationRepository,
        ExaminationCentreRepository $examinationCentreRepository,
        ExaminationStudentRepository $examinationStudentRepository

    )
    {
        $this->reportService = $reportService;
        $this->reportRepository = $reportRepository;
        $this->areaRepository = $areaRepository;
        $this->examinationRepository = $examinationRepository;
        $this->examinationCentreRepository = $examinationCentreRepository;
        $this->examinationStudentRepository = $examinationStudentRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $storage_path = 'public/reports';
        $arguments = $this->argument('id');
        
        $reportProgress = DB::table('report_progress')
                        ->where('id', '=', $arguments[0])
                        ->where('expiry_date', '>=', Carbon::now()->toDateTimeString())
                        ->where('status', '=', config('constants.reportUpdateStatus.processing'))
                        ->first();
        
        if(!empty($reportProgress)){
            $currentRecords = $reportProgress->current_records;
            $totalRecords = $reportProgress->total_records;
            $params = json_decode($reportProgress->params, true);
            $templateName = $reportProgress->name;
            $reportId = $reportProgress->id;


            $areaNameCode = $this->areaRepository->getAreaNameCodeByAreaId($params['area_id']);

            $examName = $this->examinationRepository->getExamName($params['examination_id']);

            $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
                $params['examination_centre_id']
            );

            $skip = 0;
            $take = 10;
            $pages = ceil($totalRecords/$take);        

            for($page_number = 1; $page_number <= $pages; $page_number++){
                $skip = ($page_number -1) * $take;

                $candidates = $this->examinationStudentRepository->getCandidatesByExamIdAndExamCenterId(
                    $params['examination_id'],
                    $params['examination_centre_id'],
                    $params['candidate'],
                    $skip,
                    $take
                );
                
                Log::info('Anand SKIP', ['skip' => $skip, 'Page No'=>$page_number]);
                $reportData = $this->reportService->formatReportData(
                    $areaNameCode,
                    $examName,
                    $examCenterNameCode,
                    $candidates
                );
                $this->reportService->saveUpdateReportCommand($reportData, $templateName, $reportId, count($candidates));
               
            }

            $path = Storage::disk('local')->path($storage_path . '/' . $templateName);
            $fileName = $storage_path . '/'.$templateName.'.html';
            PDF::loadHTML(Storage::get($fileName))->save($path);
            $this->reportRepository->updateReportStatus($reportId, $status = config('constants.reportUpdateStatus.completed'), $error_message = null);
            
            Log::info('Pdf Report generated and saved in database and directory', ['method' => __METHOD__, 'data' => []]);
            
            Storage::delete($fileName);  
            
            Log::info('Shell script completed');            
        }
    }

}
