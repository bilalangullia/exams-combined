<?php

namespace App\Http\Controllers\Reports\Results;

use App\Http\Controllers\Controller;
use App\Http\Requests\StatementOfResultsRequest;
use Illuminate\Http\Request;
use App\Services\Report\Results\ResultService;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use Illuminate\Support\Facades\Log;

class ResultController extends Controller
{
    protected $resultService;
    protected $examinationRepository;
    protected $examinationCentreRepository;

    public function __construct(
        ResultService $resultService,
        ExaminationRepository $examinationRepository,
        ExaminationCentreRepository $examinationCentreRepository
    ) {
        $this->resultService = $resultService;
        $this->examinationRepository = $examinationRepository;
        $this->examinationCentreRepository = $examinationCentreRepository;
    }

    public function generateReport(StatementOfResultsRequest $request)
    {
        try {
            $reportData = $request->all();
            $examName = $this->examinationRepository->getExamName($reportData['examination_id']);
            $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
                $reportData['examination_centre_id']
            );

            $status = $this->resultService->saveUpdateReport($reportData);

            return $this->sendSuccessResponse("Result Report Generated Successfully", true);
        } catch (\Exception $e) {
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Result Report Not Generated');
        }

    }
}
