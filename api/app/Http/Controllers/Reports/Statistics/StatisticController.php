<?php

namespace App\Http\Controllers\Reports\Statistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CandidatePerSubjectStatisticsRequest;
use App\Http\Requests\CandidatePerCentreStatisticsRequest;
use App\Services\Report\Statistics\StatisticService;
use Illuminate\Support\Facades\Log;

class StatisticController extends Controller
{
    /**
     * StatisticService instance.
     * @var $statisticsService
     */
    protected $statisticService;

    /**
     * StatisticController Constructor.
     * @param StatisticService $statisticService
     */
    public function __construct(
        StatisticService $statisticService
    )
    {
        $this->statisticService = $statisticService;
    }

    /**
     * Generating Candidate Per Subject Report.
     * @param CandidatePerSubjectStatisticsRequest $request
     * @return JsonResponse
     */
    public function candidatePerSubjectReport(CandidatePerSubjectStatisticsRequest $request)
    {
    	try {
    		$data = $request->all();
    		$generate = $this->statisticService->candidatePerSubjectReport($data);
            if ($generate == 1) {
                return $this->sendSuccessResponse("Candidate Per Subject Report Generated Successfully");
            } elseif ($generate == 2) {
                return $this->sendErrorResponse("Expiry time limit exceeded");
            } else {
                return $this->sendErrorResponse("Candidate Per Subject Report Not Generated");
            }
    	} catch (\Exception $e) {
    		Log::error(
                'Failed to generate candidate per subject statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate candidate per subject statistics report");
    	}
    }

    /**
     * Getting Statistics Report List
     * @param Request $request
     * @return JsonResponse
     */
    public function statisticsReportList(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received', [
                'method' => __METHOD__,
                'data' => ['data' => $data]
            ]);
            $list = $this->statisticService->statisticsReportList($data);
            
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);
            
            if (count($list) > 0) {
                return $this->sendSuccessResponse('Candidate per subject statistics report list found', $list);
            } else {
                return $this->sendSuccessResponse('Candidate per subject statistics report list not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get list of candidate per subject statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get list of candidate per subject statistics report");
        }
    }

    /**
     * Generating Statistics Report.
     * @param CandidatePerSubjectStatisticsRequest $request
     * @return JsonResponse
     */
    public function candidatePerCentreReport(CandidatePerCentreStatisticsRequest $request)
    {
        try {
            $data = $request->all();
            $generate = $this->statisticService->candidatePerCentreReport($data);
            if ($generate == 1) {
                return $this->sendSuccessResponse("Candidate Per Centre Report Generated Successfully");
            } elseif ($generate == 2) {
                return $this->sendErrorResponse("Expiry time limit exceeded");
            } else {
                return $this->sendErrorResponse("Candidate Per Centre Report Not Generated");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to generate candidate per centre statistics report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate candidate per centre statistics report");
        }
    }
}
