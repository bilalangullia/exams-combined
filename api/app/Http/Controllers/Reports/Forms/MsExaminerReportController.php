<?php

namespace App\Http\Controllers\Reports\Forms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MsExaminerReportGenerateRequest;
use Illuminate\Support\Facades\Log;
use PDF;
use Response;
use Storage;
use Validator;
use App\Services\Report\Forms\MsExaminerReportService;

class MsExaminerReportController extends Controller
{
    /**
     * MsExaminerReportService instance.
     * @var $msExaminerReportService
     */
    protected $msExaminerReportService;

    /**
     * MsExaminerReportController Constructor.
     * @param MsExaminerReportService
     */
    public function __construct(
        MsExaminerReportService $msExaminerReportService
    )
    {
        $this->msExaminerReportService = $msExaminerReportService;
    }

    /**
     * Generating Report
     * @param MsExaminerReportGenerateRequest $request
     * @return JsonResponse
     */
    public function generateMsExaminerReport(MsExaminerReportGenerateRequest $request)
    {
    	try {
    		$data = $request->all();
            $generate = $this->msExaminerReportService->generate($data);
            
    		if ($generate == 1) {
                return $this->sendSuccessResponse("Report Generated Successfully");
            } elseif ($generate == 2) {
                return $this->sendErrorResponse("Expiry time limit exceeded");
            } else {
                return $this->sendErrorResponse("Report Not Generated");
            }
    	} catch (\Exception $e) {
    		log::error(
                'Failed to genrate report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to genrate report");
    	}
    }
}
