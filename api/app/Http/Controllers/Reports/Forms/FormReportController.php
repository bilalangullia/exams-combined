<?php

namespace App\Http\Controllers\Reports\Forms;

use App\Http\Controllers\Controller;
use App\Http\Requests\MultipleChoiceReportRequest;
use App\Jobs\ProcessReport;
use App\Models\AcademicPeriod;
use App\Models\Examination;
use App\Models\ExaminationCentre;
use App\Models\ExaminationSubject;
use App\Repositories\ExaminationStudents\ExaminationStudentRepository;
use App\Repositories\FormReportRepository;
use App\Services\Report\Forms\FormReportService;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PDF;
use Response;
use Storage;
use Validator;

class FormReportController extends Controller
{

    /**
     * @var FormReportRepository
     */
    protected $formReportRepository;

    /**
     * @var FormReportService
     */
    protected $formReportService;

    /**
     * @var ExaminationStudentRepository
     */
    protected $examinationStudentRepository;

    /**
     * FormReportController constructor.
     * @param FormReportService $formReportService
     * @param FormReportRepository $formReportRepository
     */
    public function __construct(
        FormReportService $formReportService,
        FormReportRepository $formReportRepository,
        ExaminationStudentRepository $examinationStudentRepository

    ) {
        $this->formReportService = $formReportService;
        $this->formReportRepository = $formReportRepository;
        $this->examinationStudentRepository = $examinationStudentRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function candidateListDropdown(Request $request)
    {
        try {
            $candidatelist = $this->formReportRepository->candidateDropdown($request);
            if ($candidatelist->count()) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['CandidateList' => $candidatelist]]
                );

                return $this->sendSuccessResponse("Candidate List Found", $candidatelist);
            } else {
                return $this->sendSuccessResponse("Candidate List not Found", $candidatelist);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to add review into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }

    /**
     * @param string $candidateId
     * @return \Illuminate\Http\JsonResponse
     */
    public function candidateSubjectDropdown(Request $request, string $candidates)
    {
        try {
            $subjectlist = $this->formReportRepository->candidateSelectSubject($request, $candidates);
            if ($subjectlist->count()) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['SubjectList' => $subjectlist]]
                );

                return $this->sendSuccessResponse("Subject List Found", $subjectlist);
            } else {
                return $this->sendSuccessResponse("Subject List not Found", $subjectlist);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to add review into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(' Subject List Not Found');
        }
    }

    /**
     * @param MultipleChoiceReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateReport(MultipleChoiceReportRequest $request)
    {
        try {
            $academicPeriod = AcademicPeriod::select('code', 'name', 'start_year')->where('id', $request['academic_period_id'])->first();
            $examName = Examination::select('code', 'attendance_type_id', 'name')->where('id', $request['examination_id'])->first();
            $examCenterNameCode = ExaminationCentre::select('code', 'name')
                ->where('id', $request['examination_centre_id'])->first();
            $subject = ExaminationSubject::select('code', 'name')->where('id', $request['examination_subject_id'])
                ->where('examination_id', $request['examination_id'])->first();
            if (empty($academicPeriod)) {
                return $this->sendErrorResponse("No such academic period found");
            }
            if (empty($examName)) {
                return $this->sendErrorResponse("No such exam found");
            }
            if (empty($examCenterNameCode)) {
                return $this->sendErrorResponse("No such exam centre found");
            }
            if (empty($subject)) {
                return $this->sendErrorResponse("No such exam subject found");
            }
            $candidates = $this->formReportRepository->getCandidatesByExamIdAndExamCenterId(
                $request->examination_id,
                $request->examination_centre_id,
                $request->candidate
            );
            if (empty($candidates)) {
                return $this->sendErrorResponse("No candidate found");
            }
            $reportData = $this->formReportService->formatReportData(
                $academicPeriod,
                $examName,
                $examCenterNameCode,
                $subject,
                $candidates
            );
            if (empty($reportData)) {
                return $this->sendErrorResponse("No report data found");
            }
            $status = $this->formReportService->saveUpdateReport($reportData, $request->all());
            if (!empty($status['msg'])) {
                return $this->sendErrorResponse($status['msg']);
            }

            return $this->sendSuccessResponse("Report generated and saved successfully");
        } catch (\Exception $e) {
            Log::error(
                'Failed to handle to generate the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $error['msg'] = "something went wrong";
        }
    }
    /**
     * To download the pdf report from url
     *
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */

    public function downloadReport(Request $request)
    {
        $url = $request->url;
        $fileName = basename($url);
        if (Storage::disk('local')->exists('public/reports/' . $fileName)) {
            return response()->download(public_path("storage/reports/" . $fileName));
        } else {
            return $this->sendErrorResponse("Report does not exist");
        }
    }
    /**
     * Getting Reports List
     * @param array $data
     * @return array
     */
    public function listReports(Request $request)
    {
        try {
            $data = $request->all();
            $reports = $this->formReportService->listingReports($request);
            if (empty($reports)) {
                return $this->sendSuccessResponse("No reports found");
            } else {
                return $this->sendSuccessResponse("Report listing is successful", $reports);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }
}
