<?php

namespace App\Http\Controllers\Reports\Forms;

use App\Http\Controllers\Controller;
use App\Services\Report\Forms\ForecastGradeService;
use App\Http\Requests\ForecastGradeFormRequest;
use Illuminate\Support\Facades\Log;

class ForecastGradeFormController extends Controller
{
    protected $forecastGradeService;

	public function __construct(
        ForecastGradeService $forecastGradeService
     )
    {
        $this->forecastGradeService = $forecastGradeService;
    }

    public function generateReport(ForecastGradeFormRequest $request)
    {
        try {
            $data = $request->all();
            $generate = $this->forecastGradeService->generateReport($data);
            if ($generate == 1) {
                return $this->sendSuccessResponse("Report Generated Successfully", true);
            } elseif ($generate == 2) {
                return $this->sendErrorResponse("Expiry time limit exceeded");
            } else {
                return $this->sendErrorResponse("Report Not Generated");
            }
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Forecast Grade Report Not Generated'); 
        }
       
    }
}
