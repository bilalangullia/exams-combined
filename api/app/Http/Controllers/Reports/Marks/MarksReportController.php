<?php

namespace App\Http\Controllers\Reports\Marks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\MarksReportRepository;
use App\Services\Report\Marks\MarksReportService;
use Illuminate\Support\Facades\Log;

class MarksReportController extends Controller
{
    /**
     * @var MarksReportRepository
     */
    protected $marksReportRepository;

    /**
     * @var MarksReportService
     */
    protected $marksReportService;

    /**
     * MarksReportController constructor.
     * @param MarksReportService $marksReportService
     * @param MarksReportRepository $marksReportRepository
     */

    public function __construct(
        MarksReportService $marksReportService,
        MarksReportRepository $marksReportRepository

    ) {
        $this->marksReportService = $marksReportService;
        $this->marksReportRepository = $marksReportRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function candidateListDropdown(Request $request)
    {
        try {
            $candidatelist = $this->marksReportRepository->candidateDropdownList($request);
            if ($candidatelist->count()) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['componentlist' => $candidatelist]]
                );

                return $this->sendSuccessResponse("Candidate List Found", $candidatelist);
            } else {
                return $this->sendSuccessResponse("Candidate List not Found", $candidatelist);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to add review into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateReport(Request $request)
    {
        try {
            $data = $request->all();
            $generate = $this->marksReportRepository->generate($data);
            return $this->sendSuccessResponse("Report Generated Successfully");
        } catch (\Exception $e) {
            echo $e;
            log::error(
                'Failed to generate report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to generate report");
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function componenetListDropdown(int $optionId)
    {
        try {
            $componentlist = $this->marksReportRepository->getComponentDropdownList($optionId);
            if ($componentlist->count()) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['componentlist' => $componentlist]]
                );

                return $this->sendSuccessResponse("component List Found", $componentlist);
            } else {
                return $this->sendSuccessResponse("component List not Found", $componentlist);
            }
        } catch (\Exception $e) {
            log::error(
                'Failed to genrate report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Component Not Found ");
        }
    }
}
