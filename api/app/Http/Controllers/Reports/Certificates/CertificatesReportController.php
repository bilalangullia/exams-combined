<?php

namespace App\Http\Controllers\Reports\Certificates;

use App\Http\Controllers\Controller;
use App\Http\Requests\CertificatesReportRequest;
use Illuminate\Http\Request;
use App\Services\Report\Certificates\CertificatesReportService;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use Illuminate\Support\Facades\Log;
use App\Repositories\CertificatesReportRepositoy;

class CertificatesReportController extends Controller
{
   protected $certificatesReportRepositoy;
   protected $certificatesReportService;
   protected $examinationRepository;
   protected $examinationCentreRepository;

    public function __construct(
    	CertificatesReportRepositoy $certificatesReportRepositoy,
        CertificatesReportService $certificatesReportService,
        ExaminationRepository $examinationRepository,
        ExaminationCentreRepository $examinationCentreRepository
    ) {
    	$this->certificatesReportRepositoy = $certificatesReportRepositoy;
        $this->certificatesReportService = $certificatesReportService;
        $this->examinationRepository = $examinationRepository;
        $this->examinationCentreRepository = $examinationCentreRepository;
    }

    public function getExaminationCentreDropdown(Request $request)
    {
        try{
            $centre = $this->certificatesReportService->getExamCentreDropDown($request);

            return $this->sendSuccessResponse("Examination Centre Drop down Found", $centre);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from db',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Exam Centre Drop down not Found');
        }
    }

    public function generateReport(CertificatesReportRequest $request)
    {
        try {
            $reportData = $request->all();
            $examName = $this->examinationRepository->getExamName($reportData['examination_id']);
            $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
                $reportData['examination_centre_id']
            );

            $status = $this->certificatesReportService->saveUpdateReport($reportData);

            return $this->sendSuccessResponse("Certificate Report Generated Successfully", true);
        } catch (\Exception $e) {
            Log::error(
                'unable to update status and error message of the report',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

             return $this->sendErrorResponse('Certificate Report Not Generated'); 
        }
       
    }
}
