<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public function sendErrorResponse($message, $data = [])
    {
        return response()->json(
            [
                'message' => $message,
                'data' => $data,
            ],
            config('constants.statusCodes.resourceNotFound')
        );
    }

    public function sendSuccessResponse($message, $data = [])
    {
        return response()->json(
            [
                'message' => $message,
                'data' => $data,
            ],
            config('constants.statusCodes.success')
        );
    }


    public function changeDateFormat($date)
    {
        return (Carbon::createFromFormat('Y-m-d', $date)->toDateString());
    }

    public function sendDeleteErrorResponse($message)
    {
        return response()->json(
            [
                'message' => $message,
            ],
            config('constants.statusCodes.deleteError')
        );
    }
}
