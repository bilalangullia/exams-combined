<?php

namespace App\Http\Controllers\Administration\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Administration\Security\RoleService;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\SecurityUserRoleStoreRequest;
use App\Http\Requests\SecuritySystemRoleStoreRequest;
use App\Http\Requests\SecurityUserRoleUpdateRequest;
use App\Http\Requests\SecuritySystemRoleUpdateRequest;
use App\Http\Requests\SecurityRoleReorderRequest;
use App\Http\Requests\SecurityRolePermissionUpdateRequest;
use App\Repositories\RoleRepository;

class RoleController extends Controller
{
    /**
     * RoleService instance
     * @var RoleService
     */
    protected $roleService;

    /**
     * RoleRepository instance
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * RoleController constructor.
     * @param RoleService $roleService
     * @param RoleRepository $roleRepository
     */
    public function __construct(
        RoleService $roleService,
        RoleRepository $roleRepository
    )
    {
        $this->roleService = $roleService;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Getting User Role List
     * @param Request $request
     * @param int $groupId
     * @return JsonResponse
     */
    public function userRolesList(Request $request, int $groupId)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data, 'groupId' => $groupId]]);

            $roles = $this->roleService->userRolesList($data, $groupId);

            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['userRoles' => $roles]]);

            return $this->sendSuccessResponse("User Roles List Found", $roles);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    /**
     * Getting User Role Details
     * @param int $roleId
     * @return JsonResponse
     */
    public function userRoleView(int $roleId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['roleId' => $roleId]]);
            $roleDetails = $this->roleService->userRoleView($roleId);

            Log::info('Fetched use role details from DB', ['method' => __METHOD__, 'data' => ['roleDetails' => $roleDetails]]);
            if (!empty($roleDetails)) {
                return $this->sendSuccessResponse("User Roles Details Found", $roleDetails);
            } else {
                return $this->sendSuccessResponse("User Roles Details Not Found", $roleDetails);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch role details from DB");
        }
    }

    /**
     * Storing User Role Details
     * @param SecurityUserRoleStoreRequest $request
     * @return JsonResponse
     */
    public function userRoleStore(SecurityUserRoleStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $store = $this->roleRepository->userRoleStore($data);

            if ($store == 1) {
                Log::info('User Roles Details Stored', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("User Roles Details Stored");
            } else {
                Log::info('User Roles Details Not Stored', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("User Roles Details Not Stored");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store role details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store role details in DB");
        }
    }

    /**
     * Updating User Role Details
     * @param SecurityUserRoleUpdateRequest $request
     * @return JsonResponse
     */
    public function userRoleUpdate(SecurityUserRoleUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $update = $this->roleRepository->userRoleUpdate($data);

            if ($update == 1) {
                Log::info('User Roles Details Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("User Roles Details Updated");
            } else {
                Log::info('User Roles Details Not Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("User Roles Details Not Updated");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update user role details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update user role details in DB");
        }
    }

    /**
     * Getting System Role List
     * @param Request $request
     * @return JsonResponse
     */
    public function systemRolesList(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $roles = $this->roleService->systemRolesList($data);

            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['systemRoles' => $roles]]);

            return $this->sendSuccessResponse("System Roles List Found", $roles);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    /**
     * Getting System Role Details
     * @param int $roleId
     * @return JsonResponse
     */
    public function systemRoleView(int $roleId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['roleId' => $roleId]]);
            $roleDetails = $this->roleService->systemRoleView($roleId);

            if (!empty($roleDetails)) {
                return $this->sendSuccessResponse("System Roles Details Found", $roleDetails);
            } else {
                return $this->sendSuccessResponse("System Roles Details Not Found", $roleDetails);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch role details from DB");
        }
    }

    /**
     * Storing System Role Details
     * @param SecurityUserRoleStoreRequest $request
     * @return JsonResponse
     */
    public function systemRoleStore(SecuritySystemRoleStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $store = $this->roleRepository->systemRoleStore($data);

            if ($store == 1) {
                Log::info('System Role Details Stored', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("System Role Details Stored");
            } else {
                Log::info('System Role Details Not Stored', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("System Role Details Not Stored");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store role details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store role details in DB");
        }
    }

    /**
     * Updating System Role Details
     * @param SecuritySystemRoleUpdateRequest $request
     * @return JsonResponse
     */
    public function systemRoleUpdate(SecuritySystemRoleUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $update = $this->roleRepository->systemRoleUpdate($data);

            if ($update == 1) {
                Log::info('System Roles Details Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("System Roles Details Updated");
            } else {
                Log::info('User Roles Details Not Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("User Roles Details Not Updated");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update user role details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update user role details in DB");
        }
    }

    /**
     * Updating System Role Orders
     * @param SecurityRoleReorderRequest $request
     * @param int $groupId
     * @return JsonResponse
     */
    public function securityRoleReorder(SecurityRoleReorderRequest $request, int $groupId = -1)
    {
        try {
            $rolesOrder = $request->all();
            $update = $this->roleRepository->securityRoleReorder($rolesOrder, $groupId);

            if ($update == 1) {
                Log::info('Security Roles Orders Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("System Roles Orders Updated");
            } else {
                Log::info('Security Roles Orders Not Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Security Roles Orders Not Updated");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update role orders in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update role orders in DB");
        }
    }

    /**
     * Getting Role Permission List
     * @param Request $request
     * @param int $roleId
     * @return JsonResponse
     */
    public function permissionList(Request $request, int $roleId)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['roleId' => $roleId, 'data' => $data]]);
            $list = $this->roleService->permissionList($data, $roleId);
            
            Log::info('Fetched use role permission list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);
            if (count($list)>0) {
                return $this->sendSuccessResponse("Role Permission List Found", $list);
            } else {
                return $this->sendSuccessResponse("Role Permission List Not Found", $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch role permission list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch role permission list from DB");
        }
    }

    /**
     * Updating Role Permission List
     * @param SecurityRolePermissionUpdateRequest $request
     * @return JsonResponse
     */
    public function permissionListUpdate(SecurityRolePermissionUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $update = $this->roleRepository->permissionListUpdate($data);
            if ($update == 1) {
                Log::info('Roles Permission List Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Roles Permission List Updated");
            } else {
                Log::info('Roles Permission List Not Updated', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Roles Permission List Not Updated");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update role permission list in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update role permission list in DB");
        }
    }
}
