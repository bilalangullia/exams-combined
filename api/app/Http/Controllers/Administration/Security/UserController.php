<?php

namespace App\Http\Controllers\Administration\Security;

use App\Http\Controllers\Controller;
use App\Http\Requests\SecurityUserAddRequest;
use App\Http\Requests\SecurityUserUpdateRequest;
use App\Models\SecurityGroupUser;
use App\Models\SecurityUser;
use App\Repositories\UserRepository;
use App\Services\Administration\Security\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @var UserService
     */

    protected $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserService $userService,
        UserRepository $userRepository
    ) {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityUserList(Request $request)
    {
        try {
            $list = $this->userService->listingUser($request);
            if (empty($list)) {
                return $this->sendErrorResponse(" User List Not Found");
            }
                return $this->sendSuccessResponse("User List Found", $list);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User list not found");
        }
    }

    /**
     * security User View Details
     * @param string $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityUserView(string $openEmisno)
    {
        try {
            $userView = $this->userService->userDetailView($openEmisno);
            return $this->sendSuccessResponse("User Details  Found", $userView);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Details not found");
        }
    }

    /**
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityUserEdit(string $openEmisno)
    {
        try {
            $userView = $this->userService->userDetailEdit($openEmisno);
            return $this->sendSuccessResponse("User Details  Found", $userView);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Details not found");
        }
    }

    /**
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityUserAdded(SecurityUserAddRequest $request)
    {
        try {
            $addDetails = $this->userRepository->userDetailsAdded($request);
             Log::info(
                 'added details into DB',
                 ['method' => __METHOD__, 'data' => ['useradded' => $addDetails]]
             );
            return $this->sendSuccessResponse("Security User added Successfully", $addDetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to add user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Security User not Successfully");
        }
    }

    /**
     * @param SecurityUserAddRequest $request
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityUserUpdate(SecurityUserUpdateRequest $request, string $openEmisno)
    {
        try {
            $updateDetails = $this->userRepository->userDetailsUpdate($request, $openEmisno);
             Log::info(
                 'added details into DB',
                 ['method' => __METHOD__, 'data' => ['useradded' => $updateDetails]]
             );
            return $this->sendSuccessResponse("User updated Successfully", $updateDetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to add user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User not updated Successfully");
        }
    }

    /**
     * @param string $openEmisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityAccountView(string $openEmisno)
    {
        try {
            $userView = $this->userService->accountView($openEmisno);
            return $this->sendSuccessResponse("User Details  Found", $userView);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Details not found");
        }
    }

    /**
     * @param Request $request
     * @param string $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityAccountEdit(Request $request, string $username)
    {
        try {
            $userView = $this->userRepository->accountEdit($request, $username);
            Log::info(
                'Fetched edit details from DB',
                ['method' => __METHOD__, 'data' => ['password' => $userView]]
            );
            return $this->sendSuccessResponse("Account Password changed successfully", $userView);
        } catch (\Exception $e) {
            Log::error(
                'Failed to update user Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Account Password not changed");
        }
    }

    /**
     * generate unique openemis no
     * @return \Illuminate\Http\JsonResponse
     */
    public function securityuserOpenemis()
    {
        try {
            $openemis = $this->userRepository->generateOpenemisId();
            return $this->sendSuccessResponse("Generate successfully", $openemis);
        } catch (\Exception $e) {
            Log::error(
                'Failed to generated',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("openemis number not generated");

        }
    }
}
