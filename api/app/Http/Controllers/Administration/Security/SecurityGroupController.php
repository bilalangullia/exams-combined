<?php

namespace App\Http\Controllers\Administration\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SecurityGroupRepository;
use App\Services\Administration\Security\SecurityGroupService;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\SecurityUserGroupAddRequest;
use App\Jobs\DeleteSecurityUserGroup;

class SecurityGroupController extends Controller
{
    protected $securityGroupRepository;

    protected $securityGroupService;

    public function __construct(
        SecurityGroupRepository $securityGroupRepository,
        SecurityGroupService $securityGroupService
    ) {
        $this->securityGroupRepository = $securityGroupRepository;
        $this->securityGroupService = $securityGroupService;
    }

    //getting autocompelete Security User list
    public function getAdministrationSecurityGroupsList(Request $request)
    {
    	try{
			$list = $this->securityGroupService->getGroupList($request);

    		return $this->sendSuccessResponse("Security User Group List Found", $list);
    	} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Security User Group List Not Found");
		}
    }

    //getting autocompelete Security Users
    public function getAutocompleteSecurityUsers(string $openemisId)
    {
        try{
            $userData = $this->securityGroupService->autocompleteSecurityUser($openemisId);

            return $this->sendSuccessResponse("Security User Group Details Found", $userData);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Security User Group Details Not Found");
        }
    }

    //getting autocompelete Area
    public function getAutocompleteArea(string $areaName)
    {
        try{
            $areaData = $this->securityGroupService->autocompleteArea($areaName);

            return $this->sendSuccessResponse("Area Details Found", $areaData);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Area Details Not Found");
        }
    }

    //getting autocompelete Area
    public function getAutocompleteInstitutions(string $institutionsName)
    {
        try{
            $institutionsData = $this->securityGroupService->autocompleteInstitutions($institutionsName);

            return $this->sendSuccessResponse("Institution Details Found", $institutionsData);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Institution Details Not Found");
        }
    }

    //getting security user groups view
    public function getSecurityUserGroupDetails(string $groupId)
     {
        try{
            $groupDetails = $this->securityGroupService->getSecurityUserGroupDetails($groupId);

            return $this->sendSuccessResponse("Security User Group Details Found", $groupDetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Security User Group Details Not Found");
        }
    }

    //getting security system groups list
    public function getAdministrationSecuritySystemList(Request $request)
    {
        try{
            $systemGroupList = $this->securityGroupService->getSystemGroupList($request);

            return $this->sendSuccessResponse("Security System Group List Found", $systemGroupList);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Security System Group List Not Found");
        }
    }

    //getting security system groups  view
    public function getSecuritySystemGroupDetails(string $sysGroupId)
     {
        try{
            $systemGroupDetails = $this->securityGroupService->getSecuritySystemGroupDetails($sysGroupId);

            return $this->sendSuccessResponse("Security System Group Details Found", $systemGroupDetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Security System Group Details Not Found");
        }
    }

    //getting security role drop-down
    public function getSecurityRoleDropdown()
    {
        try{
            $role = $this->securityGroupRepository->getRoleDropdown();

            return $this->sendSuccessResponse("Security Role Dropdown Found", $role);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Role Dropdown Not Found");
        }

    }

    public function addSecurityUserGroup(SecurityUserGroupAddRequest $request)
    {
        try{
            $addUserGroup = $this->securityGroupRepository->addUserGroup($request);
            
            return $this->sendSuccessResponse("User Group  Added Successfully", $addUserGroup);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group Not Added Successfully");
        }
    }

    public function updateSecurityUserGroup(SecurityUserGroupAddRequest $request, int $userGroupId)
    {
        try{
            $addUserGroup = $this->securityGroupRepository->updateUserGroup($request, $userGroupId);
            
            return $this->sendSuccessResponse("User Group  Updated Successfully", $addUserGroup);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("User Group Not Updated Successfully");
        }
    }

    public function destroySecurityUserGroup(int $userGroupId)
    {
        try{
            $job = new DeleteSecurityUserGroup($userGroupId, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if($response && is_array($response)){
                return $this->sendSuccessResponse("User Group Can Not Delete", $response);
            } else {
                return $this->sendSuccessResponse("User Group Deleted.", $response);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("User Group Can Not Delete.");
        }
    }
}
