<?php

namespace App\Http\Controllers\Administration\SystemSetup\AdministrativeBoundaries;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Administration\SystemSetup\AdministrativeBoundaries\AdministrativeBoundaryService;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\AreaLevelEducationUpdateRequest;
use App\Http\Requests\AreaLevelEducationStoreRequest;
use App\Http\Requests\AreaEducationUpdateRequest;
use App\Http\Requests\AreaEducationStoreRequest;
use App\Http\Requests\AreaLevelAdministrativeStoreRequest;
use App\Http\Requests\AreaLevelAdministrativeUpdateRequest;
use App\Http\Requests\AreaAdministrativeStoreRequest;
use App\Http\Requests\AreaAdministrativeUpdateRequest;
use App\Repositories\AdministrativeBoundaryRepository;
use App\Jobs\DeleteAreaLevelEducation;
use App\Jobs\DeleteAreaEducation;
use App\Jobs\DeleteAreaLevelAdministrative;
use App\Jobs\DeleteAreaAdministrative;

class AdministrativeBoundaryController extends Controller
{
    /**
     * AdministrativeBoundaryService instance
     * @var $administrativeBoundaryService
     */
    protected $administrativeBoundaryService;

    /**
     * AdministrativeBoundaryRepository instance
     * @var $administrativeBoundaryRepository
     */
    protected $administrativeBoundaryRepository;

    /**
     * AdministrativeBoundariesController constructor.
     * @param AdministrativeBoundaryService $administrativeBoundaryService
     */
    public function __construct(
        AdministrativeBoundaryService $administrativeBoundaryService,
        AdministrativeBoundaryRepository $administrativeBoundaryRepository
    )
    {
        $this->administrativeBoundaryService = $administrativeBoundaryService;
        $this->administrativeBoundaryRepository = $administrativeBoundaryRepository;
    }

	/**
     * Getting Area level (education) List
     * @param Request $request
     * @return JsonResponse
     */
	public function areaLevelEducationList(Request $request)
    {
    	try {
 			$data = $request->all();
 			Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

 			$list = $this->administrativeBoundaryService->areaLevelEducation($data);
 			Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('Area level list found', $list);
            } else {
                return $this->sendSuccessResponse('Area level list not found', $list);
            }
    	} catch (\Exception $e) {
    		Log::error(
                'Failed to get area level education list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level education list");
    	}
    }

    /**
     * Getting Area level (education) Details
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelEducationView(int $levelId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['levelId' => $levelId]]);

            $data = $this->administrativeBoundaryService->areaLevelEducationView($levelId);
            Log::info('Fetched area level details from DB', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            if (count($data) > 0) {
                return $this->sendSuccessResponse('Area level details found', $data);
            } else {
                return $this->sendSuccessResponse('Area level details not found', $data);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level education details");
        }
    }

    /**
     * Updating Area level (education) Details
     * @param AreaLevelEducationUpdateRequest $request
     * @return JsonResponse
     */
    public function areaLevelEducationUpdate(AreaLevelEducationUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $update = $this->administrativeBoundaryRepository->areaLevelEducationUpdate($data);
            if ($update == 1) {
                Log::info('Area Level Education details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level Education details updated in DB");
            } else {
                Log::info('Area Level Education details not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level Education details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area level education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update area level education details");
        }
    }

    /**
     * Storing Area level (education) Details
     * @param AreaLevelEducationStoreRequest $request
     * @return JsonResponse
     */
    public function areaLevelEducationStore(AreaLevelEducationStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $store = $this->administrativeBoundaryRepository->areaLevelEducationStore($data);
            if ($store == 1) {
                Log::info('Area Level Education details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level Education details stored in DB");
            } else {
                Log::info('Area Level Education details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level Education details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store area level education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store area level education details");
        }
    }

    /**
     * Getting Area (education) List
     * @param Request $request
     * @return JsonResponse
     */
    public function areaEducationList(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $list = $this->administrativeBoundaryService->areaEducationList($data);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('Area Education list found', $list);
            } else {
                return $this->sendSuccessResponse('Area Education list not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area education list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area education list");
        }
    }

    /**
     * Getting Area (education) Details
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaEducationView(int $areaId)
    {
        try {
            $data = $this->administrativeBoundaryService->areaEducationView($areaId);
            Log::info('Fetched area details from DB', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            if (count($data) > 0) {
                return $this->sendSuccessResponse('Area (education) details found', $data);
            } else {
                return $this->sendSuccessResponse('Area (education) details not found', $data);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area education details");
        }
    }

    /**
     * Updating Area (education) Details
     * @param AreaEducationUpdateRequest $request
     * @return JsonResponse
     */
    public function areaEducationUpdate(AreaEducationUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $update = $this->administrativeBoundaryRepository->areaEducationUpdate($data);
            if ($update == 1) {
                Log::info('Area Education details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Education details updated in DB");
            } else {
                Log::info('Area Education details not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Education details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update area education details");
        }
    }

    /**
     * Storing Area (education) Details
     * @param AreaEducationStoreRequest $request
     * @return JsonResponse
     */
    public function areaEducationStore(AreaEducationStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $store = $this->administrativeBoundaryRepository->areaEducationStore($data);
            if ($store == 1) {
                Log::info('Area Education details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Education details stored in DB");
            } else {
                Log::info('Area Education details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Education details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area education details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update area education details");
        }
    }

    /**
     * Getting Area Level Dropdown via parent_id
     * @param int $parentId
     * @return JsonResponse
     */
    public function areaLevelDropdown(int $parentId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['parentId' => $parentId]]);

            $list = $this->administrativeBoundaryService->areaLevelDropdown($parentId);
            Log::info('Fetched dropdown from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('Area Level dropdown found', $list);
            } else {
                return $this->sendSuccessResponse('Area Level dropdown not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level dropdown");
        }
    }

    /**
     * Getting Area level (Administrative) Details
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelAdministrativeView(int $levelId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['levelId' => $levelId]]);

            $data = $this->administrativeBoundaryService->areaLevelAdministrativeView($levelId);
            Log::info('Fetched area level (administrative) details from DB', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            if (count($data) > 0) {
                return $this->sendSuccessResponse('Area Level (Administrative) details found', $data);
            } else {
                return $this->sendSuccessResponse('Area Level (Administrative) not found', $data);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level details");
        }
    }

    /**
     * Getting Area level (Administrative) List
     * @param Request $request
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaLevelAdministrativeList(Request $request, int $areaId)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data, 'areaId' => $areaId]]);

            $list = $this->administrativeBoundaryService->areaLevelAdministrativeList($data, $areaId);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('Area Level Administrative list found', $list);
            } else {
                return $this->sendSuccessResponse('Area Level Administrative list not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area level list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area level list");
        }
    }

    /**
     * Storing Area level (Administrative) details
     * @param AreaLevelAdministrativeStoreRequest $request
     * @return JsonResponse
     */
    public function areaLevelAdministrativeStore(AreaLevelAdministrativeStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $store = $this->administrativeBoundaryRepository->areaLevelAdministrativeStore($data);
            if ($store == 1) {
                Log::info('Area Level (Administrative) details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level (Administrative) details stored in DB");
            } else {
                Log::info('Area Level (Administrative) details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level (Administrative) details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store area level details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store area level details");
        }
    }

    /**
     * Updating Area level (Administrative) details
     * @param AreaLevelAdministrativeUpdateRequest $request
     * @return JsonResponse
     */
    public function areaLevelAdministrativeUpdate(AreaLevelAdministrativeUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $update = $this->administrativeBoundaryRepository->areaLevelAdministrativeUpdate($data);
            if ($update == 1) {
                Log::info('Area Level (Administrative) details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level (Administrative) details updated in DB");
            } else {
                Log::info('Area Level (Administrative) details not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Level (Administrative) details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area level details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update area level details");
        }
    }

    /**
     * Getting Area (Administrative) List
     * @param Request $request
     * @return JsonResponse
     */
    public function areaAdministrativeList(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $list = $this->administrativeBoundaryService->areaAdministrativeList($data);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('Area Administrative list found', $list);
            } else {
                return $this->sendSuccessResponse('Area Administrative list not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative list");
        }
    }

    /**
     * Getting Area (Administrative) Details
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaAdministrativeView(int $areaId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['areaId' => $areaId]]);

            $data = $this->administrativeBoundaryService->areaAdministrativeView($areaId);
            Log::info('Fetched area (administrative) details from DB', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            if (count($data) > 0) {
                return $this->sendSuccessResponse('Area (Administrative) details found', $data);
            } else {
                return $this->sendSuccessResponse('Area (Administrative) not found', $data);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative details");
        }
    }

    /**
     * Getting Area Level (Administrative) Dropdown
     * @param int $parentId
     * @return JsonResponse
     */
    public function areaAdministrativeLevelDropdown(int $parentId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['parentId' => $parentId]]);

            $list = $this->administrativeBoundaryService->areaAdministrativeLevelDropdown($parentId);
            Log::info('Fetched dropdown from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('Area Administrative Level dropdown found', $list);
            } else {
                return $this->sendSuccessResponse('Area Administrative Level dropdown not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative level dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative level dropdown");
        }
    }

    /**
     * Storing Area (Administrative) Details
     * @param AreaAdministrativeStoreRequest $request
     * @return JsonResponse
     */
    public function areaAdministrativeStore(AreaAdministrativeStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $store = $this->administrativeBoundaryRepository->areaAdministrativeStore($data);
            if ($store == 1) {
                Log::info('Area (Administrative) details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area (Administrative) details stored in DB");
            } else {
                Log::info('Area (Administrative) details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area (Administrative) details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store area administrative details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store area administrative details");
        }
    }

    /**
     * Updating Area (Administrative) Details
     * @param AreaAdministrativeUpdateRequest $request
     * @return JsonResponse
     */
    public function areaAdministrativeUpdate(AreaAdministrativeUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            
            $update = $this->administrativeBoundaryRepository->areaAdministrativeUpdate($data);
            if ($update == 1) {
                Log::info('Area Administrative details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Administrative details updated in DB");
            } else {
                Log::info('Area Administrative details not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Area Administrative details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update area administrative',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update area administrative");
        }
    }

    /**
     * Deleteing Area Level (Education)
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelEducationDelete(int $levelId)
    {
        try {
            $job = new DeleteAreaLevelEducation($levelId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            
            if ($response == 1) {
                return $this->sendSuccessResponse("Area Level (Education) deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Area Level (Education) not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area level (education)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area level (education)");
        }
    }

    /**
     * Deleteing Area (Education)
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaEducationDelete(int $areaId)
    {
        try {
            $job = new DeleteAreaEducation($areaId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            
            if ($response == 1) {
                return $this->sendSuccessResponse("Area (Education) deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Area (Education) not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area (education)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area (education)");
        }
    }

    /**
     * Deleteing Area Level (Administrative)
     * @param int $levelId
     * @return JsonResponse
     */
    public function areaLevelAdministrativeDelete(int $levelId)
    {
        try {
            $job = new DeleteAreaLevelAdministrative($levelId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            
            if ($response == 1) {
                return $this->sendSuccessResponse("Area Level (Administrative) deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Area Level (Administrative) not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area level (administrative)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area level (administrative)");
        }
    }

    /**
     * Deleteing Area (Administrative)
     * @param int $areaId
     * @return JsonResponse
     */
    public function areaAdministrativeDelete(int $areaId)
    {
        try {
            $job = new DeleteAreaAdministrative($areaId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            
            if ($response == 1) {
                return $this->sendSuccessResponse("Area (Administrative) deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Area (Administrative) not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete area (administrative)',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete area (administrative)");
        }
    }
}
