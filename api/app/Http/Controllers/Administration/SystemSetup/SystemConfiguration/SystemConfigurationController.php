<?php

namespace App\Http\Controllers\Administration\SystemSetup\SystemConfiguration;

use App\Http\Controllers\Controller;
use App\Http\Requests\SystemConfigurationUpdateRequest;
use App\Repositories\SystemConfigurationRepository;
use App\Services\Administration\SystemSetup\SystemConfiguration\SystemConfigurationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SystemConfigurationController extends Controller
{
    /**
     * @var SystemConfigurationRepository
     */
    protected $systemConfigurationRepository;

    /**
     * @var SystemConfigurationService
     */
    protected $systemConfigurationService;

    /**
     * SystemConfigurationController constructor.
     * @param SystemConfigurationService $systemConfigurationService
     * @param SystemConfigurationRepository $systemConfigurationRepository
     */
    public function __construct(
        SystemConfigurationService $systemConfigurationService,
        SystemConfigurationRepository $systemConfigurationRepository
    ) {
        $this->systemConfigurationService = $systemConfigurationService;
        $this->systemConfigurationRepository = $systemConfigurationRepository;
    }

    /**
     * @param Request $request
     * @param int $systemconfigId
     * @return \Illuminate\Http\JsonResponse
     */
    public function systemConfigurationList(Request $request, int $systemconfigId)
    {
        try {
            $list = $this->systemConfigurationService->getConfigurationList($request, $systemconfigId);
            if (empty($list)) {
                return $this->sendErrorResponse("System Configuration Not Found");
            }
            return $this->sendSuccessResponse("System Configuration List Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("System Configuration  list not found");
        }
    }

    /**
     * @param int $systemconfigId
     * @return \Illuminate\Http\JsonResponse
     */
    public function systemConfigurationView(int $systemconfigId)
    {
        try {
            $view = $this->systemConfigurationService->getConfigurationView($systemconfigId);
            return $this->sendSuccessResponse("System Configuration View  Found", $view);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("System Configuration View Not Found");
        }
    }

    /**
     * @param int $systemconfigId
     * @return \Illuminate\Http\JsonResponse
     */
    public function systemConfigurationUpdate(SystemConfigurationUpdateRequest $request, int $systemconfigId)
    {
        try {
            $update = $this->systemConfigurationRepository->updateSystemConfiguration($request, $systemconfigId);
            Log::info(
                'Fetched edit details from DB',
                ['method' => __METHOD__, 'data' => ['SystemConfigupdate' => $update]]
            );
            return $this->sendSuccessResponse("System Configuration Updated Successfully", $update);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch user list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("System Configuration  Not Updated");
        }
    }
}
