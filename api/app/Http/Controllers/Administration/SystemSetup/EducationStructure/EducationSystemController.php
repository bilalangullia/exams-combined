<?php

namespace App\Http\Controllers\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use App\Repositories\EducationSystemRepository;
use App\Http\Requests\EducationSystemAddRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\Administration\SystemSetup\EducationStructure\EducationSystemService;
use App\Jobs\DeleteEducationSystem;

class EducationSystemController extends Controller
{
    protected $educationSystemRepository;
    protected $educationSystemService;

    public function __construct(
    	EducationSystemRepository $educationSystemRepository,
    	EducationSystemService $educationSystemService
    ) {
    	$this->educationSystemRepository = $educationSystemRepository;
    	$this->educationSystemService = $educationSystemService;
    }

    public function getAdministrationSystemSetupEducationSystemList(Request $request)
    {
    	try{
    		$list = $this->educationSystemService->getSystemList($request);

			return $this->sendSuccessResponse("Education System List Found", $list);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System List Not Found');
		}
    }

    public function getAdministrationSystemSetupEducationSystemDetail(int $eduSysId)
    {
    	try{
    		$view = $this->educationSystemService->getSystemDetail($eduSysId);

    		return $this->sendSuccessResponse("Education System Detail Found", $view);
    	}catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System Detail Not Found');
		}

    }

    public function storeAdministrationSystemSetupEducationSystem(EducationSystemAddRequest $request)
    {
    	try{
    		$addRequest  = $request->all();
    		$add = $this->educationSystemRepository->addEducationSystem($addRequest);

    		return $this->sendSuccessResponse('Education System Added Successfully', $add);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System  Not Added');
		}
    }


    public function updateAdministrationSystemSetupEducationSystem(int $eduSysId, EducationSystemAddRequest $request)
    {
    	try{
    		$updateRequest  = $request->all();
    		$update = $this->educationSystemRepository->updateEducationSystem($eduSysId, $updateRequest);

    		return $this->sendSuccessResponse('Education System Updated Successfully', $update);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System  Not Updated');
		}
    }

    public function destroyAdministrationSystemSetupEducationSystem(int $eduSysId)
    {
    	try{
    		$job = new DeleteEducationSystem($eduSysId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Education System Deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Education System  Can Not Deleted");
            }
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System  Can Not Deleted');
		}
    }
}
