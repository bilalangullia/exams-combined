<?php

namespace App\Http\Controllers\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Administration\SystemSetup\EducationStructure\EducationGradeService;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\EducationGradeStoreRequest;
use App\Http\Requests\EducationGradeUpdateRequest;
use App\Http\Requests\EducationGradeSubjectStoreRequest;
use App\Http\Requests\EducationGradeSubjectUpdateRequest;
use App\Http\Requests\EducationSetupSubjectStoreRequest;
use App\Http\Requests\EducationSetupSubjectUpdateRequest;
use App\Http\Requests\EducationGradeReorderRequest;
use App\Http\Requests\EducationSubjectReorderRequest;
use App\Repositories\EducationGradeRepository;
use App\Jobs\DeleteEducationGrade;
use App\Jobs\DeleteEducationGradeSubject;

class EducationGradeController extends Controller
{
    /**
     * EducationGradeService instance
     * @var $educationGradeService
     */
    protected $educationGradeService;

    /**
     * EducationGradeRepository instance
     * @var $educationGradeRepository
     */
    protected $educationGradeRepository;

    /**
     * EducationGradeController constructor.
     * @param EducationGradeService $educationGradeService
     * @param EducationGradeRepository $educationGradeRepository
     */
    public function __construct(
        EducationGradeService $educationGradeService,
        EducationGradeRepository $educationGradeRepository
    )
    {
        $this->educationGradeService = $educationGradeService;
        $this->educationGradeRepository = $educationGradeRepository;
    }

    /**
     * Getting Education Grades List
     * @param Request $request
     * @param int $programId
     * @return JsonResponse
     */
    public function educationGrades(Request $request, int $programId)
    {
    	try {
    		$data = $request->all();
    		Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data, 'programId' => $programId]]);

    		$gradeList = $this->educationGradeService->educationGrades($data, $programId);

    		Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['gradeList' => $gradeList]]);

            if (count($gradeList) > 0) {
                return $this->sendSuccessResponse('Education Grade list found', $gradeList);
            } else {
                return $this->sendSuccessResponse('Education Grade list not found', $gradeList);
            }
    	} catch (\Exception $e) {
    		Log::error(
                'Failed to get education grade list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade list");
    	}
    }

    /**
     * Getting Education Grades Details
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeView(int $gradeId)
    {
        try {
            Log::info('Request Parameters Received', ['method' => __METHOD__, 'data' => ['gradeId' => $gradeId]]);

            $gradeData = $this->educationGradeService->educationGradeView($gradeId);

            Log::info('Fetched education grade details from DB', ['method' => __METHOD__, 'data' => ['gradeData' => $gradeData]]);
            if (count($gradeData) > 0) {
                return $this->sendSuccessResponse('Education Grade details found', $gradeData);
            } else {
                return $this->sendSuccessResponse('Education Grade details not found', $gradeData);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade list");
        }
    }

    /**
     * Storing Education Grades Details
     * @param EducationGradeStoreRequest $request
     * @return JsonResponse
     */
    public function educationGradeStore(EducationGradeStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $store = $this->educationGradeRepository->educationGradeStore($data);
            if ($store == 1) {
                Log::info('Education grade details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade details stored in DB");
            } else {
                Log::info('Education grade details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store education grade details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store education grade details in DB");
        }
    }

    /**
     * Updating Education Grades Details
     * @param EducationGradeUpdateRequest $request
     * @return JsonResponse
     */
    public function educationGradeUpdate(EducationGradeUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $update = $this->educationGradeRepository->educationGradeUpdate($data);
            if ($update == 1) {
                Log::info('Education grade details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade details updated in DB");
            } else {
                Log::info('Education grade details not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education grade details in DB");
        }
    }

    /**
     * Getting Education Grade Subjects
     * @param Request $request
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeSubjects(Request $request, int $gradeId)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data, 'gradeId' => $gradeId]]);

            $subjects = $this->educationGradeService->educationGradeSubjects($data, $gradeId);

            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['subjects' => $subjects]]);

            if (count($subjects) > 0) {
                return $this->sendSuccessResponse('Education grade subjects list found', $subjects);
            } else {
                return $this->sendSuccessResponse('Education grade subjects list not found', $subjects);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects list");
        }
    }

    /**
     * Getting Education Grade Subject Details
     * @param int $gradeId
     * @param int $subjectId
     * @return JsonResponse
     */
    public function educationGradeSubjectView(int $gradeId, int $subjectId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['gradeId' => $gradeId, 'subjectId' => $subjectId]]);

            $data = $this->educationGradeService->educationGradeSubjectView($gradeId, $subjectId);

            Log::info('Fetched education grade subject details from DB', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            if (count($data) > 0) {
                return $this->sendSuccessResponse('Education Grade Subject details found', $data);
            } else {
                return $this->sendSuccessResponse('Education Grade Subject details not found', $data);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade subjects details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade subjects details");
        }
    }

    /**
     * Getting Education Grade Details
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeSubjectAdd(int $gradeId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['gradeId' => $gradeId]]);

            $data = $this->educationGradeService->educationGradeSubjectAdd($gradeId);

            Log::info('Fetched education grade details from DB', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            if (count($data) > 0) {
                return $this->sendSuccessResponse('Education Grade details found', $data);
            } else {
                return $this->sendSuccessResponse('Education Grade details not found', $data);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education grade details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education grade details");
        }
    }

    /**
     * Storing Education Grade Subject Details
     * @param EducationGradeSubjectStoreRequest $request
     * @return JsonResponse
     */
    public function educationGradeSubjectStore(EducationGradeSubjectStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $store = $this->educationGradeRepository->educationGradeSubjectStore($data);
            if ($store == 1) {
                Log::info('Education grade subject details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade subject details stored in DB");
            } elseif ($store == 2) {
                Log::info('Education grade subject for select education grade already exists', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade subject for select education grade already exists");
            } else {
                Log::info('Education grade subject details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade subject details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store education grade subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store education grade subject details");
        }
    }

    /**
     * Updating Education Grade Subject Details
     * @param EducationGradeSubjectUpdateRequest $request
     * @return JsonResponse
     */
    public function educationGradeSubjectUpdate(EducationGradeSubjectUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $update = $this->educationGradeRepository->educationGradeSubjectUpdate($data);
            if ($update == 1) {
                Log::info('Education grade subject details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade subject details updated in DB");
            } else {
                Log::info('Education grade subject details not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grade subject details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education grade subject details");
        }
    }

    /**
     * Getting Setup list.
     * @param Request $request
     * @param int $setupId
     * @return JsonResponse
     */
    public function educationGradeSetUp(Request $request, int $setupId)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $list = $this->educationGradeService->educationGradeSetUp($data, $setupId);
            if (count($list) > 0) {
                return $this->sendSuccessResponse('Setup list found', $list);
            } else {
                return $this->sendSuccessResponse('Setup list not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education setup list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education setup list");
        }
    }

    /**
     * Getting Setup subject details.
     * @param int $subjectId
     * @return JsonResponse
     */
    public function educationSetupSubjectView(int $subjectId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['subjectId' => $subjectId]]);
            $data = $this->educationGradeService->educationSetupSubjectView($subjectId);
            if (count($data) > 0) {
                return $this->sendSuccessResponse('Setup subject details found', $data);
            } else {
                return $this->sendSuccessResponse('Setup subject details not found', $data);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get subject details");
        }
    }

    /**
     * Storing Setup subject details.
     * @param EducationSetupSubjectStoreRequest $request
     * @return JsonResponse
     */
    public function educationSetupSubjectStore(EducationSetupSubjectStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $store = $this->educationGradeRepository->educationSetupSubjectStore($data);

            if ($store == 1) {
                Log::info('Education subject details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education subject details stored in DB");
            } else {
                Log::info('Education subject details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education subject details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to store subject details");
        }
    }

    /**
     * Updating Setup subject details.
     * @param EducationSetupSubjectUpdateRequest $request
     * @return JsonResponse
     */
    public function educationSetupSubjectUpdate(EducationSetupSubjectUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $update = $this->educationGradeRepository->educationSetupSubjectUpdate($data);

            if ($update == 1) {
                Log::info('Education subject details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education subject details updated in DB");
            } else {
                Log::info('Education subject details not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education subject details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update subject details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update subject details");
        }
    }

    /**
     * Updating Education grades order
     * @param EducationGradeReorderRequest $request
     * @return JsonResponse
     */
    public function educationGradeReorder(EducationGradeReorderRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $update = $this->educationGradeRepository->educationGradeReorder($data);
            if ($update == 1) {
                Log::info('Education grades order updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grades order updated in DB");
            } else {
                Log::info('Education grades order not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education grades order not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade order details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education grade order details");
        }
    }

    /**
     * Updating Education grades order
     * @param EducationSubjectReorderRequest $request
     * @return JsonResponse
     */
    public function educationSubjectReorder(EducationSubjectReorderRequest $request)
    {
        try {
            $data = $request->all();
            
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $update = $this->educationGradeRepository->educationSubjectReorder($data);
            if ($update == 1) {
                Log::info('Education subject order updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education subject order updated in DB");
            } else {
                Log::info('Education subject order not updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Education subject order not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education subject order details',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education subject order details");
        }
    }

    /**
     * Deleting Education grade
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeDelete(int $gradeId)
    {
        try {
            $job = new DeleteEducationGrade($gradeId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            
            if ($response == 1) {
                return $this->sendSuccessResponse("Education grade deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Education grade can not be deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete education grade',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete education grade");
        }
    }


    /**
     * Deleting Education grade subject
     * @param int $gradeId
     * @return JsonResponse
     */
    public function educationGradeSubjectDelete(int $gradeId, int $subjectId)
    {
        try {
            $job = new DeleteEducationGradeSubject($gradeId, $subjectId);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            
            if ($response == 1) {
                return $this->sendSuccessResponse("Education grade subject deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Education grade subject can not be deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete education grade',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete education grade");
        }
    }
}
