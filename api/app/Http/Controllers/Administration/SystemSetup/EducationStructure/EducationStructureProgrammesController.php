<?php

namespace App\Http\Controllers\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use App\Http\Requests\EducationStruProgrammeAddRequest;
use App\Http\Requests\EducationStruProgrammeUpdateRequest;
use App\Models\EducationProgramme;
use App\Models\EducationProgrammesNextProgramme;
use App\Repositories\EducationStructureProgrammesRepository;
use App\Services\Administration\SystemSetup\EducationStructure\EducationStructureProgrammesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EducationStructureProgrammesController extends Controller
{
    /**
     * @var EducationStructureProgrammesRepository
     */
    protected $educationStructureProgrammesRepository;

    /**
     * @var EducationStructureProgrammesService
     */
    protected $educationStructureProgrammesService;

    /**
     * EducationStructureProgrammesController constructor.
     * @param EducationStructureProgrammesService $educationStructureProgrammesService
     * @param EducationStructureProgrammesRepository $educationStructureProgrammesRepository
     */

    public function __construct(
        EducationStructureProgrammesService $educationStructureProgrammesService,
        EducationStructureProgrammesRepository $educationStructureProgrammesRepository

    ) {
        $this->educationStructureProgrammesService = $educationStructureProgrammesService;
        $this->educationStructureProgrammesRepository = $educationStructureProgrammesRepository;
    }

    /**
     * @param Request $request
     * @param string $educationlevelId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationStructureProgrammesList(Request $request, string $educationCycleId)
    {
        try {
            $listing = $this->educationStructureProgrammesService->getProgrammesListing($request, $educationCycleId);
            if (!empty($listing)) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['Programmes List' => $listing]]
                );

                return $this->sendSuccessResponse("Programmes List Found", $listing);
            } else {
                return $this->sendSuccessResponse("Programmes List not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Education Programmes List Not Found");
        }
    }

    /**
     * @param string $programmeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationStructureProgrammesView(string $programmeId)
    {
        try {
            $view = $this->educationStructureProgrammesService->getProgrammeView($programmeId);
            return $this->sendSuccessResponse("Programmes View Found", $view);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Education Programmes view Not Found");
        }
    }

    /**
     * @param EducationStruProgrammeAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addEducationStructureProgrammes(EducationStruProgrammeAddRequest $request)
    {
        try {
            $progAdd = $this->educationStructureProgrammesRepository->educationProgrammesAdd($request);
            Log::info(
                'updated details into DB',
                ['method' => __METHOD__, 'data' => ['Programmes' => $progAdd]]
            );
            return $this->sendSuccessResponse("Programmes Added Successfully", $progAdd);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Programme Not Added");
        }
    }

    /**
     * get next programme dropdown exclude current one.
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNextProgrammeDropdown(string $programmeId)
    {
        try {
            $nextProgramme = $this->educationStructureProgrammesRepository->nextProgrammeDropdowm($programmeId);
            if (!empty($nextProgramme)) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['Next Programmes List' => $nextProgramme]]
                );

                return $this->sendSuccessResponse("Next Programmes List Found", $nextProgramme);
            } else {
                return $this->sendSuccessResponse("Next Programmes List not Found", $nextProgramme);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Next Programmes List Not Found");
        }
    }

    /**
     * @param string $nextProgId
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNextCycleProgramme(string $nextProgId)
    {
        try {
            $nextProgCycle = $this->educationStructureProgrammesRepository->nextCycleProgramme($nextProgId);
            if (!empty($nextProgCycle)) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['Cycle ­ (Programme)' => $nextProgCycle]]
                );

                return $this->sendSuccessResponse("Cycle ­ (Programme) List Found", $nextProgCycle);
            } else {
                return $this->sendSuccessResponse("Cycle ­ (Programme) not Found", $nextProgCycle);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Cycle ­ (Programme) List Not Found");
        }
    }

    /**
     * @param EducationStruProgrammeAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateEducationProgrammes(EducationStruProgrammeUpdateRequest $request, string $programmeId)
    {
        try {
            $progUpdate = $this->educationStructureProgrammesRepository->educationCycleUpdate($request, $programmeId);
            Log::info(
                'updated details into DB',
                ['method' => __METHOD__, 'data' => ['Programmes' => $progUpdate]]
            );
            return $this->sendSuccessResponse("Education Programmes Updated Successfully", $progUpdate);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Programme Not Updated");
        }
    }

    /**
     * @param string $programmeId
     * @return \Illuminate\Http\JsonResponse
     */

    public function editEducationStructureProgrammes(string $programmeId)
    {
        try {
            $progedit = $this->educationStructureProgrammesService->getProgrammeEdit($programmeId);
            return $this->sendSuccessResponse("Education Programmes Updated Successfully", $progedit);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Programme Not Updated");
        }
    }
}
