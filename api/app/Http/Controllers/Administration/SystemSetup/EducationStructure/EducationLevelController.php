<?php

namespace App\Http\Controllers\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use App\Http\Requests\EducationLevelRequest;
use Illuminate\Http\Request;
use App\Repositories\EducationLevelRepository;
use Illuminate\Support\Facades\Log;
use App\Services\Administration\SystemSetup\EducationStructure\EducationLevelService;
use App\Jobs\DeleteEducationLevel;

class EducationLevelController extends Controller
{
    protected $educationLevelRepository;
    protected $educationLevelService;

    public function __construct(
    	EducationLevelRepository $educationLevelRepository,
    	EducationLevelService $educationLevelService
    ) {
    	$this->educationLevelRepository = $educationLevelRepository;
    	$this->educationLevelService = $educationLevelService;
    }

    public function getAdministrationSystemSetupEducationLevelList(Request $request)
    {
    	try{
    		$list = $this->educationLevelService->getEducationLevelList($request);

			return $this->sendSuccessResponse("Education Level List Found", $list);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level List Not Found');
		}
    }

    public function getAdministrationSystemSetupEducationLevelDetail(int $eduLvlId)
    {
    	try{
    		$view = $this->educationLevelService->getEducationLevelDetails($eduLvlId);

    		return $this->sendSuccessResponse("Education Level Details Found", $view);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level Details Not Found');
		}
    }

    public function getEducationSystemDropdown()
    {
    	try{
    		$systemDrpdwn = $this->educationLevelRepository->educationSystemDropdown();

    		return $this->sendSuccessResponse("Education System Dropdown Found", $systemDrpdwn);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education System Dropdown Not Found');
		}
    }

    public function getEducationLevelIscedDropdown()
    {
    	try{
    		$systemDrpdwn = $this->educationLevelRepository->educationLevelIscedDropdown();

    		return $this->sendSuccessResponse("Education Level Isced Dropdown Found", $systemDrpdwn);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level Isced Dropdown Not Found');
		}
    }

    public function storeAdministrationSystemSetupEducationLevel(EducationLevelRequest $request)
    {
    	try{
    		$data = $request->all();
    		$store = $this->educationLevelRepository->addEducationLevel($data);

    		return $this->sendSuccessResponse("Education Level Added Successfully", $store);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level not added');
		}
    }

    public function updateAdministrationSystemSetupEducationLevel(int $eduLevelId, EducationLevelRequest $request)
    {
    	try{
    		$rawData = $request->all();
    		$update = $this->educationLevelRepository->updateEducationLevel($eduLevelId, $rawData);

    		return $this->sendSuccessResponse("Education Level Updated Successfully", $update);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Education Level not Updated');
		}
    }

    public function destroyAdministrationSystemSetupEducationLevel(int $eduLevelId)
    {
        try{
            $job = new DeleteEducationLevel($eduLevelId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Education Level Deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Education Level  Can Not Deleted");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Education Level  Can Not Deleted');
        }
    }
}
