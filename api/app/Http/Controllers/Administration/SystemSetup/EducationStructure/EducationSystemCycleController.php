<?php

namespace App\Http\Controllers\Administration\SystemSetup\EducationStructure;

use App\Http\Controllers\Controller;
use App\Http\Requests\EducationStructurAddCycleRequest;
use App\Repositories\EducationSystemCycleRepository;
use App\Services\Administration\SystemSetup\EducationStructure\EducationSystemCycleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EducationSystemCycleController extends Controller
{
    /**
     * @var EducationSystemCycleRepository
     */
    protected $educationSystemCycleRepository;

    /**
     * @var EducationSystemCycleService
     */
    protected $educationSystemCycleService;

    /**
     * EducationSystemCycleController constructor.
     * @param EducationSystemCycleService $educationSystemCycleService
     * @param EducationSystemCycleRepository $educationSystemCycleRepository
     */

    public function __construct(
        EducationSystemCycleService $educationSystemCycleService,
        EducationSystemCycleRepository $educationSystemCycleRepository

    ) {
        $this->educationSystemCycleService = $educationSystemCycleService;
        $this->educationSystemCycleRepository = $educationSystemCycleRepository;
    }

    /**
     * @param Request $request
     * @param string $educationlevelId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationSystemCycleList(Request $request, string $educationlevelId)
    {
        try {
            $listing = $this->educationSystemCycleService->getCycleListing($request, $educationlevelId);
            if (!empty($listing)) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['Cycle List' => $listing]]
                );

                return $this->sendSuccessResponse("Cycle List Found", $listing);
            } else {
                return $this->sendSuccessResponse("Cycle List not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Education Cycle List Not Found");
        }
    }

    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationSystemCycleView(string $eduCycleId)
    {
        try {
            $viewdetails = $this->educationSystemCycleService->getCycleView($eduCycleId);
            return $this->sendSuccessResponse("Cycle View Found", $viewdetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Cycle View Not Found");
        }
    }

    /**
     * add cycle in to db
     * @param EducationStructurAddCycleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationStructureCycleAdd(EducationStructurAddCycleRequest $request)
    {
        try {
            $addcycle = $this->educationSystemCycleRepository->educationCycleAdd($request);
                 Log::info(
                     'added details into DB',
                     ['method' => __METHOD__, 'data' => ['Cycleadded' => $addcycle]]
                 );
            return $this->sendSuccessResponse("Cycle added Successfully", $addcycle);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Cycle Not Added");
        }
    }

    /**
     * @param EducationStructurAddCycleRequest $request
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationStructureCycleUpdate(EducationStructurAddCycleRequest $request, string $eduCycleId)
    {
        try {
            $updatecycle = $this->educationSystemCycleRepository->educationCycleUpdate($request, $eduCycleId);
            Log::info(
                'updated details into DB',
                ['method' => __METHOD__, 'data' => ['CycleUpdated' => $updatecycle]]
            );
            return $this->sendSuccessResponse("Education Cycle Updated Successfully", $updatecycle);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Cycle Not Updated");
        }
    }
    /**
     * @param string $eduCycleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationSystemCycleEdit(string $eduCycleId)
    {
        try {
            $viewdetails = $this->educationSystemCycleService->getCycleEdit($eduCycleId);
            return $this->sendSuccessResponse("Cycle View Found", $viewdetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Education Cycle View Not Found");
        }
    }
}
