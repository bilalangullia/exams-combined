<?php

namespace App\Http\Controllers\Administration\SystemSetup\FieldOptions;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommonUpdateRequest;
use App\Http\Requests\CommonAddRequest;
use App\Http\Requests\DeleteRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\AttendanceTypeRepository;
use App\Services\Administration\SystemSetup\FieldOptions\AttendanceTypeService;
use App\Http\Requests\FieldOptionListRequest;
use App\Http\Requests\FieldOptionViewRequest;
use App\Jobs\DeleteAttendanceType;

class AttendanceTypeController extends Controller
{
    protected $attendanceTypeRepository;
    protected $attendanceTypeService;

    public function __construct(
        AttendanceTypeRepository $attendanceTypeRepository,
        AttendanceTypeService $attendanceTypeService
    )
    {
        $this->attendanceTypeRepository = $attendanceTypeRepository;
        $this->attendanceTypeService = $attendanceTypeService;
    }

    /**
     * Getting Field Option List
     * @param FieldOptionListRequest $request
     * @return JsonResponse
     */
    public function getFieldOptionListing(FieldOptionListRequest $request)
    {
    	try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
    		$list = $this->attendanceTypeService->getListing($data);

    		Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('List found', $list);
            } else {
                return $this->sendSuccessResponse('List not found', $list);
            }
    	} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('List Not Found');
        }
    }

    /**
     * Getting Field Option Detail
     * @param FieldOptionViewRequest $request
     * @param int $optionId
     * @return JsonResponse
     */
    public function getFieldOptionDetail(FieldOptionViewRequest $request, int $optionId)
    {
        try{
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $detail = $this->attendanceTypeService->getFieldOptionDetail($data, $optionId);

            Log::info('Fetched detail from DB', ['method' => __METHOD__, 'data' => ['detail' => $detail]]);

            if (count($detail) > 0) {
                return $this->sendSuccessResponse('Details found', $detail);
            } else {
                return $this->sendSuccessResponse('Details not found', $detail);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch detail from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Field option details not found');
        }
    }

    /**
     * Storing Field Option Detail
     * @param CommonAddRequest $request
     * @return JsonResponse
     */
    public function storeFieldOption(CommonAddRequest $request)
    {
        try{
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $store = $this->attendanceTypeRepository->storeFieldOption($data);
            if ($store == 1) {
                Log::info("Details stored in DB", ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Details stored in DB");
            } else {
                Log::info("Details not stored in DB", ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Details not stored in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store field option in DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to store field option in DB');
        }
    }

    /**
     * Updating Field Option Detail
     * @param CommonUpdateRequest $request
     * @return JsonResponse
     */
    public function updateFieldOption(CommonUpdateRequest $request)
    {
        try{
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $module = $data['module']??"";
            $store = $this->attendanceTypeRepository->updateFieldOption($data);
            if ($store == 1) {
                Log::info("Field Options details updated in DB", ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Field Options details updated in DB");
            } else {
                Log::info("Field Options details not updated in DB", ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Field Options details not updated in DB");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update field option in DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to update field option in DB');
        }
    }

    public function destroyFieldOption(DeleteRequest $request)
    {
        try {
            $data = $request->all();
            $job = new DeleteAttendanceType($data, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Record deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Record can not be deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Record not deleted.");
        }
    }
}
