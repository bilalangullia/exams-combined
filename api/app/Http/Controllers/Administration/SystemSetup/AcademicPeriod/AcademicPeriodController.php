<?php

namespace App\Http\Controllers\Administration\SystemSetup\AcademicPeriod;

use App\Http\Controllers\Controller;
use App\Http\Requests\AcademicPeriodRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\AcademicPeriodRepository;
use App\Services\Administration\SystemSetup\AcademicPeriod\AcademicPeriodService;
use App\Jobs\DeleteAcademicPeriod;

class AcademicPeriodController extends Controller
{
    protected $academicPeriodRepository;
    protected $academicPeriodService;

    public function __construct(
        AcademicPeriodRepository $academicPeriodRepository,
        AcademicPeriodService $academicPeriodService
    )
    {
        $this->academicPeriodRepository = $academicPeriodRepository;
        $this->academicPeriodService = $academicPeriodService;
    }

    public function getSystemSetupAcademicPeriodList(Request $request)
    {
    	try{
    		$list = $this->academicPeriodService->getAcademicPeriodList($request);

    		return $this->sendSuccessResponse('Academic Period List Found', $list);
    	} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period List Not Found');
        }
    }

    public function getSystemSetupAcademicPeriodDetail(int $academicPeriodId)
    {
        try{
            $result = $this->academicPeriodService->getAcademicPeriodDetail($academicPeriodId);

            return $this->sendSuccessResponse("Academic Period Detail Found", $result);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Detail Not Found');
        }
    }

    public function academicPeriodLevelDropdown()
    {
        try{
            $dropdown = $this->academicPeriodRepository->academicPeriodLevelDropdown();

            return $this->sendSuccessResponse('Academic Period Level Drop Down Found' ,$dropdown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level Drop Down Not Found');
        }
    }

    public function addSystemSetupAcademicPeriod(AcademicPeriodRequest $request)
    {
        try{
            $requestData = $request->all();
            $addData = $this->academicPeriodRepository->addAcademicPeriod($requestData);

            return $this->sendSuccessResponse('Academic Period Added Successfully!', $addData);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Added');
        }
    }


    public function updateSystemSetupAcademicPeriod(int $academicPeriodId, AcademicPeriodRequest $request)
    {
        try{
            $data = $request->all();
            $updateData = $this->academicPeriodRepository->updateAcademicPeriod($academicPeriodId, $data);

            return $this->sendSuccessResponse('Academic Period Updated Successfully!', $updateData);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Updated');
        }
    }

    public function destroySystemSetupAcademicPeriod(int $academicPeriodId)
    {
        try {
            $job = new DeleteAcademicPeriod($academicPeriodId, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Academic Period  Deleted Successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Academic Period Can Not Be Deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Record not deleted.");
        }
    }
}
