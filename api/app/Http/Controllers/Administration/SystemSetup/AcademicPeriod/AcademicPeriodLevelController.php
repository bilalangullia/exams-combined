<?php

namespace App\Http\Controllers\Administration\SystemSetup\AcademicPeriod;

use App\Http\Controllers\Controller;
use App\Http\Requests\AcademicPeriodLevelRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\AcademicPeriodLevelRepository;
use App\Services\Administration\SystemSetup\AcademicPeriod\AcademicPeriodLevelService;
use App\Jobs\DeleteAcademicLevel;

class AcademicPeriodLevelController extends Controller
{
    protected $academicPeriodLevelRepository;
    protected $academicPeriodLevelService;

    public function __construct(
        AcademicPeriodLevelRepository $academicPeriodLevelRepository,
        AcademicPeriodLevelService $academicPeriodLevelService
    )
    {
        $this->academicPeriodLevelRepository = $academicPeriodLevelRepository;
        $this->academicPeriodLevelService = $academicPeriodLevelService;
    }

    public function getSystemSetupAcademicPeriodLevelList(Request $request)
    {
    	try{
    		$list = $this->academicPeriodLevelService->getAcademicPeriodLevelList($request);

    		return $this->sendSuccessResponse('Academic Period Level List Found', $list);
    	} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level List Not Found');
        }
    }

    public function getSystemSetupAcademicPeriodLevelDetail(int $academicLevel)
    {
    	try{
    		$detail = $this->academicPeriodLevelService->getAcademicPeriodLevelDetail($academicLevel);

    		return $this->sendSuccessResponse('Academic Period Level Detail Found', $detail);
    	} catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Level Detail Not Found');
        }
    }

    public function storeSystemSetupAcademicPeriodLevel(AcademicPeriodLevelRequest $request)
    {
        try{
            $data = $request->all();
            $store  =$this->academicPeriodLevelRepository->addAcademicPeriodLevel($data);

            return $this->sendSuccessResponse('Academic Period Level Added Successfully', $store);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Added');
        }
    }

    public function updateSystemSetupAcademicPeriodLevel(int $academicLevel, AcademicPeriodLevelRequest $request)
    {
        try{
            $updateData = $request->all();
            $store  =$this->academicPeriodLevelRepository->updateAcademicPeriodLevel($academicLevel, $updateData);

            return $this->sendSuccessResponse('Academic Period Level Updated Successfully', $store);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Academic Period Not Updated');
        }
    }

    public function destroySystemSetupAcademicPeriodLevel(int $academicLevel)
    {
        try {
            $job = new DeleteAcademicLevel($academicLevel, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Academic Period Level Deleted Successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Academic Period Level Can Not Be Deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Record not deleted.");
        }
    }
}
