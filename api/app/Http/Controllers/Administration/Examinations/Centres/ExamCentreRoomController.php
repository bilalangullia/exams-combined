<?php

namespace App\Http\Controllers\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteExamCentreRoom;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Administration\Examinations\Centres\ExamCentreRoomService;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\ExamCentreRoomUpdateRequest;
use App\Http\Requests\ExamCentreRoomStoreRequest;
use App\Repositories\ExamCentreRoomRepository;

class ExamCentreRoomController extends Controller
{
    /**
     * ExamCentreRoomService
     * @var ExamCentreRoomService
     */
    protected $examCentreRoomService;

    /**
     * ExamCentreRoomRepository
     * @var ExamCentreRoomRepository
     */
    protected $examCentreRoomRepository;

    /**
     * ExamCentreRoomController constructor.
     * @param ExamCentreRoomService $examCentreRoomService
     * @param ExamCentreRoomRepository $examCentreRoomRepository
     */
    public function __construct(
        ExamCentreRoomService $examCentreRoomService,
        ExamCentreRoomRepository $examCentreRoomRepository
    ) {
        $this->examCentreRoomService = $examCentreRoomService;
        $this->examCentreRoomRepository = $examCentreRoomRepository;
    }

    /**
     * Getting Exam centre rooms list
     * @param Request $request
     * @param string $examCentreId
     * @return JsonResponse
     */
    public function getExamCentreRoomList(Request $request, string $examCentreId)
    {
        try {
            $search = $request->all();
            Log::info('Request Parameters Received', [
                'method' => __METHOD__,
                'data' => ['data' => $search]
            ]);

            $rooms = $this->examCentreRoomService->getExamCentreRoomList($search, $examCentreId);

            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['rooms' => $rooms]]);

            if (count($rooms) > 0) {
                return $this->sendSuccessResponse('Exam centre rooms list found', $rooms);
            } else {
                return $this->sendErrorResponse('Exam centre rooms list not found', $rooms);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room list not found');
        }
    }

    /**
     * Getting Exam centre rooms details
     * @param string $roomId
     * @return JsonResponse
     */
    public function getExamCentreRoomDetails(string $roomId)
    {
        try {
            Log::info('Request Parameters Received', [
                'method' => __METHOD__,
                'data' => ['roomId' => $roomId]
            ]);

            $roomDetails = $this->examCentreRoomService->getExamCentreRoomDetails($roomId);

            Log::info(
                'Fetched room details from DB',
                ['method' => __METHOD__, 'data' => ['roomDetails' => $roomDetails]]
            );
            if (count($roomDetails) > 0) {
                return $this->sendSuccessResponse('Exam centre rooms details found', $roomDetails);
            } else {
                return $this->sendErrorResponse('Exam centre rooms details not found');
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room details not found');
        }
    }

    /**
     * Updating Exam Centre Room Details
     * @param ExamCentreRoomUpdateRequest $request
     * @return JsonResponse
     */
    public function examCentreRoomUpdate(ExamCentreRoomUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received', [
                'method' => __METHOD__,
                'data' => ['roomData' => $data]
            ]);

            $update = $this->examCentreRoomRepository->examCentreRoomUpdate($data);

            Log::info('Exam centre room data updated in DB', [
                'method' => __METHOD__,
                'data' => ['roomData' => $data]
            ]);

            if ($update == 1) {
                return $this->sendSuccessResponse('Exam centre room details updated');
            } else {
                return $this->sendErrorResponse('Exam centre room details not updated');
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room details not updated');
        }
    }

    /**
     * Storing exam centre rooms
     * @param ExamCentreRoomStoreRequest $request
     * @return JsonResponse
     */
    public function examCentreRoomStore(ExamCentreRoomStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received', [
                'method' => __METHOD__,
                'data' => ['roomData' => $data]
            ]);
            $store = $this->examCentreRoomRepository->examCentreRoomStore($data);
            if ($store == 1) {
                return $this->sendSuccessResponse('Exam centre room details stored');
            } else {
                return $this->sendErrorResponse('Exam centre room details not stored');
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Exam centre room details not stored');
        }
    }

    /**
     * Deleting exam centre room
     * @param int $roomId
     * @return JsonResponse
     */
    public function examCentreRoomDelete(int $roomId)
    {
        try {
            $job = new DeleteExamCentreRoom($roomId);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Exam centre room deleted successfully.");
            } else {
                return $this->sendErrorResponse("Exam centre room not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre room from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to delete exam centre room from DB');
        }
    }
}
