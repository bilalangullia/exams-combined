<?php

namespace App\Http\Controllers\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Administration\Examinations\Centres\ExamCentreInvigilatorService;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\ExamCentreInvigilatorStoreRequest;
use App\Repositories\ExamCentreInvigilatorRepository;
use App\Jobs\DeleteExamCentreInvigilator;

class ExamCentreInvigilatorController extends Controller
{
    /**
     * ExamCentreInvigilatorService
     * @var ExamCentreInvigilatorService
     */
    protected $examCentreInvigilatorService;

    /**
     * ExamCentreInvigilatorRepository
     * @var ExamCentreInvigilatorRepository
     */
    protected $examCentreInvigilatorRepository;

    /**
     * ExamCentreInvigilatorController constructor.
     * @param ExamCentreInvigilatorService $examCentreInvigilatorService
     */
    public function __construct(
        ExamCentreInvigilatorService $examCentreInvigilatorService,
        ExamCentreInvigilatorRepository $examCentreInvigilatorRepository
    ) {
        $this->examCentreInvigilatorService = $examCentreInvigilatorService;
        $this->examCentreInvigilatorRepository = $examCentreInvigilatorRepository;
    }

    /**
     * Getting Exam Centre Invigilator List.
     * @param Request $request
     * @param string $examCentreId
     * @return JsonResponse
     */
    public function examCentreInvigilatorList(Request $request, string $examCentreId)
    {
        try {
            $search = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $search]]);

            $invigilators = $this->examCentreInvigilatorService->examCentreInvigilatorList($search, $examCentreId);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['invigilators' => $invigilators]]);

            if ($invigilators) {
                return $this->sendSuccessResponse("Exam Centre Invigilator List Found", $invigilators);
            } else {
                return $this->sendErrorResponse("Exam Centre Invigilator List Not Found", $invigilators);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam Centres Invigilators Not Found");
        }
    }

    /**
     * Getting Exam Centre Invigilator List.
     * @param string $examCentreId
     * @param $invigilatorId
     * @return JsonResponse
     */
    public function getExamCentreInvigilatorDetails(string $examCentreId, $invigilatorId)
    {
        try {
            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'examCentreId' => $examCentreId,
                        'invigilatorId' => $invigilatorId
                    ]
                ]
            );
            $invigilator = $this->examCentreInvigilatorService
                ->getExamCentreInvigilatorDetails($examCentreId, $invigilatorId);
            Log::info('Fetched details from DB', ['method' => __METHOD__, 'data' => ['invigilator' => $invigilator]]);

            if ($invigilator) {
                return $this->sendSuccessResponse("Exam Centre Invigilator Details Found", $invigilator);
            } else {
                return $this->sendErrorResponse("Exam Centre Invigilator Details Not Found", $invigilator);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam centre invigilator details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Found");
        }
    }

    /**
     * Getting Exam Centre Invigilator Dropdown
     * @param Request $request
     * @return JsonResponse
     */
    public function getInvigilatorDropdown(Request $request)
    {
        try {
            $search = $request->all();
            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'data' => $search
                    ]
                ]
            );
            $invigilators = $this->examCentreInvigilatorService->getInvigilatorDropdown($search);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['invigilators' => $invigilators]]);
            if (count($invigilators) > 0) {
                return $this->sendSuccessResponse("Exam Centre Invigilator Dropdown Found", $invigilators);
            } else {
                return $this->sendErrorResponse("Exam Centre Invigilator Dropdown Not Found", $invigilators);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch invigilator dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Found");
        }
    }

    /**
     * Storing Exam Centre Invigilator.
     * @param ExamCentreInvigilatorStoreRequest $request
     * @return JsonResponse
     */
    public function storeExamCentreInvigilator(ExamCentreInvigilatorStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'data' => $data
                    ]
                ]
            );
            $store = $this->examCentreInvigilatorRepository->storeExamCentreInvigilator($data);
            if ($store == 1) {
                return $this->sendSuccessResponse("Exam Centre Invigilator Added Successfully.");
            } elseif ($store == 2) {
                return $this->sendSuccessResponse("Exam Centre Invigilator already added to this exam centre.");
            } else {
                return $this->sendErrorResponse("Exam Centre Invigilator Not Added");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store invigilator in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Invigilator Not Stored");
        }
    }

    /**
     * Deleting Invigilators
     * @param Request $request
     * @return JsonResponse
     */
    public function examCentreInvigilatorDelete(Request $request)
    {
        try {
            $invigilatorId = $request->invigilatorId;
            $examCentreId = $request->examCentreId;
            $job = new DeleteExamCentreInvigilator($invigilatorId, $examCentreId);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Exam centre invigilator deleted successfully.");
            } else {
                return $this->sendErrorResponse("Exam centre invigilator not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete invigilator in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to delete invigilator in DB");
        }
    }
}
