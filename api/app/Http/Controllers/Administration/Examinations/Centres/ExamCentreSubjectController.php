<?php

namespace App\Http\Controllers\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Administration\Examinations\Centres\ExamCentreSubjectService;
use Illuminate\Support\Facades\Log;

class ExamCentreSubjectController extends Controller
{
    /**
     * ExamCentreSubjectService
     * @var ExamCentreSubjectService
     */
    protected $examCentreSubjectService;

    /**
     * ExamCentreSubjectController constructor.
     * @param ExamCentreSubjectService $examCentreSubjectService
     */
    public function __construct(ExamCentreSubjectService $examCentreSubjectService)
    {
        $this->examCentreSubjectService = $examCentreSubjectService;
    }

    /**
     * Getting Subject List.
     * @param Request $request
     * @param string $examinationId
     * @return JsonResponse
     */
    public function getExamCentreSubjectList(Request $request, string $examinationId)
    {
        try {
            $search = $request->all();
            $subjects = $this->examCentreSubjectService->getExamCentreSubjectList($search, $examinationId);
            if (count($subjects) > 0) {
                return $this->sendSuccessResponse("Exam Centre Subject List Found", $subjects);
            } else {
                return $this->sendErrorResponse("Exam Centre Subject List Not Found", $subjects);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subjects list in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Subjects List Not Found");
        }
    }

    /**
     * Getting Subject Details
     * @param string $examCentreId
     * @param $subjectId
     * @return JsonResponse
     */
    public function examCentreSubjectView(string $examCentreId, $subjectId)
    {
        try {
            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'examCentreId' => $examCentreId,
                        'subjectId' => $subjectId
                    ]
                ]
            );

            $subject = $this->examCentreSubjectService->examCentreSubjectView($examCentreId, $subjectId);

            Log::info('Fetched details from DB', ['method' => __METHOD__, 'data' => ['subject' => $subject]]);
            if (count($subject) > 0) {
                return $this->sendSuccessResponse("Exam Centre Subject Details Found", $subject);
            } else {
                return $this->sendErrorResponse("Exam Centre Subject Details Not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch subjects details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Subjects Details Not Found");
        }
    }
}
