<?php

namespace App\Http\Controllers\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Administration\Examinations\Centres\ExamCentreStudentService;
use Illuminate\Support\Facades\Log;

class ExamCentreStudentController extends Controller
{
    /**
     * ExamCentreStudentService
     * @var ExamCentreStudentService
     */
    protected $examCentreStudentService;

    /**
     * ExamCentreStudentController constructor.
     * @param ExamCentreStudentService $examCentreStudentService
     */
    public function __construct(ExamCentreStudentService $examCentreStudentService)
    {
        $this->examCentreStudentService = $examCentreStudentService;
    }

    /**
     * Getting Exam Centre Student List
     * @param Request $request
     * @param string $examCentreId
     * @param $examinationId
     * @return JsonResponse
     */
    public function getExamCentreStudentList(Request $request, string $examCentreId, $examinationId)
    {
        try {
            $search = $request->all();
            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'data' => $search,
                        'examCentreId' => $examCentreId,
                        'examinationId' => $examinationId
                    ]
                ]
            );
            $students = $this->examCentreStudentService
                        ->getExamCentreStudentList($search, $examCentreId, $examinationId);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['students' => $students]]);
            if (count($students) > 0) {
                return $this->sendSuccessResponse("Exam Centre Students List Found", $students);
            } else {
                return $this->sendErrorResponse("Exam Centre Students List Not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch student list in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Student List Not Found");
        }
    }

    /**
     * Getting Exam Centre Student Details
     * @param string $studentId
     * @return JsonResponse
     */
    public function examCentreStudentDetails(string $studentId)
    {
        try {
            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'studentId' => $studentId
                    ]
                ]
            );
            $student = $this->examCentreStudentService->examCentreStudentDetails($studentId);
            Log::info('Fetched details from DB', ['method' => __METHOD__, 'data' => ['student' => $student]]);
            if (count($student) > 0) {
                return $this->sendSuccessResponse("Exam Centre Student Details Found", $student);
            } else {
                return $this->sendErrorResponse("Exam Centre Student Details Not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch student details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Student Details Not Found");
        }
    }
}
