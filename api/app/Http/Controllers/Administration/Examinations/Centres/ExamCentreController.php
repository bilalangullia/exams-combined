<?php

namespace App\Http\Controllers\Administration\Examinations\Centres;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteExamCentre;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Administration\Examinations\Centres\ExamCentreService;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\ExamCentreUpdateRequest;
use App\Http\Requests\ExamCentreListRequest;
use App\Http\Requests\ExamCentreStoreRequest;
use App\Http\Requests\ExamCentreCopyRequest;
use App\Http\Requests\ExaminationCentreImportRequest;
use App\Repositories\ExamCentreRepository;
use File;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ExaminationCentreExcelImport;

class ExamCentreController extends Controller
{

    /**
     * ExamCentreService
     * @var $examCentreService
     */
    protected $examCentreService;

    /**getExamCentreList
     * ExamCentreRepository
     * @var $examCentreRepository
     */
    protected $examCentreRepository;

    /**
     * ExamCentreController constructor.
     * @param ExamCentreService $examCentreService
     * @param ExamCentreRepository $examCentreRepository
     */
    public function __construct(ExamCentreService $examCentreService, ExamCentreRepository $examCentreRepository)
    {
        $this->examCentreService = $examCentreService;
        $this->examCentreRepository = $examCentreRepository;
    }

    /**
     * Getting Exam Centre List
     * @param Request $request
     * @return JsonResponse
     */
    public function getExamCentreList(ExamCentreListRequest $request)
    {
        try {
            $search = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $search]]);
            $examCentres = $this->examCentreService->getExamCentreList($search);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['examCentres' => $examCentres]]);

            return $this->sendSuccessResponse("Exam Centre List Found", $examCentres);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam Centres Not Found");
        }
    }

    /**
     * Getting exam centre overview
     * @param int $examCentreId
     * @return array|JsonResponse
     */
    public function examCentreOverview(int $examCentreId)
    {
        try {
            Log::info('Request Parameters Received', [
                'method' => __METHOD__,
                'data' => ['examCentreId' => $examCentreId]
            ]);
            $examCentreOverview = $this->examCentreService->examCentreOverview($examCentreId);
            Log::info('Fetched exam centre overview from DB', [
                'method' => __METHOD__,
                'data' => ['examCentreOverview' => $examCentreOverview]
            ]);

            return $this->sendSuccessResponse("Exam Centre Overview Found", $examCentreOverview);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch exam center overview from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centres Overview Not Found");
        }
    }

    /**
     * Updating Exam centre code
     * @param ExamCentreUpdateRequest $request
     * @return JsonResponse
     */
    public function examCentreUpdate(ExamCentreUpdateRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received', [
                'method' => __METHOD__,
                'data' => ['examCentreData' => $data]
            ]);

            $update = $this->examCentreRepository->examCentreUpdate($data);

            Log::info('Exam centre data updated in DB', [
                'method' => __METHOD__,
                'data' => ['examCentreData' => $data]
            ]);

            if ($update == 1) {
                return $this->sendSuccessResponse('Exam centre data updated successfully');
            } else {
                return $this->sendErrorResponse('Exam centre details not updated');
            }

        } catch (\Exception $e) {
            Log::error(
                'Failed to update exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam centre data not updated.");
        }
    }

    /**
     * Deleteing Exam Centres
     * @param int $examCentreId
     * @return JsonResponse
     */
    public function examCentreDelete(int $examCentreId)
    {
        try {
            $job = new DeleteExamCentre($examCentreId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Exam centre deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Exam centre can not be deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete exam centre.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam centre not deleted.");
        }
    }

    /**
     * Storing Exam Centre
     * @param ExamCentreStoreRequest $request
     * @return JsonResponse
     */
    public function examCentreStore(ExamCentreStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $store = $this->examCentreRepository->examCentreStore($data);
            Log::info('Exam Centre Details Stored', ['method' => __METHOD__, 'data' => ['store' => $store]]);
            if ($store == 1) {
                return $this->sendSuccessResponse('Exam centre data stored successfully');
            } else {
                return $this->sendErrorResponse("Exam centre not stored.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to store exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam centre data not stored.");
        }
    }

    /**
     * Copying Exam Centre
     * @param ExamCentreCopyRequest $request
     * @return JsonResponse
     */
    public function examCentreCopy(ExamCentreCopyRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $copy = $this->examCentreRepository->examCentreCopy($data);

            if ($copy == 1) {
                Log::info('Exam Centre Details Copied', ['method' => __METHOD__]);
                return $this->sendSuccessResponse('Exam centre data copied successfully');
            } else {
                Log::info('Exam Centre Details Not Copied', ['method' => __METHOD__]);
                return $this->sendErrorResponse('Exam centre details not copied');
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to copy exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to copy exam centre data in DB");
        }
    }

    /**
     * Importing Examination Centre
     * @param ExaminationCentreImportRequest $request
     * @return JsonResponse
     */
    public function examCentreImport(ExaminationCentreImportRequest $request)
    {
        try {
            $requestParameter = $request->all();
            $validExtensions = config('constants.fileExt.excel');

            $extension = File::extension($request->template->getClientOriginalName());

            if (!in_array($extension, $validExtensions)) {
                return $this->sendErrorResponse('Not a valid file extension');
            }
            $header = config('constants.examinationCentre.header');
            
            $excel = Excel::toArray(new ExaminationCentreExcelImport(), $request->file('template'));
            
            if (empty($excel[0][1])) {
                return $this->sendErrorResponse("Header is not present");
            }
            if (empty($excel[0][2])) {
                return $this->sendErrorResponse("Imported file is empty.");
            }
            
            if (!in_array($excel[0][1][0], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][1], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][2], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][3], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][4], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][5], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][6], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][7], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][8], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            $rowsCount = count($excel[0]) - 2;

            $maxRows = config('constants.examinationCentre.maxRows');
            
            if ($rowsCount > $maxRows) {
                return $this->sendErrorResponse("File can not have than " . $maxRows . " records");
            }
            $import = $this->examCentreRepository->examCentreImport($excel, $requestParameter);
            
            Log::info('Examination Centre Data Imported in DB.', ['method' => __METHOD__, 'data' => ['import' => $import]]);

            if (count($import) > 0) {
                return $this->sendSuccessResponse("Examination Centre Data Imported in DB.", $import);
            } else {
                return $this->sendErrorResponse("Examination Centre Data not Imported in DB.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to import exam centre data in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import exam centre data in DB");
        }
    }
}
