<?php

namespace App\Http\Controllers\Administration\Examinations;

use App\Http\Controllers\Controller;
use App\Repositories\GradingTypeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Administration\Examinations\GradingTypeService;
use Exception;
use App\Http\Requests\GradingTypeRequest;
use App\Http\Requests\GradingTypeStoreRequest;
use App\Http\Requests\GradingTypeImportRequest;
use Illuminate\Support\Facades\Log;
use App\Jobs\DeleteGradingType;
use File;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GradingTypeExcelImport;

class GradingTypeController extends Controller
{
    /**
     * GradingTypeService instance
     * @var GradingTypeService
     */
    protected $gradingTypeService;

    /**
     * GradingTypeRepository instance
     * @var GradingTypeRepository
     */
    protected $gradingTypeRepository;

    /***
     * GradingTypeController constructor.
     * @param GradingTypeService $gradingTypeService
     * @param GradingTypeRepository $gradingTypeRepository
     */
    public function __construct(GradingTypeService $gradingTypeService, GradingTypeRepository $gradingTypeRepository)
    {
        $this->gradingTypeService = $gradingTypeService;
        $this->gradingTypeRepository = $gradingTypeRepository;
    }

    /**
     * Grading type listing
     * @param Request $request
     * @return JsonResponse
     */
    public function getGradingTypeList(Request $request)
    {
        try {
            $search = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $search]]);

            $gradingType = $this->gradingTypeService->getGradingTypeList($search);

            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['gradingType' => $gradingType]]);

            if (count($gradingType) > 0) {
                return $this->sendSuccessResponse("Grading Type List Found", $gradingType);
            } else {
                return $this->sendErrorResponse("Grading Type List Not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }

    /**
     * Grading Type Details
     * @param int $gradingTypeId
     * @return JsonResponse
     */
    public function getGradingTypeDetails(int $gradingTypeId)
    {
        try {
            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'gradingTypeId' => $gradingTypeId
                    ]
                ]
            );
            $gradingTypeDetails = $this->gradingTypeService->getGradingTypeDetails($gradingTypeId);
            Log::info(
                'Fetched grading type details from DB',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'gradingTypeDetails' => $gradingTypeDetails
                    ]
                ]
            );
            return $this->sendSuccessResponse("Grading Type Details Found", $gradingTypeDetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch grading type details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }

    /**
     * Grading type details update
     * @param GradingTypeRequest $request
     * @return JsonResponse
     */
    public function getGradingTypeUpdate(GradingTypeRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $update = $this->gradingTypeService->updateGradingType($data);

            if ($update == 1) {
                Log::info('Grading type details updated in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Grading Type Details Updated");
            } else {
                Log::info('Grading type details not updated in DB', ['method' => __METHOD__]);
                return $this->sendErrorResponse('Grading Type Details Not Updated');
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch grading type update in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }

    /**
     * Grading type delete
     * @param int $gradingTypeId
     * @return JsonResponse
     */
    public function gradingTypeDelete(int $gradingTypeId)
    {
        try {
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['id' => $gradingTypeId]]);
            $job = new DeleteGradingType($gradingTypeId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            
            if ($response == 1) {
                return $this->sendSuccessResponse("Grading Type deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Grading Type not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete grading type from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Something went wrong.');
        }
    }

    /**
     * Storing Grading type details
     * @param GradingTypeStoreRequest $request
     * @return JsonResponse
     */
    public function gradingTypeStore(GradingTypeStoreRequest $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);
            $store = $this->gradingTypeRepository->gradingTypeStore($data);
            if ($store == 1) {
                Log::info('Grading type details stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Grading type details stored in DB");
            } else {
                Log::info('Grading type details not stored in DB', ['method' => __METHOD__]);
                return $this->sendSuccessResponse("Grading type details not stored in DB");
            }
        } catch(\Exception $e) {
            Log::error(
                'Failed to store grading type in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to store grading type in DB');
        }
    }

    /**
     * Importing Grading options
     * @param GradingTypeImportRequest $request
     * @return JsonResponse
     */
    public function gradingTypeImport(GradingTypeImportRequest $request)
    {
        try {
            $requestParameter = $request->all();

            $validExtensions = config('constants.fileExt.excel');

            $extension = File::extension($request->template->getClientOriginalName());

            if (!in_array($extension, $validExtensions)) {
                return $this->sendErrorResponse('Not a valid file extension');
            }
            $header = config('constants.gradingType.header');
            
            $excel = Excel::toArray(new GradingTypeExcelImport(), $request->file('template'));
            
            if (empty($excel[0][1])) {
                return $this->sendErrorResponse("Header is not present");
            }
            if (empty($excel[0][2])) {
                return $this->sendErrorResponse("Imported file is empty.");
            }
            
            if (!in_array($excel[0][1][0], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][1], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][2], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][3], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][4], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][5], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            $rowsCount = count($excel[0]) - 2;
            $maxRows = config('constants.gradingType.maxRows');
            
            if ($rowsCount > $maxRows) {
                return $this->sendErrorResponse("File can not have than " . $maxRows . " records");
            }
            $import = $this->gradingTypeRepository->gradingTypeImport($excel, $requestParameter);
            
            Log::info('Grading Type Data Imported in DB.', ['method' => __METHOD__, 'data' => ['import' => $import]]);

            if (count($import) > 0) {
                return $this->sendSuccessResponse("Grading Type Data Imported in DB.", $import);
            } else {
                return $this->sendErrorResponse("Grading Type Data not Imported in DB.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to import grading type option in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse('Failed to import grading type option in DB');
        }
    }
}
