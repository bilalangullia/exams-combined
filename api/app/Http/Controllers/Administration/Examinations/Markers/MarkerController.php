<?php

namespace App\Http\Controllers\Administration\Examinations\Markers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExaminationMarkerRequest;
use App\Jobs\DeleteExamMarker;
use App\Repositories\MarkerRepository;
use App\Services\Administration\Examinations\Markers\MarkerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Response;

class MarkerController extends Controller
{
    /**
     * @var MarkerRepository
     */
    protected $markerRepository;

    /**
     * @var MarkerService
     */
    protected $markerService;

    /**
     * MarkerController constructor.
     * @param MarkerService $markerService
     * @param MarkerRepository $markerRepository
     */

    public function __construct(
        MarkerService $markerService,
        MarkerRepository $markerRepository
    ) {
        $this->markerService = $markerService;
        $this->markerRepository = $markerRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationMarkerListing(Request $request)
    {
        try {
            $list = $this->markerService->listingMarker($request);
            if (empty($list)) {
                return $this->sendErrorResponse(" Marker Listing Not Found");
            }
            return $this->sendSuccessResponse(" Marker List Found", $list);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Marker list not found");
        }
    }

    /**
     * @param string $openemisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationMarkerView(string $openemisno)
    {
        try {
            $markerview = $this->markerService->getMarkerView($openemisno);
            return $this->sendSuccessResponse("Marker view Found", $markerview);
        } catch (\Exception $e) {
            Log::error(
                'Failed to Marker view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Marker View Not Found");
        }
    }

    /**
     * @param string $openemisno
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationMarkerEdit(string $openemisno)
    {
        try {
            $markerview = $this->markerService->getMarkerEdit($openemisno);
            return $this->sendSuccessResponse("Marker Found", $markerview);
        } catch (\Exception $e) {
            Log::error(
                'Failed to Marker edit view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Marker Not Found");
        }
    }

    /**
     * @param string $examinerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationAutocompleteExaminerId(string $examinerId)
    {
        try {
            $autocompleteId = $this->markerRepository->autocompleteExaminerId($examinerId);
            if (empty($autocompleteId)) {
                return $this->sendErrorResponse("Examiner Autocomplete Not Found");
            }
            return $this->sendSuccessResponse("Examiner  Autocomplete list", $autocompleteId);
        } catch (\Exception $e) {
            Log::error(
                'Failed to Marker view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Marker Not Found");
        }
    }

    /**
     * @param string $examinerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationDetailsByExaminerId(string $examinerId)
    {
        try {
            $details = $this->markerService->getDetailsByExaminerId($examinerId);
            return $this->sendSuccessResponse("Examiner Details Found", $details);
        } catch (\Exception $e) {
            Log::error(
                'Failed to view ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examiner Details Found");
        }
    }

    /**
     * @param ExaminationMarkerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationAddMarker(ExaminationMarkerRequest $request)
    {
        try {
            $details = $this->markerRepository->addMarkerDetails($request);
            Log::info(
                'added details in DB',
                ['method' => __METHOD__, 'data' => ['added' => $details]]
            );
            return $this->sendSuccessResponse("Marker added successfully");
        } catch (\Exception $e) {
            Log::error(
                'Failed to added Marker ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Marker not added");
        }
    }

    /**
     * @param ExaminationMarkerRequest $request
     * @param string $examinerId
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationUpdateMarker(ExaminationMarkerRequest $request, string $examinerId)
    {
        try {
            $details = $this->markerRepository->updateMarker($request, $examinerId);
            Log::info(
                'update details in DB',
                ['method' => __METHOD__, 'data' => ['added' => $details]]
            );
            return $this->sendSuccessResponse("Marker Update Successfully");
        } catch (\Exception $e) {
            Log::error(
                'Failed to update Marker ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Marker not updated");
        }
    }

    /**
     * @param int $examMarkerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAdministratorExaminationMarker(int $examMarkerId)
    {
        try {
            $job = new DeleteExamMarker($examMarkerId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Marker deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Marker not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update Marker ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Marker not deleted");
        }
    }
}
