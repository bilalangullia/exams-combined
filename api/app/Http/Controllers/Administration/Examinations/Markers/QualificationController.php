<?php

namespace App\Http\Controllers\Administration\Examinations\Markers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StaffQualificationRequest;
use App\Repositories\QualificationRepository;
use App\Services\Administration\Examinations\Markers\QualificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Response;
use App\Jobs\DeleteMarkerQualification;

class QualificationController extends Controller
{
    /**
     * @var QualificationRepository
     */
    protected $qualificationRepository;

    /**
     * @var QualificationService
     */
    protected $qualificationService;

    /**
     * QualificationController constructor.
     * @param QualificationService $qualificationService
     * @param QualificationRepository $qualificationRepository
     */
    public function __construct(
        QualificationService $qualificationService,
        QualificationRepository $qualificationRepository
    ) {
        $this->qualificationService = $qualificationService;
        $this->qualificationRepository = $qualificationRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationQualificationListing(Request $request, string $openEmisno)
    {
        try {
            $list = $this->qualificationService->getQualificationlist($request, $openEmisno);
            if (empty($list)) {
                return $this->sendErrorResponse(" Qualification Listing Not Found");
            }
            return $this->sendSuccessResponse(" Qualification List Found", $list);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification list not found");
        }
    }

    /**
     * Examination Qualification View
     * @param string $levelid
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationQualificationView(string $staffqualificationId)
    {
        try {
            $viewqualification = $this->qualificationService->getQualificationView($staffqualificationId);
            return $this->sendSuccessResponse("qualification Found", $viewqualification);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification view not found");
        }
    }

    /**
     * @param string $titleid
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationQualificationEdit(string $staffqualificationId)
    {
        try {
            $viewqualification = $this->qualificationService->getQualificationedit($staffqualificationId);
            return $this->sendSuccessResponse("qualification Found", $viewqualification);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch edit details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification edit not found");
        }
    }

    /**
     * @param StaffQualificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addExaminationStaffQualification(StaffQualificationRequest $request, string $openEmisno)
    {
        try {
            $addstaff = $this->qualificationRepository->addStaffQualification($request, $openEmisno);
            Log::info(
                'added details in DB',
                ['method' => __METHOD__, 'data' => ['added' => $addstaff]]
            );
            return $this->sendSuccessResponse("qualification added", $addstaff);
        } catch (\Exception $e) {
            Log::error(
                'Failed to add into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification not added successfully ");
        }
    }

    /**
     * @param StaffQualificationRequest $request
     * @param string $openEmisno
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function updateExaminationStaffQualification(StaffQualificationRequest $request, string $staffId)
    {
        try {
            $updatestaff = $this->qualificationRepository->updateStaffQualification($request, $staffId);
            Log::info(
                'updated details in DB',
                ['method' => __METHOD__, 'data' => ['added' => $updatestaff]]
            );
            return $this->sendSuccessResponse("qualification updated", $updatestaff);
        } catch (\Exception $e) {
            Log::error(
                'Failed to update  in to DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification not updated successfully ");
        }
    }

    /**
     * delete marker qualification
     * @param int $staffqualificationId
     * @return bool|\Exception|\Illuminate\Http\JsonResponse
     */
    public function destroyExaminationQualification(int $staffqualificationId)
    {
        try {
            $job = new DeleteMarkerQualification($staffqualificationId);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("  Qualification deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Qualification not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to deleted from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification not deleted");
        }
    }
}
