<?php

namespace App\Http\Controllers\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddExaminationOptionRequest;
use App\Http\Requests\ExamOptionUpdationRequest;
use App\Http\Requests\OptionImportRequest;
use App\Jobs\DeleteOption;
use App\Repositories\OptionRepository;
use App\Services\Administration\Examinations\Exams\OptionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Imports\OptionImport;
use File;
use Excel;

class OptionController extends Controller
{
    /**
     * OptionRepository instance
     * @var OptionRepository
     */
    protected $optionRepository;

    /**
     * OptionService instance
     * @var OptionService
     */
    protected $optionService;

    /**
     * OptionController constructor.
     * @param OptionRepository $optionRepository
     * @param OptionService $optionService
     */
    public function __construct(
        OptionRepository $optionRepository,
        OptionService $optionService
    ) {
        $this->optionRepository = $optionRepository;
        $this->optionService = $optionService;
    }

    /**
     * Getting examination option tab list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExamsOptionList(Request $request, string $examId)
    {
        try {
            $optionList = $this->optionService->getExaminationOptionList($request, $examId);
            
            return $this->sendSuccessResponse("Examination Option List Found", $optionList);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Option List from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Options List Not Found");
        }
    }

    /**
     * Getting examination option tab details
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExamsOptionDetail(Request $request, string $optionId)
    {
        try {
            $optionView = $this->optionService->getExaminationOptionDetails($request, $optionId);
            Log::info('Fetched Data from DB', ['method' => __METHOD__, 'data' => ['optionDetail' => $optionView]]);

            return $this->sendSuccessResponse("Examination Option Details Found", $optionView);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Option Details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Options Details Not Found");
        }
    }

    /**
     * Updating examination option tab details
     * @param Request $request
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAdministratorExaminationOptionRecord(ExamOptionUpdationRequest $request, string $optionId)
    {
        try {
            $optionUpdate = $this->optionRepository->updateExaminationOptionRecord($request, $optionId);
            Log::info('Fetched Data from DB', ['method' => __METHOD__, 'data' => ['optionDetail' => $optionUpdate]]);

            return $this->sendSuccessResponse("Examination Option Details Updated Successfully!", $optionUpdate);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Option Details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Options Details Not Updated");
        }
    }

    /**
     * Deleting examination option
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAdministratorExaminationOption(int $optionId)
    {
        try {
            $jobs = new DeleteOption($optionId, 0, true);
            $data = $this->dispatchNow($jobs);
            $response = $jobs->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Examination Option deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Examination Option Cannot be deleted");
            }

            return $this->sendSuccessResponse($jobs->getResponse());
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Option Cannot be deleted");
        }
    }

    /**
     * Adding examination option
     * @param AddExaminationOptionRequest $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function storeAdministratorExaminationOption(AddExaminationOptionRequest $request)
    {
        try {
            $addExaminationOption = $this->optionRepository->addExaminationOption($request);

            return $this->sendSuccessResponse("Examination Option Added Successfully", $addExaminationOption);
        } catch (\Exception $e) {
            Log::error(
                'Failed to data details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Option Not Added");
        }
    }

    public function administratorExaminationOptionImport(OptionImportRequest $request)
    {
        try {
            $requestParameter = $request->all();
            $validExtensions = config('constants.fileExt.excel');

            $extension = File::extension($request->file->getClientOriginalName());

            if (!in_array($extension, $validExtensions)) {
                return $this->sendErrorResponse('Not a valid file extension');
            }


            $header = config('constants.optionImport.header');

            $excel = Excel::toArray(new OptionImport(), $request->file('file'));
            //dd($excel);
            if (empty($excel[0][1])) {
                return $this->sendErrorResponse("Header is not present");
            }

            if (empty($excel[0][2])) {
                return $this->sendErrorResponse("Imported file is empty.");
            }

            if (!in_array($excel[0][1][0], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            if (!in_array($excel[0][1][1], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][2], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            if (!in_array($excel[0][1][3], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($excel[0][1][4], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            if (!in_array($excel[0][1][5], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            $rowsCount = count($excel[0]) - 2;
            $maxRows = config('constants.optionImport.maxRows');

            if ($rowsCount > $maxRows) {
                return $this->sendErrorResponse("File can not have than " . $maxRows . " records");
            }

            $import = $this->optionRepository->optionImport($excel, $requestParameter);

            Log::info('Option Imported in DB.', ['method' => __METHOD__, 'data' => ['import' => $import]]);

            if (count($import) > 0) {
                return $this->sendSuccessResponse("Option Data Imported Successfully", $import);
            } else {
                return $this->sendErrorResponse("Option Data not Imported in DB.");
            }

        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to import option in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import option in DB");
        }
    }
}
