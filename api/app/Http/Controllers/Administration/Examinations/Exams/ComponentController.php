<?php

namespace App\Http\Controllers\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamComponentUpdationRequest;
use App\Http\Requests\ExaminationSubjectImportRequest;
use App\Jobs\DeleteComponent;
use App\Repositories\ComponentRepository;
use App\Services\Administration\Examinations\Exams\ComponentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ComponentController extends Controller
{
    /**
     * @var ComponentRepository
     */
    protected $componentRepository;

    /**
     * @var ComponentService
     */
    protected $componentService;

    /**
     * ComponentController constructor.
     * @param ComponentService $componentService
     * @param ComponentRepository $componentRepository
     */

    public function __construct(
        ComponentService $componentService,
        ComponentRepository $componentRepository
    ) {
        $this->componentService = $componentService;
        $this->componentRepository = $componentRepository;
    }

    /**
     * component list on the basis of academic period,examination,option
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationComponentList(Request $request, string $examId)
    {
        try {
            $componentlist = $this->componentService->getComponentList($request, $examId);
            if (!empty($componentlist)) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['componentlist' => $componentlist]]
                );
                return $this->sendSuccessResponse("component List Found", $componentlist);
            } else {
                return $this->sendSuccessResponse("component List not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("component List Not Found");
        }
    }

    /**
     * @param Request $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationComponentView(Request $request, string $componentId)
    {
        try {
            $componentview = $this->componentService->getcomponentView($request, $componentId);

            return $this->sendSuccessResponse("component view Found", $componentview);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("component view Not Found");
        }
    }

    /**
     * @param Request $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationComponentEdit(Request $request, string $componentId)
    {
        try {
            $componentedit = $this->componentService->getcomponentEdit($request, $componentId);
            Log::info(
                'Fetched edit details from DB',
                ['method' => __METHOD__, 'data' => ['componentedit' => $componentedit]]
            );

            return $this->sendSuccessResponse("component view Found", $componentedit);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("component view Not Found");
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExaminationComponentType()
    {
        try {
            $componentlist = $this->componentRepository->getComponentTypeDropdown();

            return $this->sendSuccessResponse("component type list Found", $componentlist);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("component type list Not Found");
        }
    }

    /**
     * @param ExamComponentUpdationRequest $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getupdateExaminationComponent(ExamComponentUpdationRequest $request, string $componentId)
    {
        try {
            $componentupdate = $this->componentRepository->getComponentUpdate($request, $componentId);
            Log::info(
                'Fetched edit details from DB',
                ['method' => __METHOD__, 'data' => ['componentupdate' => $componentupdate]]
            );
            if ($componentupdate['message'] == 'error') {
                return $this->sendErrorResponse("component type not update ", $componentupdate);
            }

            return $this->sendSuccessResponse("component updated");
        } catch (\Exception $e) {
            Log::error(
                'Failed to update into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("component not updated");
        }
    }

    /**
     * deleting examination component and related tables
     * @param string $compId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAdministratorExaminationsComponent(int $componentId)
    {
        try {
            $job = new DeleteComponent($componentId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("component deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("component not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination's component Not deleted");
        }
    }

    /**
     * add examination component
     * @param ExamComponentUpdationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAdministratorExaminationComponent(ExamComponentUpdationRequest $request)
    {
        try {
            $addcomponents = $this->componentRepository->addExaminationComponent($request);
            Log::info(
                'added details into DB',
                ['method' => __METHOD__, 'data' => ['addcomponent' => $addcomponents]]
            );
            return $this->sendSuccessResponse("Component Added Successfully", $addcomponents);
        } catch (\Exception $e) {
            Log::error(
                'Failed to update into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("component not updated");
        }
    }

    /**
     * @param ExaminationSubjectImportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importExaminationComponent(ExaminationSubjectImportRequest $request)
    {
        try {
            $importcomponents = $this->componentRepository->importComponent($request);
            return $this->sendSuccessResponse("Component Added Successfully", $importcomponents);
        } catch (\Exception $e) {
            Log::error(
                'Failed to update into DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("component not updated");
        }
    }
}
