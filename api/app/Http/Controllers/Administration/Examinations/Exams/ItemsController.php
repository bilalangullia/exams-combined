<?php

namespace App\Http\Controllers\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddExaminationItemRequest;
use App\Http\Requests\ExamItemsUpdationRequest;
use App\Http\Requests\OptionImportRequest;
use App\Jobs\DeleteItem;
use App\Repositories\ItemsRepository;
use App\Services\Administration\Examinations\Exams\ItemsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Imports\ItemImport;
use File;
use Excel;

class ItemsController extends Controller
{
    protected $itemsRepository;

    protected $itemsService;


    public function __construct(
        ItemsRepository $itemsRepository,
        ItemsService $itemsService
    ) {
        $this->itemsRepository = $itemsRepository;
        $this->itemsService = $itemsService;
    }

    /**
     * Getting admin >> examination >> item tab listing
     * @param Request $request
     * @param string $componentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationItemList(Request $request, string $examId)
    {
        try {
            $itemList = $this->itemsService->getExaminationItemsList($request, $examId);
            
            return $this->sendSuccessResponse("Examination Items List Found", $itemList);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Items List Not Found");
        }
    }

    /**
     * Getting admin >> examination >> item tab detail
     * @param Request $request
     * @param string $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationItemDetails(Request $request, string $itemId)
    {
        try {
            $itemDetails = $this->itemsService->getExaminationItemsDetails($request, $itemId);
            Log::info('Fetched Item details from DB', ['method' => __METHOD__, 'data' => ['itemView' => $itemDetails]]);

            return $this->sendSuccessResponse("Examination Item's Details Found", $itemDetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Item's Details Not Found");
        }
    }

    /**
     * updating Getting admin >> examination >> item tab
     * @param Request $request
     * @param string $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAdministratorExaminationItems(ExamItemsUpdationRequest $request, string $itemId)
    {
        try {
            $updateItem = $this->itemsRepository->updateExaminationItems($request, $itemId);
            Log::info('Fetched Item details from DB', ['method' => __METHOD__, 'data' => ['itemData' => $updateItem]]);

            return $this->sendSuccessResponse("Examination Item's Updated Successfully", $updateItem);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Item's Not Updated");
        }
    }

    /**
     * Admin >> examination >> item tab >> delete
     * @param string $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAdministratorExaminationItems(int $itemId)
    {
        try {
            dispatch(new DeleteItem($itemId));

            return $this->sendSuccessResponse("Examination Item's Removed Successfully");
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch item details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Item's Not Removed");
        }
    }

    /**
     * Admin >> examination >> item tab >> add
     * @param AddExaminationItemRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAdministratorExaminationItem(AddExaminationItemRequest $request)
    {
        try {
            $saveItem = $this->itemsRepository->addExaminationItem($request);

            return $this->sendSuccessResponse("Examination Item's Added Successfully", $saveItem);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch item details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examinatio Item's Not Added");
        }
    }

    public function administratorExaminationItemImport(OptionImportRequest $request)
    {
        try {
            $requestParameter = $request->all();
            $validExtensions = config('constants.fileExt.excel');

            $extension = File::extension($request->file->getClientOriginalName());

            if (!in_array($extension, $validExtensions)) {
                return $this->sendErrorResponse('Not a valid file extension');
            }
            $header = config('constants.itemImport.header');
            $excel = Excel::toArray(new ItemImport(), $request->file('file'));
            //dd($excel);
            if (empty($excel[0][1])) {
                return $this->sendErrorResponse("Header is not present");
            }

            if (empty($excel[0][2])) {
                return $this->sendErrorResponse("Imported file is empty.");
            }

            if (!in_array($excel[0][1][0], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            if (!in_array($excel[0][1][1], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            $rowsCount = count($excel[0]) - 2;
            $maxRows = config('constants.itemImport.maxRows');

            if ($rowsCount > $maxRows) {
                return $this->sendErrorResponse("File can not have than " . $maxRows . " records");
            }

            $import = $this->itemsRepository->itemImport($excel, $requestParameter);
        
            Log::info('Option Imported in DB.', ['method' => __METHOD__, 'data' => ['import' => $import]]);

            if (count($import) > 0) {
                return $this->sendSuccessResponse("Item Data Imported Successfully", $import);
            } else {
                return $this->sendErrorResponse("Item Data not Imported in DB.");
            }

        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to import Item in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import Item in DB");
        }
    }
}
