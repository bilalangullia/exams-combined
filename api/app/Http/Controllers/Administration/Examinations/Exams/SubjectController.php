<?php

namespace App\Http\Controllers\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExaminationSubjectImportRequest;
use App\Http\Requests\ExamSubjectUpdationRequest;
use App\Jobs\DeleteExamSubject;
use App\Repositories\SubjectRepository;
use App\Services\Administration\Examinations\Exams\SubjectService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SubjectController extends Controller
{
    /**
     * @var SubjectService
     */
    protected $subjectService;

    /**
     * @var SubjectRepository
     */
    protected $subjectRepository;

    /**
     * ExaminationController constructor.
     * @param SubjectService $subjectService
     */
    public function __construct(
        SubjectService $subjectService,
        SubjectRepository $subjectRepository
    ) {
        $this->subjectService = $subjectService;
        $this->subjectRepository = $subjectRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministrationExaminationEducationSubjectList()
    {
        try {
            $educationsubjectlist = $this->subjectRepository->educationSubjectListDropdown();
            return $this->sendSuccessResponse("Subject List Found", $educationsubjectlist);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education subject list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Subject List Not Found");
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministrationExaminationSubjectlisting(Request $request)
    {
        try {
            $subjectlist = $this->subjectService->subjectListing($request);
            if (!empty($subjectlist)) {
                Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['subjectlist' => $subjectlist]]);
                return $this->sendSuccessResponse("Subject List Found", $subjectlist);
            } else {
                return $this->sendErrorResponse("Subject  List Not Found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Subject List Not Found");
        }
    }

    /**
     * @param Request $request
     * @param int $examsubjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministrationExaminationSubjectView(Request $request, int $examsubjectId)
    {
        try {
            $subjectView = $this->subjectService->examSubjectView($request, $examsubjectId);
            Log::info('Fetched view from DB', ['method' => __METHOD__, 'data' => ['subjectView' => $subjectView]]);

            return $this->sendSuccessResponse("Subject view details Found", $subjectView);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch view from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Subject view details Not Found");
        }
    }

    /**
     * @param Request $request
     * @param int $examsubjectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministrationExaminationSubjectedit(Request $request, int $examsubjectId)
    {
        try {
            $subjectedit = $this->subjectService->examSubjectEdit($request, $examsubjectId);
            Log::info(
                'Fetched edit details from DB',
                ['method' => __METHOD__, 'data' => ['subjectedit' => $subjectedit]]
            );
            return $this->sendSuccessResponse("Subject view details Found", $subjectedit);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch edit details from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Subject view details Not Found");
        }
    }

    /**
     * update administration examination subject
     * @param Request $request
     * @param int $subjectid
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministrationExaminationSubjectupdate(ExamSubjectUpdationRequest $request, string $subjectId)
    {
        try {
            $subjectupdate = $this->subjectRepository->examinationSubjectUpdate($request, $subjectId);
            Log::info(
                'Fetched edit details from DB',
                ['method' => __METHOD__, 'data' => ['subjectupdate' => $subjectupdate]]
            );
        } catch (\Exception $e) {
            Log::error(
                'Failed to update  details in to  DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Record not updated");
        }
        if (empty($subjectupdate)) {
            return $this->sendErrorResponse("Record not updated");
        }
        return $this->sendSuccessResponse("record update", $subjectupdate);
    }

    /**
     * Delete examination subject
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAdministratorExaminationSubject(int $examSubjectId)
    {
        try {
            $job = new DeleteExamSubject($examSubjectId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("subject deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("subject not deleted.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination's Subject Not deleted");
        }
    }

    /**
     * add administration examination subjects
     * @param ExamSubjectUpdationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAdministrationExaminationSubject(ExamSubjectUpdationRequest $request)
    {
        try {
            $addsubjects = $this->subjectRepository->addExaminationSubject($request);
            Log::info(
                ' added details into DB',
                ['method' => __METHOD__, 'data' => ['addsubject' => $addsubjects]]
            );
            return $this->sendSuccessResponse("Subject Added Successfully", $addsubjects);
        } catch (\Exception $e) {
            Log::error(
                'Failed to add list in to DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination's Subject Not Added");
        }
    }

    /**
     * @param ExamSubjectUpdationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importExaminationSubject(ExaminationSubjectImportRequest $request)
    {
        try {
            $importdata = $this->subjectRepository->subjectImport($request);
            return $this->sendSuccessResponse("Subject Import Successfully", $importdata);
        } catch (\Exception $e) {
            Log::error(
                'Failed to add list in to DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination's Subject Not Imported");
        }
    }
}
