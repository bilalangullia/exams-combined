<?php

namespace App\Http\Controllers\Administration\Examinations\Exams;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddExaminationRequest;
use App\Http\Requests\AdministratorExaminationUpdateRequest;
use App\Http\Requests\CopyExamRequest;
use App\Repositories\ExaminationRepository;
use App\Services\Administration\Examinations\Exams\ExaminationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Jobs\DeleteExamination;

class ExaminationController extends Controller
{
    /**
     * ExaminationRepository Instance
     * @var ExaminationRepository
     */
    protected $examinationRepository;

    /**
     * ExaminationService
     * @var ExaminationService
     */
    protected $examinationService;

    /**
     * ExaminationController constructor.
     * @param ExaminationRepository $examinationRepository
     * @param ExaminationService $feeService
     */
    public function __construct(
        ExaminationRepository $examinationRepository,
        ExaminationService $examinationService
    ) {
        $this->examinationRepository = $examinationRepository;
        $this->examinationService = $examinationService;
    }

    /**
     *Getting examination >> exam tab >> listing
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministrationExaminationExamList(Request $request)
    {
        try {
            $examList = $this->examinationService->getExaminationExamList($request);
            
            return $this->sendSuccessResponse("Exams list Found", $examList);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting examination >> exam tab >> view
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministrationExaminationExamDetail(Request $request, string $examId)
    {
        try {
            $examDetail = $this->examinationService->getExaminationExamDetail($request, $examId);
            Log::info('Fetched data from DB', ['method' => __METHOD__, 'data' => ['examList' => $examDetail]]);

            return $this->sendSuccessResponse("Exam's Detail Found", $examDetail);
        } catch (\Exception $e) {
            Log::error(
                'Failed to Data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam's Detail Not Found");
        }
    }

    /**
     * update examination >> exam tab >> exam
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAdministrationExaminationExam(AdministratorExaminationUpdateRequest $request, string $examId)
    {
        try {
            $record = $this->examinationRepository->updateExaminationRecord($request, $examId);
            Log::info('Updated Record', ['method' => __METHOD__, 'data' => ['examList' => $record]]);

            return $this->sendSuccessResponse("Exam's Updated Successfully", $record);
        } catch (\Exception $e) {
            Log::error(
                'Failed to Update Record',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Exam's Record Not Found");
        }
    }

    /**
     * adding examination
     * @param AddExaminationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAdministrationExamination(AddExaminationRequest $request)
    {
        try {
            $addExamination = $this->examinationRepository->addExamination($request);

            return $this->sendSuccessResponse("Examination Added Successfully", $addExamination);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Not Added");
        }
    }

    public function destroyAdministratorExamination(int $examId)
    {
        try {
            $job = new DeleteExamination($examId, 0, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Examination deleted successfully.");
            } else {
                return $this->sendDeleteErrorResponse("Examination can not be deleted.");
            }
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to delete Examination',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination not deleted.");
        }
    }

    public function copyAdministrationExamination(CopyExamRequest $request)
    {
        try{
            $copyData = $request->all();
            $copyExamination = $this->examinationRepository->examCopy($copyData);

            return $this->sendSuccessResponse("Examination Copied Successfully", $copyExamination);
        } catch (\Exception $e) {
            Log::error(
                'Failed to copy exam.',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination not copied.");;
        }
    }
}
