<?php

namespace App\Http\Controllers\Administration\Examinations;

use App\Http\Controllers\Controller;
use App\Jobs\DeleteExaminationFee;
use App\Repositories\FeeRepository;
use App\Services\Administration\Examinations\FeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FeeController extends Controller
{
    /**
     * FeeRepository Instance
     * @var FeeRepository
     */
    protected $feeRepository;

    /**
     * FeeService instance
     * @var FeeService
     */
    protected $feeService;

    /**
     * FeeController constructor.
     * @param FeeRepository $feeRepository
     */
    public function __construct(
        FeeRepository $feeRepository,
        FeeService $feeService
    ) {
        $this->feeRepository = $feeRepository;
        $this->feeService = $feeService;
    }

    /**
     * Getting Examination fee list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationsFeeList(Request $request)
    {
        try {
            $feeList = $this->feeService->getExaminationsFeeList($request);
            Log::info(
                'Fetched fee List from DB',
                ['method' => __METHOD__, 'data' => ['feeList' => $feeList]]
            );

            return $this->sendSuccessResponse("Examination Fee List Found", $feeList);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee List Not found");
        }
    }

    /**
     * Getting Examination fee details
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdministratorExaminationsFeeDetails(Request $request, string $examFeeId)
    {
        try {
            $feeDetails = $this->feeService->getExaminationsFeeDetails($request, $examFeeId);
            Log::info(
                'Fetched fee details from DB',
                ['method' => __METHOD__, 'data' => ['feeDetails' => $feeDetails]]
            );

            return $this->sendSuccessResponse("Examination Fee Details Found", $feeDetails);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Details Not found");
        }
    }


    /**
     * Adding Examination fee
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAdministratorExaminationsFee(Request $request)
    {
        try {
            $feeData = $this->feeRepository->addExaminationsFee($request);

            return $this->sendSuccessResponse("Examination Fee Added", $feeData);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Not Added");
        }
    }

    /**
     * Updating examination fee
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAdministratorExaminationsFee(Request $request, string $examFeeId)
    {
        try {
            $updateExamFee = $this->feeRepository->updateExaminationFee($request, $examFeeId);

            return $this->sendSuccessResponse("Examination Fee Updated Successfully!", $updateExamFee);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Not Updated");
        }
    }

    /**
     * Deleting examination fee
     * @param string $examFeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAdministratorExaminationsFee(string $examFeeId)
    {
        try {
            $jobs = new DeleteExaminationFee($examFeeId);
            $data = $this->dispatchNow($jobs);
            if ($jobs->response) {
                return $this->sendSuccessResponse($jobs->getResponse());
            } else {
                return $this->sendErrorResponse($jobs->getResponse());
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Fee Not Removed");
        }
    }
}
