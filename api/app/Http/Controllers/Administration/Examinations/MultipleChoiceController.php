<?php

namespace App\Http\Controllers\Administration\Examinations;

use App\Http\Controllers\Controller;
use App\Repositories\MultipleChoiceRepository;
use App\Services\Administration\Examinations\MultipleChoiceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MultipleChoiceController extends Controller
{
    /**
     * @var MultipleChoiceService
     */
    protected $multipleChoiceService;
    /**
     * @var MultipleChoiceRepository
     */
    protected $multipleChoiceRepository;

    /**
     * MultipleChoiceController constructor.
     * @param MultipleChoiceRepository $multipleChoiceRepository
     * @param MultipleChoiceService $multipleChoiceService
     */
    public function __construct(
        MultipleChoiceRepository $multipleChoiceRepository,
        MultipleChoiceService $multipleChoiceService
    ) {
        $this->multipleChoiceService = $multipleChoiceService;
        $this->multipleChoiceRepository = $multipleChoiceRepository;
    }

    /**
     * Updating component multiple choice question's answer
     * @param Request $request
     * @param string $examId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAdministrationExaminationsMultipleChoiceAnswer(Request $request, string $componentId)
    {
        try {
            $multipleChoiceList = $this->multipleChoiceRepository->updateComponentQuestionAnswer(
                $request,
                $componentId
            );

            return $this->sendSuccessResponse('Examination Multiple Choice Answer Updated', $multipleChoiceList);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Examination Multiple Choice Answer Not Updated');
        }
    }

}
