<?php

namespace App\Http\Controllers\Results;

use App\Http\Controllers\Controller;
use App\Repositories\GradeReviewRepository;
use App\Services\Results\GradeReviewService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GradeReviewController extends Controller
{
    /**
     * @var GradeReviewRepository
     */
    protected $gradeReviewRepository;
    /**
     * @var GradeReviewService
     */

    protected $gradeReviewService;

    /**
     * GradeReviewController constructor.
     * @param GradeReviewService $gradeReviewService
     * @param GradeReviewRepository $gradeReviewRepository
     */
    public function __construct(
        GradeReviewService $gradeReviewService,
        GradeReviewRepository $gradeReviewRepository
    ) {
        $this->gradeReviewService = $gradeReviewService;
        $this->gradeReviewRepository = $gradeReviewRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse|int
     */
    public function resultsAddGradeReview()
    {
        try {
            $datagrade = $this->gradeReviewRepository->addGradeReview();
            return $this->sendSuccessResponse('Grade review added successfully', $datagrade);
        } catch (\Exception $e) {
            Log::error(
                'Failed to add  grades review  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Grades review not added");
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resultsGradeReviewListing(Request $request)
    {
        try {
            $listview = $this->gradeReviewService->getGradeReviewList($request);
            if (empty($listview)) {
                return $this->sendErrorResponse(" Grade review list Not Found");
            }
            return $this->sendSuccessResponse('Grade review list', $listview);
        } catch (\Exception $e) {
            Log::error(
                'Failed to find  grades list  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to find  grades list");
        }
    }

    /**
     * @param Request $request
     * @return \Exception|\Illuminate\Http\JsonResponse
     */
    public function resultsGradeReviewView(string $candidateId)
    {
        try {
            $view = $this->gradeReviewRepository->viewGradeReview($candidateId);
            return $this->sendSuccessResponse('Grade review View', $view);
        } catch (\Exception $e) {
            Log::error(
                'Failed to view review grades   ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to view  review grades ");
        }
    }
}
