<?php

namespace App\Http\Controllers\Results;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Marks\MultiChoicesService;

class FinalGradeController extends Controller
{
    protected $multiChoiceService;

    public function __construct( 
        MultiChoicesService $multiChoiceService
    )
    {
        $this->multiChoiceService = $multiChoiceService;
    }

    public function resultsFinalGradeGenerateButton(Request $request)
    {
    	try{
    		$generate = $this->multiChoiceService->finalGradeGenerateButton($request);

    		return $this->sendSuccessResponse("Candidate's Option Grade Generated Successfully", $generate);
    	} catch (Exception $e) {
             Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Candidate's Option Grade Not Generated");
        }
    }
}
