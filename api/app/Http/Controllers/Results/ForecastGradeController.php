<?php

namespace App\Http\Controllers\Results;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ForecastGradeImportRequest;
use App\Imports\ForecastGradeExcelImport;
use App\Services\Results\ForecastGradeService;
use Illuminate\Support\Facades\Log;
use File;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\ForecastGradeImportRepository;
use App\Repositories\ForecastGradeRepository;

class ForecastGradeController extends Controller
{
    /**
     * ForecastGradeImportRepository instance
     * @var $forecastGradeImportRepository
     */
    protected $forecastGradeImportRepository;

    /**
     * @var ForecastGradeRepository
     */
    protected $forecastGradeRepository;

    protected $forecastGradeService;

    /**
     * ForecastGradeController constructor.
     * @param ForecastGradeImportRepository $forecastGradeImportRepository
     * @param ForecastGradeRepository $forecastGradeRepository
     */

    public function __construct(
        ForecastGradeImportRepository $forecastGradeImportRepository,
        ForecastGradeRepository $forecastGradeRepository, 
        ForecastGradeService $forecastGradeService
    )
    {
        $this->forecastGradeImportRepository = $forecastGradeImportRepository;
        $this->forecastGradeRepository = $forecastGradeRepository;
        $this->forecastGradeService = $forecastGradeService;
    }

    /**
     * Importing Forecast Grade Excel File.
     * @param ForecastGradeImportRequest $request
     * @return JsonResponse
     */
    public function forecastGradeImport(ForecastGradeImportRequest $request)
    {
        try {
            $requestParameter = $request->all();
            $validExtensions = config('constants.fileExt.excel');

            $extension = File::extension($request->template->getClientOriginalName());

            if (!in_array($extension, $validExtensions)) {
                return $this->sendErrorResponse('Not a valid file extension');
            }


            $header = config('constants.forecastGradeImport.header');

            $excel = Excel::toArray(new ForecastGradeExcelImport(), $request->file('template'));


            if (empty($excel[0][1])) {
                return $this->sendErrorResponse("Header is not present");
            }

            if (empty($excel[0][2])) {
                return $this->sendErrorResponse("Imported file is empty.");
            }

            if (!in_array($excel[0][1][0], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            if (!in_array($excel[0][1][1], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            $rowsCount = count($excel[0]) - 2;
            $maxRows = config('constants.forecastGradeImport.maxRows');

            if ($rowsCount > $maxRows) {
                return $this->sendErrorResponse("File can not have than " . $maxRows . " records");
            }

            $import = $this->forecastGradeImportRepository->forecastGradeImport($excel, $requestParameter);

            Log::info('Forecast Grade Data Imported in DB.', ['method' => __METHOD__, 'data' => ['import' => $import]]);

            if (count($import) > 0) {
                return $this->sendSuccessResponse("Forecast Grade Data Imported in DB.", $import);
            } else {
                return $this->sendErrorResponse("Forecast Grade Data not Imported in DB.");
            }

        } catch (\Exception $e) {
            Log::error(
                'Failed to import forecast grades in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import forecast grades in DB");
        }
    }

    /**
     * Getting forecast grade dropdown
     * @param string $optionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getForecastGradeDropdown(string $optionId)
    {
        try {
            $forecastGradeDropdown = $this->forecastGradeService->forecastGradeDropdown($optionId);
            return $this->sendSuccessResponse("Forecast Grade Dropdown", $forecastGradeDropdown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Forecast Grade Dropdown Not Found");
        }
    }

    /**
     * updating candidate forecast grade
     * @param ForecastGradeRequest $request
     * @param string $candidateId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateResultsForecastGrade(Request $request, string $candidateId)
    {
        try {
            $update = $this->forecastGradeRepository->updateCandidateForecastGrade($request, $candidateId);

            return $this->sendSuccessResponse('Candidate Forecast Grade Added Successfully', $update);
            } catch (\Exception $e) {
                return $e;
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Candidate Forecast Grade Not Added");
        }
    }

    public function getForecastCandidateList(Request $request)
    {
        try{
            $listing = $this->forecastGradeService->getForecastGradeCandidateList($request);

            return $this->sendSuccessResponse('Candidate Forecast Grade List Found', $listing);
        } catch (\Exception $e) {
            return $e;
            Log::error($e);

            return $this->sendErrorResponse('Candidate Forecast Grade List Not Found');
        }
    }
}
