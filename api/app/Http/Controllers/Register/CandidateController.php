<?php

namespace App\Http\Controllers\Register;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\OptionImportRequest;
use App\Imports\CandidateExcelImport;
use App\Models\ExaminationCentre;
use App\Models\ExaminationStudent;
use App\Models\ExaminationStudentsFee;
use App\Models\ExaminationStudentsOption;
use App\Models\SecurityUser;
use App\Models\UserSpecialNeedsAssessment;
use App\Traits\SubjectTraits;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Validator;
use Illuminate\Support\Facades\Log;
use App\Jobs\DeleteRegisterCandidate;
use App\Services\Register\CandidateService;
use App\Repositories\RegisterCandidateRepository;
use App\Models\Examination;
use JWTAuth;
use File;

class CandidateController extends Controller
{
   use SubjectTraits;

    /**
     * CandidateService instance
     * @var $candidateService
     */
    protected $candidateService;
    protected $registerCandidateRepository;

    /**
     * CandidateController constructor.
     * @param CandidateService $candidateService
     */
    public function __construct(
        candidateService $candidateService,
        RegisterCandidateRepository $registerCandidateRepository
    )
    {
        $this->candidateService = $candidateService;
        $this->registerCandidateRepository = $registerCandidateRepository;
    }

    /**
     * @param string $candId
     * @return JsonResponse
     */
    public function detailsByCandId(string $candId)
    {
        $getCandidate = ExaminationStudent::select(
            'id',
            'candidate_id',
            'student_id',
            'examination_id',
            'examination_centre_id',
            'examination_attendance_types_id'
        )->with(
            'securityUser:id,openemis_no,first_name,middle_name,third_name,last_name,address,postal_code,address_area_id,birthplace_area_id,gender_id,date_of_birth,nationality_id,identity_type_id,identity_number',
            'securityUser.gender:id,name',
            'securityUser.nationality:id,name',
            'securityUser.identityType:id,name',
            'securityUser.areaAdministrative:id,name',
            'securityUser.birthplaceArea:id,name',
            'securityUser.userSpecialNeedsAssessment:security_user_id,id,special_need_type_id,special_need_difficulty_id',
            'securityUser.userSpecialNeedsAssessment.specialNeed:id,name',
            'securityUser.userSpecialNeedsAssessment.specialDifficulty:id,name',
            'examinationCentre:id,name,code,area_id',
            'examinationCentre.area:id,code,name',
            'attendanceType:id,name',
            'examName:id,name,code,academic_period_id',
            'examName.academicPeriod:id,name',
            'examinationStudentsCertificate:examination_students_id,id,certificate_id'
        )->where('candidate_id', $candId)->get()->map(
            function ($item, $key) {
                return [
                    "candidate_id" => [
                        "key" => $item['id'],
                        "value" => $item['candidate_id'],
                    ],
                    "openemis_no" => $item['securityUser']['openemis_no'],
                    "first_name" => $item['securityUser']['first_name'],
                    "middle_name" => $item['securityUser']['middle_name'],
                    "third_name" => $item['securityUser']['third_name'],
                    "last_name" => $item['securityUser']['last_name'],
                    "gender_id" => [
                        "key" => $item['securityUser']["gender"]["id"],
                        "value" => $item['securityUser']["gender"]["name"],
                    ],
                    "date_of_birth" => $item['securityUser']['date_of_birth'],
                    "nationality_id" => [
                        "key" => (!empty($item['securityUser']["nationality"]["id"]))?$item['securityUser']["nationality"]["id"]:'',
                        "value" => (!empty($item['securityUser']["nationality"]["name"]))?$item['securityUser']["nationality"]["name"]:'',
                    ],
                    "identity_type_id" => [
                        "key" => (!empty($item['securityUser']["identityType"]["id"]))?$item['securityUser']["identityType"]["id"]:'',
                        "value" => (!empty($item['securityUser']["identityType"]["name"]))?$item['securityUser']["identityType"]["name"]:'',
                    ],
                    "identity_number" => $item['securityUser']['identity_number'],
                    "address" => $item['securityUser']['address'],
                    "postal_code" => $item['securityUser']['postal_code'],
                    "address_area_id" => [
                        "key" => (!empty($item['securityUser']['areaAdministrative']['id']))?$item['securityUser']['areaAdministrative']['id']:'',
                        "value" => (!empty($item['securityUser']['areaAdministrative']['name']))?$item['securityUser']['areaAdministrative']['name']:'',
                    ],
                    "birthplace_area_id" => [
                        "key" => (!empty($item['securityUser']['birthplaceArea']['id']))?$item['securityUser']['birthplaceArea']['id']:'',
                        "value" => (!empty($item['securityUser']['birthplaceArea']['name']))?$item['securityUser']['birthplaceArea']['name']:'',
                    ],
                    "academic_period_id" => [
                        "key" => (!empty($item["examName"]["academicPeriod"]["id"]))?$item["examName"]["academicPeriod"]["id"]:'',
                        "value" => (!empty($item["examName"]["academicPeriod"]["name"]))?$item["examName"]["academicPeriod"]["name"]:'',
                    ],
                    "examination_id" => [
                        "key" => (!empty($item["examName"]["id"]))?$item["examName"]["id"]:'',
                        "value" => (!empty($item["examName"]["name"]))?$item["examName"]["name"]:'',
                    ],
                    "area_id" => [
                        "key" => (!empty($item["examinationCentre"]["area"]["id"]))?$item["examinationCentre"]["area"]["id"]:'',
                        "value" => (!empty($item["examinationCentre"]["area"]["name"]))?$item["examinationCentre"]["area"]["name"]:'',
                    ],
                    "examination_centre_id" => [
                        "key" => (!empty($item["examinationCentre"]["id"]))?$item["examinationCentre"]["id"]:'',
                        "value" => (!empty($item["examinationCentre"]["name"]))?$item["examinationCentre"]["name"]:'',
                    ],
                    "certificate_number" => [
                        "key" => (!empty($item["examinationStudentsCertificate"]["id"]))?$item["examinationStudentsCertificate"]["id"]:'',
                        "value" => (!empty($item["examinationStudentsCertificate"]["certificate_id"]))?$item["examinationStudentsCertificate"]["certificate_id"]:''
                    ],
                    "examination_attendance_types_id" => [
                        "key" => (!empty($item["attendanceType"]["id"]))?$item["attendanceType"]["id"]:'',
                        "value" => (!empty($item["attendanceType"]["name"]))?$item["attendanceType"]["name"]:'',
                    ],
                    "special_need_type_id" => [
                        "key" => (!empty($item['securityUser']["UserSpecialNeedsAssessment"]["specialNeed"]["id"]))?$item['securityUser']["UserSpecialNeedsAssessment"]["specialNeed"]["id"]:'',
                        "value" => (!empty($item['securityUser']["UserSpecialNeedsAssessment"]["specialNeed"]["name"]))?$item['securityUser']["UserSpecialNeedsAssessment"]["specialNeed"]["name"]:'',
                    ],
                    "special_need_difficulty_id" => [
                        "key" => (!empty($item['securityUser']["UserSpecialNeedsAssessment"]["specialDifficulty"]["id"]))?$item['securityUser']["UserSpecialNeedsAssessment"]["specialDifficulty"]["id"]:'',
                        "value" => (!empty($item['securityUser']["UserSpecialNeedsAssessment"]["specialDifficulty"]["name"]))?$item['securityUser']["UserSpecialNeedsAssessment"]["specialDifficulty"]["name"]:'',
                    ],
                ];
            }
        );
        if ($getCandidate->count()) {
            return $this->sendSuccessResponse("Candidate Details Found", $getCandidate);
        } else {
            return $this->sendErrorResponse('candidate Details Not Found');
        }
    }

    /**
     * @param string $emisId
     * @return JsonResponse
     */
    public function detailsByEmisId(string $emisId)
    {
        $get_emisData = SecurityUser::with(
            'gender:id,name',
            'nationality:id,name',
            'identityType:id,name',
            'examinationStudent:student_id,candidate_id,id,examination_id,examination_centre_id,examination_attendance_types_id',
            'examinationStudent.examinationCentre:id,code,name,area_id',
            'examinationStudent.attendanceType:id,name',
            'examinationStudent.examName:id,code,name,academic_period_id',
            'userSpecialNeedsAssessment.specialNeed:id,name',
            'userSpecialNeedsAssessment.specialDifficulty:id,name'
        )->where('openemis_no', $emisId)->get()->map(
            function ($item, $key) {
                return [
                    "candidate_id" => [
                        "key" => $item['examinationStudent']['id'],
                        "value" => $item['examinationStudent']['candidate_id'],
                    ],
                    "openemis_no" => $item['openemis_no'],
                    "first_name" => $item['first_name'],
                    "middle_name" => $item['middle_name'],
                    "third_name" => $item['third_name'],
                    "last_name" => $item['last_name'],
                    "gender_id" => [
                        "key" => $item["gender"]["id"],
                        "value" => $item["gender"]["name"],
                    ],
                    "date_of_birth" => $item['date_of_birth'],
                    "nationality_id" => [
                        "key" => (!empty($item["nationality"]["id"]))?$item["nationality"]["id"]:'',
                        "value" => (!empty($item["nationality"]["name"]))?$item["nationality"]["name"]:'',
                    ],
                    "identity_type_id" => [
                        "key" => (!empty($item["identityType"]["id"]))?$item["identityType"]["id"]:'',
                        "value" => (!empty($item["identityType"]["name"]))?$item["identityType"]["name"]:'',
                    ],
                    "identity_number" => $item['identity_number'],
                    "address" => $item['address'],
                    "postal_code" => $item['postal_code'],
                    "address_area_id" => [
                        "key" => (!empty($item['areaAdministrative']['id']))?$item['areaAdministrative']['id']:'',
                        "value" => (!empty($item['areaAdministrative']['name']))?$item['areaAdministrative']['name']:'',
                    ],
                    "birthplace_area_id" => [
                        "key" => (!empty($item['areaAdministrative']['id']))?$item['areaAdministrative']['id']:'',
                        "value" => (!empty($item['areaAdministrative']['name']))?$item['areaAdministrative']['name']:'',
                    ],
                    "academic_period_id" => [
                        "key" => (!empty($item["examinationStudent"]["examName"]["academicPeriod"]["id"]))?$item["examinationStudent"]["examName"]["academicPeriod"]["id"]:'',
                        "value" => (!empty($item["examinationStudent"]["examName"]["academicPeriod"]["name"]))?$item["examinationStudent"]["examName"]["academicPeriod"]["name"]:'',
                    ],
                    "examination_id" => [
                        "key" => (!empty($item["examinationStudent"]["examName"]["id"]))?$item["examinationStudent"]["examName"]["id"]:'',
                        "value" => (!empty($item["examinationStudent"]["examName"]["name"]))?$item["examinationStudent"]["examName"]["name"]:'',
                    ],
                    "area_id" => [
                        "key" => (!empty($item["examinationStudent"]["examinationCentre"]["area"]["id"]))?$item["examinationStudent"]["examinationCentre"]["area"]["id"]:'',
                        "value" => (!empty($item["examinationStudent"]["examinationCentre"]["area"]["name"]))?$item["examinationStudent"]["examinationCentre"]["area"]["name"]:'',
                    ],
                    "examination_centre_id" => [
                        "key" => (!empty($item["examinationStudent"]["examinationCentre"]["id"]))?$item["examinationStudent"]["examinationCentre"]["id"]:'',
                        "value" => (!empty($item["examinationStudent"]["examinationCentre"]["name"]))?$item["examinationStudent"]["examinationCentre"]["name"]:'',
                    ],
                    "examination_attendance_types_id" => [
                        "key" => (!empty($item["examinationStudent"]["attendanceType"]["id"]))?$item["examinationStudent"]["attendanceType"]["id"]:'',
                        "value" => (!empty($item["examinationStudent"]["attendanceType"]["name"]))?$item["examinationStudent"]["attendanceType"]["name"]:'',
                    ],
                    "special_need_type_id" => [
                        "key" => (!empty($item["UserSpecialNeedsAssessment"]["specialNeed"]["id"]))?$item["UserSpecialNeedsAssessment"]["specialNeed"]["id"]:'',
                        "value" => (!empty($item["UserSpecialNeedsAssessment"]["specialNeed"]["name"]))?$item["UserSpecialNeedsAssessment"]["specialNeed"]["name"]:'',
                    ],
                    "special_need_difficulty_id" => [
                        "key" => (!empty($item["UserSpecialNeedsAssessment"]["specialDifficulty"]["id"]))?$item["UserSpecialNeedsAssessment"]["specialDifficulty"]["id"]:'',
                        "value" => (!empty($item["UserSpecialNeedsAssessment"]["specialDifficulty"]["name"]))?$item["UserSpecialNeedsAssessment"]["specialDifficulty"]["name"]:'',
                    ],
                ];
            }
        );
        if ($get_emisData->count()) {
            return $this->sendSuccessResponse("Candidate Found", $get_emisData);
        } else {
            return $this->sendErrorResponse("Candidate Not Found");
        }
    }

    /**
     * @param RegisterRequest $request
     * @return \Exception|JsonResponse
     */
    public function registerCandidate(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = $request->validated();
            if ($request->has('openemis_no')) {
                $emis_no = $request['openemis_no'];
                $user = SecurityUser::where('openemis_no', '=', $emis_no['value'])->first();
                $user['openemis_no'] = $user->openemis_no;
                $user['username'] = $user->openemis_no;
                $identityNo  = $user['identity_number'];
            } elseif ($request->has('candidate_id')) {
                $candidate_id = $request['candidate_id'];
                $user = SecurityUser::whereHas(
                    'examinationStudent',
                    function ($query) use ($candidate_id) {
                        $query->where('candidate_id', $candidate_id['value']);
                    }
                )->first();
                $user['openemis_no'] = $user->openemis_no;
                $user['username'] = $user->openemis_no;
            } else {
                $user = new SecurityUser();
                $latestId = SecurityUser::orderBy('id','DESC')->first();
                if(is_numeric($latestId)) {
                    $user['openemis_no'] = $latestId->openemis_no + 1;
                } else {
                    $string = $latestId->openemis_no;
                    $newStr = ++$string;
                    $user['openemis_no'] = $newStr;
                }
                $user['username'] = $user['openemis_no'];
            }
            $user['first_name'] = $request->first_name;
            $user['middle_name'] = $request->middle_name;
            $user['third_name'] = $request->third_name;
            $user['last_name'] = $request->last_name;
            $user['gender_id'] = $request->gender_id;
            $user['date_of_birth'] = $this->changeDateFormat($request['date_of_birth']['text']);
            $user['address'] = $request->address;
            $user['postal_code'] = $request->postal_code;
            $user['address_area_id'] = (isset($request['address_area_id'][0]['id'])) ? $request['address_area_id'][0]['id'] : null;
            $user['birthplace_area_id'] = (isset($request['birthplace_area_id'][0]['id'])) ? $request['birthplace_area_id'][0]['id'] : null;
            $user['nationality_id'] = ($request['nationality_id'] == "null" || $request['nationality_id'] == "") ? null : $request['nationality_id'];
            $user['identity_type_id'] = ($request['identity_type_id'] == "null" || $request['identity_type_id'] == "") ? null : $request['identity_type_id'];
            if(isset($request->identity_number)) {
                $check = SecurityUser::where('openemis_no', '!=', $user->openemis_no)->where('identity_number', $request->identity_number)->first();
                if($check){
                    return $this->sendErrorResponse("Identity Number Already Exist");
                } else {
                 $user['identity_number'] = $request->identity_number;
                }
            } else {
                 $user['identity_number'] = $request->identity_number;
                }
            $user['is_student'] = config('constants.students.isStudent');
            $user['status'] = config('constants.students.status');
            $user['created_user_id'] = JWTAuth::user()->id;
            $user['created'] = Carbon::now()->toDateTimeString();
            $record = $user->save();
            if ($record) {
                $need = new UserSpecialNeedsAssessment();
                $need['special_need_type_id'] = (int)$request['special_need_type_id'] > 0 ? $request['special_need_type_id'] : 0;
                $need['special_need_difficulty_id'] = (int)$request['special_need_difficulty_id'] > 0 ? $request['special_need_difficulty_id'] : 0;
                $need['date'] = Carbon::now()->toDateTimeString();
                $need['security_user_id'] = $user->id;
                $need['created_user_id'] = JWTAuth::user()->id;
                $need['created'] = Carbon::now()->toDateTimeString();
                $needanddiff = $need->save();
                if ($needanddiff) {
                    $exam = new ExaminationStudent();
                    $exam['examination_centre_id'] = $request->examination_centre_id;
                    $exam['examination_id'] = $request->examination_id;
                    $exam['examination_attendance_types_id'] = $request->examination_attendance_types_id;
                    $exam['student_id'] = $user->id;
                    $exam['created_user_id'] = JWTAuth::user()->id;
                    $exam['candidate_id'] = '';
                    $exam['created'] = Carbon::now()->toDateTimeString();
                    $examStudent = $exam->save();
                    $candidateId = $this->registerCandidateRepository->candidateId($exam);
                }
            }
            if ($examStudent && $request->options && count($request->options)) {
                $subjects = [];
                foreach ($request->options as $subject) {
                    $data = [];
                    $data['id'] = Str::uuid();
                    $data['carry_forward'] = ($subject['carry_forward'] == "No") ? 0 : 1;
                    $data['examination_students_id'] = $exam->id;
                    $data['examination_options_id'] = $subject['option_id'];
                    $data['created_user_id'] = JWTAuth::user()->id;
                    $data['created'] = Carbon::now()->toDateTimeString();
                    $subjects[] = $data;
                }
                ExaminationStudentsOption::insert($subjects);
                foreach ($subjects as $key => $val) {
                    $result[] = [
                        "id" => $val['examination_options_id'] . $val['carry_forward'],
                        "option_id" => $val['examination_options_id'],
                        "carry_forward" => $val['carry_forward'],
                    ];
                }
            } else {
                $result[] = [];
            }
            DB::commit();
            if ($examStudent) {
                $data = [];
                $responseData = [
                    'openemis_no' => $user['openemis_no'],
                    'candidate_id' => $candidateId,
                    'academic_period_id' => $request->academic_period_id,
                    'area_id' => $request->area_id,
                    'first_name' => $user['first_name'],
                    'middle_name' => $user['middle_name'],
                    'third_name' => $user['third_name'],
                    'last_name' => $user['last_name'],
                    'gender_id' => $user['gender_id'],
                    'nationality_id' => $user['nationality_id'],
                    'address' => $user['address'],
                    'address_area_id' => $user['address_area_id'],
                    'birthplace_area_id' => $user['birthplace_area_id'],
                    'postal_code' => $user['postal_code'],
                    'identity_type_id' => $user['identity_type_id'],
                    'identity_number' => $user['identity_number'],
                    'examination_centre_id' => $exam['examination_centre_id'],
                    'examination_attendance_types_id' => $exam['examination_attendance_types_id'],
                    'examination_id' => $exam['examination_id'],
                    'special_need_type_id' => $need['special_need_type_id'],
                    'special_need_difficulty_id' => $need['special_need_difficulty_id'],
                    'options' => $result,
                ];

                return $this->sendSuccessResponse("User added successfully!", $responseData);
            } else {
                return $this->sendErrorResponse("User not added successfully");
            }
        } catch (\Exception $e) {
            echo $e;
            DB::rollback();

            return $this->sendErrorResponse('User not added successfully');
        }
    }

    public function listingView(Request $request)
    {
        try {
            $data = $request->all();
            $total = 0;
            $candidateList1 = SecurityUser::select(
                'id',
                'openemis_no',
                'first_name',
                'last_name',
                'date_of_birth',
                'gender_id'
            )
            ->with(
                [
                    'gender' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'allExaminationStudent' => function ($query) {
                        $query->select('id', 'student_id', 'candidate_id', 'examination_id', 'modified');
                    },
                    'examinationStudent.examName' => function ($query) {
                        $query->select('id', 'code', 'name');
                    },
                ]
            )
            ->where('status', config('constants.studentStatus.activeStd'))
            ->orwhere('is_student', config('constants.students.isStudent'))
            ->orderby('id', 'DESC');
            
            if (isset($data['start']) && isset($data['end'])) {
                $candidateCount = $candidateList1;
                $total = $candidateCount->count();
                $candidateList1->skip($data['start'])
                                ->take($data['end'] - $data['start']);
            } else {
                $candidateCount = $candidateList1;
                $total = $candidateCount->count();
            }

            if (isset($data['search'])) {
                $candidateList1->where(function ($q) use ($data) {
                    $q->where('openemis_no', 'LIKE', "%" . $data['search'] . "%")
                        ->orWhere('first_name', 'LIKE', "%" . $data['search'] . "%")
                        ->orWhere('last_name', 'LIKE', "%" . $data['search'] . "%")
                        ->orWhereHas(
                            'gender',
                            function ($query) use ($data) {
                                $query->where('name', 'LIKE', "%" . $data['search'] . "%");
                            }
                        )
                        ->orWhereHas(
                            'allExaminationStudent',
                            function ($query) use ($data) {
                                $query->where('candidate_id', 'LIKE', "%" . $data['search'] . "%");
                            }
                        )
                        ->orWhereHas(
                            'examinationStudent.examName',
                            function ($query) use ($data) {
                                $query->where('name', 'LIKE', "%" . $data['search'] . "%")
                                    ->orWhere('code', 'LIKE', "%" . $data['search'] . "%");
                            }
                        );
                });
            }

            $list = [];
            $count = 0;

            $candidateList = $candidateList1->get()->map(
                function ($item, $key) use (&$list, &$count) {
                    foreach ($item['allExaminationStudent'] as $key => $value) {
                            $list[$count]['id'] = $item['id'];
                            $list[$count]['openemis_no'] = $item['openemis_no'];
                            $list[$count]['candidate_id'] = $value['candidate_id'];
                            $list[$count]['examination'] = $item['examinationStudent']['examName']['full_name'];
                            $list[$count]['first_name'] = $item['first_name'];
                            $list[$count]['last_name'] = $item['last_name'];
                            $list[$count]['gender_id'] = $item['gender']['name'];
                            $list[$count]['date_of_birth'] = Carbon::parse($item['date_of_birth'])->format('d-m-Y');
                            $count++;
                        }
                    }
                ); 
                
               /* function ($item, $key) {
                    return [
                        "id" => $item['id'],
                        "openemis_no" => $item['openemis_no'],
                        "candidate_id" => $item['allExaminationStudent'][$key]['candidate_id'],
                        "examination" => $item['examinationStudent']['examName']['full_name'],
                        "first_name" => $item['first_name'],
                        "last_name" => $item['last_name'],
                        "gender_id" => $item['gender']['name'],
                        "date_of_birth" => Carbon::parse($item['date_of_birth'])->format('d-m-Y'),
                    ];
                }*/
            

            if ($candidateList->count()) {
                
                $data['candidateRecords'] = $list;
                $data['total'] = $total;

                return $this->sendSuccessResponse('Record found', $data);
            } else {
                return $this->sendSuccessResponse("Record Not Found");
            }
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
        
    }

    /**
     * @param string $candidId
     * @return JsonResponse
     */
    public function candidateView(string $candidId)
    {
        $candidate_view = ExaminationStudent::select(
            'id',
            'candidate_id',
            'examination_centre_id',
            'examination_id',
            'student_id',
            'examination_attendance_types_id',
            'modified_user_id',
            'modified',
            'created_user_id',
            'created'
        )->with(
            'examName:id,code,name,academic_period_id',
            'examinationCentre:id,code,name,area_id',
            'examName.academicPeriod:id,name',
            'examinationCentre.area:id,name',
            'attendanceType:id,name',
            'securityUser:id,openemis_no,first_name,middle_name,third_name,last_name,gender_id,date_of_birth,nationality_id,identity_type_id,identity_number,address,postal_code,address_area_id,birthplace_area_id',
            'securityUser.gender:id,name',
            'securityUser.nationality:id,name',
            'securityUser.identityType:id,name',
            'securityUser.areaAdministrative:id,name',
            'securityUser.birthplaceArea:id,name',
            'securityUser.userSpecialNeedsAssessment:security_user_id,special_need_type_id,special_need_difficulty_id',
            'securityUser.userSpecialNeedsAssessment.specialNeed:id,name',
            'securityUser.userSpecialNeedsAssessment.specialDifficulty:id,name',
            'examinationStudentsOption:examination_students_id,examination_options_id,carry_forward',
            'examinationStudentsOption.examinationOption:id,code,name'
        )->where('candidate_id', $candidId)->get()->map(
            function ($item, $key) use ($candidId) {
                return [
                    "academic_period_id" => $item['examName']['academicPeriod']['name'],
                    "examination_id" => $item['examName']->full_name,
                    "area_id" => $item['examinationCentre']['area']['name'],
                    "examination_centre_id" => $item['examinationCentre']->full_name,
                    "examination_attendance_types_id" => $item['attendanceType']['name'],
                    "openemis_no" => $item['securityUser']['openemis_no'],
                    "candidate_id" => $item['candidate_id'],
                    "first_name" => $item['securityUser']['first_name'],
                    "second_name" => $item['securityUser']['middle_name'],
                    "middle_name" => $item['securityUser']['third_name'],
                    "last_name" => $item['securityUser']['last_name'],
                    "gender_id" => $item['securityUser']['gender']['name'],
                    "date_of_birth" => Carbon::parse($item['securityUser']['date_of_birth'])->format('d-m-Y'),
                    "nationality_id" => $item['securityUser']['nationality']['name'],
                    "identity_type_id" => $item['securityUser']['identityType']['name'],
                    "identity_number" => $item['securityUser']['identity_number'],
                    "address" => $item['securityUser']['address'],
                    "postal_code" => $item['securityUser']['postal_code'],
                    "address_area_id" => $item['securityUser']['areaAdministrative']['name'],
                    "birthplace_area_id" => $item['securityUser']['birthplaceArea']['name'],
                    "special_need_type_id" => $item['securityUser']['userSpecialNeedsAssessment']['specialNeed']['name'],
                    "special_need_difficulty_id" => $item['securityUser']['userSpecialNeedsAssessment']['specialDifficulty']['name'],
                    "options" => $this->getCandidateSubject($candidId),
                    "modified_by" =>  $item['modifiedUser']['full_name'],
                    "modified_on" => Carbon::parse($item['modified'])->format('F d,Y - h:i:s'),
                    "created_by" => $item['createdByUser']['full_name'],
                    "created_on" => Carbon::parse($item['created'])->format('F d,Y - h:i:s'),
                ];
            }
        );
        if ($candidate_view) {
            $results = $candidate_view->toArray();

            return $this->sendSuccessResponse("Registration Candidate details", $results);
        }

        return $this->sendErrorResponse("Registration Candidate View details not found");
    }

    /**
     * getting selected candidate subject details
     * @param string $candidId
     * @return JsonResponse
     */
    protected function getCandidateSubject(string $candidId)
    {
        try {
           $list = [];
           $count = 0;
            $subject = ExaminationStudent::select(
                'id',
                'candidate_id'
            )
                ->with(
                    [
                        'examinationStudentsOption' => function ($query) {
                            $query->select(
                                'examination_students_id',
                                'carry_forward',
                                'examination_options_id'
                            )->groupBy('carry_forward', 'examination_options_id');
                        },
                    ]
                )->with(
                    [
                        'examinationStudentsOption.examinationOption' => function ($query) {
                            $query->select('id', 'code', 'name')->groupBy('id')->orderBy('code');
                        },
                    ]
                )->where('candidate_id', $candidId)->get()->map(
                    function ($item, $key) use (&$list, &$count) {
                       foreach ($item['examinationStudentsOption'] as $key => $value) {
                            $list[$count]['id'] = $value['examinationOption']['id'] . $value->carry_forward;
                            $list[$count]['option_id'] = $value['examinationOption']['id'];
                            $list[$count]['option_code'] = $value['examinationOption']['code'];
                            $list[$count]['option_name'] = $value['examinationOption']['name'];
                            $list[$count]['carry_forward'] = ($value->carry_forward == 0) ? 'No' : 'Yes';
                            $count++;
                        }
                    }
                );

            if ($list) {
                return response()->json(['data' => $list])->getData();
            } else {
                return response()->json(['data' => $list])->getData();
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Suject Not Found');
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function importCandidates(OptionImportRequest $request)
    {
         try{
            $requestParameter = $request->all();
            $validExtensions = config('constants.fileExt.excel');
            $excel =  Excel::toArray(new CandidateExcelImport(), $request->file('file'));
            $import = $this->registerCandidateRepository->candidateImport($excel, $requestParameter);

            Log::info('Candidate Imported in DB.', ['method' => __METHOD__, 'data' => ['import' => $import]]);
            return $this->sendSuccessResponse("Candidate Data Imported Successfully", $import);
        } catch (\Exception $e) {
            echo $e;
                DB::rollback();
                Log::error(
                    'Failed to import Candidate in DB',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );

                return array();
        }
    }

    /**
     * @param string $emisId
     * @return JsonResponse
     */
    public function autocompleteEmis(string $emisId)
    {
        $data = SecurityUser::select("id as key", "openemis_no as value")
            ->where("openemis_no", "LIKE", "%" . $emisId . "%")
            ->get();

        if ($data->count()) {
            $results = $data->toArray();

            return $this->sendSuccessResponse("Emis number found", $results);
        } else {
            return $this->sendErrorResponse('Emis number  not found');
        }
    }

    /**
     * @param string $candidId
     * @return JsonResponse
     */
    public function autocompleteCandidateId(string $candidId)
    {
        $data = ExaminationStudent::select('id as key', 'candidate_id as value')
            ->where("candidate_id", "LIKE", "%" . $candidId . "%")
            ->get();
        if ($data->count()) {
            $results = $data->toArray();

            return $this->sendSuccessResponse(" Candidate id found", $results);
        } else {
            return $this->sendErrorResponse('Candidate id not found');
        }
    }

    /**
     * @param string $candidId
     * @return JsonResponse
     */
    public function editData(string $candId)
    {
        $candidate = SecurityUser::select(
            'id',
            'openemis_no',
            'first_name',
            'middle_name',
            'third_name',
            'last_name',
            'gender_id',
            'date_of_birth',
            'nationality_id',
            'identity_type_id',
            'identity_number',
            'postal_code',
            'address',
            'address_area_id',
            'birthplace_area_id'
        )
            ->with(
                'gender:id,name',
                'nationality:id,name',
                'identityType:id,name',
                'areaAdministrative:id,name',
                'birthplacearea:id,name',
                'examinationStudent.examinationCentre:id,code,name,area_id',
                'examinationStudent.attendanceType:id,name',
                'examinationStudent.examName:id,code,name,academic_period_id',
                'userSpecialNeedsAssessment.specialNeed:id,name',
                'userSpecialNeedsAssessment.specialDifficulty:id,name'
            )
            ->whereHas(
                'examinationStudent',
                function ($query) use ($candId) {
                    $query->where('candidate_id', $candId);
                }
            )->with(
                [
                    "examinationStudent" => function ($query) {
                        $query->select(
                            'id',
                            'student_id',
                            'candidate_id',
                            'examination_id',
                            'examination_centre_id',
                            'examination_attendance_types_id'
                        );
                    },
                ]
            )->orderBy('id', 'DESC')->get()->map(
                function ($item, $key) use ($candId) {
                    return [
                        "candidate_id" => [
                            "key" => $item['examinationStudent']['id'],
                            "value" => $item['examinationStudent']['candidate_id'],
                        ],
                        "openemis_no" => $item['openemis_no'],
                        "first_name" => $item['first_name'],
                        "middle_name" => $item['middle_name'],
                        "third_name" => $item['third_name'],
                        "last_name" => $item['last_name'],
                        "address" => $item['address'],
                        "postal_code" => $item['postal_code'],
                        "gender_id" => [
                            "key" => $item["gender"]["id"],
                            "value" => $item["gender"]["name"],
                        ],
                        "address_area_id" => [
                            "key" => $item["areaAdministrative"]["id"],
                            "value" => $item["areaAdministrative"]["name"],
                        ],
                        "birthplace_area_id" => [
                            "key" => $item["birthplacearea"]["id"],
                            "value" => $item["birthplacearea"]["name"],
                        ],
                        "date_of_birth" => $item['date_of_birth'],
                        "nationality_id" => [
                            "key" => $item["nationality"]["id"],
                            "value" => $item["nationality"]["name"],
                        ],
                        "identity_type_id" => [
                            "key" => $item["identityType"]["id"],
                            "value" => $item["identityType"]["name"],
                        ],
                        "identity_number" => $item['identity_number'],
                        "academic_period_id" => [
                            "key" => $item["examinationStudent"]["examName"]["academicPeriod"]["id"],
                            "value" => $item["examinationStudent"]["examName"]["academicPeriod"]["name"],
                        ],
                        "examination_id" => [
                            "key" => $item["examinationStudent"]["examName"]["id"],
                            "value" => $item["examinationStudent"]["examName"]["name"],
                        ],
                        "area_id" => $item["examinationStudent"]["examinationCentre"]["area"]["name"],
                        "examination_centre_id" => $item["examinationStudent"]["examinationCentre"]["name"],
                        "examination_attendance_types_id" => [
                            "key" => $item["examinationStudent"]["attendanceType"]["id"],
                            "value" => $item["examinationStudent"]["attendanceType"]["name"],
                        ],
                        "special_need_type_id" => [
                            "key" => $item["UserSpecialNeedsAssessment"]["specialNeed"]["id"],
                            "value" => $item["UserSpecialNeedsAssessment"]["specialNeed"]["name"],
                        ],
                        "special_need_difficulty_id" => [
                            "key" => $item["UserSpecialNeedsAssessment"]["specialDifficulty"]["id"],
                            "value" => $item["UserSpecialNeedsAssessment"]["specialDifficulty"]["name"],
                        ],
                        "options" => $this->getCandidateSubject($candId),
                    ];
                }
            );
        if ($candidate->count()) {
            return $this->sendSuccessResponse("Candidate Details Found", $candidate);
        } else {
            return $this->sendErrorResponse("Candidate Detail Not Found");
        }
    }

    /**
     * @param Request $request
     * @param string $candidId
     * @return JsonResponse
     */
    public function updateData(UpdateRequest $request, string $candidId)
    {
        DB::beginTransaction();
        try {
            $validator = $request->validated();
            $std_id = ExaminationStudent::where('candidate_id', $candidId)->first();
            if ($std_id) {
                $s_id = $std_id->student_id;
                $data = SecurityUser::find($s_id);
                $data['first_name'] = $request->first_name;
                $data['middle_name'] = $request->middle_name;
                $data['third_name'] = $request->third_name;
                $data['last_name'] = $request->last_name;
                $data['gender_id'] = $request->gender_id;
                $data['date_of_birth'] = $this->changeDateFormat($request['date_of_birth']['text']);
                $data['nationality_id'] = $request->nationality_id;
                $data['identity_type_id'] = $request->identity_type_id;
                if(isset($request->identity_number)) {
                    $checking = SecurityUser::where('openemis_no', '!=', $data->openemis_no)->where('identity_number', $request->identity_number)->first();
                if($checking){
                    return $this->sendErrorResponse("Identity Number Already Exist");
                } else {
                     $data['identity_number'] = $request->identity_number;
                }
            } else {
                     $data['identity_number'] = $request->identity_number;
                }
                $data['address'] = $request->address;
                $data['postal_code'] = $request->postal_code;
                $data['address_area_id'] = (isset($request['address_area_id'][0]['id'])) ? $request['address_area_id'][0]['id'] : null;
                $data['birthplace_area_id'] = (isset($request['birthplace_area_id'][0]['id'])) ? $request['birthplace_area_id'][0]['id'] : null;
                $data['address'] = $request->address;
                $data['postal_code'] = $request->postal_code;
                $data['modified'] = Carbon::now()->toDateTimeString();
                $saveData = $data->save();
                $std_id = $data->id;
                $exam_std_id = ExaminationStudent::where('student_id', $std_id)->first();
                if ($saveData && $request->special_need_type_id || $request->special_need_difficulty_id) {
                    $user_spl_assmnt = UserSpecialNeedsAssessment::where('security_user_id', $std_id)->first();
                    if($user_spl_assmnt){
                        $user_spl_assmnt['special_need_type_id'] = ($request['special_need_type_id']) ? $request['special_need_type_id'] : 0;
                        $user_spl_assmnt['special_need_difficulty_id'] = ($request['special_need_difficulty_id']) ? $request['special_need_difficulty_id'] : 0;
                        $user_spl_assmnt['security_user_id'] = $std_id;
                        $user_spl_assmnt['date'] = Carbon::now();
                        $user_spl_assmnt['modified_user_id'] = JWTAuth::user()->id;
                        $user_spl_assmnt['modified'] = Carbon::now()->toDateTimeString();
                        $store = $user_spl_assmnt->save();
                    } else {
                        $need = new UserSpecialNeedsAssessment();
                        $need['special_need_type_id'] = (int)$request['special_need_type_id'] > 0 ? $request['special_need_type_id'] : 0;
                        $need['special_need_difficulty_id'] = (int)$request['special_need_difficulty_id'] > 0 ? $request['special_need_difficulty_id'] : 0;
                        $need['date'] = Carbon::now()->toDateTimeString();
                        $need['security_user_id'] = $std_id;
                        $need['created_user_id'] = JWTAuth::user()->id;
                        $need['created'] = Carbon::now()->toDateTimeString();
                        $needanddiff = $need->save();
                    }
                }
                if ($saveData && $request->options && count($request->options)) {
                    $examStudentObject = ExaminationStudentsOption::where(
                        ['examination_students_id' => $exam_std_id->id]
                    )->delete();
                    if (count($request->options)) {
                        $subjects = [];
                        $examStudentObject =[];
                        foreach ($request->options as $subject) {
                            $examStudentObject['id'] = Str::uuid();
                            $examStudentObject['carry_forward'] = ($subject['carry_forward'] == "No") ? 0 : 1;
                            $examStudentObject['examination_students_id'] = $exam_std_id->id;
                            $examStudentObject['examination_options_id'] = $subject['option_id'];
                            $examStudentObject['created_user_id'] = JWTAuth::user()->id;
                            $examStudentObject['created'] = Carbon::now()->toDateTimeString();
                            $subjects[] = $examStudentObject;
                        }
                        ExaminationStudentsOption::insert($subjects);
                    }
                } else {
                    $examStudentObject = ExaminationStudentsOption::where(
                        ['examination_students_id' => $exam_std_id->id]
                    )->delete();
                }
            } else {
                return $this->sendErrorResponse("Candidate Record Not Found");
            }
            DB::commit();
            if ($saveData) {
                return $this->sendSuccessResponse("Candidate Record Updated Successfully!", $saveData);
            } else {
                return $this->sendErrorResponse("Candidate Record Not Updated");
            }
        } catch (\Exception $e) {
            echo $e;
            DB::rollback();

            return $this->sendErrorResponse("Candidate Record Not Found");
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function candidateSearch(Request $request)
    {
        $items = ExaminationStudent::select('id', 'candidate_id', 'examination_id', 'student_id')
            ->where("candidate_id", "LIKE", "%" . $request['query'] . "%")
            ->orWhereHas(
                'securityUser',
                function ($query) use ($request) {
                    $query->where('openemis_no', 'LIKE', "%" . $request['query'] . "%")
                        ->orWhere('first_name', 'LIKE', "%" . $request['query'] . "%")
                        ->orWhere('last_name', 'LIKE', "%" . $request['query'] . "%")
                        ->orWhere('date_of_birth', 'LIKE', "%" . $request['query'] . "%");
                }
            )->orWhereHas(
                'securityUser.gender',
                function ($query) use ($request) {
                    $query->where('name', 'LIKE', "%" . $request['query'] . "%");
                }
            )->with(
                'securityUser:id,openemis_no,first_name,last_name,gender_id,date_of_birth',
                'examName:id,code,name',
                'securityUser.gender:id,name'
            )
            ->get()->map(
                function ($item, $key) {
                    return [
                        "openemis_no" => $item['securityUser']['openemis_no'],
                        "candidate_id" => $item['candidate_id'],
                        "examination" => $item['examName']['name'],
                        "first_name" => $item['securityUser']['first_name'],
                        "last_name" => $item['securityUser']['last_name'],
                        "gender_id" => $item['securityUser']['gender']['name'],
                        "date_of_birth" => Carbon::parse($item['securityUser']['date_of_birth'])->format('d-m-Y'),
                    ];
                }
            );
        if ($items->count()) {
            $res = $items->toArray();

            return $this->sendSuccessResponse("Details Found", $res);
        } else {
            return $this->sendErrorResponse("No Details Found");
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function generateCandidateIds(Request $request)
    {
        try {
            $candidateIds = ExaminationStudent::where('examination_id', $request->examination_id)
                ->where('examination_centre_id', $request->examination_centre_id)
                ->select('id', 'candidate_id')->whereHas(
                    'examName.academicPeriod',
                    function ($query) use ($request) {
                        $query->where('id', $request->academic_period_id);
                    }
                )->get();

            if ($candidateIds->count() > 0) {
                $candidateIds->prepend(['id' => 0, 'candidate_id' => 'All']);
                $candidateIdsArray = $candidateIds->toArray();

                return $this->sendSuccessResponse("Candidate Drop Down Found", $candidateIdsArray);
            } else {
                return $this->sendSuccessResponse("Candidate  Drop Down Not Found");
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Candidate Details Not Fetch');
        }
    }

    public function destroyCandidate(string $candId)
    {
        try {
            $job = new DeleteRegisterCandidate($candId, true);
            $this->dispatchNow($job);
            $response = $job->getResponse();
            if ($response == 1) {
                return $this->sendSuccessResponse("Candidate deleted successfully.");
            } else {
                return $this->sendErrorResponse("Candidate can not be deleted", $response['msg']);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to delete Candidate',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Candidate not deleted.");
        }
    }

    /**
     * Getting Candidate List Using Advance Search
     * @param Request $request
     * @return JsonResponse
     */
    public function advanceSearch(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('Request Parameters Received ', ['method' => __METHOD__, 'data' => ['data' => $data]]);

            $list = $this->candidateService->advanceSearch($data);
            Log::info('Fetched list from DB', ['method' => __METHOD__, 'data' => ['list' => $list]]);

            if (count($list) > 0) {
                return $this->sendSuccessResponse('Candidate list found', $list);
            } else {
                return $this->sendSuccessResponse('Candidate list not found', $list);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get Candidate',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get Candidate");
        }
    }
}

