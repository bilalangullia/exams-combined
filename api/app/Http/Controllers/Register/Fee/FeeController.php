<?php

namespace App\Http\Controllers\Register\Fee;

use App\Http\Controllers\Controller;
use App\Repositories\RegistrationFeeRepository;
use App\Services\Register\Fee\RegistrationFeeService;
use http\Encoding\Stream\Inflate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FeeController extends Controller
{
    /**
     * @var RegistrationFeeRepository
     */
    protected $registrationFeeRepository;

    /**
     * @var RegistrationFeeService
     */
    protected $registrationFeeService;

    /**
     * FeeController constructor.
     * @param RegistrationFeeService $registrationFeeService
     * @param RegistrationFeeRepository $registrationFeeRepository
     */
    public function __construct(
        RegistrationFeeService $registrationFeeService,
        RegistrationFeeRepository $registrationFeeRepository
    ) {
        $this->registrationFeeService = $registrationFeeService;
        $this->registrationFeeRepository = $registrationFeeRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registrationFeeList(Request $request)
    {
        try {
            $list = $this->registrationFeeService->getFeeList($request);
            if (empty($list)) {
                return $this->sendErrorResponse(" Fee Listing Not Found");
            }
            return $this->sendSuccessResponse(" Fee List Found", $list);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Fee list not found");
        }
    }

    /**
     * @param int $feeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function registrationFeeDetails(string $candId, string $examId)
    {
        try {
            $view = $this->registrationFeeService->getFeeDetails($candId, $examId);
            return $this->sendSuccessResponse(" Fee Details Found", $view);
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Fee Details not found");
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registrationFeeAdd(Request $request, string $candId)
    {
        try {
            $addFee = $this->registrationFeeRepository->feeAdd($request, $candId);
            return $this->sendSuccessResponse(" Fee Details Add", $addFee);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Fee Details not added");
        }
    }
}
