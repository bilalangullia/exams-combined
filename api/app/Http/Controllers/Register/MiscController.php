<?php

namespace App\Http\Controllers\Register;

use App\Http\Controllers\Controller;
use App\Models\AcademicPeriod;
use App\Models\Area;
use App\Models\AreaAdministrative;
use App\Models\AttendanceType;
use App\Models\Classification;
use App\Models\EducationFieldOfStudie;
use App\Models\EducationSubject;
use App\Models\Examination;
use App\Models\ExaminationCentre;
use App\Models\ExaminationOption;
use App\Models\ExaminationSubject;
use App\Models\Gender;
use App\Models\ContactOption;
use App\Models\IdentityType;
use App\Models\MarkType;
use App\Models\Nationality;
use App\Models\QualificationLevel;
use App\Models\QualificationSpecialisation;
use App\Models\QualificationTitle;
use App\Models\SpecialNeedDifficulty;
use App\Models\SpecialNeedType;
use App\Models\Statu;
use App\Repositories\MiscRepository;
use App\Services\Register\MiscService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Register\DB;

class MiscController extends Controller
{
    /**
     * @var MiscRepository
     */
    protected $miscRepository;

    /**
     * @var MiscService
     */
    protected $miscService;

    /**
     * MiscController constructor.
     * @param MiscRepository $miscRepository
     * @param MiscService $miscService
     */
    public function __construct(
        MiscRepository $miscRepository,
        MiscService $miscService

    ) {
        $this->miscRepository = $miscRepository;
        $this->miscService = $miscService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function dropdownVals(Request $request)
    {
        $outputData = [];
        if ($request->has('entities')) {
            $entities = explode(',', $request->entities);
            if (in_array('gender', $entities)) {
                $gender = Gender::select('id', 'name')->orderBy('order')->get();
                $outputData['gender_id'] = $gender->toArray();
            }
            if (in_array('nationality', $entities)) {
                $nationalities = Nationality::select('id', 'name')->orderBy('order')->where(['visible' => 1])->get();
                $outputData['nationality_id'] = $nationalities->toArray();
            }
            if (in_array('academicPeriods', $entities)) {
                $academicperiods = AcademicPeriod::select('id', 'name')->orderBy('order', 'desc')->where(['academic_period_level_id' => 1])->get(
                );
                $outputData['academic_period_id'] = $academicperiods->toArray();
            }
            if (in_array('attendanceTypes', $entities)) {
                $attendance = AttendanceType::select('id', 'name')->orderBy('order')->where('visible', 1)->get();
                $outputData['examination_attendance_types_id'] = $attendance->toArray();
            }
            if (in_array('identityTypes', $entities)) {
                $identitytypes = IdentityType::select('id', 'name')->orderBy('order')->where(['visible' => 1])
                    ->get();
                $outputData['identity_type_id'] = $identitytypes->toArray();
            }
            if (in_array('specialNeedTypes', $entities)) {
                $specialneeds = SpecialNeedType::select('id', 'name')->where(['type' => 2])->orderBy('order')->where(
                    ['visible' => 1]
                )->get();
                $outputData['special_need_type_id'] = $specialneeds->toArray();
            }
            if (in_array('specialNeedDifficulties', $entities)) {
                $specialdifficulties = SpecialNeedDifficulty::select('id', 'name')->where(
                    ['visible' => config('constants.visibleValue.visibleCode')]
                )->get();
                $outputData['special_need_difficulty_id'] = $specialdifficulties->toArray();
            }
            if (in_array('country', $entities)) {
                $country = Area::select('id', 'name')->where('area_level_id', 1)->orderBy('order')->where(
                    ['visible' => 1]
                )->get();
                $outputData['country'] = $country->toArray();
            }
            if (in_array('examinationId', $entities)) {
                $examcode = Examination::select('id', 'name', 'code')->get();
                $outputData['examination_id'] = $examcode->toArray();
            }
            if (in_array('status', $entities)) {
                $status = Statu::select('id', 'name')->orderBy('order')->where(
                    ['visible' => 1]
                )->get();
                $outputData['status'] = $status->toArray();
            }
            if (in_array('type', $entities)) {
                $type = MarkType::select('id', 'name')->orderBy('order')->where(
                    ['visible' => 1]
                )->get();
                $outputData['mark_type'] = $type->toArray();
            }
        }

        return $this->sendSuccessResponse("Dropdown values", $outputData);
    }


    /**
     * @return JsonResponse
     */
    public function countryWithRegionDropdown()
    {
        $region = Area::with(
            [
                "region" => function ($query) {
                    $query->select('id', 'code', 'name', 'parent_id');
                },
            ]
        )->select('id', 'code', 'name')->where('area_level_id', 1)->where(['visible' => 1])->get();

        $data = [];
        foreach ($region as $country) {
            $areas = [];
            foreach ($country->region as $area) {
                $areas[] = [
                    'data' => [
                        'id' => "$area->id",
                        'code' => $area->code,
                        'name' => $area->name,
                    ],
                ];
            }

            $data[] = [
                'data' => [
                    'id' => "$country->id",
                    'code' => $country->code,
                    'name' => $country->name,
                ],
                'children' =>
                    $areas,

            ];
        }

        return $this->sendSuccessResponse("Country With Region Dropdown values", $data);
    }

    /**
     * @param int $arealevelid
     * @return JsonResponse
     */
    public function regionDropdown(int $arealevelid)
    {
        $region = Area::where('parent_id', $arealevelid)->where('area_level_id', 2)->where('visible', 1)->select(
            'id',
            'code',
            'name'
        )->get();
        $value = $region->toArray();

        return $this->sendSuccessResponse("Region Dropown values", $value);
    }

    /**
     * @param int $regionId ,$academicId
     * @return JsonResponse
     */
    public function centrecodeDropdwon(int $regionId = null, int $academicId)
    {
        $centrecode = ExaminationCentre::select('id', 'code', 'name')->where(
            'academic_period_id',
            $academicId
        )->get();
        if ($regionId) {
            $centrecode = ExaminationCentre::select('id', 'code', 'name')->where('area_id', $regionId)->where(
                'academic_period_id',
                $academicId
            )->get();
        }
        $value = $centrecode->toArray();

        return $this->sendSuccessResponse("Centre Code Dropown values", $value);
    }

    /**
     * @param int $academicperiodId
     * @return JsonResponse
     */
    public function examinationNameDropdwon(int $academicperiodId)
    {
        $examination = Examination::select('id', 'name')->where('academic_period_id', $academicperiodId)->get();
        $value = $examination->toArray();

        return $this->sendSuccessResponse("Examination Name Dropown values", $value);
    }

    /**
     * description address With region Dropdown
     */
    public function addressWithAreaDropdown()
    {
        $region = AreaAdministrative::with(
            [
                "region" => function ($query) {
                    $query->select('id', 'code', 'name', 'parent_id');
                },
            ]
        )->select('id', 'code', 'name')->where('area_administrative_level_id', 2)->where(['visible' => 1])->get();

        $data = [];
        foreach ($region as $country) {
            $areas = [];
            foreach ($country->region as $area) {
                $areas[] = [
                    'data' => [
                        'id' => "$area->id",
                        'code' => $area->code,
                        'name' => $area->name,
                    ],
                ];
            }

            $data[] = [
                'data' => [
                    'id' => "$country->id",
                    'code' => $country->code,
                    'name' => $country->name,
                ],
                'children' =>
                    $areas,

            ];
        }

        return $this->sendSuccessResponse("Address With Area Dropown values", $data);
    }
    
    /**
     * description address With region Dropdown
     */
    public function areaAdministrativesDropdown()
    {
        $region = AreaAdministrative::select('id', 'code', 'name')->where(['visible' => 1])->get();

        $data = [];
        foreach ($region as $country) {
            
            $data[] = [
                'id' => "$country->id",
                'code' => $country->code,
                'name' => $country->name,
            ];
        }

        return $this->sendSuccessResponse("Area administrative Dropown values", $data);
    }

    /**
     * Getting education program dropdown
     * @return JsonResponse
     */
    public function getEducationProgramDropdown()
    {
        try {
            $educationProgramme = $this->miscRepository->getEducationProgrammeDropDown();
            Log::info(
                'Fetched  data from DB',
                ['method' => __METHOD__, 'data' => ['educationProgDropDown' => $educationProgramme]]
            );

            return $this->sendSuccessResponse("Education Programme Dropdown found", $educationProgramme);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting Education grade on education Programme ID basis
     * @param string $educationProgrammeID
     * @return JsonResponse
     */
    public function getEducationGradeDropDown(string $educationProgrammeID)
    {
        try {
            $educationGrade = $this->miscRepository->getEducationGradeDropDown($educationProgrammeID);
            Log::info(
                'Fetched  data from DB',
                ['method' => __METHOD__, 'data' => ['educationGradeDropdown' => $educationGrade]]
            );

            return $this->sendSuccessResponse("Education Grade Dropdown found", $educationGrade);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting examination type dropdown
     * @return JsonResponse
     */
    public function getExaminationTypeDropDown()
    {
        try {
            $examTypeDropdown = $this->miscRepository->getExaminationTypeDropDown();
            Log::info(
                'Fetched  data from DB',
                ['method' => __METHOD__, 'data' => ['examTypeDropdown' => $examTypeDropdown]]
            );

            return $this->sendSuccessResponse("Examination Type Dropdown found", $examTypeDropdown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam List Not Found");
        }
    }

    /**
     * Getting examination session dropdown
     * @return JsonResponse
     */
    public function getExaminationSessionDropDown()
    {
        try {
            $examTypeDropdown = $this->miscRepository->getExaminationSessionDropDown();
            Log::info(
                'Fetched  data from DB',
                ['method' => __METHOD__, 'data' => ['examSessionDropdown' => $examTypeDropdown]]
            );

            return $this->sendSuccessResponse("Examination Session Dropdown found", $examTypeDropdown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Exam Session Not Found");
        }
    }

    /**
     * Getting examination grading type dropdown
     * @return JsonResponse
     */
    public function getExaminationGradingType()
    {
        try {
            $examinationGradingType = $this->miscRepository->getExaminationGradingTypeDropDown();

            return $this->sendSuccessResponse("Examination Grading Type Dropdown Fornd", $examinationGradingType);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Grading Type Not Found");
        }
    }

    /**
     * Getting examination >> Item tab >> Items dropdown
     * @return JsonResponse
     */
    public function getAdministratorExaminationItemTabItemDropDown()
    {
        try {
            $itemDropdown = $this->miscRepository->itemsDropDown();

            return $this->sendSuccessResponse("Item's Dropdown", $itemDropdown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch item dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Item's Dropdown Not Found");
        }
    }

    /**
     * Getting examination fee dropdown
     * @return JsonResponse
     */
    public function getExaminationFeeDropDown()
    {
        try {
            $feeDropDown = $this->miscRepository->examinationFeeDropdowp();

            return $this->sendSuccessResponse("Examination Fee Dropdwon Found", $feeDropDown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("EXamination Fee Dropdown Not Found");
        }
    }

    /**
     * Getting examination fee amounts dropdown
     * @return JsonResponse
     */
    public function getExaminationFeeAmountDropDown()
    {
        try {
            $feeAmountDropDown = $this->miscRepository->examinationFeeAmountDropdown();

            return $this->sendSuccessResponse("Examination Fee Amount Dropdwon Found", $feeAmountDropDown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("EXamination Fee Amount Dropdown Not Found");
        }
    }

    /**
     * Getting subject list
     * @param $attendanceType
     * @param $examId
     * @return JsonResponse
     */
    public function getSubject(Request $request)
    {
        try {
            $list = [];
            $count = 0;
            $subject = ExaminationOption::select('id', 'code', 'name', 'attendance_type_id', 'examination_subject_id', 'carry_forward')
                ->whereHas(
                    'attendanceType',
                    function ($query) use ($request) {
                        $query->where('id', $request->attendanceType);
                    }
                )->whereHas(
                    'examinationSubject.examName',
                    function ($query) use ($request) {
                        $query->where('id', $request->examId);
                    }
                )->groupBy('id')->orderBy('code')->get()->map(
                    function ($item, $key) {
                            return [
                                'id' => $item['id'].$item->carry_forward,
                                'option_id' => $item['id'],
                                'option_code' => $item['code'],
                                'option_name' => $item['name'],
                                'carry_forward' => ($item['carry_forward'] == 0) ? 'No' : 'Yes'
                            ];
                    }
                );
            if ($subject->count()) {
                return $this->sendSuccessResponse("Subject List Found", $subject);
            } else {
                return $this->sendErrorResponse('Subject Not Found');
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Subject Not Found');
        }
    }

    /*
     * get All Exam Center based on area id and exam id
     * @return JsonResponse
     */
    public function getAllExamCentresByAreaIdAndExamId(int $areaId, int $examId)
    {
        try {
            $examCentre = ExaminationCentre::select(['id', 'code', 'name'])
                ->where('examination_id', $examId)
                ->where('area_id', $areaId);
            $examCentres = $examCentre->orderBy('id', 'DESC')->get();
            Log::info(
                'Fetched list of exam centres based on area id and exam id',
                ['method' => __METHOD__, 'data' => ['examCentres' => $examCentres]]
            );
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list of exam centres based on area id and exam id',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('exam centres not found');
        }
        if (!$examCentres->count()) {
            return $this->sendErrorResponse("exam centres not found");
        }

        return $this->sendSuccessResponse('exam centres found', $examCentres);
    }
    
    /*
     * get All Exam Center based on area id and exam id
     * @return JsonResponse
     */
    public function getAllExamCentres()
    {
        try {
            $examCentre = ExaminationCentre::select(['id', 'code', \DB::raw("TRIM(name) AS name")]);
            $examCentres = $examCentre->orderBy('id', 'ASC')->get();
            Log::info(
                'Fetched list of exam centres',
                ['method' => __METHOD__, 'data' => ['examCentres' => $examCentres]]
            );
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list of exam centres',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('exam centres not found');
        }
        if (!$examCentres->count()) {
            return $this->sendErrorResponse("exam centres not found");
        }

        return $this->sendSuccessResponse('exam centres found', $examCentres);
    }
    
    /**
     * examinations
     */
    public function examinationsDropdwon()
    {
        try {
            $examination = Examination::select(['id', 'code', \DB::raw("TRIM(name) AS name")]);
            $examinations = $examination->orderBy('id', 'ASC')->get();
            Log::info(
                'Fetched list of examinations',
                ['method' => __METHOD__, 'data' => ['examinations' => $examinations]]
            );
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list of examinations',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('examinations not found');
        }
        if (!$examinations->count()) {
            return $this->sendErrorResponse("examinations not found");
        }

        return $this->sendSuccessResponse('Examination Name Dropown values', $examinations);
    }

    /**
     * contactOptions
     */
    public function contactOptionsDropdwon()
    {
        try {
            $contactOption = ContactOption::select(['id', 'code', \DB::raw("TRIM(name) AS name")]);
            $contactOptions = $contactOption->orderBy('id', 'ASC')->get();
            Log::info(
                'Fetched list of contact option',
                ['method' => __METHOD__, 'data' => ['contactOptions' => $contactOptions]]
            );
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list of contact option',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('contact option not found');
        }
        if (!$contactOptions->count()) {
            return $this->sendErrorResponse("contact option not found");
        }

        return $this->sendSuccessResponse('Contact Option Name Dropown values', $contactOptions);
    }
    
    /**
     * @param string $examId
     * @return JsonResponse
     */
    public function getExaminationSubject(string $examId)
    {
        try {
            $examsubjectlist = ExaminationSubject::select('id', 'code', 'name')->where('examination_id', $examId)->get(
            );
            if ($examsubjectlist->count()) {
                return $this->sendSuccessResponse("Examination subject Found", $examsubjectlist);
            }

            return $this->sendErrorResponse("Examination subject Not Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse("Examination subject   Not Found");
        }
    }

    /**
     * @return JsonResponse
     */
    public function getEducationSubject()
    {
        try {
            $subjectlist = EducationSubject::select('id', 'code', 'name')->get();
            if ($subjectlist->count()) {
                return $this->sendSuccessResponse(" subject Found", $subjectlist);
            }

            return $this->sendErrorResponse(" subject Not Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(" subject  Not Found");
        }
    }

    /**
     * fetch classification dropdown for marker
     * @return JsonResponse
     */
    public function getClassificationDropdown()
    {
        try {
            $cals = Classification::select('id', 'name')->get();
            if ($cals->count()) {
                return $this->sendSuccessResponse(" Classification list Found", $cals);
            }
            return $this->sendErrorResponse(" Classification list Not Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse(" Classification  Not Found");
        }
    }
    /**
     * Fetching ownership dropdown.
     * @return JsonResponse
     */
    public function getOwnershipList()
    {
        try {
            $ownerships = $this->miscRepository->getOwnershipList();

            Log::info('Exam centre ownership dropdown fetched from DB', [
                'method' => __METHOD__,
                'data' => ['ownerships' => $ownerships]
            ]);

            if (count($ownerships) > 0) {
                return $this->sendSuccessResponse(" ownerships found", $ownerships);
            } else {
                return $this->sendSuccessResponse(" ownerships not found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch ownership dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse(" ownership  Not Found");
        }
    }

    /**
     * Getting examination Option dropdown
     * @param string $subjectId
     * @return JsonResponse
     */
    public function getExaminationOptionsDropdown(string $subjectId)
    {
        try {
            $optionDropDown = $this->miscRepository->optionDropdown($subjectId);

            return $this->sendSuccessResponse(" Examination Option Dropdown", $optionDropDown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch fee amount dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );


            return $this->sendErrorResponse("Examination Option Dropdown Not Found");
        }
    }

    /**
     * Fetching examination component dropdown
     * @param string $optionId
     * @return JsonResponse
     */
    public function getExaminationComponentsDropdown(string $optionId)
    {
        try {
            $componentDropDown = $this->miscRepository->componentDropdown($optionId);

            return $this->sendSuccessResponse(" Examination Component Dropdown", $componentDropDown);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Examination Component Dropdown Not Found");
        }
    }

    /**
     * Fetching examination Qualification Title dropdown
     * @return JsonResponse
     */
    public function getExaminationQualificationTitle()
    {
        try {
            $title = QualificationTitle::select('id', 'name')->orderby('order', 'asc')->get();
            if ($title->count()) {
                return $this->sendSuccessResponse("Qualification Title Dropdown  Found", $title);
            }
            return $this->sendErrorResponse("Qualification Title Dropdown Not Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification Title Dropdown Not Found");
        }
    }

    /**
     * Fetching examination Qualification Level  dropdown
     * @return JsonResponse
     */
    public function getExaminationQualificationlevel()
    {
        try {
            $qualificationLevel = QualificationLevel::select('id', 'name')->orderby('order', 'asc')->get();
            if ($qualificationLevel->count()) {
                return $this->sendSuccessResponse("Qualification Level Dropdown  Found", $qualificationLevel);
            }
            return $this->sendErrorResponse("Qualification Level Dropdown Not Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification Level Dropdown Not Found");
        }
    }

    /**
     * fetch examination qualification of study list
     * @return JsonResponse
     */
    public function getExaminationEducationFieldOfStudy()
    {
        try {
            $qualificationstudy = EducationFieldOfStudie::select('id', 'name')->orderby('order', 'asc')->get();
            if ($qualificationstudy->count()) {
                return $this->sendSuccessResponse("Qualification Level Dropdown  Found", $qualificationstudy);
            }
            return $this->sendErrorResponse("Qualification Level Dropdown Not Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification Level Dropdown Not Found");
        }
    }

    /**
     * Getting Exam Centre List
     * @param string $examId
     * @return JsonResponse
     */
    public function getExamCentreList(string $examId)
    {
        try {
            $examCentres = $this->miscService->getExamCentreList($examId);
            Log::info('Exam centre dropdown fetched from DB', [
                'method' => __METHOD__,
                'data' => ['examCentres' => $examCentres]
            ]);

            if (count($examCentres) > 0) {
                return $this->sendSuccessResponse("Exam centre dropdown found", $examCentres);
            } else {
                return $this->sendErrorResponse(" Exam centre not found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Dropdown Not Found");
        }
    }

    /**
     * get marker qualification Specialisation dropdown
     * @return JsonResponse
     */
    public function getQualificationSpecialisation()
    {
        try {
            $qualificationSpecialisation = QualificationSpecialisation::select('id', 'name')
                ->orderby('order', 'asc')->get();
            if ($qualificationSpecialisation->count()) {
                return $this->sendSuccessResponse(" Specialisation Dropdown  Found", $qualificationSpecialisation);
            }
            return $this->sendErrorResponse("Qualification Specialisation Level Dropdown Not Found");
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Qualification Specialisation  Dropdown Not Found");
        }
    }
    /**
     * Getting Exam Centre List
     * @param string $examId
     * @return JsonResponse
     */
    public function getCentreExams(string $examId)
    {
        try {
            $examCentres = $this->miscRepository->getExamCentreList($examId);
            Log::info('Exam centre dropdown fetched from DB', [
                'method' => __METHOD__,
                'data' => ['examCentres' => $examCentres]
            ]);

            if (count($examCentres) > 0) {
                return $this->sendSuccessResponse("Exam centre dropdown found", $examCentres);
            } else {
                return $this->sendSuccessResponse(" Exam centre not found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch data DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Exam Centre Dropdown Not Found");
        }
    }

    /**
     * Getting User Group Dropdown
     * @return JsonResponse
     */
    public function getGroupDropdown()
    {
        try {
            $groups = $this->miscRepository->getGroupDropdown();

            if (count($groups) > 0) {
                Log::info('Group dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['groups' => $groups]
                ]);
                return $this->sendSuccessResponse("Group dropdown found", $groups);
            } else {
                Log::info('Group dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Group dropdown not found");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch group dropdown from DB");
        }
    }

    /**
     * @return JsonResponse
     */
    public function educationStructureCycleDropdown()
    {
        try {
            $listing = $this->miscRepository->getEducationStructureCycledropdown();
            return $this->sendSuccessResponse("Cycle Dropdown Found", $listing);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch cycle dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch cycle dropdown from DB");
        }
    }


    /**
     * cycle level dropdown
     * @return JsonResponse
     */
    public function getEducationStructureCycle(string $levelId)
    {
        try {
            $listing = $this->miscRepository->getEducationStructureCycle($levelId);
            return $this->sendSuccessResponse("Cycle Level Dropdown Found", $listing);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch cycle dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch Level dropdown from DB");
        }
    }

    /**
     * Getting Education Level Dropdown
     * @return JsonResponse
     */
    public function getEducationLevelDropdown()
    {
        try {
            $levels = $this->miscService->getEducationLevelDropdown();
            if (count($levels) > 0) {
                Log::info('Education levels dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['levels' => $levels]
                ]);
                return $this->sendSuccessResponse("Education levels dropdown found", $levels);
            } else {
                Log::info('Education levels dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Education levels dropdown not found", $levels);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education group dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }

    /**
     * Getting Education Program Dropdown Via Level Id
     * @return JsonResponse
     */
    public function getEducationProgramDropdownViaLevelId(int $levelId)
    {
        try {
            $programs = $this->miscService->getEducationProgramDropdownViaLevelId($levelId);
            if (count($programs) > 0) {
                Log::info('Education programs dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['programs' => $programs]
                ]);
                return $this->sendSuccessResponse("Education programs dropdown found", $programs);
            } else {
                Log::info('Education programs dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Education programs dropdown not found", $programs);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }

    /**
     * option dropdown
     * @return JsonResponse
     */
    public function getOptionDropdown()
    {
        try {
            $optionlist = $this->miscRepository->getOption();
            if (!empty($optionlist)) {
                return $this->sendSuccessResponse("Examination Option list", $optionlist);
            } else {
                return $this->sendErrorResponse("Failed to fetch Examination Option list");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }
    /**
     *  education certification dropdown
     * @return JsonResponse
     */
    public function getEducationCertificationDropdown()
    {
        try {
            $certificatelist = $this->miscRepository->getEducationCertification();
            return $this->sendSuccessResponse("Education programs dropdown found", $certificatelist);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education group dropdown from DB");
        }
    }

    /**
     * Getting Education Stages Dropdown
     * @return JsonResponse
     */
    public function getEducationStageDropdown()
    {
        try {
            $stages = $this->miscService->getEducationStageDropdown();
            if (count($stages) > 0) {
                Log::info('Education stages dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['stages' => $stages]
                ]);
                return $this->sendSuccessResponse("Education stages dropdown found", $stages);
            } else {
                Log::info('Education stages dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Education stages dropdown not found", $stages);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education stage dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education stage dropdown from DB");
        }
    }

    /**
     *  education level dropdown
     * @return JsonResponse
     */
    public function getEducationLevelForProgrTab()
    {
        try {
            $levellist = $this->miscRepository->educationLevelForProgrTab();
            return $this->sendSuccessResponse("Education programs dropdown found", $levellist);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch education program dropdown from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch education level dropdown from DB");
        }
    }

    /**
     * Getting Education Grades Dropdown
     * @param int $programId
     * @return JsonResponse
     */
    public function getEducationGradeDropdownViaProgramId(int $programId)
    {
        try {
            $grades = $this->miscService->getEducationGradeDropdownViaProgramId($programId);
            if (count($grades) > 0) {
                Log::info('Education grades dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['grades' => $grades]
                ]);
                return $this->sendSuccessResponse("Education grades dropdown found", $grades);
            } else {
                Log::info('Education grades dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Education grades dropdown not found", $grades);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to update education grade details in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to update education grade details in DB");
        }
    }

    /**
     * Getting Education Subjects Dropdown
     * @return JsonResponse
     */
    public function getEducationSubjectDropdown()
    {
        try {
            $subjects = $this->miscService->getEducationSubjectDropdown();
            if (count($subjects) > 0) {
                Log::info('Education subjects dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['subjects' => $subjects]
                ]);
                return $this->sendSuccessResponse("Education subjects dropdown found", $subjects);
            } else {
                Log::info('Education subjects dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Education subjects dropdown not found", $subjects);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get education subject dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get education subject dropdown");
        }
    }

    /**
     * Getting Area administrative Dropdown
     * @return JsonResponse
     */
    public function getAreaAdministrativeDropdown()
    {
        try {
            $areas = $this->miscService->getAreaAdministrativeDropdown();
            if (count($areas) > 0) {
                Log::info('Area administrative dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['areas' => $areas]
                ]);
                return $this->sendSuccessResponse("Area administrative dropdown found", $areas);
            } else {
                Log::info('Area administrative dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Area administrative dropdown not found", $areas);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get area administrative dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get area administrative dropdown");
        }
    }

    /**
     * Getting Country Dropdown
     * @return JsonResponse
     */
    public function getCountryDropdown()
    {
        try {
            $country = $this->miscService->getCountryDropdown();
            if (count($country) > 0) {
                Log::info('Country dropdown fetched from DB', [
                    'method' => __METHOD__,
                    'data' => ['country' => $country]
                ]);
                return $this->sendSuccessResponse("Country dropdown found", $country);
            } else {
                Log::info('Country dropdown fetched from DB', [
                    'method' => __METHOD__
                ]);
                return $this->sendSuccessResponse("Country dropdown not found", $country);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to get country dropdown',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get country dropdown");
        }
    }

    /**
     * Getting System Configuration Dropdown
     * @return JsonResponse
     */
    public function getSystemConfigurationDropdown()
    {
        try {
            $list = $this->miscRepository->systemConfigurationDropdown();
            return $this->sendSuccessResponse("System Configuration dropdown found", $list);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch System Configuration dropdown found from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch System Configuration dropdown found");
        }
    }

    /**
     * @return JsonResponse
     */
    public function getExaminationSubjectList()
    {
        try {
            $list = $this->miscRepository->examinationSubject();
            if (!empty($list)) {
                return $this->sendSuccessResponse("Examination Subject dropdown found", $list);
            } else {
                return $this->sendErrorResponse("Failed to fetch");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch  dropdown found from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch Examination Subject");
        }
    }
    
    /* 
     * Candidate Template  
     * Date: 10/7/2020    
     */
    public function getCandidateTemplate(){
        try {
            
            $outputData = [];
            
            $outputData['Data']['header'] = [
                "Candidate ID (Leave as blank for new entries)",
                "First Name",
                "Middle Name",
                "Third Name",
                "Last Name",
                "Preferred Name",
                "Gender Code (M/F)",
                "Date Of Birth ( DD/MM/YYYY )",
                "Address",
                "Postal",
                "Address Area Code",
                "Birthplace Area Code",
                "Account Type Code",
                "Nationality Id",
                "Identity Type Code",
                "Identity Number",
                "Contact Type",
                "Contact",
                "Special Needs Assessment Date ( DD/MM/YYYY ) (Optional)",
                "Special Needs Type (Optional)",
                "Special Needs Difficulties (Optional)",
                "Exam Centre",
                "Attendance Type",
                "Option 1",
                "CF",
                "Option 2",
                "CF",
                "Option 3",
                "CF",
                "Option 4",
                "CF",
                "Option 5",
                "CF",
                "Option 6",
                "CF",
                "Registration Fee Type (Optional)",
                "Payment Date ( DD/MM/YYYY ) (Optional)",
                "Amount Paid ($) (Optional)",
                "Receipt Number (Optional)"
            ];
            
            $outputData['References'] = [];
            
            $outputData['References']['Gender']['header'] = ['Name', 'Code'];
            $gender = Gender::select('*')->orderBy('order')->get()->pluck('id', 'name');
            $outputData['References']['Gender']['data'] = $gender->toArray();
            
            $outputData['References']['AddressArea']['header'] = ['Name', 'Code'];
            $addressArea = AreaAdministrative::select('id', 'code', 'name')->where(['visible' => 1])
                            ->get()->pluck('code', 'name');            
            $outputData['References']['AddressArea']['data'] = $addressArea->toArray();
            
            $outputData['References']['BirthplaceArea']['header'] = ['Name', 'Code'];
            $birthplaceArea = AreaAdministrative::select('id', 'code', 'name')->where(['visible' => 1])
                            ->get()->pluck('code', 'name');            
            $outputData['References']['BirthplaceArea']['data'] = $birthplaceArea->toArray();
            
            $outputData['References']['AccountType']['header'] = ['Name', 'Code'];                        
            $outputData['References']['AccountType']['data'] = [
                "Students" => "STU",
                "Staff" => "STA",
                "Guardians" => "GUA",
                "Others" => "OTH"
            ];
            
            $outputData['References']['Nationality']['header'] = ['Name', 'Code'];
            $nationalities = Nationality::select('id', 'name')->orderBy('order')->where(['visible' => 1])
                            ->get()->pluck('id', 'name');            
            $outputData['References']['Nationality']['data'] = $nationalities->toArray();
            
            $outputData['References']['IdentityType']['header'] = ['Name', 'National Code'];
            $identityTypes = IdentityType::select('id', 'name')->orderBy('order')->where(['visible' => 1])
                            ->get()->pluck('id', 'name');            
            $outputData['References']['IdentityType']['data'] = $identityTypes->toArray();
            
            $outputData['References']['ContactType']['header'] = ['Name', 'National Code'];
            $contactOptions = ContactOption::select(['id', 'code', \DB::raw("TRIM(name) AS name")])
                            ->orderBy('id', 'ASC')->get()->pluck('id', 'name'); 
            $outputData['References']['ContactType']['data'] = $contactOptions->toArray();
            
            $outputData['References']['SpecialNeedsType']['header'] = ['Name', 'Code'];
            $specialNeedsTypes = SpecialNeedType::select('id', 'name')->orderBy('order')->where(['visible' => 1, 'type' => 2])
                            ->get()->pluck('id', 'name');            
            $outputData['References']['SpecialNeedsType']['data'] = $specialNeedsTypes->toArray();
            
            $outputData['References']['SpecialNeedsDifficulties']['header'] = ['Name', 'Code'];
            $specialNeedDifficulties = SpecialNeedDifficulty::select('id', 'name')->where(
                                ['visible' => config('constants.visibleValue.visibleCode')]
                            )->get()->pluck('id', 'name');            
            $outputData['References']['SpecialNeedsDifficulties']['data'] = $specialNeedDifficulties->toArray();
            
            $outputData['References']['ExamCentre']['header'] = ['Name', 'Code'];
            $examCentres = ExaminationCentre::select(['id', 'code', \DB::raw('CONCAT(code, "-", TRIM(name)) AS name')])
                    ->orderBy('id', 'ASC')->get()->pluck('id', 'name');                       
            $outputData['References']['ExamCentre']['data'] = $examCentres->toArray();
            
            $outputData['References']['AttendanceType']['header'] = ['Name', 'Code'];
            $attendanceTypes = AttendanceType::select('id', 'name')->orderBy('order')->where('visible', 1)
                    ->get()->pluck('id', 'name');                       
            $outputData['References']['AttendanceType']['data'] = $attendanceTypes->toArray();
            
            $outputData['References']['Option']['header'] = ['Name', 'Code'];
            $examinationOptions = ExaminationOption::select(['id', 'code', \DB::raw('CONCAT(code, "-", TRIM(name)) AS name')
                    ])
                    ->orderBy('id', 'ASC')->get()->pluck('id', 'name');                       
            $outputData['References']['Option']['data'] = $examinationOptions->toArray();
            
            $outputData['References']['CarryForward']['header'] = ['Name', 'Code'];                        
            $outputData['References']['CarryForward']['data'] = [
                "No" => 0,
                "Yes" => 1
            ];
            
            $outputData['References']['ExaminationFeeType']['header'] = ['Name', 'Code'];                        
            $outputData['References']['ExaminationFeeType']['data'] = [
                "Registration Fee" => 1,
                "Late Payment Fee" => 2
            ];
            
            if (!empty($outputData)) {
                return $this->sendSuccessResponse("Candidate Template", $outputData);
            } else {
                return $this->sendErrorResponse("Failed to fetch");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch  dropdown found from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch");
        }
    }
    
}
