<?php

namespace App\Http\Controllers\Authentication\Authentication;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate(
            [
                'name' => 'required|string|unique:users',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string|same:c_password',
                'c_password' => 'required',
            ]
        );
        $user = new User(
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]
        );
        $record = $user->save();
        if ($record) {
            return $this->sendSuccessResponse(200, "User added successfully!", $user);
        } else {
            return $this->sendErrorResponse(404, "User not added successfully");
        }
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $data = [];
        $request->validate(
            [
                'name' => 'required|string',
                'password' => 'required|string',
            ]
        );
        $credentials = request(['name', 'password']);
        if (!Auth::attempt($credentials)) {
            return $this->sendErrorResponse(404, "Unauthorized userd");
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $record = $token->save();
        if ($record) {
            $data = [
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'user' => $user,
            ];

            return $this->sendSuccessResponse(200, "User login successfully!", $data);
        } else {
            return $this->sendErrorResponse(404, "User unable to login");
        }
        /*return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);*/
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $result = $request->user()->token()->revoke();
        if ($result) {
            return $this->sendSuccessResponse(200, "Logout successfully!");
        } else {
            return $this->sendErrorResponse(404, "Something went wrong");
        }
        /*return response()->json([
            'message' => 'Successfully logged out'
        ]);*/
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $userObj = $request->user();
        if ($userObj) {
            return $this->sendSuccessResponse(200, "User record!", $userObj);
        } else {
            return $this->sendErrorResponse(404, "Something went wrong");
        }
    }
}