<?php

namespace App\Http\Controllers\Register;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use App\Jobs\ProcessReport;
use App\Repositories\Areas\AreaRepository;
use App\Repositories\ExaminationCentres\ExaminationCentreRepository;
use App\Repositories\Examinations\ExaminationRepository;
use App\Repositories\ExaminationStudents\ExaminationStudentRepository;
use App\Repositories\Report\ReportRepository;
use App\Services\Report\ReportService;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PDF;
use Response;
use Storage;
use Validator;

class ReportController extends Controller
{

    /**
     * The report service instance.
     *
     * @var ReportService
     * @var ReportRepository
     * @var AreaRepository
     * @var ExaminationRepository
     * @var ExaminationCentreRepository
     * @var ExaminationStudentRepository
     */
    protected $reportService;
    protected $reportRepository;
    protected $areaRepository;
    protected $examinationRepository;
    protected $examinationCentreRepository;
    protected $examinationStudentRepository;

    /**
     * Create a new controller instance.
     *
     * @param ReportService $reportService
     * @param ReportRepository $reportRepository
     * @param AreaRepository $areaRepository
     * @param ExaminationRepository $examinationRepository
     * @param ExaminationStudentRepository $examinationStudentRepository
     * @param ExaminationStudentRepository
     * @return void
     */
    public function __construct(
        ReportService $reportService,
        ReportRepository $reportRepository,
        AreaRepository $areaRepository,
        ExaminationRepository $examinationRepository,
        ExaminationCentreRepository $examinationCentreRepository,
        ExaminationStudentRepository $examinationStudentRepository

    )
    {
        $this->reportService = $reportService;
        $this->reportRepository = $reportRepository;
        $this->areaRepository = $areaRepository;
        $this->examinationRepository = $examinationRepository;
        $this->examinationCentreRepository = $examinationCentreRepository;
        $this->examinationStudentRepository = $examinationStudentRepository;
    }

    public function generateReport(ReportRequest $request)
    {

        $areaNameCode = $this->areaRepository->getAreaNameCodeByAreaId($request->area_id);
        if (empty($areaNameCode)) {
            return $this->sendErrorResponse("No such area found");
        }
        $examName = $this->examinationRepository->getExamName($request->examination_id);
        if (empty($examName)) {
            return $this->sendErrorResponse("No such exam found");
        }
        $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
            $request->examination_centre_id
        );
        if (empty($examCenterNameCode)) {
            return $this->sendErrorResponse("No such exam centre found");
        }
        $candidates = $this->examinationStudentRepository->getCandidatesByExamIdAndExamCenterId(
            $request->examination_id,
            $request->examination_centre_id,
            $request->candidate
        );
        if (empty($candidates)) {
            return $this->sendErrorResponse("No candidate found");
        }
        $reportData = $this->reportService->formatReportData(
            $areaNameCode,
            $examName,
            $examCenterNameCode,
            $candidates
        );
        if (empty($reportData)) {
            return $this->sendErrorResponse("No report data found");
        }
        $status = $this->reportService->saveUpdateReport($reportData, $request->all());
        if (!empty($status['msg'])) {
            return $this->sendErrorResponse($status['msg']);
        }

        return $this->sendSuccessResponse("Report generated and saved successfully");
    }
    
    public function getPercentageReports($id){
        
        $reportProgress = $this->reportService->getPercentageReportService($id);
        
        if($reportProgress->status == config('constants.reportUpdateStatus.completed')){
            $status = "Completed";
            $percent = 100;
        }else{
            $currentRecords = $reportProgress->current_records;
            $totalRecords = $reportProgress->total_records;            
            $percent = ceil((($currentRecords/$totalRecords)*100));
            if($percent == 100 && $reportProgress->status == config('constants.reportUpdateStatus.completed')){
                $status = "Completed";                
            }elseif($percent == 100 && $reportProgress->status == config('constants.reportUpdateStatus.processing')){
                $percent = 99;
                $status = "Processing";                
            }else{                
                $status = "Processing";
            }
        }
        
        $reportStatus = ['status' => $status, 'percent' => $percent];
        
        return $this->sendSuccessResponse("Report progress and status", $reportStatus);
    }

    /**
     * Getting Reports List
     * @param array $data
     * @return array
     */
    public function listReports(Request $request)
    {
        try {
            $data = $request->all();
            $reports = $this->reportService->listReports($data);
            if (empty($reports)) {
                return $this->sendSuccessResponse("No reports found");
            } else {
                return $this->sendSuccessResponse("Report listing Found", $reports);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to fetch list from DB");
        }
    }

    /**
     * To download the pdf report from url
     *
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */

    public function downloadReport(Request $request)
    {
        print_r(URL::to('/'));
        dd;
        $url = $request->url;
        $fileName = basename($url);
        if (Storage::disk('local')->exists('public/reports/' . $fileName)) {
            return response()->download(public_path("storage/reports/" . $fileName));
        } else {
            return $this->sendErrorResponse("Report does not exist");
        }
    }

    /**
     * processReport to generate and process the registration report for all and individual Candidate
     *
     * @param ReportRequest $request
     * @return JsonResponse|string
     */
    public function processReport(ReportRequest $request)
    {
        $areaNameCode = $this->areaRepository->getAreaNameCodeByAreaId($request->area_id);
        if (empty($areaNameCode)) {
            return $this->sendErrorResponse("No such area found");
        }
        $examName = $this->examinationRepository->getExamName($request->examination_id);
        if (empty($examName)) {
            return $this->sendErrorResponse("No such exam found");
        }
        $examCenterNameCode = $this->examinationCentreRepository->getExamCenterNameCodeByExamCenterId(
            $request->examination_centre_id
        );
        if (empty($examCenterNameCode)) {
            return $this->sendErrorResponse("No such exam centre found");
        }
        $candidates = $this->examinationStudentRepository->getCandidatesByExamIdAndExamCenterId(
            $request->examination_id,
            $request->examination_centre_id,
            $request->candidate
        );
        if (empty($candidates)) {
            return $this->sendErrorResponse("No candidate found");
        }
        $reportData = $this->reportService->formatReportData(
            $areaNameCode,
            $examName,
            $examCenterNameCode,
            $candidates
        );
        if (empty($reportData)) {
            return $this->sendErrorResponse("No report data found");
        }
        $status = $this->processQueueReport($reportData, $request->all());

        return $status;
    }

    /**
     * To add the report in queue
     *
     * @param array $reportData
     * @param array $request
     * @return JsonResponse|string
     */
    public function processQueueReport($reportData, $request)
    {
        try {
            //ProcessReport::dispatch($reportData, $request)->onQueue("processing");
            dispatch(new ProcessReport($reportData, $request))->onQueue("reports");
            Log::info('Handle to process the report in queue', ['method' => __METHOD__, 'data' => ['reportData' => $reportData, 'requestData' => $request]]);
        } catch (\Exception $e) {
            Log::error(
                'Failed to handle to process the report in queue',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $error['msg'] = "something went wrong";
        }

        return $this->sendSuccessResponse("Report generated and saved successfully");
    }
}
