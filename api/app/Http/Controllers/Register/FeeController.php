<?php

namespace App\Http\Controllers\Register;

use App\Http\Controllers\Controller;
use App\Models\ExaminationFee;
use App\Models\ExaminationStudent;
use App\Services\Fee\FeeService;
use DB;
use Excel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Redirect;
use Response;

class FeeController extends Controller
{
    /**
     * fee service instance
     * @var FeeService
     */
    protected $feeService;

    /**
     * FeeController constructor.
     * @param FeeService $feeService
     */
    public function __construct(
        FeeService $feeService

    ) {
        $this->feeService = $feeService;
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function examFee(int $id)
    {
        $amount = ExaminationFee::select('id', 'amount')->where('examinations_id', $id)->whereHas(
            "feeType",
            function ($query) {
                $query->where('name', 'Registration FeeController');
            }
        )->first();
        $value = $amount->toArray();

        return $this->sendSuccessResponse("FeeController amount", $value);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getFeeList(Request $request)
    {
        try {
            $fees = $this->feeService->feeList($request);
            return $this->sendSuccessResponse("Fee List Found", $fees);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Fee List Not Found');
        }
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function getSingleFeeDetail(string $id)
    {
        $feeData = ExaminationStudent::select('id', 'candidate_id', 'examination_centre_id', 'student_id')
            ->with(
                'securityUser:id,openemis_no,first_name,middle_name,third_name,last_name',
                'examinationCentre:id,code,name',
                'examinationStudentsFee'
            )
            ->where('candidate_id', $id)
            ->get();
        if ($feeData) {
            return $this->sendSuccessResponse("Candidate Registration FeeController Details Found", $feeData);
        } else {
            return $this->sendErrorResponse("Candidate Registration FeeController Details Not Found");
        }
    }
}