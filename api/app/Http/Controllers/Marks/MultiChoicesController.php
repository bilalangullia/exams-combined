<?php

namespace App\Http\Controllers\Marks;

use App\Http\Controllers\Controller;
use App\Imports\MultipleChoiceExcelImport;
use App\Repositories\CandidateRepository;
use App\Repositories\SubjectRepository;
use App\Repositories\MultipleChoiceImportRepository;
use App\Services\Marks\MultiChoicesService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use File;
use App\Http\Requests\MultipleChoiceImportRequest;

class MultiChoicesController extends Controller
{
    /**
     * CandidateRepository instance
     * @var CandidateRepository
     */
    protected $candidateRepository;

    /**
     * MultiChoicesService instance
     * @var MultiChoicesService
     */
    protected $multiChoicesService;

    /**
     * SubjectRepository instance
     * @var SubjectRepository
     */
    protected $subjectRepository;

    /**
     * MultipleChoiceImportRepository instance
     * @var MultipleChoiceImportRepository
     */
    protected $multipleChoiceImportRepository;

    /**
     * MultiChoicesController constructor
     * @param CandidateRepository $candidateRepository
     * @param SubjectRepository $subjectRepository
     * @param MultiChoicesService $multiChoicesService
     * @param MultipleChoiceImportRepository $multipleChoiceImportRepository
     */
    public function __construct(
        CandidateRepository $candidateRepository,
        SubjectRepository $subjectRepository,
        MultiChoicesService $multiChoicesService,
        MultipleChoiceImportRepository $multipleChoiceImportRepository
    ) {
        $this->multiChoicesService = $multiChoicesService;
        $this->candidateRepository = $candidateRepository;
        $this->subjectRepository = $subjectRepository;
        $this->multipleChoiceImportRepository = $multipleChoiceImportRepository;
    }

    /**
     * Multiple-choice-candidate's-listing
     * @param Request $request
     * @return JsonResponse
     */
    public function getMultipleChoiceCandidateList(Request $request)
    {
        try {
            $data = $this->multiChoicesService->getCandidateList($request);
            
            return $this->sendSuccessResponse("Candidate List Found", $data);
            } catch (\Exception $e) {
            Log::error($e);

            return $this->sendErrorResponse('Candidate List Not Found');
        }
    }

    /**
     * Getting multiple-choice subject dropdown
     * @param Request $request
     * @return JsonResponse
     */
    public function getMultipleChoiceSubjectDropDown(Request $request)
    {
        try {
            $data = $this->multiChoicesService->getSubjectList($request);

            return $this->sendSuccessResponse("Subject List Found", $data);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Subject List Not Found');
        }
    }

    /**
     * Getting multiple-choice component dropdown
     * @param Request $request
     * @return JsonResponse
     */
    public function getMultipleChoiceComponentDropDown(Request $request)
    {
        try {
            $record = $this->multiChoicesService->getComponentList($request);

            return $this->sendSuccessResponse("Component List Found", $record);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Component List Not Found');
        }
    }

    /**
     * Getting multiple-choice question dropdown
     * @param Request $request
     * @return JsonResponse
     */
    public function getMultipleChoiceQuestionDropDown(Request $request)
    {
        try {
            $record = $this->subjectRepository->getQuestionDropDown($request);

            return $this->sendSuccessResponse("Question List Found", $record);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Question List Not Found');
        }
    }

    /**
     * Getting multiple-choice candidate view
     * @param Request $request
     * @param string $candidId
     * @return JsonResponse
     */
    public function getMultipleChoiceCandidateDetails(Request $request, string $candidId)
    {
        try {
            $listing = $this->multiChoicesService->multiChoiceView($request, $candidId);

            return $this->sendSuccessResponse("Candidate View Found", $listing);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Candidate View Not Found');
        }
    }

    /**
     * Storing or updating candidate's multiple-choice response
     * @param Request $request
     * @param string $candId
     * @return \Exception|JsonResponse
     */
    public function storeCandidateMultipleChoiceResponse(Request $request, string $candId, int $compId)
    {
        try {
            $update_data = $this->candidateRepository->getCandidateExistResponse($request, $candId);
            $data_update = $this->multiChoicesService->updateCandidateResponse($request, $candId, $compId);

            return $this->sendSuccessResponse("Candidate Record Update Successfully", $data_update);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Candidate Record Not Update!');
        }
    }

    /**
     * Getting multiple-choice selected candidate response
     * @param Request $request
     * @param string $candId
     * @return JsonResponse
     */
    public function getMultiChoiceCandidateResponse(Request $request, string $candId)
    {
        try {
            $savingCandidateResponse = $this->multiChoicesService->getCandidateResponse($request, $candId);

            return $this->sendSuccessResponse("Candidate Record Generated Successfully", $savingCandidateResponse);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Candidate Record Not Generated!');
        }
    }

    /**
     * Getting multiple-choice all candidate response
     * @param Request $request
     * @return JsonResponse
     */
    public function getMultipleChoiceAllCandidateResponse(Request $request)
    {
        try {
            $allCandidateResponseList = $this->candidateRepository->getAllCandidateResponse($request);

            return $this->sendSuccessResponse("Candiadtes Record found", $allCandidateResponseList);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Candidate Record Not Found!');
        }
    }

    /**
     * Importing Multiple Choice Excel File.
     * @param MultipleChoiceImportRequest $request
     * @return JsonResponse
     */
    public function importMulitpleChoice(MultipleChoiceImportRequest $request)
    {
        try {
            $requestParameter =  $request->all();

            Log::info(
                'Request Parameters Received ',
                [
                    'method' => __METHOD__,
                    'data' => [
                        'requestParameter' => $requestParameter
                    ]
                ]
            );

            $validExtension = ['xlsx', 'xls', 'csv'];
            $extension = File::extension($request->template->getClientOriginalName());
            if (!in_array($extension, $validExtension)) {
                return $this->sendErrorResponse("Not a valid file extension");
            }

            $header = ['Candidate ID', 'Question Num', 'Response'];
            $results = Excel::toArray(new MultipleChoiceExcelImport(), $request->file('template'));

            if (empty($results[0][1])) {
                return $this->sendErrorResponse("Header is not present");
            }

            if (empty($results[0][2])) {
                return $this->sendErrorResponse("imported file is empty");
            }

            if (!in_array($results[0][1][0], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($results[0][1][1], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }
            if (!in_array($results[0][1][2], $header)) {
                return $this->sendErrorResponse("Not a valid header");
            }

            $rowsCount = count($results[0]) - 2;
            if ($rowsCount > 2000) {
                return $this->sendErrorResponse("File can not have more than 2000 records.");
            }

            $import = $this->multipleChoiceImportRepository->importMulitpleChoice($results, $requestParameter);
            Log::info('Fetched details from DB', ['method' => __METHOD__, 'data' => ['import' => $import]]);

            if (count($import) > 0) {
                return $this->sendSuccessResponse("Multiple Choice Data Imported.", $import);
            } else {
                return $this->sendErrorResponse("Multiple Choice Data Not Imported.");
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to import multiple choice response in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import multiple choice response in DB");
        }
    }

    public function generateAllCandidateMark(Request $request)
    {
        try{
            $generate = $this->multiChoicesService->multipleChoiceGenerateButton($request);

            return $this->sendSuccessResponse("Candidate's Marks Generated Successfully", $generate);
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to import multiple choice response in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to Generate Marks");
        }
    }
}
