<?php

namespace App\Http\Controllers\Marks;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarkComponentAddRequest;
use App\Http\Requests\MarksComponentImportRequest;
use App\Repositories\MarkRepository;
use App\Services\Marks\MarkService;
use Illuminate\Http\Request;
use Response;

class ComponentController extends Controller
{
    /**
     * @var MarkService
     */
    protected $markService;

    /**
     * @var MarkRepository
     */
    protected $markRepository;

    /**
     * ComponentController constructor.
     * @param MarkService $markService
     */
    public function __construct(
        MarkService $markService,
        MarkRepository $markRepository
    ) {
        $this->markService = $markService;
        $this->markRepository = $markRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComponentoptionList(Request $request)
    {
        try {
            $optionlist = $this->markService->OptionDropdown($request);
            if (empty($optionlist)) {
                return $this->sendErrorResponse(" Subject Option  Not Found");
            }
            return $this->sendSuccessResponse(" Subject Option Found", $optionlist);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Data Not Found ');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComponentTab(Request $request)
    {
        try {
            $component = $this->markService->markComponentTab($request);
            if (empty($component)) {
                return $this->sendErrorResponse(" Component Tab Not Found");
            }
            return $this->sendSuccessResponse(" Component Tab Found", $component);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Data Not Found ');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatusDropdown(Request $request)
    {
        try {
            $dropdown = $this->markRepository->statusDropdownVals();
            if (empty($dropdown)) {
                return $this->sendErrorResponse("Status dropdown Not Found");
            }
            return $this->sendSuccessResponse(" Status dropdown Found", $dropdown);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Data Not Found ');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMarkTypeDropdown(Request $request)
    {
        try {
            $dropdown = $this->markRepository->markTypedropdownVals();
            if (empty($dropdown)) {
                return $this->sendErrorResponse("Mark dropdown Not Found");
            }
            return $this->sendSuccessResponse(" Mark dropdown Found", $dropdown);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Data Not Found ');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComponentMarkList(Request $request)
    {
        try {
            $list = $this->markService->markComponentListing($request);
            if (empty($list)) {
                return $this->sendErrorResponse(" List Not Found");
            } else {
                return $this->sendSuccessResponse("List Found", $list);
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Data Not Found ');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function candidateDetailsViaRequestParam(Request $request)
    {
        try {
            $details = $this->markRepository->detailsByCandidate($request);
            if (empty($details)) {
                return $this->sendErrorResponse(" candidate marks details Not Found");
            } else {
                return $this->sendSuccessResponse("candidate marks details  Found", $details);
            }
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Data Not Found ');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function candidateMarkAdd(MarkComponentAddRequest $request)
    {
        try {
            $candidate = $this->markRepository->addMark($request);
            if (!$candidate) {
                return $this->sendErrorResponse(" Candidate Marks Not Add");
            }
            return $this->sendSuccessResponse("Candidate Marks Add", $candidate);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Candidate Marks Not Add');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function marksComponentSearch(Request $request)
    {
        try {
            $candidate = $this->markService->marksSearch($request);
            if (!$candidate) {
                return $this->sendErrorResponse(" Candidate marks search not found");
            }
            return $this->sendSuccessResponse("Candidate Marks search found", $candidate);
        } catch (\Exception $e) {
            return $this->sendErrorResponse('Candidate Marks Not Add');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function marksComponentImport(MarksComponentImportRequest $request)
    {
        try {
            $import = $this->markRepository->importmMarksComponent($request);
            if ($import == false) {
                return $this->sendErrorResponse("Please check Excel File");
            }
            return $this->sendSuccessResponse("Marks Imported Successfully", $import);
        } catch (\Exception $e) {
            Log::error(
                'Failed to import forecast grades in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import Marks in DB");
        }
    }
}
