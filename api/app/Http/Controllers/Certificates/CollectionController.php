<?php

namespace App\Http\Controllers\Certificates;

use App\Http\Controllers\Controller;
use App\Http\Requests\CollectionRequest;
use App\Repositories\CollectionRepository;
use App\Services\Certificates\CollectionService;
use App\Http\Requests\UpdateCollectionRequest;
use App\Http\Requests\CollectionImportRequest;
use Illuminate\Http\Request;
use File;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CollectionImport;
use Illuminate\Support\Facades\Log;

class CollectionController extends Controller
{
    protected $collectionRepository;
    protected $collectionService;

    public function __construct(
    	CollectionRepository $collectionRepository,
    	CollectionService $collectionService
    ) {
    	$this->collectionRepository = $collectionRepository;
    	$this->collectionService = $collectionService;
    }

    public function getCertificatesCollectionList(Request $request)
    {
    	try{
    		$list = $this->collectionService->getCollectionList($request);

    		return $this->sendSuccessResponse("Collection List Found", $list);
    	} catch (\Exception $e) {
			Log::error(
                'Failed to fetch list from DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );

            return $this->sendErrorResponse('Collection List Not Found');
        }
    }

    public function getCertificateCollectionDetail(int $collectionId)
    {
    	try{
    		$detail = $this->collectionService->getCollectionDetail($collectionId);

    		return $this->sendSuccessResponse("Collection Detail Found", $detail);
    	} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Collection Detail Not Found');
		}
    }

    public function storeCertificateCollection(CollectionRequest $request)
    {
    	try{
    		$data = $request->all();
    		$record = $this->collectionRepository->addCollection($data);
            if($record == 2){
                return $this->sendSuccessResponse("Collection Added Successfully", true);
            } else{
                return $this->sendErrorResponse('Certificate Id already Exist', false);
            }
    	} catch (\Exception $e) {
            echo $e;
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Collection Not Added');
		}
    }


    public function updateCertificateCollection(int $collectionId, UpdateCollectionRequest $request)
    {
    	try{
    		$updateData = $request->all();
    		$update = $this->collectionRepository->updateCollection( $collectionId, $updateData);

    		return $this->sendSuccessResponse("Collection Details Updated Successfully", $update);
		} catch (\Exception $e) {
			Log::error(
				'Failed to fetch list from DB',
				['message'=> $e->getMessage(), 'trace' => $e->getTraceAsString()]
			);

			return $this->sendErrorResponse('Collection Not Updated');
		}
    }

    public function collectionImport(CollectionImportRequest $request)
    {
        try {
            $import = $this->collectionRepository->collectionImport($request);

            return $this->sendSuccessResponse("Collection Imported Successfully", $import);
        } catch (\Exception $e) {
            echo $e;
            Log::error(
                'Failed to import forecast grades in DB',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to import forecast grades in DB");
        }
    }
}
