<?php

namespace App\Http\Controllers\Certificates;

use App\Http\Controllers\Controller;
use App\Http\Requests\DuplicateCertficatImportRequest;
use App\Http\Requests\DuplicatesCertificateAddRequest;
use App\Http\Requests\DuplicatesCertificateUpdateRequest;
use App\Repositories\DuplicatesCertificateRepository;
use App\Services\Certificates\DuplicatesCertificateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DuplicateController extends Controller
{
    /**
     * @var DuplicatesCertificateRepository
     */
    protected $duplicatesCertificateRepository;

    /**
     * @var DuplicatesCertificateService
     */
    protected $duplicatesCertificateService;

    /**
     * DuplicateController constructor.
     * @param DuplicatesCertificateService $duplicatesCertificateService
     * @param DuplicatesCertificateRepository $duplicatesCertificateRepository
     */

    public function __construct(
        DuplicatesCertificateService $duplicatesCertificateService,
        DuplicatesCertificateRepository $duplicatesCertificateRepository

    ) {
        $this->duplicatesCertificateService = $duplicatesCertificateService;
        $this->duplicatesCertificateRepository = $duplicatesCertificateRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function certificateDuplicatesList(Request $request)
    {
        try {
            $listing = $this->duplicatesCertificateService->dulicatesListData($request);
            if (!empty($listing)) {
                Log::info(
                    'Fetched edit details from DB',
                    ['method' => __METHOD__, 'data' => ['CertificatesList' => $listing]]
                );

                return $this->sendSuccessResponse("Certificates List Found", $listing);
            } else {
                return $this->sendSuccessResponse("Certificates List not Found", $listing);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Certificates Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Certificates list not found");
        }
    }

    /**
     * @param string $certificateId
     * @return \Illuminate\Http\JsonResponse
     */
    public function certificateduplicatesView(string $certificateId)
    {
        try {
            $view = $this->duplicatesCertificateService->duplicatesCertificateViewData($certificateId);
            return $this->sendSuccessResponse("Certificates View Found", $view);
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Certificates Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Certificates list not found");
        }
    }

    /**
     * Add duplicate certificate
     * @param DuplicatesCertificateAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function certificateDuplicatesAdd(DuplicatesCertificateAddRequest $request)
    {
        try {
            $data = $request->all();
            $record = $this->duplicatesCertificateRepository->duplicatesCertificateAdd($request);
            if ($record == 2) {
                return $this->sendSuccessResponse("Duplicate Certificate Added Successfully", true);
            } else {
                return $this->sendErrorResponse('Certificate Id already Exist', false);
            }
        } catch (\Exception $e) {
            Log::error(
                'Failed to fetch Certificates Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Duplicate  Certificate Not Added");
        }
    }

    /**
     * @param DuplicatesCertificateUpdateRequest $request
     * @param string $certifiId
     * @return \Illuminate\Http\JsonResponse
     */
    public function certificateDuplicatesUpdate(DuplicatesCertificateUpdateRequest $request, string $certificateId)
    {
        try {
            $updatecertificate = $this->duplicatesCertificateRepository->duplicatesCertificateUpdate($request, $certificateId);
            return $this->sendSuccessResponse("Duplicate Certificate updated", $updatecertificate);
        } catch (\Exception $e) {
            Log::error(
                'Failed to Update Certificates Details  ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Duplicate  Certificate Not updated");
        }
    }

    /**
     * @param DuplicateCertficatImportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importDuplicatesCertificate(DuplicateCertficatImportRequest $request)
    {
        try {
            $importcertificate = $this->duplicatesCertificateRepository->duplicateImport($request);
            return $this->sendSuccessResponse("Duplicate Certificate import", $importcertificate);
        } catch (\Exception $e) {
            Log::error(
                'Failed to Import Certificates ',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Duplicate  Certificate Not impory");
        }
    }
}
