<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class MultipleChoiceReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'report_name' => 'required',
            'academic_period_id' => 'required',
            'examination_id' => 'required',
            'examination_centre_id' => 'required',
            'candidate' => 'required',
            'examination_subject_id' => 'required',
            'format' => 'required'


        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'report_name.required' => 'Report Name is required!',
            'academic_period_id.required' => 'Acedemic Period is required!',
            'examination_id.required' => 'Examination is required!',
            'examination_centre_id.required' => 'Examination Centre is required!',
            'candidate.required' => 'Candidate is required!',
            'examination_subject_id.required' => 'subject is required!',
            'format' => 'Format is required',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
                                                         ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
