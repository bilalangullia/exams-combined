<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class ExamCentreCopyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules = [
            'from_academic_period_id' => 'required',
            'from_examination_id' => 'required',
            'to_academic_period_id' => 'required',
            'to_examination_id' => 'required|different:from_examination_id',
            'link_all_centres' => 'required|in:0,1',
        ];
        

        if ($this->input('link_all_centres') == 0) {
            $rules['centres'] = 'required|array';
        }
        
        return $rules;
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'from_academic_period_id.required' => 'From academic period id is required!',
            'from_examination_id.required' => 'From examination id is required!',
            'to_academic_period_id.required' => 'To academic period id is required!',
            'to_examination_id.required' => 'To examination id is required!',
            'link_all_centres.required' => 'Link all centres is required!',
            'link_all_centres.in' => 'Link all centres is invalid!',
            'centres.required' => 'Centres is required!',
        ];
    }


    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(
                [
                    'Enter Required fields' => $errors,
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            )
        );
    }
}
