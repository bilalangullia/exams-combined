<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class CollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'candidate_id' => 'required',
            'date_received' => 'required|date',
            'method' => 'required',
            'examination_students_certificates_id' => 'required'
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        if(isset($errors['examination_students_certificates_id'])) {
            throw new HttpResponseException(
            response()->json(
                [
                    'message' => 'Certificate Number does not exist',
                    'data' => false,
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            )
          );
        }
        throw new HttpResponseException(
            response()->json(
                [
                    'error' => $errors,
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            )
        );
    }
}
