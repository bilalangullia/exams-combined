<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MultipleChoiceExcelImport implements WithMultipleSheets
{
    /*ToCollection, WithStartRow,WithMultipleSheets,WithHeadingRow*/
    /**
     * @param Collection $collection
     */


    public function sheets(): array
    {
        return [
            new MultipleChoiceExcelImport(),
        ];
    }

}
