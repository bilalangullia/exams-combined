<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ItemImport implements WithMultipleSheets
{
    /**
    * @param array
    */
    public function sheets(): array
    {
        return [
            new ItemImport(),
        ];
    }
}
