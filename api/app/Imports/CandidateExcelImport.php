<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CandidateExcelImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            new DataSheetImport(),
        ];
    }
}
