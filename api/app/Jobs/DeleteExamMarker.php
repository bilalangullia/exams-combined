<?php

namespace App\Jobs;

use App\Services\Administration\Examinations\Markers\MarkerService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteExamMarker implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $markerId;
    protected $parentId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $markerId = 0, int $parentId = 0, $check = false)
    {
        $this->markerId = $markerId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * @param MarkerService $markerService
     */
    public function handle(MarkerService $markerService)
    {
        $examMarkerId = $this->markerId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $markerService->deleteExaminationMarker($examMarkerId, $parentId, $check);
    }
    /**
     *  Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
