<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\Examinations\Centres\ExamCentreInvigilatorService;

class DeleteExamCentreInvigilator implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $examCentreId;
    protected $invigilatorId;
    protected $checkSafeToDelete;

    /**
     * Create a new job instance.
     *
     * @param int $invigilatorId
     * @param int $examCentreId
     * @param bool $checkSafeToDelete
     */
    public function __construct(int $invigilatorId = 0, int $examCentreId = 0, $checkSafeToDelete = false)
    {
        $this->invigilatorId = $invigilatorId;
        $this->examCentreId = $examCentreId;
        $this->checkSafeToDelete = $checkSafeToDelete;
    }

    /**
     * Execute the job.
     *
     * @param ExamCentreInvigilatorService $examCentreInvigilatorService
     * @return void
     */
    public function handle(ExamCentreInvigilatorService $examCentreInvigilatorService)
    {
        $invigilatorId = $this->invigilatorId;
        $examCentreId = $this->examCentreId;
        $checkSafeToDelete = $this->checkSafeToDelete;

        $this->response = $examCentreInvigilatorService->deleteInvigilator($invigilatorId, $examCentreId, $checkSafeToDelete);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
