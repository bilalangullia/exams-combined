<?php

namespace App\Jobs;

use App\Services\Administration\SystemSetup\EducationStructure\EducationGradeService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteEducationGradeSubject implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $gradeId;
    protected $subjectId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @param int $gradeId
     * @param int $subjectId
     * @param bool $check
     */
    public function __construct(int $gradeId = 0, int $subjectId = 0, $check = false)
    {
        $this->gradeId = $gradeId;
        $this->subjectId = $subjectId;
        $this->check = $check;
    }

    /**
     * Deleting Education grade subject
     * @param EducationGradeService $educationGradeService
     * @return int
     */
    public function handle(EducationGradeService $educationGradeService)
    {
        $gradeId = $this->gradeId;
        $subjectId = $this->subjectId;
        $check = $this->check;
        $this->response = $educationGradeService->educationGradeSubjectDelete($gradeId, $subjectId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
