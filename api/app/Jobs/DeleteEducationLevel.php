<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\SystemSetup\EducationStructure\EducationLevelService;

class DeleteEducationLevel implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $eduLevelId;
    protected $parentId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $eduLevelId = 0, int $parentId = 0, $check = false)
    {
        $this->eduLevelId = $eduLevelId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(EducationLevelService $educationLevelService)
    {
        $eduLevelId = $this->eduLevelId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $educationLevelService->deleteEducationLevel($eduLevelId, $parentId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
