<?php

namespace App\Jobs;

use App\Services\Administration\Examinations\FeeService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteExaminationFee implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $examFeeId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($examFeeId)
    {
        $this->examFeeId  = $examFeeId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(FeeService $feeService)
    {
        $this->response = $feeService->deleteExaminationFee($this->examFeeId);
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
