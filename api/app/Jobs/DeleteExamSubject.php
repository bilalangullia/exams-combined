<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\Examinations\Exams\SubjectService;

class DeleteExamSubject implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $subjectId;
    protected $parentId;
    protected $check;

    /**
     * DeleteExamSubject constructor.
     * @param int $subjectId
     * @param int $parentId
     * @param bool $check
     */
    public function __construct(int $subjectId = 0, int $parentId = 0, $check = false)
    {
        $this->subjectId = $subjectId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * @param SubjectService $subjectService
     */
    public function handle(SubjectService $subjectService)
    {
        $examSubjectId = $this->subjectId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $subjectService->deleteExaminationSubject($examSubjectId, $parentId, $check);
    }
    /**
     *  Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
