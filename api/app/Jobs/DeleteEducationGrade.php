<?php

namespace App\Jobs;

use App\Services\Administration\SystemSetup\EducationStructure\EducationGradeService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteEducationGrade implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $gradeId;
    protected $parentId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @param int $gradeId
     * @param int $parentId
     * @param bool $check
     */
    public function __construct(int $gradeId = 0, int $parentId = 0, $check = false)
    {
        $this->gradeId = $gradeId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * Deleting Education grade
     * @param EducationGradeService $educationGradeService
     * @return int
     */
    public function handle(EducationGradeService $educationGradeService)
    {
        $gradeId = $this->gradeId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $educationGradeService->educationGradeDelete($gradeId, $parentId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
