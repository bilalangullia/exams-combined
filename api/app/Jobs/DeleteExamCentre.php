<?php

namespace App\Jobs;

use App\Services\Administration\Examinations\Centres\ExamCentreService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteExamCentre implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $examCentreId;
    protected $parentId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @param int $examCentreId
     * @param int $parentId
     * @param bool $check
     */
    public function __construct(int $examCentreId = 0, int $parentId = 0, $check = false)
    {
        $this->examCentreId = $examCentreId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * @param ExamCentreService $examCentreService
     */
    public function handle(ExamCentreService $examCentreService)
    {
        $centreId = $this->examCentreId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $examCentreService->deleteExamCentre($centreId, $parentId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
