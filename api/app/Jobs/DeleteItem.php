<?php

namespace App\Jobs;

use App\Repositories\ItemsRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteItem
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var
     */
    protected $itemId;

    /**
     * DeleteItem constructor.
     * @param $itemId
     */
    public function __construct($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @param ItemsRepository $itemsRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(ItemsRepository $itemsRepository)
    {
        $error['msg'] = "";
        $status = $itemsRepository->deleteEXaminationItem($this->itemId);
        if (!empty($status['msg'])) {
            return $status;
        }

        return $error;
    }
}
