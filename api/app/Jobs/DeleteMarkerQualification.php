<?php

namespace App\Jobs;

use App\Services\Administration\Examinations\Markers\QualificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteMarkerQualification implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $staffqualificationId;
    protected $check;

    /**
     * DeleteMarkerQualification constructor.
     * @param int $staffqualificationId
     * @param bool $check
     */
    public function __construct(int $staffqualificationId, $check = false)
    {
        $this->staffqualificationId = $staffqualificationId;
        $this->check = $check;
    }

    /**
     * @param QualificationService $qualificationService
     */
    public function handle(QualificationService $qualificationService)
    {
        $check = $this->check;
        $staffqualificationId = $this->staffqualificationId;
        $this->response = $qualificationService->deleteQualification($staffqualificationId, $check);
    }

    /**
     *  Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
