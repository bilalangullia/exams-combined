<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\Examinations\Exams\ExaminationService;

class DeleteExamination implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $examId;
    protected $parentId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $examId = 0, int $parentId = 0, $check = false)
    {
        $this->examId = $examId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ExaminationService $examinationService)
    {
        $examId = $this->examId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $examinationService->deleteExamination($examId, $parentId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
