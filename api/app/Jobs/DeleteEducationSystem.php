<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\SystemSetup\EducationStructure\EducationSystemService;

class DeleteEducationSystem implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $eduSysId;
    protected $parentId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $eduSysId = 0, int $parentId = 0, $check = false)
    {
        $this->eduSysId = $eduSysId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(EducationSystemService $educationSystemService)
    {
        $eduSysId = $this->eduSysId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $educationSystemService->deleteEducationSystem($eduSysId, $parentId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
