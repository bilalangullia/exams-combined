<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\Examinations\GradingTypeService;

class DeleteGradingType implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $gradingTypeId;
    protected $parentId;
    protected $checkSafeToDelete;

    /**
     * Create a new job instance.
     *
     * @param int $gradingTypeId
     * @param int $parentId
     * @param bool $checkSafeToDelete
     */
    public function __construct(int $gradingTypeId = 0, int $parentId = 0, $checkSafeToDelete = false)
    {
        $this->gradingTypeId = $gradingTypeId;
        $this->parentId = $parentId;
        $this->checkSafeToDelete = $checkSafeToDelete;
    }

    /**
     * Execute the job.
     *
     * @param GradingTypeService $gradingTypeService
     * @return void
     */
    public function handle(GradingTypeService $gradingTypeService)
    {
        $gradingTypeId = $this->gradingTypeId;
        $parentId = $this->parentId;
        $checkSafeToDelete = $this->checkSafeToDelete;
        $this->response = $gradingTypeService->gradingTypeDelete($gradingTypeId, $parentId, $checkSafeToDelete);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
