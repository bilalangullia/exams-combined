<?php

namespace App\Jobs;

use App\Services\Administration\Examinations\Centres\ExamCentreRoomService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteExamCentreRoom implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $roomId;
    protected $examCentreId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @param int $roomId
     * @param int $examCentreId
     * @param bool $check
     */
    public function __construct(int $roomId = 0, int $examCentreId = 0, $check = false)
    {
        $this->roomId = $roomId;
        $this->examCentreId = $examCentreId;
        $this->check = $check;
    }

    /**
     * Execute the job
     * @param ExamCentreRoomService $examCentreRoomService
     */
    public function handle(ExamCentreRoomService $examCentreRoomService)
    {
        $check = $this->check;
        $roomId = $this->roomId;
        $examCentreId = $this->examCentreId;
        $this->response = $examCentreRoomService->deleteExamCentreRoom($roomId, $examCentreId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
