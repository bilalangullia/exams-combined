<?php

namespace App\Jobs;

use App\Services\Administration\Examinations\Exams\OptionService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteOption implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;


    public $response;
    protected $optionId;
    protected $parentId;
    protected $check;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $optionId = 0, int $parentId = 0, $check = false)
    {
        $this->optionId = $optionId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OptionService $optionService)
    {
        $optionId = $this->optionId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $optionService->deleteExamOption($optionId, $parentId, $check);
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
