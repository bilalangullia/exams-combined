<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\SystemSetup\AdministrativeBoundaries\AdministrativeBoundaryService;

class DeleteAreaLevelAdministrative implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $levelId;
    protected $parentId;
    protected $checkSafeToDelete;

    /**
     * Create a new job instance.
     *
     * @param int $levelId
     * @param int $parentId
     * @param bool $checkSafeToDelete
     */
    public function __construct(int $levelId = 0, int $parentId = 0, $checkSafeToDelete = false)
    {
        $this->levelId = $levelId;
        $this->parentId = $parentId;
        $this->checkSafeToDelete = $checkSafeToDelete;
    }

    /**
     * Execute the job.
     *
     * @param AdministrativeBoundaryService $administrativeBoundaryService
     * @return void
     */
    public function handle(AdministrativeBoundaryService $administrativeBoundaryService)
    {
        $levelId = $this->levelId;
        $parentId = $this->parentId;
        $checkSafeToDelete = $this->checkSafeToDelete;
        
        $this->response = $administrativeBoundaryService->areaLevelAdministrativeDelete($levelId, $parentId, $checkSafeToDelete);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
