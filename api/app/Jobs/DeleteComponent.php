<?php

namespace App\Jobs;

use App\Services\Administration\Examinations\Exams\ComponentService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteComponent implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $componentId;
    protected $parentId;
    protected $check;

    /**
     * DeleteExamcomponent constructor.
     * @param int $componentId
     * @param int $parentId
     * @param bool $check
     */
    public function __construct(int $componentId = 0, int $parentId = 0, $check = false)
    {
        $this->componentId = $componentId;
        $this->parentId = $parentId;
        $this->check = $check;
    }

    /**
     * @param ComponentService $componentService
     */
    public function handle(ComponentService $componentService)
    {
        $componentId = $this->componentId;
        $parentId = $this->parentId;
        $check = $this->check;
        $this->response = $componentService->deleteExaminationComponent($componentId, $parentId, $check);
    }
    /**
     *  Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
