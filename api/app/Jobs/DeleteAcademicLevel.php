<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\SystemSetup\AcademicPeriod\AcademicPeriodLevelService;

class DeleteAcademicLevel implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $academicLevel;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $academicLevel = 0, $check = false)
    {
        $this->academicLevel = $academicLevel;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AcademicPeriodLevelService $academicPeriodLevelService)
    {
        $academicLevel = $this->academicLevel;
        $check = $this->check;
        $this->response = $academicPeriodLevelService->deleteAcademicLevel($academicLevel, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
