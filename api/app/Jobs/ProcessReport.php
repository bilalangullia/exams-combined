<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Report\ReportService;
use http\Env\Request;

class ProcessReport implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var $reportData
     * @var $request
     */
    protected $reportData;
    protected $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($reportData, $request)
    {
        $this->reportData = $reportData;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @param ReportService $reportService
     *
     * @return array
     */
    public function handle(ReportService $reportService)
    {
        $error['msg'] = "";
        $status = $reportService->saveUpdateReport($this->reportData, $this->request);
        if (!empty($status['msg'])) {
            return $status;
        }
        return $error;
    }
}
