<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\SystemSetup\FieldOptions\AttendanceTypeService;

class DeleteAttendanceType implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $data;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $check = false)
    {
        $this->data = $data;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AttendanceTypeService $attendanceTypeService)
    {
        $data = $this->data;
        $check = $this->check;
        $this->response = $attendanceTypeService->deleteFieldOption($data, $check);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
