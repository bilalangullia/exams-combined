<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Register\CandidateService;

class DeleteRegisterCandidate implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $candId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $candId, $check = false)
    {
        $this->candId = $candId;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CandidateService $candidateService)
    {
        $candId = $this->candId;
        $check = $this->check;
        $this->response = $candidateService->deleteCandidate($candId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
