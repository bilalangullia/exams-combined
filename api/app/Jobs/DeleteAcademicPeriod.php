<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\SystemSetup\AcademicPeriod\AcademicPeriodService;

class DeleteAcademicPeriod implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $academicPeriodId;
    protected $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $academicPeriodId = 0, $check = false)
    {
        $this->academicPeriodId = $academicPeriodId;
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AcademicPeriodService $academicPeriodService)
    {
        $academicPeriodId = $this->academicPeriodId;
        $check = $this->check;
        $this->response = $academicPeriodService->deleteAcademicPeriod($academicPeriodId, $check);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
