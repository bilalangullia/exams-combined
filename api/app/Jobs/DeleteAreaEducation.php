<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Administration\SystemSetup\AdministrativeBoundaries\AdministrativeBoundaryService;

class DeleteAreaEducation implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $response;
    protected $areaId;
    protected $parentId;
    protected $checkSafeToDelete;

    /**
     * Create a new job instance.
     *
     * @param int $areaId
     * @param int $parentId
     * @param bool $checkSafeToDelete
     */
    public function __construct(int $areaId = 0, int $parentId = 0, $checkSafeToDelete = false)
    {
        $this->areaId = $areaId;
        $this->parentId = $parentId;
        $this->checkSafeToDelete = $checkSafeToDelete;
    }

    /**
     * Execute the job.
     *
     * @param AdministrativeBoundaryService $examCentreInvigilatorService
     * @return void
     */
    public function handle(AdministrativeBoundaryService $administrativeBoundaryService)
    {
        $areaId = $this->areaId;
        $parentId = $this->parentId;
        $checkSafeToDelete = $this->checkSafeToDelete;
        
        $this->response = $administrativeBoundaryService->areaEducationDelete($areaId, $parentId, $checkSafeToDelete);
    }

    /**
     * Getting job response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}
