<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationLevelIsced extends Model
{
   protected $table = "education_level_isced";

   public $timestamps = false;
}
