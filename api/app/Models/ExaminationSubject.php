<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationSubject extends Model
{
    public $timestamps = false;

    public function getFullNameAttribute()
    {
        return $this->attributes['code'] . '-' . $this->attributes['name'];
    }

    public function examName()
    {
        return $this->belongsTo(Examination::class, 'examination_id', 'id');
    }

    public function educationSubject()
    {
        return $this->belongsTo(EducationSubject::class, 'education_subject_id', 'id');
    }

    public function examinationOption()
    {
        return $this->hasOne(ExaminationOption::class, 'examination_subject_id', 'id');
    }
    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function options()
    {
        return $this->hasMany(ExaminationOption::class, 'examination_subject_id', 'id');
    }
}
