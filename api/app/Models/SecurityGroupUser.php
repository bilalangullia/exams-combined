<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityGroupUser extends Model
{
    public $timestamps = false;

    public function securityGroup()
    {
        return $this->belongsTo(SecurityGroup::class, 'security_group_id', 'id');
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'security_user_id', 'id');
    }

    public function securityRole()
    {
        return $this->belongsTo(SecurityRole::class, 'security_role_id', 'id');
    }
    
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
    public function modifiedByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
