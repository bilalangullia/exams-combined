<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QualificationLevel extends Model
{
    public $timestamps = false;

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
