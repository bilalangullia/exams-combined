<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeeType extends Model
{
    public function examType()
    {
        return $this->hasOne(ExaminationFee::class);
    }
}
