<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportProgress extends Model
{
    protected $table = 'report_progress';
    protected $fillable = ['id','name','params','created_user_id','modified','file_path','created','total_records','expiry_date','module','error_message'];
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function createdBy()
	{
	    return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
	}

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
