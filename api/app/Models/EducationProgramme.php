<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationProgramme extends Model
{
    public $timestamps = false;

    public function educationFieldOfStudie()
    {
        return $this->belongsTo(EducationFieldOfStudie::class, 'education_field_of_study_id', 'id')->where(['visible' => 1]);
    }
    public function educationCycle()
    {
        return $this->belongsTo(EducationCycle::class, 'education_cycle_id', 'id')->where(['visible' => 1]);
    }
    public function educationCertification()
    {
        return $this->belongsTo(EducationCertification::class, 'education_certification_id', 'id')->where(['visible' => 1]);
    }
    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
