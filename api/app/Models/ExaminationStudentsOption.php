<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsOption extends Model
{
    public $timestamps = false;

    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }

    public function examinationOption()
    {
        return $this->belongsTo(ExaminationOption::class, 'examination_options_id', 'id');
    }
}
