<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsOptionsGrade extends Model
{
    public $timestamps = false;

    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }

    public function examinationGradingOption()
    {
    	return $this->belongsTo(ExaminationGradingOption::class, 'grade', 'id');
    }

    public function examinationOption()
    {
    	return $this->belongsTo(ExaminationOption::class, 'examination_options_id', 'id');
    }
}

