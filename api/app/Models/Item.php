<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $timestamps = false;

    public function getFullNameAttribute()
    {
        return $this->attributes['code'] . ' - ' . $this->attributes['name'];
    }
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
