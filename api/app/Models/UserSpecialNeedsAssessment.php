<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSpecialNeedsAssessment extends Model
{

    public $timestamps = false;

    public function securityUser()
    {
        return $this->belongsTo('App\RegisterModel\SecurityUser', 'security_user_id', 'id');
    }

    public function specialNeed()
    {
        return $this->belongsTo(SpecialNeedType::class, 'special_need_type_id', 'id');
    }

    public function specialDifficulty()
    {
        return $this->belongsTo(SpecialNeedDifficulty::class, 'special_need_difficulty_id', 'id');
    }
    
}
