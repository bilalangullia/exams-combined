<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityGroupArea extends Model
{
    public $timestamps = false;

    public function area()
    {
    	return $this->belongsTo(Area::class, 'area_id', 'id');
    }
}
