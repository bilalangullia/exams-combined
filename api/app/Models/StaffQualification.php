<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffQualification extends Model
{
    public $timestamps = false;
    public function qualificationTitle()
    {
        return $this->belongsTo(QualificationTitle::class, 'qualification_title_id', 'id');
    }

    public function educationFieldOfStudie()
    {
        return $this->belongsTo(EducationFieldOfStudie::class, 'education_field_of_study_id', 'id');
    }

    public function countrie()
    {
        return $this->belongsTo(Countrie::class, 'qualification_country_id', 'id');
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
    public function securityUserStaffId()
    {
        return $this->belongsTo(SecurityUser::class, 'staff_id', 'id');
    }
    public function staffQualificationsSubject()
    {
        return $this->hasMany(StaffQualificationsSubject::class, 'staff_qualification_id', 'id');
    }
    public function staffQualificationsSpecialisation()
    {
        return $this->hasMany(StaffQualificationsSpecialisation::class, 'staff_qualification_id', 'id');
    }
}
