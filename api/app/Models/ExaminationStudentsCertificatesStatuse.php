<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsCertificatesStatuse extends Model
{
     public $timestamps = false;

    public function examinationStudentsCertificate()
    {
        return $this->belongsTo(ExaminationStudentsCertificate::class, 'examination_students_certificates_id', 'id');
    }

    public function securityUserBy()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
