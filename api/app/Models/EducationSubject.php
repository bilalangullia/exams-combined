<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationSubject extends Model
{
	public $timestamps = false;
	
	public function fieldOfStudy()
    {
        return $this->belongsToMany(EducationFieldOfStudie::class, 'education_subjects_field_of_studies', 'education_subject_id', 'education_field_of_study_id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
