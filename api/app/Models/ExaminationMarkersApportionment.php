<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationMarkersApportionment extends Model
{
    public function examinationMarker()
    {
        return $this->belongsTo(ExaminationMarker::class, 'examination_markers_id', 'id');
    }
}
