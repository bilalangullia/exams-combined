<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffQualificationsSpecialisation extends Model
{
    public $timestamps = false;
    public function qualificationSpecialisation()
    {
        return $this->hasMany(QualificationSpecialisation::class, 'id', 'qualification_specialisation_id');
    }
}
