<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationMarkType extends Model
{
    public function markType()
    {
        return $this->belongsTo(MarkType::class, 'mark_types_id', 'id');
    }
    public function examinationComponent()
    {
        return $this->belongsTo(ExaminationComponent::class, 'examination_components_id', 'id');
    }

}
