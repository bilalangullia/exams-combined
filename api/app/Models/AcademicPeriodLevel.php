<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademicPeriodLevel extends Model
{
    public $timestamps = false;

    public function createdBy()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
