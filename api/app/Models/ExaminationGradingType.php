<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationGradingType extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasmany
     */
    public $timestamps = false;
    
    public function examinationGradingOption()
    {
        return $this->hasMany(ExaminationGradingOption::class, 'examination_grading_type_id', 'id');
    }
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function examinationOption()
    {
        return $this->hasOne(ExaminationOption::class, 'examination_grading_type_id', 'id');
    }

    public function examinationComponent()
    {
        return $this->hasOne(ExaminationComponent::class, 'examination_grading_type_id', 'id');
    }
}
