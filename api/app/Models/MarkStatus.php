<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarkStatus extends Model
{
    protected $table = 'mark_status';

    public $timestamps = false;

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id')
        			->where('status', config('constants.createdByUser.status'));
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id')
        			->where('status', config('constants.createdByUser.status'));
    }

    public function examinationStudentComponentMarks()
    {
        return $this->hasMany(ExaminationStudentsComponentsMark::class, 'mark_status_id', 'id');
    }
}
