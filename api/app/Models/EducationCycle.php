<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationCycle extends Model
{
    public $timestamps = false;

    public function educationProgram()
    {
        return $this->hasMany(EducationProgramme::class, 'education_cycle_id', 'id');
    }

    public function educationLevel()
    {
        return $this->belongsTo(EducationLevel::class, 'education_level_id', 'id');
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
