<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationCentreExaminations extends Model
{
    protected $table = 'examination_centres_examinations';
}
