<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsFeesPayment extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function examinationStudentsFee()
    {
        return $this->belongsTo(ExaminationStudentsFee::class, 'examination_student_fees_id', 'id');
    }
    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
}
