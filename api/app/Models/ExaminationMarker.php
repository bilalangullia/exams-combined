<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationMarker extends Model
{
    public $timestamps = false;

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'staff_id', 'id');
    }

    public function examName()
    {
        return $this->belongsTo(Examination::class, 'examination_id', 'id');
    }

    public function examinationCentre()
    {
        return $this->belongsTo(ExaminationCentre::class, 'examination_centre_id', 'id');
    }

    public function securityUserCreatedId()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function examinationMarkersSubject()
    {
        return $this->hasOne(ExaminationMarkersSubject::class, 'examination_markers_id', 'id');
    }
    public function classification()
    {
        return $this->belongsTo(Classification::class, 'classification', 'id');
    }
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

}
