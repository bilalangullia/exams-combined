<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationGradeReviewStudent extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $guarded = [];
    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }
    public function examinationGradeReviewCriteria()
    {
        return $this->belongsTo(ExaminationGradeReviewCriteria::class, 'examination_grade_review_criterias_id', 'id');
    }
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
}
