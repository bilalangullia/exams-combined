<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationCentreRoomExaminations extends Model
{
    protected $table = 'examination_centre_rooms_examinations';
}
