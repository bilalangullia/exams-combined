<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationFee extends Model
{
    public $timestamps = false;

    public function feeType()
    {
        return $this->belongsTo(FeeType::class, 'fee_types_id', 'id')->where(['visible' => 1]);
    }

    public function examination()
    {
        return $this->belongsTo(Examination::class, 'examinations_id', 'id');
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function examinationStudentsFee()
    {
        return $this->hasMany(ExaminationStudentsFee::class, 'examination_fees_id', 'id');
    }

}
