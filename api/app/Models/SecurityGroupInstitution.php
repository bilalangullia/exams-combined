<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityGroupInstitution extends Model
{
    public $timestamps = false;

    public function examinationCentre()
    {
    	return $this->belongsTo(ExaminationCentre::class, 'institution_id', 'id');
    }
}
