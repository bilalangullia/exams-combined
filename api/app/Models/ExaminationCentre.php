<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationCentre extends Model
{
    public $timestamps = false;

    public function getFullNameAttribute()
    {
        return $this->attributes['code'] . ' - ' . $this->attributes['name'];
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function ownership()
    {
        return $this->belongsTo(ExaminationCentreOwnership::class, 'examination_centres_ownerships_id', 'id');
    }

    public function exams()
    {
        return $this->belongsTo(Examination::class, 'examination_id', 'id');
    }

    public function invigilators()
    {
        return $this->belongsToMany(SecurityUser::class, 'examination_centre_invigilators', 'examination_centre_id', 'invigilator_id');
    }

    public function specialNeeds(){
         return $this->belongsToMany(SpecialNeedType::class, 'examination_centre_special_needs', 'examination_centre_id', 'special_need_type_id');
    }

    public function markScalling()
    {
        return $this->hasMany(ExaminationCentresExaminationsMarksScaling::class, 'examination_centre_id', 'id');
    }

    public function rooms()
    {
        return $this->hasMany(ExaminationCentreRoom::class, 'examination_centre_id', 'id');
    }


    public function groups()
    {
        return $this->belongsToMany(SecurityGroup::class, 'security_group_institutions', 'institution_id', 'security_group_id');
    }
    
    public function securityGroup()
    {
        return $this->belongsTo(SecurityGroup::class, 'id', 'security_group_id');

    }

    public function students()
    {
        return $this->hasMany(ExaminationStudent::class, 'examination_centre_id', 'id');
    }
}
