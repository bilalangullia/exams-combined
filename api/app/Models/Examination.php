<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    public $timestamps = false;

    public function getFullNameAttribute()
    {
        return $this->attributes['code'] . ' - ' . $this->attributes['name'];
    }

    public function academicPeriod()
    {
        return $this->hasOne(AcademicPeriod::class, 'id', 'academic_period_id');
    }

    public function educationGrade()
    {
        return $this->belongsTo(EducationGrade::class, 'education_grade_id', "id");
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function examinationSubject()
    {
        return $this->hasMany(ExaminationSubject::class, 'examination_id', 'id');
    }

    public function examSessionType()
    {
        return $this->belongsTo(ExamSessionType::class, 'exam_session_types_id', 'id');
    }

    public function examinationType()
    {
        return $this->belongsTo(ExaminationType::class, 'examination_types_id', 'id');
    }

    public function examinationCentre()
    {
        return $this->hasMany(ExaminationCentre::class, 'examination_id', 'id');
    }

    public function attendanceType()
    {
        return $this->belongsTo(AttendanceType::class, 'attendance_type_id', 'id');
    }

    public function examinationStudent()
    {
        return $this->hasMany(ExaminationStudent::class, 'examination_id', 'id');
    }

    public function examinationMarker()
    {
        return $this->hasMany(ExaminationMarker::class, 'examination_id', 'id');
    }

}
