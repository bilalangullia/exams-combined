<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsFee extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }

    public function examinationFee()
    {
        return $this->hasMany(ExaminationFee::class, 'id', 'examination_fees_id');
    }

    public function examinationStudentsFeesPayment()
    {
        return $this->hasMany(ExaminationStudentsFeesPayment::class, 'examination_student_fees_id', 'id');
    }
}
