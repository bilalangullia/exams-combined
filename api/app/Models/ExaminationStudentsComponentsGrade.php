<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsComponentsGrade extends Model
{
    public $timestamps = false;

    public function examinationComponent()
    {
        return $this->belongsTo(ExaminationComponent::class, 'examination_components_id', 'id');
    }
}
