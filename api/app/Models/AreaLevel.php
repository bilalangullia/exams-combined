<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaLevel extends Model
{
    public $timestamps = false;
    
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function areas()
    {
        return $this->hasMany(Area::class, 'area_level_id', 'id');
    }
}
