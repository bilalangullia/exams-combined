<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationGrade extends Model
{

    public $timestamps = false;

    public function educationProgramme()
    {
        return $this->belongsTo(EducationProgramme::class, 'education_programme_id', 'id');
    }

    public function educationStage()
    {
        return $this->belongsTo(EducationStage::class, 'education_stage_id', 'id');
    }

    public function educationGradeSubject()
    {
        return $this->hasMany(EducationGradesSubject::class, 'education_grade_id', 'id');
    }

    public function GradeSubjects()
    {
        return $this->belongsToMany(EducationSubject::class, 'education_grades_subjects', 'education_grade_id', 'education_subject_id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function exams()
    {
        return $this->hasMany(Examination::class, 'education_grade_id', 'id');
    }
}
