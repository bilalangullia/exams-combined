<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationComponentsMultiplechoice extends Model
{
    public $timestamps = false;

    public $table = "examination_components_multiplechoice";

    public function examinationComponent()
    {
        return $this->belongsTo(ExaminationComponent::class, 'examination_components_id', 'id');
    }

}
