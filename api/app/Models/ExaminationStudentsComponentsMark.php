<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsComponentsMark extends Model
{
    public $timestamps = false;

    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }
    public function markStatus()
    {
        return $this->belongsTo(MarkStatus::class, 'mark_status_id', 'id');
    }
    public function examinationComponent()
    {
        return $this->belongsTo(ExaminationComponent::class, 'examination_components_id', 'id');
    }
    public function examinationComponentsMarkTypes()
    {
        return $this->belongsTo(ExaminationComponentsMarkType::class, 'examination_components_mark_types_id', 'id');
    }
    public function examinationMarker()
    {
        return $this->belongsTo(ExaminationMarker::class, 'examination_markers_id', 'id');
    }
    public function createdBy()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

}
