<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationGradingOption extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public $timestamps = false;
    
    public function examinationGradingType()
    {
        return $this->belongsTo(ExaminationGradingType::class, 'examination_grading_type_id', 'id');
    }

}
