<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsMultiplechoiceResponse extends Model
{
    public $timestamps = false;

    public $table = "examination_students_multiplechoice_response";

    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }

    public function examinationComponent()
    {
        return $this->belongsTo(ExaminationComponent::class, 'examination_components_id', 'id');
    }
}
