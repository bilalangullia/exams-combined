<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationMarkersSubject extends Model
{
    public $timestamps = false;

    public function examinationMarker()
    {
        return $this->hasMany(ExaminationMarker::class, 'examination_markers_id', 'id');
    }
    public function educationSubject()
    {
        return $this->belongsTo(EducationSubject::class, 'education_subjects_id', 'id');
    }
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
