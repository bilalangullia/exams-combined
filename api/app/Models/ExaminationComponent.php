<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationComponent extends Model
{

    public $timestamps = false;

    public function getFullNameAttribute()
    {
        return $this->attributes['code'] . ' - ' . $this->attributes['name'];
    }

    public function examinationOption()
    {
        return $this->belongsTo(ExaminationOption::class, 'examination_options_id', 'id');
    }

    public function componentType()
    {
        return $this->belongsTo(ComponentType::class, 'component_types_id', 'id');
    }

    public function examinationGradingType()
    {
        return $this->belongsTo(ExaminationGradingType::class, 'examination_grading_type_id', 'id');
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function examinationComponentsMarkType()
    {
        return $this->hasOne(ExaminationComponentsMarkType::class, 'examination_components_id', 'id');
    }

    public function examinationComponentsMultiplechoice()
    {
        return $this->hasMany(ExaminationComponentsMultiplechoice::class, 'examination_components_id', 'id');
    }

    public function examinationStudentsComponentsMark()
    {
        return $this->hasOne(ExaminationStudentsComponentsMark::class, 'examination_components_id', 'id');
    }

    public function examinationStudentsMultiplechoiceResponse()
    {
        return $this->hasMany(ExaminationStudentsMultiplechoiceResponse::class, 'examination_components_id', 'id');
    }

    public function examinationCentresExaminationsMarksScaling()
    {
        return $this->hasMany(ExaminationCentresExaminationsMarksScaling::class, 'examination_components_id', 'id');
    }
}
