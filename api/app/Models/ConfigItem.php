<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigItem extends Model
{
    public $timestamps = false;

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
