<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationProgrammesNextProgramme extends Model
{
    public $timestamps = false;
    public function educationProgramme()
    {
        return $this->belongsTo(EducationProgramme::class, 'education_programme_id', 'id');
    }

}
