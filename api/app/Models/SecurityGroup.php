<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityGroup extends Model
{

    public $timestamps = false;

    public function securityGroupUser()
    {
    	return $this->hasMany(SecurityGroupUser::class, 'security_group_id', 'id');
    }

    public function securityGroupArea()
    {
    	return $this->hasMany(SecurityGroupArea::class, 'security_group_id', 'id');
    }

    public function securityGroupInstitution()
    {
    	return $this->hasMany(SecurityGroupInstitution::class, 'security_group_id', 'id');
    }

     public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function institutions()
    {
        return $this->belongsToMany(ExaminationCentre::class, 'security_group_institutions', 'security_group_id', 'institution_id');
    }

    public function examinationCentre()
    {
        return $this->hasOne(ExaminationCentre::class, 'security_group_id', 'id');
    }

    public function securityRole()
    {
        return $this->hasMany(SecurityRole::class, 'security_group_id', 'id');
    }
}
