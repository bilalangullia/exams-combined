<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudent extends Model
{

    public $timestamps = false;

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'student_id', 'id');
    }

    public function examinationCentre()
    {
        return $this->belongsTo(ExaminationCentre::class, 'examination_centre_id', 'id');
    }

    public function attendanceType()
    {
        return $this->belongsTo(AttendanceType::class, 'examination_attendance_types_id', 'id');
    }

    public function examName()
    {
        return $this->belongsTo(Examination::class, 'examination_id', 'id');
    }

    public function examinationStudentsFee()
    {
        return $this->hasOne(ExaminationStudentsFee::class, 'examination_students_id', 'id');
    }

    public function examinationStudentsOption()
    {
        return $this->hasMany(ExaminationStudentsOption::class, 'examination_students_id', 'id');
    }

    public function examinationStudentsMultiplechoiceResponse()
    {
        return $this->hasOne(ExaminationStudentsMultiplechoiceResponse::class, 'examination_students_id', 'id');
    }

    public function examinationStudentsComponentsMark()
    {
        return $this->hasOne(ExaminationStudentsComponentsMark::class, 'examination_students_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function examinationStudentsForecastGrade()
    {
        return $this->hasOne(ExaminationStudentsForecastGrade::class, 'examination_students_id', 'id');
    }

    public function examinationStudentsOptionsGrade()
    {
        return $this->hasOne(ExaminationStudentsOptionsGrade::class, 'examination_students_id', 'id');
    }

    public function options()
    {
        return $this->belongsToMany(ExaminationOption::class, 'examination_students_options', 'examination_students_id', 'examination_options_id');
    }

    public function examinationStudentsCertificate()
    {
        return $this->hasOne(ExaminationStudentsCertificate::class, 'examination_students_id', 'id');
    }

    public function examinationGradeReviewStudent()
    {
        return $this->hasMany(ExaminationGradeReviewStudent::class, 'examination_students_id', 'id');
    }

    public function examinationStudentsComponentsGrade()
    {
        return $this->hasMany(ExaminationStudentsComponentsGrade::class, 'examination_students_id', 'id');
    }

    public function examinationFee()
    {
        return $this->belongsToMany(ExaminationFee::class, 'examination_students_fees', 'examination_students_id', 'examination_fees_id');
    }
}
