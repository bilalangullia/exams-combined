<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationItem extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function item()
    {
        return $this->belongsTo(Item::class, 'items_id', 'id');
    }

    public function examinationComponent()
    {
        return $this->belongsTo(ExaminationComponent::class, 'examination_components_id', 'id');
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
}
