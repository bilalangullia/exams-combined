<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityRole extends Model
{
    public $timestamps = false;
    public function securityGroup()
    {
        return $this->belongsTo(SecurityGroup::class, 'security_group_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function securityFunction()
    {
        return $this->belongsToMany(SecurityFunction::class, 'security_role_functions', 'security_role_id', 'security_function_id');
    }
}
