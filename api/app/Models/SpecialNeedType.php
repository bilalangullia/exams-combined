<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialNeedType extends Model
{
	public $timestamps = false;
    
    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id')
        			->where('status', config('constants.createdByUser.status'));
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id')
        			->where('status', config('constants.createdByUser.status'));
    }

    public function examCentre()
    {
        return $this->belongsToMany(ExaminationCentre::class, 'examination_centre_special_needs', 'special_need_type_id', 'examination_centre_id');
    }
}
