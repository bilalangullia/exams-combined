<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationCentresExaminationsMarksScaling extends Model
{
    public $timestamps = false;

    public $table = "examination_centres_examinations_marks_scaling";
}
