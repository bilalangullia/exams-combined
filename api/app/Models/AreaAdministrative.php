<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaAdministrative extends Model
{
    public $timestamps = false;
    
    public function region()
    {
        return $this->hasMany(AreaAdministrative::class, 'parent_id', 'id')->where('area_administrative_level_id', 3);
    }

    public function areaAdministrativeLevel()
    {
        return $this->belongsTo(AreaAdministrativeLevel::class, 'area_administrative_level_id', 'id');
    }

    public function parentArea()
    {
        return $this->belongsTo(AreaAdministrative::class, 'parent_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function securityUserAddress()
    {
        return $this->hasMany(SecurityUser::class, 'address_area_id', 'id');
    }

    public function securityUserBirthplace()
    {
        return $this->hasMany(SecurityUser::class, 'birthplace_area_id', 'id');
    }

    public function areaLevel()
    {
        return $this->hasMany(AreaAdministrativeLevel::class, 'area_administrative_id', 'id');
    }
}
