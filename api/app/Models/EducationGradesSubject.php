<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationGradesSubject extends Model
{
    public $timestamps = false;

    public function educationSubject()
    {
        return $this->belongsTo(EducationSubject::class, 'education_subject_id', 'id');
    }
}
