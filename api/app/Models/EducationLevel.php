<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationLevel extends Model
{
    public $timestamps = false;

    public function educationSystem()
    {
        return $this->belongsTo(EducationSystem::class, 'education_system_id', 'id');
    }

    public function educationLevelIsced()
    {
        return $this->belongsTo(EducationLevelIsced::class, 'education_level_isced_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
}
