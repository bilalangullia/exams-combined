<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public $timestamps = false;
    
    public function region()
    {
        return $this->hasMany(Area::class, 'parent_id', 'id')->where('area_level_id', 2);
    }

    public function areaLevel()
    {
        return $this->belongsTo(AreaLevel::class, 'area_level_id', 'id');
    }

    public function parentArea()
    {
        return $this->belongsTo(Area::class, 'parent_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function examCentres()
    {
        return $this->hasMany(ExaminationCentre::class, 'area_id', 'id');
    }

    public function securityGroups()
    {
        return $this->belongsToMany(SecurityGroup::class, 'security_group_areas', 'area_id', 'security_group_id');
    }
}
