<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsCertificate extends Model
{
    public $timestamps = false;

    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }
    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
    public function securityUserBy()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
