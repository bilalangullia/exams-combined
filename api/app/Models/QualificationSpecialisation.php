<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QualificationSpecialisation extends Model
{
    public $timestamps = false;

    public function educationFieldOfStudie()
    {
        return $this->belongsTo(EducationFieldOfStudie::class, 'education_field_of_study_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
