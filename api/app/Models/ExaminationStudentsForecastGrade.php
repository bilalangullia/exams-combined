<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationStudentsForecastGrade extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $guarded = [];

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

    public function examinationGradingOption()
    {
        return $this->belongsTo(ExaminationGradingOption::class, 'grade', 'id');
    }

    public function examinationStudentsOptionsGrade()
    {
        return $this->belongsTo(ExaminationStudentsOptionsGrade::class, 'examination_students_id', 'id');
    }
    public function examinationOption()
    {
        return $this->belongsTo(ExaminationOption::class, 'examination_options_id', 'id');
    }

    public function examinationStudent()
    {
        return $this->belongsTo(ExaminationStudent::class, 'examination_students_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }
}
