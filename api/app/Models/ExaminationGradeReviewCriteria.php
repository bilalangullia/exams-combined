<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationGradeReviewCriteria extends Model
{
    public $timestamps = false;
    
    public function gradeReviewCriteriaType()
    {
        return $this->belongsTo(GradeReviewCriteriaType::class, 'grade_review_criteria_types_id', 'id');
    }
    public function examinationOption()
    {
        return $this->belongsTo(ExaminationOption::class, 'examination_options_id', 'id');
    }
}
