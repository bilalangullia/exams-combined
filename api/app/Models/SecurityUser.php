<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class SecurityUser extends Authenticatable implements JWTSubject
{
    use Notifiable;

    public $timestamps = false;

    protected $casts = [
        'date_of_birth' => 'date:Y-m-d',
    ];

    public function getFullNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['middle_name'] . ' ' . $this->attributes['third_name'] . ' ' . $this->attributes['last_name'];
    }

    public function allExaminationStudent()
    {
        return $this->hasMany(ExaminationStudent::class, 'student_id', 'id');
    }

    public function examinationStudent()
    {
        return $this->hasOne(ExaminationStudent::class, 'student_id', 'id');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id', 'id');
    }

    public function nationality()
    {
        return $this->belongsTo(Nationality::class, 'nationality_id', 'id');
    }

    public function identityType()
    {
        return $this->belongsTo(IdentityType::class, 'identity_type_id', 'id');
    }

    public function areaAdministrative()
    {
        return $this->belongsTo(AreaAdministrative::class, 'address_area_id', 'id');
    }

    public function userSpecialNeedsAssessment()
    {
        return $this->hasOne(UserSpecialNeedsAssessment::class, 'security_user_id', 'id');
    }

    public function birthplaceArea()
    {
        return $this->belongsTo(AreaAdministrative::class, 'birthplace_area_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function examinationMarker()
    {
        return $this->hasOne(ExaminationMarker::class, 'id', 'staff_id');
    }

    public function securityUserLogin()
    {
        return $this->hasOne(SecurityUserLogin::class, 'security_user_id', 'id');
    }

    public function securityRole()
    {
        return $this->belongsToMany(SecurityRole::class, 'security_group_users', 'security_user_id', 'security_role_id');
    }
    public function securityGroup()
    {
        return $this->belongsToMany(SecurityGroup::class, 'security_group_users', 'security_user_id', 'security_group_id');
    }

    public function securityGroupUser()
    {
        return $this->hasMany(SecurityGroupUser::class, 'security_user_id', 'id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }

}
