<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceType extends Model
{

    public $timestamps = false;
    
    public function examinationCentre()
    {
        return $this->belongsTo(ExaminationStudent::class, 'id', 'examination_attendance_types_id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
