<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationCentreOwnership extends Model
{
    protected $table = 'examination_centre_ownerships';
}
