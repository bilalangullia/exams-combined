<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademicPeriod extends Model
{

    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo(AcademicPeriod::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(AcademicPeriod::class, 'parent_id');
    }

	public function academicPeriodLevel()
	{
	 	return $this->belongsTo(AcademicPeriodLevel::class, 'academic_period_level_id', 'id');
	}

	public function createdBy()
	{
	    return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
	}

    public function modifiedBy()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
 
}
