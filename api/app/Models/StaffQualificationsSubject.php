<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffQualificationsSubject extends Model
{
    public $timestamps = false;
    public function educationSubject()
    {
        return $this->hasMany(EducationSubject::class, 'id', 'education_subject_id');
    }
}
