<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationOption extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function getFullNameAttribute()
    {
        return $this->attributes['code'] . '-' . $this->attributes['name'];
    }

    public function examinationSubject()
    {
        return $this->belongsTo(ExaminationSubject::class, 'examination_subject_id', 'id');
    }

    public function attendanceType()
    {
        return $this->belongsTo(AttendanceType::class, 'attendance_type_id', 'id');
    }

    public function examinationComponent()
    {
        return $this->hasMany(ExaminationComponent::class, 'examination_options_id', 'id');
    }

    public function examinationGradingType()
    {
        return $this->belongsTo(ExaminationGradingType::class, 'examination_grading_type_id', 'id');
    }

    public function securityUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function examinationStudentsForecastGrade()
    {
        return $this->hasMany(ExaminationStudentsForecastGrade::class, 'examination_options_id', 'id');
    }

    public function examinationStudentsOptionsGrade()
    {
        return $this->hasOne(ExaminationStudentsOptionsGrade::class, 'examination_options_id', 'id');
    }

    public function examinationStudentsOption()
    {
         return $this->hasMany(ExaminationStudentsOption::class, 'examination_options_id', 'id');
    }

    public function examinationGradeReviewCriteria()
    {
         return $this->hasMany(ExaminationGradeReviewCriteria::class, 'examination_options_id', 'id');
    }

}
