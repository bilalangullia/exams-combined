<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminationCentreRoom extends Model
{
    public $timestamps = false;
    public function examCentre()
    {
        return $this->belongsTo(ExaminationCentre::class, 'examination_centre_id', 'id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(SecurityUser::class, 'created_user_id', 'id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo(SecurityUser::class, 'modified_user_id', 'id');
    }
}
