<?php

return [
    'statusCodes' => [
        'resourceNotFound' => 404,
        'internalError' => 500,
        'success' => 200,
        'deleteError' => 403,
    ],

    'visibleValue' => [
        'visibleCode' => 1,
    ],
    'typeValue' => [
        'typeCode' => 2,

    ],

    'orderValue' => [
        'value' => 1,
    ],

    'componentType' => [
        'type' => 2,

    ],
    'reportProgressStatus' => [
        '1' => 'Not Generated or New',
        '2' => 'Processing',
        '3' => 'Completed',
        '4' => 'Not Completed',
    ],
    'reportUpdateStatus' => [
        'notGeneratedOrNew' => '1',
        'processing' => '2',
        'completed' => '3',
        'notCompleted' => '4',
    ],

    'modifiedByUser' => [
        'id' => 2,
        'status' => 1,
    ],

    'createdByUser' => [
        'id' => 2,
        'status' => 1,
    ],
    'modifiedUser' => [
        "user_id" => 2,
    ],
    'createdUser' => [
        "user_id" => 2,
    ],

    'invigilatorUser' => [
        "status" => 1,
        "isStaff" => 1,
        "isStudent" => 0,
        "isGuardian" => 0,
    ],

    'staffIds' => [
        "staff" => 1,
        "status" => 1,
    ],

    'students' => [
        "status" => 1,
        "isStaff" => 0,
        "isStudent" => 1,
        "isGuardian" => 0,
    ],

    'markType' => [
        'visible' => 1,
        'id' => 1,
    ],

    'fileExt' => [
        'excel' => [
            'xlsx',
            'xls',
            'csv'
        ]
    ],

    'forecastGradeImport' => [
        'header' => [
            'Candidate ID',
            'Forecast Grade',
        ],
        'maxRows' => 2000,
    ],

    'multipleChoiceImport' => [
        'header' => [
            'Candidate ID',
            'Question Num',
            'Response',
        ],
        'maxRows' => 2000,
    ],

    'gradingType' => [
        'status' => 1,
    ],
    'totalSubject' => [
        'name' => 1,
    ],

    'alphabet' => [
    'position' => 31,
    ],

    'systemRoles' => [
        'securityGroupId' => -1,
    ],
    'user' => [
        'super_admin' => 1,
    ],
    'courseComponentType' => [
        'type' => 3,

    ],
    'canLogIn' => [
        'superAdmin' => 1,
        'isStaff' => 1
    ],
    'certificatetype' => [
        'duplicate' => 2,
        'initial' => 1
    ],
    'certificatemethod' => [
        'collect' => 1,
        'post' => 2
    ],
    'orderValue' => [
        'value' => 1,
    ],
    'academicPeriodLevel' => [
        'editable' => 0,
        'level' => 1
    ],

    'allData' => [
        'parent' => 0,
        'child' => 1
    ],

    'examinationCentre' => [
        'header' => [
            'Centre Code',
            'Centre Name',
            'EMIS Code',
            'Area Name',
            'Address',
            'Postal Code',
            'Contact Person',
            'Telephone',
            'Ownership',
        ],
        'maxRows' => 2000,
    ],
        
    'optionImport' => [
        'header' => [
            'Subject',
            'Option Code',
            'Option Name',
            'Grading Type',
            'Max Option Mark',
            'Attendance Type',
            'Carry Forward'
        ],
        'maxRows' => 2000,
    ],

    'itemImport' => [
        'header' => [
            'Component',
            'Item'
        ],
        'maxRows' => 2000,
    ],

    'gradingType' => [
        'header' => [
            'Grading Type Id',
            'Name',
            'Description',
            'Points',
            'Min',
            'Max',
        ],
        'maxRows' => 2000,
    ],
];
