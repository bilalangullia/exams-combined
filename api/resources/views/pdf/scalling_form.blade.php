<!DOCTYPE HTML>
<html lang="en" style="width:22.1cm; padding-left:1.7cm; padding-right:1.7cm;">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>OMR Sheet - Pg63</title>
	</head>
	
	<body style="width:22.1cm; padding-left:1.7cm; padding-right:1.7cm;">
		<section class="date_count" style="margin-top:2.3cm;">
		  	<div class="date" style="display: inline-block; padding-right: 550px; margin-left: -70px;">@if($date) {{$date}} @endif</div>
		  	<div class="count" style="display: inline-block;">PAGE: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1</div>
		</section>
		<section class="exam-details" style="margin-top:2.4cm; width:14.9cm;; margin-left:3.8cm">
			<table>
				<tr>
					<td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">{{$exam['name']}}</p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">4</p></td>
					<td>
						<table>
							<tr>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"><img src="{{public_path('images/pdf/black-bubble.png')}}" alt=""></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
								<td><div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;"></div></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">{{$academicPeriod['start_year']}}</p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$academicPeriod['codeArray'][2]}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <= 9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($academicPeriod['codeArray'][2] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$academicPeriod['codeArray'][3]}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <=9; $i++)
									<td style="margin-left:20px;">
										
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($academicPeriod['codeArray'][3] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">
							{{$examCentre['name']}}
					</p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examCentre['codeArray'][0]??""}}</p></td>
					<td>
						<table>
							<tr>
								@foreach(range('A', 'M') as $column)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examCentre['codeArray'][0] == $column)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endforeach
							</tr>
							<tr>
								@foreach(range('N', 'Z') as $val)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examCentre['codeArray'][0] == $val)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endforeach
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examCentre['codeArray'][1]??""}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <=9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examCentre['codeArray'][1] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examCentre['codeArray'][2]??""}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <= 9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examCentre['codeArray'][2] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm;"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">
						{{$examSubject['name']}}
					</p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examSubject['codeArray'][0]??""}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <= 9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examSubject['codeArray'][0] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examSubject['codeArray'][1]??""}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <= 9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examSubject['codeArray'][1] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm;"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examSubject['codeArray'][2]??""}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <= 9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examSubject['codeArray'][2] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm;"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examSubject['codeArray'][3]??""}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <= 9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examSubject['codeArray'][3] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">{{$examComponent['name']}}</p></td>
					<td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;">{{$examComponent['codeArray'][0]}}</p></td>
					<td>
						<table>
							<tr>
								@for($i = 0; $i <= 9; $i++)
									<td style="margin-left:20px;">
										<div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
											@if ($examComponent['codeArray'][0] == $i)
												<img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
											@else
												<img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
											@endif
										</div>
									</td>
								@endfor
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</section>
	</body>
</html>