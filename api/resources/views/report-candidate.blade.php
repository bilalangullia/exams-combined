<!DOCTYPE html>
<head>
    <title>Page Title</title>
</head>

<body>
<table align="center" width="100%">
        
    <tr>
                
        <td align="left">{{$date}}</td>
                
        <td align="right">Page No:1</td>
                
            
    </tr>
        
    <tr>
                
        <td colspan="2">Full time candidates</td>
            
    </tr>
        
    <tr>
                
        <td>Region:<span>{{$areaName}}</span><span>{{$areaCode}}</span></td>
                
                
            
    </tr>
      
        
    <tr>
                 
        <td colspan="5">Examination centre :<span>{{$examCenterCode}}</span><span>{{$examCenterName}}</span></td>
            
    </tr>
    <tr>
        <td colspan="5" align="center">Certified as correct by teacher/head of the centre</td>
    </tr>
    <tr>
        <td colspan="5" align="center">Signature of teacher/head of the centre:______________</td>
    </tr>
</table>
<table align="center" width="100%">
        
    <tr>
                
        <td>
                        
            <table>
                                
                <tr>
                                        
                    <td>CANDIDATE ID</td>
                                        
                    <td>NAME</td>
                                    
                </tr>
                            
                <tr>
                                            
                    <td>{{$candidates[0]['id']}}</td>
                    <td>{{$candidates[0]['name']}}</td>
                                        
                </tr>
                            
            </table>
                    
        </td>

        <td>
                            
            <table>
                                    
                <tr>
                                            
                    <td>Date of Birth:</td>
                                            
                    <td>{{$candidates[0]['date_of_birth'] }}</td>
                                        
                </tr>
                    
                <tr>
                                                
                    <td>Gender:</td>
                                                
                    <td>{{$candidates[0]['gender']}}</td>
                                            
                </tr>
                        
                <tr>
                                                    
                    <td>Identification:</td>
                                                   
                    <td>{{$candidates[0]['identification'] }}</td>
                                                
                </tr>
                                
            </table>
                        
        </td>
    </tr>
    <tr>
               
         
        <td>
                        
            <table>
                    
                <tr>
                            
                    <td colspan="2">Subject</td>
                        
                </tr>

                           @foreach ($candidates[0]['subjects'] as $value1)
                                
                    <tr>
                                    
                        <td style="padding: 0 35px 0 0;">{{$value1['code']}}</td>
                                       
                        <td>{{$value1['name']}}</td>
                                    
                    </tr>
                        @endforeach
                  
                    
                            
            </table>
                        

                        
        </td>
            
        <td>
                        
            <table>
                    
                <tr>
                            
                    <td colspan="2">Option</td>
                        
                </tr>

                           @foreach($candidates[0]['options'] as $value2)
                                
                    <tr>
                                    
                        <td style="padding:0 35px 0 0;">{{$value2['code']}}</td>
                                       
                        <td>{{$value2['name']}}</td>
                         
                                    
                    </tr>
                                @endforeach
                    
                            
            </table>
                        

                        
        </td>
                
    </tr>
        
    <tr>
            
        <td colspan="2" align="right">Signature:_____________</td>

    </tr>
</table>
</body>
</html>