<html>
<head>
    <title>Grade Report</title>
</head>
<body>
    <div class="container">
        <div align="left">
            <label> {{Carbon\Carbon::parse($current_date)->format('d-M-Y')}} </label>
        </div>
        <div  align="right">
            <label> PAGE: 1 </label>
        </div>
    
<table align="center" width="100%" style="padding-top:50px;">
    <tr>
        <td width="37%">Examination Name</td>
        <td colspan="3" width="63%">{{ $exam_details['name'] }}</td>
    </tr>
    <tr>
        <td width="37%">Examination Code</td>
        <td colspan="3" width="63%">{{ $exam_details['code'] }}</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td width="37%">Examination Centre Code</td>
        <td colspan="2" width="63%">{{ $exam_centre_detail['code'] }}</td>
    </tr>
    <tr>
        <td width="37%">Examination Centre Name</td>
        <td colspan="2" width="63%">{{ $exam_centre_detail['name'] }}</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td width="37%">Subject Code</td>
        <td colspan="2" width="63%">{{ $subject_details['code'] }}</td>
    </tr>
    <tr>
        <td width="37%">Subject Name </td>
        <td colspan="2" width="63%">{{ $subject_details['name'] }}</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td width="37%">Academic Period</td>
        <td colspan="2" width="63%">{{ $exam_details['academicPeriod']['name'] }}</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td><b>Candidate Name</b></td>
        <td><b>Date of Birth</b></td>
        <td><b>Gender</b></td>
    </tr>
        @foreach($candidate_details as $value)
            <tr>
                <td>{{$value['securityUser']['full_name']}}</td>
                <td>{{Carbon\Carbon::parse($value['securityUser']['date_of_birth'])->format('d-M-Y')}}</td>
                @if($value['securityUser']['gender']['name'] == 'Female')
                    <td> F </td>
                @else 
                    <td> M </td>
                @endif
            </tr>
        @endforeach
</table>
</div>
</body>
</html>