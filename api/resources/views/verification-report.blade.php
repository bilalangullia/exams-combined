<html>
<head>
    <title>Page Title</title>
</head>

<body>
@foreach($candidates as $value)
<table align="center" width="100%" style="padding-top:50px;">

        <tr>
                <td width="37%">Examination Name</td>
        <td colspan="3" width="63%">{{$value['examCode']}}&nbsp;-&nbsp;{{$value['examName']}}</td>
            </tr>
      <tr><td>&nbsp;</td></tr>
        <tr>
                 <td width="37%">Examination Centre Region</td>
        <td colspan="2" width="63%"><span>{{$value['areaName']}}</span>&nbsp;<span>{{$value['areaCode']}}</span></td>
            </tr>
    <tr>
        <td width="37%">Examination Centre Code</td>
        <td colspan="2" width="63%">{{$value['examCenterCode']}}</td>
    </tr>
    <tr>
        <td width="37%">Examination Centre Name</td>
        <td colspan="2" width="63%">{{$value['examCenterName']}}</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td width="37%">Candidate Number</td>
        <td colspan="2" width="63%">{{$value['id']}}</td>
    </tr>
    <tr>
        <td width="37%">National ID</td>
        <td colspan="2" width="63%">{{$value['identification']}}</td>
    </tr>
    <tr>
        <td width="37%">Candidate Name</td>
        <td colspan="2" width="63%">{{$value['name']}}</td>
    </tr>
    <tr>
        <td width="37%">Date of Birth</td>
        <td colspan="2" width="63%">{{$value['date_of_birth']}}</td>
    </tr>
    <tr>
        <td width="37%">Gender</td>
        <td colspan="2" width="63%">{{$value['gender']}}</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td><b>Option Code</b></td>
        <td><b>Option Name</b></td>
        <td><b>Signature</b></td>
    </tr>
    @foreach($value['options'] as $value2)
        <tr>
            <td>{{$value2['code']}}</td>
            <td>{{$value2['name']}}</td>
            <td>______________________</td>
        </tr>
        @endforeach
    <tr >
        <td colspan="3" align="center" style="padding-top:350px; text-align:center">Certified as correct by teacher/head of the centre</td>
    </tr>
        <tr><td colspan="3" align="center" style="padding:20px 0px; text-align:center">Signature of teacher/head of the Centre:<span>_________________________</span></td></tr>
</table>
<div style="page-break-after:always;"></div>
@endforeach
</body>
</html>
