<html>
<head>
    <title>@if(isset($reportName)) {{$reportName}} @else Page Title @endif</title>
</head>

<body>
	@if(isset($reportName))
		<strong>{{$reportName}}</strong>
	@endif
	<div style="padding-top:30px;">
		<span>Maximum Mark</span> <span>{{floatval($examComponent['examination_grading_type']['max'])}}</span>
	</div>
	<div>
		<table align="center" width="100%" style="padding-top:30px;">

			<tr>
	            <td width="37%">Examination</td>
	            <td colspan="3" width="63%">{{$exam['name']}}</td>
	        </tr>   		
    		<tr>    
            	<td width="37%">Year</td>
	            <td colspan="3" width="63%">{{$academicPeriod['start_year']}}</td>  
	        </tr>

	        <tr>
	            <td width="37%">Centre</td>
	            <td colspan="2" width="63%">{{$examCentre['name']}}</td>
	        </tr>
	        <tr>
	            <td width="37%">Subject</td>
	            <td colspan="2" width="63%">{{$examSubject['name']}}</td>
	        </tr>
	        <tr>
	            <td width="37%">Paper</td>
	            <td colspan="2" width="63%">{{$examComponent['name']}}</td>
	        </tr>
	    </table>
	</div>
	<div>
		@if(count($candidates)>0)
	        @foreach($candidates as $c)
	            <p>{{$c['security_user']['first_name']}} {{$c['security_user']['middle_name']}} {{$c['security_user']['third_name']}} {{$c['security_user']['last_name']}}</p>
	            <p>{{$c['security_user']['date_of_birth']}}  {{$c['security_user']['gender']['name']}}</p>
	        @endforeach
	    @endif
	</div>
    
</body>
</html>