<!DOCTYPE HTML>
<html lang="en" style="width:22.1cm; padding-left:1.7cm; padding-right:1.7cm;">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Multiple-choice</title>
</head>

<body style="width:22.1cm; padding-left:1.7cm; padding-right:1.7cm;">
@foreach($candidates as $value)
<section class="date_count" style="margin-top:2.3cm;">
    <div class="date" style="display: inline-block; padding-right: 550px; margin-left: -70px;">{{$value['date']}}</div>
    <div class="count" style="display: inline-block;">PAGE: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1</div>
</section>
<section class="exam-details" style="margin-top:2.4cm; width:14.9cm; height:5.5cm; margin-left:3.8cm">
    <table>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">{{$value['examName']}}</p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;">{{$value['examNameatt']}}</p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($value['examNameatt'] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <?php $year = $value['academicPeriodcode'];
        $year = substr($year, -2);
        $arr2 = str_split($year, 1);
        ?>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">{{$value['academicPeriodcode']}}{{$value['academicYr']}}</p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo $arr2[0]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($arr2[0] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:8cm;"><p style="width:1.4cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo $arr2[1]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <=9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($arr2[1] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <?php $center = $value['examCenterCode'];
        $centerarr = str_split($center, 1); ?>
        <tr>
            <td style="width:8cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">
                    {{$value['examCenterCode']}}&nbsp;-&nbsp;{{$value['examCenterName']}}
                </p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo  $centerarr[0]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @foreach(range('A', 'M') as $column)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($centerarr[0] == $column)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        @foreach(range('N', 'Z') as $val)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($centerarr[0] == $val)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endforeach
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo  $centerarr[1]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <=9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($centerarr[1] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo  $centerarr[2]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($centerarr[2] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <?php $subject = $value['subjectcode'];
        $subjectarr = str_split($subject, 1); ?>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;">
                    {{$value['subjectname']}}
                </p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo $subjectarr[0]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($subjectarr[0] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo $subjectarr[1]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($subjectarr[1] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo $subjectarr[2]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($subjectarr[2] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px;"></p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;"><?php echo $subjectarr[3]; ?></p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if ($subjectarr[3] == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:8cm; height:0.4cm;"><p style="width:8cm; height:0.4cm; margin:0; font-size:12px">{{$value['component']}}</p></td>
            <td style="width:1.4cm"><p style="width:1.4cm; height:0.4cm; margin:0;">1</p></td>
            <td>
                <table>
                    <tr>
                        @for($i = 0; $i <= 9; $i++)
                            <td style="margin-left:20px;">
                                <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                                    @if (1 == $i)
                                        <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                                    @else
                                        <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                                    @endif
                                </div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</section>
<?php $cid = $value['id'];
$cidarr2  = substr($cid, -4);
$lastid = str_split($cidarr2, 1);?>
<section class="pg-content" style="margin-top:0.8cm;">
    <p style="height:0.6cm; font-size:12px;margin-top:0.8cm;">{{$value['name']}}</p>
    <table>
        <tr>
            <td style="width:1.3cm"><?php echo $lastid[0]; ?></td>
            @for($i = 0; $i <= 9; $i++)
                <td style="margin-left:20px;">
                    <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                        @if ($lastid[0] == $i)
                            <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                        @else
                            <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                        @endif
                    </div>
                </td>
            @endfor
        </tr>
        <tr>
            <td><?php echo $lastid[1]; ?></td>
            @for($i = 0; $i <= 9; $i++)
                <td style="margin-left:20px;">
                    <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                        @if ($lastid[1] == $i)
                            <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                        @else
                            <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                        @endif
                    </div>
                </td>
            @endfor
        </tr>
        <tr>
            <td><?php echo $lastid[2]; ?></td>
            @for($i = 0; $i <= 9; $i++)
                <td style="margin-left:20px;">
                    <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                        @if ($lastid[2] == $i)
                            <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                        @else
                            <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                        @endif
                    </div>
                </td>
            @endfor
        </tr>
        <tr>
            <td><?php echo $lastid[3]; ?></td>
            @for($i = 0; $i <= 9; $i++)
                <td style="margin-left:20px;">
                    <div class="option mr-3" style="background-color:white; border-radius:50%; width:0.3cm; height:0.2cm;">
                        @if ($lastid[3] == $i)
                            <img src="{{public_path('images/pdf/black-bubble.png')}}" alt="">
                        @else
                            <img src="{{public_path('images/pdf/white-bubble.png')}}" alt="">
                        @endif
                    </div>
                </td>
            @endfor
        </tr>
    </table>
</section>
@endforeach
</body>

</html>
