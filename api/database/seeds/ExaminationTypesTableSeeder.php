<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ExaminationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\ExaminationType::create(
            [
                'name' => 'Ordinary',
                'order' => '1',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );
    }
}
