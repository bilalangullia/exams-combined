<?php

use Illuminate\Database\Seeder;
use App\Models\ExaminationStudentsCertificate;
use Carbon\Carbon;;

class CandidateCerificateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       ExaminationStudentsCertificate::create(
            [
                'certificate_id' => 'AM2012345671',
                'examination_students_id' => '1',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );

        ExaminationStudentsCertificate::create(
            [
                'certificate_id' => 'AM2012345672',
                'examination_students_id' => '2',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );
        ExaminationStudentsCertificate::create(
            [
                'certificate_id' => 'AM2012345673',
                'examination_students_id' => '3',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );
       	ExaminationStudentsCertificate::create(
            [
                'certificate_id' => 'AM2012345673',
                'examination_students_id' => '4',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );
    }
}
