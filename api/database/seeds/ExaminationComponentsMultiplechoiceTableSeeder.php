<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ExaminationComponentsMultiplechoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\ExaminationComponentsMultiplechoice::create(
            [
                'response' => 'A',
                'question' => '1',
                'examination_components_id' => '131',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );

        App\Models\ExaminationComponentsMultiplechoice::create(
            [
                'response' => 'B',
                'question' => '2',
                'examination_components_id' => '131',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );
        App\Models\ExaminationComponentsMultiplechoice::create(
            [
                'response' => 'C',
                'question' => '3',
                'examination_components_id' => '131',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );
        App\Models\ExaminationComponentsMultiplechoice::create(
            [
                'response' => 'D',
                'question' => '4',
                'examination_components_id' => '131',
                'created_user_id' => '1',
                'created' => Carbon::now()->toDateTimeString(),
            ]
        );
    }
}
