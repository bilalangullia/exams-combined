<?php

use Illuminate\Database\Seeder;

class ExaminationStudentsForecastGradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('examination_students_forecast_grades')->insert(
            [
                [
                    'key' => 'username',
                    'value' => 'testusername',
                ],
                [
                    'key' => 'password',
                    'value' => 'plain',
                ],
            ]
        );
    }
}
