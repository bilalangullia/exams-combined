<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::get(
    'clear-cache',
    function () {
        Artisan::call('route:clear');
        Artisan::call('clear-compiled');
        Artisan::call('config:cache');
        Artisan::call('cache:clear');
        return "Cache is cleared";
    }
);

Route::group(
    ["namespace" => "Authentication"],
    function () {
        Route::post('login', 'LoginController@login');
        Route::get('permissionlist', 'LoginController@permissionList')->middleware('auth.jwt');
        Route::get('user-name', 'LoginController@userName')->middleware('auth.jwt');
    }
);

Route::group(
    ["namespace" => "Register", "middleware" => "auth.jwt"],
    function () {
        Route::prefix('registration/candidate')->group(
            function () {
                Route::prefix('/dropdown')->group(
                    function () {
                        Route::get('/', 'MiscController@dropdownVals');
                        Route::get('/country/region', 'MiscController@countryWithRegionDropdown');
                        Route::get('country/{id}/region', 'MiscController@regionDropdown');
                        Route::get('/address/region', 'MiscController@addressWithAreaDropdown');
                        Route::get('/all-exam-centres', 'MiscController@getAllExamCentres');
                        Route::get('/area-administrative', 'MiscController@areaAdministrativesDropdown');
                        Route::get('/examinations', 'MiscController@examinationsDropdwon');
                        Route::get('/contact-option', 'MiscController@contactOptionsDropdwon');
                        Route::get(
                            '/region/{regionId?}/academic-id/{academicId}',
                            'MiscController@centrecodeDropdwon'
                        );
                        Route::get('/{academicperiodId}/examination', 'MiscController@examinationNameDropdwon');
                        Route::get('education-programme', 'MiscController@getEducationProgramDropdown');
                        Route::get('education-grade/{eduProId}', 'MiscController@getEducationGradeDropDown');
                        Route::get('exam-type', 'MiscController@getExaminationTypeDropDown');
                        Route::get('exam-session', 'MiscController@getExaminationSessionDropDown');
                        Route::get('examination-grading-type', 'MiscController@getExaminationGradingType');
                        Route::get(
                            'examination-itemsdropdown',
                            'MiscController@getAdministratorExaminationItemTabItemDropDown'
                        );
                        Route::get('examination-fee', 'MiscController@getExaminationFeeDropDown');
                        Route::get('examination-fee-amount', 'MiscController@getExaminationFeeAmountDropDown');
                        Route::get(
                            '/area/{areaId}/exam/{examId}',
                            'MiscController@getAllExamCentresByAreaIdAndExamId'
                        )->where('examId', '[0-9]+')->where('areaId', '[0-9]+');
                        Route::get('{examId}/examination-subject', 'MiscController@getExaminationSubject');
                        Route::get('education-subject', 'MiscController@getEducationSubject');
                        Route::get('{subjectId}/examination-options', 'MiscController@getExaminationOptionsDropdown');
                        Route::get(
                            '{optionId}/examination-components',
                            'MiscController@getExaminationComponentsDropdown'
                        );
                    }
                );
                
                Route::get('/candidate-template', 'MiscController@getCandidateTemplate');

                Route::get('/{id}/details-by-id', 'CandidateController@detailsByCandId');
                Route::get('/{id}/details-by-emis', 'CandidateController@detailsByEmisId');
                Route::get('/exam/{id}/fee', 'FeeController@examFee');
                Route::post('/register', 'CandidateController@registerCandidate');
                Route::post('/import-candidates', 'CandidateController@importCandidates');
                Route::get('/listing', 'CandidateController@listingView');
                Route::get('/subjects', 'MiscController@getSubject');
                Route::get('/view/{id}/details', 'CandidateController@candidateView');
                Route::get('autocomplete-emis-no/{id}', 'CandidateController@autocompleteEmis');
                Route::get('autocomplete-candidate-id/{id}', 'CandidateController@autocompleteCandidateId');
                Route::get('/get-candidate-detail/{id}', 'CandidateController@editData');
                Route::post('/update-candidate-detail/{id}', 'CandidateController@updateData');
                Route::get('/candidate-search', 'CandidateController@candidateSearch');
                Route::get('/generate-candidate-ids', 'CandidateController@generateCandidateIds');
                Route::get('/fee-list', 'FeeController@getFeeList');
                Route::get('/list-reports', 'ReportController@listReports');
                Route::post('/process-report', 'ReportController@processReport');
                Route::post('/generate-report', 'ReportController@generateReport');
                Route::post('/download-report', 'ReportController@downloadReport');
                Route::delete('destroy-candidate/{candId}', 'CandidateController@destroyCandidate');
                Route::post('/advance-search', 'CandidateController@advanceSearch');
                Route::get('/get-percentage-reports/{id}', 'ReportController@getPercentageReports');
            }
        );
        Route::get("classification-dropdown", 'MiscController@getClassificationDropdown');
        Route::get('dropdown/get-ownershiplist', 'MiscController@getOwnershipList');
        Route::get('dropdown/get-examcentrelist/{examId}', 'MiscController@getExamCentreList');
        Route::get('dropdown/qualificationtitlelist', 'MiscController@getExaminationQualificationTitle');
        Route::get('dropdown/qualificationlevel', 'MiscController@getExaminationQualificationlevel');
        Route::get('dropdown/qualificationfield-of-study', 'MiscController@getExaminationEducationFieldOfStudy');
        Route::get("dropdown/qualification-specialisation", 'MiscController@getQualificationSpecialisation');
        Route::get("dropdown/{examId}/examcentres", 'MiscController@getCentreExams');
        Route::get("dropdown/groups", 'MiscController@getGroupDropdown');
        Route::get("dropdown/education-level", 'MiscController@getEducationLevelDropdown');
        Route::get("dropdown/education-program/{levelId}", 'MiscController@getEducationProgramDropdownViaLevelId');
        Route::get("dropdown/education-cycle", 'MiscController@educationStructureCycleDropdown');
        Route::get("dropdown/{levelId}/education-cycle", 'MiscController@getEducationStructureCycle');
        Route::get("dropdown/option-list", 'MiscController@getOptionDropdown');
        Route::get("dropdown/education-certification", 'MiscController@getEducationCertificationDropdown');
        Route::get("dropdown/education-stage", 'MiscController@getEducationStageDropdown');
        Route::get("dropdown/education-level-prog", 'MiscController@getEducationLevelForProgrTab');
        Route::get("dropdown/education-grade/{programId}", 'MiscController@getEducationGradeDropdownViaProgramId');
        Route::get("dropdown/education-subject", 'MiscController@getEducationSubjectDropdown');
        Route::get("dropdown/area-administrative", 'MiscController@getAreaAdministrativeDropdown');
        Route::get("dropdown/country", 'MiscController@getCountryDropdown');
        Route::get("dropdown/system-configuration", 'MiscController@getSystemConfigurationDropdown');
        Route::get('dropdown/examination-grading-type', 'MiscController@getExaminationGradingType');
        Route::get('dropdown/examination-subject', 'MiscController@getExaminationSubjectList');
    }
);
Route::group(
    ["namespace" => "Marks"],
    function () {
        Route::group(
            ["prefix" => 'multiple-choice', "middleware" => "auth.jwt"],
            function () {
                Route::get('candidates-list', 'MultiChoicesController@getMultipleChoiceCandidateList');
                Route::get('{examId}/subjects', 'MultiChoicesController@getMultipleChoiceSubjectDropDown');
                Route::get('{optionId}/components', 'MultiChoicesController@getMultipleChoiceComponentDropDown');
                Route::get('{compId}/questions', 'MultiChoicesController@getMultipleChoiceQuestionDropDown');
                Route::get('candidate/{candId}/view', 'MultiChoicesController@getMultipleChoiceCandidateDetails');
                Route::post('{candId}/update-response/{compId}', 'MultiChoicesController@storeCandidateMultipleChoiceResponse');
                Route::post('candidate/{candId}/queList', 'MultiChoicesController@getMultiChoiceCandidateResponse');
                Route::get('candidates/response', 'MultiChoicesController@getMultipleChoiceAllCandidateResponse');
                Route::post('import-questions-response', 'MultiChoicesController@importMulitpleChoice');
                Route::post('generate-all', 'MultiChoicesController@generateAllCandidateMark');
            }
        );
        Route::group(
            ["prefix" => 'marks/component', "middleware" => "auth.jwt"],
            function () {
                Route::get('option-list/{examId}', 'ComponentController@getComponentoptionList');
                Route::get('component-tab/{optionId}', 'ComponentController@getComponentTab');
                Route::get('component-list', 'ComponentController@getComponentMarkList');
                Route::get('status-dropdown', 'ComponentController@getStatusDropdown');
                Route::get('marktype-dropdown', 'ComponentController@getMarkTypeDropdown');
                Route::get('detailsby-cid', 'ComponentController@candidateDetailsViaRequestParam');
                Route::post('mark-componentadd', 'ComponentController@candidateMarkAdd');
                Route::get('search-markcomponent', 'ComponentController@marksComponentSearch');
                Route::post('import', 'ComponentController@marksComponentImport');
            }
        );
    }
);
Route::group(
    ["namespace" => "Administration\Examinations\Exams"],
    function () {
        Route::group(
            ["prefix" => 'administration/examinations/exam', "middleware" => "auth.jwt"],
            function () {
                Route::get("exam-list", 'ExaminationController@getAdministrationExaminationExamList');
                Route::get("exam-detail/{examId}", 'ExaminationController@getAdministrationExaminationExamDetail');
                Route::post("update/{examId}", 'ExaminationController@updateAdministrationExaminationExam');
                Route::post("add-exam", 'ExaminationController@storeAdministrationExamination');
                Route::delete("delete-exam/{examID}", 'ExaminationController@destroyAdministratorExamination');
                Route::post("copy-exam", 'ExaminationController@copyAdministrationExamination');
            }
        );

        Route::group(
            ["prefix" => 'administration/examinations/option', "middleware" => "auth.jwt"],
            function () {
                Route::get("{examId}/option-list", 'OptionController@getAdministratorExamsOptionList');
                Route::get("{optionId}/option-details", 'OptionController@getAdministratorExamsOptionDetail');
                Route::post("update/{optionId}", 'OptionController@updateAdministratorExaminationOptionRecord');
                Route::delete("{optionId}/destroy", 'OptionController@destroyAdministratorExaminationOption');
                Route::post("add-option", 'OptionController@storeAdministratorExaminationOption');
                Route::post("import-option", 'OptionController@administratorExaminationOptionImport');
            }
        );
        Route::group(
            ["prefix" => 'administration/examinations/subject', "middleware" => "auth.jwt"],
            function () {
                Route::get('subject-listing/{examId}', 'SubjectController@getAdministrationExaminationSubjectlisting');
                Route::get(
                    'subject-view/{examsubjectId}',
                    'SubjectController@getAdministrationExaminationSubjectView'
                );
                Route::get(
                    'subject-edit/{examsubjectId}',
                    'SubjectController@getAdministrationExaminationSubjectedit'
                );
                Route::get(
                    'education-subjectdropdown',
                    'SubjectController@getAdministrationExaminationEducationSubjectList'
                );
                Route::post(
                    'update-educationsubject/{subjectid}',
                    'SubjectController@getAdministrationExaminationSubjectupdate'
                );
                Route::post(
                    'add-examinationsubject',
                    'SubjectController@addAdministrationExaminationSubject'
                );
                Route::delete("{examSubjectId}/destroy", 'SubjectController@destroyAdministratorExaminationSubject');
                Route::post("import", 'SubjectController@importExaminationSubject');
            }
        );

        Route::group(
            ["prefix" => 'administration/examinations/component', "middleware" => "auth.jwt"],
            function () {
                Route::get('component-list/{examId}', 'ComponentController@getExaminationComponentList');
                Route::get('component-view/{componentId}', 'ComponentController@getExaminationComponentView');
                Route::get('component-edit/{componentId}', 'ComponentController@getExaminationComponentEdit');
                Route::get('component-type', 'ComponentController@getExaminationComponentType');
                Route::post('update-component/{componentId}', 'ComponentController@getupdateExaminationComponent');
                Route::post('add-component', 'ComponentController@addAdministratorExaminationComponent');
                Route::get('component-list', 'ComponentController@getComponentList');
                Route::delete('{componentId}/destroy', 'ComponentController@destroyAdministratorExaminationsComponent');
                Route::post('import', 'ComponentController@importExaminationComponent');
            }
        );

        Route::group(
            ["prefix" => 'administration/examinations/items', "middleware" => "auth.jwt"],
            function () {
                Route::get("{examId}/items-list", 'ItemsController@getAdministratorExaminationItemList');
                Route::get("{itemId}/items-detail", 'ItemsController@getAdministratorExaminationItemDetails');
                Route::post("{itemId}/update", 'ItemsController@updateAdministratorExaminationItems');
                Route::delete("destroy/{itemId}", 'ItemsController@destroyAdministratorExaminationItems');
                Route::post("add-item", 'ItemsController@storeAdministratorExaminationItem');
                 Route::post("import-item", 'ItemsController@administratorExaminationItemImport');
            }
        );
    }
);
Route::group(
    ["namespace" => "Administration\Examinations\Centres"],
    function () {
        Route::group(
            ["prefix" => 'administration/examinations/centre', "middleware" => "auth.jwt"],
            function () {
                Route::get('room/list/{examCentreId}', 'ExamCentreRoomController@getExamCentreRoomList');
                Route::get('room/view/{examCentreRoomId}', 'ExamCentreRoomController@getExamCentreRoomDetails');
                Route::post('room/update', 'ExamCentreRoomController@examCentreRoomUpdate');
                Route::post('room/store', 'ExamCentreRoomController@examCentreRoomStore');
                Route::delete('room/{roomId}', 'ExamCentreRoomController@examCentreRoomDelete');

                Route::get(
                    'invigilator/list/{examCentreId}',
                    'ExamCentreInvigilatorController@examCentreInvigilatorList'
                );
                Route::get(
                    'invigilator/view/{examCentreId}/{invigilatorId}',
                    'ExamCentreInvigilatorController@getExamCentreInvigilatorDetails'
                );
                Route::get('invigilator/dropdown', 'ExamCentreInvigilatorController@getInvigilatorDropdown');
                Route::post('invigilator/store', 'ExamCentreInvigilatorController@storeExamCentreInvigilator');
                Route::delete(
                    '{examCentreId}/invigilator/{invigilatorId}',
                    'ExamCentreInvigilatorController@examCentreInvigilatorDelete'
                );

                Route::get(
                    'student/list/{examCentreId}/{examinationId}',
                    'ExamCentreStudentController@getExamCentreStudentList'
                );
                Route::get(
                    'student/view/{studentId}',
                    'ExamCentreStudentController@examCentreStudentDetails'
                );

                Route::get(
                    'subject/list/{examinationId}',
                    'ExamCentreSubjectController@getExamCentreSubjectList'
                );

                Route::get(
                    'subject/view/{examCentreId}/{subjectId}',
                    'ExamCentreSubjectController@examCentreSubjectView'
                );

                Route::get('list', 'ExamCentreController@getExamCentreList');
                Route::get(
                    '{examCentreId}',
                    'ExamCentreController@examCentreOverview'
                );
                Route::post('update', 'ExamCentreController@examCentreUpdate');
                Route::post('store', 'ExamCentreController@examCentreStore');
                Route::delete('delete/{examCentreId}', 'ExamCentreController@examCentreDelete');
                Route::post('copy', 'ExamCentreController@examCentreCopy');
                Route::post('import', 'ExamCentreController@examCentreImport');
            }
        );
    }
);

Route::group(
    ["namespace" => "Administration\Examinations"],
    function () {
        Route::group(
            ["prefix" => 'administration/examinations/fees', "middleware" => "auth.jwt"],
            function () {
                Route::get("fee-list", 'FeeController@getAdministratorExaminationsFeeList');
                Route::get("{examFeeId}/fee-details", 'FeeController@getAdministratorExaminationsFeeDetails');
                Route::post("fee-add", 'FeeController@addAdministratorExaminationsFee');
                Route::post("fee-update/{examFeeId}", 'FeeController@updateAdministratorExaminationsFee');
                Route::delete("fee-delete/{examFeeId}", 'FeeController@destroyAdministratorExaminationsFee');
            }
        );

        Route::group(
            ["prefix" => 'administration/examinations/grading-type', "middleware" => "auth.jwt"],
            function () {
                Route::get('list', 'GradingTypeController@getGradingTypeList');
                Route::get('{gradingTypeId}', 'GradingTypeController@getGradingTypeDetails');
                Route::post('update', 'GradingTypeController@getGradingTypeUpdate');
                Route::delete('{gradingTypeId}', 'GradingTypeController@gradingTypeDelete');
                Route::post('store', 'GradingTypeController@gradingTypeStore');
                Route::post('import', 'GradingTypeController@gradingTypeImport');
            }
        );

        Route::group(
            ['prefix' => 'administration/examinations/multiple-choice', "middleware" => "auth.jwt"],
            function () {
                Route::post(
                    'update-response/{componentId}',
                    'MultipleChoiceController@updateAdministrationExaminationsMultipleChoiceAnswer'
                );
            }
        );
    }
);
Route::group(
    ["namespace" => "Administration\Examinations\Markers", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'administration/examinations/markers'],
            function () {
                Route::get("marker-list", 'MarkerController@getAdministratorExaminationMarkerListing');
                Route::get("{openemisno}/marker-view", 'MarkerController@getAdministratorExaminationMarkerView');
                Route::get("{openemisno}/marker-edit", 'MarkerController@getAdministratorExaminationMarkerEdit');
                Route::get(
                    "{examinerId}/autocomplete-examinerid",
                    'MarkerController@getAdministratorExaminationAutocompleteExaminerId'
                );
                Route::get(
                    "{examinerId}/details-by-examinerid",
                    'MarkerController@getAdministratorExaminationDetailsByExaminerId'
                );
                Route::post("add-marker", 'MarkerController@getAdministratorExaminationAddMarker');
                Route::post("{examinerId}/update-marker", 'MarkerController@getAdministratorExaminationUpdateMarker');
                Route::delete("{examinerId}/delete-marker", 'MarkerController@destroyAdministratorExaminationMarker');
            }
        );
        Route::group(
            ["prefix" => 'administration/examinations/qualifications'],
            function () {
                Route::get(
                    "{openEmisno}/qualification-list",
                    'QualificationController@getAdministratorExaminationQualificationListing'
                );
                Route::get(
                    "{staffqualificationId}/qualification-view",
                    'QualificationController@getAdministratorExaminationQualificationView'
                );
                Route::get(
                    "{staffqualificationId}/qualification-edit",
                    'QualificationController@getAdministratorExaminationQualificationEdit'
                );
                Route::post(
                    "{openEmisno}/qualification-add",
                    'QualificationController@addExaminationStaffQualification'
                );
                Route::post(
                    "{staffId}/qualification-update",
                    'QualificationController@updateExaminationStaffQualification'
                );
                Route::delete(
                    "{staffqualificationId}/qualification-delete",
                    'QualificationController@destroyExaminationQualification'
                );
            }
        );
    }
);

Route::group(
    ["namespace" => "Results", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'results/forecast_grades'],
            function () {
                Route::post("import", 'ForecastGradeController@forecastGradeImport');
                Route::get('forecast-grade-dropdown/{optionId}', 'ForecastGradeController@getForecastGradeDropdown');
                Route::post('update-forecatGrade/{candidateId}', 'ForecastGradeController@updateResultsForecastGrade');
                Route::get('forecast-list', 'ForecastGradeController@getForecastCandidateList');
            }
        );
        Route::group(
            ["prefix" => 'results/grade-reviews'],
            function () {
                Route::post('add-gradereview', 'GradeReviewController@resultsAddGradeReview');
                Route::get('gradereview-list', 'GradeReviewController@resultsGradeReviewListing');
                Route::get('{candidateId}/gradereview-view', 'GradeReviewController@resultsGradeReviewView');
            }
        );
        Route::group(
            ["prefix" => 'results/final-grade'],
            function () {
                Route::post('generate', 'FinalGradeController@resultsFinalGradeGenerateButton');
            }
        );
    }
);


Route::group(
    ["namespace" => "Administration\Security"],
    function () {
        Route::group(
            ["prefix" => 'administration/security/user-groups', "middleware" => "auth.jwt"],
            function () {
                Route::get('user-group-list', 'SecurityGroupController@getAdministrationSecurityGroupsList');
                Route::get(
                    'autocomplete-security-users/{openemisId}',
                    'SecurityGroupController@getAutocompleteSecurityUsers'
                );
                Route::get('autocomplete-area/{areaName}', 'SecurityGroupController@getAutocompleteArea');
                Route::get(
                    'autocomplete-institution/{institutionsName}',
                    'SecurityGroupController@getAutocompleteInstitutions'
                );
                Route::get('user-group-details/{groupId}', 'SecurityGroupController@getSecurityUserGroupDetails');
                Route::get('role-dropdown', 'SecurityGroupController@getSecurityRoleDropdown');
                Route::post('add-user-group', 'SecurityGroupController@addSecurityUserGroup');
                Route::put('update-user-group/{userGroupId}', 'SecurityGroupController@updateSecurityUserGroup');
                Route::delete('delete-user-group/{userGroupId}', 'SecurityGroupController@destroySecurityUserGroup');
            }
        );


        Route::group(
            ["prefix" => 'administration/security/system-groups', "middleware" => "auth.jwt"],
            function () {
                Route::get('system-group-list', 'SecurityGroupController@getAdministrationSecuritySystemList');
                Route::get(
                    'system-group-details/{sysGroupId}',
                    'SecurityGroupController@getSecuritySystemGroupDetails'
                );
            }
        );

        Route::group(
            ["prefix" => 'administration/security/users', "middleware" => "auth.jwt"],
            function () {
                Route::get("user-listing", 'UserController@securityUserList');
                Route::get("{openEmisno}/user-details", 'UserController@securityUserView');
                Route::get("{openEmisno}/user-details-edit", 'UserController@securityUserEdit');
                Route::post("user-added", 'UserController@securityUserAdded');
                Route::post("{openEmisno}/user-update", 'UserController@securityUserUpdate');
                Route::get("{openEmisno}/user-account-view", 'UserController@securityAccountView');
                Route::post("{username}/user-account-edit", 'UserController@securityAccountEdit');
                Route::get("/generate-emisno", 'UserController@securityuserOpenemis');
            }
        );

        Route::group(
            ["prefix" => 'administration/security/user-roles', "middleware" => "auth.jwt"],
            function () {
                Route::get("list/{groupId}", 'RoleController@userRolesList');
                Route::get("view/{roleId}", 'RoleController@userRoleView');
                Route::post("store", 'RoleController@userRoleStore');
                Route::post("update", 'RoleController@userRoleUpdate');
                Route::post("reorder/{groupId}", 'RoleController@securityRoleReorder');
                Route::get("permission-list/{roleId}", 'RoleController@permissionList');
                Route::post("permission-list/update", 'RoleController@permissionListUpdate');
            }
        );

        Route::group(
            ["prefix" => 'administration/security/system-roles', "middleware" => "auth.jwt"],
            function () {
                Route::get("list", 'RoleController@systemRolesList');
                Route::get("view/{roleId}", 'RoleController@systemRoleView');
                Route::post("store", 'RoleController@systemRoleStore');
                Route::post("update", 'RoleController@systemRoleUpdate');
                Route::post("reorder", 'RoleController@securityRoleReorder');
                Route::get("permission-list/{roleId}", 'RoleController@permissionList');
                Route::post("permission-list/update", 'RoleController@permissionListUpdate');
            }
        );
    }
);

Route::group(
    ["namespace" => "Reports\Forms", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'reports/forms/multiple-choice'],
            function () {
                Route::get('candidate-dropdown', 'FormReportController@candidateListDropdown');
                Route::get('{candidates}/subject-dropdown', 'FormReportController@candidateSubjectDropdown');
                Route::post('/process-report', 'FormReportController@processReport');
                Route::post('/generate-report', 'FormReportController@generateReport');
                Route::get('/download-report', 'FormReportController@downloadReport');
                Route::get('/list-reports', 'FormReportController@listReports');
            }
        );

        Route::group(
            ["prefix" => 'reports/forms/grades'],
            function () {
                Route::post('generate-forecastGrade-report', 'ForecastGradeFormController@generateReport');
            }
        );

        Route::group(
            ["prefix" => 'reports/forms'],
            function () {
                Route::post('generate/ms-examiner', 'MsExaminerReportController@generateMsExaminerReport');
            }
        );
    }
);

Route::group(
    ["namespace" => "Reports\Marks", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'reports/marks/coursework-marks'],
            function () {
                Route::get('candidate-dropdown', 'MarksReportController@candidateListDropdown');
                Route::get('{optionId}/component-dropdown', 'MarksReportController@componenetListDropdown');
                Route::post('generate-report', 'MarksReportController@generateReport');
            }
        );
    }
);
Route::group(
    ["namespace" => "Reports\Results", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'reports/results'],
            function () {
                Route::post('generate-report-result', 'ResultController@generateReport');
            }
        );
    }
);

Route::group(
    ["namespace" => "Certificates", "middleware" => 'auth.jwt'],
    function () {
        Route::group(
            ["prefix" => 'certificates/duplicates'],
            function () {
                Route::get('listing', 'DuplicateController@certificateDuplicatesList');
                Route::get('{certificateId}/view', 'DuplicateController@certificateduplicatesView');
                Route::post('add', 'DuplicateController@certificateDuplicatesAdd');
                Route::post('{certificateId}/update', 'DuplicateController@certificateDuplicatesUpdate');
                Route::post('import', 'DuplicateController@importDuplicatesCertificate');
            }
        );

        Route::group(
            ["prefix" => 'certificates/collections'],
            function () {
                Route::get('get-list', 'CollectionController@getCertificatesCollectionList');
                Route::get('get-detail/{collectionId}', 'CollectionController@getCertificateCollectionDetail');
                Route::post('add-collection', 'CollectionController@storeCertificateCollection');
                Route::put('update-collection/{collectionId}', 'CollectionController@updateCertificateCollection');
                Route::post('import', 'CollectionController@collectionImport');
            }
        );
    }
);

Route::group(
    ["namespace" => "Reports\Certificates", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'reports/certificates'],
            function () {
                Route::get('dropdown-examCentre', 'CertificatesReportController@getExaminationCentreDropdown');
                Route::post('generate-report-certificate', 'CertificatesReportController@generateReport');
            }
        );
    }
);

Route::group(
    ["namespace" => "Reports\Statistics", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'reports/statistics'],
            function () {
                Route::post('candidate-per-subject', 'StatisticController@candidatePerSubjectReport');
                Route::get('list', 'StatisticController@statisticsReportList');
                Route::post('exam-reports', 'StatisticController@candidatePerCentreReport');
            }
        );
    }
);

Route::group(
    ["namespace" => "Administration\SystemSetup\EducationStructure", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'administration/systemsetup/educationstructure'],
            function () {
                Route::get('education-system-list', 'EducationSystemController@getAdministrationSystemSetupEducationSystemList');
                Route::get('{eduSysId}/education-system-detail', 'EducationSystemController@getAdministrationSystemSetupEducationSystemDetail');
                Route::post('add-education-system', 'EducationSystemController@storeAdministrationSystemSetupEducationSystem');
                Route::put('{eduSysId}/update-education-system', 'EducationSystemController@updateAdministrationSystemSetupEducationSystem');
                Route::delete('{eduSysId}/destroy-education-system', 'EducationSystemController@destroyAdministrationSystemSetupEducationSystem');
            }
        );

        Route::group(
            ["prefix" => 'administration/systemsetup/educationstructure'],
            function () {
                Route::get('education-level-list', 'EducationLevelController@getAdministrationSystemSetupEducationLevelList');
                Route::get('{eduLvlId}/education-level-details', 'EducationLevelController@getAdministrationSystemSetupEducationLevelDetail');
                Route::get('edu-sys-drpdwn', 'EducationLevelController@getEducationSystemDropdown');
                Route::get('edu-lvlIsc-drpdwn', 'EducationLevelController@getEducationLevelIscedDropdown');
                Route::post('add-education-level', 'EducationLevelController@storeAdministrationSystemSetupEducationLevel');
                Route::put('{eduLevelId}/update-education-level', 'EducationLevelController@updateAdministrationSystemSetupEducationLevel');
                Route::delete('{eduLevelId}/destroy-education-level', 'EducationLevelController@destroyAdministrationSystemSetupEducationLevel');
            }
        );
    }
);

Route::group(
    ["namespace" => "Administration\SystemSetup\EducationStructure", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'administration/systemsetup/educationstructure'],
            function () {
                Route::get('education-grades/{programId}', 'EducationGradeController@educationGrades');
                Route::get('education-grades/view/{gradeId}', 'EducationGradeController@educationGradeView');
                Route::post('education-grades/store', 'EducationGradeController@educationGradeStore');
                Route::post('education-grades/update', 'EducationGradeController@educationGradeUpdate');
                Route::get('education-grade-subject/list/{gradeId}', 'EducationGradeController@educationGradeSubjects');
                Route::get('education-grade-subject/view/{gradeId}/{subjectId}', 'EducationGradeController@educationGradeSubjectView');
                Route::get('education-grade-subject/add/{gradeId}', 'EducationGradeController@educationGradeSubjectAdd');
                Route::post('education-grade-subject/store', 'EducationGradeController@educationGradeSubjectStore');
                Route::post('education-grade-subject/update', 'EducationGradeController@educationGradeSubjectUpdate');
                Route::get('education-setup/list/{setUpId}', 'EducationGradeController@educationGradeSetUp');
                Route::get('education-setup/subject/view/{subjectId}', 'EducationGradeController@educationSetupSubjectView');
                Route::post('education-setup/subject/store', 'EducationGradeController@educationSetupSubjectStore');
                Route::post('education-setup/subject/update', 'EducationGradeController@educationSetupSubjectUpdate');
                Route::post('education-grades/reorder', 'EducationGradeController@educationGradeReorder');
                Route::post('education-setup/subject/reorder', 'EducationGradeController@educationSubjectReorder');
                Route::delete('education-grades/{gradeId}', 'EducationGradeController@educationGradeDelete');
                Route::delete('education-grade-subject/{gradeId}/{subjectId}', 'EducationGradeController@educationGradeSubjectDelete');
            }
        );
    }
);
Route::group(
    ["namespace" => "Administration\SystemSetup\EducationStructure","middleware" => 'auth.jwt'],
    function () {
        Route::group(
            ["prefix" => 'administration/systemsetup/educationstructure'],
            function () {
                Route::get('{educationlevelId}/education-cycle-list', 'EducationSystemCycleController@educationSystemCycleList');
                Route::get('{eduCycleId}/education-cycle-view', 'EducationSystemCycleController@educationSystemCycleView');
                Route::get('{eduCycleId}/education-cycle-edit', 'EducationSystemCycleController@educationSystemCycleEdit');
                Route::post('education-cycle-add', 'EducationSystemCycleController@educationStructureCycleAdd');
                Route::post('{eduCycleId}/education-cycle-update', 'EducationSystemCycleController@educationStructureCycleUpdate');
                Route::get('{eduCycleId}/education-programme-list', 'EducationStructureProgrammesController@educationStructureProgrammesList');
                Route::get('{programmeId}/education-programme-view', 'EducationStructureProgrammesController@educationStructureProgrammesView');
                Route::post('education-programme-add', 'EducationStructureProgrammesController@addEducationStructureProgrammes');
                Route::get('dropdown/{programmeId}/add-next-programme', 'EducationStructureProgrammesController@addNextProgrammeDropdown');
                Route::get('{nextProgId}/add-cycle-programme', 'EducationStructureProgrammesController@addNextCycleProgramme');
                Route::post('{programmeId}/update-education-programme', 'EducationStructureProgrammesController@updateEducationProgrammes');
                Route::get('{programmeId}/edit-education-programme', 'EducationStructureProgrammesController@editEducationStructureProgrammes');
            }
        );
    }
);

Route::group(
    ["namespace" => "Administration\SystemSetup\AdministrativeBoundaries", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'administration/systemsetup/administrative-boundaries'],
            function () {
                Route::get('area-level-education/list', 'AdministrativeBoundaryController@areaLevelEducationList');
                Route::get('area-level-education/view/{level}', 'AdministrativeBoundaryController@areaLevelEducationView');
                Route::post('area-level-education/update', 'AdministrativeBoundaryController@areaLevelEducationUpdate');
                Route::post('area-level-education/store', 'AdministrativeBoundaryController@areaLevelEducationStore');
                Route::get('area-education/list', 'AdministrativeBoundaryController@areaEducationList');
                Route::get('area-education/view/{areaId}', 'AdministrativeBoundaryController@areaEducationView');
                Route::post('area-education/update', 'AdministrativeBoundaryController@areaEducationUpdate');
                Route::post('area-education/store', 'AdministrativeBoundaryController@areaEducationStore');
                Route::get('area-education/area-level-dropdown/{parentId}', 'AdministrativeBoundaryController@areaLevelDropdown');
                Route::get('area-level-administrative/view/{levelId}', 'AdministrativeBoundaryController@areaLevelAdministrativeView');
                Route::get('area-level-administrative/list/{areaId}', 'AdministrativeBoundaryController@areaLevelAdministrativeList');
                Route::post('area-level-administrative/store', 'AdministrativeBoundaryController@areaLevelAdministrativeStore');
                Route::post('area-level-administrative/update', 'AdministrativeBoundaryController@areaLevelAdministrativeUpdate');
                Route::get('area-administrative/list', 'AdministrativeBoundaryController@areaAdministrativeList');
                Route::get('area-administrative/view/{areaId}', 'AdministrativeBoundaryController@areaAdministrativeView');
                Route::get('area-administrative/area-level-dropdown/{parentId}', 'AdministrativeBoundaryController@areaAdministrativeLevelDropdown');
                Route::post('area-administrative/store', 'AdministrativeBoundaryController@areaAdministrativeStore');
                Route::post('area-administrative/update', 'AdministrativeBoundaryController@areaAdministrativeUpdate');
                Route::delete('area-level-education/{levelId}', 'AdministrativeBoundaryController@areaLevelEducationDelete');
                Route::delete('area-education/{areaId}', 'AdministrativeBoundaryController@areaEducationDelete');
                Route::delete('area-level-administrative/{levelId}', 'AdministrativeBoundaryController@areaLevelAdministrativeDelete');
                Route::delete('area-administrative/{areaId}', 'AdministrativeBoundaryController@areaAdministrativeDelete');
            }
        );
    }
);


Route::group(
    ["namespace" => "Administration\SystemSetup\AcademicPeriod", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'administration/systemsetup/academic_period_level'],
            function () {
                Route::get('list-academic-period-level', 'AcademicPeriodLevelController@getSystemSetupAcademicPeriodLevelList');
                Route::get('detail-academic-period-level/{academicLevel}', 'AcademicPeriodLevelController@getSystemSetupAcademicPeriodLevelDetail');
                Route::post('store-academic-period-level', 'AcademicPeriodLevelController@storeSystemSetupAcademicPeriodLevel');
                Route::put('update-academic-period-level/{academicLevel}', 'AcademicPeriodLevelController@updateSystemSetupAcademicPeriodLevel');
                Route::delete('delete-academic-period-level/{academicLevel}', 'AcademicPeriodLevelController@destroySystemSetupAcademicPeriodLevel');
            }
        );

        Route::group(
            ["prefix" => 'administration/systemsetup/academic_period'],
            function () {
                Route::get('list-academic-period', 'AcademicPeriodController@getSystemSetupAcademicPeriodList');
                Route::get('detail-academic-period/{academicPeriodId}', 'AcademicPeriodController@getSystemSetupAcademicPeriodDetail');
                Route::get('academic-period-level-dropdown', 'AcademicPeriodController@academicPeriodLevelDropdown');
                Route::post('store-academic-period', 'AcademicPeriodController@addSystemSetupAcademicPeriod');
                Route::put('update-academic-period/{academicPeriodId}', 'AcademicPeriodController@updateSystemSetupAcademicPeriod');
                Route::delete('delete-academic-period/{academicPeriodId}', 'AcademicPeriodController@destroySystemSetupAcademicPeriod');
            }
        );
    }
);
Route::group(
    ["namespace" => "Administration\SystemSetup\SystemConfiguration", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'administration/systemsetup/system-configuration'],
            function () {
                Route::get('{systemconfigId}/list', 'SystemConfigurationController@systemConfigurationList');
                Route::get('{systemconfigId}/view', 'SystemConfigurationController@systemConfigurationView');
                Route::post('{systemconfigId}/update', 'SystemConfigurationController@systemConfigurationUpdate');
            }
        );
    }
);

Route::group(
    ["namespace" => "Administration\SystemSetup\FieldOptions", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'administration/systemsetup/field-option'],
            function () {
                Route::get('list', 'AttendanceTypeController@getFieldOptionListing');
                Route::get('detail/{optionId}', 'AttendanceTypeController@getFieldOptionDetail');
                Route::post('store', 'AttendanceTypeController@storeFieldOption');
                Route::post('update', 'AttendanceTypeController@updateFieldOption');
                Route::delete('destory', 'AttendanceTypeController@destroyFieldOption');
            }
        );
    }
);
Route::group(
    ["namespace" => "Register\Fee", "middleware" => "auth.jwt"],
    function () {
        Route::group(
            ["prefix" => 'register/fee'],
            function () {
                Route::get('list', 'FeeController@registrationFeeList');
                Route::get('{candId}/details/{examId}', 'FeeController@registrationFeeDetails');
                Route::post('{candId}/add', 'FeeController@registrationFeeAdd');
            }
        );
    }
);
